CraFT Virtual DMA
===============

version 1.1.0 Harmonic\
*Wednesday February 24 2021*

To compile **CraFT Virtual DMA / 1.1.0 Harmonic version** you can do it the same way than the original CraFT version (see procedure bellow), with additional option: `HARMONIC=yes` to be set in file [`./options.in`](options.in).\
In this package, `HARMONIC` option is already set to `yes`.\
Please see "**Compiling CraFT (and companions)**" section below.

**CraFT** is written mostly in *C (C99)* and some features are written in *Fortran 90* (in an almost *Fortran 77* manner). Its installation has been tested on Linux Debian Squeeze but it should be able to be compiled on any Unix system. The following text gives some details on how to compile it.

In this package, craft is accompanied with two other programs:
* **xhmv**: which is a simple 2d image viewer (I wrote it several years ago, but it can be useful) 
* **pycraft**: which is a pre- post-processing program written in python

Prerequisites:
-----------------

For all the codes (**craft**, **xhmv**, **pycraft**), you need:
   * **Gnu make** 
   * a *C* compiler with *C99* features + a *Fortran 90* compiler\
     Typically:
     - either *gnu* compilers: **gcc + gfortran**
     - or *Intel* compilers  : **icc + ifort**

To install **craft**, you need:
   * **FFTW3**: You can download it from www.fftw.org or as a package of your linux distibution (if it is your os).\
     [ *Remark:* CraFT uses fast Fourier transforms. It could be implemented with different FFT packages (previous versions of the algorithm used many other FFT packages) but this version has been implemented with FFTW3 package. If you prefer to use another FFT package, I'm afraid you will have to modify [`./craft/src/craft_fft_image.c`](craft/src/craft_fft_image.c) file. ]
   * **lapack, blas**: You can download lapack and blas libraries form www.netlib.org or as packages of your linux distributions. 
   * **hdf5**: HDF5 version > 1.8.0 (optional. See [`options.in`](options.in) file)
   * **texlive**: Texlive is require to create CraFT documentation in pdf files.

To install **xhmv**, you need:
* **X11** library
* **gtk+-2.0**
  
Compiling CraFT (and companions):
-------------------------------------------

* If necessary edit and modify what you need in file [`./options.in`](options.in) present in this directory. In this file, you should just have:

  1. to choose the compilers you want to use,
      set `COMPILERS` variable: \
      `COMPILERS=intel` if you want to use **icc** and **ifort**\
      `COMPILERS=gnu` if you want to use **gcc** and **gfortran**

  2. to choose to use *OpenMP parallelization* option by setting `OPENMP` variable to `yes` or `no`

  3. to choose to get a *static* or a *dynamic* executable by
      setting `STATIC_LIBRARIES` to `yes` or `no`

  4. to set the directories where `fftw3` libraries and include files have been installed.

  5. to set `XHMV` to `yes` or `no` if you want or don't want to install **xhmv** (simple 2d image viewer)

  6. to set `PYCRAFT` to `yes` or `no` if you want or don't want to install **pycraft** (pre- post-processing program written in python)

  7. to set `HARMONIC` to `yes` if you want to compile **CraFT-Virtual-DMA / 1.1.0 harmonic** version (default option in this package).\
     At the end of the compilation procedure, the following command\
       `./craft –V`\
     in directory \
        `craft_1.1.0_harmonic/bin/`\
     should return\
        `$ CraFT version 1.1.0 Harmonic`

  Of course, you can change other things in [`options.in`](options.in) (typically `CFLAGS`, `FFLAGS` variables) but it is not necessary.\
  [`options.in`](options.in) has been initially set to compile with **gcc** anf **gfortran** compiler without `OpenMP` options, and to build an executable using *dynamic libraries*.


* Type `make` or `make all` in the current directory `./` (the one containing this `INSTALL.md` file)

  This should build the executables:\
  `./bin/craft` (CraFT program)\
  `./bin/xhmv`\
  `./lib/python/craft/*` (pycraft)

  and some tools:\
  `./bin/i3dtovtk`         (converts *i3d* CraFT image files into simple *legacy VTK* file format)\
  `./bin/vtktoi3d`         (converts simple *legacy VTK* files into *i3d* CraFT image file format)\
  `./bin/i3stat`           (displays some infos about images in *i3d* or *VTK format* )\
  `./bin/general_i3d`      (creates a descriptor file for use of a *i3d* file in *3D scientific visualization program OpenDX*)

   If you want to compile just **craft** or **xhmv**:\
   `make craft`\
   `make xhmv`

* type `make clean`: to clean all what has been compiled

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTYyODAzNjc5LDEzMjI2Mjk3ODMsMjAyMT
gwMTM4NSwtMTU1MzQ4NzQyOSwtMjAzNzQwODIxNiwxMzY5OTM5
OTIxLC0xNzk0MTg2NTM5LDIwNjQzODM0OSwtMTE5NTI4MzQ3NS
wyMDA3Mjc1NTEzLDEzNTQ3MzIyMDksMTcwMDk0Mzg0OV19
-->
