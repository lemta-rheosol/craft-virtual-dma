#---------------------------------------------------------------------
SHELL = /bin/sh
#---------------------------------------------------------------------
# One checks if options.in file exists. If it doesn't make stops .
#
$(if $(wildcard options.in),,$(error "Craft compilation error: options.in is missing, bye bye."))

# options.in file exists, one includes it here:
include options.in
#
#---------------------------------------------------------------------
ifeq ($(strip $(CC)),)
	$(error echo "Unknown compiling system. Bye, bye.")
endif

all: craft xhmv pycraft i3d microstructures miscellaneous

i3d:
	test -d "bin" || mkdir bin
	test -d "lib" || mkdir lib
	cd $(I3DDIR); $(MAKE) -e all;

craft: i3d
	test -d "bin" || mkdir bin
	test -d "lib" || mkdir lib
	cd $(CRAFTDIR); $(MAKE) -e all;

xhmv: i3d
ifeq ($(XHMV),yes)
	test -d "bin" || mkdir bin
	test -d "lib" || mkdir lib
	cd $(XHMVDIR); $(MAKE) -e all;
endif

pycraft:
ifeq ($(PYCRAFT),yes)
	test -d "bin" || mkdir bin
	test -d "lib" || mkdir lib
	if test -d $(PYCRAFTDIR); then cd $(PYCRAFTDIR) && $(MAKE) all; fi;
#	if test -d $(PYCRAFTDIR); then cd $(PYCRAFTDIR); python setup.py build; fi;
endif

microstructures: i3d
ifeq ($(MICROSTRUCTURES),yes)
	test -d "bin" || mkdir bin
	if test -d $(MICRODIR); then cd $(MICRODIR) && $(MAKE) -e all; fi;
endif

miscellaneous: i3d
ifeq ($(MISCELLANEOUS),yes)
	test -d "bin" || mkdir bin
	if test -d $(MISCDIR); then cd $(MISCDIR); $(MAKE) -e all; fi
endif

dummy:
	@echo "Dummy command"


#---------------------------------------------------------------------
clean:
	cd $(I3DDIR); $(MAKE) -e clean;
	cd $(CRAFTDIR); $(MAKE) -e clean;
  ifeq ($(XHMV),yes)
	cd $(XHMVDIR); $(MAKE) -e clean;
  endif
  ifeq ($(PYCRAFT),yes)
	if test -d $(PYCRAFTDIR); then cd $(PYCRAFTDIR); $(MAKE) -e clean; fi;
  endif
ifeq ($(MICROSTRUCTURES),yes)
	if test -d $(MICRODIR); then cd $(MICRODIR); $(MAKE) -e clean; fi
endif
ifeq ($(MISCELLANEOUS),yes)
	if test -d $(MISCDIR); then cd $(MISCDIR); $(MAKE) -e clean; fi
endif

i3dclean:
	cd $(I3DDIR); $(MAKE) -e clean;


craftclean:
	cd $(CRAFTDIR); $(MAKE) -e clean;

xhmvclean:
  ifeq ($(XHMV),yes)
	cd $(XHMVDIR); $(MAKE) -e clean;
  endif

microstructuresclean:
ifeq ($(MICROSTRUCTURES),yes)
	cd $(MICRODIR); $(MAKE) -e clean;
endif

miscellaneousclean:
ifeq ($(MISCELLANEOUS),yes)
	cd $(MISCDIR); $(MAKE) -e clean;
endif
