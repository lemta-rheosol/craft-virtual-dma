# CraFT Virtual DMA

version 1.1.0 Harmonic\
_Wednesday February 24 2021_

## Authors
Alice Labé, Hervé Moulinec, Pierre Suquet _(original CraFT code)_ \
Boisse Julien, André Stéphane _(CraFT Virtual DMA / Harmonic version)_

## Description
**CraFT** ("**C**omposite **r**esponse **a**nd **F**ourier **T**ransforms" or "**Cr**ystals **a**nd **F**ourier **T**ransform")
is a program to compute the mechanical response of heteregeneous materials.

**CraFT virtual DMA** is a special version of CraFT dedicated to simulate DMA experiments 
on composites made of viscoelastic materials. Having recourse to the correspondence principle, 
it gives access to the effective conservative and dissipative moduli of any heterogenous material 
described through a voxel grid.

CraFT is a free software under Cecill-B licence. 
The text of the licence is available here:\
  https://cecill.info/licences/Licence_CeCILL-B_V1-en.html    (english version)\
  https://cecill.info/licences/Licence_CeCILL-B_V1-fr.html    (french version)

or in the files of the current directory:\
 [`./Licence_CeCILL-B_V1-en.txt`](Licence_CeCILL-B_V1-en.txt)     (english version),\
 [`./Licence_CeCILL-B_V1-fr.txt`](Licence_CeCILL-B_V1-fr.txt)     (french version).

It is based on an original idea by Pierre Suquet (1990) and it has been developed in
Moulinec, Suquet (1994). The algorithm has been described in Moulinec, Suquet (1998) and 
in Michel, Moulinec, Suquet (2001) where some details are given on the convergence of 
the iterative algorithm.\
In Michel, Moulinec, Suquet (2000) an alternative algorithm has been proposed for high 
contrast materials *[not yet implemented in CraFT]*.

CraFT development has been partly supported by ANR (Agence Nationale de la Recherche)\
grant ANR-08-BLAN-0138 for project: _"Micromechanical approach of nonlinear ELasto-VIScoplasticity in polycrystalline materials: theory, modeling, and experimental comparison"_\
CraFT virtual DMA has been developed thanks to a special funding from the EMPP (Energy Mechanics Process Products) research axis of Lorraine University.

CraFT in its original version (https://lma-software-craft.cnrs.fr) has been developed by:
* Alice Labé
* Hervé Moulinec
* Pierre Suquet

_In the french laboratory "Laboratoire de Mécanique et d'Acoustique",_ \
_CNRS (Centre National de la Recherche Scientifique) in Marseilles France._

CraFT Virtual DMA version has been developed by:
* Julien Boisse (julien.boisse@univ-lorraine.fr)
* Stéphane André (stephane.andre@univ-lorraine.fr)

_In the Laboratory of Energies Theoretical and Applied Mechanics,_ \
_CNRS (Centre National de la Recherche Scientifique) in Nancy France._

## How to install CraFT Virtual DMA ?

[`./INSTALL.md`](INSTALL.md) file details how to compile CraFT program.\
Please contact us for more details on the compilation procedure in the case of an installation on EXPLOR Mesocenter (http://explor.univ-lorraine.fr/). \
The authors gratefully acknowledge the EXPLOR Mesocenter for providing HPC resources.

## How to run CraFT Virtual DMA ?

A user guide for CraFT original version is available in [`./documentation/CraFT_UserGuide.pdf`](./documentation/CraFT_UserGuide.pdf).

## Short bibliography:

* P. SUQUET. A simplified method for the prediction of homogeneized elastici properties
of composites with a periodic structure. C. R. Acad. Sc. Paris, II, 311 (1990) 769\-774.

* H. MOULINEC and P. SUQUET. A fast numerical method for computing the linear
and nonlinear properties of composites. C. R. Acad. Sc. Paris, II, 318 (1994) 1417\-1423.

* H. MOULINEC and P. SUQUET. A numerical method for computing the overall
response of nonlinear composites with complex microstructure. Comp. Meth.
Appl. Mech. Engng., 157 (1998) 69\-94.

* J.C. MICHEL, H. MOULINEC, P. SUQUET. Effective properties of composite ma-
terials with periodic microstructure : a computational approach Comp. Meth.
Appl. Mech. Engng., 172 (1999) 109\-143.

* J.C. MICHEL, H. MOULINEC, P. SUQUET. A computational method based on
augmented lagrangians and fast Fourier transforms for composites with high
contrast, Computer Modeling in Engineering & Science, volume 1, n&#176;2 (2000) 79\-88.

* J.C. MICHEL, H. MOULINEC, P. SUQUET. A computational scheme for linear
and non-linear composites with arbitrary phase contrast, Int. J. Numer. Meth.
Engng , 52 (2001) 139\-160.

* S. ANDRE, J. BOISSE, C. NOUS. An FFT-solver used for virtual Dynamic Mechanical Analysis 
experiments: Application on a glassy/amorphous system and on a particulate composite, 
Journal of Theoretical, Computational and Applied Mechanics (JTCAM), 2021.

<!--stackedit_data:
eyJoaXN0b3J5IjpbNjAzNjQ3Mzc1XX0=
-->
