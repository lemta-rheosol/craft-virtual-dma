#include <stdlib.h>
#include <stdio.h>

#include <craft.h>

#include <materials.h>
#include <LE_materials.h>
#include <meca.h>
#include <craft_schemes.h>
//#include <craft_options.h>

#include <math.h>
#include <LE_Cmatrix.h>

#ifndef TINY
#define TINY 1.e-15
#endif

//void isotropie_(double C[6][6],double *lb, double *mu, double *d);
double isotropy(double C[6][6],double *lb, double *mu);

void  dsyev_(char *,char *,int *n,double *a, int *lda, double *w, double *wk, int *lwk, int *info);

/********************************************************************************/
/*        Function which automatically compute C0 parameters                    */
/*        as the (max + min)/2 of all materials                                 */
/********************************************************************************/
int compute_C0parameters(LE_param *C0, Material *material, Euler_Angles *orientation, int Nph, int scheme){
  
  double lb[Nph], mu[Nph];

  double C_ini[6][6];
  double C[6][6];


  int iph;
  int i,j;
  
  int isotropic;
  double lb_max, mu_max;
  double lb_min, mu_min;

  double k, k_min, k_max;
  double d;

  /*--------------------------------------------------------------------------------*/
  /* First step: look if the elastic part of every behavior is isotropic */
  isotropic=1;

  for(iph=0; iph<Nph; iph++) {

    /* load the linear elastic part of the behavior */    
    material[iph].load_Cmatrix(material[iph].parameters, C_ini);
    /* rotate the stiffness matrix in the local system */
    //    rotate_Cmatrix(C,C_ini,orientation[iph]);
    for(i=0;i<6;i++) {
      for(j=0;j<6;j++) {
        C[i][j]=C_ini[i][j];
      }
    }


    /* isotropic projection of C */
    //    isotropie_(C, &lb[iph], &mu[iph],&d);
    d = isotropy( C, &lb[iph], &mu[iph]);


    /* test if C is isotropic */
    if (verbose) printf("deviation from isotropy=%g\n",d);
    if(d>=TINY) {
      isotropic=0;
      break;
    }
  }

  if(verbose){
    if(isotropic==1) {
      printf("isotropic case!\n");
    }
    else{
      printf("anisotropic case!\n");
    }
  }

  /*--------------------------------------------------------------------------------*/
  /* second step, depending on the case that all the phases have an elastic part being  isotropic
     or not, compute the Lame coefficient of the reference medium C0:
  */
  /*--------------------------------------------------------------------------------*/
  /* isotropic case: */
  /*--------------------------------------------------------------------------------*/
  if(isotropic==1){
    

    lb_max = lb[0];
    mu_max = mu[0];

    lb_min = lb[0];
    mu_min = mu[0];

    for(iph=1;iph<Nph;iph++) {
      lb_max = (lb[iph] > lb_max ? lb[iph] : lb_max);
      mu_max = (mu[iph] > mu_max ? mu[iph] : mu_max);

      lb_min = (lb[iph] < lb_min ? lb[iph] : lb_min);
      mu_min = (mu[iph] < mu_min ? mu[iph] : mu_min);
    }




#ifdef BLABLA
    printf("------------------------------\n");
    printf("\n");
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max+2.*mu_max, lb_max, lb_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max, lb_max+2.*mu_max, lb_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max, lb_max, lb_max+2.*mu_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 2.*mu_max, 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 2.*mu_max, 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 0., 2.*mu_max);
    printf("\n");
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min+2.*mu_min, lb_min, lb_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min, lb_min+2.*mu_min, lb_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min, lb_min, lb_min+2.*mu_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 2.*mu_min, 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 2.*mu_min, 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 0., 2.*mu_min);
    printf("------------------------------\n");
#endif
  

  }
  /*--------------------------------------------------------------------------------*/
  /* anisotropic case  */
  /*--------------------------------------------------------------------------------*/
  else{
    char mot1='N';
    char mot2='L';
    int n=6;
    int lwk=3*n-1;
    double wk[lwk];
    double w[n];
    int info;
  
    mu_max=0.;
    mu_min=HUGE_VAL;
    for(iph=0; iph<Nph; iph++) {

      /* load the linear elastic part of the behavior */    
      material[iph].load_Cmatrix(material[iph].parameters, C);
      
      /* look for the eigenvalues of matrix C */
      dsyev_(&mot1,&mot2,&n,&C[0][0],&n, w, wk, &lwk, &info);
      
      mu_max=( 0.5*w[n-1] > mu_max? 0.5*w[n-1] : mu_max );
      mu_min=( 0.5*w[0]   < mu_min? 0.5*w[0]   : mu_min );

      lb_max=lb_min=0.;


    }


    


  }


  /* step 3: compute the Lame coefficients depending on the scheme being used */

#ifdef BLABLA
  printf("lambda max = %g\n",lb_max);
  printf("lambda min = %g\n",lb_min);
  printf("mu max = %g\n",mu_max);
  printf("mu min = %g\n",mu_min);
  printf("scheme=%d\n",scheme);
#endif
  
  switch(scheme){
  case CRAFT_BASIC_SCHEME:
    C0->sub.ile->lb = (lb_max+lb_min)/2.;
    C0->sub.ile->mu = (mu_max+mu_min)/2.;
    break;
  case CRAFT_MILTON_EYRE_SCHEME:
  case CRAFT_LAGRANGIAN_SCHEME:
  case CRAFT_ALPHA_BETA_SCHEME:
  default:
    k_max = lb_max + 2./3.*mu_max;
    k_min = lb_min + 2./3.*mu_min;
    k = sqrt(k_max*k_min);
    C0->sub.ile->mu = sqrt(mu_max*mu_min);
    C0->sub.ile->lb = k-2./3.*C0->sub.ile->mu;
    
    break;
  }
    

  
  /*--------------------------------------------------------------------------------*/
  elastic(&(C0->sub.ile->E), &(C0->sub.ile->nu),
          &(C0->sub.ile->lb),&(C0->sub.ile->mu),
          &(C0->sub.ile->k));
    
  /*--------------------------------------------------------------------------------*/
  return(0);
}


/********************************************************************************/
/*        Function which automatically update C0 parameters                     */
/*        as the (max + min)/2 of all materials                                 */
/********************************************************************************/
int update_C0parameters(LE_param *C0, Material *material, Euler_Angles *orientation, int Nph, int scheme, Load_Step load_step)
{
  
  double lb[Nph], mu[Nph];

  double C_ini[6][6];
  double C[6][6];


  int iph;
  int i,j;
  
  int isotropic;
  double lb_max, mu_max;
  double lb_min, mu_min;

  double k, k_min, k_max;
  double d;

  /*--------------------------------------------------------------------------------*/
  /* First step: look if the elastic part of every behavior is isotropic */
  isotropic=1;

  for(iph=0; iph<Nph; iph++) {

    /* update the linear elastic part of the behavior */    
    material[iph].update_Cmatrix(material[iph].parameters, C_ini, load_step.time);
    /* rotate the stiffness matrix in the local system */
    //    rotate_Cmatrix(C,C_ini,orientation[iph]);
    for(i=0;i<6;i++) {
      for(j=0;j<6;j++) {
        C[i][j]=C_ini[i][j];
      }
    }


    /* isotropic projection of C */
    //    isotropie_(C, &lb[iph], &mu[iph],&d);
    d = isotropy( C, &lb[iph], &mu[iph]);

//    printf("material %d : lb = %f ; mu = %f , isotropy = %f\n", iph, lb[iph], mu[iph], d);

    /* test if C is isotropic */
    if (verbose) printf("deviation from isotropy=%g\n",d);
    if(d>=TINY) {
      isotropic=0;
      break;
    }
  }

  if(verbose){
    if(isotropic==1) {
      printf("isotropic case!\n");
    }
    else{
      printf("anisotropic case!\n");
    }
  }

  /*--------------------------------------------------------------------------------*/
  /* second step, depending on the case that all the phases have an elastic part being  isotropic
     or not, compute the Lame coefficient of the reference medium C0:
  */
  /*--------------------------------------------------------------------------------*/
  /* isotropic case: */
  /*--------------------------------------------------------------------------------*/
  if(isotropic==1){
    

    lb_max = lb[0];
    mu_max = mu[0];

    lb_min = lb[0];
    mu_min = mu[0];

    for(iph=1;iph<Nph;iph++) {
      lb_max = (lb[iph] > lb_max ? lb[iph] : lb_max);
      mu_max = (mu[iph] > mu_max ? mu[iph] : mu_max);

      lb_min = (lb[iph] < lb_min ? lb[iph] : lb_min);
      mu_min = (mu[iph] < mu_min ? mu[iph] : mu_min);
    }




#ifdef BLABLA
    printf("------------------------------\n");
    printf("\n");
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max+2.*mu_max, lb_max, lb_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max, lb_max+2.*mu_max, lb_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_max, lb_max, lb_max+2.*mu_max, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 2.*mu_max, 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 2.*mu_max, 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 0., 2.*mu_max);
    printf("\n");
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min+2.*mu_min, lb_min, lb_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min, lb_min+2.*mu_min, lb_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",lb_min, lb_min, lb_min+2.*mu_min, 0., 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 2.*mu_min, 0., 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 2.*mu_min, 0.);
    printf("%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n",0., 0., 0. , 0., 0., 2.*mu_min);
    printf("------------------------------\n");
#endif
  

  }
  /*--------------------------------------------------------------------------------*/
  /* anisotropic case  */
  /*--------------------------------------------------------------------------------*/
  else{
    char mot1='N';
    char mot2='L';
    int n=6;
    int lwk=3*n-1;
    double wk[lwk];
    double w[n];
    int info;
  
    mu_max=0.;
    mu_min=HUGE_VAL;
    for(iph=0; iph<Nph; iph++) {

      /* load the linear elastic part of the behavior */    
      material[iph].update_Cmatrix(material[iph].parameters, C, load_step.time);
      
      /* look for the eigenvalues of matrix C */
      dsyev_(&mot1,&mot2,&n,&C[0][0],&n, w, wk, &lwk, &info);
      
      mu_max=( 0.5*w[n-1] > mu_max? 0.5*w[n-1] : mu_max );
      mu_min=( 0.5*w[0]   < mu_min? 0.5*w[0]   : mu_min );

      lb_max=lb_min=0.;


    }


    


  }


  /* step 3: compute the Lame coefficients depending on the scheme being used */

#ifdef BLABLA
  printf("lambda max = %g\n",lb_max);
  printf("lambda min = %g\n",lb_min);
  printf("mu max = %g\n",mu_max);
  printf("mu min = %g\n",mu_min);
  printf("scheme=%d\n",scheme);
#endif
  
  switch(scheme){
  case CRAFT_BASIC_SCHEME:
    C0->sub.ile->lb = (lb_max+lb_min)/2.;
    C0->sub.ile->mu = (mu_max+mu_min)/2.;
    printf(" lb = %g ; mu = %g \n", C0->sub.ile->lb, C0->sub.ile->mu );
    break;
  case CRAFT_MILTON_EYRE_SCHEME:
  case CRAFT_LAGRANGIAN_SCHEME:
  case CRAFT_ALPHA_BETA_SCHEME:
  default:
    k_max = lb_max + 2./3.*mu_max;
    k_min = lb_min + 2./3.*mu_min;
    k = sqrt(k_max*k_min);
    C0->sub.ile->mu = sqrt(mu_max*mu_min);
    C0->sub.ile->lb = k-2./3.*C0->sub.ile->mu;
    
    break;
  }
    

  
  /*--------------------------------------------------------------------------------*/
  elastic(&(C0->sub.ile->E), &(C0->sub.ile->nu),
          &(C0->sub.ile->lb),&(C0->sub.ile->mu),
          &(C0->sub.ile->k));
    
  /*--------------------------------------------------------------------------------*/
  return(0);
}

/********************************************************************************/
/*             Function which read C0 matrix                                    */
/********************************************************************************/
int read_C0matrix(FILE *f,Material C0){
  ILE_param param0;
  double C[6][6];
  int i,j;
  double d;
  
  for(i=0;i<6;i++){
    for(j=0;j<6;j++){
      fscanf(f,"%lf ",&(C[i][j]));
    }
  }
  
  // isotropie_(C,&(param0.lb),&(param0.mu),&d);
  d = isotropy( C, &param0.lb, &param0.mu);
    
  elastic(&(param0.E),&(param0.nu),&(param0.lb),&(param0.mu),&(param0.k));
  
  C0.copy_parameters( C0.parameters, &param0);
  
  return(0);
}



/********************************************************************************/
/*        Function which asks how we want to enter C0                            */
/*        
          auto                :    C0 is "automatically" computed
          param   lb0 mu0     :    C0 is linear elastic with Lame coefficients entered as lb0 mu0
          symmetric matrix  (21 coef of traingular matrix Cij) 
	                      : C0 is entered as a half - 6x6 matrix 
*/
/********************************************************************************/
int enter_C0parameters(char *C0_line_command, LE_param *C0, Material *material, Euler_Angles *orientation, int Nph, int scheme){

  int status=0;
  
  C0->isotropy = ISOTROPIC;
  C0->sub.ile = malloc(sizeof(ILE_param));
  
  
  switch (C0_line_command[0]){
  case 'u' : // updated case "u": ADDED for harmonic implementation 2017-10-12
  case 'a' :
    status = compute_C0parameters(C0, material, orientation, Nph, scheme);
    break;
    
  case 'p':
    /* 
       C0.read_parameters(stdin, (void *)C0.parameters);
    */
    {
      sscanf(C0_line_command,"param %lf, %lf",
             &(C0->sub.ile->lb),
             &(C0->sub.ile->mu)
             );
      
      elastic( &(C0->sub.ile->E),  &(C0->sub.ile->nu),
               &(C0->sub.ile->lb), &(C0->sub.ile->mu) , &(C0->sub.ile->k) );
      
      
    }
    break;
    /* 
       une bizarrerie: comment C0 pourrait il etre rentre comme matrice?
    */
    /*
      case 'm':
      
      read_C0matrix(stdin, C0);
      break;
    */
  default:
    status=-1;
    printf("\n error in definition of C0\n");
    break;
    
    
  }
  return(status);
}
