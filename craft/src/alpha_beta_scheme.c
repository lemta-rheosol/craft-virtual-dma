#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <craft.h>
#include <craftimage.h>
#include <materials.h>
#include <LE_materials.h>
#include <loading.h>

#include <variables.h>
#include <io_variables.h>
#include <utils.h>
#include <meca.h>

#include <euler.h>
#include <moments.h>

#include <craft_compatibility_equilibrium.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include <LE_Cmatrix.h>

#include <craft_timer.h>

#include <craft_schemes.h>
#include <craft_lsp.h>

#ifndef TINY
#define TINY 1.e-15
#endif

#include <gdrmacro.h>

#include <complex.h>
/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   6th march 2012


*/
/*********************************************************************************/
int alpha_beta_scheme(

                 int Nph,        /* total number of phases in the microstructure */
                 int *nppp,      /* number of points per phase */
                 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
                                    the Lippmann-Schwinger algorithm) of the point number ipt
                                    in the phase number iph                                     */
                 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
                                               orientation (or the orientation of its anistropy)
                                               of phase number iph.
                                               If iph is an isotropic material, orientation is useless     */
                 
                 Material *material,
                 /* material[iph] gives every information about the material
                    in phase iph */
                 
                 Variables *variables,
                 /* variables[iph] : pointer to the structure describing
                    the variables in phase iph                            */
                 
                 int load_type, /* loading conditions                                          */
                 Load_Step load_step,   /* load step */
                 double E[6], double S[6], /* macroscopic strain and stress */
                 
                 LE_param *C0,    /* "reference material"                                        */
                 
                 double (**phase_epsilon)[6],
                 /* phase_epsilon[iph][ipt] gives the strain tensor of point
                    number ipt of phase number iph                              */
                 
                 
                 double dt,  /* time step between current step and previous step */
                 double (**phase_previous_epsilon)[6],
                 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
                    of point number ipt of phase number iph at previous loading
                    step.                                                        */
                 
                 double (**phase_sigma)[6],
                 /* phase_sigma[iph][ipt] gives the stress tensor of point
                    number ipt of phase number iph                              */

                 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                       relation                                                 */
                 
                 double *compatibility_error,  /* error on comptatibility of the strain */
                 double *divs_error,  /* error on divergence of the stress                       */
                 double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                        stress (depending on loading conditions)                */
                 double compatibility_precision, /* precision required on strain compatibility */
                 double divs_precision,   /* precision required by user on divergence of the
                                             stress                                                      */
                 double macro_precision,  /* precision required by user on macroscopic stress
                                             or direction of macroscopic
                                             stress (depending on loading conditions)                    */
                 
                 int maxiter, /* max number of iterations allowed */
                 int *niter,

                 double *alpha,
                 double *beta

                 ) {

  /*-----------------------------------------------------------------------------*/
  int convergence_reached;
  int status;

  int i, j;
  int iph;
  /*-----------------------------------------------------------------------------*/
  /* this scheme forces the strain field to be compatible */
  *compatibility_error = 0.;

  *niter = 0;

  do {
    (*niter)++;
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* error tests:                                                            */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* equilibrium test. 
       I.e.:
       div(sigma) <? precision
    */
    start_timer(CRAFT_TIMER_EQUILIBRIUM);        
    /* phase to image */
    status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
    if(status!=0) exit(1);

    /* FFT */
    status = craft_fft_image( image, -1);
    if(status!=0) exit(1);

    /* stores macroscopic stress */
    for(j=0;j<6;j++) {
      S[j] = creal(image->pxl.ft2[0][j]);
    }
    /* equilibirum test */
    status = equilibrium( image, divs_error);

    /* set image to 2d order tensor type */
    image->type[0] = CRAFT_IMAGE_TENSOR2;
    image->type[1] = 6;
    image->type[2] = 0;
    image->nd[0] = image->nd[0]*2;
    for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*divs_error)==0) ) {
      return -1;
    }

    stop_timer(CRAFT_TIMER_EQUILIBRIUM);        

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* compatibility test
    */
    start_timer(CRAFT_TIMER_COMPATIBILITY);        

    status = phase_to_image( Nph, nppp, index, (void *)phase_epsilon, image);
    if(status!=0) exit(1);

    //    double energy;
    //    status=image_energy( image, &energy);
    //    printf("energy avant fft = %g\n",energy);


    status = craft_fft_image( image, -1);
    if(status!=0) exit(1);

    /* stores macroscopic strain */
    for(j=0;j<6;j++) {
      E[j] = creal(image->pxl.ft2[0][j]);
    }
    //    if (verbose){
    //      fprintf(stdout, "     <e>= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
    //              E[0], E[1], E[2], E[5], E[4], E[3] );
    //
    //    }

    //    status=image_energy( image, &energy);
    //    printf("energy apres fft = %g\n",energy);

     status = compatibility( image, compatibility_error);
    //    status = compatibility_MB2( image, compatibility_error);
    //    status = compatibility2( image, C0, compatibility_error);

    image->type[0] = CRAFT_IMAGE_TENSOR2;
    image->type[1] = 6;
    image->type[2] = 0;
    image->nd[0] = image->nd[0]*2;
    for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*compatibility_error)==0) ) {
      return -3;
    }
    
    stop_timer(CRAFT_TIMER_COMPATIBILITY);        

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* calculates error on prescribed macroscopic conditions                   */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    compute_loading_error(
                          load_type,
                          //                          *alpha, *beta,
                          C0->sub.ile->k, C0->sub.ile->mu, 
                          load_step.load,
                          load_step.direction,
                          E, S,
                          macro_error
                          );
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*macro_error)==0) ) {
      return -2;
    }
    
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* summary the 3 tests:                                                    */

    convergence_reached =
      (
       (*divs_error < divs_precision)   &&
       (*macro_error < macro_precision) &&
       (*compatibility_error < compatibility_precision)
       )
      || ( *niter >= maxiter);
    

    /* If convergence has been reached, it should not be necessary to go on
       within the current iterative process, and one could simply stop
       iterative process here.
    */
    /*
    printf("%d %d %d %d\n",
           convergence_reached,
           (*divs_error < divs_precision),
           (*macro_error < macro_precision) ,
           (*compatibility_error < compatibility_precision)
           );
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e compatibility error=%15.8g\n",
              load_step.time, *niter,*divs_error, *macro_error, *compatibility_error );
      fprintf(stdout, "     E(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              E[0], E[1], E[2], E[5], E[4], E[3] );
      fprintf(stdout, "     S(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (convergence_reached) break;

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* calculation of :
       alpha*sigma - beta* C0: epsilon          result stored in epsilon       
       sigma + (1-beta)*C0:epsilon              result stored in sigma
    */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    start_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    for(iph=0; iph<Nph; iph++) {
      status = linear_elastic( C0, &orientation[iph], 
                               nppp[iph],
                               phase_epsilon[iph], phase_epsilon[iph]);
    }

    double xe, xs;

    for(iph=0; iph<Nph; iph++) {
#pragma omp parallel for                        \
  default(none) \
  shared(iph)                                   \
  shared(phase_epsilon, phase_sigma, nppp)      \
  private(i, j) \
  private(xe,xs) \
  shared(alpha, beta)
      for(i=0; i<nppp[iph]; i++) {
        for(j=0;j<6;j++) {
          xe = phase_epsilon[iph][i][j] ;
          xs = phase_sigma[iph][i][j] ;
          phase_epsilon[iph][i][j] = *alpha*xs - *beta*xe ;
          phase_sigma[iph][i][j] = xs + (1.-*beta)*xe;
        }
      }
    }    
    stop_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* polarization: phase organization -> image organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    status = phase_to_image( Nph, nppp, index, (void *)phase_epsilon, image);
    stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);

    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* FFT applied to stress field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTD);
    status = craft_fft_image( image, -1);
    if(status!=0) exit(1);
    stop_timer(CRAFT_TIMER_FFTD);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Lippmann-Schwinger equation (calculates -Gamma0:tau in Fourier space) */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_GAMMA0);
    status=lsp( image,  C0 , (double *)0 );
    stop_timer(CRAFT_TIMER_GAMMA0);


    if(status!=0) {
      fprintf(stderr,"CraFT error in lsp\n");
      fprintf(stderr,"exit!\n");
      exit(1);
    }


#ifdef DEBUG
    printf("%g\n",*divs_error);
#endif

    /* one put into null frequency component what has to be put, depending on the loading conditions  */
    double X[6];
    
    compute_macro(
                  load_type,
                  *alpha, *beta,
                  C0->sub.ile->k, C0->sub.ile->mu, 
                  load_step.load,
                  load_step.direction,
                  E, S,
                  X
                  );
    for(j=0;j<6;j++) image->pxl.ft2[0][j]=X[j];

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* inverse FFT applied to "-Gamma0:tau" field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTI);
    status = craft_fft_image( image, +1);
    stop_timer(CRAFT_TIMER_FFTI);
    if(status!=0) exit(1);


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* image organization -> phase organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_IMAGE_TO_PHASE);
    status = image_to_phase( Nph, nppp, index, (void *)phase_epsilon, image);
    stop_timer(CRAFT_TIMER_IMAGE_TO_PHASE);

    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* calculation of sigma + C0: epsilon 
       result stored in sigma */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* C'est ici qu'il faudrait ajouter qq chose pour le epsilon thermique. Un truc du genre:
       for(i=0; i<nppp[iph]; i++) {
         for(j=0;j<6;j++) {
           phase_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
          phase_previous_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
         }
       }
    */
    start_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    for(iph=0; iph<Nph; iph++) {
      status = linear_elastic( C0, &orientation[iph], 
                               nppp[iph],
                               phase_epsilon[iph], phase_epsilon[iph]);
    }

    for(iph=0; iph<Nph; iph++) {
#pragma omp parallel for                        \
  shared(iph)                                   \
  shared(phase_epsilon, phase_sigma, nppp)      \
  private(i, j)
      for(i=0; i<nppp[iph]; i++) {
        for(j=0;j<6;j++) {
          phase_sigma[iph][i][j] += phase_epsilon[iph][i][j];
        }
      }
    }    

    stop_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* find epsilon which verifies:
             sigma(epsilon) + C0:epsilon = tau

       (tau is presently stored phase_sigma)
       

       results stored in phase_sigma and phase_epsilon */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_SOLVE);
    for(iph=0; iph<Nph; iph++) {
      status=material[iph].solve_spc0e(
                                material[iph].parameters,
                                orientation[iph],
                                &variables[iph],
                                dt,
                                C0,
                                phase_sigma[iph]
                                );
      if(status!=0) {
        fprintf(stderr,"alpha beta scheme: pb in function material.solve_spc0e.\n");
        if (status==-1){
          fprintf(stderr,"  function material.solve_spc0e has not been yet implemented for\n");
          fprintf(stderr,"  phase number %d.\n",iph);
        }
        fprintf(stderr," CraFT exit.\n");
        exit(1);
      }

    }
    stop_timer(CRAFT_TIMER_SOLVE);
    /* C'est ici qu'il faudrait ajouter qq chose pour le epsilon thermique. Un truc du genre:
       for(i=0; i<nppp[iph]; i++) {
         for(j=0;j<6;j++) {
           phase_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
           phase_previous_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
         }
       }
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e compatibility error=%15.8g\n",
              load_step.time, *niter,*divs_error, *macro_error, *compatibility_error );
      fprintf(stdout, "     E= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              E[0], E[1], E[2], E[5], E[4], E[3] );
      fprintf(stdout, "     S= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  } while( !convergence_reached  );

  return 0;
}

/* modified for harmonic implementation 2018/07/09  J. Boisse */
int alpha_beta_scheme_harmonic(

                 int Nph,        /* total number of phases in the microstructure */
                 int *nppp,      /* number of points per phase */
                 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
                                    the Lippmann-Schwinger algorithm) of the point number ipt
                                    in the phase number iph                                     */
                 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
                                               orientation (or the orientation of its anistropy)
                                               of phase number iph.
                                               If iph is an isotropic material, orientation is useless     */
                 
                 Material *material,
                 /* material[iph] gives every information about the material
                    in phase iph */
                 
                 Variables *variables,
                 /* variables[iph] : pointer to the structure describing
                    the variables in phase iph                            */
                 
                 int load_type, /* loading conditions                                          */
                 Load_Step load_step,   /* load step */
                 double complex E[6],
                 double complex S[6], /* macroscopic strain and stress */
                 
                 LE_param *C0,    /* "reference material"                                        */
                 
                 double complex (**phase_epsilon)[6],
                 /* phase_epsilon[iph][ipt] gives the strain tensor of point
                    number ipt of phase number iph                              */
                 
                 
                 double dt,  /* time step between current step and previous step */
                 double complex (**phase_previous_epsilon)[6],
                 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
                    of point number ipt of phase number iph at previous loading
                    step.                                                        */
                 
                 double complex (**phase_sigma)[6],
                 /* phase_sigma[iph][ipt] gives the stress tensor of point
                    number ipt of phase number iph                              */

                 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                       relation                                                 */
                 
                 double *compatibility_error,  /* error on comptatibility of the strain */
                 double *divs_error,  /* error on divergence of the stress                       */
                 double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                        stress (depending on loading conditions)                */
                 double compatibility_precision, /* precision required on strain compatibility */
                 double divs_precision,   /* precision required by user on divergence of the
                                             stress                                                      */
                 double macro_precision,  /* precision required by user on macroscopic stress
                                             or direction of macroscopic
                                             stress (depending on loading conditions)                    */
                 
                 int maxiter, /* max number of iterations allowed */
                 int *niter,

                 double *alpha,
                 double *beta

                 ) {

  /*-----------------------------------------------------------------------------*/
  int convergence_reached;
  int status;

  int i, j;
  int iph;
  /*-----------------------------------------------------------------------------*/
  /* this scheme forces the strain field to be compatible */
  *compatibility_error = 0.;

  *niter = 0;

  do {
    (*niter)++;
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* error tests:                                                            */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* equilibrium test. 
       I.e.:
       div(sigma) <? precision
    */
    start_timer(CRAFT_TIMER_EQUILIBRIUM);        
    /* phase to image */
    status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
    if(status!=0) exit(1);

    /* FFT */
    /* modified for harmonic implementation 2018/07/09  J. Boisse */
    status = craft_fft_image_harmonic( image, -1);
    if(status!=0) exit(1);

    /* stores macroscopic stress */
    for(j=0;j<6;j++) {
      //S[j] = creal(image->pxl.ft2[0][j]);
      /* modified for harmonic implementation 2018/07/09  J. Boisse */
      S[j] = image->pxl.ft2[0][j];
    }
    /* equilibirum test */
    /* modified for harmonic implementation 2018/07/09  J. Boisse */
    status = equilibrium_divs_sum_harmonic( image, divs_error);

    /* set image to 2d order tensor type */
    image->type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
    image->type[1] = 6;
    image->type[2] = 0;
    image->nd[0] = image->nd[0];/* modified for harmonic implementation 2018/07/09  J. Boisse */
    for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*divs_error)==0) ) {
      return -1;
    }

    stop_timer(CRAFT_TIMER_EQUILIBRIUM);        

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* compatibility test
    */
    start_timer(CRAFT_TIMER_COMPATIBILITY);        

    status = phase_to_image( Nph, nppp, index, (void *)phase_epsilon, image);
    if(status!=0) exit(1);

    //    double energy;
    //    status=image_energy( image, &energy);
    //    printf("energy avant fft = %g\n",energy);

    /* modified for harmonic implementation 2018/07/09  J. Boisse */
    status = craft_fft_image_harmonic( image, -1);
    if(status!=0) exit(1);

    /* stores macroscopic strain */
    for(j=0;j<6;j++) {
      /* modified for harmonic implementation 2018/07/09  J. Boisse */
      E[j] = image->pxl.ft2[0][j];
    }
    //    if (verbose){
    //      fprintf(stdout, "     <e>= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
    //              E[0], E[1], E[2], E[5], E[4], E[3] );
    //
    //    }

    //    status=image_energy( image, &energy);
    //    printf("energy apres fft = %g\n",energy);
     
     /* modified for harmonic implementation 2018/07/09  J. Boisse */
     status = compatibility_harmonic( image, compatibility_error);
    //    status = compatibility_MB2( image, compatibility_error);
    //    status = compatibility2( image, C0, compatibility_error);

    image->type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
    image->type[1] = 6;
    image->type[2] = 0;
    image->nd[0] = image->nd[0]; /* modified for harmonic implementation 2018/07/09  J. Boisse */
    for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*compatibility_error)==0) ) {
      return -3;
    }
    
    stop_timer(CRAFT_TIMER_COMPATIBILITY);        

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* calculates error on prescribed macroscopic conditions                   */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* modified for harmonic implementation 2018/07/09  J. Boisse */
    compute_loading_error_harmonic(
                          load_type,
                          //                          *alpha, *beta,
                          C0->sub.ile->k, C0->sub.ile->mu, 
                          load_step.load,
                          load_step.direction,
                          E, S,
                          macro_error
                          );
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*macro_error)==0) ) {
      return -2;
    }
    
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* summary the 3 tests:                                                    */

    convergence_reached =
      (
       (*divs_error < divs_precision)   &&
       (*macro_error < macro_precision) &&
       (*compatibility_error < compatibility_precision)
       )
      || ( *niter >= maxiter);

    // test à effacer
    /*
    convergence_reached =
      (
       (*divs_error < divs_precision)   &&
       (*compatibility_error < compatibility_precision)
       )
      || ( *niter >= maxiter);
    */

    /* If convergence has been reached, it should not be necessary to go on
       within the current iterative process, and one could simply stop
       iterative process here.
    */
    /*
    printf("%d %d %d %d\n",
           convergence_reached,
           (*divs_error < divs_precision),
           (*macro_error < macro_precision) ,
           (*compatibility_error < compatibility_precision)
           );
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e compatibility error=%15.8g\n",
              load_step.time, *niter,*divs_error, *macro_error, *compatibility_error );
      fprintf(stdout, "     E(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              E[0], E[1], E[2], E[5], E[4], E[3] );
      fprintf(stdout, "     S(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (convergence_reached) break;

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* calculation of :
       alpha*sigma - beta* C0: epsilon          result stored in epsilon       
       sigma + (1-beta)*C0:epsilon              result stored in sigma
    */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    start_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    for(iph=0; iph<Nph; iph++) {
      status = linear_elastic_harmonic( C0, &orientation[iph], 
                               nppp[iph],
                               phase_epsilon[iph], phase_epsilon[iph]);
    }

    double complex xe, xs;

    for(iph=0; iph<Nph; iph++) {
#pragma omp parallel for                        \
  default(none) \
  shared(iph)                                   \
  shared(phase_epsilon, phase_sigma, nppp)      \
  private(i, j) \
  private(xe,xs) \
  shared(alpha, beta)
      for(i=0; i<nppp[iph]; i++) {
        for(j=0;j<6;j++) {
          xe = phase_epsilon[iph][i][j] ;
          xs = phase_sigma[iph][i][j] ;
          phase_epsilon[iph][i][j] = *alpha*xs - *beta*xe ;
          phase_sigma[iph][i][j] = xs + (1.-*beta)*xe;
        }
      }
    }    
    stop_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* polarization: phase organization -> image organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    status = phase_to_image( Nph, nppp, index, (void *)phase_epsilon, image);
    stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);

    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* FFT applied to stress field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTD);
    status = craft_fft_image_harmonic( image, -1);
    if(status!=0) exit(1);
    stop_timer(CRAFT_TIMER_FFTD);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Lippmann-Schwinger equation (calculates -Gamma0:tau in Fourier space) */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_GAMMA0);
    status=lsp_harmonic( image,  C0 , (double *)0 );
    stop_timer(CRAFT_TIMER_GAMMA0);


    if(status!=0) {
      fprintf(stderr,"CraFT error in lsp\n");
      fprintf(stderr,"exit!\n");
      exit(1);
    }


#ifdef DEBUG
    printf("%g\n",*divs_error);
#endif

    /* one put into null frequency component what has to be put, depending on the loading conditions  */
    double complex X[6];
    
    compute_macro_harmonic(
                  load_type,
                  *alpha, *beta,
                  C0->sub.ile->k, C0->sub.ile->mu, 
                  load_step.load,
                  load_step.direction,
                  E, S,
                  X
                  );
    for(j=0;j<6;j++) image->pxl.ft2[0][j]=X[j];

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* inverse FFT applied to "-Gamma0:tau" field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTI);
    status = craft_fft_image_harmonic( image, +1);
    stop_timer(CRAFT_TIMER_FFTI);
    if(status!=0) exit(1);


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* image organization -> phase organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_IMAGE_TO_PHASE);
    status = image_to_phase( Nph, nppp, index, (void *)phase_epsilon, image);
    stop_timer(CRAFT_TIMER_IMAGE_TO_PHASE);

    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* calculation of sigma + C0: epsilon 
       result stored in sigma */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* C'est ici qu'il faudrait ajouter qq chose pour le epsilon thermique. Un truc du genre:
       for(i=0; i<nppp[iph]; i++) {
         for(j=0;j<6;j++) {
           phase_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
          phase_previous_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
         }
       }
    */
    start_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    for(iph=0; iph<Nph; iph++) {
      status = linear_elastic_harmonic( C0, &orientation[iph], 
                               nppp[iph],
                               phase_epsilon[iph], phase_epsilon[iph]);
    }

    for(iph=0; iph<Nph; iph++) {
#pragma omp parallel for                        \
  shared(iph)                                   \
  shared(phase_epsilon, phase_sigma, nppp)      \
  private(i, j)
      for(i=0; i<nppp[iph]; i++) {
        for(j=0;j<6;j++) {
          phase_sigma[iph][i][j] += phase_epsilon[iph][i][j];
        }
      }
    }    

    stop_timer(CRAFT_TIMER_ALPHA_BETA_MISC);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* find epsilon which verifies:
             sigma(epsilon) + C0:epsilon = tau

       (tau is presently stored phase_sigma)
       

       results stored in phase_sigma and phase_epsilon */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_SOLVE);
    for(iph=0; iph<Nph; iph++) {
      status=material[iph].solve_spc0e(
                                material[iph].parameters,
                                orientation[iph],
                                &variables[iph],
                                load_step.time,
                                C0,
                                phase_sigma[iph]
                                );
      if(status!=0) {
        fprintf(stderr,"alpha beta scheme: pb in function material.solve_spc0e.\n");
        if (status==-1){
          fprintf(stderr,"  function material.solve_spc0e has not been yet implemented for\n");
          fprintf(stderr,"  phase number %d.\n",iph);
        }
        fprintf(stderr," CraFT exit.\n");
        exit(1);
      }

    }
    stop_timer(CRAFT_TIMER_SOLVE);
    /* C'est ici qu'il faudrait ajouter qq chose pour le epsilon thermique. Un truc du genre:
       for(i=0; i<nppp[iph]; i++) {
         for(j=0;j<6;j++) {
           phase_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
           phase_previous_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
         }
       }
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e compatibility error=%15.8g\n",
              load_step.time, *niter,*divs_error, *macro_error, *compatibility_error );
      fprintf(stdout, "     E= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              E[0], E[1], E[2], E[5], E[4], E[3] );
      fprintf(stdout, "     S= %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
              S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  } while( !convergence_reached  );

  return 0;
}




