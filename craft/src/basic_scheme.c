#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <craft.h>
#include <craftimage.h>
#include <materials.h>
#include <LE_materials.h>
#include <loading.h>

#include <variables.h>
#include <io_variables.h>
#include <utils.h>
#include <meca.h>

#include <euler.h>
#include <moments.h>

#include <craft_compatibility_equilibrium.h>
#include <craft_lsp.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include <craft_timer.h>

#include <craft_schemes.h>

#include <gdrmacro.h>

#include <complex.h>
/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   9th januaray 2012

   "Basic scheme" of iterative resolution of a probleme at a givien loading step.
*/
/*********************************************************************************/
int basic_scheme(

		 int Nph,        /* total number of phases in the microstructure */
		 int *nppp,      /* number of points per phase */
		 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
				    the Lippmann-Schwinger algorithm) of the point number ipt
				    in the phase number iph                                     */
		 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
					       orientation (or the orientation of its anistropy)
					       of phase number iph.
					       If iph is an isotropic material, orientation is useless     */
		 
		 Material *material,
		 /* material[iph] gives every information about the material
		    in phase iph */
		 
		 Variables *variables,
		 /* variables[iph] : pointer to the structure describing
		    the variables in phase iph                            */
		 
		 int load_type, /* loading conditions                                          */
		 Load_Step load_step,   /* load step */
		 double E[6], double S[6], /* macroscopic strain and stress */
		 
		 LE_param *C0,    /* "reference material"                                        */
		 
		 double dt,  /* time step between current step and previous step */

		 double (**phase_epsilon)[6],
		 /* phase_epsilon[iph][ipt] gives the strain tensor of point
		    number ipt of phase number iph                              */
		 
		 
		 double (**phase_previous_epsilon)[6],
		 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
		    of point number ipt of phase number iph at previous loading
		    step.                                                        */
		 
		 double (**phase_sigma)[6],
		 /* phase_sigma[iph][ipt] gives the stress tensor of point
		    number ipt of phase number iph                              */

		 double (**phase_epsilon_thermal)[6],
		 /* phase_epsilon_thermal[iph][ipt] gives the thermal strain tensor of point
		    number ipt of phase number iph, if any.
                    In the case when no thermal strain has been applied,
                    phase_epsilon_thermal[iph] is equal to null for any iph
                 */
		 
		 
		 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
				       relation                                                 */
		 
		 double *compatibility_error,  /* error on comptatibility of the strain */
		 double *divs_error,  /* error on divergence of the stress                       */
		 double *macro_error, /* error on macroscopic stress or direction of macroscopic
					stress (depending on loading conditions)                */
		 double compatibility_precision, /* precision required on strain compatibility */
		 double divs_precision,   /* precision required by user on divergence of the
					     stress                                                      */
		 double macro_precision,  /* precision required by user on macroscopic stress
					     or direction of macroscopic
					     stress (depending on loading conditions)                    */
                 int maxiter, /* max number of iterations allowed */

		 
		 int *niter

		 ) {

  /*-----------------------------------------------------------------------------*/
  int convergence_reached;

  int status;

  int i, j;
  double save_E[6];  
  double norm;
  int iph;

  double temp;
  double *xxxdivs_error;
  xxxdivs_error = &temp;
  /*-----------------------------------------------------------------------------*/
  /* this scheme forces the strain field to be compatible */
  *compatibility_error = 0.;
  *niter = 0;
  
  do {
    (*niter)++;

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* error tests:                                                            */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* equilibrium test. 
       I.e.:
       div(sigma) <? precision
    */

    if ( equilibrium != equilibrium_divs_sum ) {
      start_timer(CRAFT_TIMER_BASIC_MISC);
      /* phase to image */
      //
      status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
      //      stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
      if(status!=0) exit(1);
      
      /* FFT */
      //      start_timer(CRAFT_TIMER_FFTD);
      status = craft_fft_image( image, -1);
      //      stop_timer(CRAFT_TIMER_FFTD);
      if(status!=0) exit(1);
      
      /* stores macroscopic stress */
      for(j=0;j<6;j++) {
        S[j] = creal(image->pxl.ft2[0][j]);
      }
      /* equilibirum test */
      //      start_timer(CRAFT_TIMER_EQUILIBRIUM);
      status = equilibrium( image, xxxdivs_error);
      //      stop_timer(CRAFT_TIMER_EQUILIBRIUM);
      
      /* set image to 2d order tensor type */
      image->type[0] = CRAFT_IMAGE_TENSOR2;
      image->type[1] = 6;
      image->type[2] = 0;
      image->nd[0] = image->nd[0]*2;
      for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      /* one verifies if an error has occured (this typically happens when
         iterative process is diverging)
      */
      if ( (isfinite(*xxxdivs_error)==0) ) {
        return -1;
      }

      stop_timer(CRAFT_TIMER_BASIC_MISC);

    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* stress: phase organization -> image organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
    stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* FFT applied to stress field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTD);
    status = craft_fft_image( image, -1);
    stop_timer(CRAFT_TIMER_FFTD);

    if(status!=0) exit(1);

    for(j=0;j<6;j++) {
      S[j] = creal(image->pxl.ft2[0][j]);
    }


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* calculates macroscopic strain to be applied                             */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    for(j=0;j<6;j++) save_E[j]=E[j];

    double alpha=1.;
    double beta=1.;

    double X[6];
    compute_macro(
                  load_type,
                  1., 1., 
                  C0->sub.ile->k, C0->sub.ile->mu, 
                  load_step.load,
                  load_step.direction,
                  E, S,
                  X
                  );

    compute_loading_error(
                          load_type,
                          //                          *alpha, *beta,
                          C0->sub.ile->k, C0->sub.ile->mu, 
                          load_step.load,
                          load_step.direction,
                          E, S,
                          macro_error
                          );
    
    for(j=0;j<6;j++) E[j]=X[j];
    
    /*
    gdrmacro_( &load_type,
               &beta,
	       &( C0->sub.ile->E),
	       &( C0->sub.ile->nu),
	       &load_step.load,
	       &load_step.direction[0], &load_step.direction[1], &load_step.direction[2],
	       &load_step.direction[5], &load_step.direction[4], &load_step.direction[3],
	       &E[0], &E[1], &E[2], &E[5], &E[4], &E[3],
	       &S[0], &S[1], &S[2], &S[5], &S[4], &S[3],
	       macro_error);
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Lippmann-Schwinger equation (calculates -Gamma0:sigma in Fourier space) */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_GAMMA0);
    status=lsp( image,  C0 , divs_error );
    if(status!=0) {
      fprintf(stderr,"CraFT error in lsp\n");
      fprintf(stderr,"exit!\n");
      exit(1);
    }
    stop_timer(CRAFT_TIMER_GAMMA0);


    /* divs_error contains in fact div(sigma) squared, on has to compute its
       square root and to normalize it                                         */
    norm =
      S[0]*S[0] +    S[1]*S[1] +    S[2]*S[2]
      +2.*S[3]*S[3] + 2.*S[4]*S[4] + 2.*S[5]*S[5];

    //    printf("S=%g %g %g %g %g %g\n",S[0],S[1],S[2],S[3],S[4],S[5]);
    //    printf("load=%g %g %g %g %g %g\n",
    //           load_step.direction[0],load_step.direction[1],load_step.direction[2],
    //           load_step.direction[3],load_step.direction[4],load_step.direction[5]
    //           );
    //    printf("load_step.load=%g\n",load_step.load);
    //    printf("norm=%e\n",norm);

    /* in the case when the macroscopic stress is null one must not normalize by it*/
    if ( norm < 1.e-6 ){
      //      printf("ici!\n");
      norm = 1.;
    }

    
    *divs_error = sqrt( *divs_error / norm );

    if ( equilibrium != equilibrium_divs_sum ) {
      *divs_error = *xxxdivs_error;
    }

    /* frequence 0                                                             */
    for(j=0;j<6;j++) image->pxl.ft2[0][j]=E[j]-save_E[j];

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*divs_error)==0) ) {
      return -1;
    }

    if ( (isfinite(*macro_error)==0) ) {
      return -2;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e\n",
	      load_step.time, *niter,*divs_error, *macro_error );
      fprintf(stdout, "     E(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
	      save_E[0], save_E[1], save_E[2], save_E[5], save_E[4], save_E[3] );
      fprintf(stdout, "     S(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
	      S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* convergence has been reached if divs_error and macro_error are lower
       than prescribed precision                                               */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    convergence_reached =
      ( (*divs_error < divs_precision) && (*macro_error < macro_precision) )  || ( *niter >= maxiter);
      
    /* If convergence has been reached, it is not necessary to go on
       within the current iterative process, and one can simply stop
       iterative process here.

       But in doing so, one loses compatibility with ancient versions of craft.

    */
    if (convergence_reached){
      /* one wants the average of the strain field to be equal to E
         and the average of the stress field to be equal to S (it is already the case):
       */
      for(j=0; j<6; j++){
        E[j] = save_E[j];
      }

      /* reset image to real space 2d order tensor type */
      image->type[0] = CRAFT_IMAGE_TENSOR2;
      image->type[1] = 6;
      image->type[2] = 0;
      image->nd[0] = image->nd[0]*2;
      for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

      break;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* inverse FFT applied to "-Gamma0:sigma" field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTI);
    status = craft_fft_image( image, +1);
    stop_timer(CRAFT_TIMER_FFTI);


    if(status!=0) exit(1);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* stress: image organization -> phase organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_IMAGE_TO_PHASE);
    status = image_to_phase( Nph, nppp, index, (void *)phase_sigma, image);
    stop_timer(CRAFT_TIMER_IMAGE_TO_PHASE);


    if(status!=0) exit(1);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* updates epsilon:                                                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_UPDATE);

    for(iph=0;iph<Nph;iph++) {
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_sigma, nppp)	\
  private(i, j)
      for(i=0; i<nppp[iph]; i++) {
	for(j=0;j<6;j++) {
	  phase_epsilon[iph][i][j] += phase_sigma[iph][i][j];
	}
      }
    }
    stop_timer(CRAFT_TIMER_UPDATE);


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* apply behavior                                                           */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /* it could seem to be a good idea to parallelize here, but it is not */
    start_timer(CRAFT_TIMER_BEHAVIOR);
    for(iph=0; iph<Nph; iph++) {
      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
          }
        }

      }

      material[iph].behavior(
			     material[iph].parameters,
			     orientation[iph],
			     &variables[iph],
			     dt);

      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
          }
        }

      }


    }
    stop_timer(CRAFT_TIMER_BEHAVIOR);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  } while( !convergence_reached  );

  return 0;
}

/*********************************************************************************/
/* added for harmonic implementation 2017/07/05  J. Boisse */
int basic_scheme_harmonic(

		 int Nph,        /* total number of phases in the microstructure */
		 int *nppp,      /* number of points per phase */
		 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
				    the Lippmann-Schwinger algorithm) of the point number ipt
				    in the phase number iph                                     */
		 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
					       orientation (or the orientation of its anistropy)
					       of phase number iph.
					       If iph is an isotropic material, orientation is useless     */
		 
		 Material *material,
		 /* material[iph] gives every information about the material
		    in phase iph */
		 
		 Variables *variables,
		 /* variables[iph] : pointer to the structure describing
		    the variables in phase iph                            */
		 
		 int load_type, /* loading conditions                                          */
		 Load_Step load_step,   /* load step */
		 double complex E[6], double complex S[6], /* macroscopic strain and stress */
		 
		 LE_param *C0,    /* "reference material"                                        */
		 
		 double dt,  /* time step between current step and previous step */

		 double complex (**phase_epsilon)[6],
		 /* phase_epsilon[iph][ipt] gives the strain tensor of point
		    number ipt of phase number iph                              */
		 
		 
		 double complex (**phase_previous_epsilon)[6],
		 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
		    of point number ipt of phase number iph at previous loading
		    step.                                                        */
		 
		 double complex (**phase_sigma)[6],
		 /* phase_sigma[iph][ipt] gives the stress tensor of point
		    number ipt of phase number iph                              */

		 double (**phase_epsilon_thermal)[6],
		 /* phase_epsilon_thermal[iph][ipt] gives the thermal strain tensor of point
		    number ipt of phase number iph, if any.
                    In the case when no thermal strain has been applied,
                    phase_epsilon_thermal[iph] is equal to null for any iph
                 */
		 
		 
		 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
				       relation                                                 */
		 
		 double *compatibility_error,  /* error on comptatibility of the strain */
		 double *divs_error,  /* error on divergence of the stress                       */
		 double *macro_error, /* error on macroscopic stress or direction of macroscopic
					stress (depending on loading conditions)                */
		 double compatibility_precision, /* precision required on strain compatibility */
		 double divs_precision,   /* precision required by user on divergence of the
					     stress                                                      */
		 double macro_precision,  /* precision required by user on macroscopic stress
					     or direction of macroscopic
					     stress (depending on loading conditions)                    */
                 int maxiter, /* max number of iterations allowed */

		 
		 int *niter

		 ) {

  /*-----------------------------------------------------------------------------*/
  int convergence_reached;

  int status;

  int i, j;

  /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex save_E[6];  

  double norm;
  int iph;

  double temp;
  double *xxxdivs_error;
  xxxdivs_error = &temp;
  /*-----------------------------------------------------------------------------*/
  /* this scheme forces the strain field to be compatible */
  *compatibility_error = 0.;
  *niter = 0;
  
#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), start do{}while(!convergence_reached) loop\n");
#endif 

  do {
    (*niter)++;

#ifdef DEBUG

  printf("\n###\n");
  printf("basic_scheme.c : in basic_scheme_harmonic(), LOOP, (*niter)= %d\n",(*niter));
#endif

    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* error tests:                                                            */
    /*= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =*/
    /* equilibrium test. 
       I.e.:
       div(sigma) <? precision
    */

    /* 2019-06-08 norm computed the same way than in amitex => do this part in any case */
    //if ( equilibrium != equilibrium_divs_sum ) {
      start_timer(CRAFT_TIMER_BASIC_MISC);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), CASE: equilibrium != equilibrium_divs_sum\n");
  printf("basic_scheme.c : in basic_scheme_harmonic(), RUN phase_to_image(...(void *)phase_sigma, image)\n");
#endif 

      /* phase to image */
      //
      status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
      //      stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
      if(status!=0) exit(1);
      
      /* FFT */
      //      start_timer(CRAFT_TIMER_FFTD);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), RUN craft_fft_image_harmonic( image, -1)\n");
#endif 

      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      status = craft_fft_image_harmonic( image, -1);
      //      stop_timer(CRAFT_TIMER_FFTD);
      if(status!=0) exit(1);
 
#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), S[j] = image->pxl.ft2[0][j]:\n");
#endif 
     
      /* stores macroscopic stress */
      for(j=0;j<6;j++) {
        /* modified for harmonic implementation 2017/07/05  J. Boisse */
        //  S[j] = creal(image->pxl.ft2[0][j]);
        S[j] = image->pxl.ft2[0][j];
      }

#ifdef DEBUG
  printf("S[j]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(S[i]),cimag(S[i]));
  }
#endif 

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), equilibrium test, equilibrium_divs_sum_harmonic()\n");
#endif 
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      /* equilibirum test */
      //      start_timer(CRAFT_TIMER_EQUILIBRIUM);
      status = equilibrium_divs_sum_harmonic( image, xxxdivs_error);
      //      stop_timer(CRAFT_TIMER_EQUILIBRIUM);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), set image to 2d order tensor type\n");
#endif 
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      /* set image to 2d order tensor type */
      image->type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
      image->type[1] = 6;
      image->type[2] = 0;
      image->nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/07/05  J. Boisse */
      for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      /* one verifies if an error has occured (this typically happens when
         iterative process is diverging)
      */
      if ( (isfinite(*xxxdivs_error)==0) ) {
        return -1;
      }

      stop_timer(CRAFT_TIMER_BASIC_MISC);

      /********************************************/
      /********************************************/
      /*    build known field to test criteria    */
      //printf("[test criteria] *xxxdivs_error = %g\n",*xxxdivs_error);
      /* END : build known field to test criteria */
      /********************************************/
      /********************************************/

    //} /* 2019-06-08 norm computed the same way than in amitex => do this part in any case */


#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), stress: phase organization -> image organization:\n");
#endif 
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* stress: phase organization -> image organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    status = phase_to_image( Nph, nppp, index, (void *)phase_sigma, image);
    stop_timer(CRAFT_TIMER_PHASE_TO_IMAGE);
    if(status!=0) exit(1);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), FFT applied to stress field\n");
#endif 
    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* FFT applied to stress field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTD);
    status = craft_fft_image_harmonic( image, -1);
    stop_timer(CRAFT_TIMER_FFTD);

    if(status!=0) exit(1);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), S[j] = image->pxl.ft2[0][j]:\n");
#endif 
    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    for(j=0;j<6;j++) {
      S[j] = image->pxl.ft2[0][j];
    }

#ifdef DEBUG
  printf("S[j]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(S[i]),cimag(S[i]));
  }
#endif 


#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), calculates macroscopic strain to be applied\n");
#endif 

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* calculates macroscopic strain to be applied                             */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    for(j=0;j<6;j++) save_E[j]=E[j];

    double alpha=1.;
    double beta=1.;

   /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex X[6];

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), RUN compute_macro_harmonic()\n");
#endif 

   /* modified for harmonic implementation 2017/07/05  J. Boisse */
    compute_macro_harmonic(
                  load_type,
                  1., 1., 
                  C0->sub.ile->k, C0->sub.ile->mu, 
                  load_step.load,
                  load_step.direction,
                  E, S,
                  X
                  );

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), RUN compute_loading_error_harmonic()\n");
#endif 

   /* modified for harmonic implementation 2017/07/05  J. Boisse */
    compute_loading_error_harmonic(
                          load_type,
                          //                          *alpha, *beta,
                          C0->sub.ile->k, C0->sub.ile->mu, 
                          load_step.load,
                          load_step.direction,
                          E, S,
                          macro_error
                          );
    
    for(j=0;j<6;j++) E[j]=X[j];
    
    /*
    gdrmacro_( &load_type,
               &beta,
	       &( C0->sub.ile->E),
	       &( C0->sub.ile->nu),
	       &load_step.load,
	       &load_step.direction[0], &load_step.direction[1], &load_step.direction[2],
	       &load_step.direction[5], &load_step.direction[4], &load_step.direction[3],
	       &E[0], &E[1], &E[2], &E[5], &E[4], &E[3],
	       &S[0], &S[1], &S[2], &S[5], &S[4], &S[3],
	       macro_error);
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), RUN lsp_harmonic(), ( -Gamma0:sigma ):\n");
#endif 

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Lippmann-Schwinger equation (calculates -Gamma0:sigma in Fourier space) */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_GAMMA0);

   /* modified for harmonic implementation 2017/07/05  J. Boisse */
    status=lsp_harmonic( image,  C0 , divs_error );
    if(status!=0) {
      fprintf(stderr,"CraFT error in lsp\n");
      fprintf(stderr,"exit!\n");
      exit(1);
    }
    stop_timer(CRAFT_TIMER_GAMMA0);


    /* divs_error contains in fact div(sigma) squared, on has to compute its */
    /* square root and to normalize it */

#ifdef DEBUG
  printf("-Gamma0:sigma[0][i]= \n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(image->pxl.ft2[0][i]),cimag(image->pxl.ft2[0][i]));
  }
#endif

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), calculate norm(S*S):\n");
#endif 

    /* modified for harmonic implementation 2017/07/05  J. Boisse */ /* corrected 2019/07/17 */
    norm = creal( S[0]*conj(S[0]) + S[1]*conj(S[1]) + S[2]*conj(S[2]) 
           + 2.*S[3]*conj(S[3]) + 2.*S[4]*conj(S[4]) + 2.*S[5]*conj(S[5]) );

    //    printf("S=%g %g %g %g %g %g\n",S[0],S[1],S[2],S[3],S[4],S[5]);
    //    printf("load=%g %g %g %g %g %g\n",
    //           load_step.direction[0],load_step.direction[1],load_step.direction[2],
    //           load_step.direction[3],load_step.direction[4],load_step.direction[5]
    //           );
    //    printf("load_step.load=%g\n",load_step.load);
    //    printf("norm=%e\n",norm);

#ifdef DEBUG
  printf("norm= %lf\n",norm);
#endif 

    /* in the case when the macroscopic stress is null one must not normalize by it */
    if ( norm < 1.e-6 ){
      //      printf("ici!\n");
      norm = 1.;
    }


#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), calculate *divs_error:\n");
#endif 
    
    *divs_error = sqrt( *divs_error / norm );

#ifdef DEBUG
  printf("*divs_error= %lf\n", *divs_error);
#endif 

    /* 2019-06-08 norm computed the same way than in amitex => do this part in any case */
    //if ( equilibrium != equilibrium_divs_sum ) {
      *divs_error = *xxxdivs_error;
#ifdef DEBUG
  printf("*divs_error= %lf\n", *divs_error);
#endif 
 
    //}


#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), image->pxl.ft2[0][j]=E[j]-save_E[j]:\n");
#endif 

    /* frequence 0                                                             */
    for(j=0;j<6;j++) image->pxl.ft2[0][j]=E[j]-save_E[j];

#ifdef DEBUG
  printf("E[j]-save_E[j]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[i]-save_E[i]),cimag(E[i]-save_E[i]));
  }
#endif 

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), one verifies if an error has occured (diverging ?)\n");
#endif
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* one verifies if an error has occured (this typically happens when
       iterative process is diverging)
    */
    if ( (isfinite(*divs_error)==0) ) {
      return -1;
    }

    if ( (isfinite(*macro_error)==0) ) {
      return -2;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if (verbose){
      fprintf(stdout, "------> t=%15.8g iter=%d divs error=%15.8e macro error=%15.8e\n",
	      load_step.time, *niter,*divs_error, *macro_error );
      fprintf(stdout, "     E(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
	      save_E[0], save_E[1], save_E[2], save_E[5], save_E[4], save_E[3] );
      fprintf(stdout, "     S(11,22,33,23,13,12) = %15.8g %15.8g %15.8g %15.8g %15.8g %15.8g \n",
	      S[0], S[1], S[2], S[5], S[4], S[3] );
      fprintf(stdout,"\n");
    }

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), convergence_reached ?\n");
#endif
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* convergence has been reached if divs_error and macro_error are lower
       than prescribed precision                                               */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    convergence_reached =
      ( (*divs_error < divs_precision) && (*macro_error < macro_precision) )  || ( *niter >= maxiter);
      
    /* If convergence has been reached, it is not necessary to go on
       within the current iterative process, and one can simply stop
       iterative process here.

       But in doing so, one loses compatibility with ancient versions of craft.

    */
    if (convergence_reached){

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), convergence_reached ? => YES\n");
#endif
      /* one wants the average of the strain field to be equal to E
         and the average of the stress field to be equal to S (it is already the case):
       */

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), E[j] = save_E[j]:\n");
#endif
      for(j=0; j<6; j++){
        E[j] = save_E[j];
      }

#ifdef DEBUG
  printf("E[j]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[i]),cimag(E[i]));
  }
#endif 

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), reset image to DIRECT space 2d order tensor type\n");
#endif
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      /* reset image to real space 2d order tensor type */
      image->type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
      image->type[1] = 6;
      image->type[2] = 0;
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      image->nd[0] = image->nd[0];
      for(i=0;i<3;i++) image->p[i] = 1. / ( image->p[i]*image->n[i] );

#ifdef DEBUG

  printf("\n###\n");
  printf("basic_scheme.c : in basic_scheme_harmonic(), end of LOOP, (*niter)= %d\n",(*niter));
#endif

      break;
    }

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), inverse FFT applied to -Gamma0:sigma field:\n");
  printf("image->pxl.ft2[0][i]= (before)\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(image->pxl.ft2[0][i]),cimag(image->pxl.ft2[0][i]));
  }
#endif

    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* inverse FFT applied to "-Gamma0:sigma" field */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_FFTI);
    status = craft_fft_image_harmonic( image, +1);
    stop_timer(CRAFT_TIMER_FFTI);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), inverse FFT of -Gamma0:sigma: image -> phase \n");
#endif
    if(status!=0) exit(1);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* stress: image organization -> phase organization                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_IMAGE_TO_PHASE);
    status = image_to_phase( Nph, nppp, index, (void *)phase_sigma, image);
    stop_timer(CRAFT_TIMER_IMAGE_TO_PHASE);

    if(status!=0) exit(1);

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), updates epsilon\n");
#endif
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* updates epsilon:                                                        */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    start_timer(CRAFT_TIMER_UPDATE);

    for(iph=0;iph<Nph;iph++) {
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_sigma, nppp)	\
  private(i, j)
      for(i=0; i<nppp[iph]; i++) {
        for(j=0;j<6;j++) {
//          printf("%f+%fi (iph,i,j)=(%d,%d,%d)\n",creal(phase_sigma[iph][i][j]),cimag(phase_sigma[iph][i][j]), iph,i,j);
          phase_epsilon[iph][i][j] += phase_sigma[iph][i][j];
        }
      }
    }
    stop_timer(CRAFT_TIMER_UPDATE);
#ifdef DEBUG
        for(j=0;j<6;j++) {
          printf("%f+%fi (iph,i,j)=(%d,%d,%d)\n",creal(phase_sigma[0][0][j]),cimag(phase_sigma[0][0][j]), 0,0,j);
          printf("%f+%fi (iph,i,j)=(%d,%d,%d)\n",creal(phase_sigma[0][1][j]),cimag(phase_sigma[0][1][j]), 0,1,j);
          printf("%f+%fi (iph,i,j)=(%d,%d,%d)\n",creal(phase_sigma[0][2][j]),cimag(phase_sigma[0][2][j]), 0,2,j);
        }
#endif

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), apply behavior\n");
#endif
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* apply behavior                                                           */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

    /* it could seem to be a good idea to parallelize here, but it is not */
    start_timer(CRAFT_TIMER_BEHAVIOR);
    for(iph=0; iph<Nph; iph++) {
/*
#ifdef DEBUG
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_sigma, nppp)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            printf("%f+%fi\n",creal(phase_epsilon[iph][i][j]),cimag(phase_epsilon[iph][i][j]));
          }
        }
#endif
*/

      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
          }
        }

      }

#ifdef DEBUG
  printf("basic_scheme.c : in basic_scheme_harmonic(), material[iph].behavior()\n");
#endif
      material[iph].behavior(
			     material[iph].parameters,
			     orientation[iph],
			     &variables[iph],
			     load_step.time); /* => modified for harmonic implementation  */

      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
          }
        }

      }


    }
    stop_timer(CRAFT_TIMER_BEHAVIOR);
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  } while( !convergence_reached  );

  return 0;
}
