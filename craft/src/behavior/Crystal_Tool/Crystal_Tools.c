#define _DEFAULT_SOURCE
#define _ISOC99_SOURCE

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <utils.h>

#ifdef _OPENMP
#include <omp.h>
#endif


#include <materials.h>

#include <LE_Cmatrix.h>

/*******************************************************************
 function that rotate ngl and mgl vectors 
 to create Schmid's tensor sgl
********************************************************************/

int rotate_sgl (double (*sgl)[6],double (*ngl)[3], double (*mgl)[3],
                 int nsyst,
		 Euler_Angles orientation)

/*void rotate_sgl (double sgl[12][6],double ngl[12][3], double mgl[12][3],
  int nsyst,
  Euler_Angles orientation)*/



{
  double cphi1,sphi1,cphi,sphi,cphi2,sphi2;
  double p11, p12, p13, p21, p22, p23, p31, p32, p33;
  int isys;
  double rngl[3],rmgl[3];
  double ur2;

  ur2=1./sqrt(2.);

  cphi1 = cos(orientation.phi1);
  cphi = cos(orientation.Phi);
  cphi2 = cos(orientation.phi2);
  sphi1 = sin(orientation.phi1);
  sphi = sin(orientation.Phi);
  sphi2 = sin(orientation.phi2);
	  
  p11 = cphi2*cphi1-sphi2*sphi1*cphi;
  p12 = -sphi2*cphi1-cphi2*sphi1*cphi;
  p13 = sphi1*sphi;
  p21 = cphi2*sphi1+sphi2*cphi1*cphi;
  p22 = -sphi2*sphi1+cphi2*cphi1*cphi;
  p23 = -cphi1*sphi;
  p31 = sphi2*sphi;
  p32 = cphi2*sphi;
  p33 = cphi;
      
  for(isys=0;isys<nsyst;isys++)
    {

      /* rotation of the normale to the sliding plan                   */
      rngl[0] = p11*ngl[isys][0]+p12*ngl[isys][1]+p13*ngl[isys][2];
      rngl[1] = p21*ngl[isys][0]+p22*ngl[isys][1]+p23*ngl[isys][2];
      rngl[2] = p31*ngl[isys][0]+p32*ngl[isys][1]+p33*ngl[isys][2];

      /* rotation of the sliding direction                             */
      rmgl[0] = p11*mgl[isys][0]+p12*mgl[isys][1]+p13*mgl[isys][2];
      rmgl[1] = p21*mgl[isys][0]+p22*mgl[isys][1]+p23*mgl[isys][2];
      rmgl[2] = p31*mgl[isys][0]+p32*mgl[isys][1]+p33*mgl[isys][2];

      /* Schmid's tensor mu = m*n                                     */
      sgl[isys][0] = rngl[0]*rmgl[0];
      sgl[isys][1] = rngl[1]*rmgl[1];
      sgl[isys][2] = rngl[2]*rmgl[2];
      sgl[isys][3] = 1./2.*(rngl[1]*rmgl[2]+rngl[2]*rmgl[1]);
      sgl[isys][4] = 1./2.*(rngl[0]*rmgl[2]+rngl[2]*rmgl[0]);
      sgl[isys][5] = 1./2.*(rngl[0]*rmgl[1]+rngl[1]*rmgl[0]);
  
    }

  return 0;
}


/********************************************************************************************/
/* HM 4th november 2011                                                 
   reads slip systems by entering thein number and n and m vectors                          
*/
/********************************************************************************************/
int read_crystal_parameters(
				      FILE *f,
				      int *nsys,
				      double (**ngl)[3],
				      double (**mgl)[3],
				      double (**sgl)[6]) {  

  int status;
  int i,j;
  double norme;

  

  /*-----------------------------------------------------------------------*/  
  status=craft_fscanf(f,"%d\n",nsys);	
  if (status!=1) return -1;
  


  /*-----------------------------------------------------------------------*/  
  /* crystallographic parameters */

  *ngl = (double (*)[3])malloc((*nsys)*sizeof(**ngl));
  *mgl = (double (*)[3])malloc((*nsys)*sizeof(**mgl));
  *sgl = (double (*)[6])malloc((*nsys)*sizeof(**sgl));
  
  for(i=0;i<*nsys;i++) {
    
    status=craft_fscanf( f,
			 "%lf %lf %lf %lf %lf %lf", 
			 &((*ngl)[i][0]), &((*ngl)[i][1]), &((*ngl)[i][2]), 
			 &((*mgl)[i][0]), &((*mgl)[i][1]), &((*mgl)[i][2])
			 );
    if(status!=6) return -1;

    norme = sqrt((*ngl)[i][0]*(*ngl)[i][0] +
		 (*ngl)[i][1]*(*ngl)[i][1] + 
		 (*ngl)[i][2]*(*ngl)[i][2]);
    
    (*ngl)[i][0] = (*ngl)[i][0]/norme;
    (*ngl)[i][1] = (*ngl)[i][1]/norme;
    (*ngl)[i][2] = (*ngl)[i][2]/norme;
    
    
    norme = sqrt((*mgl)[i][0]*(*mgl)[i][0] + 
		 (*mgl)[i][1]*(*mgl)[i][1] + 
		 (*mgl)[i][2]*(*mgl)[i][2]);
    
    (*mgl)[i][0] = (*mgl)[i][0]/norme;
    (*mgl)[i][1] = (*mgl)[i][1]/norme;
    (*mgl)[i][2] = (*mgl)[i][2]/norme;
    
    
    for(j=0;j<6;j++)
      {
	//	  (*sgl)[i][j] = HUGE_VAL;
	(*sgl)[i][j] = NAN;
      }
    
  }
  
  return 0;
  
}

