#ifndef CRYSTAL_TOOLS
#define CRYSTAL_TOOLS
/*********************************************************************************************/
int rotate_sgl (double (*sgl)[6],double (*ngl)[3], double (*mgl)[3],
                 int nsyst,
		Euler_Angles orientation);

int read_crystal_parameters(
				      FILE *f,
				      int *nsys,
				      double (**ngl)[3],
				      double (**mgl)[3],
				      double (**sgl)[6]);
#endif
