#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>
#include <utils.h>

#include <EPIH_materials.h>
#include <variables.h>

#ifdef _OPENMP
#include <omp.h>
#endif
/*######################################################################*/
/* H. Moulinec
   F. Silva
   LMA/CNRS
   july 12th 2011

   functions to manage Elastic Plastic Isotrope Hardening behavior

*/
/*######################################################################*/
int read_hardening( FILE *f, EPIH_param *x) {
  double ys,H;
  int status, Nt, it;
  double p;
  double s0;

  status=craft_fscanf(f,"%d",&(x->hardening));
  if (status!=1) return -1;

  switch (x->hardening){

  case WITHOUT_HARDENING:
    x->hp.whp = malloc(1*sizeof(*x->hp.whp));
    if (x->hp.whp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(ys));
    if (status!=1) return -1;
    x->hp.whp->ys = ys;
    break;

  case LINEAR_HARDENING:
    x->hp.lhp = malloc(1*sizeof(*x->hp.lhp));
    if (x->hp.lhp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(ys));
    if (status!=1) return -1;
    x->hp.lhp->ys = ys;
    status=craft_fscanf(f,"%lf",&(H));
    if (status!=1) return -1;
    x->hp.lhp->H = H;
    break;

  case TABULATED_HARDENING:
    x->hp.thp = malloc(1*sizeof(*x->hp.thp));
    if (x->hp.thp==NULL) return -1;

    status=craft_fscanf(f,"%d",&(Nt));
    if (status!=1) return -1;
    x->hp.thp->Nt = Nt;
    x->hp.thp->p  = malloc(Nt*sizeof(*x->hp.thp->p));
    x->hp.thp->s0 = malloc(Nt*sizeof(*x->hp.thp->s0));
    if (x->hp.thp->p ==NULL) return -1;
    if (x->hp.thp->s0==NULL) return -1;

    for(it=0; it<(Nt); it++){
      status=craft_fscanf(f,"%lf",&(p));
      if (status!=1) return -1;
      x->hp.thp->p[it] = p;

      status=craft_fscanf(f,"%lf",&(s0));
      if (status!=1) return -1;
      x->hp.thp->s0[it] = s0;
    }
    x->hp.thp->s0_3mu = NULL;
    x->hp.thp->s0_3mup = NULL;
    
    break;
  }
  return 1;
}

int read_parameters_EPIH( FILE *f, void *p ) {

  EPIH_param *x;

  int status;

  x = (EPIH_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );

  status = read_hardening(f, x);
  if (status!=1) return -1;

  return 0;
}
/*######################################################################*/
int print_parameters_EPIH( FILE *f, void *p , int flag ) {
  int status,it;
  EPIH_param *x;
  status=0;
  x = (EPIH_param *)p;

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(x->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(x->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lam� coef.  = %lf  ",(x->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lam� coef. = %lf  ",(x->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(x->k));
    fprintf(f,"\n");
    switch (x->hardening){
    case WITHOUT_HARDENING:
      fprintf(f,"# yield stress = %lf  ", x->hp.whp->ys);
      fprintf(f,"\n");
      break;
    case LINEAR_HARDENING:
      fprintf(f,"# yield stress = %lf  ", x->hp.lhp->ys);
      fprintf(f,"\n");
      fprintf(f,"# linear hardening = %lf  ", x->hp.lhp->H);
      fprintf(f,"\n");
      break;
    case TABULATED_HARDENING:
      fprintf(f,"# tabulated plastic stress  ");
      for(it=0; it<(x->hp.thp->Nt); it++){
        fprintf(f,"# \t %lf \t %lf", x->hp.thp->p[it], x->hp.thp->s0[it]);
      break;
      }
    }
  }
  else {
    fprintf(f,"E  = %lf  ",(x->E));
    fprintf(f,"nu = %lf  ",(x->nu));
    fprintf(f,"1st Lam� coef.  = %lf  ",(x->lb));
    fprintf(f,"2d Lam� coef. = %lf  ",(x->mu));
    fprintf(f,"bulk modulus  = %lf  ",(x->k));
    switch (x->hardening){
    case WITHOUT_HARDENING:
      fprintf(f,"yield stress = %lf  ", x->hp.whp->ys);
      break;
    case LINEAR_HARDENING:
      fprintf(f,"yield stress = %lf  ", x->hp.lhp->ys);
      fprintf(f,"linear hardening = %lf  ", x->hp.lhp->H);
      break;
    case TABULATED_HARDENING:
      fprintf(f,"tabulated plastic stress  ");
      for(it=0; it<(x->hp.thp->Nt); it++){
        fprintf(f,"# \t %lf \t %lf", x->hp.thp->p[it], x->hp.thp->s0[it]);
      }
      break;
    }
  }

  return status;
}

/*######################################################################*/
int allocate_parameters_EPIH( void **p ) {

  EPIH_param *x;
  int status;



  status=0;

  x=malloc(sizeof(*x));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }

  return status;

}

/*######################################################################*/
int deallocate_parameters_EPIH( void *x ) {

  int status;

  status=0;
  switch(((EPIH_param *) x)->hardening){
  case WITHOUT_HARDENING:
    free(((EPIH_param *)x)->hp.whp);
    break;
  case LINEAR_HARDENING:
    free(((EPIH_param *)x)->hp.lhp);
    break;
  case TABULATED_HARDENING:
    free(((EPIH_param *)x)->hp.thp->p);
    free(((EPIH_param *)x)->hp.thp->s0);
    if (((EPIH_param *)x)->hp.thp->s0_3mu != NULL )
      free(((EPIH_param *)x)->hp.thp->s0_3mu);
    if (((EPIH_param *)x)->hp.thp->s0_3mup != NULL )
      free(((EPIH_param *)x)->hp.thp->s0_3mup);

    free(((EPIH_param *)x)->hp.thp);
  }
  free(x);

  return status;

}

/*######################################################################*/
int copy_parameters_EPIH( void *param_dest, void *param_src) {
  int i;
  EPIH_param *p,*q;

  p = (EPIH_param *)param_dest;
  q = (EPIH_param *)param_src;
  *p = *q;

  switch(q->hardening){
  case WITHOUT_HARDENING:
    p->hp.whp = malloc(1*sizeof(*p->hp.whp));
    *p->hp.whp = *q->hp.whp;
    break;
  case LINEAR_HARDENING:
    p->hp.lhp = malloc(1*sizeof(*p->hp.lhp));
    *p->hp.lhp = *q->hp.lhp;
    break;
  case TABULATED_HARDENING:
    p->hp.thp = malloc(1*sizeof(*p->hp.thp));
    p->hp.thp->Nt = q->hp.thp->Nt;
    p->hp.thp->p  = malloc( (p->hp.thp->Nt)*sizeof(*p->hp.thp->p) );
    p->hp.thp->s0 = malloc( (p->hp.thp->Nt)*sizeof(*p->hp.thp->s0));
    if ( p->hp.thp->p==NULL ) return -1;
    if ( p->hp.thp->s0==NULL ) return -1;
    p->hp.thp->s0_3mu = NULL;
    p->hp.thp->s0_3mup = NULL;

    for(i=0; i<p->hp.thp->Nt;i++){
      p->hp.thp->p[i]  = q->hp.thp->p[i];
      p->hp.thp->s0[i] = q->hp.thp->s0[i];
    }
    break;
  }

  return 0;
}

/*######################################################################*/
int behavior_EPIH(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt){
  int status;
  int N;
  EPIH_param *p;

  double (*epsilon) [6], (*epsilon_p) [6];
  double (*sigma) [6],   (*sigma_p) [6];
  double *plastic, *plastic_p;

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  N = sv->np;

  p = (EPIH_param *)param;
  status = 0;

  int epih3d_without_hardening(
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      wo_hardening_param *whp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double (*e)[6],   /* strain */
      double (*s)[6],   /* stress */
      double (*ep)[6],  /* strain at previous loading step */
      double (*sp)[6]   /* stress at previous loading step */
      );
  int epih3d_linear_hardening(
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      linear_hardening_param *lhp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double (*e)[6],   /* strain */
      double (*s)[6],   /* stress */
      double *p,        /* equivalent plastic strain */
      double (*ep)[6],  /* strain at previous loading step */
      double (*sp)[6],  /* stress at previous loading step */
      double *pp        /* equivalent plastic strain at previous loading step */
      );
  int epih3d_tab_hardening(
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      tab_hardening_param *thp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double e[N][6],   /* strain */
      double s[N][6],   /* stress */
      double p[N],      /* equivalent plastic strain */
      double ep[N][6],  /* strain at previous loading step */
      double sp[N][6],  /* stress at previous loading step */
      double pp[N]      /* equivalent plastic strain at previous loading step */
      );

  switch (p->hardening){
  case WITHOUT_HARDENING:
    status = epih3d_without_hardening( p->k, p->mu, p->hp.whp, N,
        epsilon, sigma, epsilon_p, sigma_p);
    break;
  case LINEAR_HARDENING:
    plastic   = (double *)(ptr_variable("plastic", sv, param)->values);
    plastic_p = (double *)(ptr_variable("previous plastic", sv, param)->values);
    status = epih3d_linear_hardening( p->k, p->mu, p->hp.lhp, N,
        epsilon, sigma, plastic, epsilon_p, sigma_p, plastic_p);
    break;
  case TABULATED_HARDENING:
    plastic   = (double *)(ptr_variable("plastic", sv, param)->values);
    plastic_p = (double *)(ptr_variable("previous plastic", sv, param)->values);
    
    /* store sigma_0(p)+3*mu*p in the table s0_3mu so that inversion is cheaper */
    if ( p->hp.thp->s0_3mu == NULL ){
      int it;
      p->hp.thp->s0_3mu = malloc(p->hp.thp->Nt * sizeof(*p->hp.thp->s0_3mu));
      if ( p->hp.thp->s0_3mu == NULL ) return -1;

      for(it=0; it<p->hp.thp->Nt; it++){
        p->hp.thp->s0_3mu[it] = p->hp.thp->s0[it] + 3 * p->mu * p->hp.thp->p[it];
      }
    }

    status = epih3d_tab_hardening( p->k, p->mu, p->hp.thp, N,
        epsilon, sigma, plastic, epsilon_p, sigma_p, plastic_p);
  }

  return status;
}

/*######################################################################*/
int allocate_variables_EPIH( int N, Variables *sv, void *param) {

  int status;
  int scalar_t[3]={SCALAR,0,0};
  int tensor_t[3]={TENSOR2,0,0};
  status = 0;
  switch (((EPIH_param *)param)->hardening){
  case WITHOUT_HARDENING:
    sv->nvar = 4;
    break;
  case LINEAR_HARDENING:
  case TABULATED_HARDENING:
    sv->nvar = 6;
    break;
  }

  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  switch (((EPIH_param *)param)->hardening){
  case WITHOUT_HARDENING:
    break;
  case LINEAR_HARDENING:
  case TABULATED_HARDENING:
    status = allocate_variable(&sv->vars[4], "plastic", N, scalar_t, 0);
    if (status<0) return -1;
    status = allocate_variable(&sv->vars[5], "previous plastic", N, scalar_t, 0);
    if (status<0) return -1;
    break;
  }

#ifdef DEBUG
  printf("allocate_variables_EPIH %d \n",status);
#endif

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_EPIH(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3) {
  int status;
  double (*epsilon) [6];
  double (*epsilon_p) [6];

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);

  extrapol2( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

  /* sigma -> sigma_p */
  status = copy_variable_values( &(sv->vars[3]), &(sv->vars[1]), sv->np);

  /* plastic -> plastic_p */
  switch (((EPIH_param *)param)->hardening){
  case LINEAR_HARDENING:
  case TABULATED_HARDENING:
    status = copy_variable_values( &(sv->vars[5]), &(sv->vars[4]), sv->np);
  }

  return status;
}

/*######################################################################*/
int load_Cmatrix_EPIH(void *param, double C[6][6])
{

int i,j;
EPIH_param p;

p = *((EPIH_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}

C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
}

/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
int solve_spc0e_EPIH( void *param,
                    Euler_Angles orientation,
                    Variables *sv,
                    double dt,
                    LE_param *L0,
                    double (*tau)[6]
                  ){

  int status;
  int N;
  EPIH_param *p;

  double (*epsilon) [6], (*epsilon_p) [6];
  double (*sigma) [6],   (*sigma_p) [6];
  double *plastic, *plastic_p;

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  N = sv->np;

  if ( L0->isotropy != ISOTROPIC){
    fprintf(stderr, "The reference material has to be isotropic to solve (c+c0)e=tau\n"
      "for the elastoplastic materials.");
    return -1;
  }

  p = (EPIH_param *)param;
  status = 0;

  int epih3d_spc0e_without_hardening(
      double k0,        /* reference material bulk modulus */
      double mu0,       /* reference material 2d Lam� coefficient */
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      wo_hardening_param *whp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double (*e)[6],   /* strain */
      double (*s)[6],   /* stress */
      double (*ep)[6],  /* strain at previous loading step */
      double (*sp)[6],  /* stress at previous loading step */
      double (*tau)[6]  /* right-hand side term of equation sigma+C0:epsilon = tau */
      );
  int epih3d_spc0e_linear_hardening(
      double k0,        /* reference material bulk modulus */
      double mu0,       /* reference material 2d Lam� coefficient */
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      linear_hardening_param *lhp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double (*e)[6],   /* strain */
      double (*s)[6],   /* stress */
      double *p,        /* equivalent plastic strain */
      double (*ep)[6],  /* strain at previous loading step */
      double (*sp)[6],  /* stress at previous loading step */
      double *pp,       /* equivalent plastic strain at previous loading step */
      double (*tau)[6]  /* right-hand side term of equation sigma+C0:epsilon = tau */
      );
  int epih3d_spc0e_tab_hardening(
      double k0,        /* reference material bulk modulus */
      double mu0,       /* reference material 2d Lam� coefficient */
      double k,         /* bulk modulus */
      double mu,        /* 2d Lam� coefficient */
      tab_hardening_param *thp, /* structure containing the hardening data */
      int N,            /* number of points on which to apply constutive law */
      double e[N][6],   /* strain */
      double s[N][6],   /* stress */
      double p[N],      /* equivalent plastic strain */
      double ep[N][6],  /* strain at previous loading step */
      double sp[N][6],  /* stress at previous loading step */
      double pp[N],     /* equivalent plastic strain at previous loading step */
      double (*tau)[6]  /* right-hand side term of equation sigma+C0:epsilon = tau */
      );

  switch (p->hardening){
  case WITHOUT_HARDENING:
    status = epih3d_spc0e_without_hardening(
        L0->sub.ile->k, L0->sub.ile->mu,
        p->k, p->mu, p->hp.whp, N,
        epsilon, sigma, epsilon_p, sigma_p, tau);
    break;
  case LINEAR_HARDENING:
    plastic   = (double *)(ptr_variable("plastic", sv, param)->values);
    plastic_p = (double *)(ptr_variable("previous plastic", sv, param)->values);
    status = epih3d_spc0e_linear_hardening(
        L0->sub.ile->k, L0->sub.ile->mu,
        p->k, p->mu, p->hp.lhp, N,
        epsilon, sigma, plastic, epsilon_p, sigma_p, plastic_p, tau);
    break;
  case TABULATED_HARDENING:
    plastic   = (double *)(ptr_variable("plastic", sv, param)->values);
    plastic_p = (double *)(ptr_variable("previous plastic", sv, param)->values);
    
    /* store sigma_0(p)+3*mu*mu0/(mu+mu0)*p in the table s0_3mup */
    /* so that inversion is cheaper */
    if ( p->hp.thp->s0_3mup == NULL ){
      int it;
      double coef;

      p->hp.thp->s0_3mup = malloc(p->hp.thp->Nt * sizeof(*p->hp.thp->s0_3mup));
      if (p->hp.thp->s0_3mup==NULL) return -1;

      coef = 3*p->mu*L0->sub.ile->mu / ( p->mu+L0->sub.ile->mu );
      for(it=0; it<p->hp.thp->Nt; it++){
        p->hp.thp->s0_3mup[it] = p->hp.thp->s0[it] + coef * p->hp.thp->p[it];
      }
    }

    status = epih3d_spc0e_tab_hardening(
        L0->sub.ile->k, L0->sub.ile->mu,
        p->k, p->mu, p->hp.thp, N,
        epsilon, sigma, plastic, epsilon_p, sigma_p, plastic_p, tau);
  }

  return status;
}
