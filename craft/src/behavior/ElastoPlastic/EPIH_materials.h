/**************************************************************************/
/* H. Moulinec
   F. Silva
   LMA/CNRS
   july 12th 2011
   
   header file for Elastic Plastic Isotrope Hardening behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef EPIH_MATERIAL
#define EPIH_MATERIAL 4         /* Elastic Plastic Isotrope Hardening behavior */



#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>
#include <stdio.h>
#include <LE_materials.h>


/**************************************************************************/
enum hardening_type{
  WITHOUT_HARDENING     =0,
  LINEAR_HARDENING      =1,
  TABULATED_HARDENING   =2,
};

typedef struct {
  double ys;
} wo_hardening_param;

typedef struct {
  double ys;
  double H;
} linear_hardening_param;

typedef struct {
  int Nt;       /* number of points on which yield stress is tabulated */
  double *p;    /* tabulated equ. plastic strains */
  double *s0;   /* tabulated plastic stresses */
  double *s0_3mu; /* tabulated plastic stresses plus 3*mu*p */
  double *s0_3mup;/* tabulated plastic stresses plus 3*mu*mu0/(mu+mu0)*p */
} tab_hardening_param;

/*------------------------------------------------------------------------*/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */

  /* plasticity parameters */
  int hardening;
  union {
    wo_hardening_param* whp;
    linear_hardening_param* lhp;
    tab_hardening_param* thp;
    } hp;
} EPIH_param;

/*------------------------------------------------------------------------*/
int read_hardening( FILE *f, EPIH_param *x);
int read_parameters_EPIH( FILE *f, void *p );
int print_parameters_EPIH( FILE *f, void *p , int flag);

int allocate_parameters_EPIH( void **p );
int deallocate_parameters_EPIH( void *p );
int copy_parameters_EPIH( void *param_dest, void *param_src);


int behavior_EPIH(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt);

int allocate_variables_EPIH( int N, Variables *sv, void *param);
int extrapolate_variables_EPIH(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3) ;
int load_Cmatrix_EPIH(void *param, double C[6][6]);
int solve_spc0e_EPIH( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double (*tau)[6]
		    );
/**************************************************************************/
#endif
/**************************************************************************/




