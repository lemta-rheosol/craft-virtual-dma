#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <EPP_materials.h>
#include <variables.h>

#include <utils.h>
/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage Elastic Perfectly Plastic behavior

*/
/*######################################################################*/
int read_parameters_EPP( FILE *f, void *p ) {

  EPP_param *x;
  
  int status;

  x = (EPP_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->ys));
  if (status!=1) return -1;



  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );


  return 0;
}
/*######################################################################*/
int print_parameters_EPP( FILE *f, void *p , int flag ) {
  int status;
  status=0;

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((EPP_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((EPP_param *)p)->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lam� coef.  = %lf  ",(((EPP_param *)p)->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lam� coef. = %lf  ",(((EPP_param *)p)->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(((EPP_param *)p)->k));
    fprintf(f,"\n");
    fprintf(f,"# yield stress = %lf  ",(((EPP_param *)p)->ys));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"E  = %lf  ",(((EPP_param *)p)->E));
    fprintf(f,"nu = %lf  ",(((EPP_param *)p)->nu));
    fprintf(f,"1st Lam� coef.  = %lf  ",(((EPP_param *)p)->lb));
    fprintf(f,"2d Lam� coef. = %lf  ",(((EPP_param *)p)->mu));
    fprintf(f,"bulk modulus  = %lf  ",(((EPP_param *)p)->k));
    fprintf(f,"yield stress = %lf  ",(((EPP_param *)p)->ys));
  }
  
  return status;
}

/*######################################################################*/
int allocate_parameters_EPP( void **p ) {

  EPP_param *x;
  int status;



  status=0;

  x=(EPP_param *)malloc(sizeof(EPP_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_EPP( void *p ) {

  int status;

  status=0;

  free(p);
  
  return status;

}
/*######################################################################*/
int copy_parameters_EPP( void *param_dest, void *param_src) {

  *((EPP_param *)param_dest) = *((EPP_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_EPP( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt){
  int status;
  
  EPP_param *p;

  double (*epsilon)[6];
  double (*sigma)[6];
  double (*epsilon_p)[6];
  double (*sigma_p)[6];

  int N;

  int epp3d(
	    double k,         /* bulk modulus */
	    double mu,        /* 2d Lam� coefficient */
	    double ys,        /* yield stress */
	    int N,            /* number of points on which to apply constutive law */
	    double e[N][6],   /* strain */
	    double s[N][6],   /* stress */
	    double ep[N][6],  /* strain at previous loading step */
	    double sp[N][6]   /* stress at previous loading step */
	    );
  
  int epp3d_f_(
	    double *k,         /* bulk modulus */
	    double *mu,        /* 2d Lam� coefficient */
	    double *ys,        /* yield stress */
	    int *N,            /* number of points on which to apply constutive law */
	    double *e,   /* strain */
	    double *s,   /* stress */
	    double *ep,  /* strain at previous loading step */
	    double *sp   /* stress at previous loading step */
	    );
  N=sv->np;
  status=0;

  p = (EPP_param *)param;

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  status = epp3d( p->k, p->mu, p->ys, N, epsilon, sigma, epsilon_p, sigma_p);

  //status = epp3d_f_( &p->k, &p->mu, &p->ys, &N, epsilon, sigma, prev_epsilon, sv->sigma_p);

  /*
  status = epp3d( p->k, p->mu, p->ys, N, 
		  (double (*)[6]) sv->vars[0].values,
		  (double (*)[6]) sv->vars[1].values,
		  (double (*)[6]) sv->vars[2].values,
		  (double (*)[6]) sv->vars[3].values
		  		  );
  */

  return status;
}

/*######################################################################*/
int  allocate_variables_EPP( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;


  sv->nvar=4;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_EPP( 
				    void *param,
				    Euler_Angles orientation,
				    Variables *sv,
				    double t1, double t2, double t3) {
  int status;
  double (*epsilon)[6];
  double (*epsilon_p)[6];

  double (*sigma)[6];
  double (*sigma_p)[6];

  int N;
  int i,j;

  status=0;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);

  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  sigma_p = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);
  
  extrapol2( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );

  /* sigma -> sigma_p */
  N = sv->np;
#pragma omp parallel for \
  default(none)		 \
  shared(N)		 \
  private(i,j)		 \
  shared(sigma,sigma_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }


  return status;
}
/*######################################################################*/
int load_Cmatrix_EPP(void *param, double C[6][6])
{

int i,j;
EPP_param p;

p = *((EPP_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
} 
/*######################################################################*/
