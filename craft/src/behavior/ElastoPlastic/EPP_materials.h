/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   header file for Elastic Perfectly Plastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef EPP_MATERIAL
#define EPP_MATERIAL 2              /* Elastic Perfectly Plastic behavior */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */

  /* plasticity parameters */
  double ys;    /* yield stress         */
} EPP_param;


/*------------------------------------------------------------------------*/
int read_parameters_EPP( FILE *f, void *p );
int print_parameters_EPP( FILE *f, void *p , int flag);

int allocate_parameters_EPP( void **p );
int deallocate_parameters_EPP( void *p );
int copy_parameters_EPP( void *param_dest, void *param_src);


int behavior_EPP( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int allocate_variables_EPP( int N, Variables *sv, void *param);

int extrapolate_variables_EPP( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       ) ;
int load_Cmatrix_EPP(void *param, double C[6][6]);
/**************************************************************************/
#endif
/**************************************************************************/




