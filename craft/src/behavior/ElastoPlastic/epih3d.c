/******************/
/* Herv� Moulinec
   F. Silva
   LMA/CNRS

   july 12th 2011

   Elastic-Plastic-Isotrope Hardening constitutive law in 3D.
   Generic interface for perfectly plastic, linear and tabulated hardening cases.
   Radial return algorithm.

   EPIH constitutive law is applied on N points.

*/
/*******************************************************************************/
#include <math.h>
#include <EPIH_materials.h>
#ifdef _OPENMP
#include <omp.h>
#endif

int epih3d_without_hardening(
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    wo_hardening_param *whp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double (*e)[6],   /* strain */
    double (*s)[6],   /* stress */
    double (*ep)[6],  /* strain at previous loading step */
    double (*sp)[6]   /* stress at previous loading step */
    ) {
  int i,j;
  double buff;
  double one_third=1./3.;
  double seq, coef;

#pragma omp parallel for \
  default(none)     \
  private(i,j, buff, seq, coef)      \
  shared(N)          \
  shared(k,mu, whp)        \
  shared(e,s,ep,sp)        \
  shared(one_third)
  for(i=0; i<N; i++) {
    /* deviatoric part of elastic trial */
    for(j=0;j<6;j++) {
      s[i][j] = sp[i][j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    buff = - one_third * ( s[i][0]+s[i][1]+s[i][2] );
    for(j=0;j<3;j++) {
      s[i][j] += buff;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( s[i][0]*s[i][0] + s[i][1]*s[i][1] + s[i][2]*s[i][2] ) +
         3.  * ( s[i][3]*s[i][3] + s[i][4]*s[i][4] + s[i][5]*s[i][5] )
    );
    /* radial return coef */
    if ( seq > whp->ys ) {
      coef = whp->ys / seq;
    }
    else {
      coef = 1.;
    }
    /* radial return is applied . One has to add spherical part of stress. */
    buff = k*(e[i][0]+e[i][1]+e[i][2]);
    s[i][0] = coef * s[i][0] + buff;
    s[i][1] = coef * s[i][1] + buff;
    s[i][2] = coef * s[i][2] + buff;
    s[i][3] = coef * s[i][3] ;
    s[i][4] = coef * s[i][4] ;
    s[i][5] = coef * s[i][5] ;
  }

  return 0;
}

int epih3d_linear_hardening(
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    linear_hardening_param *lhp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double (*e)[6],   /* strain */
    double (*s)[6],   /* stress */
    double *p,        /* equivalent plastic strain */
    double (*ep)[6],  /* strain at previous loading step */
    double (*sp)[6],  /* stress at previous loading step */
    double *pp        /* equivalent plastic strain at previous loading step */
    ) {
  int i,j;
  double buff;
  double one_third=1./3.;
  double inv_H3mu;
  double seq, coef, s0;

  inv_H3mu = 1./( lhp->H + 3.*mu );

#pragma omp parallel for \
  default(none)     \
  private(i,j, buff, seq, coef, s0)      \
  shared(N)          \
  shared(k, mu, lhp)        \
  shared(e,s,ep,sp,p,pp)        \
  shared(one_third, inv_H3mu)
  for(i=0; i<N; i++) {
    /* deviatoric part of elastic trial */
    for(j=0;j<6;j++) {
      s[i][j] = sp[i][j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    buff = - one_third * ( s[i][0]+s[i][1]+s[i][2] );
    for(j=0;j<3;j++) {
      s[i][j] += buff;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( s[i][0]*s[i][0] + s[i][1]*s[i][1] + s[i][2]*s[i][2] ) +
         3.  * ( s[i][3]*s[i][3] + s[i][4]*s[i][4] + s[i][5]*s[i][5] )
    );
    s0 = lhp->ys + lhp->H * pp[i];
    /* radial return coef */
    if ( seq > s0 ) {
      p[i] = pp[i] + ( seq-s0 ) * inv_H3mu;
      coef = ( lhp->ys + lhp->H * p[i] ) / seq;
    }
    else {
      p[i] = pp[i];
      coef = 1.;
    }
    /* radial return is applied . One has to add spherical part of stress. */
    buff = k*(e[i][0]+e[i][1]+e[i][2]);
    s[i][0] = coef * s[i][0] + buff;
    s[i][1] = coef * s[i][1] + buff;
    s[i][2] = coef * s[i][2] + buff;
    s[i][3] = coef * s[i][3] ;
    s[i][4] = coef * s[i][4] ;
    s[i][5] = coef * s[i][5] ;
  }

  return 0;
}

int epih3d_tab_hardening(
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    tab_hardening_param *thp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double e[N][6],   /* strain */
    double s[N][6],   /* stress */
    double p[N],      /* equivalent plastic strain */
    double ep[N][6],  /* strain at previous loading step */
    double sp[N][6],  /* stress at previous loading step */
    double pp[N]      /* equivalent plastic strain at previous loading step */
    ) {
  int i,j,it;
  double buff;
  double one_third=1./3.;
  double mu3 = 3.*mu;
  double seq, coef, s0, X;

#pragma omp parallel for \
  default(none)     \
  private(i,j, buff, seq, coef, s0, X, it)      \
  shared(N)          \
  shared(k, mu, thp)        \
  shared(e,s,ep,sp,p,pp)        \
  shared(one_third, mu3)
  for(i=0; i<N; i++) {
    /* deviatoric part of elastic trial */
    for(j=0;j<6;j++) {
      s[i][j] = sp[i][j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    buff = - one_third * ( s[i][0]+s[i][1]+s[i][2] );
    for(j=0;j<3;j++) {
      s[i][j] += buff;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( s[i][0]*s[i][0] + s[i][1]*s[i][1] + s[i][2]*s[i][2] ) +
         3.  * ( s[i][3]*s[i][3] + s[i][4]*s[i][4] + s[i][5]*s[i][5] )
    );

    /* Apply tabulated law to get sigma_0(pp[i]). */
    /* Could be stored in order not to recompute it at each iteration.*/
    it = -1;
    do {
      it ++;
    } while((it<thp->Nt-2) && (pp[i]>thp->p[it+1]));
    coef = ( thp->s0[it+1] - thp->s0[it] ) / ( thp->p[it+1] - thp->p[it] );
    s0 = thp->s0[it] + coef * ( pp[i] - thp->p[it] );

    /* radial return coef */
    if ( seq > s0 ) {
      X = seq+mu3*pp[i];
      /* Look for the usable range starting from the one where pp[i] is. */
      it --;
      do {
        it ++;
      } while((it<thp->Nt-2) && (X>thp->s0_3mu[it+1]));
      /* Inverting  sigma_0(p)+3mu*p = seq+3mu*pp */
      /* s0_t contains the tabulated function sigma_0(p)+3*mu*p */
      coef = ( thp->p[it+1] - thp->p[it] ) / ( thp->s0_3mu[it+1] - thp->s0_3mu[it] );
      p[i] = thp->p[it] + coef * ( X - thp->s0_3mu[it] );
      coef = ( X-mu3*p[i] ) / seq;
    }
    else {
      p[i] = pp[i];
      coef = 1.;
    }

    /* radial return is applied . One has to add spherical part of stress. */
    buff = k*(e[i][0]+e[i][1]+e[i][2]);
    s[i][0] = coef * s[i][0] + buff;
    s[i][1] = coef * s[i][1] + buff;
    s[i][2] = coef * s[i][2] + buff;
    s[i][3] = coef * s[i][3] ;
    s[i][4] = coef * s[i][4] ;
    s[i][5] = coef * s[i][5] ;
  }

  return 0;
}

int epih3d_spc0e_without_hardening(
    double k0,         /* reference material bulk modulus */
    double mu0,        /* reference material 2d Lam� coefficient */
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    wo_hardening_param *whp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double (*e)[6],   /* strain */
    double (*s)[6],   /* stress */
    double (*ep)[6],  /* strain at previous loading step */
    double (*sp)[6],  /* stress at previous loading step */
    double (*tau)[6]  /* right-hand side term of equation sigma+c0:epsilon = tau */
    ) {
  int i,j;
  double one_third=1./3.;
  double st[6];
  double seq, coef, buf, trtau, coef_trs, coef_tre, invmumu0, inv2mu0;

  invmumu0 = 1./(mu+mu0);
  inv2mu0 = 0.5/mu0;
  coef_trs = one_third * k / ( k + k0 );
  coef_tre = one_third *( one_third / ( k + k0 ) - inv2mu0 );

#pragma omp parallel for \
  default(none)     \
  private(i,j, st, seq, coef, buf, trtau) \
  shared(N,stderr)          \
  shared(mu, whp, mu0)        \
  shared(e,s,ep,sp,tau)        \
  shared(one_third, coef_trs, coef_tre, invmumu0, inv2mu0)
  for(i=0; i<N; i++) {
    trtau = tau[i][0]+tau[i][1]+tau[i][2];

    /* elastic trial */
    for(j=0;j<6;j++) {
      st[j] = invmumu0 * ( mu0 * ( sp[i][j] - 2.*mu*ep[i][j] ) + mu*tau[i][j] );
    }
    /* deviatoric part of elastic trial */
    coef = -one_third * ( st[0] + st[1] + st[2] );
    for(j=0;j<3;j++) {
      st[j] += coef;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( st[0]*st[0] + st[1]*st[1] + st[2]*st[2] ) +
         3.  * ( st[3]*st[3] + st[4]*st[4] + st[5]*st[5] )
    );
    /* radial return coef */
    if ( seq > whp->ys ) {
      coef = whp->ys / seq;
    }
    else {
      coef = 1.;
    }
    /* radial return is applied. */
    for(j=0;j<6;j++) {
      /* tau and s may point to the same memory space */
      /* this is the case in the current lagrangian scheme implementation */
      buf = tau[i][j];
      s[i][j] = coef * st[j];                    /* deviatoric */      
      e[i][j] = inv2mu0 * ( buf-s[i][j] ); /* not deviatoric */
    }
    /* spherical part of stress */
    coef = coef_trs * trtau;
    for(j=0;j<3;j++) {
      s[i][j] += coef;
    }
    /* spherical part of strain */
    coef = coef_tre * trtau;
    for(j=0;j<3;j++) {
      e[i][j] += coef;
    }
  }

  return 0;
}

int epih3d_spc0e_linear_hardening(
    double k0,         /* reference material bulk modulus */
    double mu0,        /* reference material 2d Lam� coefficient */
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    linear_hardening_param *lhp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double (*e)[6],   /* strain */
    double (*s)[6],   /* stress */
    double *p,        /* equivalent plastic strain */
    double (*ep)[6],  /* strain at previous loading step */
    double (*sp)[6],  /* stress at previous loading step */
    double *pp,       /* equivalent plastic strain at previous loading step */
    double (*tau)[6]  /* right-hand side term of equation sigma+c0:epsilon = tau */
    ) {
  int i,j;
  double one_third=1./3.;
  double st[6];
  double seq, s0, buf, trtau, coef, coef_tre, coef_trs, invmumu0, inv2mu0, invH3mup;

  invmumu0 = 1./(mu+mu0);
  inv2mu0 = 0.5/mu0;
  invH3mup = 1./( lhp->H + 3.*mu*mu0*invmumu0 );
  coef_trs = one_third * k / ( k + k0 );
  coef_tre = one_third *( one_third / ( k + k0 ) - inv2mu0 );

#pragma omp parallel for \
  default(none)     \
  private(i,j, st, seq, s0, buf, trtau, coef) \
  shared(N)          \
  shared(mu, lhp, mu0)        \
  shared(e,s,ep,sp,p,pp,tau)        \
  shared(one_third, coef_tre, coef_trs, invmumu0, inv2mu0, invH3mup)
  for(i=0; i<N; i++) {
    trtau = tau[i][0]+tau[i][1]+tau[i][2];
    /* elastic trial */
    for(j=0;j<6;j++) {
      st[j] = invmumu0 * ( mu0 * ( sp[i][j] - 2.*mu*ep[i][j] ) + mu*tau[i][j] );
    }
    /* deviatoric part of elastic trial */
    coef = -one_third * ( st[0] + st[1] + st[2] );
    for(j=0;j<3;j++) {
      st[j] += coef;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( st[0]*st[0] + st[1]*st[1] + st[2]*st[2] ) +
         3.  * ( st[3]*st[3] + st[4]*st[4] + st[5]*st[5] )
    );
    /* radial return coef */
    s0 = lhp->ys + lhp->H * pp[i];
    if ( seq > s0 ) {
      p[i] = pp[i] + (seq-s0)*invH3mup;
      coef = ( lhp->ys + lhp->H * p[i] ) / seq;
    }
    else {
      p[i] = pp[i];
      coef = 1.;
    }
    /* radial return is applied. */
    for(j=0;j<6;j++) {
      /* tau and s may point to the same memory space */
      /* this is the case in the current lagrangian scheme implementation */
      buf = tau[i][j];
      s[i][j] = coef * st[j];                    /* deviatoric */
      e[i][j] = inv2mu0 * ( buf-s[i][j] ); /* not deviatoric */
    }
    /* full spherical part of stress */
    coef = coef_trs * trtau;
    for(j=0;j<3;j++) {
      s[i][j] += coef;
    }
    /* missing spherical part of strain */
    coef = coef_tre * trtau;
    for(j=0;j<3;j++) {
      e[i][j] += coef;
    }
  }

  return 0;
}

int epih3d_spc0e_tab_hardening(
    double k0,         /* reference material bulk modulus */
    double mu0,        /* reference material 2d Lam� coefficient */
    double k,         /* bulk modulus */
    double mu,        /* 2d Lam� coefficient */
    tab_hardening_param *thp, /* structure containing the hardening data */
    int N,            /* number of points on which to apply constutive law */
    double (*e)[6],   /* strain */
    double (*s)[6],   /* stress */
    double *p,        /* equivalent plastic strain */
    double (*ep)[6],  /* strain at previous loading step */
    double (*sp)[6],  /* stress at previous loading step */
    double *pp,       /* equivalent plastic strain at previous loading step */
    double (*tau)[6]  /* right-hand side term of equation sigma+c0:epsilon = tau */
    ) {
  int i,j,it;
  double one_third=1./3.;
  double st[6];
  double seq, X, s0, buf, trtau, coef, coef_trs, coef_tre, invmumu0, inv2mu0, mup3;

  invmumu0 = 1./(mu+mu0);
  inv2mu0 = 0.5/mu0;
  mup3 = 3.*mu*mu0*invmumu0;
  coef_trs = one_third * k / ( k + k0 );
  coef_tre = one_third *( one_third / ( k + k0 ) - inv2mu0 );

#pragma omp parallel for \
  default(none)     \
  private(i,j,it, st, seq, s0, buf, trtau, coef, X) \
  shared(N)          \
  shared(mu, thp, mu0, mup3)        \
  shared(e,s,p,ep,sp,pp,tau)        \
  shared(one_third, coef_trs, coef_tre, invmumu0, inv2mu0)
  for(i=0; i<N; i++) {
    trtau = tau[i][0]+tau[i][1]+tau[i][2];
    /* elastic trial */
    for(j=0;j<6;j++) {
      st[j] = invmumu0 * ( mu0 * ( sp[i][j] - 2.*mu*ep[i][j] ) + mu*tau[i][j] );
    }
    /* deviatoric part of elastic trial */
    coef = -one_third * ( st[0] + st[1] + st[2] );
    for(j=0;j<3;j++) {
      st[j] += coef;
    }
    /* equivalent stress of elastic trial */
    seq = sqrt(
         1.5 * ( st[0]*st[0] + st[1]*st[1] + st[2]*st[2] ) +
         3.  * ( st[3]*st[3] + st[4]*st[4] + st[5]*st[5] )
    );

    /* Apply tabulated law to get sigma_0(pp[i]). */
    /* Could be stored in order not to recompute it at each iteration.*/
    it = -1;
    do {
      it ++;
    } while((it<thp->Nt-2) && (pp[i]>thp->p[it+1]));
    coef = ( thp->s0[it+1] - thp->s0[it] ) / ( thp->p[it+1] - thp->p[it] );
    s0 = thp->s0[it] + coef * ( pp[i] - thp->p[it] );

    /* radial return coef */
    if ( seq > s0 ) {
      X = seq + mup3*pp[i];
      /* Look for the usable range starting from the one where pp[i] is. */
      it --;
      do {
        it ++;
      } while((it<thp->Nt-2) && (X>thp->s0_3mup[it+1]));
      /* Inverting  sigma_0(p)+3mup*p = seq+3mup*pp = X */
      /* s0_3mup contains the tabulated function sigma_0(p)+3*mup*p */
      coef = ( thp->p[it+1] - thp->p[it] ) / ( thp->s0_3mup[it+1] - thp->s0_3mup[it] );
      p[i] = thp->p[it] + coef * ( X - thp->s0_3mup[it] );
      coef = ( X-mup3*p[i] ) / seq;
    }
    else {
      coef = 1.;
    }
    /* radial return is applied. */
    for(j=0;j<6;j++) {
      /* tau and s may point to the same memory space */
      /* this is the case in the current lagrangian scheme implementation */
      buf = tau[i][j];
      s[i][j] = coef * st[j];                    /* deviatoric */
      e[i][j] = inv2mu0 * ( buf-s[i][j] ); /* not deviatoric */
    }
    /* spherical part of stress */
    coef = coef_trs * trtau;
    for(j=0;j<3;j++) {
      s[i][j] += coef;
    }
    /* spherical part of strain */
    coef = coef_tre * trtau;
    for(j=0;j<3;j++) {
      e[i][j] += coef;
    }
  }

  return 0;
}
