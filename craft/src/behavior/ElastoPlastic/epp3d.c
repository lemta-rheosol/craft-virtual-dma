/*******************************************************************************/
/* Herv� Moulinec
   LMA/CNRS

   october 5th 2009

   Elastic-Perfectly-Plastic constitutive law in 3D.

   Radial return algorithm.

   EPP constitutive law is applied on N points.

*/
/*******************************************************************************/
#include <math.h>
#ifdef _OPENMP
#include <omp.h>
#endif

int epp3d(
	  double k,         /* bulk modulus */
	  double mu,        /* 2d Lam� coefficient */
	  double ys,        /* yield stress */
	  int N,            /* number of points on which to apply constutive law */
	  double e[N][6],   /* strain */
	  double s[N][6],   /* stress */
	  double ep[N][6],  /* strain at previous loading step */
	  double sp[N][6]   /* stress at previous loading step */
	  ) {

  /*---------------------------------------------------------------------------*/  
  int i,j;
  double buff;
  double one_third=1./3.;
  int status;
  double seq, coef;

  /*---------------------------------------------------------------------------*/  
  status=0;

  /*---------------------------------------------------------------------------*/  
#pragma omp parallel for \
  default(none)		 \
  private(i,j, buff, seq, coef)			\
  shared(N)					\
  shared(k,mu,ys)				\
  shared(e,s,ep,sp)				\
  shared(one_third)
  for(i=0; i<N; i++) {

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  
    /* deviatoric part of elastic trial */    
    for(j=0;j<6;j++) {
      s[i][j] = sp[i][j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    /*
    s[i][0] = sp[i][0] + 2.*mu*(e[i][0]-ep[i][0]);
    s[i][1] = sp[i][1] + 2.*mu*(e[i][1]-ep[i][1]);
    s[i][2] = sp[i][2] + 2.*mu*(e[i][2]-ep[i][2]);
    s[i][3] = sp[i][3] + 2.*mu*(e[i][3]-ep[i][3]);
    s[i][4] = sp[i][4] + 2.*mu*(e[i][4]-ep[i][4]);
    s[i][5] = sp[i][5] + 2.*mu*(e[i][5]-ep[i][5]);
    */
    buff = - one_third * ( s[i][0]+s[i][1]+s[i][2] );

    for(j=0;j<3;j++) {
      s[i][j] += buff;
    }
    /*
    s[i][0] += buff;
    s[i][1] += buff;
    s[i][2] += buff;
    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  
    /* equivalent stress */
    seq = sqrt( 
	       1.5 * ( s[i][0]*s[i][0] + s[i][1]*s[i][1] + s[i][2]*s[i][2] ) +
	       3.  * ( s[i][3]*s[i][3] + s[i][4]*s[i][4] + s[i][5]*s[i][5] ) 
		);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  
    /* radial return coef */
    if ( seq > ys ) {
      coef = ys /seq;
    }
    else {
      coef = 1.;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  
    /* radial return is applied . One has to add spherical part of stress. */
    buff = k*(e[i][0]+e[i][1]+e[i][2]);
    s[i][0] = coef * s[i][0] + buff;
    s[i][1] = coef * s[i][1] + buff;
    s[i][2] = coef * s[i][2] + buff;
    s[i][3] = coef * s[i][3] ;
    s[i][4] = coef * s[i][4] ;
    s[i][5] = coef * s[i][5] ;
  }
/*---------------------------------------------------------------------------*/  
  return status;
}
