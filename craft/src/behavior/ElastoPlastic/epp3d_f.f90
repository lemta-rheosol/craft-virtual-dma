subroutine epp3d_f( k, mu, ys, N, e, s, ep, sp )
  implicit none
  real(kind=8) :: k,mu,ys
  integer :: N
  real(kind=8) :: e(6,N), s(6,N), ep(6,N), sp(6,N)

  integer i,j

  real(kind=8) seq, coef, buff

  !$OMP PARALLEL &
  !$OMP DEFAULT(NONE) &
  !$OMP SHARED(N) &
  !$OMP SHARED(k,mu,ys) &
  !$OMP SHARED(e,s,ep,sp) &
  !$OMP PRIVATE(i,j,seq,coef,buff)

  !$OMP DO
  do i = 1,N
     
     do j=1,6
        s(j,i)=sp(j,i)+2.d0*mu*(e(j,i)-ep(j,i))
     end do

     buff = -1.d0/3.d0*(s(1,i)+s(2,i)+s(3,i))

     do j = 1,3
        s(j,i) = s(j,i) + buff
     end do

     seq = sqrt( &
          1.5d0 * ( s(1,i)*s(1,i)+s(2,i)*s(2,i)+s(3,i)*s(3,i) ) + &
          3.d0  * ( s(4,i)*s(4,i)+s(5,i)*s(5,i)+s(6,i)*s(6,i) ) &
          )

     if (seq .gt. ys) then
        coef=ys/seq
     else
        coef = 1.d0
     end if

     do j=1,6
        s(j,i) = coef * s(j,i)
     end do
     
     buff = k*(e(1,i)+e(2,i)+e(3,i))
     do j=1,3
        s(j,i) = s(j,i) + buff
     end do

  end do
  !$OMP END DO
  !$OMP END PARALLEL
end subroutine epp3d_f
  
