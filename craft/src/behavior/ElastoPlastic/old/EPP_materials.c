#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <EPP_materials.h>
#include <variables.h>

#include <utils.h>
/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage Elastic Perfectly Plastic behavior

*/
/*######################################################################*/
int read_parameters_EPP( FILE *f, void *p ) {

  EPP_param *x;
  
  int status;

  x = (EPP_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->ys));
  if (status!=1) return -1;



  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );


  return 0;
}
/*######################################################################*/
int print_parameters_EPP( FILE *f, void *p , int flag ) {
  int status;
  status=0;

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((EPP_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((EPP_param *)p)->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lam� coef.  = %lf  ",(((EPP_param *)p)->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lam� coef. = %lf  ",(((EPP_param *)p)->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(((EPP_param *)p)->k));
    fprintf(f,"\n");
    fprintf(f,"# yield stress = %lf  ",(((EPP_param *)p)->ys));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"E  = %lf  ",(((EPP_param *)p)->E));
    fprintf(f,"nu = %lf  ",(((EPP_param *)p)->nu));
    fprintf(f,"1st Lam� coef.  = %lf  ",(((EPP_param *)p)->lb));
    fprintf(f,"2d Lam� coef. = %lf  ",(((EPP_param *)p)->mu));
    fprintf(f,"bulk modulus  = %lf  ",(((EPP_param *)p)->k));
    fprintf(f,"yield stress = %lf  ",(((EPP_param *)p)->ys));
  }
  
  return status;
}

/*######################################################################*/
int allocate_parameters_EPP( void **p ) {

  EPP_param *x;
  int status;



  status=0;

  x=(EPP_param *)malloc(sizeof(EPP_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_EPP( void *p ) {

  int status;

  status=0;

  free(p);
  
  return status;

}
/*######################################################################*/
int copy_parameters_EPP( void *param_dest, void *param_src) {

  *((EPP_param *)param_dest) = *((EPP_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_EPP( int N, 
		  void *param, Euler_Angles orientation, 
		  double epsilon[N][6], double sigma[N][6], 
		  double prev_epsilon[N][6],
		  void *state_variables,
		  double dt){
  int status;
  
  EPP_param *p;
  EPP_State_Variables *sv;

  int epp3d(
	    double k,         /* bulk modulus */
	    double mu,        /* 2d Lam� coefficient */
	    double ys,        /* yield stress */
	    int N,            /* number of points on which to apply constutive law */
	    double e[N][6],   /* strain */
	    double s[N][6],   /* stress */
	    double ep[N][6],  /* strain at previous loading step */
	    double sp[N][6]   /* stress at previous loading step */
	    );
  
  int epp3d_f_(
	    double *k,         /* bulk modulus */
	    double *mu,        /* 2d Lam� coefficient */
	    double *ys,        /* yield stress */
	    int *N,            /* number of points on which to apply constutive law */
	    double *e,   /* strain */
	    double *s,   /* stress */
	    double *ep,  /* strain at previous loading step */
	    double *sp   /* stress at previous loading step */
	    );

  
  status=0;
  p = (EPP_param *)param;
  sv = (EPP_State_Variables *)state_variables;
  

  status = epp3d( p->k, p->mu, p->ys, N, epsilon, sigma, prev_epsilon, sv->sigma_p);
  //status = epp3d_f_( &p->k, &p->mu, &p->ys, &N, epsilon, sigma, prev_epsilon, sv->sigma_p);

  return status;
}

/*######################################################################*/
int  allocate_state_variables_EPP( int N, void **state_variables, void *param) {

  EPP_State_Variables *sv;

  int status;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  sv = (EPP_State_Variables *)malloc(sizeof(*sv));
  if ( sv == (EPP_State_Variables *)NULL ) {
    status = -1;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  sv->epsilon = (double (*)[6])malloc(N*sizeof(*(sv->epsilon)));
  if ( sv->epsilon == (double (*)[6])NULL ) {
    status = -1;
  }
  
  sv->sigma = (double (*)[6])malloc(N*sizeof(*(sv->sigma)));
  if ( sv->sigma == (double (*)[6])NULL ) {
    status = -1;
  }
  
  sv->epsilon_p = (double (*)[6])malloc(N*sizeof(*(sv->epsilon_p)));
  if ( sv->epsilon_p == (double (*)[6])NULL ) {
    status = -1;
  }
  
  sv->sigma_p = (double (*)[6])malloc(N*sizeof(*(sv->sigma_p)));
  if ( sv->sigma_p == (double (*)[6])NULL ) {
    status = -1;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    *state_variables = (void *)sv;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}
/*######################################################################*/
int  deallocate_state_variables_EPP( int N, void **state_variables, void *param) {

  EPP_State_Variables *sv;

  sv = (EPP_State_Variables *)(*state_variables);

  free(sv->epsilon);
  free(sv->sigma);

  free(sv->epsilon_p);
  free(sv->sigma_p);

  free(sv);

  //  sv = (  EPP_State_Variables *) 0;

  return 0;
}
/*######################################################################*/
int  init_state_variables_EPP( int N, void **state_variables, void *param) {

  EPP_State_Variables *x;
  EPP_param *p;
  int i,j;

  
  x = (EPP_State_Variables *)*state_variables;
  p = (EPP_param *)param;


  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      x->sigma_p[i][j]=0.;
    }
  }

  return 0;
}

/*######################################################################*/
int extrapolate_state_variables_EPP( 
				    int N, 
				    void *param, Euler_Angles orientation, 
				    double epsilon[N][6], double sigma[N][6], 
				    double epsilon_p[N][6],
				    void *state_variables, 
				    double t1, double t2, double t3
				     ) {
  int i,j;
  EPP_State_Variables *sv;
  int status;

  status = 0;

  sv = (EPP_State_Variables *)state_variables;

  extrapol2( 6*N, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );

#pragma omp parallel for \
  default(none)		 \
  shared(N)		 \
  private(i,j)		 \
  shared(sigma,sv)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sv->sigma_p[i][j] = sigma[i][j];
    }
  }

  
  return status;
}
/*######################################################################*/
int load_Cmatrix_EPP(void *param, double C[6][6])
{

int i,j;
EPP_param p;

p = *((EPP_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
} 
/*######################################################################*/
void *ptr_state_variable_EPP( 
			     char *state_variable_name, 
			     void *state_variables, 
			     void *param,
			     int *type_of_variable,
			     int N,
			     int *releasable
			      ){
  
  EPP_State_Variables *sv;
  EPP_param *p;

  sv = (EPP_State_Variables *)state_variables;
  p = (EPP_param *)param;

  /*--------------------------------------------------------------------*/
  if ( strcasecmp_ignorespace(state_variable_name , "stress") == 0 ) {
    *type_of_variable = TENSOR2;
    *releasable=0;
    return (void *)(sv->sigma);
  }
  /*--------------------------------------------------------------------*/
  else if ( strcasecmp_ignorespace(state_variable_name , "strain") == 0 ) {
    *type_of_variable = TENSOR2;
    *releasable=0;
    return (void *)(sv->epsilon);
  }
  /*--------------------------------------------------------------------*/
  else if ( strcasecmp_ignorespace(state_variable_name , "equivalent strain") == 0 ) {
    double *es;
    int status;

    type_of_variable[0] = SCALAR;
    
    es = (double *)malloc(N*sizeof(*es));
    status=equivalent("strain", N, sv->epsilon, es);

    if(status!=0) {
      free(es);
      *releasable=0;
      es = (double *)0;
    }
    else{
      *releasable=1;
    }
    
    return (void *)(es);
  }
  /*--------------------------------------------------------------------*/
  else if ( strcasecmp_ignorespace(state_variable_name , "equivalent stress") == 0 ) {
    double *es;
    int status;

    type_of_variable[0] = SCALAR;
    
    es = (double *)malloc(N*sizeof(*es));

    status=equivalent("stress", N, sv->sigma, es);

    if(status!=0) {
      free(es);
      *releasable=0;
      es = (double *)0;
    }
    else{
      *releasable=1;
    }

    return (void *)(es);
  }
  /*--------------------------------------------------------------------*/
  else if ( strcasecmp_ignorespace(state_variable_name , "previousstress") == 0 ) {
    *type_of_variable = TENSOR2;
    *releasable=0;
    return (void *)(sv->sigma_p);
  }
  /*--------------------------------------------------------------------*/
  else if ( strcasecmp_ignorespace(state_variable_name , "previousstrain") == 0 ) {
    *type_of_variable = TENSOR2;
    *releasable=0;
    return (void *)(sv->epsilon_p);
  }
  /*--------------------------------------------------------------------*/
  else{
    *type_of_variable = UNKNOWN;
    *releasable=0;
    return (void *)0;
  }
  /*--------------------------------------------------------------------*/



}
