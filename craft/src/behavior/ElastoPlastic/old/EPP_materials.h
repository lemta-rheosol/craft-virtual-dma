/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   header file for Elastic Perfectly Plastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef EPP_MATERIAL
#define EPP_MATERIAL 2              /* Elastic Perfectly Plastic behavior */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */

  /* plasticity parameters */
  double ys;    /* yield stress         */
} EPP_param;


/*------------------------------------------------------------------------*/
typedef struct{
  double (*epsilon)[6];
  double (*sigma)[6];
  double (*epsilon_p)[6];
  double (*sigma_p)[6];
} EPP_State_Variables;



/*------------------------------------------------------------------------*/
int read_parameters_EPP( FILE *f, void *p );
int print_parameters_EPP( FILE *f, void *p , int flag);

int allocate_parameters_EPP( void **p );
int deallocate_parameters_EPP( void *p );
int copy_parameters_EPP( void *param_dest, void *param_src);


int behavior_EPP( int N, 
	      void *param, Euler_Angles orientation, 
	      double epsilon[N][6], double sigma[N][6], 
	      double previous_epsilon[N][6], 
		  void *state_variables,
		  double dt);

int allocate_state_variables_EPP( int N, void **state_variables, void *param);
int deallocate_state_variables_EPP( int N, void **state_variables, void *param);
int init_state_variables_EPP( int N, void **state_variables, void *param) ;
int extrapolate_state_variables_EPP( 
				    int N, 
				    void *param, Euler_Angles orientation, 
				    double epsilon[N][6], double sigma[N][6], 
				    double epsilon_p[N][6],
				    void *state_variables, 
				    double t1, double t2, double t3
				     ) ;
int load_Cmatrix_EPP(void *param, double C[6][6]);
void *ptr_state_variable_EPP( 
			     char *state_variable_name, 
			     void *state_variables, 
			     void *param,
			     int *type_of_variable,
			     int N,
			     int *releasable
			      );
/**************************************************************************/
#endif
/**************************************************************************/




