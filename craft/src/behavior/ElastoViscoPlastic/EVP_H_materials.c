#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>

#include <EVP_H_materials.h>
#include <variables.h>

#include <utils.h>
static int compute_backstress( Variables *sv, EVP_H_param *param, Variable *var);
/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage power law Elastic Visco-Plastic behavior
   with linear hardening

*/
/*######################################################################*/
int read_parameters_EVP_H( FILE *f, void *p ) {
  int status;
  EVP_H_param *x;
  x = (EVP_H_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->eta));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->npow));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->H));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->sy));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );
  return 0;
}

/*######################################################################*/
int print_parameters_EVP_H( FILE *f, void *p , int flag ) {
  int status;
  status=0;

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((EVP_H_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((EVP_H_param *)p)->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lam� coef.  = %lf  ",(((EVP_H_param *)p)->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lam� coef. = %lf  ",(((EVP_H_param *)p)->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(((EVP_H_param *)p)->k));
    fprintf(f,"\n");
    fprintf(f,"# eta  = %lf  ",(((EVP_H_param *)p)->eta));
    fprintf(f,"\n");
    fprintf(f,"# power law exponent = %lf  ",(((EVP_H_param *)p)->npow));
    fprintf(f,"\n");
    fprintf(f,"# linear hardening coef. = %lf  ",(((EVP_H_param *)p)->H));
    fprintf(f,"\n");
    fprintf(f,"# sigma_y  = %lf  ",(((EVP_H_param *)p)->sy));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"E  = %lf  ",(((EVP_H_param *)p)->E));
    fprintf(f,"nu = %lf  ",(((EVP_H_param *)p)->nu));
    fprintf(f,"1st Lam� coef.  = %lf  ",(((EVP_H_param *)p)->lb));
    fprintf(f,"2d Lam� coef. = %lf  ",(((EVP_H_param *)p)->mu));
    fprintf(f,"bulk modulus  = %lf  ",(((EVP_H_param *)p)->k));
    fprintf(f,"eta = %lf  ",(((EVP_H_param *)p)->eta));
    fprintf(f,"power law exponent= %lf  ",(((EVP_H_param *)p)->npow));
    fprintf(f,"linear hardening coef. = %lf  ",(((EVP_H_param *)p)->H));
    fprintf(f,"sigma_y  = %lf  ",(((EVP_H_param *)p)->sy));
  }
  
  return status;
}

/*######################################################################*/
int allocate_parameters_EVP_H( void **p ) {
  EVP_H_param *x;
  int status;
  status=0;
  x=(EVP_H_param *)malloc(sizeof(EVP_H_param));

  *p = (void *)x;
  if( p == (void *)0 ) {
    status=-1;
  }

  return status;
}

/*######################################################################*/
int deallocate_parameters_EVP_H( void *p ) {
  int status;
  status=0;
  free(p);
  return status;
}

/*######################################################################*/
int copy_parameters_EVP_H( void *param_dest, void *param_src) {
  *((EVP_H_param *)param_dest) = *((EVP_H_param *)param_src);
  return 0;
}

/*######################################################################*/
int behavior_EVP_H(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt){

  int N;
  EVP_H_param *p;
  double (*epsilon)[6];
  double (*sigma)[6];
  double (*epsilon_p)[6];
  double (*sigma_p)[6];

  N = sv->np;

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  
  p = (EVP_H_param *)param;


  void hm_evp_klh3d(
        double lb,        /* 1st Lam� coefficient */
        double mu,        /* 2d Lam� coefficient */
        double eta,       /* yield stress */
        double npow,      /* power law exponent */
        double Hecl,      /* linear hardening coefficient */
        double sy,        /*sigma y coef. */
        double dt,        /* time step */
        int N,            /* Number of points */
        double (*e)[6],         /* strain at current loading step */
        double (*s)[6],         /* stress at current loading step */
        double (*ep)[6],        /* strain at previous loading step */
        double (*sp)[6]         /* stress at previous loading step */
        );

  hm_evp_klh3d( (p->lb), (p->mu), (p->eta), (p->npow), (p->H), (p->sy), 
          dt, N,
          epsilon, sigma, epsilon_p, sigma_p);
  return 0;
}

/*######################################################################*/
int  allocate_variables_EVP_H( int N, Variables *sv, void *param) {
  int status;
  //  int scalar_t[3]={SCALAR,0,0};
  int tensor_t[3]={TENSOR2,0,0};

  status=0;
  sv->np = N;
  sv->nvar = 4;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  return status;
}

/*######################################################################*/
int extrapolate_variables_EVP_H(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3) {
  int status;
  double (*epsilon) [6];
  double (*epsilon_p) [6];
  Variable *sigma;
  Variable *sigma_p;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma   = ptr_variable("stress", sv, param);
  sigma_p = ptr_variable("previous stress", sv, param);

  extrapol2( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

  status = copy_variable_values( sigma_p, sigma, sv->np);
  return status;
}

/*######################################################################*/
int load_Cmatrix_EVP_H(void *param, double C[6][6])
{

int i,j;
EVP_H_param p;

p = *((EVP_H_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
}

/*######################################################################*/
Variable *ptr_variable_EVP_H( char *var_name, Variables *sv, void *p ) {

  Variable *var;
  int status;

  EVP_H_param *param=(EVP_H_param *)p;
  /*--------------------------------------------------------------------*/
  /* call generioc ptr_variable method : */
  var = ptr_variable( var_name, sv , p );

  /* if it succeeded in finding the variable, return it */
  if(var != (Variable *)NULL) {
    return var;
  }

  /*--------------------------------------------------------------------*/
  /* if it doesn't, enter in specialized research of ptr variable : */
  if ( strcasecmp_ignorespace(var_name , "backstress") == 0 ){
    int type[3]={TENSOR2, 0, 0};


    /* compute gamma dot */
    var = (Variable *)malloc( sizeof(*var) );
    if ( var == (Variable *)0 ) {
      fprintf(stderr,"craft: error in ptr_variable_EVP_H (malloc failed)\n");
      exit(EXIT_FAILURE);
    }
    
    status=allocate_variable( var, "backstress", sv->np, type, 1);
    if(status!=0) {
      free(var);
      return (Variable *)0;
    }

    status=compute_backstress( sv, param, var );
    return var;

  }

  return (Variable *)0;


  /*--------------------------------------------------------------------*/
}
/**************************************************************************************/
static int compute_backstress( Variables *sv, EVP_H_param *param, Variable *var){
  double (*e)[6];
  double (*s)[6];


  double (*X)[6];
  int i,j;
  double trs;
  
  e = (double (*)[6])ptr_variable( "strain", sv, (void *)param )->values;
  if (e==(double (*)[6])0) {
    return -1;
  }

  s = (double (*)[6])ptr_variable( "stress", sv, (void *)param )->values;
  if (e==(double (*)[6])0) {
    return -1;
  }

  X =  (double (*)[6])var->values;

  for (i=0; i< sv->np; i++) {

    for (j=0; j<6; j++) {
      X[i][j] =  param->H/(3.*param->mu)*(2.*param->mu*e[i][j]-s[i][j]);
    }

    trs=(X[i][0]+X[i][1]+X[i][2])/3.;

    for (j=0; j<3; j++) {
      X[i][j] -=  trs;
    }
    /*
    printf("e="); for (j=0; j<6; j++) {    printf("%f ",e[i][j]); }    printf("\n");
    printf("s="); for (j=0; j<6; j++) {    printf("%f ",s[i][j]); }    printf("\n");
    printf("X="); for (j=0; j<6; j++) {    printf("%f ",X[i][j]); }    printf("\n");
    */

  }

  return 0;

}
