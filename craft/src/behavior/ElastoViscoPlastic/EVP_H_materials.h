/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   january 14 2010
   
   header file for power law Elastic Visco-Plastic behavior
   with linear hardening

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef EVP_H_MATERIAL
#define EVP_H_MATERIAL 41              /* Elastic Visco-Plastic behavior with linear hardening */



#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */
  /* plasticity parameters */
  double eta;    /* "yield stress"         */
  /* power law exponent */
  double npow;
  /* linear hardening coefficient */
  double H;
  /* sigma y */
  double sy;
} EVP_H_param;

/*------------------------------------------------------------------------*/
int read_parameters_EVP_H( FILE *f, void *p );
int print_parameters_EVP_H( FILE *f, void *p , int flag);

int allocate_parameters_EVP_H( void **p );
int deallocate_parameters_EVP_H( void *p );
int copy_parameters_EVP_H( void *param_dest, void *param_src);

int behavior_EVP_H(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt);

int allocate_variables_EVP_H( int N, Variables *sv, void *param);
int extrapolate_variables_EVP_H(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3);
int load_Cmatrix_EVP_H(void *param, double C[6][6]);

Variable *ptr_variable_EVP_H( char *var_name, Variables *sv, void *p );
/**************************************************************************/
#endif
/**************************************************************************/

