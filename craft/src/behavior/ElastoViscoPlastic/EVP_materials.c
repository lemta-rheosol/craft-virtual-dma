#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>

#include <EVP_materials.h>
#include <variables.h>

#include <utils.h>
/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage power law Elastic Visco-Plastic behavior

*/
/*######################################################################*/
int read_parameters_EVP( FILE *f, void *p ) {
  int status;
  EVP_param *x;
  x = (EVP_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->ys));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->npow));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );
  return 0;
}

/*######################################################################*/
int print_parameters_EVP( FILE *f, void *p , int flag ) {
  int status;
  status=0;

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((EVP_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((EVP_param *)p)->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lam� coef.  = %lf  ",(((EVP_param *)p)->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lam� coef. = %lf  ",(((EVP_param *)p)->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(((EVP_param *)p)->k));
    fprintf(f,"\n");
    fprintf(f,"# yield stress = %lf  ",(((EVP_param *)p)->ys));
    fprintf(f,"\n");
    fprintf(f,"# power law exponent = %lf  ",(((EVP_param *)p)->npow));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"E  = %lf  ",(((EVP_param *)p)->E));
    fprintf(f,"nu = %lf  ",(((EVP_param *)p)->nu));
    fprintf(f,"1st Lam� coef.  = %lf  ",(((EVP_param *)p)->lb));
    fprintf(f,"2d Lam� coef. = %lf  ",(((EVP_param *)p)->mu));
    fprintf(f,"bulk modulus  = %lf  ",(((EVP_param *)p)->k));
    fprintf(f,"yield stress = %lf  ",(((EVP_param *)p)->ys));
    fprintf(f,"power law exponent= %lf  ",(((EVP_param *)p)->npow));
  }
  
  return status;
}

/*######################################################################*/
int allocate_parameters_EVP( void **p ) {
  EVP_param *x;
  int status;
  status=0;
  x=(EVP_param *)malloc(sizeof(EVP_param));

  *p = (void *)x;
  if( p == (void *)0 ) {
    status=-1;
  }

  return status;
}

/*######################################################################*/
int deallocate_parameters_EVP( void *p ) {
  int status;
  status=0;
  free(p);
  return status;
}

/*######################################################################*/
int copy_parameters_EVP( void *param_dest, void *param_src) {
  *((EVP_param *)param_dest) = *((EVP_param *)param_src);
  return 0;
}

/*######################################################################*/
int behavior_EVP(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt){
  /*--------------------------------------------------------------------*/

  int N;
  EVP_param *p;

  double (*epsilon)[6];
  double (*sigma)[6];
  double (*epsilon_p)[6];
  double (*sigma_p)[6];


  void evp3d_c(
        double lb,        /* 1st Lam� coefficient */
        double mu,        /* 2d Lam� coefficient */
        double ys,        /* yield stress */
        double npow,      /* power law exponent */
        double dt,        /* time step */
        int N,            /* Number of points */
        double *e,         /* strain at current loading step */
        double *s,         /* stress at current loading step */
        double *ep,        /* strain at previous loading step */
        double *sp         /* stress at previous loading step */
        );
  /*--------------------------------------------------------------------*/

  N = sv->np;

  p = (EVP_param *)param;

  

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  
  evp3d_c( (p->lb), (p->mu), (p->ys), (p->npow), 
           dt, 
           N,
           &(epsilon[0][0]), 
           &(sigma[0][0]),
           &(epsilon_p[0][0]), 
           &(sigma_p[0][0])
           );

  
  return 0;
}

/*######################################################################*/
int allocate_variables_EVP( int N, Variables *sv, void *param) {
  int status;
  //  int scalar_t[3]={SCALAR,0,0};
  int tensor_t[3]={TENSOR2,0,0};

  status=0;
  sv->np = N;
  sv->nvar = 4;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  return status;
}

/*######################################################################*/
int extrapolate_variables_EVP(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3) {
  int status;
  double (*epsilon) [6];
  double (*epsilon_p) [6];
  Variable *sigma;
  Variable *sigma_p;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma   = ptr_variable("stress", sv, param);
  sigma_p = ptr_variable("previous stress", sv, param);

  extrapol2( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

  status = copy_variable_values( sigma_p, sigma, sv->np);
  return status;
}

/*######################################################################*/
int load_Cmatrix_EVP(void *param, double C[6][6])
{

int i,j;
EVP_param p;

p = *((EVP_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
}

