/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   december 18th 2009
   
   header file for power law Elastic Visco-Plastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef EVP_MATERIAL
#define EVP_MATERIAL 40              /* Elastic Visco-Plastic behavior */



#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */
  /* plasticity parameters */
  double ys;    /* yield stress         */
  /* power law exponent */
  double npow;
} EVP_param;

/*------------------------------------------------------------------------*/
int read_parameters_EVP( FILE *f, void *p );
int print_parameters_EVP( FILE *f, void *p , int flag);

int allocate_parameters_EVP( void **p );
int deallocate_parameters_EVP( void *p );
int copy_parameters_EVP( void *param_dest, void *param_src);

int behavior_EVP( void *param, Euler_Angles orientation, Variables *sv, double dt);

int allocate_variables_EVP( int N, Variables *sv, void *param);
int extrapolate_variables_EVP(void *param, Euler_Angles orientation, 
			      Variables *sv, 
			      double t1, double t2, double t3);
int load_Cmatrix_EVP(void *param, double C[6][6]);
/**************************************************************************/
#endif
/**************************************************************************/

