/***************************************************************************/
/*
  Hervé Moulinec
  December 7th 2016

  translation of evp3d from fortran to C


  Elastic-Visco-Perfectly Plastic constitutive law in 3D
     sigma = L:(epsilon-epsilon_vp)
     d(epsilon_vp)/dt = 3/2*(sigma_eq/ys)**npow*dev(sigma)/sigma_eq

     with:
     dev(sigma): deviatoric part of the stress tensor sigma
     sigma_eq  : von Misses stress of the stress tensor sigma
     ys        : yield stress
     npow      : power law exponent
     L         : elasticity tensor (assumed isotropic linear elastic)

*/
/***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <utils_materials.h>
/***************************************************************************/
static double f_evpp(double x, double param[3]){
  return  param[1]*pow((x),param[0])+x+param[2];
}
/*******************************************************************************
 Nonlinear equation that need to be solved in the radial return method
 for the Elastic-Visco-Plastic with kinematic linear hardening 
 constitutive law in 3D
        x + an * pp(x-syn)**en + a0
*/
double f_evp_klh(double x,double param[4]){

  double en, an, a0, syn;
  double xx;
  en = param[0];
  an = param[1];
  a0 = param[2];
  syn = param[3];
  
  xx = x-syn;
  if ( (xx) > 0. ){
    return x + an*pow(xx,en) + a0;
  }
  else{
    return x + a0;
  }
}

/*******************************************************************************/
void evp3d_c(
           double lb, double mu, double s0, double n,
           double dt,
           int Npt,
           double (*e)[6],  double (*s)[6],
           double (*ep)[6], double (*sp)[6]
           ){

  int i,j,stat;
  int status;
  double t[6];
  double  trs, seq, steq, coef, err;
  double coef0;
  //double f_evpp(double *x, double param[3]);
  double f_evpp(double x, double param[3]);
  double param[3];
  double h;
  mon_pointeur_fonction *fonction=f_evpp;

  coef0 = 3.*mu*dt/pow(s0,n);
  err=1.e-6;
    
  status=0;
  /*------------------------------------------------------------------------*/
#pragma omp parallel for                        \
  default(none)                                 \
  private(i,j)                                  \
  private(t,trs,steq,coef, param, seq, h, stat) \
  shared(e, s, ep, sp)                          \
  shared(Npt, lb, mu, s0, n, dt)                \
  shared(coef0,err,stderr,fonction)                    \
  shared(status)
  for(i=0;i<Npt;i++){
    for(j=0;j<6;j++){
      t[j] = sp[i][j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    trs=(t[0]+t[1]+t[2])/3.;
    for(j=0;j<3;j++){
      t[j]-=trs;
    }
    // Von Mises stress of the trial elastic stress
    steq = sqrt(1.5*(t[0]*t[0]+t[1]*t[1]+t[2]*t[2]) + 3.*(t[3]*t[3]+t[4]*t[4]+t[5]*t[5]));

    if (fabs(steq)<1.e-10){
      //  Separate handling of the case steq=0 (stress is purely hydrostatic)
      coef=0.;
    }
    else{
      // Solving seq + coef0 * seq**n = steq
      param[0]=n;
      param[1]=coef0;
      param[2]=-steq;
      seq = steq/2.;
      h = steq/2.;
     // muller_(f_evpp, &seq, param, &h, &err, &stat);
      stat=muller(&seq,f_evpp,param,h,err);
/*int muller(double *x, mon_pointeur_fonction f, 
                 double *paramg, double h, double eps){*/
      if(i==0){fprintf(stderr,"stat=%i, seq=%f, steq=%f\n",stat,seq,steq);}
      if(stat!=0){
        fprintf(stderr,"in evp3d (point %d): ",i);
        fprintf(stderr,"steq=%f coef=%f n=%f\n",steq,coef0,n);
#pragma omp critical
        {
          status=-1;
        }
        
      }
      // Radial return method factor
      coef = seq/steq;
    }
    // Computing the third part of the trace
    trs = trs + lb * (e[i][0]+e[i][1]+e[i][2]-ep[i][0]-ep[i][1]-ep[i][2]);

    // Restoring hydrostatic component of the stress
    // and applying factor determined by radial return method
    for(j=0;j<3;j++){
      s[i][j] = t[j]*coef + trs;
    }
    for(j=3;j<6;j++){
      s[i][j] = t[j]*coef;
    }

  }
  /*------------------------------------------------------------------------*/
  

  if(status==-1){
    fprintf(stderr,"error while calling muller function in evp3d.\n");
  }
  
}
/***************************************************************************/
void hm_evp_klh3d(
                  double lb, double mu,
                  double eta, double n, double Hecl, double sy,
                  double dt, int Npt,
                  double e[Npt][6], double s[Npt][6],
                  double ep[Npt][6], double sp[Npt][6]
                  ){
  
  double X[6], Xp[6];
  double Y[6], Yp[6];
  double st[6];
  double steq;
  double ed[6];

  double coef;

  int i,j;
  double param[4];
  double Yeq, h, err;
  int stat;
  double f_evp_klh( double x, double param[3]);
  double trs, tre;
  int status;

  //----------------------------------------------------------------------------------
  status=0;
#pragma omp parallel for                        \
  default(none)                                 \
  shared(stderr)                                \
  shared(Npt)                                   \
  shared(lb,mu,eta,n,Hecl,sy,dt)                \
  shared(e,s,ep,sp)                             \
  private(i,j)                                  \
  private(Xp,tre,trs,st,steq,Yeq,ed)            \
  private(coef,stat,param,h,err)                \
  private(Y)                                    \
  shared(status) 
  for(i=0; i<Npt; ++i){
    // previous backstress:
    for( j=0; j<6; ++j){
      Xp[j] = Hecl/(3.*mu)*(2.*mu*ep[i][j]-sp[i][j]);
    }
    trs = (Xp[0]+Xp[1]+Xp[2])/3.;
    for(j=0;j<3;++j){
      Xp[j]=Xp[j]-trs;
    }
         
    //  sigma trial deviatoric:
    for( j=0; j<6; ++j){
      st[j] = sp[i][j] - Xp[j] + 2.*mu*(e[i][j]-ep[i][j]);
    }
    trs = ( st[0]+st[1]+st[2] ) / 3.;
    for( j=0; j<3; ++j){
      st[j]=st[j]-trs;
    }

    // sigma trial equivalent:         
    steq = sqrt( 
                1.5 * 
                ( st[0]*st[0] + st[1]*st[1] + st[2]*st[2] ) +
                3. * 
                ( st[3]*st[3] + st[4]*st[4] + st[5]*st[5] ) 
                 );
    

    if (abs(steq)<=1.e-10){
      coef=0.;
      stat=0;
    }
    else{
      param[1] = n;
      param[2]= (3.*mu+Hecl)*dt/pow(eta,n);
      param[3] = - steq;
      param[4] = sy;

      Yeq = steq/2.;
      h = steq/2.;
      err=1.e-6;
      //      call muller(f_evp_klh, Yeq, param, h, err, stat );
      stat=muller(&Yeq,f_evp_klh,param,h,err);
      if (stat!=0) {
        fprintf(stderr,"non convergence de la routine de resol de l'equation non lineaire\n");
#pragma omp critical
        {
          status=-1;
        }
      }

      coef = Yeq/steq;

    }

    for(j=0;j<6;++j){
      Y[j]=coef*st[j];
    }
    // epsilon deviatoric
    for(j=0;j<6;++j){
      ed[j]=e[i][j];
    }

    tre=(ed[0]+ed[1]+ed[2])/3.;
    trs =  (3.*lb+2.*mu)*tre;

    for(j=0;j<3;++j){
      ed[j]=ed[j]-tre;
    }
    // sigma deviatoric
    for(j=0;j<6;++j){
      s[i][j] = 3.*mu/(Hecl+3.*mu) * ( Y[j] + 2.*Hecl/3.*ed[j] );
    }

    // + hydrostatic part:
    for(j=0; j<3; ++j){
      s[i][j] = s[i][j] + trs;
    }
         
 
  }
  //----------------------------------------------------------------------------------
  if (status==-1){
         fprintf(stderr,"error while calling muller function in hm_evp_klh3d.\n");
  }


}




  
