#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>
#include <GURSON_materials.h>
#include <gurson_functions.h>
#include <variables.h>
#include <utils_materials.h>
#include <LE_Cmatrix.h>
#include <Crystal_Tools.h>


#ifdef _OPENMP
#include <omp.h>
#endif


/*######################################################################*/
/* G. Boittin
   LMA/CNRS
   August 6th 2014
   
   functions to manage gurson type plasticity behavior
   only isotropic case for elasticity is yet implemented
*/
/*######################################################################*/
int read_poro( FILE *f, GURSON_param *x) {
  double poro_init, poro_slop, poro_fixed,poro_ini;
  int status, Nt, it;
  double tim;
  double poro;

  status=craft_fscanf(f,"%d",&(x->poro_int));
  if (status!=1) return -1;

  switch (x->poro_int){

  case  FIXED_VALUE_OF_POROSITY:
    x->pp.fpp = malloc(1*sizeof(*x->pp.fpp));
    if (x->pp.fpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(poro_fixed));
    if (status!=1) return -1;
    x->pp.fpp->poro_fixed = poro_fixed;
    break;

  case LINEAR_EVO_OF_POROSITY:
    x->pp.lpp = malloc(1*sizeof(*x->pp.lpp));
    if (x->pp.lpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(poro_init));
    if (status!=1) return -1;
    x->pp.lpp->poro_init = poro_init;
    status=craft_fscanf(f,"%lf",&(poro_slop));
    if (status!=1) return -1;
    x->pp.lpp->poro_slop = poro_slop;
    break;

  case TABULATED_EVO_OF_POROSITY:
    x->pp.tpp = malloc(1*sizeof(*x->pp.tpp));
    if (x->pp.tpp==NULL) return -1;

    status=craft_fscanf(f,"%d",&(Nt));
    if (status!=1) return -1;
    x->pp.tpp->Nt = Nt;
    x->pp.tpp->tim  = malloc(Nt*sizeof(*x->pp.tpp->tim));
    x->pp.tpp->poro = malloc(Nt*sizeof(*x->pp.tpp->poro));
    if (x->pp.tpp->tim ==NULL) return -1;
    if (x->pp.tpp->poro==NULL) return -1;


    for(it=0; it<(Nt); it++){
      status=craft_fscanf(f,"%lf",&(tim));
      if (status!=1) return -1;
      x->pp.tpp->tim[it] = tim;

      status=craft_fscanf(f,"%lf",&(poro));
      if (status!=1) return -1;
      x->pp.tpp->poro[it] = poro;
    }
//    x->pp.tpp->s0_3mu = NULL;
//    x->pp.tpp->s0_3mup = NULL;
    
    break;
  case  EXPLICITLY_CALCULATED_EVO_OF_POROSITY:
    x->pp.cpp = malloc(1*sizeof(*x->pp.cpp));
    if (x->pp.cpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(poro_ini));
    if (status!=1) return -1;
    x->pp.cpp->poro_ini = poro_ini;
    break;
  case  CALCULATED_EVO_OF_POROSITY:
    x->pp.cpp = malloc(1*sizeof(*x->pp.cpp));
    if (x->pp.cpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(poro_ini));
    if (status!=1) return -1;
    x->pp.cpp->poro_ini = poro_ini;
    break;
  }
  return 1;
}
/*######################################################################*/
int read_bubble_pressure( FILE *f, GURSON_param *x) {
  double pressure_init,pressure_slop,pressure_fixed,pressure_ini,pressure_max;
  int status, Nt, it;
  double timbp;
  double bubble_pressure;

  status=craft_fscanf(f,"%d",&(x->pressure_int));
  if (status!=1) return -1;

//  ((LE_param *)p)->isotropy = isotropy;

  switch (x->pressure_int){

  case  FIXED_VALUE_OF_PRESSURE:
    x->bpp.fbpp = malloc(1*sizeof(*x->bpp.fbpp));
    if (x->bpp.fbpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(pressure_fixed));
    if (status!=1) return -1;
    x->bpp.fbpp->pressure_fixed = pressure_fixed;
    break;

  case LINEAR_EVO_OF_PRESSURE:
    x->bpp.lbpp = malloc(1*sizeof(*x->bpp.lbpp));
    if (x->bpp.lbpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(pressure_init));
    if (status!=1) return -1;
    x->bpp.lbpp->pressure_init = pressure_init;
    status=craft_fscanf(f,"%lf",&(pressure_slop));
    if (status!=1) return -1;
    x->bpp.lbpp->pressure_slop = pressure_slop;
    break;

  case TABULATED_EVO_OF_PRESSURE:
    x->bpp.tbpp = malloc(1*sizeof(*x->bpp.tbpp));
    if (x->bpp.tbpp==NULL) return -1;

    status=craft_fscanf(f,"%d",&(Nt));
    printf("Nt=%i\n",Nt);
    if (status!=1) return -1;
    x->bpp.tbpp->Nt = Nt;
    printf("Nt=%i\n",x->bpp.tbpp->Nt);
    x->bpp.tbpp->timbp  = malloc(Nt*sizeof(*x->bpp.tbpp->timbp));
    x->bpp.tbpp->bubble_pressure = 
      malloc(Nt*sizeof(*x->bpp.tbpp->bubble_pressure));
    if (x->bpp.tbpp->timbp ==NULL) return -1;
    if (x->bpp.tbpp->bubble_pressure==NULL) return -1;

    for(it=0; it<(Nt); it++){
      status=craft_fscanf(f,"%lf",&(timbp));
      if (status!=1) return -1;
      x->bpp.tbpp->timbp[it] = timbp;

      status=craft_fscanf(f,"%lf",&(bubble_pressure));
      if (status!=1) return -1;
      x->bpp.tbpp->bubble_pressure[it] = bubble_pressure;
    }
    
    break;

  case  CALCULATED_EVO_OF_PRESSURE:
    x->bpp.cbpp = malloc(1*sizeof(*x->bpp.cbpp));
    if (x->bpp.cbpp==NULL) return -1;

    status=craft_fscanf(f,"%lf",&(pressure_ini));
    if (status!=1) return -1;
    x->bpp.cbpp->pressure_ini = pressure_ini;
    status=craft_fscanf(f,"%lf",&(pressure_max));
    if (status!=1) return -1;
    x->bpp.cbpp->pressure_max = pressure_max;
    break;

  }
  return 1;
}
/*######################################################################*/



int read_parameters_GURSON( FILE *f, void *p ) {
  int status;
/*  int isotropie;*/
//  printf("enter read parameters\n");

  GURSON_param *x;

  x = (GURSON_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->sig0));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->q3));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->q1));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );
  //printf("lame read parameters\n");

  status = read_poro(f, x);
//  printf("poro read parameters\n");
  status = read_bubble_pressure(f, x);
  if (status!=1) return -1;
 // printf("end read parameters\n");
  return 0;
}


/*######################################################################*/
int print_parameters_GURSON( FILE *f, void *p , int flag ) {
  int status;
  int i,j,it;
//  printf("enter print param\n");
//  printf("test_mem\n");
  GURSON_param *x;
  status=0;
  x = (GURSON_param *)p;

  //  fprintf(f,"# C_matrix:\n");
    for(i=0;i<6;i++) {
      fprintf(f,"# ");
      for(j=0;j<6;j++){
        fprintf(f,"%lf ",(((GURSON_param *)p)->C_ini[i][j]));
      }
     fprintf(f,"\n"); 
    }
    fprintf(f,"#\n");
//  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((GURSON_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((GURSON_param *)p)->nu));
    fprintf(f,"\n");
    fprintf(f,"# 1st Lamé coef.  = %lf  ",(((GURSON_param *)p)->lb));
    fprintf(f,"\n");
    fprintf(f,"# 2d Lamé coef. = %lf  ",(((GURSON_param *)p)->mu));
    fprintf(f,"\n");
    fprintf(f,"# bulk modulus  = %lf  ",(((GURSON_param *)p)->k));
    fprintf(f,"\n");
    fprintf(f,"# sigma_y  = %lf  ",(((GURSON_param *)p)->sig0));
    fprintf(f,"\n");
    fprintf(f,"# q3  = %lf  ",(((GURSON_param *)p)->q3));
    fprintf(f,"\n");
    fprintf(f,"# q1  = %lf  ",(((GURSON_param *)p)->q1));
    fprintf(f,"\n");


    switch (x->poro_int){
    case FIXED_VALUE_OF_POROSITY:
      fprintf(f,"# constant porosity = %lf  ", x->pp.fpp->poro_fixed);
      fprintf(f,"\n");
      break;
    case LINEAR_EVO_OF_POROSITY:
      fprintf(f,"# initial porosity = %lf  ", x->pp.lpp->poro_init);
      fprintf(f,"\n");
      fprintf(f,"# (porosity=f(time)) slop = %lf  ", x->pp.lpp->poro_slop);
      fprintf(f,"\n");
      break;
    case TABULATED_EVO_OF_POROSITY:
      fprintf(f,"# tabulated porosity  ");
      for(it=0; it<(x->pp.tpp->Nt); it++){
        fprintf(f,"# \t %lf \t %lf", 
          x->pp.tpp->tim[it], x->pp.tpp->poro[it]);
      }
      break;
    case CALCULATED_EVO_OF_POROSITY:
      fprintf(f,"# initial porosity = %lf  ", x->pp.cpp->poro_ini);
      fprintf(f,"\n");
      break;
    case EXPLICITLY_CALCULATED_EVO_OF_POROSITY:
      fprintf(f,"# initial porosity = %lf  ", x->pp.cpp->poro_ini);
      fprintf(f,"\n");
      break;
    }
    switch (x->pressure_int){
    case FIXED_VALUE_OF_PRESSURE:
      fprintf(f,"# constant bubble pressure = %lf  ",
               x->bpp.fbpp->pressure_fixed);
      fprintf(f,"\n");
      break;
    case LINEAR_EVO_OF_PRESSURE:
      fprintf(f,"# initial bubble pressure = %lf  ",
              x->bpp.lbpp->pressure_init);
      fprintf(f,"\n");
      fprintf(f,"# (bubble pressure=f(time)) slop = %lf  ",
              x->bpp.lbpp->pressure_slop);
      fprintf(f,"\n");
      break;
    case TABULATED_EVO_OF_PRESSURE:
      fprintf(f,"# tabulated bubble pressure Nt=%i ",x->bpp.tbpp->Nt);
      for(it=0; it<(x->bpp.tbpp->Nt); it++){
        fprintf(f,"# \t %lf \t %lf", 
          x->bpp.tbpp->timbp[it], x->bpp.tbpp->bubble_pressure[it]);
      }
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      fprintf(f,"# initial bubble pressure = %lf  ",
               x->bpp.cbpp->pressure_ini);
      fprintf(f,"\n");
      fprintf(f,"# maximal bubble pressure = %lf  ",
               x->bpp.cbpp->pressure_ini);
      fprintf(f,"\n");
      break;
    }
  //printf("fin print param\n");

  return status;
}

/*######################################################################*/
int allocate_parameters_GURSON( void **p ) {
 // printf("enter allocate param\n");

  GURSON_param *x;
  int status;



  status=0;

  x=(GURSON_param *)malloc(sizeof(GURSON_param));

  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
 // printf("fin allocate param\n");
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_GURSON( void *p ) {
  int status;
  status=0;
  free(p);
  return status;

}
/*######################################################################*/
int copy_parameters_GURSON( void *param_dest, void *param_src) {
  *((GURSON_param *)param_dest) = *((GURSON_param *)param_src);
  return 0;
}
/*######################################################################*/
int  allocate_variables_GURSON( int N, Variables *sv, void *param) {
  int status;
  int scalar_t[3]={SCALAR,0,0};
  int tensor_t[3]={TENSOR2,0,0};
  GURSON_param *x;
  x = (GURSON_param *)param;

 // printf("enter allocate variables\n");
  status=0;
  sv->np = N;


  int if_poro=0;
  int if_pressure=0;

  switch (x->poro_int){
    case  CALCULATED_EVO_OF_POROSITY:
      if_poro=1;
    break;
    case  EXPLICITLY_CALCULATED_EVO_OF_POROSITY:
      if_poro=1;
    break;
  }

  switch (x->pressure_int){
    case  CALCULATED_EVO_OF_PRESSURE:
      printf("evo of bubble pressure\n");
      if_pressure=1;
      if (if_poro){
        sv->nvar = 9;
        sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
        if ( sv->vars == NULL ) return -1;
        status = allocate_variable
            (&sv->vars[6], "porosity", N, scalar_t, 0);
        status = allocate_variable
            (&sv->vars[7], "bubble_pressure", N, scalar_t, 0);
        status = allocate_variable
            (&sv->vars[8], "previous_porosity", N, scalar_t, 0);
      if (status<0) return -1;
      }
      else{
        sv->nvar = 7;
        sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
        if ( sv->vars == NULL ) return -1;
//        sv->nvar = 6;
        status = allocate_variable
           (&sv->vars[6], "bubble_pressure", N, scalar_t, 0);}
//        k=2;
      if (status<0) return -1;
    break;
  }

  if ( (if_poro) && (!if_pressure) ){
      printf("evo of porosity\n");
      sv->nvar = 8;
      sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
      if ( sv->vars == NULL ) return -1;
      status = allocate_variable
            (&sv->vars[6], "porosity", N, scalar_t, 0);
      if (status<0) return -1;
      status = allocate_variable
            (&sv->vars[7], "previous_porosity", N, scalar_t, 0);
      if (status<0) return -1;

  }




  if ((!if_poro)&&(!if_pressure)) {
      sv->nvar = 6;
      sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
      if ( sv->vars == NULL ) return -1;
  }
//  printf("courant allocate variables, sv->nvar =%i\n",sv->nvar);


  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable
         (&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable
         (&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;
//  status = allocate_variable(&sv->vars[4], "porosity", N, scalar_t, 0);
//  if (status<0) return -1;
//  status = allocate_variable(&sv->vars[6], "bubble pressure", N, scalar_t, 0);
//  if (status<0) return -1;
  status = allocate_variable
          (&sv->vars[4], "previous_cum_plastic_strain", N, scalar_t, 0);
  status = allocate_variable
          (&sv->vars[5], "cumulated_plastic_strain", N, scalar_t, 0);
  if (status<0) return -1;



  printf(" allocated variables, sv->nvar =%i, N=%i\n",sv->nvar,N);

  return status;
}
/*######################################################################*/
int extrapolate_variables_GURSON(

    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3) {
 //  printf("in extrapolate_variables_GURSON\n");

// For Gurson_law, the porosity and bubble pressure needs to be actualized
// even if they are NOT variables
// Either a new type of precalculation should be add
// or extralopate variables should be renamed precalcul

   int status;
  double (*epsilon)[6];
  double (*epsilon_p)[6];

  double (*sigma)[6];
  double (*sigma_p)[6];
  double *vporo;
  double *pvporo;
  double *cump;
  double *pvcump;

  int N;
  int i,j;

  status=0;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);

  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  sigma_p = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);
  cump   = (double (*))
         (ptr_variable("cumulated_plastic_strain", sv, param)->values);
  pvcump   = (double (*))
         (ptr_variable("previous_cum_plastic_strain", sv, param)->values);
  
  extrapol2( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );

  /* sigma -> sigma_p */
  N = sv->np;
#pragma omp parallel for \
  default(none)		 \
  shared(N)		 \
  private(i,j)		 \
  shared(sigma,sigma_p,pvcump,cump)
  for(i=0;i<N;i++) {
    pvcump=cump;
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }

//Calculation of porosity and bubble pressure
  GURSON_param *x;
  x = (GURSON_param *)param;


  double slop;
  double coef;
  double new_pressure,new_poro;
  int k,it;
  switch (x->pressure_int){
    case LINEAR_EVO_OF_PRESSURE:
       slop =x->bpp.lbpp->pressure_slop;
       new_pressure=x->bpp.tbpp->pressure_actu+(t3)*slop;
       x->bpp.lbpp->pressure_actu=new_pressure;
       
      break;
    case TABULATED_EVO_OF_PRESSURE:
      it=k=0;
      while((it<(x->bpp.tbpp->Nt)) && (!k)){
          if (t2>(x->bpp.tbpp->timbp[it])){it++;}
          else{
            k=1;
            if (t3<(x->bpp.tbpp->timbp[it])){
               coef=(t3-t2)/
               ((x->bpp.tbpp->timbp[it])-(x->bpp.tbpp->timbp[it-1]));
                new_pressure=x->bpp.tbpp->pressure_actu+
                       coef*(x->bpp.tbpp->bubble_pressure[it]-
                             x->bpp.tbpp->bubble_pressure[it-1]);
                x->bpp.tbpp->pressure_actu=new_pressure;
            }
            else {
               coef=(t3-(x->bpp.tbpp->timbp[it]))/
               ((x->bpp.tbpp->timbp[it+1])-(x->bpp.tbpp->timbp[it]));
                new_pressure=x->bpp.tbpp->bubble_pressure[it]+
                       coef*(x->bpp.tbpp->bubble_pressure[it+1]-
                             x->bpp.tbpp->bubble_pressure[it]);
                x->bpp.tbpp->pressure_actu=new_pressure;
            }
      //printf("# tab bubble pressure t2=%g,t3=%g,pressure1=%g,pressure2=%g, coef=%g \n ",t2,t3,x->bpp.tbpp->pressure_actu,new_pressure,coef);
              
          }//end else
      }//end while
      break;
   }//end switch

  switch (x->poro_int){
    case LINEAR_EVO_OF_POROSITY:
       slop =x->pp.lpp->poro_slop;
       new_poro=x->pp.tpp->poro_actu+(t3-t2)*slop;
       x->pp.lpp->poro_actu=new_poro;
      break;
    case TABULATED_EVO_OF_POROSITY:
      it=k=0;
      while((it<(x->pp.tpp->Nt)) && (!k)){
          if (t2>(x->pp.tpp->tim[it])){it++;}
          else{
            k=1;
            if (t3<(x->pp.tpp->tim[it])){
               coef=(t3-t2)/
               ((x->pp.tpp->tim[it])-(x->pp.tpp->tim[it-1]));
                new_poro=x->pp.tpp->poro_actu+
                       coef*(x->pp.tpp->poro[it]-
                             x->pp.tpp->poro[it-1])/2.0;
                x->pp.tpp->poro_actu=new_poro;
            }
            else {
               coef=(t3-(x->pp.tpp->tim[it]))/
               ((x->pp.tpp->tim[it+1])-(x->pp.tpp->tim[it]));
                new_poro=x->pp.tpp->poro[it]+
                       coef*(x->pp.tpp->poro[it+1]-
                             x->pp.tpp->poro[it])/2.0;
                x->pp.tpp->poro_actu=new_poro;
            }
              
          }//end else
      }//end while
      if (t2>=(x->pp.tpp->tim[(x->pp.tpp->Nt)-1])){
               it =(x->pp.tpp->Nt)-2;
               coef=(t3-(x->pp.tpp->tim[it+1]))/
               ((x->pp.tpp->tim[it+1])-(x->pp.tpp->tim[it]));
                new_poro=x->pp.tpp->poro[it+1]+
                       coef*(x->pp.tpp->poro[it+1]-
                             x->pp.tpp->poro[it]);
                x->pp.tpp->poro_actu=new_poro;
      }


      break;
    case EXPLICITLY_CALCULATED_EVO_OF_POROSITY:
      vporo   = (double (*))
         (ptr_variable("porosity", sv, param)->values);
      for(i=0;i<sv->np;i++) {
        vporo[i]=vporo[i]+(1.0-vporo[i])*(epsilon[1][i]-epsilon_p[1][i]+epsilon[2][i]-epsilon_p[2][i]+epsilon[3][i]-epsilon_p[3][i]);
      }
      break;
    case CALCULATED_EVO_OF_POROSITY:
      vporo   = (double (*))
         (ptr_variable("porosity", sv, param)->values);
      pvporo   = (double (*))
         (ptr_variable("previous_porosity", sv, param)->values);
      for(i=0;i<sv->np;i++) {
        pvporo[i]=vporo[i];
      }
      break;
   }//end switch
  return status;
}
/*######################################################################*/
int  init_variables_GURSON( Variables *sv, void *param) {
   
  GURSON_param *p;
  
  int i,j;

 double (*sigma)[6], (*sigma_p)[6];



  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  sigma_p    = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);
  p  = ((GURSON_param *)param);  


   double poro_tmp;
   double pressure_tmp;
   double *cump;
   double *pvcump;
   cump = (double (*))(ptr_variable
         ("cumulated_plastic_strain", sv, param)->values);
   pvcump = (double (*))(ptr_variable
         ("previous_cum_plastic_strain", sv, param)->values);
   for(i=0;i<sv->np;i++) {
        cump[i]=0.0;
        pvcump[i]=0.0;
        for(j=0;j<6;j++) {
		 sigma_p[i][j] =0.0;
		 sigma[i][j] =0.0;
        }
   }
   switch (p->poro_int){
      double *vporo,*pvporo;
/*    case FIXED_VALUE_OF_POROSITY:
      double poro_tmp= p->pp.fpp->poro_fixed;
      for(i=0;i<sv->np;i++) {
        vporo[i]=poro_tmp;
      }
      break;*/
    case LINEAR_EVO_OF_POROSITY:
      p->pp.lpp->poro_actu=p->pp.lpp->poro_init;
      break;
    case TABULATED_EVO_OF_POROSITY:
      p->pp.tpp->poro_actu=p->pp.tpp->poro[0];
      break;
    case EXPLICITLY_CALCULATED_EVO_OF_POROSITY:
      poro_tmp= p->pp.cpp->poro_ini;
      vporo   = (double (*))
         (ptr_variable("porosity", sv, param)->values);
      for(i=0;i<sv->np;i++) {
        vporo[i]=poro_tmp;
      }
      break;
    case CALCULATED_EVO_OF_POROSITY:
      poro_tmp= p->pp.cpp->poro_ini;
      vporo   = (double (*))
         (ptr_variable("porosity", sv, param)->values);
      pvporo   = (double (*))
         (ptr_variable("previous_porosity", sv, param)->values);
      for(i=0;i<sv->np;i++) {
        vporo[i]=poro_tmp;
        pvporo[i]=poro_tmp;
      }
      break;
    }

   switch (p->pressure_int){
      double *vPb;
/*    case FIXED_VALUE_OF_PRESSURE:
      double pressure_tmp= p->bpp.fbpp->pressure_fixed;
      for(i=0;i<sv->np;i++) {
        vPb[i]=pressure_tmp;
      }
      break;;*/
    case LINEAR_EVO_OF_PRESSURE:
      p->bpp.lbpp->pressure_actu=p->bpp.lbpp->pressure_init;
      break;
    case TABULATED_EVO_OF_PRESSURE:
      p->bpp.tbpp->pressure_actu=p->bpp.tbpp->bubble_pressure[0];
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      vPb  = (double (*))
         (ptr_variable("bubble_pressure", sv, param)->values);
      pressure_tmp= p->bpp.cbpp->pressure_ini;
      for(i=0;i<sv->np;i++) {
        vPb[i]=pressure_tmp;
      }
      break;
   }

 /* for(i=0;i<sv->np;i++) {
    for(j=0;j<6;j++) {
      ii = i*6+j;
      sigma[ii] = 0.;
      sigma_p[ii] = 0.;
//      epst[ii] = 0.;
//      epst_p[ii] = 0.;
    }
    vporo[i]=0.001;
    vPb[i]=0.0;
//    vporo_p[i]=0.001;
//    vPb_p[i]=0.0;
  }*/
  
  return 0;
}

/*######################################################################*/
int load_Cmatrix_GURSON(void *param, double C[6][6])
{

int i,j;
GURSON_param p;

p = *((GURSON_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}

C[0][0] =  p.lb + 2.*p.mu;
C[1][1] = C[0][0];
C[2][2] = C[0][0];
C[0][1] = p.lb;
C[0][2] = C[0][1];
C[1][2] = C[0][1];
C[1][0] = C[0][1];
C[2][0] = C[0][1];
C[2][1] = C[0][1];
C[3][3] = 2.*p.mu;
C[4][4] = C[3][3];
C[5][5] = C[3][3];


return(0);
}

/*######################################################################*/


//typedef double mon_pointeur_fonction (double,double[*]);

/*********************************************************************************/

/*********************************************************************************/
int behavior_GURSON( 
		   void *param, Euler_Angles orientation, 
		   Variables *sv,double dt ){

  int function_evo_poro=0;
  GURSON_param *p;
  p  = ((GURSON_param *)param);
  int status;

  switch (p->poro_int){
    case CALCULATED_EVO_OF_POROSITY:
      function_evo_poro=1;
      break;
  }

  if (function_evo_poro) status=reso_gurson_basic_poro_evo(p,sv,dt);
  else status=reso_gurson_basic_poro_fix(p,sv,dt);
  if (status<0)  printf("behavior iteration basic NOK, status=%i\n",status);
  if (status<0)  status=-1;
  if (status<0)  exit(0);
  return status;

}
/*********************************************************************************/
/*********************************************************************************/
int solve_spc0e_GURSON( void *param,
                    Euler_Angles orientation,
                    Variables *sv,
                    double dt,
                    LE_param *L0,
                    double (*tau)[6]
                  ){
  int status;
  GURSON_param *p;
  p  = ((GURSON_param *)param);

  int function_evo_poro=0;

  switch (p->poro_int){
    case CALCULATED_EVO_OF_POROSITY:
      function_evo_poro=1;
      break;
  } 
  if ( L0->isotropy != ISOTROPIC){
    fprintf(stderr, "The reference material has to be isotropic to solve (c+c0)e=tau\n"
      "for the elastoplastic gurson materials.");
    return -1;
  }
  

  if (function_evo_poro) status=reso_gurson_spc0e_poro_evo(p,sv,dt,L0,tau);
  else status=reso_gurson_spc0e_poro_fix(p,sv,dt,L0,tau);
  if (status<0)  printf("behavior iteration accel NOK, status=%i\n",status);
  if (status<0)  exit(status);
  return status;
}


