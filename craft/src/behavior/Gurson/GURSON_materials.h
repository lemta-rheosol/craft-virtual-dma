/**************************************************************************/
/* G. Boittin
   LMA/CNRS
   august 04th 2014
   
   header file for Gurson law Elastic Visco-Plastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef GURSON_MATERIAL
#define GURSON_MATERIAL 60              /* GURSON Elastic Plastic behavior */



#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>
//#include <zero_of_function.h>
//#include <complex.h>

//#include <Gurson2.h>

enum Pressure_evolution{
  NO_PRESSURE                =0,
  FIXED_VALUE_OF_PRESSURE    =1,
  LINEAR_EVO_OF_PRESSURE     =2,
  TABULATED_EVO_OF_PRESSURE  =3,
  CALCULATED_EVO_OF_PRESSURE  =4,
};

/* empty structure are theoretically forbidden. Thus, to avoid a warning message
   such as:
           warning: struct has no members [-Wpedantic]
   during compilation stage, one replaces the definitions of  the structure:
           typedef struct {} no_pressure_param;
   by:
*/
typedef struct _no_pressure_param no_pressure_param;



typedef struct {
  double pressure_fixed;
} fixed_pressure_param;

typedef struct {
  double pressure_init;
  double pressure_slop;
  double pressure_actu;
} linear_pressure_param;

typedef struct {
  int Nt;       /* number of times on which porosity evolution is tabulated */
  double *timbp;    /* tabulated times */
  double *bubble_pressure;   /* tabulated plastic stresses */
  double pressure_actu;
} tab_pressure_param;

typedef struct {
  double pressure_ini;
  double pressure_max;
} calc_pressure_param;


enum Porosity_evolution{
  FIXED_VALUE_OF_POROSITY    =1,
  LINEAR_EVO_OF_POROSITY     =2,
  TABULATED_EVO_OF_POROSITY  =3,
  EXPLICITLY_CALCULATED_EVO_OF_POROSITY  =4,
  CALCULATED_EVO_OF_POROSITY  =5,
};


typedef struct {
  double poro_fixed;
} fixed_poro_param;

typedef struct {
  double poro_init;
  double poro_slop;
  double poro_actu;
} linear_poro_param;

typedef struct {
  int Nt;       /* number of times on which porosity evolution is tabulated */
  double *tim;    /* tabulated times */
  double *poro;   /* tabulated plastic stresses */
  double poro_actu;
} tab_poro_param;

typedef struct {
  double poro_ini;
} calc_poro_param;


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* compression modulus  */
  double C_ini[6][6];  /* Elastic Rigidity matrix in the cristal axis*/
  double C[6][6];      /* Elastic Rigidity matrix in the laboratory axis*/
  /* plasticity parameters */
  double sig0;    /* yield stress         */
  double q3;    /* gurson law coef for sigeq^2         */
  double q1;    /* gurson law coef for f        */
  /* param�tres d�fini par l'utilisateur*/
//  double poro_ini; /*porosit�*/
//  double Pb_ini; /*Pression dans les bulles*/
 /* Bubbles Pressure parameters */
  int pressure_int;
  union {
    fixed_pressure_param* fbpp;
    linear_pressure_param* lbpp;
    tab_pressure_param* tbpp;
    calc_pressure_param* cbpp;
    } bpp;
  /* porosity parameters */
  int poro_int;
  union {
    fixed_poro_param* fpp;
    linear_poro_param* lpp;
    tab_poro_param* tpp;
    calc_poro_param* cpp;
    } pp;
} GURSON_param;

//typedef double mon_pointeur_fonction (double,double[*]);
//typedef double mon_pointeur_fonction2 (double,double,double[*]);
/*------------------------------------------------------------------------*/
int read_poro( FILE *f, GURSON_param *x) ;
int read_bubble_pressure( FILE *f, GURSON_param *x) ;
int read_parameters_GURSON( FILE *f, void *p );
int print_parameters_GURSON( FILE *f, void *p , int flag);
int init_variables_GURSON(Variables *sv, void *param );

int allocate_parameters_GURSON( void **p );
int deallocate_parameters_GURSON( void *p );
int copy_parameters_GURSON( void *param_dest, void *param_src);

int behavior_GURSON( void *param, Euler_Angles orientation, Variables *sv, double dt);

int allocate_variables_GURSON( int N, Variables *sv, void *param);
int extrapolate_variables_GURSON(void *param, Euler_Angles orientation, 
			      Variables *sv, 
			      double t1, double t2, double t3);
/*int extrapolate_variables_GURSON(void **p );;*/
int load_Cmatrix_GURSON(void *param, double C[6][6]);

double dfdsig_sigpoint_gurson(double x,double paramg[9]);
double f_gurson(double x,double y,double paramf[5]);
double f_gurson_p_hydro_nulle(double x,double paramg[9]);
double f_gurson_forbidden(double x,double paramg[9]);
double f_gurson2(double x,double paramg[7]);
double f_gurson_spc0e(double x,double param_spc0e[7]);
int solve_spc0e_GURSON( void *param,
                    Euler_Angles orientation,
                    Variables *sv,
                    double dt,
                    LE_param *L0,
                    double (*tau)[6]
                  );
int reso_gurson_basic_poro_evo( 
		   void *param, Variables *sv,double dt);
int reso_gurson_spc0e_poro_evo(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]);

int reso_gurson_basic_poro_fix( 
		   void *param, Variables *sv,double dt);
int reso_gurson_spc0e_poro_fix(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]);
/**************************************************************************/
#endif
/**************************************************************************/

