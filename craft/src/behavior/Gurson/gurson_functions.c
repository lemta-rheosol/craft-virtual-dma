#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



#ifdef _OPENMP
#include <omp.h>
#endif
/*######################################################################*/
/* G. Boittin
   LMA/CNRS
   October 28th 2014
   
   functions to manage gurson type plasticity behavior
   only isotropic case for elasticity is yet implemented
*/
/*######################################################################*/


//typedef double mon_pointeur_fonction (double,double[*]);
//basic scheme//
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/
//typedef double mon_pointeur_fonction2 (double,double,double[*]);
/********************************************************************************
c Nonlinear equation that need to be solved in the radial return method
c for the Gurson Plastic constitutive law in 3D

/********************************************************************************/ 
      double h2f_gurson(double x,double paramg[8]){
        double sinh1,sinint,res,num;
        sinint=paramg[1]*(x+paramg[0]);
        sinh1=sinh(sinint);
        num=(paramg[5]*sinh1+paramg[6]+paramg[7]*x);
        res=paramg[2]*sinh1*sinh1+(paramg[3]*cosh(sinint)+paramg[4])*num*num;
        return res;
      }  


/********************************************************************************/ 


      double h2f_gurson2(double x,double paramg[7]){
/*  paramg[0] = 6.0*p->mu;
    paramg[1] = paramdgdsig[1] = (3.0)/(2.0*sig0);
    paramg[2]= porosity;
    paramg[3] = 9.0*k*sig0; 
    paramg[4]=(steq2)/(sig0*sig0); 
    paramg[5]=trsp+k*(e[i][0]+e[i][1]+e[i][2])
                  -k*(ep[i][0]+ep[i][1]+ep[i][2]);
    paramg[6]= bubble pressure;
*/
        double denom,res,num,inter;
        num=paramg[3]*paramg[2]*(sinh(paramg[1]*(x+paramg[6])));
        denom=num+paramg[0]*(paramg[5]-x);
        inter=2.0*paramg[2]*cosh(paramg[1]*(x+paramg[6]))-1.0;
        inter=(inter-(paramg[2]*paramg[2]));
        res= (inter*denom*denom+paramg[4]*num*num)*paramg[1]*paramg[1]*paramg[1]*paramg[1];

        return res;
      }  
/********************************************************************************/ 
      double df_h2f_gurson2(double x,double paramg[7]){
        double denom,res,num,inter;
/*  paramg[0] = 6.0*p->mu;
    paramg[1] = paramdgdsig[1] = (3.0)/(2.0*sig0);
    paramg[2]= porosity;
    paramg[3] = 9.0*k*sig0; 
    paramg[4]=(steq2)/(sig0*sig0); 
    paramg[5]=trsp+k*(e[i][0]+e[i][1]+e[i][2])
                  -k*(ep[i][0]+ep[i][1]+ep[i][2]);
    paramg[6]= bubble pressure;
*/
        num=paramg[3]*paramg[2]*(sinh(paramg[1]*(x+paramg[6])));
        denom=num+paramg[0]*(paramg[5]-x);
        inter=2.0*paramg[2]*cosh(paramg[1]*(x+paramg[6]))-1.0;
        inter=(inter-(paramg[2]*paramg[2]));
        res= 2.0*(num*paramg[1]/paramg[3]*denom*denom+denom*(paramg[2]*paramg[3]*paramg[1]*cosh(paramg[1]*(x+paramg[6]))-paramg[0])*inter+paramg[4]*num*paramg[3]*paramg[2]*paramg[1]*cosh(paramg[1]*(x+paramg[6])));
        return res;
      }       
/********************************************************************************/ 
/********************************************************************************/  
/********************************************************************************/ 
      double f_gurson2(double x,double paramg[7]){
        double denom,res,num,inter;
/*  paramg[0] = 6.0*p->mu;
    paramg[1] = paramdgdsig[1] = (3.0)/(2.0*sig0);
    paramg[2]= porosity;
    paramg[3] = 9.0*k*sig0; 
    paramg[4]=(steq2)/(sig0*sig0); 
    paramg[5]=trsp+k*(e[i][0]+e[i][1]+e[i][2])
                  -k*(ep[i][0]+ep[i][1]+ep[i][2]);
    paramg[6]= bubble pressure;
*/
        num=paramg[3]*paramg[2]*(sinh(paramg[1]*(x+paramg[6])));
        denom=num+paramg[0]*(paramg[5]-x);
        inter=2.0*paramg[2]*cosh(paramg[1]*(x+paramg[6]))-1.0;
        inter=(inter-(paramg[2]*paramg[2]));
        res= inter+paramg[4]*num/denom*num/denom;
        //res=res;
        return res;
      } 
   /********************************************************************************/ 
      double df_f_gurson2(double x,double paramg[7]){
        double denom,res,num,inter;
/*  paramg[0] = 6.0*p->mu;
    paramg[1] = paramdgdsig[1] = (3.0)/(2.0*sig0);
    paramg[2]= porosity;
    paramg[3] = 9.0*k*sig0; 
    paramg[4]=(steq2)/(sig0*sig0); 
    paramg[5]=trsp+k*(e[i][0]+e[i][1]+e[i][2])
                  -k*(ep[i][0]+ep[i][1]+ep[i][2]);
    paramg[6]= bubble pressure;
*/
        num=paramg[3]*paramg[2]*(sinh(paramg[1]*(x+paramg[6])));
        denom=num+paramg[0]*(paramg[5]-x);
        inter=2.0*paramg[2]*cosh(paramg[1]*(x+paramg[6]))-1.0;
        inter=(inter-(paramg[2]*paramg[2]));
        res= 2.0*(num*paramg[1]/paramg[3]*denom*denom+denom*(paramg[2]*paramg[3]*paramg[1]*cosh(paramg[1]*(x+paramg[6]))-paramg[0])*inter+paramg[4]*num*paramg[3]*paramg[2]*paramg[1]*cosh(paramg[1]*(x+paramg[6])));
        res=res/denom/denom-2.0*(inter/denom+paramg[4]*num*num/denom/denom/denom)*(paramg[2]*paramg[3]*paramg[1]*cosh(paramg[1]*(x+paramg[6]))-paramg[0]);
        return res;
      }  
/********************************************************************************/ 
/********************************************************************************/

/********************************************************************************
c Nonlinear equation gurson (represent f)
c for the Gurson Plastic constitutive law in 3D
*/
      double f_gurson(double x,double y,double paramf[5]){
//      f(x,y)=(y/sig0^2)+2fcosh(3(x+Pb)/2sig_0)-1-f^2
//       x=sig_m
//       y=sig_eq^2
        double res;
        res=y*paramf[0]+paramf[2]*cosh(paramf[1]*(x+paramf[3]))+paramf[4];
        return res;
      }

//*********************************************************************************
/*######################################################################*/
/********************************************************************************
*/
      double f1(double sig_m,double f,double paramg[10]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=sigT_eq^2/sig0^2
        paramg[2]=9sig0^2 k
        paramg[3]=-6 mu
        paramg[4]=6 mu(3k delta t eps_m_point-\sig_m_t0)
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double num=paramg[2]*f*sin1;
        double denom=num+(paramg[3]*sig_m+paramg[4]);
        res=paramg[1]*(num/denom)*(num/denom)+2.0*f*cosh(sinint)-1.0-f*f;
        return res;
      } 
/********************************************************************************/
/*********************************************************************************/
/********************************************************************************/
/********************************************************************************
*/
/********************************************************************************/
/********************************************************************************
*/
      double df1_dsig_m(double sig_m,double f,double paramg[10]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=sigT_eq^2/sig0^2
        paramg[2]=9sig0^2 k
        paramg[3]=-6 mu
        paramg[4]=6 mu(3k delta t eps_m_point-\sig_m_t0)
*/

        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double p2fsin=paramg[2]*f*sin1;
        double denom=p2fsin+paramg[3]*sig_m+paramg[4];
        double  res=2.0*paramg[1]*(paramg[0]*paramg[2]*f*cosh(sinint)*(paramg[3]*sig_m+paramg[4])-p2fsin*paramg[3])*p2fsin;
        res=res/(denom*denom*denom);
        res=res+2.0*paramg[0]*f*sin1;
        return res;
      } 
/********************************************************************************/
/********************************************************************************
*/
      double df1_df(double sig_m,double f,double paramg[10]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=sigT_eq^2/sig0^2
        paramg[2]=9sig0^2 k
        paramg[3]=-6 mu
        paramg[4]=6 mu(3k delta t eps_m_point-\sig_m_t0)
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double p2fsin=paramg[2]*f*sin1;
        double denom=p2fsin+paramg[3]*sig_m+paramg[4];
        res=paramg[1]*2.0*(p2fsin*paramg[2]*sin1*(paramg[3]*sig_m+paramg[4]));
        res=res/(denom*denom*denom);
        res=res+2.0*(cosh(sinint)-f);
        return res;
      } 
/********************************************************************************/
/********************************************************************************
*/
 

//lagrangian augmented scheme//
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/


/********************************************************************************/ 
      double f_gurson_spc0e(double x,double param_spc0e[7]){
/*
  double k5=3.0*muplusmu0*sig02*p->k;
  double k1=13.5*sig02*mu0*mu0*p->k*p->k;
  double k2=musurmu0*musurmu0;
  double k3=1.0+musurmu0;
  double k4=2.*musurmu0*k3;
    param_spc0e[0]=1.5/p->sig0;
    param_spc0e[4]=-deuxmumu0*unplusksurk0*p->sig0;
    param_spc0e[2]=k5*porosity;
    param_spc0e[5]=porosity;
    param_spc0e[6]=pressure;
    param_spc0e[1]=k1*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)*porosity*porosity;
    param_spc0e[3]=deuxmumu0*(ksurk0*deltataum+trsp*unplusksurk0)*p->sig0;*/

        double sinint,res;
        sinint=param_spc0e[0]*(x+param_spc0e[6]);
        res=(param_spc0e[2]*sinh(sinint)+param_spc0e[3]+param_spc0e[4]*x);
        res=sinh(sinint)*sinh(sinint)*param_spc0e[1]/res/res+((2.0*param_spc0e[5]*cosh(sinint)-1.0-param_spc0e[5]*param_spc0e[5]));
        return res;
      }  
/********************************************************************************/    
      double h2f_gurson_spc0e(double x,double param_spc0e[7]){
/*
  double k5=3.0*muplusmu0*sig02*p->k;//à multiplier par la porosité pour avoir param_spc0e[2]
  double k1=13.5*sig02*mu0*mu0*p->k*p->k;
  double k2=musurmu0*musurmu0;
  double k3=1.0+musurmu0;
  double k4=2.*musurmu0*k3;
    param_spc0e[0]=1.5/p->sig0; 
    param_spc0e[4]=-deuxmumu0*unplusksurk0*p->sig0;
    param_spc0e[2]=k5*porosity;
    param_spc0e[5]=porosity;
    param_spc0e[6]=pressure;
    param_spc0e[1]=k1*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)*porosity*porosity;
    param_spc0e[3]=deuxmumu0*(ksurk0*deltataum+trsp*unplusksurk0)*p->sig0;*/

        double sinint,res;
        sinint=param_spc0e[0]*(x+param_spc0e[6]);
        res=(param_spc0e[2]*sinh(sinint)+param_spc0e[3]+param_spc0e[4]*x);
        res=(sinh(sinint)*sinh(sinint)*param_spc0e[1]+((2.0*param_spc0e[5]*cosh(sinint)-1.0-param_spc0e[5]*param_spc0e[5]))*res*res);
        double multi =param_spc0e[0]*param_spc0e[0]*param_spc0e[0]*param_spc0e[0]/5.025;
        res=res*multi*multi;
        return res;
      }  
 
/********************************************************************************/
      double f1_spc0e(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/k0
*/
        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double cos1=cosh(sinint);
        double denom=paramg[2]*f*sin1+paramg[3]*sig_m+paramg[4];
        res=paramg[7]*f*f*sin1*sin1;
        res=res/denom/denom;
        res=res+2.0*f*cos1-1.0-f*f;
        return res;
      } 
/********************************************************************************/
      double f2_spc0e(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double cos1=cosh(sinint);
        double denom=paramg[2]*f*sin1+paramg[3]*sig_m+paramg[4];
       // res=paramg[11]*f*f*sin1*sin1;
        res=(2.0+2.0*f*f-4.0*f*cos1);
        res=res+paramg[12]*f*sin1/denom;
        res=res+2.0*paramg[0]*f*sin1*(sig_m+paramg[8])+(cos1-f)*(1.0-f)*(paramg[9]+paramg[10]*sig_m);
        return res;
      } 

/********************************************************************************/
/********************************************************************************
*/
      double df1_spc0e_df(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/k0
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double denom=paramg[2]*f*sin1+paramg[3]*sig_m+paramg[4];
        double cos1=cosh(sinint);
        res=2.0*paramg[7]*f*sin1*sin1*(paramg[3]*sig_m+paramg[4]);
        res=res/(denom*denom*denom);
        res=res+2.0*(cos1-f);
        return res;
      } 
/********************************************************************************/
/********************************************************************************
*/
      double df1_spc0e_dsig_m(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        //p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/k0
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double p2fsin=paramg[2]*f*sin1;
        double denom=p2fsin+paramg[3]*sig_m+paramg[4];
        double cos1=cosh(sinint);
        res= 2.0*sin1*paramg[7]*f*f*(paramg[0]*cos1*(paramg[3]*sig_m+paramg[4])-sin1*paramg[3]);
        res=res/(denom*denom*denom);
        res=res+2.0*f*sin1*paramg[0];
        return res;
      } 
/********************************************************************************/
/********************************************************************************
*/
      double df2_spc0e_df(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        paramg[7]=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*paramg[7]
        paramg[8]=-sig_mt0
        paramg[9]=2/k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/k0
//+(cos1-f)*(1.0-f)*(paramg[9]+paramg[10]*sig_m)
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double denom=paramg[2]*f*sin1+paramg[3]*sig_m+paramg[4];
        double cos1=cosh(sinint);
        res=4.0*(f-cos1);
        res=res+paramg[12]*sin1*(paramg[3]*sig_m+paramg[4])/denom/denom;
        res=res+2.0*paramg[0]*sin1*(sig_m+paramg[8])-(paramg[9]+paramg[10]*sig_m)*(1.0-2.0*f+cos1);
        return res;
      } 
/********************************************************************************/
/********************************************************************************
*/
      double df2_spc0e_dsig_m(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        paramg[7]=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*paramg[7]
        paramg[8]=-sig_mt0
        paramg[9]=2/k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/k0
//+(cos1-f)*(1.0-f)*(paramg[9]+paramg[10]*sig_m)
*/

        double res;
        double sinint=paramg[0]*(sig_m+paramg[6]);
        double sin1=sinh(sinint);
        double p2fsin=paramg[2]*f*sin1;
        double denom=p2fsin+paramg[3]*sig_m+paramg[4];
        double cos1=cosh(sinint);
        res=-4.0*f*sin1*paramg[0];
        res=res+paramg[12]*f*(paramg[0]*cos1*(paramg[3]*sig_m+paramg[4])-paramg[3]*sin1)/denom/denom;
        res=res+2.0*paramg[0]*f*(paramg[0]*cos1*(sig_m+paramg[8])+sin1)+(1.0-f)*(paramg[10]*(cos1-f)+paramg[0]*sin1*(paramg[9]+paramg[10]*sig_m));
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double f2_bis_spc0e(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7//previous_porosity
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
        if (f>0.99) res=0.0;
        else if (f<1e-10) res=1.5*(paramg[9]+paramg[10]*sig_m);
        else res=paramg[5]-f+1.5*(1.0-f)*(paramg[9]+paramg[10]*sig_m);
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double df2_bis_spc0e_dsig_m(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7//previous_porosity
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
        if (f>0.99) res=0.0;
        else if (f<1e-10) res=1.5*paramg[10];
        else res=1.5*(1.0-f)*(paramg[10]);
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double df2_bis_spc0e_df(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7//previous_porosity
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
        res=-1.5*(paramg[9]+paramg[10]*sig_m)-1.0;
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double f2_bis(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=sigT_eq^2/sig0^2
        paramg[2]=9sig0^2 k
        paramg[3]=-6 mu
        paramg[4]=6 mu(3k delta t eps_m_point-\sig_m_t0)
        paramg[5]=f_t0
        paramg[5]=delta t eps_m_point
*/
        double res;
//        res=paramg[5]-f+3.0*(1.0-f)*(paramg[7]);//version avec trace epsilon
//       res=paramg[5]-f+(1.0-f)*(-paramg[8]-sig_m)/paramg[9]*3.0;//version avec trace epsilon_p
        if (f<1e-10) res=(-paramg[8]-sig_m)/paramg[9]*3.0;
        else if (f>0.99) res=0.0;
        else res=paramg[5]-f+(1.0-f)*(-paramg[8]-sig_m)/paramg[9]*3.0;
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double df2_bis_dsig_m(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7//previous_porosity
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
        //res=0.0;//version avec trace epsilon
 //       res=-(1.0-f)/paramg[9]*3.0;//version avec trace epsilon_p
        if (f<1e-10) res=-1.0/paramg[9]*3.0;
        else if (f>0.99) res=0.0;
        else res=-(1.0-f)/paramg[9]*3.0;//version avec trace epsilon_p
        return res;
      } 
/********************************************************************************/
/********************************************************************************/
      double df2_bis_df(double sig_m,double f,double paramg[13]){
/*      paramg[0]=1.5/sig0
        paramg[6]=P_b
        paramg[1]=27*sig0*mu0*k
        paramg[2]=9sig0^2 k(mu+mu0)
        paramg[3]=-6 mu*mu0((k+k_0)/k0)
        paramg[4]=6 mu*mu0(k/k0 delta t tau_m_point+(k+k0)/k0\sig_m_t0)
        //%paramg[5]=9kmu^2/mu0\sig0 taupointD:taupointD
        //%paramg[7]=18ksig0(mu+mu0)mu/mu0 taupointD:sigt0D
        //%paramg[8]=9ksig0(mu+mu0)^2/mu0 sigt0D:sigt0D
        *p7=mu2/mu0^2taupointD:taupointD+(1+mu/mu0)^2sigt0D:sigt0D+2mu/mu0(1+mu/mu0)taupointD:sigt0D
        paramg[5]=9ksig_0*p7//previous_porosity
        paramg[7]=121.5*mu0*mu0*k*k*p7*sig0*sig0=paramg[1]*paramg[1]*p7/6.0
        paramg[8]=-sig_mt0
        paramg[9]=2/3k0 (tau_m-taumt0+sig_mt0)
        paramg[10]=-2/3k0
        paramg[11]=27.0*sig02*sig02*mu0*mu0*p7*k*k*9.0=paramg[1]^2/3.0*p7//non 243*k*k/sig02
        paramg[12]=27.0*sig02*k*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)//non 27.0/sig02*k/mu0*(mu taupointD:sigt0D+(mu+mu0)*sigt0D:sigt0D)
        paramg[9]=2/3k (k/k0(tau_m-taumt0)+(1+k/k0)sig_mt0)
        paramg[10]=-2/3k(1+k/k0)
*/
        double res;
//        res=-3.0*(paramg[7])-1.0;//version avec trace epsilon
        res=-((-paramg[8]-sig_m)/paramg[9]*3.0)-1.0  ;//version avec trace epsilon_p
        return res;
      } 
/********************************************************************************/
/********************************************************************************/












