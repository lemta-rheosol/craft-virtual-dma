/**************************************************************************/
/* G. Boittin
   LMA/CNRS
   august 04th 2014
   
   header file for gurson and jacobian functions

*/
/**************************************************************************/
/* 
*/


#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>
#include <GURSON_materials.h>

      //double f_gurson2(double x,double paramg[7]);
      //double df_f_gurson2(double x,double paramg[8]);
      //double h2f_gurson2(double x,double paramg[7]);
      double h2f_gurson(double x,double paramg[]);
      //double df_h2f_gurson2(double x,double paramg[7]);
      double f_gurson(double x,double y,double paramf[5]);
      double f1(double sig_m,double f,double paramg[10]);
      double df1_dsig_m(double sig_m,double f,double paramg[10]);
      double df1_df(double sig_m,double f,double paramg[10]);
     // double f_gurson_spc0e(double x,double param_spc0e[7]);
     // double h2f_gurson_spc0e(double x,double param_spc0e[7]);
      double f1_spc0e(double sig_m,double f,double paramg[13]);
      //double f2_spc0e(double sig_m,double f,double paramg[13]);
      double df1_spc0e_df(double sig_m,double f,double paramg[13]);
      double df1_spc0e_dsig_m(double sig_m,double f,double paramg[13]);
      //double df2_spc0e_df(double sig_m,double f,double paramg[13]);
      //double df2_spc0e_dsig_m(double sig_m,double f,double paramg[13]);
      double f2_bis_spc0e(double sig_m,double f,double paramg[13]);
      double df2_bis_spc0e_df(double sig_m,double f,double paramg[13]);
      double df2_bis_spc0e_dsig_m(double sig_m,double f,double paramg[13]);
      double f2_bis(double sig_m,double f,double paramg[13]);
      double df2_bis_df(double sig_m,double f,double paramg[13]);
      double df2_bis_dsig_m(double sig_m,double f,double paramg[13]);
      //double f1_bis(double sig_m,double f,double paramg[13]);
      //double df1_bis_df(double sig_m,double f,double paramg[13]);
      //double df1_bis_dsig_m(double sig_m,double f,double paramg[13]);
      //double df1_dsig_eq(double sig_m,double f,double paramg[10]);
      //double df1_seq_df(double sig_m,double f,double paramg[10]);
      //double df2_seq_df(double sig_m,double f,double paramg[10]);
      //double df2_dsig_eq(double sig_m,double f,double paramg[10]);


