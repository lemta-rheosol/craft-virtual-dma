#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>
#include <GURSON_materials.h>
#include <gurson_functions.h>
#include <utils_materials.h>
#include <variables.h>
#include <LE_Cmatrix.h>
#include <Crystal_Tools.h>


#ifdef _OPENMP
#include <omp.h>
#endif
/*######################################################################*/
/* G. Boittin
   LMA/CNRS
   October 28th 2014
   
   functions to manage gurson type plasticity behavior
   only isotropic case for elasticity is yet implemented
*/
/*######################################################################*/

/*#Headers in GURSON_materials.h#*/
/*int reso_gurson_basic_poro_evo( 
		   void *param, Variables *sv,double dt);
int reso_gurson_spc0e_poro_evo(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]);*/


/*######################################################################*/
//p,dt,sv,e,s,ep,sp,ps,cump
int reso_gurson_basic_poro_evo(
		   void *param, Variables *sv,double dt){ 
/*######################################################################*/
//calcul des paramètres indépendants du point des fonctions

//boucle sur i et parallélisation

//calcul des paramètres dépendants du point des fonctions

//choix de la méthode de résoltion en fonction de la valeur de (sigma_m+p_b)

//résolution

//Controls

// Mise à jour des variables
  int i;
  int status=0;
  int point_pb=0;
  GURSON_param *p;
  int N;

  N = sv->np;
  p  = ((GURSON_param *)param);
/*######################################################################*/
//déclarations des variables
/*######################################################################*/
//copy of variables
  double (*e)[6];
  double (*s)[6];
  double (*ep)[6];
  double (*sp)[6];
  double (*vporo);
  double (*pvporo);
  double (*vpb);
  double (*cump);
  double (*pvcump);
  e = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  s = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  ep = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sp   = (double (*)[6])(ptr_variable
          ("previous stress", sv, param)->values);
  cump = (double (*))(ptr_variable
         ("cumulated_plastic_strain", sv, param)->values);
  pvcump = (double (*))(ptr_variable
         ("previous_cum_plastic_strain", sv, param)->values);
  vporo   = (double (*))(ptr_variable("porosity", sv, param)->values);
  pvporo   = (double (*))(ptr_variable("previous_porosity", sv, param)->values);

/*######################################################################*/
//calcul des paramètres indépendants du point des fonctions
/*######################################################################*/
  double  sig0=(p->sig0);
  double  sig02=sig0*sig0;
  double  t [6];
  double  k, trs;
  double  err, devstdevsp, steq2, trsp;
  double  sig_m, dlb,pressure,porosity;
  int     j;
  double  paramf [5];// parameters for function f_gurson=f(seq2,sigm)
  double  param_syst_2eq [10];

  param_syst_2eq[6]=paramf[3] =0.0;
  k = (3.0*(p->lb)+2.0*(p->mu))/3.0;//bulk modulus

  err = 1.e-9;
  switch (p->pressure_int){
    case NO_PRESSURE:
      pressure= param_syst_2eq[6]=paramf[3]=0.0;
      break;
    case FIXED_VALUE_OF_PRESSURE:
      pressure=param_syst_2eq[6]=paramf[3]=p->bpp.fbpp->pressure_fixed;
      break;
    case LINEAR_EVO_OF_PRESSURE:
//      printf("Not yet impemented : pressure= pressure (t=0)");//q à hervé : où puis-je stocker cette variable macro? ou comment ai-je accès au temps tot?
//      mis dans les paramètres à voir si on stocke ailleurs des variables macros
//      ou si 'on considère des paramètres matériaux variables (par exemple fonction de la température), du coup prévoir une procédure de mise à jour des paramètres
      pressure=param_syst_2eq[6]=paramf[3]=p->bpp.lbpp->pressure_actu;//+dt*p->bpp.lbpp->pressure_slop;
      break;
    case TABULATED_EVO_OF_PRESSURE:
      pressure=paramf[3]=  p->bpp.tbpp->pressure_actu;
      pressure=paramf[3];
      param_syst_2eq[6]=p->bpp.tbpp->pressure_actu;
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      status=-10;
      printf("calculated evo of pressure is Not yet implemented\n"); 
      vpb   = (double (*))
         (ptr_variable("bubble_pressure", sv, param)->values);
      break;
  }

  if (status!=0) return status;
  paramf[1] = 1.5/sig0;
  paramf[0] = p->q3/(sig02);

        param_syst_2eq[0]=1.5/sig0;
  //      param_syst_2eq[1]=sigT_eq^2/sig0^2
        param_syst_2eq[2]=9.0*sig0*k;
        param_syst_2eq[3]=-6.0*p->mu;
    //    param_syst_2eq[4]=6.0*p-<mu*(3.0*k deltaeps_m_point+\sig_m_t0)
    //    param_syst_2eq[5]=3sigDT:sigD_t0
  //      param_syst_2eq[6]=P_b
    //    paramg[8]=-3keps_m-epsm_t0-sig_m_t0
        param_syst_2eq[9]=3.0*k;

/*######################################################################*/
//boucle sur i et parallélisation
/*######################################################################*/

#pragma omp parallel for			\
  default(none)					\
  private(i,j)					\
  firstprivate(paramf,param_syst_2eq)					\
  firstprivate(pressure)			\
  private(t,trsp,trs,devstdevsp,steq2,sig_m,dlb,porosity)					\
  shared(dt,N,k,sig0,err,sig02,point_pb,stderr,stdout)					\
  shared(p,e, ep, s, sp,vpb,vporo,pvporo,cump,pvcump,status)
  for (i=0;i<(N);i++){
/*######################################################################*/
//calcul des paramètres dépendants du point des fonctions
/*######################################################################*/
       int if_poro_almost_1=0;
       porosity=p->q1*(pvporo[i]);//+(1.0-pvporo[i])*((e[i][0]+e[i][1]+e[i][2])
         //         -(ep[i][0]+ep[i][1]+ep[i][2])));
       if (porosity>0.989) {if_poro_almost_1=1;}
       paramf[4]=-1.0-porosity*porosity;
       paramf[2]=2.0*porosity;
//   sigma trial (trial elastic stress) 

       for (j=0;j<6;j++){
          t[j] = sp[i][j] + 2.0 * (p->mu) * (e[i][j] - ep[i][j]);
       }
     
       for (j=0;j<3;j++){
          t[j] += (p->lb)*(e[i][1]+e[i][2]+e[i][0]);
          t[j] -= (p->lb)*(ep[i][1]+ep[i][2]+ep[i][0]);
       }

//     Now t[j] is sigma_trial
//     Hydrostatic part of the trial elastic stress and previous stress
       trsp = (sp[i][0]+sp[i][1]+sp[i][2])/3.0;
       trs=(t[0]+t[1]+t[2])/3.0;

//      Deviator part of the trial elastic stress 

       for (j=0;j<3;j++){
          t[j] = t[j]-trs;
       }

      // double speq=0.5*((sp[i][0]-sp[i][1])*(sp[i][0]-sp[i][1])+(sp[i][2]-sp[i][0])*(sp[i][2]-sp[i][0])+(sp[i][2]-sp[i][1])*(sp[i][2]-sp[i][1]))+3.0*(sp[i][3]*sp[i][3]+sp[i][4]*sp[i][4]+sp[i][5]*sp[i][5]);
//      Now t[j] is not any more sigma_trial but the deviatoric part of sigma_trial


//      Contracted product of the deviatoric part of the trial elastic stress and the previous deviatoric part of the stress
       devstdevsp=0.0;
       for (j=0;j<3;j++){
          devstdevsp = devstdevsp +((sp[i][j]-trsp) * t[j]);
       }
       for (j=3;j<6;j++) {
          devstdevsp = devstdevsp+(sp[i][j] * t[j]);
// ou          devstdevsp = devstdevsp+2*(sp[j][i] * t[j][i]), si je n'ai pas la bonne notation, à vérifier, notation de Kelvin  OK
       }
//       

//       Von Mises stress of the trial elastic stress to the power two
       steq2 = 1.5*((t[1]*t[1])+(t[2]*t[2])+(t[0]*t[0]))
                   +3.0*(t[4]*t[4]+t[5]*t[5]+t[3]*t[3]);  

 //      Calculating last parameters of function f_gurson2
       

  ////      param_syst_2eq[0]=1.5/sig0;
         param_syst_2eq[1]=steq2/sig02;
  ////      param_syst_2eq[2]=9.0*sig0*k;
  ///      param_syst_2eq[3]=-6.0*p->mu;
   ///     param_syst_2eq[4]=6.0*p->mu*(3.0*k deltaeps_m_point+\sig_m_t0)
   ///     param_syst_2eq[6]=pressure
         param_syst_2eq[4]=6.0*p->mu*(trsp+k*(
                   (e[i][0]+e[i][1]+e[i][2])
                  -(ep[i][0]+ep[i][1]+ep[i][2])
                  ));
         param_syst_2eq[5]=pvporo[i];
     //    param_syst_2eq[5]=3.0*devstdevsp/sig02;
///        param_syst_2eq[5]=3.0*sigDT:sigD_t0
  //       param_syst_2eq[7]=trsp;
         param_syst_2eq[7]=e[i][1]+e[i][2]+e[i][0]-(ep[i][1]+ep[i][2]+ep[i][0]);
    //    paramg[8]=-3keps_m-epsm_t0-sig_m_t0
         param_syst_2eq[8]=k*(-(e[i][0]+e[i][1]+e[i][2])
                  +(ep[i][0]+ep[i][1]+ep[i][2]))-trsp;
/*######################################################################*/
//cas elastique?
/*######################################################################*/
       double test_trial=f_gurson(trs,steq2,paramf);
       int actu_porosity=1;int to_test=1;

/*######################################################################*/
//choix de la méthode de résolution en fonction de la valeur de (sigma_m+p_b)//et résolution
/*######################################################################*/
     if (!if_poro_almost_1){
       if (test_trial>0){
         double abs_t_and_pb=fabs(trs+param_syst_2eq[6]);
            mon_pointeur_fonction_2var *my_f1= f1;
            mon_pointeur_fonction_2var *my_f2= f2_bis;
            mon_pointeur_fonction_2var *my_df1_dsig_m= df1_dsig_m;
            mon_pointeur_fonction_2var *my_df2_dsig_m= df2_bis_dsig_m;
            mon_pointeur_fonction_2var *my_df1_df= df1_df;
            mon_pointeur_fonction_2var *my_df2_df= df2_bis_df;
         double porosity_test=pvporo[i]+3.0*(1.0-pvporo[i])*(-param_syst_2eq[8]-trsp)/param_syst_2eq[9];
         //if (i==16){printf("porosity=%g,porosity_test=%g\n",porosity,porosity_test);}
         if (abs_t_and_pb<(err*err)){
         //we consider abs_t_and_pb=0, we have then just to solve a classic Mises problem
           dlb=((sqrt(steq2)/porosity)-sig0)*sig0/param_syst_2eq[0]/9.0;// dlb is dot(lambda)*dt
           sig_m=trs;
         }
         else if ((porosity_test>5.0e-5) && (porosity>5.0e-5)){
         //general method
            double my_trs_test=trs;
            if (fabs(trs-trsp)>0.3*sig0){
                 double init_trs=trsp;
                 if (trsp>trs) init_trs=trs; 
                 init_trs=init_trs-fabs(trs-trsp)/2.0;
                 double inter=fabs(trs-trsp)/100.0;
                 double my_min=fabs(f1(init_trs,porosity_test,param_syst_2eq));
                 my_trs_test=init_trs;
                 for (j=1;j<200;j++){
                    double my_test=init_trs+j*inter;
                    double res0=fabs(f1(my_test,porosity_test,param_syst_2eq));
                    if (res0 < my_min) {my_min=res0;my_trs_test=my_test;}
                 }
				 
            }
            //sig_m=trsp+(trs-trsp)/2.0;porosity=porosity_test;
            sig_m=my_trs_test;//porosity=test_porosity;
            //sig_m=trs;
            double f_trial_lim=test_trial; if (f_trial_lim>5.0)f_trial_lim=5.0;
            int newton_status= newton_2eq(&sig_m,&porosity, my_f1, my_df1_dsig_m,
                 my_f2,my_df2_df, my_df1_df, my_df2_dsig_m,
                 param_syst_2eq, err);
     /*       if ((newton_status!=0)||(porosity>0.99)) {
                  printf("point %i, test_trial=%g, first newton_status=%i, porosity=%g, trs_ini=%g,trs=%g,trsp=%g,sig_m=%g,f_ini=%g,f_ini=%g \n",i,test_trial,newton_status,porosity,my_trs_test,trs,trsp,sig_m,f1(my_trs_test,porosity_test,param_syst_2eq),f1(trs,former_porosity,param_syst_2eq));
                   printf("f2_bisini=%g, f2bis_ini=%g\n",f2_bis(my_trs_test,porosity_test,param_syst_2eq),f2_bis(trs,former_porosity,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",trs,former_porosity,f1(trs,former_porosity,param_syst_2eq),f2_bis(trs,former_porosity,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",my_trs_test,former_porosity,f1(my_trs_test,former_porosity,param_syst_2eq),f2_bis(my_trs_test,former_porosity,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",my_trs_test,porosity_test,f1(my_trs_test,porosity_test,param_syst_2eq),f2_bis(my_trs_test,porosity_test,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",trs,porosity_test,f1(trs,porosity_test,param_syst_2eq),f2_bis(trs,porosity_test,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",trsp,former_porosity,f1(trsp,former_porosity,param_syst_2eq),f2_bis(trsp,former_porosity,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",trsp+(trs-trsp)/2.0,former_porosity,f1(trsp+(trs-trsp)/2.0,former_porosity,param_syst_2eq),f2_bis(trsp+(trs-trsp)/2.0,former_porosity,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",trsp+(trs-trsp)/2.0,porosity_test,f1(trsp+(trs-trsp)/2.0,porosity_test,param_syst_2eq),f2_bis(trsp+(trs-trsp)/2.0,porosity_test,param_syst_2eq));
                   printf("sig_m=%g, porosity=%g, f1=%g, f2bis_ini=%g\n",my_trs_test,test_porosity,f1(my_trs_test,test_porosity,param_syst_2eq),f2_bis(my_trs_test,test_porosity,param_syst_2eq));
            }*/

            dlb=sig02*(param_syst_2eq[3]*sig_m+param_syst_2eq[4])/(-param_syst_2eq[3])/(param_syst_2eq[2]*porosity*sinh(param_syst_2eq[0]*(sig_m+pressure)));// dlb is dot(lambda)*dt
            if ((porosity>0.99)||(newton_status==-1)||(dlb<-err)){
              double sig_m=trsp; 
              if (fabs(trsp)>fabs(trs)) {sig_m=trs;}
              porosity=porosity_test;
              newton_status = newton_2eq(&sig_m,&porosity, my_f1, my_df1_dsig_m,
                 my_f2,my_df2_df, my_df1_df, my_df2_dsig_m,
                 param_syst_2eq, err);
              if ((newton_status!=0)||(porosity>=0.99)) fprintf(stderr,"point %i, second newton_status=%i, porosity=%g,porosity-ini=%g trs_ini=%g,trs=%g,sig_m=%g\n",i,newton_status,porosity,porosity_test,my_trs_test,trs,sig_m);
              dlb=sig02*(param_syst_2eq[3]*sig_m+param_syst_2eq[4])/(-param_syst_2eq[3])/(param_syst_2eq[2]*porosity*sinh(param_syst_2eq[0]*(sig_m+pressure)));
              if (dlb<-err) fprintf(stderr,"point =%i,trsp=%g,sig_m=%g,porosity=%g,stat=%i,err=%g,f1=%g,f2=%g,status=%i\n",i,trsp,sig_m,porosity,status,err,my_f1(sig_m,porosity,param_syst_2eq),f2_bis(sig_m,porosity,param_syst_2eq),status);
              if (((newton_status!=0)&&(newton_status!=-4))||(porosity>=0.99)){
                 fprintf(stderr,"point =%i, newton_status=%i, porosity=%g, trs_ini=%g,trs=%g,sig_m=%g\n",i,newton_status,porosity,my_trs_test,trs,sig_m);
                 for (j=0;j<6;j++){
                   printf("t[%i]=%g\n",j,t[j]);
                 }
		 if (porosity>=0.99){
		    if_poro_almost_1=1;porosity=0.99;sig_m=trs;newton_status=0;
                    actu_porosity=0;
                    if (porosity_test<0.2) {newton_status=0;porosity=0.0;sig_m=trs;}
		    else {fprintf(stdout,"point i=%i is a void\n",i);}
		 }
                 fprintf(stderr,"param[0]=%g\n",param_syst_2eq[0]);
                 fprintf(stderr,"param[1]=%g\n",param_syst_2eq[1]);
                 fprintf(stderr,"param[2]=%g\n",param_syst_2eq[2]);
                 fprintf(stderr,"param[3]=%g\n",param_syst_2eq[3]);
                 fprintf(stderr,"param[4]=%g\n",param_syst_2eq[4]);
                 fprintf(stderr,"param[5]=%g\n",param_syst_2eq[5]);
                 fprintf(stderr,"param[6]=%g\n",param_syst_2eq[6]);
                 fprintf(stderr,"param[7]=%g\n",param_syst_2eq[7]);
                 fprintf(stderr,"param[8]=%g\n",param_syst_2eq[8]);
                 fprintf(stderr,"param[9]=%g\n",param_syst_2eq[9]);
                 fprintf(stderr," point =%i, newton_status=%i, porosity=%g, trs_ini=%g,trs=%g,sig_m=%g\n",i,newton_status,porosity,my_trs_test,trs,sig_m);
                 for (j=0;j<6;j++){
                   fprintf(stderr,"t[%i]=%g\n",j,t[j]);
                 } 
                 status=-1;
                 exit(1);
              }

	      if (porosity<0.0){
   		porosity=0.0;
   		if (test_trial<err){
   			dlb=0.0;
   			sig_m=trs;
   		}
   		else{
    			dlb=(sqrt(steq2)-sig0)*sig0/(6.0*p->mu*p->q3);
    		if (dlb<0.0) {sig_m=trs;fprintf(stderr,"point %i mises direct ,porosity=0 after newton neg dlb=-%g\n",i,dlb);dlb=0.0;}
    	      }
            }
			}
            if (newton_status==-4) {
    	       dlb=(sqrt(steq2)-sig0)*sig0/(6.0*p->mu*p->q3);
               fprintf(stderr,"in newton point %i,porosity=0 after newton dlb=%g\n",i,dlb);
               newton_status=0;porosity=0.0;
               if (dlb<0.0) {
                   fprintf(stderr,"pb in newton point %i,porosity=0 after newton neg dlb=-%g\n",i,dlb);
                   dlb=0.0;
                   exit(1);
               }
            }
            else if (newton_status != 0) {
               point_pb=i;
               fprintf(stderr,"pb in newton point %i,status=%i,pvporo=%g\n",point_pb,newton_status,pvporo[i]);
               status=-8;
                 for (j=0;j<6;j++){
                   printf("t[%i]=%g\n",j,t[j]);
                 }
            }

         }
         else {
           if (test_trial<err){
             dlb=0.0;porosity=0.0;
             sig_m=trs;
           }
           else{
    			        dlb=(sqrt(steq2)-sig0)*sig0/(6.0*p->mu*p->q3);porosity=0.0;
             if (dlb<0.0) {
               dlb=0.0;
               sig_m=trs;
               fprintf(stderr,"point %i mises direct ,porosity=0 after newton neg dlb=-%g\n",i,dlb);
             }
           }
         }
       }//end plastic case
       else {// elastic case
         sig_m=trs;
         dlb=0.0;
       }//end elastic case
    }
    else {dlb=0.0;}

/*######################################################################*/
//First Controls
/*######################################################################*/
       if ((dlb<-err)&&(!if_poro_almost_1)&&(porosity>1.0e-5)) {
//         statusN[i]=-3;
         status=-5;
         point_pb=i;
         fprintf(stderr,"Negative plasticity schema basic ,point i=%i, dlb=%g,sig_m=%g,trs=%g,trsp=%g,test_trial=%g !!!!!!! \n",i,dlb,sig_m,trs,trsp,test_trial);
         fprintf(stderr, "sig_m=%g \n",sig_m);
         fprintf(stderr, "trs=%g,trsp=%g \n",trs,trsp);
         fprintf(stderr,"param[0]=%g\n",param_syst_2eq[0]);
         fprintf(stderr,"param[1]=%g\n",param_syst_2eq[1]);
         fprintf(stderr,"param[2]=%g\n",param_syst_2eq[2]);
         fprintf(stderr,"param[3]=%g\n",param_syst_2eq[3]);
         fprintf(stderr,"param[4]=%g\n",param_syst_2eq[4]);
         fprintf(stderr,"param[5]=%g\n",param_syst_2eq[5]);
         fprintf(stderr,"param[6]=%g\n",param_syst_2eq[6]);
         fprintf(stderr,"param[7]=%g\n",param_syst_2eq[7]);
         fprintf(stderr,"param[8]=%g\n",param_syst_2eq[8]);
         fprintf(stderr,"param[9]=%g\n",param_syst_2eq[9]);
         fprintf(stderr, "porosity=%g,pvporo=%g\n",porosity,pvporo[i]);
         fprintf(stderr, "if_poro_almost_1=%i",if_poro_almost_1);
         dlb=0.0;actu_porosity=1;//statusN[i]=-2;
       }
       else if ((dlb<0)&&(!if_poro_almost_1)&&(porosity>1.0e-5))  {
         fprintf(stderr,"plasticty negative but very close to zero, negecting plasticity,point i=%i \n",i);
         dlb=0.0;actu_porosity=1;
       }
       if ((isnan(sig_m))&&(!if_poro_almost_1)){
             fprintf(stderr,"sig_m not a number!,porosity=%g \n",porosity);//return-2;
             status=-9;
             point_pb=i;
             dlb=0.0;sig_m=trs;
             actu_porosity=0;
             fprintf(stdout,"point i=%i is a has been considered elastic\n",i);
             to_test=0;
       }
/*######################################################################*/
// Mise à jour des variables
/*######################################################################*/
         double sig_D_coef=1.0+(6.0*p->q3*(p->mu)*dlb/(sig0*sig0));
         sig_D_coef=1.0/sig_D_coef;
         for (j=0;j<6;j++){
           s[i][j] = sig_D_coef*t[j];
         } // s is the stress deviator
         double verif_seq2;
         verif_seq2 = 1.5*((s[i][1]*s[i][1])+(s[i][2]*s[i][2])+
                     (s[i][0]*s[i][0]))
                   +3.0*(s[i][4]*s[i][4]+s[i][5]*s[i][5]+
                    s[i][3]*s[i][3]); 
         double dotepsp[6];
         for (j=0;j<3;j++){
          dotepsp[j] = 
           3.0*dlb/sig0*porosity*sinh(param_syst_2eq[0]*(sig_m+param_syst_2eq[6]));
          //ps[i][j]=ps[i][j]+dotepsp[j];
         }   
         for (j=0;j<6;j++){
          dotepsp[j] =dlb*3.0/(sig0*sig0)*s[i][j];
          //ps[i][j]=ps[i][j]+dotepsp[j];
         }
         for (j=0;j<3;j++){
          s[i][j] = s[i][j]+sig_m;
         }
       //now s is the stress
         double normdoteps=0.0;
         for (j=0;j<6;j++){
            normdoteps=normdoteps+sqrt(2.0/3.0*dotepsp[j]*dotepsp[j]);
         }
         cump[i]=pvcump[i]+normdoteps;
         if (porosity>0.99){
            if_poro_almost_1=1;
            porosity=0.995;
            fprintf(stdout,"point i=%i is a void\n",i);
         } 
         if (actu_porosity) vporo[i]=porosity/(p->q1);
         else vporo[i]=pvporo[i];//+(1.0-pvporo[i])*((e[i][0]+e[i][1]+e[i][2])
           //       -(ep[i][0]+ep[i][1]+ep[i][2]));
         if (if_poro_almost_1){ 
           for (j=0;j<3;j++){
             s[i][j] = pressure;
           }
           for (j=3;j<6;j++){
             s[i][j] = 0.0;
           }
         };
/*######################################################################*/
//Last Controls
/*######################################################################*/
         double verif_sol=f1(sig_m,porosity,param_syst_2eq);
         if ((verif_sol > (1.e3*err))&&(!if_poro_almost_1)&&(to_test)){
 
          fprintf(stderr,"verif_sol positive, F(sigma)=%g\n",verif_sol);
          fprintf(stderr,"sig_m=%g\n",sig_m);
          fprintf(stderr,"porosity=%g\n",porosity);
          fprintf(stderr,"pvporosity=%g\n",pvporo[i]);
          fprintf(stderr,"trs=%g\n",trs);
          fprintf(stderr,"steq2=%g\n",steq2);
          fprintf(stderr,"verif_seq2=%g\n",verif_seq2);
          fprintf(stderr,"param[0]=%g\n",param_syst_2eq[0]);
          fprintf(stderr,"param[1]=%g\n",param_syst_2eq[1]);
          fprintf(stderr,"param[2]=%g\n",param_syst_2eq[2]);
          fprintf(stderr,"param[3]=%g\n",param_syst_2eq[3]);
          fprintf(stderr,"param[4]=%g\n",param_syst_2eq[4]);
          fprintf(stderr,"param[5]=%g\n",param_syst_2eq[5]);
          fprintf(stderr,"param[6]=%g\n",param_syst_2eq[6]);
          fprintf(stderr,"param[7]=%g\n",param_syst_2eq[7]);
          fprintf(stderr,"param[8]=%g\n",param_syst_2eq[8]);
          fprintf(stderr,"param[9]=%g\n",param_syst_2eq[9]);
          fprintf(stderr,"paramf[0]=%20.11g\n",paramf[0]);
          fprintf(stderr,"paramf[1]=%20.11g\n",paramf[1]);
          fprintf(stderr,"paramf[2]=%20.11g\n",paramf[2]);
          fprintf(stderr,"paramf[3]=%20.11g\n",paramf[3]);
          fprintf(stderr,"paramf[4]=%20.11g\n",paramf[4]);
//            statusN[i]=-2;
         status=-6;
         point_pb=i;
         fprintf(stderr,"pb=%i\n",i);

         }


  }// end for i
//C$OMP END DO
//C$OMP END PARALLEL
/*######################################################################*/
//Control on the global calculation
/*######################################################################*/
  if (status !=0) fprintf(stderr,"   bilan status2 :  point i=%i, status=%i point_pb=%i!!!!!!! \n",i,status,point_pb);
  if (point_pb!=0) {fprintf(stderr,"iteration NOK, status=%i, point_pb=%i\n",status,point_pb);status=-1;
                 for (j=0;j<6;j++){
                   printf("t[%i]=%g\n",j,t[j]);
                 }}
  return status;
}
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/

/*######################################################################*/
/*######################################################################*/
//p,dt,sv,e,s,ep,sp,ps,cump
int reso_gurson_spc0e_poro_evo(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]){ 

/*######################################################################*/
//calcul des paramètres indépendants du point des fonctions

//boucle sur i et parallélisation

//calcul des paramètres dépendants du point des fonctions

//choix de la méthode de résoltion en fonction de la valeur de (sigma_m+p_b)

//résolution

//Controls

// Mise à jour des variables
  int i;
  int status=0;
  int point_pb=0;
  GURSON_param *p;
  int N;

  N = sv->np;
  p  = ((GURSON_param *)param);
/*######################################################################*/
//déclarations des variables
/*######################################################################*/
  double (*e)[6];
  double (*s)[6];
  double (*ep)[6];
  double (*sp)[6];
  //double (*ps)[6];
  double (*vpb);
  double (*pvcump);
  double (*cump);
  double (*vporo);
  double (*pvporo);
  e = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  s = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  ep = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sp   = (double (*)[6])(ptr_variable
          ("previous stress", sv, param)->values);
  //ps = (double (*)[6])(ptr_variable("plastic_strain", sv, param)->values);
  cump = (double (*))(ptr_variable
         ("cumulated_plastic_strain", sv, param)->values);
  pvcump = (double (*))(ptr_variable
         ("previous_cum_plastic_strain", sv, param)->values);
  vporo   = (double (*))(ptr_variable("porosity", sv, param)->values);
  pvporo   = (double (*))(ptr_variable("previous_porosity", sv, param)->values);
/*######################################################################*/
//calcul des paramètres indépendants du point des fonctions
/*######################################################################*/
  //printf( "in solve_spc0e gurson, nombre de point=%i\n",N);
  int j;
  double paramf[5]  ;
  double param_spc0e_syst_2eq[13]  ;
  double taud[6], deltataud[6];
  double tau_ini[6];
  double pressure,porosity,sig_m;
  double k = (3.0*(p->lb)+2.0*(p->mu))/3.0;//bulk modulus
  double err=1.0e-9;
  double   mu0=L0->sub.ile->mu;
  double   musurmu0 =(p->mu/mu0);
  double  deuxmumu0=2.*p->mu*mu0;
  double ksurk0=p->k/L0->sub.ile->k;
  double  unplusksurk0=1.0+(p->k/L0->sub.ile->k);
  double  muplusmu0=(p->mu+L0->sub.ile->mu);
  double  sig0=p->sig0;
  double  sig02=p->sig0*p->sig0;
  double dlb;
  double k2=musurmu0*musurmu0;
  double k3=1.0+musurmu0;
  double k6=1.0/k3;;
  double k4=2.*musurmu0*k3;

  k3=k3*k3;
  param_spc0e_syst_2eq[0]=1.5/sig0;
  param_spc0e_syst_2eq[1]=27*sig0*mu0*k;
  param_spc0e_syst_2eq[2]=9.0*sig02* p->k*muplusmu0;
  param_spc0e_syst_2eq[3]=-3.0* deuxmumu0*unplusksurk0*sig0;
  param_spc0e_syst_2eq[10]=-2.0/(3.0*p->k)*unplusksurk0;
  param_spc0e_syst_2eq[6]=0.0;
  paramf[0]=p->q3/sig02;
  paramf[1]=1.5/p->sig0;

  switch (p->pressure_int){
    case NO_PRESSURE:
      pressure=0.0;
      param_spc0e_syst_2eq[6]=pressure;
      paramf[3]=pressure;
      break;
    case FIXED_VALUE_OF_PRESSURE:
      pressure= p->bpp.fbpp->pressure_fixed;
      param_spc0e_syst_2eq[6]=pressure;
      paramf[3]=pressure;
      break;
    case LINEAR_EVO_OF_PRESSURE:
      printf("LINEAR_EVO_OF_PRESSURENot yet tested");
      pressure=p->bpp.lbpp->pressure_actu;//+dt*p->bpp.lbpp->pressure_slop;
      param_spc0e_syst_2eq[6]=pressure;
      paramf[3]=pressure;
      break;
    case TABULATED_EVO_OF_PRESSURE:
      //printf("TABULATED_EVO_OF_PRESSURE Not yet tested");
      pressure=  p->bpp.tbpp->pressure_actu;
      param_spc0e_syst_2eq[6]=pressure;
      paramf[3]=pressure;
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      vpb   = (double (*))
         (ptr_variable("bubble_pressure", sv, param)->values);
      break;
  }

/*######################################################################*/
//boucle sur i et parallélisation
/*######################################################################*/
//  int statusN[N];
//  status=0;
#pragma omp parallel for			\
  default(none)					\
  private(i,j)					\
  firstprivate(paramf,param_spc0e_syst_2eq)					\
  firstprivate(pressure,porosity,point_pb)			\
  private(taud,deltataud)           \
  private(sig_m,dlb,tau_ini,status)					\
  shared(dt,N,err,k2,k3,k4,k6,mu0,musurmu0,deuxmumu0,ksurk0,unplusksurk0,muplusmu0,sig02)					\
  shared(p,L0,e, ep, s, sp,cump,pvcump,vporo,vpb,tau,pvporo,sig0,stderr,stdout)
  for(i=0; i<N; i++) {
    for(j=0;j<6;j++){tau_ini[j]=tau[i][j];}  
    double     trtau = tau[i][0]+tau[i][1]+tau[i][2];
    double err2=1e-4;
    int if_poro_almost_zero=0;
    int if_poro_almost1=0;

/*######################################################################*/
//calcul des paramètres dépendants du point des fonctions
/*######################################################################*/
    double    trep = (ep[i][0]+ep[i][1]+ep[i][2])/3.0;
    double    trsp = (sp[i][0]+sp[i][1]+sp[i][2])/3.0;
    double    taueq2=0.0;
    for (j=0;j<3;j++){
      taud[j]=tau_ini[j]-trtau/3.0;
      taueq2=taueq2+1.5*taud[j]*taud[j];
    }
    for (j=3;j<6;j++){
      taud[j]=tau_ini[j];
      taueq2=taueq2+3.0*taud[j]*taud[j];
    }
    /* elastic trial */
    //double speq2=0.0;
    for (j=0;j<3;j++){
      deltataud[j]=taud[j]-sp[i][j]+trsp-2.0*L0->sub.ile->mu*(ep[i][j]-trep);
      //speq2=speq2+1.5*(sp[i][j]-trsp)*(sp[i][j]-trsp);
    }
    for (j=3;j<6;j++){
      deltataud[j]=taud[j]-sp[i][j]-2.*L0->sub.ile->mu *ep[i][j];
      //speq2=speq2+3.0*(sp[i][j]-trsp)*(sp[i][j]-trsp);
    }
    double deltataum= trtau/3.0;
    deltataum=deltataum-trsp-3.0*L0->sub.ile->k*trep;
    double      deltataudsigp= 0.;
    double      deltatauddeltataud= 0.;
    double      sigpsigp= 0.;
    for (j=0;j<3;j++){
      deltataudsigp=deltataudsigp+deltataud[j]*(sp[i][j]-trsp);
      sigpsigp=sigpsigp+(sp[i][j]-trsp)*(sp[i][j]-trsp);
      deltatauddeltataud=deltatauddeltataud+deltataud[j]*deltataud[j];
    }
    for (j=3;j<6;j++){
      deltataudsigp=deltataudsigp+2.0*deltataud[j]*sp[i][j];
      sigpsigp=sigpsigp+2.0*sp[i][j]*sp[i][j];
      deltatauddeltataud=deltatauddeltataud+2.0*deltataud[j]*deltataud[j];
    }
    
    double  trst=trsp + deltataum* (p->k)/(L0->sub.ile->k+p->k);
  
    if (pvporo[i]>5.e-5){
      porosity=p->q1*(pvporo[i]);//+(1.0-pvporo[i])*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k));
      if (porosity>0.989) {if_poro_almost1=1;}
    }
    else porosity=0.0;
    if (porosity<0.0) { fprintf( stderr,"porosity neg at the begining of time step, porosity=%g at point i=%i,vporo=%g,deltataum=%g,trst=%g,3k0=%g\n",porosity,i,vporo[i],deltataum,trst,3.0*L0->sub.ile->k);porosity=0.0;}

    paramf[4]=-1.0-porosity*porosity;
    paramf[2]=2.0*porosity;

    double  steq2=0;
    double std[6];
    for (j=0;j<3;j++){
      std[j]=k6*(musurmu0*deltataud[j]+(1.+musurmu0)*(sp[i][j]-trsp));
      steq2=steq2+1.5*std[j]*std[j];
    }
    for (j=3;j<6;j++){
      std[j]=1.0/(1.0+musurmu0)*(musurmu0*deltataud[j]+(1.+musurmu0)*(sp[i][j]));
      steq2=steq2+3.0*std[j]*std[j];
    }

    double inter=k3*sigpsigp+k4*deltataudsigp+k2*deltatauddeltataud;
    param_spc0e_syst_2eq[4]=3.0 *deuxmumu0*(ksurk0* deltataum+unplusksurk0*trsp)*sig0;
    param_spc0e_syst_2eq[5]=pvporo[i];//9.0*p->k*sig0*inter;
    //    param_spc0e_syst_2eq[7]=param_spc0e_syst_2eq[1]*param_spc0e_syst_2eq[1]*inter/6.0;
    param_spc0e_syst_2eq[7]=inter*27.0*9.0*sig02*p->k*p->k*L0->sub.ile->mu*L0->sub.ile->mu/2.0;
    param_spc0e_syst_2eq[8]=-trsp;
    param_spc0e_syst_2eq[9]=2.0/(3.0*p->k)*(ksurk0*deltataum+unplusksurk0*trsp);
   //     param_spc0e_syst_2eq[11]=27.0*sig02*sig02*mu0*mu0*inter*9.0*k*k;
   //     param_spc0e_syst_2eq[11]=param_spc0e_syst_2eq[1]*param_spc0e_syst_2eq[1]*inter/3.0;
   //     param_spc0e_syst_2eq[12]=27.0*sig02*p->k*(L0->sub.ile->mu*deltataudsigp+(muplusmu0)*sigpsigp);;
    param_spc0e_syst_2eq[11]=243*p->k*p->k*sig02*inter*L0->sub.ile->mu*L0->sub.ile->mu;
    param_spc0e_syst_2eq[12]=-27.0*p->k*(p->mu*deltataudsigp+(muplusmu0)*sigpsigp);



/*######################################################################*/
//choix de la méthode de résolution en fonction de la valeur de (sigma_m+p_b)
/*######################################################################*/
    //double test_trial=f_gurson_spc0e(trst,param_spc0e);
    double test_trial2=f_gurson(trst,steq2,paramf);
    double abs_t_and_pb=fabs(trst+param_spc0e_syst_2eq[6]);
    int actu_porosity=1;int to_test=1;int if_poro_almost0=0;

    if ((!if_poro_almost1)){    
      if (porosity<5.e-5){
        actu_porosity=0;if_poro_almost0=1;
        if (test_trial2<err){
          dlb=0.0;
          sig_m=trst;
        }
        else{
          double param_mises[2];
          param_mises[0]=1.5*(sig02)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
          param_mises[1]=muplusmu0*sig02/3.0/deuxmumu0/p->q3;
          dlb=sqrt(param_mises[0])-param_mises[1];
          if (dlb<0.0) dlb=-dlb;
          sig_m=trst;
        }
      }
      else{
    
        if (test_trial2<err){
         dlb=0.0;
         sig_m=trst;
        }
        else{
          if (abs_t_and_pb<(err*err)){
//        special case where plasticity is activated and   sinh(3(trst+Pb)/2sig0) is 0 
            double param_mises[2];
            param_mises[0]=1.5*(sig02)*(1.0+porosity*porosity)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
            param_mises[1]=muplusmu0*sig02*(1.0+porosity*porosity)/3.0/deuxmumu0/p->q3;
            dlb=sqrt(param_mises[0])-param_mises[1];
            sig_m=trst;
  //       porosity=porosity+(1.0-porosity)*(tre-trep);
            if (dlb<0.0) dlb=-dlb;

          }

          else{
            if (porosity<5.e-5){
                fprintf(stderr,"I should NOT be here\n");
            }

            mon_pointeur_fonction_2var *my_f1= f1_spc0e;
            mon_pointeur_fonction_2var *my_df1_dsig_m= df1_spc0e_dsig_m;
            mon_pointeur_fonction_2var *my_df1_df= df1_spc0e_df;
            mon_pointeur_fonction_2var   *my_f2= f2_bis_spc0e;
            mon_pointeur_fonction_2var   *my_df2_df= df2_bis_spc0e_df;
            mon_pointeur_fonction_2var   *my_df2_dsig_m= df2_bis_spc0e_dsig_m;
            double porosity_test=porosity+(1.0-pvporo[i])*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k)/10.0;

            if (porosity_test<0.0)porosity_test=porosity;
            if (porosity_test>1.0)porosity_test=porosity;
            sig_m=trst;
            int newton_status= newton_2eq(&sig_m,&porosity, my_f1, my_df1_dsig_m,
                 my_f2,my_df2_df, my_df1_df, my_df2_dsig_m,param_spc0e_syst_2eq, err);

            dlb=(param_spc0e_syst_2eq[3]*sig_m+param_spc0e_syst_2eq[4])/(deuxmumu0*p->q3*27.0*p->k*porosity*sinh(param_spc0e_syst_2eq[0]*(sig_m+pressure)));

            if (newton_status==-4){
              double param_mises[2];
              param_mises[0]=1.5*(sig02)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
              param_mises[1]=muplusmu0*sig02/3.0/deuxmumu0/p->q3;
              dlb=sqrt(param_mises[0])-param_mises[1];
              actu_porosity=0;if_poro_almost0=1;
              if (dlb<0.0) {dlb=-dlb;fprintf(stderr,"point i=%i param_mises[0]=%g,param_mises[1]=%g,dlb=%g,porosity=%g,trst=%g\n",i,param_mises[0],param_mises[1],dlb,porosity,trst );}
              sig_m=trst;
            }
            else if ((newton_status==-1)){
              porosity= (pvporo[i]+(1.0-pvporo[i])*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k));
              sig_m=trst+3.0*p->k*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k);
              newton_status= newton_2eq(&sig_m,&porosity, my_f1, my_df1_dsig_m,
                 my_f2,my_df2_df, my_df1_df, my_df2_dsig_m,
                 param_spc0e_syst_2eq, err);
            dlb=(param_spc0e_syst_2eq[3]*sig_m+param_spc0e_syst_2eq[4])/(deuxmumu0*p->q3*27.0*p->k*porosity*sinh(param_spc0e_syst_2eq[0]*(sig_m+pressure)));
            }
            double deltaporo=porosity-pvporo[i];
            if (deltaporo>0.2) {
              fprintf(stderr,"point i=%i was bad calculated, porosity=%g,vporo[i]=%g,sig_m=%g,trst=%g\n",i,porosity,pvporo[i],sig_m,trst );
              double poro_theo= (pvporo[i]+(1.0-pvporo[i])*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k));
              fprintf(stderr,"deltataum=%g,3k0=%g,poro_theo=%g,f1=%g\n",deltataum,3.0*L0->sub.ile->k,(pvporo[i]+(1.0-pvporo[i])*(deltataum)/(3.0*L0->sub.ile->k+3.0*p->k)),f1_spc0e(sig_m,porosity,param_spc0e_syst_2eq));
              if (poro_theo<0.0){
                double param_mises[2];
                param_mises[0]=1.5*(sig02)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
                param_mises[1]=muplusmu0*sig02/3.0/deuxmumu0/p->q3;
                dlb=sqrt(param_mises[0])-param_mises[1];
                porosity=0.0;
                fprintf(stderr,"point i=%i remise à zero delta poro grand et poro theo neg, param_mises[0]=%g,param_mises[1]=%g,dlb=%g,porosity=%g\n",i,param_mises[0],param_mises[1],dlb,porosity );
                actu_porosity=0;if_poro_almost0=1;
                if (dlb<0.0) dlb=-dlb;
                printf("point i=%i ,dlb=%g\n",i,dlb );
                sig_m=trst;
                porosity=0.0;

              }
              else{
                 fprintf(stderr,"param[0]=%g\n",param_spc0e_syst_2eq[0]);
                 fprintf(stderr,"param[1]=%g\n",param_spc0e_syst_2eq[1]);
                 fprintf(stderr,"param[2]=%g\n",param_spc0e_syst_2eq[2]);
                 fprintf(stderr,"param[3]=%g\n",param_spc0e_syst_2eq[3]);
                 fprintf(stderr,"param[4]=%g\n",param_spc0e_syst_2eq[4]);
                 fprintf(stderr,"param[5]=%g\n",param_spc0e_syst_2eq[5]);
                 fprintf(stderr,"param[6]=%g\n",param_spc0e_syst_2eq[6]);
                 fprintf(stderr,"param[7]=%g\n",param_spc0e_syst_2eq[7]);
                 fprintf(stderr,"param[8]=%g\n",param_spc0e_syst_2eq[8]);
                 fprintf(stderr,"param[9]=%g\n",param_spc0e_syst_2eq[9]);
                 fprintf(stderr,"param[10]=%g\n",param_spc0e_syst_2eq[10]);
                 fprintf(stderr,"param[11]=%g\n",param_spc0e_syst_2eq[11]);
                 fprintf(stderr,"param[12]=%g\n",param_spc0e_syst_2eq[12]);
                 fprintf(stderr,"status =%i\n",newton_status);
       //    return -2;
                 status=-13;
                 point_pb=i;
              }

            }
            if ((porosity>0.99)&& (!if_poro_almost1)){
	      if_poro_almost1=1;
	      porosity=0.995;
	      fprintf(stdout,"point i=%i is a void\n",i);
	      dlb=(param_spc0e_syst_2eq[3]*sig_m+param_spc0e_syst_2eq[4])/(deuxmumu0*p->q3*27.0*p->k*porosity*sinh(param_spc0e_syst_2eq[0]*(sig_m+pressure)));
              actu_porosity=0;
              if (pvporo[i]<0.01) {
                     double param_mises[2];
                     param_mises[0]=1.5*(sig02)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
                     param_mises[1]=muplusmu0*sig02/3.0/deuxmumu0/p->q3;
                     dlb=sqrt(param_mises[0])-param_mises[1];
                     porosity=0.0;
                     printf("point i=%iremise à zero after porosity>0.99, param_mises[0]=%g,param_mises[1]=%g,dlb=%g,porosity=%g\n",i,param_mises[0],param_mises[1],dlb,porosity );
                     actu_porosity=0;if_poro_almost0=1;
                     if (dlb<0.0) dlb=-dlb;
                     printf("point i=%i ,dlb=%g\n",i,dlb );
                     sig_m=trst;
                     porosity=0.0;
              }
	    } 

          }
        }
      }
    }

    else {// (poro_almost1)  
      dlb=0.0;
    }  
/*######################################################################*/
//First Controls
/*######################################################################*/
    if (porosity<0.0) {
          if (porosity<-err){ fprintf(stderr, "porosity neg, porosity=%g au point i=%i \n",porosity,i);}
          porosity=0.0;
          sig_m=trst;
          double param_mises[2];
          param_mises[0]=1.5*(sig02)*(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)/36.0/p->mu/p->mu/p->q3;
          param_mises[1]=muplusmu0*sig02/3.0/deuxmumu0/p->q3;
          dlb=sqrt(param_mises[0])-param_mises[1];
          if (dlb<0.0){
            fprintf(stderr, "dlb neg mises , dlb=%g \n",dlb);
            dlb=-dlb;
          }
          to_test=0;
        }
 
        if (isnan(sig_m)){
           fprintf(stderr,"sig_m not a number at point i=%i!\n",i);
           status=-9;
           point_pb=i;
        }
        if ((f1_spc0e(sig_m,porosity,param_spc0e_syst_2eq)>1.e-2)&&(!if_poro_almost1)&&(to_test)){
           fprintf(stderr, "Pb in Newton : zeros bad found \n");
           fprintf(stderr, "sig_m=%g \n",sig_m);
           fprintf(stderr, "status==%i \n",status);
           fprintf(stderr, "err=%g \n",err);
           fprintf(stderr, "trst=%g \n",trst);
           fprintf(stderr, "verif_elast=%g \n",f1_spc0e(trst,porosity,param_spc0e_syst_2eq));
           fprintf(stderr, "verif_sigm=%g \n",f1_spc0e(sig_m,porosity,param_spc0e_syst_2eq));
           fprintf( stderr,"porosity=%g \n",porosity);
           fprintf(stderr,"param[0]=%g\n",param_spc0e_syst_2eq[0]);
           fprintf(stderr,"param[1]=%g\n",param_spc0e_syst_2eq[1]);
           fprintf(stderr,"param[2]=%g\n",param_spc0e_syst_2eq[2]);
           fprintf(stderr,"param[3]=%g\n",param_spc0e_syst_2eq[3]);
           fprintf(stderr,"param[4]=%g\n",param_spc0e_syst_2eq[4]);
           fprintf(stderr,"param[5]=%g\n",param_spc0e_syst_2eq[5]);
           fprintf(stderr,"param[6]=%g\n",param_spc0e_syst_2eq[6]);
           fprintf(stderr,"param[7]=%g\n",param_spc0e_syst_2eq[7]);
           fprintf(stderr,"param[8]=%g\n",param_spc0e_syst_2eq[8]);
           fprintf(stderr,"param[9]=%g\n",param_spc0e_syst_2eq[9]);
           fprintf(stderr,"param[10]=%g\n",param_spc0e_syst_2eq[10]);
           fprintf(stderr,"param[11]=%g\n",param_spc0e_syst_2eq[11]);
           fprintf(stderr,"param[12]=%g\n",param_spc0e_syst_2eq[12]);
           status=-8;
           point_pb=i; 
        }

        if (dlb<-1.0e-8){  
           fprintf(stderr, "dlb negative!!!,dlb=%g, pt=%i\n",dlb,i);
           fprintf(stderr,"param[0]=%g\n",param_spc0e_syst_2eq[0]);
           fprintf(stderr,"param[1]=%g\n",param_spc0e_syst_2eq[1]);
           fprintf(stderr,"param[2]=%g\n",param_spc0e_syst_2eq[2]);
           fprintf(stderr,"param[3]=%g\n",param_spc0e_syst_2eq[3]);
           fprintf(stderr,"param[4]=%g\n",param_spc0e_syst_2eq[4]);
           fprintf(stderr,"param[5]=%g\n",param_spc0e_syst_2eq[5]);
           fprintf(stderr,"param[6]=%g\n",param_spc0e_syst_2eq[6]);
           fprintf(stderr,"param[7]=%g\n",param_spc0e_syst_2eq[7]);
           fprintf(stderr,"param[8]=%g\n",param_spc0e_syst_2eq[8]);
           fprintf(stderr,"param[9]=%g\n",param_spc0e_syst_2eq[9]);
           fprintf(stderr,"param[10]=%g\n",param_spc0e_syst_2eq[10]);
           fprintf(stderr,"param[11]=%g\n",param_spc0e_syst_2eq[11]);
           fprintf(stderr,"param[12]=%g\n",param_spc0e_syst_2eq[12]);
           fprintf( stderr,"sig_m=%g\n",sig_m);
           fprintf( stderr,"porosity=%g\n",porosity);
           fprintf(stderr, "trst=%g\n",trst);
           fprintf(stderr, "k=%g\n",p->k);
           fprintf(stderr, "2mumu0=%g\n",deuxmumu0);
           double test_trial=f_gurson(trst,steq2,paramf);
           fprintf(stderr, "test_trial1=%g\n",test_trial);
           fprintf( stderr,"verif_elast1=%g \n",f1_spc0e(trst,porosity,param_spc0e_syst_2eq));
           fprintf( stderr,"verif1=%g \n",f1_spc0e(sig_m,porosity,param_spc0e_syst_2eq));
           if ((dlb<-1.0e-8) &&(!if_poro_almost1))  { 
                dlb=0.0;
                status=-5;
                point_pb=i;
           }
           dlb=0.0;
           actu_porosity=1;
    }

/*######################################################################*/
// Mise à jour des variables
/*######################################################################*/

    //  if (dlb>0) printf( "dlb pour actu, dlb=%g \n",dlb);
    double sig_D_coef=1.0+p->mu/mu0+6.0*p->mu*p->q3*dlb/sig02;
    sig_D_coef=1.0/sig_D_coef;
    double dotepsp[6];


    for (j=0;j<3;j++){
        s[i][j] = ((p->mu/mu0)*deltataud[j]+(sp[i][j]-trsp)*(1.0+p->mu/mu0))*sig_D_coef;
    }
    for (j=3;j<6;j++){
        s[i][j] = ((p->mu/mu0)*deltataud[j]+(sp[i][j])*(1.0+p->mu/mu0))*sig_D_coef;
    }
// s is the stress deviator
    for (j=0;j<6;j++){
            dotepsp[j] =s[i][j]*3.0*p->q3*dlb/sig02;
           // ps[i][j]=ps[i][j]+dotepsp[j];
    }
    double normdoteps=0.0;
    for (j=0;j<6;j++){
         normdoteps=normdoteps+sqrt(2.0/3.0*dotepsp[j]*dotepsp[j]);
    }
    cump[i]=pvcump[i]+normdoteps;
    double dotepspm=dlb*3.0*porosity*sinh(param_spc0e_syst_2eq[0]*(sig_m+pressure))/p->sig0;
    for (j=0;j<3;j++){
          dotepsp[j] =dotepsp[j]+dotepspm;
          //ps[i][j]=ps[i][j]+dotepspm;
    }
    double verif_seq2;
    verif_seq2 = 1.5*((s[i][1]*s[i][1])+(s[i][2]*s[i][2])+
                       (s[i][0]*s[i][0]))
                   +3.0*(s[i][4]*s[i][4]+s[i][5]*s[i][5]+
                    s[i][3]*s[i][3]); 
    double test_normdot=fabs(normdoteps);
    if (test_normdot>5.0) {
         fprintf(stderr,"Pb!,normdoteps=%g\n, point i=%i",normdoteps,i);
         fprintf(stderr,"Pb!,verif_seq2=%g\n",verif_seq2);
    }
   
    for (j=0;j<3;j++){
         e[i][j]=ep[i][j]+((s[i][j]-sp[i][j]+trsp)/(2.0*p->mu))+(sig_m-trsp)/(3.0*p->k)+dotepsp[j];
    }   
    for (j=3;j<6;j++){
         e[i][j]=ep[i][j]+((s[i][j]-sp[i][j])/(2.0*p->mu))+dotepsp[j];
    }   
    for (j=0;j<3;j++){
          s[i][j] = s[i][j]+sig_m;
    }
    if (porosity>0.99){
         if_poro_almost1=1;
         porosity=0.99;
         fprintf(stdout,"point i=%i is a void\n",i);
         actu_porosity=0;
    } 
    if (actu_porosity) {
          vporo[i]=porosity/(p->q1);
    }
    else {
          if (if_poro_almost1) vporo[i]=0.99;
          else if (if_poro_almost0) vporo[i]=0.0;
          else vporo[i]=pvporo[i];
    }
    if (vporo[i]<0.0){ fprintf(stderr, "vporo neg fin pas de temps, porosity=%g au point i=%i \n",vporo[i],i); vporo[i]=0.0; }
//now s is the stress


    if (if_poro_almost1){ 
      for (j=0;j<3;j++){
          s[i][j] = pressure;
      }
      for (j=3;j<6;j++){
          s[i][j] = 0.0;
      }   
    }


/*######################################################################*/
//Last Controls
/*######################################################################*/

    paramf[4]=-1.0-porosity*porosity;
    paramf[2]=2.0*porosity;
    double verif_res2=(f_gurson(sig_m,verif_seq2,paramf));
    if ((verif_res2>1.0e-6)&&(!if_poro_almost1)){
           fprintf(stderr, "False Equations pt=%i\n",i);
           fprintf(stderr,  "dlb=%g \n",dlb);
           fprintf(stderr, "test_trial2= %g\n",test_trial2);
           fprintf(stderr, "steq2= %g\n",steq2);
           fprintf(stderr,  "verif_seq2=%g \n",verif_seq2);
           //printf( "speq2=%g \n",speq2);
           fprintf(stderr, "taueq2=%g \n",taueq2);
           fprintf(stderr, "verif_res2=%g \n",verif_res2);
           fprintf(stderr,"param[0]=%g\n",param_spc0e_syst_2eq[0]);
           fprintf(stderr,"param[1]=%g\n",param_spc0e_syst_2eq[1]);
           fprintf(stderr,"param[2]=%g\n",param_spc0e_syst_2eq[2]);
           fprintf(stderr,"param[3]=%g\n",param_spc0e_syst_2eq[3]);
           fprintf(stderr,"param[4]=%g\n",param_spc0e_syst_2eq[4]);
           fprintf(stderr,"param[5]=%g\n",param_spc0e_syst_2eq[5]);
           fprintf(stderr,"param[6]=%g\n",param_spc0e_syst_2eq[6]);
           fprintf(stderr,"param[7]=%g\n",param_spc0e_syst_2eq[7]);
           fprintf(stderr,"param[8]=%g\n",param_spc0e_syst_2eq[8]);
           fprintf(stderr,"param[9]=%g\n",param_spc0e_syst_2eq[9]);
           fprintf(stderr,"param[10]=%g\n",param_spc0e_syst_2eq[10]);
           fprintf(stderr,"param[11]=%g\n",param_spc0e_syst_2eq[11]);
           fprintf(stderr,"param[12]=%g\n",param_spc0e_syst_2eq[12]);
           fprintf( stderr,"sig_m=%g\n",sig_m);
           fprintf( stderr,"porosity=%g\n",porosity);
           fprintf(stderr, "trst=%g\n",trst);
           double test_trial=f_gurson(trst,steq2,paramf);
           fprintf(stderr, "test_trial1=%g\n",test_trial);
           fprintf( stderr,"verif_elast1=%g \n",f1_spc0e(trst,porosity,param_spc0e_syst_2eq));
           fprintf( stderr,"verif1=%g \n",f1_spc0e(sig_m,porosity,param_spc0e_syst_2eq));
//           printf("status=%i\n",statusN[i]);
           for (j=0;j<6;j++){
              fprintf(stderr, "dotepsp[%i]=%g\n",j,dotepsp[j]);
            }  
            status=-12;
	    point_pb=i;
    }


  }//end for i
  if (status!=0){fprintf(stderr,"iteration NOK, status=%i,point_pb=%i\n",status,point_pb);}
  if (point_pb!=0){
       fprintf(stderr,"iteration NOK, status=%i,point_pb=%i\n",status,point_pb);
       status=-2;
  }
  return status;
}

