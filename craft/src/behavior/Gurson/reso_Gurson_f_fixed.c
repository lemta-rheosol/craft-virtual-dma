#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>
#include <euler.h>
#include <utils.h>
#include <GURSON_materials.h>
#include <gurson_functions.h>
#include <variables.h>
#include <utils_materials.h>
#include <LE_Cmatrix.h>
#include <Crystal_Tools.h>


#ifdef _OPENMP
#include <omp.h>
#endif
/*######################################################################*/
/* G. Boittin
   LMA/CNRS
   October 28th 2014
   
   functions to manage gurson type plasticity behavior
   only isotropic case for elasticity is yet implemented
*/
/*######################################################################*/
/*#Headers in GURSON_materials.h#*/

/*######################################################################*/

/*int reso_gurson_basic_poro_fix( 
		   void *param, Variables *sv,double dt);
int reso_gurson_spc0e_poro_fix(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]);*/
/*######################################################################*/

/*######################################################################*/

int reso_gurson_basic_poro_fix( 
		   void *param, Variables *sv,double dt){

/*######################################################################*/
//variables declaration

//calculation of parameters independant of the point

//loop on the point i

//calculation of parameters dependant of the point

//call muller

// variables updating
/*######################################################################*/
int i;//variables declaration

  GURSON_param *p;
  int N;
  int status=0;
  int point_pb=0;
  N = sv->np;
  p  = ((GURSON_param *)param);
//copy of variables
  double (*e)[6];
  double (*s)[6];
  double (*ep)[6];
  double (*sp)[6];
  //double (*ps)[6];
  double (*vpb);
  double (*pvcump);
  double (*cump);
  e = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  s = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  ep = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sp   = (double (*)[6])(ptr_variable
          ("previous stress", sv, param)->values);
  //ps = (double (*)[6])(ptr_variable("plastic_strain", sv, param)->values);
  cump = (double (*))(ptr_variable
         ("cumulated_plastic_strain", sv, param)->values);
  pvcump = (double (*))(ptr_variable
         ("previous_cum_plastic_strain", sv, param)->values);

  double  sig0=(p->sig0);
  double  sig02=sig0*sig0;
  double  t [6];
  double  k, trs;
  double  err, devstdevsp, steq2, trsp ;
  double  sig_m, dlb,pressure,porosity;
  int     j;
  double  paramg [8];// parameters for function f_gurson=f(sig_m)
  double  paramf [5];// parameters for function f_gurson=f(seq2,sigm)


/*######################################################################*/
//calculation of parameters dependant of the point


  paramg[0]=paramf[3] =0.0;
  k = (3.0*(p->lb)+2.0*(p->mu))/3.0;//bulk modulus
  
  
  switch (p->pressure_int){
    case NO_PRESSURE:
      pressure=paramg[0]=paramf[3]=0.0;
      break;
    case FIXED_VALUE_OF_PRESSURE:
       paramg[0]= p->bpp.fbpp->pressure_fixed;
      pressure=paramf[3]=p->bpp.fbpp->pressure_fixed;
      break;
    case LINEAR_EVO_OF_PRESSURE:
      pressure=paramf[3]= paramg[0]=p->bpp.lbpp->pressure_actu;
      break;
    case TABULATED_EVO_OF_PRESSURE:
      pressure=paramf[3]=  p->bpp.tbpp->pressure_actu;
      pressure=paramg[0]=paramf[3];
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      status=-10;
      printf("calculated evo of pressure is Not yet implemented\n");    
      vpb   = (double (*))
         (ptr_variable("bubble_pressure", sv, param)->values);
      break;
  }
  switch (p->poro_int){
    case FIXED_VALUE_OF_POROSITY:
      porosity=p->q1*p->pp.fpp->poro_fixed;
      paramf[2]=2.0*p->q1*(p->pp.fpp->poro_fixed);
      paramf[4]=-1.0-p->q1*(p->pp.fpp->poro_fixed)*p->q1*(p->pp.fpp->poro_fixed);
      break;
    case LINEAR_EVO_OF_POROSITY:
      porosity=p->q1*p->pp.lpp->poro_actu;
      paramf[2]=2.0*(p->q1*p->pp.lpp->poro_actu);
      paramf[4]=-1.0-p->q1*(p->pp.lpp->poro_actu)*p->q1*(p->pp.lpp->poro_actu);
      break;
    case TABULATED_EVO_OF_POROSITY:
      porosity=p->q1*p->pp.tpp->poro_actu;
      paramf[2]=2.0*p->q1*(p->pp.tpp->poro_actu);
      paramf[4]=-1.0-p->q1*(p->pp.tpp->poro_actu)*p->q1*(p->pp.tpp->poro_actu);
      break;
    case CALCULATED_EVO_OF_POROSITY:
      status=-10;
      printf("It is absolutely unnormal to see this message!!!!\n There is a problem with the mode of calculs of the porosity.\n In this method the porosity is given by the user not calculated!\n");      
      break;
  }
  
  
  if (status!=0) return status;

//  paramg[0] = 6.0*p->mu*p->q3;
//  paramg[1] =  (1.5/sig0);
// paramg[3] = 9.0*k*sig0; 
  paramf[1] = (1.5/sig0);
  paramf[0] = p->q3/(sig02);
  
 // paramg[0]= pressure;
  paramg[1] = paramf[1];
  //paramg[2] dependant
  paramg[3]=2.0*porosity;
  paramg[4]=-1.0-porosity*porosity;
  paramg[5]=3.0*k*porosity/sig0;
  //paramg[6] dependant
  paramg[7]=-2.0*p->mu*p->q3/sig02; 
/*######################################################################*/ 
  //loop on the point i
#pragma omp parallel for			\
  default(none)					\
  private(i,j)					\
  firstprivate(paramf,paramg)					\
  firstprivate(pressure)			\
  private(t,trsp,trs,devstdevsp,steq2,sig_m,dlb,status)					\
  shared(dt,N,k,sig0,err,sig02,point_pb,porosity)					\
  shared(p,e, ep, s, sp,vpb,cump,pvcump)
  for (i=0;i<(N);i++){
  
  
/*######################################################################*/ 
//calculation of parameters dependant of the point

       //double seq;
       for (j=0;j<6;j++){
          t[j] = sp[i][j] + 2.0 * (p->mu) * (e[i][j] - ep[i][j]);
       }
 
       for (j=0;j<3;j++){
          t[j] += (p->lb)*(e[i][1]+e[i][2]+e[i][0]);
          t[j] -= (p->lb)*(ep[i][1]+ep[i][2]+ep[i][0]);
       }//end for

//     Now t[j] is sigma_trial
//     Hydrostatic part of the trial elastic stress and previous stress
       trsp = (sp[i][0]+sp[i][1]+sp[i][2])/3.0;
       trs=(t[0]+t[1]+t[2])/3.0;

//      Deviator part of the trial elastic stress 

       for (j=0;j<3;j++){
          t[j] = t[j]-trs;
       }//end for

//      Now t[j] is not any more sigma_trial but the deviatoric part of sigma_trial


//      Contracted product of the deviatoric part of the trial elastic stress and the previous deviatoric part of the stress
       devstdevsp=0.0;
       for (j=0;j<3;j++){
          devstdevsp = devstdevsp +((sp[i][j]-trsp) * t[j]);
       }
       for (j=3;j<6;j++) {
          devstdevsp = devstdevsp+(sp[i][j] * t[j]);
// ou          devstdevsp = devstdevsp+2*(sp[j][i] * t[j][i]), si je n'ai pas la bonne notation, à vérifier, notation de Kelvin  OK
       }
//       

//       Von Mises stress of the trial elastic stress to the power two
       steq2 = 1.5*((t[1]*t[1])+(t[2]*t[2])+(t[0]*t[0]))
                   +3.0*(t[4]*t[4]+t[5]*t[5]+t[3]*t[3]);  

 //      Calculating last parameters of function f_gurson2
       
//        Rappel of parameters
  //     paramg[4]=p->q3*(steq2)/(sig0*sig0);
 //      paramg[5]=
 //      trst=trsp+k*(e[i][0]+e[i][1]+e[i][2])
 //                 -k*(ep[i][0]+ep[i][1]+ep[i][2]);
       paramg[2]=p->q3*(steq2)*(9.0*k*porosity*k*porosity)/(sig02*sig02);
       paramg[6]=-paramg[7]*trs;
		  

/*######################################################################*/
//elastic case ?
/*######################################################################*/
       //double test_trial=f_gurson(trs,steq2,paramf);
       //double seq2,seq2_2,dlb1;		  
       //double dlb1;		  
       if (f_gurson(trs,steq2,paramf)>TINY){
         //double abs_t_and_pb=fabs(trs+paramg[0]);
         mon_pointeur_fonction *ma_fonction2= h2f_gurson;
/*######################################################################*/
	 //call muller
	 sig_m=(trs-paramg[0])/2.0;
	 int stat_muller=muller(&sig_m,*ma_fonction2, paramg,fabs(sig_m), TINY);
	 if (stat_muller !=  0){
	   printf("stat_muller=%i,f(trs,seq)=%g,trs=%g,steq=%g,paramg[2]=%g\n",stat_muller,f_gurson(trs,steq2,paramf),trs,sqrt(steq2),paramg[2]);
	 }
         //seq2=(1.0+porosity*(porosity-2*cosh(paramf[1]*(sig_m+paramf[3]))))/paramf[0];
         dlb=sig0*(trs-sig_m)/
                (9.0*k*porosity*sinh(paramg[1]*(sig_m+pressure)));// dlb is dot(lambda)*dt
	if (fabs(sig_m+pressure)<TINY){dlb=(sqrt(steq2)/(sig0*(1.0-porosity)/sqrt(p->q3))-1.0)*sig0/(6.0*p->mu*p->q3);}
       }
       else {// elastic case
         sig_m=trs;
         dlb=0.0;
	 //seq2=steq2;
       }//end elastic case
/*######################################################################*/
// variables updating
       if (dlb<-TINY) {
         status=-5;
         point_pb=i;
         printf("Negative schema basic plasticity,point i=%i !!!!!!!,dlb=%g,trs=%g,trsp=%g,sig_m=%g,steq2=%g,mu=%g f(elast)=%g\n",i,dlb,trs,trsp,sig_m,steq2,p->mu,f_gurson(trs,steq2,paramf));
	     printf("param[0]=%g\n",paramg[0]);
             printf("param[1]=%g\n",paramg[1]);
             printf("param[2]=%g\n",paramg[2]);
             printf("param[3]=%g\n",paramg[3]);
             printf("param[4]=%g\n",paramg[4]);
             printf("param[5]=%g\n",paramg[5]);
             printf("param[6]=%g\n",paramg[6]);
             printf("param[7]=%g\n",paramg[7]);
             for (j=0;j<6;j++){
                   printf("t[%i]=%g\n",j,t[j]);
             }
       }
         
       double sig_D_coef=1.0+(6.0*p->q3*(p->mu)*dlb/(sig02));
       sig_D_coef=1.0/sig_D_coef;
      
       for (j=0;j<6;j++){
           s[i][j] = sig_D_coef*t[j];
       } // s is the stress deviator
       /*double verif_seq2;
       verif_seq2 = 1.5*((s[i][1]*s[i][1])+(s[i][2]*s[i][2])+
                     (s[i][0]*s[i][0]))
                   +3.0*(s[i][4]*s[i][4]+s[i][5]*s[i][5]+
                    s[i][3]*s[i][3]); */
       double dotepsp[6];
       for (j=0;j<3;j++){
          dotepsp[j] = 
           3.0*dlb/sig0*porosity*sinh(paramg[1]*(sig_m+paramg[0]));
          //ps[i][j]=ps[i][j]+dotepsp[j];
       }   
       for (j=0;j<6;j++){
          dotepsp[j] =dlb*3.0/(sig0*sig0)*s[i][j];
        //  ps[i][j]=ps[i][j]+dotepsp[j];
       }
       for (j=0;j<3;j++){
          s[i][j] = s[i][j]+sig_m;
       }
       //now s is the stress
       double normdoteps=0.0;
       for (j=0;j<6;j++){
         normdoteps=normdoteps+sqrt(2.0/3.0*dotepsp[j]*dotepsp[j]);
       }
       cump[i]=pvcump[i]+normdoteps;
  }// end for i
  if (point_pb != 0) {status=-2; printf("iteration NOK, status=%i,point_pb=%i\n",status,point_pb);}
 // else {status=0; printf("iteration OK, status=%i,point_pb=%i\n",status,point_pb);}
  return status;
}
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/
/*######################################################################*/
   
int reso_gurson_spc0e_poro_fix(
		   void *param, Variables *sv,double dt,
                    LE_param *L0,
                    double (*tau)[6]){     
/*######################################################################*/
//variables declaration

//calculation of parameters independant of the point

//loop on the point i

//calculation of parameters dependant of the point

//call muller

// variables updating
/*######################################################################*/
  //variables declaration
  int i;
  int status=0;
  int point_pb=0;
  GURSON_param *p;
  int N;

  N = sv->np;
  p  = ((GURSON_param *)param);
  double (*e)[6];
  double (*s)[6];
  double (*ep)[6];
  double (*sp)[6];
  //double (*ps)[6];
  double (*vpb);
  double (*pvcump);
  double (*cump);
  e = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  s = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  ep = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sp   = (double (*)[6])(ptr_variable
          ("previous stress", sv, param)->values);
  //ps = (double (*)[6])(ptr_variable("plastic_strain", sv, param)->values);
  cump = (double (*))(ptr_variable
         ("cumulated_plastic_strain", sv, param)->values);
  pvcump = (double (*))(ptr_variable
         ("previous_cum_plastic_strain", sv, param)->values);
  int j;
  double paramf[5]  ;
  double param_spc0e[8]  ;
  double taud[6], deltataud[6];
  double pressure,porosity,sig_m;
  double   mu0=L0->sub.ile->mu;
  double  muplusmu0=(p->mu+L0->sub.ile->mu);
  double   musurmu0 =(p->mu/mu0);
  double  deuxmumu0=2.*p->mu*mu0;
  double ksurk0=p->k/L0->sub.ile->k;
  double  unplusksurk0=1.0+(p->k/L0->sub.ile->k);
  double  sig02=p->sig0*p->sig0;
  double dlb;
  double k2=musurmu0*musurmu0; 
  double k3=1.0+musurmu0; 
  double k4=2.*musurmu0*k3;
  k3=k3*k3;
/*  double k5=3.0*muplusmu0*sig02*p->k;//k5 should be mulitply by porosity to obtain param_spc0e[2]
  double k1=13.5*p->q3*sig02*mu0*mu0*p->k*p->k;
  double k2=musurmu0*musurmu0;
  double k3=1.0+musurmu0;
  double k6=1.0/k3;;
  double k4=2.*musurmu0*k3;
  double err=1.0e-9;
  k3=k3*k3;*/

  switch (p->pressure_int){
    case NO_PRESSURE:
      pressure=0.0;
      param_spc0e[0]=pressure;
      paramf[3]=pressure;
      break;
    case FIXED_VALUE_OF_PRESSURE:
      pressure= p->bpp.fbpp->pressure_fixed;
      param_spc0e[0]=pressure;
      paramf[3]=pressure;
      break;
    case LINEAR_EVO_OF_PRESSURE:
      printf("LINEAR_EVO_OF_PRESSURENot yet tested");
      pressure=p->bpp.lbpp->pressure_actu;
      param_spc0e[0]=pressure;
      paramf[3]=pressure;
      break;
    case TABULATED_EVO_OF_PRESSURE:
      pressure=  p->bpp.tbpp->pressure_actu;
      param_spc0e[0]=pressure;
      paramf[3]=pressure;
      break;
    case CALCULATED_EVO_OF_PRESSURE:
      vpb   = (double (*))
         (ptr_variable("bubble_pressure", sv, param)->values);
//      function_evo_pb=1;
      break;
  }

  int function_evo_poro=0;
  switch (p->poro_int){
    case FIXED_VALUE_OF_POROSITY:
      porosity=p->q1*(p->pp.fpp->poro_fixed);
      if ((porosity>1.0)||(porosity<0.0)) {printf("ds lecture, porosity>1.0 or poro neg\n, q1=%g,porosity=%g",p->q1,porosity);status=-5;return status;} 
      paramf[2]=2.0*porosity;
      paramf[4]=-1.0-(porosity*porosity); 
      break;
    case LINEAR_EVO_OF_POROSITY:
      porosity=p->q1*(p->pp.lpp->poro_actu);
      paramf[2]=2.0*porosity;
      paramf[4]=-1.0-(porosity*porosity);  
      break;
    case TABULATED_EVO_OF_POROSITY:
      porosity=p->q1*(p->pp.tpp->poro_actu);
      paramf[2]=2.0*porosity;
      paramf[4]=-1.0-(porosity*porosity); 
      break;
  }
  if ((porosity>1.0)||(porosity<0.0)) {
         printf("porosity>1.0 or poro neg\n, q1=%g,porosity=%g",p->q1,porosity);
         status=-5;
         return status;
  }
  
  
  paramf[0]=p->q3/sig02;
  paramf[1]=1.5/p->sig0;
  
  param_spc0e[1]=paramf[1];
  //    param_spc0e[2]= dependant
  param_spc0e[3]= 2.0*porosity;
  param_spc0e[4]= -1.0-porosity*porosity;
  param_spc0e[5]= 3.0*muplusmu0*p->k*porosity/sig02;
  //    param_spc0e[6]= dependant
  param_spc0e[7]=-deuxmumu0*p->q3*unplusksurk0/(p->sig0*sig02);

  
/*######################################################################*/
  
//loop on the point i
#pragma omp parallel for			\
  default(none)					\
  private(i,j)					\
  firstprivate(paramf,param_spc0e)					\
  shared(pressure,porosity)			\
  private(taud,deltataud)           \
  private(sig_m,dlb)					\
  shared(dt,N,mu0,musurmu0,deuxmumu0,ksurk0,unplusksurk0,muplusmu0,sig02)					\
  shared(p,L0,e, ep, s, sp,vpb,cump,pvcump,tau,point_pb,k2,k3,k4,status)
  for(i=0; i<N; i++) {
  //  statusN[i]=0;
    double tau_ini[6];
  /*######################################################################*/
//calculation of parameters dependant of the point
    for(j=0;j<6;j++){tau_ini[j]=tau[i][j];}  
    double     trtau = tau[i][0]+tau[i][1]+tau[i][2];

    double    trep = (ep[i][0]+ep[i][1]+ep[i][2])/3.0;
    double    trsp = (sp[i][0]+sp[i][1]+sp[i][2])/3.0;
    //double    taueq2=0.0;
    for (j=0;j<3;j++){
      taud[j]=tau_ini[j]-trtau/3.0;
      //taueq2=taueq2+1.5*taud[j]*taud[j];
    }
    for (j=3;j<6;j++){
      taud[j]=tau_ini[j];
      //taueq2=taueq2+3.0*taud[j]*taud[j];
    }
    /* elastic trial */
    //double speq2=0.0;
    for (j=0;j<3;j++){
      deltataud[j]=taud[j]-sp[i][j]+trsp-2.0*L0->sub.ile->mu*(ep[i][j]-trep);
    //  speq2=speq2+1.5*(sp[i][j]-trsp)*(sp[i][j]-trsp);
    }
    for (j=3;j<6;j++){
      deltataud[j]=taud[j]-sp[i][j]-2.*L0->sub.ile->mu *ep[i][j];
    //  speq2=speq2+3.0*(sp[i][j]-trsp)*(sp[i][j]-trsp);
    }
    double deltataum= trtau/3.0;
    deltataum=deltataum-trsp-3.0*L0->sub.ile->k*trep;
    double      deltataudsigp= 0.;
    double      deltatauddeltataud= 0.;
    double      sigpsigp= 0.;
    for (j=0;j<3;j++){
      deltataudsigp=deltataudsigp+deltataud[j]*(sp[i][j]-trsp);
      sigpsigp=sigpsigp+(sp[i][j]-trsp)*(sp[i][j]-trsp);
      deltatauddeltataud=deltatauddeltataud+deltataud[j]*deltataud[j];
    }
    for (j=3;j<6;j++){
      deltataudsigp=deltataudsigp+2.0*deltataud[j]*sp[i][j];
      sigpsigp=sigpsigp+2.0*sp[i][j]*sp[i][j];
      deltatauddeltataud=deltatauddeltataud+2.0*deltataud[j]*deltataud[j];
    }
    
    double  trst=trsp + deltataum* (p->k)/(L0->sub.ile->k+p->k);
    param_spc0e[2]=(k2*deltatauddeltataud+k3*sigpsigp+k4*deltataudsigp)*13.5*porosity*porosity*mu0*mu0*p->k*p->k*p->q3/(sig02*sig02*sig02);
    param_spc0e[6]= -param_spc0e[7]*trst;

    double  steq2=0;
    double std[6];
    for (j=0;j<3;j++){
       std[j]=(musurmu0*deltataud[j]+(1.+musurmu0)*(sp[i][j]-trsp))/(1.0+musurmu0);
       steq2=steq2+1.5*std[j]*std[j];
    }
    for (j=3;j<6;j++){
       std[j]=(musurmu0*deltataud[j]+(1.+musurmu0)*(sp[i][j]))/(1.0+musurmu0);
       steq2=steq2+3.0*std[j]*std[j];
    }
 
/*######################################################################*/
//elastic case ?
/*######################################################################*/
    //double abs_t_and_pb=fabs(trst+param_spc0e[0]);
    //double seq2;
    if (f_gurson(trst,steq2,paramf)<TINY){//elastic case
       dlb=0.0;
       sig_m=trst;
       //seq2=steq2;
    }
    else{
      mon_pointeur_fonction *ma_fonction2= h2f_gurson;
      sig_m=(trst-param_spc0e[0])/2.0;
      int muller_stat=muller(&sig_m,*ma_fonction2, param_spc0e,fabs(sig_m), TINY);

      if (muller_stat!=0){
              printf( "muller f_gurson did not converged, point %i, h=%g,trst=%g,trsp=%g  status=%i,sig_m_atester=%g,deltataum=%g\n",i,fabs((trst-param_spc0e[0])/2.0),trst,trsp,muller_stat,(trst-param_spc0e[0])/2.0,deltataum); 
	      printf( "using h2f_gurson_spc0e\n");
              point_pb=i;
              status=-4;
	      exit(-4);
      }
      //seq2=(1.0+porosity*(porosity-2.0*cosh(paramf[1]*(sig_m+paramf[3]))))/paramf[0];
      dlb=p->sig0*(trst-sig_m)*unplusksurk0/(9.0*p->k*porosity*sinh(param_spc0e[1]*(sig_m+pressure)));
      if (fabs(sig_m+pressure)<TINY){
	printf( "sig_m=-pressure, dlb=%g\n",dlb);	
	dlb=(sqrt(steq2)/(p->sig0*(1.0-porosity)/sqrt(p->q3))-1.0)*p->sig0/(6.0*p->mu*p->q3);
	printf( "sig_m=-pressure, dlb=%g\n",dlb);	
      }

    }
         
    double sig_D_coef=1.0+p->mu/mu0+(6.0*p->q3*(p->mu)*dlb/(sig02));
    sig_D_coef=1.0/sig_D_coef;
    
    double dotepsp[6];
       
    for (j=0;j<3;j++){
        s[i][j] = ((p->mu/mu0)*deltataud[j]+(sp[i][j]-trsp)*(1.0+p->mu/mu0))*sig_D_coef;
    }
    for (j=3;j<6;j++){
        s[i][j] = ((p->mu/mu0)*deltataud[j]+(sp[i][j])*(1.0+p->mu/mu0))*sig_D_coef;
    }

// s is the stress deviator

/*    double verif_seq2;
    verif_seq2 = 1.5*((s[i][1]*s[i][1])+(s[i][2]*s[i][2])+
                       (s[i][0]*s[i][0]))
                   +3.0*(s[i][4]*s[i][4]+s[i][5]*s[i][5]+
                    s[i][3]*s[i][3]); 
    if (fabs(verif_seq2 -seq2)>1.0e-6*p->sig0*p->sig0){
      printf("verif_seq2=%g,seq2=%g,sig_m=%g,trst=%g,dlb=%g,pressure=%g,param_spc0e[1]=%g\n",verif_seq2,seq2,sig_m,trst,dlb,pressure,param_spc0e[1]);
      printf("k=%g,mu=%g,mu0=%g,unplusksurk0=%g,param_spc0e[6]=%g,param_spc0e[7]=%g,f(sig_m,verif)=%g,test_trial2=%g\n",p->k,p->mu,mu0,unplusksurk0,param_spc0e[6],param_spc0e[7],f_gurson(sig_m,verif_seq2,paramf),test_trial2);
      status=-3;
      printf("porosity=%g\n",porosity); 
      printf("param[0]=%g\n",param_spc0e[0]); 
      printf("param[1]=%g\n",param_spc0e[1]);
      printf("param[2]=%g\n",param_spc0e[2]);
      printf("param[3]=%g\n",param_spc0e[3]);
      printf("param[4]=%g\n",param_spc0e[4]);
      printf("param[5]=%g\n",param_spc0e[5]);
      printf("param[6]=%g\n",param_spc0e[6]);
      printf("param[7]=%g\n",param_spc0e[7]);
    }*/

    for (j=0;j<6;j++){
        dotepsp[j] =s[i][j]*3.0*p->q3*dlb/sig02;
            //ps[i][j]=ps[i][j]+dotepsp[j];
    }
    double normdoteps=0.0;
    for (j=0;j<6;j++){
        normdoteps=normdoteps+sqrt(2.0/3.0*dotepsp[j]*dotepsp[j]);
    }
    cump[i]=pvcump[i]+normdoteps;
    double dotepspm=dlb*3.0*porosity*sinh(param_spc0e[1]*(sig_m+pressure))/p->sig0;
    for (j=0;j<3;j++){
        dotepsp[j] =dotepsp[j]+dotepspm;
          //ps[i][j]=ps[i][j]+dotepspm;
    }
    double test_normdot=fabs(normdoteps);
    if (test_normdot>5.0) {
        printf("Pb!,norm(dot(eps))=%g\n",normdoteps);
        status=-11;
        point_pb=i;
    }
   
    for (j=0;j<3;j++){
         e[i][j]=ep[i][j]+((s[i][j]-sp[i][j]+trsp)/(2.0*p->mu))+(sig_m-trsp)/(3.0*p->k)+dotepsp[j];
    }   
    for (j=3;j<6;j++){
         e[i][j]=ep[i][j]+((s[i][j]-sp[i][j])/(2.0*p->mu))+dotepsp[j];
    }   
    for (j=0;j<3;j++){
          s[i][j] = s[i][j]+sig_m;
    }
//now s is the stress
  }//end for i
  //printf( "pressure=%g\n",pressure);
  if (point_pb!=0) {printf(" !!!!!!!!!!!!!!status=%i!!!!!!!!!!!!!!,point_pb=%i!!!\n",status,point_pb);status=-1;}
  return status;
}

