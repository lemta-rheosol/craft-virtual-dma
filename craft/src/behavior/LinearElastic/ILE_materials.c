#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <meca.h>
#include <materials.h>
#include <string.h>
#include <utils.h>

#include <ILE_materials.h>
#include <LE_materials.h>
#include <LE_Cmatrix.h>
#include <variables.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009

   functions to manage Isotropic Linear Elastic behavior

*/
/*######################################################################*/
int read_parameters_ILE( FILE *f, void *p ) {
  int status;
  int isotropy;
  void *q;

  ((LE_param *)p)->isotropy = ISOTROPIC;

  if ( (q = malloc(sizeof(ILE_param))) == NULL) return -1;
  status = read_ILE_param(f, (ILE_param *)q);
  ((LE_param *)p)->sub.ile = q;

  if (status!=0) return -1;

  return 0;
}
