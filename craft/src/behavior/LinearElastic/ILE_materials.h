/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   header file for Isotropic Linear Elastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef ILE_MATERIAL
#define ILE_MATERIAL 1                 /* Isotropic Linear Elastic  behavior */

#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>




/**************************************************************************/
typedef struct {
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lamé coefficient */
  double mu;    /* 2d Lamé coefficient  */
  double k;     /* compression modulus  */
} ILE_param;

/*------------------------------------------------------------------------*/
int read_parameters_ILE( FILE *f, void *p );
/**************************************************************************/
#endif
/**************************************************************************/

