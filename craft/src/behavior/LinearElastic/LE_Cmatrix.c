#define _ISOC99_SOURCE

#ifdef _OPENMP
#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <materials.h>
#include <utils.h>
#include <meca.h>

#include <LE_Cmatrix.h>
#include <LE_materials.h>

#include <complex.h>


/*************************************************************/
/*        Function reading and writing the C matrix          */
/*                   in the cristal axis                     */
/*************************************************************/
int read_Cmatrix( FILE *f, double C[6][6], double C_ini[6][6], int isotropie) {
  int i,j,status;
  LE_param *p;
  void *q;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      C[i][j] = NAN;
      C_ini[i][j] = 0.;
    }
  }

  p = malloc(sizeof(LE_param));
  if (p==NULL) return -1;
  p->isotropy = isotropie;

  switch (isotropie){
  case ANISOTROPIC:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_ALE_param(f, (ALE_param *)q);
    p->sub.ale = q;
    break;
  case ISOTROPIC:
    if ( (q = malloc(sizeof(ILE_param))) == NULL) return -1;
    status = read_ILE_param(f, (ILE_param *)q);
    p->sub.ile = q;
    break;
  case CUBIC_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_CSLE_param(f, (ALE_param *)q);
    p->sub.ale = q;
    break;
  case HEXAGONAL_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_HSLE_param(f, (ALE_param *)q);
    p->sub.ale = q;
    break;
  case ORTHOTROPIC_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_OSLE_param(f, (ALE_param *)q);
    p->sub.ale = q;
    break;
  default:
    fprintf(stderr, "Invalid isotropy flag (%d).\n", isotropie);
    status = -1;
  }
  //  load_Cmatrix_LE((void *)p, C);
  load_Cmatrix_LE((void *)p, C_ini);
  free(q);
  free(p);
  return 0;
}


/*********************************************************************************************/
int read_ALE_param( FILE *f, ALE_param *x){
  int status;
  int i,j;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      x->C[i][j] = NAN;
      x->C_ini[i][j] = 0.;
    }
  }

  status=craft_fscanf(f,"%lf",&(x->C_ini[0][0]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[0][1]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[0][2]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[0][3]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[0][4]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[0][5]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[1][1]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[1][2]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[1][3]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[1][4]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[1][5]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[2][2]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[2][3]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[2][4]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[2][5]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[3][3]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[3][4]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[3][5]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[4][4]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[4][5]));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->C_ini[5][5]));
  if (status!=1) return -1;


  x->C_ini[1][0] = x->C_ini[0][1];
  x->C_ini[2][0] = x->C_ini[0][2];
  x->C_ini[2][1] = x->C_ini[1][2];

  x->C_ini[3][0] = x->C_ini[0][3];
  x->C_ini[4][0] = x->C_ini[0][4];
  x->C_ini[5][0] = x->C_ini[0][5];
  x->C_ini[3][1] = x->C_ini[1][3];
  x->C_ini[4][1] = x->C_ini[1][4];
  x->C_ini[5][1] = x->C_ini[1][5];
  x->C_ini[3][2] = x->C_ini[2][3];
  x->C_ini[4][2] = x->C_ini[2][4];
  x->C_ini[5][2] = x->C_ini[2][5];
  x->C_ini[4][3] = x->C_ini[3][4];
  x->C_ini[5][3] = x->C_ini[3][5];
  x->C_ini[5][4] = x->C_ini[4][5];

  return 0;
}

/*********************************************************************************************/
int read_ILE_param( FILE *f, ILE_param *x){
  int status;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );
  return 0;

/*  C_ini[0][0]= lb + 2.*mu;*/
/*  C_ini[0][1]= lb;*/
/*  C_ini[0][2]= lb;*/
/*  C_ini[1][1]= lb + 2.*mu;*/
/*  C_ini[1][2]= lb;*/
/*  C_ini[2][2]= lb + 2.*mu;*/
/*  C_ini[3][3]=  2.*mu;*/
/*  C_ini[4][4]=  2.*mu;*/
/*  C_ini[5][5]=  2.*mu;*/
/*  C_ini[1][0] = C_ini[0][1];*/
/*  C_ini[2][0] = C_ini[0][2];*/
/*  C_ini[2][1] = C_ini[1][2];*/
}

/*********************************************************************************************/
int read_CSLE_param( FILE *f, ALE_param *x){
  int status,i,j;
  double K, mu1, mu2;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      x->C[i][j] = NAN;
      x->C_ini[i][j] = 0.;
    }
  }

  status=craft_fscanf(f,"%lf",&K);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&mu1);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mu2);
  if (status!=1) return -1;
  /*
  x->C_ini[0][0] = x->C_ini[1][1] = x->C_ini[2][2] = K+4./3.*mu1;
  x->C_ini[0][1] = x->C_ini[0][2] = x->C_ini[1][2] = K-2./3.*mu1;
  x->C_ini[1][0] = x->C_ini[2][0] = x->C_ini[2][1] = K-2./3.*mu1;
  x->C_ini[3][3] = x->C_ini[4][4] = x->C_ini[5][5] = 2.*mu2;
  */

    x->C_ini[0][0]= (3.*K+4.*mu1)/3.;
    x->C_ini[0][1]= (3.*K-2.*mu1)/3.;
    x->C_ini[0][2]= (3.*K-2.*mu1)/3.;
    x->C_ini[1][1]= (3.*K+4.*mu1)/3.;
    x->C_ini[1][2]= (3.*K-2.*mu1)/3.;
    x->C_ini[2][2]= (3.*K+4.*mu1)/3.;
    x->C_ini[3][3]=  2.*mu2;
    x->C_ini[4][4]=  2.*mu2;
    x->C_ini[5][5]=  2.*mu2;
    
    x->C_ini[1][0] = x->C_ini[0][1];
    x->C_ini[2][0] = x->C_ini[0][2];
    x->C_ini[2][1] = x->C_ini[1][2];


  return 0;
}

/*********************************************************************************************/
int read_HSLE_param( FILE *f, ALE_param *x){
  int status,i,j;
  double El, nul, K, mut, gamma, alpha, mul;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      x->C[i][j] = NAN;
      x->C_ini[i][j] = 0.;
    }
  }

  status=craft_fscanf(f,"%lf",&K);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&mut);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mul);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&El);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&nul);
  if (status!=1) return -1;

  alpha = El+4.*nul*nul*K;
  gamma = 2*nul*K;

  /* c'etait tout faux!!! */
  /*
  x->C_ini[0][0] = x->C_ini[1][1] = K+4./3.*mut;
  x->C_ini[2][2] = alpha;
  x->C_ini[0][1] = x->C_ini[1][0] = K-mut;
  x->C_ini[0][2] = x->C_ini[1][2] = x->C_ini[2][0] = x->C_ini[2][1] = gamma;
  x->C_ini[3][3] = x->C_ini[4][4] = x->C_ini[5][5] = 2.*mul;
  */

    x->C_ini[0][0]= K + mut;
    x->C_ini[0][1]= K - mut;
    x->C_ini[0][2]= gamma;
    x->C_ini[1][1]= K + mut;
    x->C_ini[1][2]= gamma;
    x->C_ini[2][2]= alpha;
    x->C_ini[3][3]= 2.*mul;
    x->C_ini[4][4]= 2.*mul;
    x->C_ini[5][5]= 2.*mut;    
    
    x->C_ini[1][0] = x->C_ini[0][1];
    x->C_ini[2][0] = x->C_ini[0][2];
    x->C_ini[2][1] = x->C_ini[1][2];


  return 0;
}

/*********************************************************************************************/
int read_OSLE_param( FILE *f, ALE_param *x){
  int status,i,j;
  double E1,E2,E3;
  double nu12, nu13, nu23;
  double nu21, nu31, nu32;
  double mu12, mu13, mu23;
  double k, D;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      x->C[i][j] = NAN;
      x->C_ini[i][j] = 0.;
    }
  }

  status=craft_fscanf(f,"%lf",&E1);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&E2);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&E3);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&nu12);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&nu13);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&nu23);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&mu12);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mu13);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mu23);
  if (status!=1) return -1;

  nu21 = E2/E1*nu12;
  nu32 = E3/E2*nu23;
  nu31 = E3/E1*nu13;

  k = 1. - nu23*nu32 - nu13*nu31 - nu12*nu21 - 2.*nu12*nu23*nu31;
  D = E1*E2*E3/k;

  x->C_ini[0][0] = E1/k*(1-nu23*nu32);
  x->C_ini[1][1] = E2/k*(1-nu13*nu31);
  x->C_ini[2][2] = E3/k*(1-nu12*nu21);
  x->C_ini[0][1] = x->C_ini[1][0] = E1/k*(nu23*nu31+nu21);
  x->C_ini[0][2] = x->C_ini[2][0] = E1/k*(nu32*nu21+nu31);
  x->C_ini[1][2] = x->C_ini[2][1] = E2/k*(nu31*nu12+nu32);
  x->C_ini[3][3] = 2*mu23;
  x->C_ini[4][4] = 2*mu13;
  x->C_ini[5][5] = 2*mu12;

  return 0;
}

/*************************************************************/
/*   Function computing the C matrix in the lab axis         */
/*************************************************************/
int rotate_Cmatrix(double C[6][6], double C_ini[6][6],
                   Euler_Angles orientation) {

  int status;
  int i,j,k,l;
  double r[6][6];
  double cphi1,cphi,cphi2,sphi1,sphi,sphi2;
  double p11,p12,p13,p21,p22,p23,p31,p32,p33;double rac2,urac2;

  status=0;

  rac2=sqrt(2.);
  urac2=1./rac2;

  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }


  cphi1 = cos(orientation.phi1);
  cphi = cos(orientation.Phi);
  cphi2 = cos(orientation.phi2);
  sphi1 = sin(orientation.phi1);
  sphi = sin(orientation.Phi);
  sphi2 = sin(orientation.phi2);

  p11 = cphi2*cphi1-sphi2*sphi1*cphi;
  p12 = -sphi2*cphi1-cphi2*sphi1*cphi;
  p13 = sphi1*sphi;
  p21 = cphi2*sphi1+sphi2*cphi1*cphi;
  p22 = -sphi2*sphi1+cphi2*cphi1*cphi;
  p23 = -cphi1*sphi;
  p31 = sphi2*sphi;
  p32 = cphi2*sphi;
  p33 = cphi;

  /* Matrice de passage labo -> cristal en notations Bornert-Suquet */

  r[0][0]=p11*p11;
  r[0][1]=p21*p21;
  r[0][2]=p31*p31;
  r[0][3]=rac2*p31*p21;
  r[0][4]=rac2*p11*p31;
  r[0][5]=rac2*p11*p21;
  r[1][0]=p12*p12;
  r[1][1]=p22*p22;
  r[1][2]=p32*p32;
  r[1][3]=rac2*p32*p22;
  r[1][4]=rac2*p12*p32;
  r[1][5]=rac2*p12*p22;
  r[2][0]=p13*p13;
  r[2][1]=p23*p23;
  r[2][2]=p33*p33;
  r[2][3]=rac2*p33*p23;
  r[2][4]=rac2*p13*p33;
  r[2][5]=rac2*p13*p23;
  r[3][0]=rac2*p12*p13;
  r[3][1]=rac2*p22*p23;
  r[3][2]=rac2*p32*p33;
  r[3][3]=p22*p33+p32*p23;
  r[3][4]=p12*p33+p32*p13;
  r[3][5]=p12*p23+p22*p13;
  r[4][0]=rac2*p11*p13;
  r[4][1]=rac2*p21*p23;
  r[4][2]=rac2*p31*p33;
  r[4][3]=p21*p33+p31*p23;
  r[4][4]=p11*p33+p31*p13;
  r[4][5]=p11*p23+p21*p13;
  r[5][0]=rac2*p11*p12;
  r[5][1]=rac2*p21*p22;
  r[5][2]=rac2*p31*p32;
  r[5][3]=p21*p32+p31*p22;
  r[5][4]=p11*p32+p31*p12;
  r[5][5]=p11*p22+p21*p12;


  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      for (k=0;k<6;k++){
    	for (l=0;l<6;l++){
  	  C[i][j] = C[i][j] + r[l][i] * C_ini[l][k] * r[k][j];
    	}
      }
    }
  }

  return status;
  }


/*************************************************************/
/*   Linear Elastic behaviour law computing from C matrix    */
/* (may be applied "in place": i.e. sigma and epsilon 
   are on the same memory place */
/*************************************************************/
int LE_behavior_Cmatrix(double epsilon[6],
                        double sigma[6],
                        double C[6][6]) {

   double rac2,urac2;
   int status;

   status = 0;

   rac2=sqrt(2.);
   urac2=1./rac2;

   double e[6];

   e[0] = epsilon[0];
   e[1] = epsilon[1];
   e[2] = epsilon[2];
   e[3] = epsilon[3];
   e[4] = epsilon[4];
   e[5] = epsilon[5];

   sigma[0] = C[0][0] * e[0]+
              C[0][1] * e[1]+
	      C[0][2] * e[2]+
	      rac2 * C[0][3] * e[3]+
	      rac2 * C[0][4] * e[4]+
	      rac2 * C[0][5] * e[5];

    sigma[1] = C[1][0] * e[0]+
               C[1][1] * e[1]+
	       C[1][2] * e[2]+
	       rac2 * C[1][3] * e[3]+
	       rac2 * C[1][4] * e[4]+
	       rac2 * C[1][5] * e[5];

    sigma[2] = C[2][0] * e[0]+
               C[2][1] * e[1]+
	       C[2][2] * e[2]+
	       rac2 * C[2][3] * e[3]+
	       rac2 * C[2][4] * e[4]+
	       rac2 * C[2][5] * e[5];

    sigma[3] = urac2 * C[3][0] * e[0]+
               urac2 * C[3][1] * e[1]+
	       urac2 * C[3][2] * e[2]+
	       C[3][3] * e[3]+
	       C[3][4] * e[4]+
	       C[3][5] * e[5];

    sigma[4] = urac2 * C[4][0] * e[0]+
               urac2 * C[4][1] * e[1]+
	       urac2 * C[4][2] * e[2]+
	       C[4][3] * e[3]+
	       C[4][4] * e[4]+
	       C[4][5] * e[5];

    sigma[5] = urac2 * C[5][0] * e[0]+
               urac2 * C[5][1] * e[1]+
	       urac2 * C[5][2] * e[2]+
	       C[5][3] * e[3]+
	       C[5][4] * e[4]+
	       C[5][5] * e[5];

    return status;
}


/*************************************************************/
/* apply linear elasticity in the generic case 
   
   epsilon and sigma may designate the same memory region
   (in place computation)

*/
/*************************************************************/

int linear_elastic( LE_param *L,  Euler_Angles *orientation, int N, double (*epsilon)[6], double (*sigma)[6]) {

  double coef1, coef2;
  int i;
  double P;
  int status;

  status=0;

  switch(L->isotropy){
  case ANISOTROPIC:
    /* if the matrix C had never been innitialized */
    //    if (L->sub.ale->C[0][0] == NAN){
    if (isnan(L->sub.ale->C[0][0])){
      status = rotate_Cmatrix(L->sub.ale->C, L->sub.ale->C_ini, *orientation);
    }
    /* application of the linear elastic behavior law */
#pragma omp parallel for \
default(none) \
private(i) \
shared(N,epsilon,sigma,L,status)
    for(i=0;i<N;i++){
      status = LE_behavior_Cmatrix(epsilon[i],sigma[i],L->sub.ale->C);
    }
    break;

  case ISOTROPIC:
    coef1 = L->sub.ile->lb;
    coef2 = 2.*L->sub.ile->mu;
#pragma omp parallel for                        \
  default(none)                                 \
  private(i, P)					\
  shared (N, coef1, coef2, sigma, epsilon)
    for(i=0;i<N;i++){
      P = coef1 * (epsilon[i][0] + epsilon[i][1] + epsilon[i][2]) ;
      sigma[i][0] = P + coef2*epsilon[i][0];
      sigma[i][1] = P + coef2*epsilon[i][1];
      sigma[i][2] = P + coef2*epsilon[i][2];
      sigma[i][3] = coef2 * epsilon[i][3];
      sigma[i][4] = coef2 * epsilon[i][4];
      sigma[i][5] = coef2 * epsilon[i][5];
    }
    break;

  default:
    fprintf(stderr, "Isotropy flag should not be >1 here.\n");
    status = -1;
    break;
  }

  return status;

}

/* added for harmonic implementation 2018/07/09  J. Boisse */
int linear_elastic_harmonic( LE_param *L,  Euler_Angles *orientation, int N, double complex (*epsilon)[6], double complex (*sigma)[6]) {

  double coef1, coef2;
  int i;
  double complex P;
  int status;

  status=0;

  /* harmonic implementation 2018/07/09  J. Boisse => only work for ISOTROPIC for now */
  switch(L->isotropy){
  case ANISOTROPIC:
    /* if the matrix C had never been innitialized */
    //    if (L->sub.ale->C[0][0] == NAN){
    if (isnan(L->sub.ale->C[0][0])){
      status = rotate_Cmatrix(L->sub.ale->C, L->sub.ale->C_ini, *orientation);
    }
    /* application of the linear elastic behavior law */
#pragma omp parallel for \
default(none) \
private(i) \
shared(N,epsilon,sigma,L,status)
    for(i=0;i<N;i++){
      status = LE_behavior_Cmatrix(epsilon[i],sigma[i],L->sub.ale->C);
    }
    break;

  case ISOTROPIC:
    coef1 = L->sub.ile->lb;
    coef2 = 2.*L->sub.ile->mu;
#pragma omp parallel for                        \
  default(none)                                 \
  private(i, P)					\
  shared (N, coef1, coef2, sigma, epsilon)
    for(i=0;i<N;i++){
      P = coef1 * (epsilon[i][0] + epsilon[i][1] + epsilon[i][2]) ;
      sigma[i][0] = P + coef2*epsilon[i][0];
      sigma[i][1] = P + coef2*epsilon[i][1];
      sigma[i][2] = P + coef2*epsilon[i][2];
      sigma[i][3] = coef2 * epsilon[i][3];
      sigma[i][4] = coef2 * epsilon[i][4];
      sigma[i][5] = coef2 * epsilon[i][5];
    }
    break;

  default:
    fprintf(stderr, "Isotropy flag should not be >1 here.\n");
    status = -1;
    break;
  }

  return status;

}
