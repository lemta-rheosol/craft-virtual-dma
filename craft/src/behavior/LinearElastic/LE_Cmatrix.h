#ifndef _LE_CMATRIX_
#define _LE_CMATRIX_

#include <LE_materials.h>

int read_Cmatrix( FILE *f, double C[6][6], double C_ini[6][6], int isotropie);
int read_ALE_param( FILE *f, ALE_param *x);
int read_ILE_param( FILE *f, ILE_param *x);
int read_CSLE_param( FILE *f, ALE_param *x);
int read_HSLE_param( FILE *f, ALE_param *x);
int read_OSLE_param( FILE *f, ALE_param *x);

int rotate_Cmatrix(double C[6][6], double C_ini[6][6], 
                   Euler_Angles orientation);

int LE_behavior_Cmatrix(double epsilon[6],
                        double sigma[6],
                        double C[6][6]);

int linear_elastic( LE_param *C,  Euler_Angles *orientation, int N, double (*epsilon)[6], double (*sigma)[6]);
/* added for harmonic implementation 2018/07/09  J. Boisse */
int linear_elastic_harmonic( LE_param *C,  Euler_Angles *orientation, int N, double complex (*epsilon)[6], double complex (*sigma)[6]);
#endif
