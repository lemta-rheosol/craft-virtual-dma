#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>

#include <LE_materials.h>
#include <LE_Cmatrix.h>
#include <variables.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#define NEGLIGIBLE 1.e-12

/*######################################################################*/
/* A. Labe
   LMA/CNRS
   october 7th 2009

   functions to manage Anisotropic Linear Elastic behavior

*/
/*######################################################################*/
int read_parameters_LE( FILE *f, void *p ) {
  int status;
  int isotropy;
  void *q;
  /*
  printf("\n enter the kind of isotropy :\n\t0 -> Cmatrix \n\t1->Isotropy ");
  printf("\n\t2->cubic symetry ");
  printf("\n\t3 -> hexagonal symetry \n\t4->Orthotropic symetry");
  */
  status=craft_fscanf(f,"%d",&isotropy);
  if (status!=1) return -1;
  ((LE_param *)p)->isotropy = isotropy;

  switch (isotropy){
  case ANISOTROPIC:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_ALE_param(f, (ALE_param *)q);
    ((LE_param *)p)->sub.ale = q;
    break;
  case ISOTROPIC:
    if ( (q = malloc(sizeof(ILE_param))) == NULL) return -1;
    status = read_ILE_param(f, (ILE_param *)q);
    ((LE_param *)p)->sub.ile = q;
    break;
  case CUBIC_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_CSLE_param(f, (ALE_param *)q);
    ((LE_param *)p)->sub.ale = q;
    break;
  case HEXAGONAL_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_HSLE_param(f, (ALE_param *)q);
    ((LE_param *)p)->sub.ale = q;
    break;
  case ORTHOTROPIC_SYMMETRY:
    if ( (q = malloc(sizeof(ALE_param))) == NULL) return -1;
    status = read_OSLE_param(f, (ALE_param *)q);
    ((LE_param *)p)->sub.ale = q;
    break;
  default:
    fprintf(stderr, "Invalid isotropy flag (%d).\n", isotropy);
    status = -1;
  }

  if (status!=0) return -1;
  if (isotropy!=ISOTROPIC){
    /* Reverting all non-isotropic cases to ANISOTROPIC
       Symmetries only allows an economic parameter passing */
    ((LE_param *)p)->isotropy = ANISOTROPIC;
  }
  return 0;
}

/*######################################################################*/
int print_parameters_LE( FILE *f, void *p , int flag ) {
  int status;
  int i,j;
  status=0;
  LE_param *q;
  q = (LE_param *)p;

  switch (q->isotropy){
  case ANISOTROPIC:
    fprintf(f,"# C_matrix (in the crystal reference):\n");
    for(i=0;i<6;i++) {
      fprintf(f,"# ");
      for(j=0;j<6;j++){
        fprintf(f,"%lf ",(q->sub.ale->C_ini[i][j]));
      }
      fprintf(f,"\n");
    }
    break;
  case ISOTROPIC:
    if (flag==1) {
      fprintf(f,"# Isotropic Linear Elastic Material\n");
      fprintf(f,"# E  = %lf\n",(q->sub.ile->E));
      fprintf(f,"# nu = %lf\n",(q->sub.ile->nu));
      fprintf(f,"# 1st Lamé coef.  = %lf\n",(q->sub.ile->lb));
      fprintf(f,"# 2d Lamé coef. = %lf\n",(q->sub.ile->mu));
      fprintf(f,"# bulk modulus  = %lf\n",(q->sub.ile->k));
    }
    else{
      fprintf(f,"Isotropic Linear Elastic Material\n");
      fprintf(f,"E  = %lf\n",(q->sub.ile->E));
      fprintf(f,"nu = %lf\n",(q->sub.ile->nu));
      fprintf(f,"1st Lamé coef.  = %lf\n",(q->sub.ile->lb));
      fprintf(f,"2d Lamé coef. = %lf\n",(q->sub.ile->mu));
      fprintf(f,"bulk modulus  = %lf\n",(q->sub.ile->k));
    }
    break;
  default:
    fprintf(stderr, "Isotropy flag should not be >1 here.\n");
    status = -1;
    break;
  }

  return status;
}

/*######################################################################*/
int allocate_parameters_LE( void **p ) {
  LE_param *x;

  x=(LE_param *)malloc(sizeof(LE_param));
  if ( x == NULL ){
    return -1;
  }

  *p = (void *)x;
  if( p == (void *)0 ) {
    return -1;
  }

  return 0;
}

/*######################################################################*/
int deallocate_parameters_LE( void *p ) {
  int status;
  LE_param *q;

  status=0;
  q = (LE_param *)p;
  free(q->sub.ale);
/*  switch(q->isotropy){*/
/*  case ANISOTROPIC:*/
/*    free(q->sub.ale);*/
/*    break;*/
/*  case ISOTROPIC:*/
/*    free(q->sub.ile);*/
/*    break;*/
/*  case CUBIC_SYMMETRY:*/
/*    free(q->sub.csle);*/
/*    break;*/
/*  case HEXAGONAL_SYMMETRY:*/
/*    free(q->sub.hsle);*/
/*    break;*/
/*  case ORTHOTROPIC_SYMMETRY:*/
/*    free(q->sub.osle);*/
/*    break;*/
/*  }*/
  free(p);
  return status;
}

/*######################################################################*/
int copy_parameters_LE( void *param_dest, void *param_src) {
  LE_param *p;
  LE_param *q;
  p = (LE_param *)param_dest;
  q = (LE_param *)param_src;
  *p = *q;

  switch(q->isotropy){
  case ANISOTROPIC:
    p->sub.ale = malloc(sizeof(ALE_param));
    if (p->sub.ale==NULL) return -1;
    *(p->sub.ale) = *(q->sub.ale);
    break;
  case ISOTROPIC:
    p->sub.ile = malloc(sizeof(ILE_param));
    if (p->sub.ile==NULL) return -1;
    *(p->sub.ile) = *(q->sub.ile);
    break;
  default:
    fprintf(stderr, "Isotropy flag should not be >1 here.\n");
    return -1;
    break;
  }

  return 0;
}

/*######################################################################*/
int behavior_LE(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt ){
  int status;
  int N;

  double (*epsilon) [6];
  double (*sigma) [6];
  LE_param *L;
  L = (LE_param *)param;
  status = 0;
  N = sv->np;
  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);

  status = linear_elastic( L, &orientation, N, epsilon, sigma);
			  
  return status;
}

/*######################################################################*/
int  allocate_variables_LE( int N, Variables *sv, void *param) {
  int status;
  int scalar_t[3]={SCALAR,0,0};
  int tensor_t[3]={TENSOR2,0,0};
  status=0;

  sv->np = N;
  sv->nvar = 3;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;

  return status;
}

/*######################################################################*/
int extrapolate_variables_LE(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3
     ) {
  int i,j,status;
  double (*epsilon) [6];
  double (*epsilon_p) [6];

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);

  status = extrapol2( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );
  return status;
}


/*######################################################################*/
int load_Cmatrix_LE(void *param, double C[6][6]) {
  int i,j;
  LE_param p;
  p = *((LE_param *)param);

  if (p.isotropy != ANISOTROPIC){
    C[0][3] = C[0][4] = C[0][5] = 0.;
    C[1][3] = C[1][4] = C[1][5] = 0.;
    C[2][3] = C[2][4] = C[2][5] = 0.;
    C[3][0] = C[3][1] = C[3][2] = C[3][4] = C[3][5] = 0.;
    C[4][0] = C[4][1] = C[4][2] = C[4][3] = C[4][5] = 0.;
    C[5][0] = C[5][1] = C[5][2] = C[5][3] = C[5][4] = 0.;
  }

  switch(p.isotropy){
  case ANISOTROPIC:
    for (i=0;i<6;i++){
      for (j=0;j<6;j++){
        C[i][j] = p.sub.ale->C_ini[i][j];
      }
    }
    break;

  case ISOTROPIC:
    C[0][0] = C[1][1] = C[2][2] = p.sub.ile->lb + 2.*p.sub.ile->mu;
    C[0][1] = C[1][0] = p.sub.ile->lb;
    C[0][2] = C[2][0] = p.sub.ile->lb;
    C[1][2] = C[2][1] = p.sub.ile->lb;
    C[3][3] = C[4][4] = C[5][5] = 2.*p.sub.ile->mu;
    break;

  default:
    fprintf(stderr, "Isotropy flag should not be >1 here.\n");
    return -1;
  }

  return(0);
}

/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
void inversion_(double *a, double *b, int *n, double *err);
int solve_spc0e_LE( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double (*tau)[6]
		 ){
  
  int status;
  int N;
  
  double (*epsilon) [6];
  double (*sigma) [6];
  LE_param *L;
  int i,j;

  L = (LE_param *)param;
  status = 0;
  N = sv->np;
  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  

  /* anisotropic case */
  if ( (L->isotropy == ANISOTROPIC) || (L0->isotropy == ANISOTROPIC) ) {
    double C[6][6];
    double C0[6][6];
    double CC[6][6];
    double CCC[6][6];

    status = load_Cmatrix_LE( L,  CC);
    status = load_Cmatrix_LE( L0, C0);
    for(i=0;i<6;i++) {
      for(j=0;j<6;j++) {
	CCC[i][j] = CC[i][j] + C0[i][j];
      }
    }

    // bug fixing (HM 18 dec 2015)
    status = rotate_Cmatrix(C,CC,orientation);    
    status = rotate_Cmatrix(CC,CCC,orientation);    
    
    double err;
    int n=6;
    inversion_(&CC[0][0],&CC[0][0],&n,&err); /* inversion of C+C0 matrix */
    //    if(err > 1.e-15 ) {
    if(err > NEGLIGIBLE ) {
      fprintf(stderr,"CraFT error: matrix inversion failed in solve_spc0e\n");
      status = -1;
      return status;
    }

#pragma omp parallel for			\
  default(none)					\
  private(i)					\
  shared(N,epsilon,sigma,tau,L,status,CC,C)
    for(i=0;i<N;i++){
      status = LE_behavior_Cmatrix(tau[i],epsilon[i],CC);
      status = LE_behavior_Cmatrix(epsilon[i],sigma[i],C);
    }



  }
  /* isotropic case */
  else if( (L->isotropy == ISOTROPIC) & (L0->isotropy == ISOTROPIC) ) {
    double lb,mu;

    double lb2,mu2;
    double k2;

    double h;
    double coef1, coef2;
    double h2;

    lb = L->sub.ile->lb;
    mu = L->sub.ile->mu;

    lb2 = L->sub.ile->lb + L0->sub.ile->lb;
    mu2 = L->sub.ile->mu + L0->sub.ile->mu;

    coef1 = 1./(3.*lb2+2.*mu2);
    coef2 = 0.5/mu2;

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2,lb,mu) 
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3.;
      h2 = coef1 * h;

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

      h2 = 3.*lb*h2;

      sigma[i][0] = h2 + 2.*mu*epsilon[i][0];
      sigma[i][1] = h2 + 2.*mu*epsilon[i][1];
      sigma[i][2] = h2 + 2.*mu*epsilon[i][2];

      sigma[i][3] = 2.*mu*epsilon[i][3];
      sigma[i][4] = 2.*mu*epsilon[i][4];
      sigma[i][5] = 2.*mu*epsilon[i][5];


    }

      
      
  }
			  
  return status;
}
