/**************************************************************************/
/* A. Labe
   LMA/CNRS
   october 7th 2009

   header file for Linear Elastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef LE_MATERIAL
#define LE_MATERIAL 10              /* Linear Elastic */
#define LE_MATERIAL_OLD 3           /* Linear Elastic old version */

#include <ILE_materials.h>


#define _ISOC99_SOURCE

#include <euler.h>


/**************************************************************************/
enum isotropy_entry_type{
  ANISOTROPIC   =0,
  ISOTROPIC     =1,
  CUBIC_SYMMETRY       =2,
  HEXAGONAL_SYMMETRY   =3,
  ORTHOTROPIC_SYMMETRY =4,
};

typedef struct {
  double C_ini[6][6];  /* Rigidity matrix in the cristal axis*/
  double C[6][6];      /* Rigidity matrix in the laboratory axis*/
} ALE_param;


#ifndef ILE_MATERIAL
typedef struct {
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* bulk modulus  */
} ILE_param;
#endif

typedef struct {
  int isotropy;
  union {
    ALE_param* ale;
    ILE_param* ile;
  } sub;
} LE_param;

/*------------------------------------------------------------------------*/
int read_parameters_LE( FILE *f, void *p );
int print_parameters_LE( FILE *f, void *p , int flag);

int allocate_parameters_LE( void **p );
int deallocate_parameters_LE( void *p );
int copy_parameters_LE( void *param_dest, void *param_src);


int behavior_LE(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt);

int allocate_variables_LE( int N, Variables *sv, void *param);
int extrapolate_variables_LE(
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3);
int load_Cmatrix_LE(void *param, double C[6][6]);
int solve_spc0e_LE( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double (*tau)[6]
		    );
/**************************************************************************/
#endif
/**************************************************************************/
