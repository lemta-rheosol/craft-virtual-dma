#include <math.h>
#include <strings.h>

/*######################################################################*/
/* function computing Lam� coefficients and bulk modulus from
   Young's modulus and Poisson coefficient                              */
void lame( double *E, double *nu, double *lb, double *mu, double *k) {
  *lb = *E**nu/(1.+*nu)/(1.-2.**nu);
  *mu = *E/2./(1.+*nu);
  *k = *lb+2./3.**mu;
}

/*######################################################################*/
/* function computing Young's modulus, Poisson coefficient and bulk modulus from
   Lam� coefficients */                            
void elastic( double *E, double *nu, double *lb, double *mu, double *k) {
  *k = *lb+2./3.**mu;
  *E = *mu*(3.**lb+2.**mu)/(*lb+*mu);
  *nu = *lb/(2.*(*lb+*mu));
}

/*######################################################################*/
/* calculates the equivalent strain or stress on N points               */
int equivalent( const char *strain_or_stress, int N, double x[N][6], double eeq[N] ){
  int i;
  int status;

  double coef;
  double trace;
  double xd2;
  
  if ( strcasecmp(strain_or_stress,"strain")==0 ) {
    coef = 2./3.;
  }
  else if ( strcasecmp(strain_or_stress,"stress")==0 ) {
    coef = 3./2.;
  }
  else{
    status = -1;
    return status;
  }

#pragma omp parallel for                        \
  default(none)                                 \
  private(i,trace,xd2)                          \
  shared(N,coef,eeq,x)
    for(i=0;i<N;i++) {
      trace = x[i][0] + x[i][1]  + x[i][2];
      xd2 =
        x[i][0]*x[i][0] + x[i][1]*x[i][1] + x[i][2]*x[i][2] +
        2.*( x[i][3]*x[i][3] + x[i][4]*x[i][4] + x[i][5]*x[i][5] )
        - 1./3. * trace*trace;
      eeq[i] = sqrt( coef * xd2); 
    }
    
    status=0;
    
    return status;
    
}
