#ifndef __MECA__
#define __MECA__
/*----------------------------------------------------------------------------*/
/* function computing Lam� coefficients and bulk modulus from
   Young's modulus and Poisson coefficient                                    */
void lame( double *E, double *nu, double *lb, double *mu, double *k);
void elastic( double *E, double *nu, double *lb, double *mu, double *k) ;
int equivalent( const char *strain_or_stress, int N, double x[N][6], double eeq[N] );
/*----------------------------------------------------------------------------*/
#endif
