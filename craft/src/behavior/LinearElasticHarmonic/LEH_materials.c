#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <LEH_materials.h>
#include <variables.h>

#include <utils.h>
/*######################################################################*/
/*  Nancy
   LEMTA
   08 Septembre 2017
   
   
   functions to manage Linear Elastic Harmonic behavior 

*/
/*######################################################################*/
int read_parameters_LEH( FILE *f, void *p ) {

  LEH_param *x;
  
  int status;

#ifdef DEBUG
  printf("LEH_material.c : in read_parameters_LEH()\n");
#endif 

  x = (LEH_param *)p;

  status=craft_fscanf(f,"%lf",&(x->E));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->nu));
  if (status!=1) return -1;

  lame( &(x->E), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );

  return 0;
}
/*######################################################################*/
int print_parameters_LEH( FILE *f, void *p , int flag ) {
  int status;
  status=0;

#ifdef DEBUG
  printf("LEH_material.c : in print_parameters_LEH()\n");
#endif 

  if (flag==1) {
    fprintf(f,"# E  = %lf  ",(((LEH_param *)p)->E));
    fprintf(f,"\n");
    fprintf(f,"# nu = %lf  ",(((LEH_param *)p)->nu));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"E  = %lf  ",(((LEH_param *)p)->E));
    fprintf(f,"nu = %lf  ",(((LEH_param *)p)->nu));
  }
  
  return status;
}

/*######################################################################*/
int allocate_parameters_LEH( void **p ) {

  LEH_param *x;
  int status;

#ifdef DEBUG
  printf("LEH_material.c : in allocate_parameters_LEH()\n");
#endif 

  status=0;

  x=(LEH_param *)malloc(sizeof(LEH_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_LEH( void *p ) {

  int status;

#ifdef DEBUG
  printf("LEH_material.c : in deallocate_parameters_LEH()\n");
#endif 

  status=0;

  free(p);
  
  return status;

}
/*######################################################################*/
int copy_parameters_LEH( void *param_dest, void *param_src) {

#ifdef DEBUG
  printf("LEH_material.c : in copy_parameters_LEH()\n");
#endif 

  *((LEH_param *)param_dest) = *((LEH_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_LEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt){
  int status;
  
  LEH_param *p;
  p = (LEH_param *)param;

  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  double complex (*epsilon_p)[6];
  double complex (*sigma_p)[6];

  int N;
  int leh(
	  double E,
	  double nu, 
	  int N,            /* number of points on which to apply constutive law */
	  double complex (*e)[6],   /* strain */
	  double complex (*s)[6],   /* stress */
	  double complex (*ep)[6],  /* strain at previous loading step */
	  double complex (*sp)[6],   /* stress at previous loading step */
	  double dt /* time step */
	  ) ;

  N=sv->np;
  status=0;

// mis plus haut
// p = (LEH_param *)param;

// test OK 
// printf("%d\n",p->N_modes);

#ifdef DEBUG
  printf("LEH_material.c : in behavior_LEH(), associate variables with ptr_harmonic_variable(), N= %d:\n",N);
#endif 

  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);
  sigma_p   = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);

#ifdef DEBUG
  printf("LEH_material.c : in behavior_LEH(), RUN leh()\n");
#endif 

  status = leh( p->E,
               p->nu,
               N,
               epsilon,
               sigma,
               epsilon_p,
               sigma_p,
               dt
             );


  return status;
}

/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
int solve_spc0e_LEH( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double complex (*tau)[6]
		 ){
  //

  int i,j,status;
  int N=sv->np;
  status = 0;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
    
  double lambda, lambdatriple;
  double k;
  double mu, mudouble;
  LEH_param *p = (LEH_param *)param;

  mu = (p->mu);
  mudouble = 2.*mu;
  lambda =  p->lb;
  lambdatriple = 3.*lambda;

  double lb2,mu2;
  double coef1, coef2;
  lb2 = lambda + L0->sub.ile->lb;
  mu2 = mu + L0->sub.ile->mu;
  coef1 = 1./(3.*lb2+2.*mu2);
  coef2 = 0.5/mu2;

  double complex h;
  double complex h2;

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2,lambdatriple,mudouble) 
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3.;
      h2 = coef1 * h;

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

      h2 = lambdatriple*h2;

      sigma[i][0] = h2 + mudouble*epsilon[i][0];
      sigma[i][1] = h2 + mudouble*epsilon[i][1];
      sigma[i][2] = h2 + mudouble*epsilon[i][2];
      sigma[i][3] = mudouble*epsilon[i][3];
      sigma[i][4] = mudouble*epsilon[i][4];
      sigma[i][5] = mudouble*epsilon[i][5];

    }    
			  
  return status;
}

/*######################################################################*/
int  allocate_variables_LEH( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  
  LEH_param *p;
  p = (LEH_param *)param;

// test OK
// printf("%d\n",p->N_modes);

  sv->nvar=4;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

#ifdef DEBUG
  printf("LEH_material.c : in allocate_variables_LEH(), RUN allocate_harmonic_variable(), N= %d:\n",N);
#endif 

  status = allocate_harmonic_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_LEH( 
				    void *param,
				    Euler_Angles orientation,
				    Variables *sv,
				    double t1, double t2, double t3) {
  int status;

  LEH_param *p;
  p = (LEH_param *)param;

  double complex (*epsilon)[6];
  double complex (*epsilon_p)[6];

  double complex (*sigma)[6];
  double complex (*sigma_p)[6];

  int N;
  int i,j,k;

  status=0;

// test OK
//  printf("%d\n",p->N_modes);

#ifdef DEBUG
  printf("LEH_material.c : in extrapolate_variables_LEH(), use of ptr_harmonic_variable(), N= %d:\n",N);
#endif 

  epsilon   = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);

  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  sigma_p = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);
  
#ifdef DEBUG
  printf("LEH_material.c : in extrapolate_variables_LEH(), RUN extrapol2(), N= %d:\n",N);
  printf("t1=%lf, t2=%lf, t3=%lf\n", t1, t2, t3);
#endif
  status = extrapol2_harmonic( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );
//  status = extrapol2_harmonic( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );


#ifdef DEBUG
  printf("LEH_material.c : in extrapolate_variables_LEH(), N= %d:\n",N);
  printf("epsilon[0][i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(epsilon[0][i]),cimag(epsilon[0][i]));
  }
  printf("sigma[0][i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(sigma[0][i]),cimag(sigma[0][i]));
  }
  printf("epsilon_p[0][i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(epsilon_p[0][i]),cimag(epsilon_p[0][i]));
  }
  printf("sigma_p[0][i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(sigma_p[0][i]),cimag(sigma_p[0][i]));
  }
#endif

/*
#ifdef DEBUG
  printf("LEH_material.c : in extrapolate_variables_LEH(), sigma -> sigma_p\n");
#endif 
*/

  /* sigma -> sigma_p */

/*
  N = sv->np;
#pragma omp parallel for \
  default(none)		 \
  shared(N)		 \
  private(i,j,k)		 \
  shared(sigma,sigma_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }
*/

/*
#ifdef DEBUG
  printf("LEH_material.c : in extrapolate_variables_LEH(), epsilon -> epsilon_p\n");
#endif 
*/

  /* epsilon -> epsilon_p */

/*
  N = sv->np;
#pragma omp parallel for \
  default(none)		 \
  shared(N)		 \
  private(i,j,k)		 \
  shared(epsilon,epsilon_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      epsilon_p[i][j] = epsilon[i][j];
    }
  }
*/

  return status;
}
/*######################################################################*/
int load_Cmatrix_LEH(void *param, double C[6][6])
{

  int i,j;
  LEH_param p;
  p = *((LEH_param *)param);
  double k, lb, mu;

#ifdef DEBUG
  printf("LEH_material.c : in load_Cmatrix_LEH()\n");
#endif 

  lame( &p.E, &p.nu, &lb, &mu, &k);

  //printf("%lf %lf \n",lb,mu);

  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }
  
  C[0][0] =  lb + 2.*mu;
  C[1][1] = C[0][0];
  C[2][2] = C[0][0];
  C[0][1] = lb;
  C[0][2] = C[0][1];
  C[1][2] = C[0][1];
  C[1][0] = C[0][1];
  C[2][0] = C[0][1];
  C[2][1] = C[0][1];
  C[3][3] = 2.*mu;
  C[4][4] = C[3][3];
  C[5][5] = C[3][3];
  
  
  return(0);
} 
/*######################################################################*/
