/**************************************************************************/
/* Nancy
   LEMTA
   08 spetembre 2017
   
   header of Linar Elastic harmonic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef LEH_MATERIAL
#define LEH_MATERIAL 101              /* Linear Elastic Harmonic behavior */

//#define NMODESMAX 100

#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>
#include <complex.h>

/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;         /* Young's modulus      */
  double nu;        /* Poisson coefficient  */
  double k;         /* bulk modulus  */
  double mu;        /* shear modulus  */
  double lb;        /* 1rst lame coefficient  */

} LEH_param;


/*------------------------------------------------------------------------*/
int read_parameters_LEH( FILE *f, void *p );
int print_parameters_LEH( FILE *f, void *p , int flag);

int allocate_parameters_LEH( void **p );
int deallocate_parameters_LEH( void *p );
int copy_parameters_LEH( void *param_dest, void *param_src);


int behavior_LEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int solve_spc0e_LEH( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double complex (*tau)[6]
		 );

int allocate_variables_LEH( int N, Variables *sv, void *param);

int extrapolate_variables_LEH( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       );
int load_Cmatrix_LEH(void *param, double C[6][6]);
/**************************************************************************/
#endif
/**************************************************************************/




