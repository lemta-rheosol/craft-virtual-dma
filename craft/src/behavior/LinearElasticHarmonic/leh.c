/*******************************************************************************/
/*******************************************************************************/
#include <math.h>
#include <stdio.h>
#ifdef _OPENMP
#include <omp.h>
#endif

#include <LEH_materials.h>

int leh(
	  double E,
	  double nu,
	  int N,            /* number of points on which to apply constutive law */
	  double complex (*e)[6],   /* strain */
	  double complex (*s)[6],   /* stress */
	  double complex (*ep)[6],  /* strain at previous loading step */
	  double complex (*sp)[6],   /* stress at previous loading step */
	  double dt /* time step */
	  ) {

  /*---------------------------------------------------------------------------*/  
  int status;
  int i, j, k;
  double lambda, mu;
  double complex tre;

  /*---------------------------------------------------------------------------*/  
  status=0;

  lambda = E*nu/((1 + nu)*(1 - 2*nu));
  mu = E/(1 + nu);

  /*---------------------------------------------------------------------------*/  
#pragma omp parallel for \
  default(none)		 \
  private(i,j,k) \
  shared(N)					\
  shared(E, nu, dt)			\
  shared(e,s) \
  shared(tre) \
  shared(lambda, mu)	

  for(i=0; i<N; i++) {

    /* diagonal terms and cross terms */
    for(j=0;j<6;j++) {
      s[i][j] = mu*e[i][j];
    }

    /* diagonal terms */
    tre = e[i][0] + e[i][1] + e[i][2];
    for(j=0;j<3;j++) {
      s[i][j] += lambda*tre;
    }


  }
/*---------------------------------------------------------------------------*/  


#ifdef DEBUG
  printf("leh.c : in leh(), N= %d:\n",N);
  double complex somme[6];

  printf("mean(e[:][i])=\n");
  for(i=0;i<6;i++) {
    somme[i]=0.+I*0.;
    for(j=0;j<N;j++) {
      somme[i]+=e[j][i];
    }
    somme[i]=somme[i]/(double)(N);
  }
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(somme[i]),cimag(somme[i]));
  }

  printf("mean(s[:][i])=\n");
  for(i=0;i<6;i++) {
    somme[i]=0.+I*0.;
    for(j=0;j<N;j++) {
      somme[i]+=s[j][i];
    }
    somme[i]=somme[i]/(double)(N);
  }
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(somme[i]),cimag(somme[i]));
  }

  printf("mean(ep[:][i])=\n");
  for(i=0;i<6;i++) {
    somme[i]=0.+I*0.;
    for(j=0;j<N;j++) {
      somme[i]+=ep[j][i];
    }
    somme[i]=somme[i]/(double)(N);
  }
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(somme[i]),cimag(somme[i]));
  }

  printf("mean(sp[:][i])=\n");
  for(i=0;i<6;i++) {
    somme[i]=0.+I*0.;
    for(j=0;j<N;j++) {
      somme[i]+=sp[j][i];
    }
    somme[i]=somme[i]/(double)(N);
  }
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(somme[i]),cimag(somme[i]));
  }
#endif


  return status;
}
