#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <VE_materials.h>
#include <variables.h>

#include <utils.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/*######################################################################*/
/*  Nancy
   LEMTA
   26 juin 2015
   
   
   functions to manage Visco Elastic behavior 

*/
/*######################################################################*/
int read_parameters_VE( FILE *f, void *p )
{

  VE_param *x;
  
  int status;
    
  void dec_DNLR(VE_param *data,void *ps);

  x = (VE_param *)p;

  status=craft_fscanf(f,"%lf",&(x->k_inst));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->k_rel));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu_inst));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu_rel));
  if (status!=1) return -1;
    
  status=craft_fscanf(f,"%lf",&(x->tau_k));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->tau_mu));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%d",&(x->Nbr_mode));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%d",&(x->Nbr_dec_k));
  if (status!=1) return -1;
 
  status=craft_fscanf(f,"%d",&(x->Nbr_dec_mu));
  if (status!=1) return -1;
   
  dec_DNLR(x,(void *)calloc(1,sizeof(double [x->Nbr_mode][6])));
    
//  lame( &(x->E1), &(x->nu), &(x->lb), &(x->mu) , &(x->k) );

  return 0;
}
/*######################################################################*/
int print_parameters_VE( FILE *f, void *p , int flag ) {
  int status;
  status=0;
    
  if (flag==1) {
    fprintf(f,"# k_inst  = %lf  ",(((VE_param *)p)->k_inst));
    fprintf(f,"\n");
    fprintf(f,"# k_rel  = %lf  ",(((VE_param *)p)->k_rel));
    fprintf(f,"\n");
    fprintf(f,"# tau_k = %lf  ",(((VE_param *)p)->tau_k));
    fprintf(f,"\n");
    fprintf(f,"# mu_inst = %lf  ",(((VE_param *)p)->mu_inst));
    fprintf(f,"\n");
    fprintf(f,"# mu_rel  = %lf  ",(((VE_param *)p)->mu_rel));
    fprintf(f,"\n");
    fprintf(f,"# tau_mu = %lf  ",(((VE_param *)p)->tau_mu));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_mode  = %d  ",(((VE_param *)p)->Nbr_mode));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_dec_k  = %d  ",(((VE_param *)p)->Nbr_dec_k));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_dec_mu  = %d  ",(((VE_param *)p)->Nbr_dec_mu));
      
  }
  else {
    fprintf(f,"# k_inst  = %lf  ",(((VE_param *)p)->k_inst));
    fprintf(f,"# k_rel  = %lf  ",(((VE_param *)p)->k_rel));
    fprintf(f,"# tau_k = %lf  ",(((VE_param *)p)->tau_k));
    fprintf(f,"# mu_inst = %lf  ",(((VE_param *)p)->mu_inst));
    fprintf(f,"# mu_rel  = %lf  ",(((VE_param *)p)->mu_rel));
    fprintf(f,"# tau_mu = %lf  ",(((VE_param *)p)->tau_mu));
    fprintf(f,"# Nbr_mode  = %d  ",(((VE_param *)p)->Nbr_mode));
    fprintf(f,"# Nbr_dec_k  = %d  ",(((VE_param *)p)->Nbr_dec_k));
    fprintf(f,"# Nbr_dec_mu  = %d  ",(((VE_param *)p)->Nbr_dec_mu));  }
    return status;
}

/*######################################################################*/
int allocate_parameters_VE( void **p ) {

  VE_param *x;
  int status;

  status=0;

  x=(VE_param *)malloc(sizeof(VE_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_VE( void *p ) {

  int status;

  status=0;

  free(p);
  
  return status;

}
/*######################################################################*/
int copy_parameters_VE( void *param_dest, void *param_src) {

  *((VE_param *)param_dest) = *((VE_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_VE( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt)
{
  int i,j,status;
    int N=sv->np;

  double (*epsilon)[6];
  double (*sigma)[6];
  double (*epsilon_p)[6];
  double (*sigma_p)[6];

  double (*sigma_m)[N][6];
  double (*sigma_m_p)[N][6];
    
  VE_param *p = (VE_param *)param;
    
 int ve(
	 double E1, 
	 double E2, 
	 double nu, 
	 double tau, 
	 int N,            /* number of points on which to apply constutive law */
	 double e[N][6],   /* strain */
	 double s[N][6],   /* stress */
	 double ep[N][6],  /* strain at previous loading step */
	 double sp[N][6],   /* stress at previous loading step */
	 double sm[N][6][10],   /* modal stresses */
	 double dt /* time step */
	 );
    
  void f(int N,double sigpr[N][6],double sigac[N][6] ,double sigtot[N][6] ,double epspr[N][6],double epsac[N][6],double param[6],double h);

  //N=sv->np;
  status=0;

  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  sigma_p   = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  sigma_m   = (double (*)[N][6])(ptr_variable("modal_stresses", sv, param)->values);
  sigma_m_p   = (double (*)[N][6])(ptr_variable("previous modal_stresses", sv, param)->values);

  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma[i][j]=0.;
    }
  }
  for(i=0;i<p->Nbr_mode;i++) {
    f(N,sigma_m_p[i],sigma_m[i],sigma,epsilon_p,epsilon,(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i],dt);
//    printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][0],(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][1],(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][2],(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][3],(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][4],(*((double (*)[p->Nbr_mode][6])(p->param_m)))[i][5]);
  }

  return status;
}

/*######################################################################*/
int  allocate_variables_VE( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  VE_param *p=(VE_param *)param;

//  sv->nvar=4;
  sv->nvar=6;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  tensor_t[0] = MATRIX;
  tensor_t[1] = p->Nbr_mode;
  tensor_t[2] = 6;
  status = allocate_variable(&sv->vars[4], "modal_stresses", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[5], "previous modal_stresses", N, tensor_t, 0);
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_VE( 
				    void *param,
				    Euler_Angles orientation,
				    Variables *sv,
				    double t1, double t2, double t3){
  int status,i0,i,j,Nbr;
  int N=sv->np;
    
  double (*epsilon)[6];
  double (*epsilon_p)[6];

  double (*sigma)[6];
  double (*sigma_p)[6];

  double (*sigma_m)[N][6];
  double (*sigma_m_p)[N][6];
    
  VE_param *p=(VE_param *)param;

  status=0;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);

  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  sigma_p = (double (*)[6])(ptr_variable("previous stress", sv, param)->values);

  sigma_m = (double (*)[N][6])(ptr_variable("modal_stresses", sv, param)->values);
    
  sigma_m_p = (double (*)[N][6])(ptr_variable("previous modal_stresses", sv, param)->values);
  
  extrapol2( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );

  Nbr=p->Nbr_mode;

  /* sigma -> sigma_p */
#pragma omp parallel for \
  default(none)		 \
  shared(N,Nbr)	 \
  private(i0,i,j)		 \
  shared(sigma,sigma_p,sigma_m,sigma_m_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }
  for(i0=0;i0<Nbr;i0++) {
    for(i=0;i<N;i++) {
      for(j=0;j<6;j++) {
        sigma_m_p[i0][i][j]=sigma_m[i0][i][j];
      }
    }
  }
  /*
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma[i][j]=0.01.;
    }
  }*/

  return status;
}
/*######################################################################*/
int load_Cmatrix_VE(void *param, double C[6][6])
{

  int i,j;
  VE_param p;

  double lb, mu;
    
  p = *((VE_param *)param);
    
  //lame( &p.E1, &p.nu, &lb, &mu, &k);
    
    //lb=0.5*(p.k_inst+p.k_rel-(2./3)*(p.mu_inst+p.mu_rel));
    //mu=0.5*((p.mu_inst+p.mu_rel));
    lb=p.k_inst-(2./3)*p.mu_inst;
    mu=p.mu_inst;

  
  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }
  
  C[0][0] =  lb + 2.*mu;
  C[1][1] = C[0][0];
  C[2][2] = C[0][0];
  C[0][1] = lb;
  C[0][2] = C[0][1];
  C[1][2] = C[0][1];
  C[1][0] = C[0][1];
  C[2][0] = C[0][1];
  C[2][1] = C[0][1];
  C[3][3] = 2.*mu;
  C[4][4] = C[3][3];
  C[5][5] = C[3][3];
  
  
  return(0);
} 
/*######################################################################*/

void f(int N,double sigpr[N][6],double sigac[N][6] ,double sigtot[N][6] ,double epspr[N][6],double epsac[N][6],double param[6],double h)

{
    int i0,i;
    
    double alpha, beta, gamma, tau;
    
    alpha=1/(1.+h/param[2]);
    
    beta=1/(1.+h/param[5]);
    
    gamma=param[0]*alpha+param[1]/(1.+param[2]/h);
    
    tau=param[3]*beta+param[4]/(1.+param[5]/h);
    
#pragma omp parallel for \
default(none)		 \
private(i0,i) \
shared(N)					\
shared(sigpr, sigac, epspr, epsac, sigtot, param)			\
shared(h) \
shared(alpha, beta, gamma, tau)

    
    for(i0=0;i0<N;i0++)
    {
        for(i=0;i<3;i++)
        {
            sigac[i0][i]=(1./3.)*(sigpr[i0][0]+sigpr[i0][1]+sigpr[i0][2])*alpha\
            -(param[0])*(epspr[i0][0]+epspr[i0][1]+epspr[i0][2])*alpha\
            +gamma*(epsac[i0][0]+epsac[i0][1]+epsac[i0][2])+(2./3.)*sigpr[i0][i]*beta\
            -(4./3.)*param[3]*epspr[i0][i]*beta+(4./3.)*tau*epsac[i0][i];
        }
        
        sigtot[i0][0]+=(sigac[i0][0]=sigac[i0][0]-(1./3.)*beta*(sigpr[i0][1]+sigpr[i0][2]-2*param[3]\
                                              *(epspr[i0][1]+epspr[i0][2]))-(2./3.)\
        *tau*(epsac[i0][1]+epsac[i0][2]));
        
        sigtot[i0][1]+=(sigac[i0][1]=sigac[i0][1]-(1./3.)*beta*(sigpr[i0][0]+sigpr[i0][2]-2*param[3]\
                                              *(epspr[i0][0]+epspr[i0][2]))-(2./3.)\
        *tau*(epsac[i0][0]+epsac[i0][2]));
        
        sigtot[i0][2]+=(sigac[i0][2]=sigac[i0][2]-(1./3.)*beta*(sigpr[i0][0]+sigpr[i0][1]-2*param[3]\
                                              *(epspr[i0][0]+epspr[i0][1]))-(2./3.)\
        *tau*(epsac[i0][0]+epsac[i0][1]));
        
        for( ;i<6;i++)
        {
            sigtot[i0][i]+=(sigac[i0][i]=alpha*sigpr[i0][i]-2.0*param[3]*alpha*epspr[i0][i]+2.0*tau*epsac[i0][i]);
        }
    }
    
}

void dec_DNLR(VE_param *data,void *ps)
{
    int i,N=data->Nbr_mode;
    
    double p[2][N], f_k, f_mu, s[2];
    
    double (*tab)[N][6]=(double (*)[N][6])ps;
    
    if (N>1)
    {
        f_k=pow(10.,-((double)data->Nbr_dec_k)/(N-1));
        
        f_mu=pow(10.,-((double)data->Nbr_dec_mu)/(N-1));
    }
    
    (*tab)[N-1][2]=data->tau_k;
    
    p[0][N-1]=sqrt((*tab)[N-1][2]);
    
    s[0]=p[0][N-1];
    
    (*tab)[N-1][5]=data->tau_mu;
    
    p[1][N-1]=sqrt((*tab)[N-1][5]);
    
    s[1]=p[1][N-1];
    
    for(i=N-2;i>=0;i--)
    {
        (*tab)[i][2]=(*tab)[i+1][2]*f_k;
        
        p[0][i]=sqrt((*tab)[i][2]);
        
        s[0]=s[0]+p[0][i];
        
        (*tab)[i][5]=(*tab)[i+1][5]*f_mu;
        
        p[1][i]=sqrt((*tab)[i][5]);
        
        s[1]=s[1]+p[1][i];
    }
    
    for(i=0;i<N;i++)
    {
        (*tab)[i][0]=data->k_inst*p[0][i]/s[0];
        
        (*tab)[i][1]=data->k_rel*p[0][i]/s[0];
        
        (*tab)[i][3]=data->mu_inst*p[1][i]/s[1];
        
        (*tab)[i][4]=data->mu_rel*p[1][i]/s[1];
    }
    
    data->param_m=ps;
}
/*########################################################################*/
