/**************************************************************************/
/* Nancy
   LEMTA
   26 juin 2015
   
   header of ViscoElastic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef VE_MATERIAL
#define VE_MATERIAL 200              /* Visco Elastic  behavior */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  
    double k_inst;     /* instantaneous bulk modulus */
    double k_rel;     /* relaxed bulk modulus */
    double mu_inst;    /* instantaneous shear modulus */
    double mu_rel;    /* relaxed shear modulus */
    
  /* kinetic parameters */
    
    double tau_k;    /* relaxation time bended to bulk modulus */
    double tau_mu;     /* relaxation time bended to shear modulus */
    
  /* modals parameters */
    
    int Nbr_mode;
    int Nbr_dec_k;
    int Nbr_dec_mu;
    
    /*pointeur relatif aux différents modes*/
    void *param_m;

} VE_param;


/*------------------------------------------------------------------------*/
int read_parameters_VE( FILE *f, void *p );
int print_parameters_VE( FILE *f, void *p , int flag);

int allocate_parameters_VE( void **p );
int deallocate_parameters_VE( void *p );
int copy_parameters_VE( void *param_dest, void *param_src);


int behavior_VE( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int allocate_variables_VE( int N, Variables *sv, void *param);

int extrapolate_variables_VE( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       ) ;
int load_Cmatrix_VE(void *param, double C[6][6]);
/**************************************************************************/
#endif
/**************************************************************************/




