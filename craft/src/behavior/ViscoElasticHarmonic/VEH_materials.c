#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <VEH_materials.h>
#include <variables.h>

#include <utils.h>

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/**************************************************************************/
/* Nancy
   LEMTA
   20 juillet 2017
   
   functions to manage Visco Elastic behavior 

*/
/*######################################################################*/
int read_parameters_VEH( FILE *f, void *p )
{

  VEH_param *x;
  
  int status;
    
  void dec_DNLR_harmonic(VEH_param *data,void *ps);

  x = (VEH_param *)p;

  status=craft_fscanf(f,"%lf",&(x->k_inst));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu_inst));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%d",&(x->Nbr_mode));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->k_rel));
  if (status!=1) return -1;
 
  status=craft_fscanf(f,"%lf",&(x->tau_k));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%d",&(x->Nbr_dec_k)); 
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu_rel));
  if (status!=1) return -1;
    
  status=craft_fscanf(f,"%lf",&(x->tau_mu));
  if (status!=1) return -1;
    
  status=craft_fscanf(f,"%d",&(x->Nbr_dec_mu));
  if (status!=1) return -1;
    
  dec_DNLR_harmonic(x,(void *)calloc(1,sizeof(double [x->Nbr_mode][6])));

  return 0;
}
/*######################################################################*/
int print_parameters_VEH( FILE *f, void *p , int flag ) {
  int status;
  status=0;
    
  if (flag==1) {
    fprintf(f,"# k_inst  = %lf  ",(((VEH_param *)p)->k_inst));
    fprintf(f,"\n");
    fprintf(f,"# mu_inst = %lf  ",(((VEH_param *)p)->mu_inst));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_mode  = %d  ",(((VEH_param *)p)->Nbr_mode));
    fprintf(f,"\n");
    fprintf(f,"# k_rel  = %lf  ",(((VEH_param *)p)->k_rel));
    fprintf(f,"\n");
    fprintf(f,"# tau_k = %lf  ",(((VEH_param *)p)->tau_k));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_dec_k  = %d  ",(((VEH_param *)p)->Nbr_dec_k));
    fprintf(f,"\n");
    fprintf(f,"# mu_rel  = %lf  ",(((VEH_param *)p)->mu_rel));
    fprintf(f,"\n");
    fprintf(f,"# tau_mu = %lf  ",(((VEH_param *)p)->tau_mu));
    fprintf(f,"\n");
    fprintf(f,"# Nbr_dec_mu  = %d  ",(((VEH_param *)p)->Nbr_dec_mu)); 
    fprintf(f,"\n"); 
  }
  else {
    fprintf(f,"# k_inst  = %lf  ",(((VEH_param *)p)->k_inst));
    fprintf(f,"# mu_inst = %lf  ",(((VEH_param *)p)->mu_inst));
    fprintf(f,"# Nbr_mode  = %d  ",(((VEH_param *)p)->Nbr_mode));
    fprintf(f,"# k_rel  = %lf  ",(((VEH_param *)p)->k_rel));
    fprintf(f,"# tau_k = %lf  ",(((VEH_param *)p)->tau_k));
    fprintf(f,"# Nbr_dec_k  = %d  ",(((VEH_param *)p)->Nbr_dec_k));
    fprintf(f,"# mu_rel  = %lf  ",(((VEH_param *)p)->mu_rel));
    fprintf(f,"# tau_mu = %lf  ",(((VEH_param *)p)->tau_mu));
    fprintf(f,"# Nbr_dec_mu  = %d  ",(((VEH_param *)p)->Nbr_dec_mu));  }
    return status;
  }

/*######################################################################*/
int allocate_parameters_VEH( void **p ) {

  VEH_param *x;
  int status;

  status=0;

  x=(VEH_param *)malloc(sizeof(VEH_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_VEH( void *p ) {

  int status;

  status=0;
  free(p);
  return status;
}
/*######################################################################*/
int copy_parameters_VEH( void *param_dest, void *param_src) {

  *((VEH_param *)param_dest) = *((VEH_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_VEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt)
{
  int i,j,status;
  int N=sv->np;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  double complex (*epsilon_p)[6];
  double complex (*sigma_p)[6];

  VEH_param *p = (VEH_param *)param;

  void veh(int N ,double complex s[N][6] ,double complex e[N][6],double complex lambda, double complex mu);

  //N=sv->np;
  status=0;

  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);
  sigma_p   = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);
    
  double complex lambda_omega;

  double complex k_omega, iwtau_k;
  double k_inst_i, k_rel_i, tau_k_i;

  double complex mu_omega, iwtau_mu;
  double mu_inst_i, mu_rel_i, tau_mu_i;

  double omega;
  
  omega = dt;

//  printf("omega = %f\n",omega);

// in VE material arg is => (*((double (*)[p->Nbr_mode][6])(p->param_m)))

  mu_omega = 0. + I*0.;
  k_omega = 0. + I*0.;
  for(i=0;i<p->Nbr_mode;i++) {
    // here p->param_m correspond to :
    // p->param_m[i][0] => k_inst_i
    // p->param_m[i][1] => k_rel_i
    // p->param_m[i][2] => tau_k_i
    // p->param_m[i][3] => mu_inst_i
    // p->param_m[i][4] => mu_rel_i
    // p->param_m[i][5] => tau_mu_i

    k_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][0];
    k_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][1];
    tau_k_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][2];
    mu_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][3];
    mu_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][4];
    tau_mu_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][5];

    iwtau_k = (I*1.)*(omega+I*0.)*(tau_k_i + I*0.);
    k_omega += (iwtau_k*k_inst_i + k_rel_i)/(1.+iwtau_k);
    
    iwtau_mu = (I*1.)*(omega+I*0.)*(tau_mu_i + I*0.);
    mu_omega += (iwtau_mu*mu_inst_i + mu_rel_i)/(1.+iwtau_mu);
  }

  lambda_omega =  k_omega - (2./3.)*mu_omega;

  veh(N, sigma, epsilon, lambda_omega, mu_omega);

  return status;
}

/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
int solve_spc0e_VEH( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double complex (*tau)[6]
		 ){
  //

  int i,j,status;
  int N=sv->np;
  status = 0;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];

  VEH_param *p = (VEH_param *)param;

  //void veh(int N ,double complex s[N][6] ,double complex e[N][6],double complex lambda, double complex mu);

  //N=sv->np;
  status=0;

  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
    
  double complex lambda_omega;

  double complex k_omega, iwtau_k;
  double k_inst_i, k_rel_i, tau_k_i;

  double complex mu_omega, iwtau_mu;
  double mu_inst_i, mu_rel_i, tau_mu_i;

  double omega;
  
  omega = dt;

//  printf("omega = %f\n",omega);

// in VE material arg is => (*((double (*)[p->Nbr_mode][6])(p->param_m)))

  mu_omega = 0. + I*0.;
  k_omega = 0. + I*0.;
  for(i=0;i<p->Nbr_mode;i++) {
    // here p->param_m correspond to :
    // p->param_m[i][0] => k_inst_i
    // p->param_m[i][1] => k_rel_i
    // p->param_m[i][2] => tau_k_i
    // p->param_m[i][3] => mu_inst_i
    // p->param_m[i][4] => mu_rel_i
    // p->param_m[i][5] => tau_mu_i

    k_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][0];
    k_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][1];
    tau_k_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][2];
    mu_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][3];
    mu_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][4];
    tau_mu_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][5];

    iwtau_k = (I*1.)*(omega+I*0.)*(tau_k_i + I*0.);
    k_omega += (iwtau_k*k_inst_i + k_rel_i)/(1.+iwtau_k);
    
    iwtau_mu = (I*1.)*(omega+I*0.)*(tau_mu_i + I*0.);
    mu_omega += (iwtau_mu*mu_inst_i + mu_rel_i)/(1.+iwtau_mu);
  }

  lambda_omega =  k_omega - (2./3.)*mu_omega;

  //veh(N, sigma, epsilon, lambda_omega, mu_omega);
  
  //

  // LE_param *L;
  // L = (LE_param *)param;
  
  /* isotropic case only for now ! */
  // else if( (L->isotropy == ISOTROPIC) & (L0->isotropy == ISOTROPIC) ) {
    //double lb,mu;

    double complex lb2,mu2;
    double complex k2;

    double complex h;
    double complex coef1, coef2;
    double complex h2;

    //lb = L->sub.ile->lb;
    //mu = L->sub.ile->mu;

    lb2 = lambda_omega + L0->sub.ile->lb;
    mu2 = mu_omega + L0->sub.ile->mu;

    coef1 = 1./(3.*lb2+2.*mu2);
    coef2 = 0.5/mu2;

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2,lambda_omega,mu_omega) 
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3.;
      h2 = coef1 * h;

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

      h2 = 3.*lambda_omega*h2;

      sigma[i][0] = h2 + 2.*mu_omega*epsilon[i][0];
      sigma[i][1] = h2 + 2.*mu_omega*epsilon[i][1];
      sigma[i][2] = h2 + 2.*mu_omega*epsilon[i][2];

      sigma[i][3] = 2.*mu_omega*epsilon[i][3];
      sigma[i][4] = 2.*mu_omega*epsilon[i][4];
      sigma[i][5] = 2.*mu_omega*epsilon[i][5];

    }    
      
  //}
			  
  return status;
}


/*######################################################################*/
int  allocate_variables_VEH( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  VEH_param *p=(VEH_param *)param;

  sv->nvar=4;
//  sv->nvar=6;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  /* harmonic implementation */
  status = allocate_harmonic_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_VEH( void *param,
			       Euler_Angles orientation,
			       Variables *sv,
			       double t1, double t2, double t3){

  int status,i0,i,j,Nbr;
  int N=sv->np;
    
  double complex (*epsilon)[6];
  double complex (*epsilon_p)[6];

  double complex (*sigma)[6];
  double complex (*sigma_p)[6];

//  double complex (*sigma_m)[N][6];
//  double complex (*sigma_m_p)[N][6];
    
  VEH_param *p=(VEH_param *)param;

  status=0;

  epsilon   = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);

  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  sigma_p = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);

//  sigma_m = (double complex (*)[N][6])(ptr_harmonic_variable("modal_stresses", sv, param)->values);
//  sigma_m_p = (double complex (*)[N][6])(ptr_harmonic_variable("previous modal_stresses", sv, param)->values);

//  extrapol2_harmonic( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );
  status = extrapol2_harmonic( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

//  Nbr=p->Nbr_mode;

  /* sigma -> sigma_p */
#pragma omp parallel for \
  default(none)		 \
  shared(N)	 \
  private(i0,i,j)		 \
  shared(sigma,sigma_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }

  /*
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma[i][j]=0.01.;
    }
  }*/

  return status;
}
/*######################################################################*/
int load_Cmatrix_VEH(void *param, double C[6][6])
{

  int i,j;
  VEH_param p;

  double lb, mu;
    
  p = *((VEH_param *)param);
    
//  lame( &p.E1, &p.nu, &lb, &mu, &k);    
//  lb=0.5*(p.k_inst+p.k_rel-(2./3)*(p.mu_inst+p.mu_rel));
//  mu=0.5*((p.mu_inst+p.mu_rel));

// le plus performant sans update de C
//  lb=(p.k_rel+p.k_inst)/2.-(2./3)*(p.mu_rel+p.mu_inst)/2.;
//  mu=(p.mu_rel+p.mu_inst)/2.;

//  lb=p.k_rel-(2./3)*p.mu_rel;
//  mu=p.mu_rel;

  lb=p.k_inst-(2./3)*p.mu_inst;
  mu=p.mu_inst; 

  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }
  
  C[0][0] =  lb + 2.*mu;
  C[1][1] = C[0][0];
  C[2][2] = C[0][0];
  C[0][1] = lb;
  C[0][2] = C[0][1];
  C[1][2] = C[0][1];
  C[1][0] = C[0][1];
  C[2][0] = C[0][1];
  C[2][1] = C[0][1];
  C[3][3] = 2.*mu;
  C[4][4] = C[3][3];
  C[5][5] = C[3][3]; 
  
  return(0);
} 
/*######################################################################*/
int update_Cmatrix_VEH(void *param, double C[6][6], double omega)
{

  int i,j;
//  VEH_param p;

  double lb, mu;
    
//  p = *((VEH_param *)param);

  VEH_param *p = (VEH_param *)param;

  double complex lambda_omega;

  double complex k_omega, iwtau_k;
  double k_inst_i, k_rel_i, tau_k_i;

  double complex mu_omega, iwtau_mu;
  double mu_inst_i, mu_rel_i, tau_mu_i;

  mu_omega = 0. + I*0.;
  k_omega = 0. + I*0.;
  for(i=0;i<p->Nbr_mode;i++) {

    k_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][0];
    k_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][1];
    tau_k_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][2];
    mu_inst_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][3];
    mu_rel_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][4];
    tau_mu_i = (*( (double (*)[p->Nbr_mode][6]) (p->param_m) ))[i][5];

    iwtau_k = (I*1.)*(omega+I*0.)*(tau_k_i + I*0.);
    k_omega += (iwtau_k*k_inst_i + k_rel_i)/(1.+iwtau_k);
    
    iwtau_mu = (I*1.)*(omega+I*0.)*(tau_mu_i + I*0.);
    mu_omega += (iwtau_mu*mu_inst_i + mu_rel_i)/(1.+iwtau_mu);
  }

  lambda_omega =  k_omega - (2./3.)*mu_omega;

  lb=cabs(lambda_omega);
  mu=cabs(mu_omega);
 
  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }
  
  C[0][0] =  lb + 2.*mu;
  C[1][1] = C[0][0];
  C[2][2] = C[0][0];
  C[0][1] = lb;
  C[0][2] = C[0][1];
  C[1][2] = C[0][1];
  C[1][0] = C[0][1];
  C[2][0] = C[0][1];
  C[2][1] = C[0][1];
  C[3][3] = 2.*mu;
  C[4][4] = C[3][3];
  C[5][5] = C[3][3]; 
  
  return(0);
} 
/*######################################################################*/

//void f_harmonic(int N, double smp[N][6], double sm[N][6], double s[N][6], double ep[N][6], double e[N][6], double param[6], double dt){
  void veh(int N, double complex s[N][6], double complex e[N][6], double complex lambda, double complex mu){

  /*---------------------------------------------------------------------------*/  
  int status;
  int i, j;
  double complex tre;

  /*---------------------------------------------------------------------------*/  
  status=0;

#pragma omp parallel for \
  default(none) \
  private(i,j) \
  shared(tre) \
  shared(N) \
  shared(e, s) \
  shared(lambda, mu)
  for(i=0;i<N;i++){

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    // for diagonal terms and cross terms */        
    for(j=0;j<6;j++){
//      s[i][j] += mu*e[i][j];
      s[i][j] = (2.+I*0.)*mu*e[i][j]; // attention à " += " !!!???
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* for diagonal terms only */
    tre = e[i][0] + e[i][1] + e[i][2];
    for(j=0;j<3;j++) {
      s[i][j] += lambda*tre;
    }
  }
  /*---------------------------------------------------------------------------*/  

}

/* Joel stage M2 2017 */
void dec_DNLR_harmonic(VEH_param *data,void *ps)
{

  int i,N=data->Nbr_mode;
  double p[2][N], f_k, f_mu, s[2];
  double (*tab)[N][6]=(double (*)[N][6])ps;
    
  if (N>1){
    // k
    f_k=pow(10.,-((double)data->Nbr_dec_k)/(N-1));

    // mu
    f_mu=pow(10.,-((double)data->Nbr_dec_mu)/(N-1));
  }

  // p_tau_k
  (*tab)[N-1][2]=data->tau_k; // = tau_k max 
  p[0][N-1]=sqrt((*tab)[N-1][2]); // sqrt(tau_k_0)
  s[0]=p[0][N-1]; // init sum(sqrt(tau_k_j)) =

  // p_tau_mu
  (*tab)[N-1][5]=data->tau_mu; // = tau_mu max
  p[1][N-1]=sqrt((*tab)[N-1][5]); // sqrt(tau_mu_0)
  s[1]=p[1][N-1]; // init sum(sqrt(tau_mu_j)) = sqrt(tau_mu_0)

  for(i=N-2;i>=0;i--){
    // k
    (*tab)[i][2]=(*tab)[i+1][2]*f_k; // tau_k_j
    p[0][i]=sqrt((*tab)[i][2]); // sqrt(tau_k_j)
    s[0]=s[0]+p[0][i]; // sum(sqrt(tau_k_j))

    // mu
    (*tab)[i][5]=(*tab)[i+1][5]*f_mu; // tau_mu_j
    p[1][i]=sqrt((*tab)[i][5]); // sqrt(tau_mu_j)
    s[1]=s[1]+p[1][i]; // sum(sqrt(tau_mu_j))
  }
    
  for(i=0;i<N;i++){
    //k
    (*tab)[i][0]=data->k_inst*p[0][i]/s[0]; // inst => k_inst_j (= k_inst if N=1)
    (*tab)[i][1]=data->k_rel*p[0][i]/s[0]; // rel => k_rel_j (= k_rel if N=1)

    //mu
    (*tab)[i][3]=data->mu_inst*p[1][i]/s[1]; // inst => mu_inst_j (= mu_inst if N=1)
    (*tab)[i][4]=data->mu_rel*p[1][i]/s[1]; // rel => mu_rel_j (= mu_rel if N=1)
    
    //printf(" k_inst[%d]= %f\n",i,(*tab)[i][0]);
    //printf("mu_inst[%d]= %f\n",i,(*tab)[i][3]);
    //printf("  k_rel[%d]= %f\n",i,(*tab)[i][1]);
    //printf(" mu_rel[%d]= %f\n",i,(*tab)[i][4]);
  }
  data->param_m=ps;
}
/*########################################################################*/
