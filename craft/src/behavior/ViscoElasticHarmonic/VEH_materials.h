/**************************************************************************/
/* Nancy
   LEMTA
   20 juillet 2017
   
   header of ViscoElasticHarmonic behavior

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef VEH_MATERIAL
#define VEH_MATERIAL 201              /* Harmonic Visco Elastic behavior */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  
    double k_inst;     /* instantaneous bulk modulus */
    double k_rel;     /* relaxed bulk modulus */
    double mu_inst;    /* instantaneous shear modulus */
    double mu_rel;    /* relaxed shear modulus */
    
  /* kinetic parameters */
    
    double tau_k;    /* relaxation time bended to bulk modulus */
    double tau_mu;     /* relaxation time bended to shear modulus */
    
  /* modals parameters */
    
    int Nbr_mode;
    int Nbr_dec_k;
    int Nbr_dec_mu;
    
    /*pointeur relatif aux différents modes*/
    void *param_m;

} VEH_param;


/*------------------------------------------------------------------------*/
int read_parameters_VEH( FILE *f, void *p );
int print_parameters_VEH( FILE *f, void *p , int flag);

int allocate_parameters_VEH( void **p );
int deallocate_parameters_VEH( void *p );
int copy_parameters_VEH( void *param_dest, void *param_src);


int behavior_VEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int solve_spc0e_VEH( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double complex (*tau)[6]
		 );

int allocate_variables_VEH( int N, Variables *sv, void *param);

int extrapolate_variables_VEH( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       ) ;
int load_Cmatrix_VEH(void *param, double C[6][6]);
int update_Cmatrix_VEH(void *param, double C[6][6], double omega);
/**************************************************************************/
#endif
/**************************************************************************/




