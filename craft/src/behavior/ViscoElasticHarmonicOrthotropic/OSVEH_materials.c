#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <OSVEH_materials.h>
#include <variables.h>

#include <utils.h>

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/**************************************************************************/
/* Nancy
   LEMTA
   10 octobre 2017
   
   header of ViscoElasticHarmonic orthotropic behavior 

*/
/*######################################################################*/
int read_parameters_OSVEH( FILE *f, void *p ){

  OSVEH_param *x;

  int status,i,j;
  double E1,E2,E3;
  double nu12, nu13, nu23;
  double nu21, nu31, nu32;
  double mu12, mu13, mu23;
  double k, D;    

  x = (OSVEH_param *)p;

  for (i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      x->C[i][j] = NAN;
      x->C_ini[i][j] = 0.;
    }
  }

  status=craft_fscanf(f,"%lf",&E1);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&E2);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&E3);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&nu12);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&nu13);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&nu23);
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&mu12);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mu13);
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&mu23);
  if (status!=1) return -1;
    
  status=craft_fscanf(f,"%lf",&(x->tau_mu12));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->tau_mu13));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->tau_mu23));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu12_relax));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->mu13_relax));
  if (status!=1) return -1;
  status=craft_fscanf(f,"%lf",&(x->mu23_relax));
  if (status!=1) return -1;

  nu21 = E2/E1*nu12;
  nu32 = E3/E2*nu23;
  nu31 = E3/E1*nu13;

  k = 1. - nu23*nu32 - nu13*nu31 - nu12*nu21 - 2.*nu12*nu23*nu31;
  D = E1*E2*E3/k;

  x->C_ini[0][0] = E1/k*(1-nu23*nu32);
  x->C_ini[1][1] = E2/k*(1-nu13*nu31);
  x->C_ini[2][2] = E3/k*(1-nu12*nu21);
  x->C_ini[0][1] = x->C_ini[1][0] = E1/k*(nu23*nu31+nu21);
  x->C_ini[0][2] = x->C_ini[2][0] = E1/k*(nu32*nu21+nu31);
  x->C_ini[1][2] = x->C_ini[2][1] = E2/k*(nu31*nu12+nu32);
  x->C_ini[3][3] = 2*mu23;
  x->C_ini[4][4] = 2*mu13;
  x->C_ini[5][5] = 2*mu12;

  x->E1 = E1;
  x->E2 = E2;
  x->E3 = E3;
  x->nu12 = nu12;
  x->nu13 = nu13;
  x->nu23 = nu23;
  x->mu12 = mu12;
  x->mu13 = mu13;
  x->mu23 = mu23;
  x->nu21 = nu21;
  x->nu31 = nu31;
  x->nu32 = nu32;

  return 0;
}
/*######################################################################*/
int print_parameters_OSVEH( FILE *f, void *p , int flag ) {
  int status;
  status=0;
    
  if (flag==1) {
    fprintf(f,"# E1  = %lf  ",(((OSVEH_param *)p)->E1));
    fprintf(f,"\n");
    fprintf(f,"# E2  = %lf  ",(((OSVEH_param *)p)->E2));
    fprintf(f,"\n");
    fprintf(f,"# E3  = %lf  ",(((OSVEH_param *)p)->E3));
    fprintf(f,"\n");
    fprintf(f,"# nu12  = %lf  ",(((OSVEH_param *)p)->nu12));
    fprintf(f,"\n");
    fprintf(f,"# nu13  = %lf  ",(((OSVEH_param *)p)->nu13));
    fprintf(f,"\n");
    fprintf(f,"# nu23  = %lf  ",(((OSVEH_param *)p)->nu23));
    fprintf(f,"\n");
    fprintf(f,"# mu12  = %lf  ",(((OSVEH_param *)p)->mu12));
    fprintf(f,"\n");
    fprintf(f,"# mu13  = %lf  ",(((OSVEH_param *)p)->mu13));
    fprintf(f,"\n");
    fprintf(f,"# mu23  = %lf  ",(((OSVEH_param *)p)->mu23));
    fprintf(f,"\n");
    fprintf(f,"# tau_mu12  = %lf  ",(((OSVEH_param *)p)->tau_mu12));
    fprintf(f,"\n");
    fprintf(f,"# tau_mu13  = %lf  ",(((OSVEH_param *)p)->tau_mu13));
    fprintf(f,"\n");
    fprintf(f,"# tau_mu23  = %lf  ",(((OSVEH_param *)p)->tau_mu23));
    fprintf(f,"\n");
    fprintf(f,"# mu12_relax  = %lf  ",(((OSVEH_param *)p)->mu12_relax));
    fprintf(f,"\n");
    fprintf(f,"# mu13_relax  = %lf  ",(((OSVEH_param *)p)->mu13_relax));
    fprintf(f,"\n");
    fprintf(f,"# mu23_relax  = %lf  ",(((OSVEH_param *)p)->mu23_relax));
    fprintf(f,"\n");
  }
  else {
    fprintf(f,"# E1  = %lf  ",(((OSVEH_param *)p)->E1));
    fprintf(f,"# E2  = %lf  ",(((OSVEH_param *)p)->E2));
    fprintf(f,"# E3  = %lf  ",(((OSVEH_param *)p)->E3));
    fprintf(f,"# nu12  = %lf  ",(((OSVEH_param *)p)->nu12));
    fprintf(f,"# nu13  = %lf  ",(((OSVEH_param *)p)->nu13));
    fprintf(f,"# nu23  = %lf  ",(((OSVEH_param *)p)->nu23));
    fprintf(f,"# mu12  = %lf  ",(((OSVEH_param *)p)->mu12));
    fprintf(f,"# mu13  = %lf  ",(((OSVEH_param *)p)->mu13));
    fprintf(f,"# mu23  = %lf  ",(((OSVEH_param *)p)->mu23));
    fprintf(f,"# tau_mu12  = %lf  ",(((OSVEH_param *)p)->tau_mu12));
    fprintf(f,"# tau_mu13  = %lf  ",(((OSVEH_param *)p)->tau_mu13));
    fprintf(f,"# tau_mu23  = %lf  ",(((OSVEH_param *)p)->tau_mu23));
    fprintf(f,"# mu12_relax  = %lf  ",(((OSVEH_param *)p)->mu12_relax));
    fprintf(f,"# mu13_relax  = %lf  ",(((OSVEH_param *)p)->mu13_relax));
    fprintf(f,"# mu23_relax  = %lf  ",(((OSVEH_param *)p)->mu23_relax));
  }
    return status;
  }

/*######################################################################*/
int allocate_parameters_OSVEH( void **p ) {

  OSVEH_param *x;
  int status;

  status=0;

  x=(OSVEH_param *)malloc(sizeof(OSVEH_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_OSVEH( void *p ) {

  int status;

  status=0;
  free(p);
  return status;
}
/*######################################################################*/
int copy_parameters_OSVEH( void *param_dest, void *param_src) {

  *((OSVEH_param *)param_dest) = *((OSVEH_param *)param_src);

  return 0;
}
/*######################################################################*/
int behavior_OSVEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt)
{
  int i,j,status;
  int N=sv->np;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  double complex (*epsilon_p)[6];
  double complex (*sigma_p)[6];

  OSVEH_param *p = (OSVEH_param *)param;

  int OSVEH(double complex s[6],
            double complex e[6],
            double C[6][6],
            double complex H_mu[3]);

  double complex H[3]; // H_mu

  status=0;

  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);
  sigma_p   = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);

  double omega;
  
  omega = dt;

  H[0] = ((I*1.+0)*omega*(p->tau_mu12)*(p->C_ini)[5][5] + 2.*(p->mu12_relax))/(1. + (I*1.+0)*omega*(p->tau_mu12) );
  H[1] = ((I*1.+0)*omega*(p->tau_mu13)*(p->C_ini)[4][4] + 2.*(p->mu13_relax))/(1. + (I*1.+0)*omega*(p->tau_mu13) );
  H[2] = ((I*1.+0)*omega*(p->tau_mu23)*(p->C_ini)[3][3] + 2.*(p->mu23_relax))/(1. + (I*1.+0)*omega*(p->tau_mu23) );

#pragma omp parallel for \
  default(none) \
  private(i) \
  shared(epsilon,sigma,p,H,N, status)
    for(i=0;i<N;i++){
      status = OSVEH(sigma[i], epsilon[i], (p->C_ini), H);
    }

  return status;
}
/*######################################################################*/
int OSVEH(double complex s[6],
          double complex e[6],
          double C[6][6],
          double complex H_mu[3]){

    int status;

    status = 0;

    s[0] = C[0][0] * e[0]+
           C[0][1] * e[1]+
           C[0][2] * e[2];

    s[1] = C[1][0] * e[0]+
           C[1][1] * e[1]+
	   C[1][2] * e[2];


    s[2] = C[2][0] * e[0]+
           C[2][1] * e[1]+
	   C[2][2] * e[2];

    s[3] = H_mu[2] * e[3];

    s[4] = H_mu[1] * e[4];

    s[5] = H_mu[0] * e[5];

    return status;
}
/*######################################################################*/
int  allocate_variables_OSVEH( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  OSVEH_param *p=(OSVEH_param *)param;

  sv->nvar=4;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  /* harmonic implementation */
  status = allocate_harmonic_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_OSVEH( void *param,
			       Euler_Angles orientation,
			       Variables *sv,
			       double t1, double t2, double t3){

  int status,i0,i,j,Nbr;
  int N=sv->np;
    
  double complex (*epsilon)[6];
  double complex (*epsilon_p)[6];

  double complex (*sigma)[6];
  double complex (*sigma_p)[6];
    
  OSVEH_param *p=(OSVEH_param *)param;

  status=0;

  epsilon   = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);

  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  sigma_p = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);

//  extrapol2_harmonic( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );
  status = extrapol2_harmonic( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

 /* sigma -> sigma_p */
#pragma omp parallel for \
  default(none)		 \
  shared(N)	 \
  private(i0,i,j)		 \
  shared(sigma,sigma_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }

  return status;
}
/*######################################################################*/
int load_Cmatrix_OSVEH(void *param, double C[6][6])
{

  int i,j;
  OSVEH_param p;

  double lb, mu;
    
  p = *((OSVEH_param *)param);
 
  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = p.C_ini[i][j];
    }
  }
  
  return(0);
} 
/*######################################################################*/
int update_Cmatrix_OSVEH(void *param, double C[6][6], double omega)
{

  int i,j;
  OSVEH_param p;
  p = *((OSVEH_param *)param);
  double complex H[3];
  double H_norm;

  H[0] = (I*1.+0)*omega*(p.tau_mu12)/(1. + (I*1.+0)*omega*(p.tau_mu12) );
  H[1] = (I*1.+0)*omega*(p.tau_mu13)/(1. + (I*1.+0)*omega*(p.tau_mu13) );
  H[2] = (I*1.+0)*omega*(p.tau_mu23)/(1. + (I*1.+0)*omega*(p.tau_mu23) );

  H_norm = (cabs(H[0])+cabs(H[1])+cabs(H[2]))*2.;
//  printf("H_norm = %f\n", H_norm);

  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = p.C_ini[i][j] ; //* H_norm;
    }
  }

//  C[0][0] = p.C_ini[0][0] * H_norm;
//  C[1][1] = p.C_ini[1][1] * H_norm;
//  C[2][2] = p.C_ini[2][2] * H_norm;

  C[3][3] = p.C_ini[3][3] * cabs(H[2]);
  C[4][4] = p.C_ini[4][4] * cabs(H[1]);
  C[5][5] = p.C_ini[5][5] * cabs(H[0]);

/*
  printf("C0[3][3] = %f\n", C[3][3]);
  printf("C0[4][4] = %f\n", C[4][4]);
  printf("C0[5][5] = %f\n", C[5][5]);
*/
  return(0);
} 
/*######################################################################*/
