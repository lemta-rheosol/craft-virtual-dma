/**************************************************************************/
/* Nancy
   LEMTA
   10 octobre 2017
   
   header of ViscoElasticHarmonic orthotropic

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef OSVEH_MATERIAL
#define OSVEH_MATERIAL 202              /* Harmonic Visco Elastic orthotropic behavior */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E1;     /* Young's modulus      */
  double E2;     /* Young's modulus      */
  double E3;     /* Young's modulus      */
  double nu12;    /* Poisson coefficient  */
  double nu13;    /* Poisson coefficient  */
  double nu23;    /* Poisson coefficient  */
  double mu12;    /* Poisson coefficient  */
  double mu13;    /* Poisson coefficient  */
  double mu23;    /* Poisson coefficient  */
  double nu21;    /* Poisson coefficient  */
  double nu31;    /* Poisson coefficient  */
  double nu32;    /* Poisson coefficient  */
    
  /* kinetic parameters */
  double tau_mu12;    /* relaxation time bended to shear modulus */
  double tau_mu13;
  double tau_mu23;

  /* relaxed parameters */
  double mu12_relax;    /* relaxed shear modulus */
  double mu13_relax;
  double mu23_relax;
    
  double C_ini[6][6];  /* Rigidity matrix in the cristal axis*/
  double C[6][6];      /* Rigidity matrix in the laboratory axis*/

} OSVEH_param;


/*------------------------------------------------------------------------*/
int read_parameters_OSVEH( FILE *f, void *p );
int print_parameters_OSVEH( FILE *f, void *p , int flag);

int allocate_parameters_OSVEH( void **p );
int deallocate_parameters_OSVEH( void *p );
int copy_parameters_OSVEH( void *param_dest, void *param_src);


int behavior_OSVEH( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int allocate_variables_OSVEH( int N, Variables *sv, void *param);

int extrapolate_variables_OSVEH( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       ) ;
int load_Cmatrix_OSVEH(void *param, double C[6][6]);
int update_Cmatrix_OSVEH(void *param, double C[6][6], double omega);
/**************************************************************************/
#endif
/**************************************************************************/




