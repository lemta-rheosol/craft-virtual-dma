#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <meca.h>
#include <materials.h>

#include <VEHT_materials.h>
#include <variables.h>

#include <utils.h>

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/**************************************************************************/
/* Nancy
   LEMTA
   6 juillet 2019
   Julien Boisse
   
   functions to manage Visco Elastic behavior with tabulated values

*/
/*######################################################################*/
int read_parameters_VEHT( FILE *f, void *p )
{

  VEHT_param *x;
  
  int status, Nt, it;
  double twf, ks, kl, mus, mul;
    
  printf("read_parameters_VEHT\n");

  x = (VEHT_param *)p;

  status=craft_fscanf(f,"%lf",&(x->k));
  if (status!=1) return -1;

  status=craft_fscanf(f,"%lf",&(x->mu));
  if (status!=1) return -1;

  //status=craft_fscanf(f,"%lf",&(x->Nt));
  status=craft_fscanf(f,"%d",&(Nt));
  if (status!=1) return -1;
  
  x->Nt = Nt;

  printf("k = %f\n",x->k);
  printf("mu = %f\n",x->mu);
  printf("Nt = %d\n",x->Nt);

  x->twf  = malloc(Nt*sizeof(*x->twf));

  x->k_storage = malloc(Nt*sizeof(*x->k_storage));
  x->k_loss = malloc(Nt*sizeof(*x->k_loss));

  x->mu_storage = malloc(Nt*sizeof(*x->mu_storage));
  x->mu_loss = malloc(Nt*sizeof(*x->mu_loss));

  if (x->twf ==NULL) return -1;
  if (x->k_storage==NULL) return -1;
  if (x->k_loss==NULL) return -1;
  if (x->mu_storage==NULL) return -1;
  if (x->mu_loss==NULL) return -1;

  printf("# tabulated values : twf k_storage k_loss mu_storage mu_loss\n");

  for(it=0; it<(Nt); it++){
    status=craft_fscanf(f,"%lf",&(twf));
    if (status!=1) return -1;
    x->twf[it] = twf;

    status=craft_fscanf(f,"%lf",&(ks));
    if (status!=1) return -1;
    x->k_storage[it] = ks;

    status=craft_fscanf(f,"%lf",&(kl));
    if (status!=1) return -1;
    x->k_loss[it] = kl;

    status=craft_fscanf(f,"%lf",&(mus));
    if (status!=1) return -1;
    x->mu_storage[it] = mus;

    status=craft_fscanf(f,"%lf",&(mul));
    if (status!=1) return -1;
    x->mu_loss[it] = mul;
    
    printf("# \t %lf \t %lf \t %lf \t %lf \t %lf \n", x->twf[it], x->k_storage[it], x->k_loss[it], x->mu_storage[it], x->mu_loss[it]);

  }

  return 0;
}
/*######################################################################*/
int print_parameters_VEHT( FILE *f, void *p , int flag ) {
  int status, it;
  VEHT_param *x;
  status=0;
  x = (VEHT_param *)p;  
    
  if (flag==1) {
    fprintf(f,"# k  = %lf  ",(x->k));
    fprintf(f,"\n");
    fprintf(f,"# mu = %lf  ",(x->mu));
    fprintf(f,"\n");
    fprintf(f,"# tabulated values : twf k_storage k_loss mu_storage mu_loss  ");
    for(it=0; it<(x->Nt); it++){
      fprintf(f,"# \t %lf \t %lf \t %lf \t %lf \t %lf", x->twf[it], x->k_storage[it], x->k_loss[it], x->mu_storage[it], x->mu_loss[it]);
      fprintf(f,"\n");
    }
  }
  else {
    fprintf(f,"# k  = %lf  ",(x->k));
    fprintf(f,"# mu = %lf  ",(x->mu));
    fprintf(f,"# tabulated values : twf k_storage k_loss mu_storage mu_loss  ");
    for(it=0; it<(x->Nt); it++){
      fprintf(f,"# \t %lf \t %lf \t %lf \t %lf \t %lf", x->twf[it], x->k_storage[it], x->k_loss[it], x->mu_storage[it], x->mu_loss[it]);
    }
  }
    return status;
  }

/*######################################################################*/
int allocate_parameters_VEHT( void **p ) {

  VEHT_param *x;
  int status;

  status=0;

  x=(VEHT_param *)malloc(sizeof(VEHT_param));
  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_VEHT( void *p ) {

  int status;

  status=0;
  free(p);
  return status;
}

/*######################################################################*/
int copy_parameters_VEHT( void *param_dest, void *param_src) {

  *((VEHT_param *)param_dest) = *((VEHT_param *)param_src);

  return 0;
}

/*######################################################################*/
int behavior_VEHT( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt)
{
  int status;
  int N=sv->np;
  status=0;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);

  int it;
  double coef, ks, kl, mus, mul;
  double complex k, mu, mu2, lb;
  VEHT_param *p = (VEHT_param *)param;

  /* Apply tabulated law to get sigma */
  /* Could be stored in order not to recompute it at each iteration.*/
  it = -1;
  do {
    it ++;
  } while((it<((p->Nt)-2)) && (dt>(p->twf[it+1])));
  coef = ( dt - p->twf[it] ) / ( p->twf[it+1] - p->twf[it] );
  ks = p->k_storage[it] + coef * ( p->k_storage[it+1] - p->k_storage[it] );
  kl = p->k_loss[it] + coef * ( p->k_loss[it+1] - p->k_loss[it] );
  mus = p->mu_storage[it] + coef * ( p->mu_storage[it+1] - p->mu_storage[it] );
  mul = p->mu_loss[it] + coef * ( p->mu_loss[it+1] - p->mu_loss[it] );

  k = ks + I*kl;
  mu = mus + I*mul;
  mu2 = (2.+I*0.)*mu;
  lb = (k - 2.*mu/3);

  int i, j;
  double complex tre;

#pragma omp parallel for \
  default(none) \
  private(i,j) \
  private(tre) \
  shared(N) \
  shared(epsilon, sigma) \
  shared(mu2, lb)
  for(i=0;i<N;i++){
    // for diagonal terms and cross terms */        
    for(j=0;j<6;j++){
      sigma[i][j] = mu2*epsilon[i][j];
    }
    /* for diagonal terms only */
    tre = epsilon[i][0] + epsilon[i][1] + epsilon[i][2];
    for(j=0;j<3;j++) {
      sigma[i][j] += lb*tre;
    }
  }  
 
  return status;
}

/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
int solve_spc0e_VEHT( void *param, Euler_Angles orientation, Variables *sv, double dt,
		      LE_param *L0, 
		      double complex (*tau)[6]
		      ){
  //

  int i,j,status;
  int N=sv->np;
  status = 0;

  /* for harmonic implementation */
  double complex (*epsilon)[6];
  double complex (*sigma)[6];
  epsilon = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);

  int it;
  double coef, ks, kl, mus, mul;
  VEHT_param *p = (VEHT_param *)param;

  /* Apply tabulated law to get sigma */
  /* Could be stored in order not to recompute it at each iteration.*/
  it = -1;
  do {
    it ++;
  } while((it<((p->Nt)-2)) && (dt>(p->twf[it+1])));
  coef = ( dt - p->twf[it] ) / ( p->twf[it+1] - p->twf[it] );
  ks = p->k_storage[it] + coef * ( p->k_storage[it+1] - p->k_storage[it] );
  kl = p->k_loss[it] + coef * ( p->k_loss[it+1] - p->k_loss[it] );
  mus = p->mu_storage[it] + coef * ( p->mu_storage[it+1] - p->mu_storage[it] );
  mul = p->mu_loss[it] + coef * ( p->mu_loss[it+1] - p->mu_loss[it] );

  double complex k, mu, mudouble, lb, lbtriple;
  k = ks + I*kl;
  mu = mus + I*mul;
  mudouble = (2.+I*0.)*mu;
  lb = (k - 2.*mu/3);
  lbtriple = 3.*lb;

  double complex lb2,mu2;
  double complex coef1, coef2;
  lb2 = lb + L0->sub.ile->lb;
  mu2 = mu + L0->sub.ile->mu;
  coef1 = 1./(3.*lb2+2.*mu2);
  coef2 = 0.5/mu2;

  double complex h;
  double complex h2;

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2,lbtriple,mudouble) 
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3.;
      h2 = coef1 * h;

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

      h2 = lbtriple*h2;

      sigma[i][0] = h2 + mudouble*epsilon[i][0];
      sigma[i][1] = h2 + mudouble*epsilon[i][1];
      sigma[i][2] = h2 + mudouble*epsilon[i][2];
      sigma[i][3] = mudouble*epsilon[i][3];
      sigma[i][4] = mudouble*epsilon[i][4];
      sigma[i][5] = mudouble*epsilon[i][5];

    } 
		  
  return status;
}

/*######################################################################*/
int  allocate_variables_VEHT( int N, Variables *sv, void *param) {

  int status;
  int tensor_t[3]={TENSOR2,0,0};
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  VEHT_param *p=(VEHT_param *)param;

  sv->nvar=4;
  sv->np = N;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;

  /* harmonic implementation */
  status = allocate_harmonic_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_harmonic_variable(&sv->vars[3], "previous stress", N, tensor_t, 0);
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;
}

/*######################################################################*/
int extrapolate_variables_VEHT( void *param,
			        Euler_Angles orientation,
			        Variables *sv,
			        double t1, double t2, double t3){

  int status,i0,i,j,Nbr;
  int N=sv->np;
    
  double complex (*epsilon)[6];
  double complex (*epsilon_p)[6];

  double complex (*sigma)[6];
  double complex (*sigma_p)[6];
    
  VEHT_param *p=(VEHT_param *)param;

  status=0;

  epsilon   = (double complex (*)[6])(ptr_harmonic_variable("strain", sv, param)->values);
  epsilon_p = (double complex (*)[6])(ptr_harmonic_variable("previous strain", sv, param)->values);

  sigma   = (double complex (*)[6])(ptr_harmonic_variable("stress", sv, param)->values);
  sigma_p = (double complex (*)[6])(ptr_harmonic_variable("previous stress", sv, param)->values);

//  extrapol2_harmonic( 6*sv->np, t1, &epsilon_p[0][0], t2, &epsilon[0][0], t3 );
  status = extrapol2_harmonic( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

  /* sigma -> sigma_p */
#pragma omp parallel for \
  default(none)		 \
  shared(N)	 \
  private(i0,i,j)		 \
  shared(sigma,sigma_p)
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma_p[i][j] = sigma[i][j];
    }
  }

  /*
  for(i=0;i<N;i++) {
    for(j=0;j<6;j++) {
      sigma[i][j]=0.01.;
    }
  }*/

  return status;
}

/*######################################################################*/
int load_Cmatrix_VEHT(void *param, double C[6][6])
{

  int i,j;
  VEHT_param p;

  double lb, mu;
    
  p = *((VEHT_param *)param);

  lb=p.k-(2./3)*p.mu;
  mu=p.mu;

  for (i=0;i<6;i++){
    for (j=0;j<6;j++){
      C[i][j] = 0.;
    }
  }
  
  C[0][0] =  lb + 2.*mu;
  C[1][1] = C[0][0];
  C[2][2] = C[0][0];
  C[0][1] = lb;
  C[0][2] = C[0][1];
  C[1][2] = C[0][1];
  C[1][0] = C[0][1];
  C[2][0] = C[0][1];
  C[2][1] = C[0][1];
  C[3][3] = 2.*mu;
  C[4][4] = C[3][3];
  C[5][5] = C[3][3]; 
  
  return(0);
} 
