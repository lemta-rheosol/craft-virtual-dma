/**************************************************************************/

/* Nancy
   LEMTA
   6 juillet 2019
   Julien Boisse
   
   functions to manage Harmonic Visco Elastic behavior with tabulated values

*/

/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/

#ifndef VEHT_MATERIAL
#define VEHT_MATERIAL 205              /* Harmonic Visco Elastic behavior with tabulated values */



#define _ISOC99_SOURCE

#include <materials.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  
    double k;     /* reference bulk modulus */
    double mu;    /* reference shear modulus */
    
  /* tab parameters */
    
    int Nt;    /* number of tabulated values */

    double *twf;     /* tabulated temperaure, purlsation or frequency */
    double *k_storage;     /* tabulated bulk storage modulus */
    double *k_loss;     /* tabulated bulk loss modulus */
    double *mu_storage;     /* tabulated shear storage modulus */
    double *mu_loss;     /* tabulated shear loss modulus */
    
} VEHT_param;


/*------------------------------------------------------------------------*/
int read_parameters_VEHT( FILE *f, void *p );
int print_parameters_VEHT( FILE *f, void *p , int flag);

int allocate_parameters_VEHT( void **p );
int deallocate_parameters_VEHT( void *p );
int copy_parameters_VEHT( void *param_dest, void *param_src);


int behavior_VEHT( void *param, Euler_Angles orientation, 
		  Variables *sv,
		  double dt );

int solve_spc0e_VEHT( void *param, Euler_Angles orientation, Variables *sv, double dt,
		      LE_param *L0, 
		      double complex (*tau)[6]
		      );

int allocate_variables_VEHT( int N, Variables *sv, void *param);

int extrapolate_variables_VEHT( void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
			       );

int load_Cmatrix_VEHT(void *param, double C[6][6]);

//int update_Cmatrix_VEHT(void *param, double C[6][6], double omega);

/**************************************************************************/
#endif
/**************************************************************************/


