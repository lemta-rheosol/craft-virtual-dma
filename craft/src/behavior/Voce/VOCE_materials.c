#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>

#include <VOCE_materials.h>
#include <variables.h>

#include <LE_Cmatrix.h>
#include <Crystal_Tools.h>

#include <utils.h>

#ifdef _OPENMP
#include <omp.h>
#endif

int syst_cubic_voce(
		    FILE *f,int *nsys,
		    double (**ngl)[3],double (**mgl)[3],double (**sgl)[6],
		    double **tau_ini,double **tau_sta,
		    double **theta_ini,double **theta_sta,
		    double **gammap0, double **npui,
		    double ***mat_H
		    );

/*######################################################################*/
/* A. Labe
   LMA/CNRS
   october 8th 2009
   
   functions to manage Crystallo Elasto-Visco-Plastic behavior 
   without hardening

   HM 27th january 2010 :
   model initially proposed by Olivier Castelnau (in EVPC_H_materials), 
   modified by Pierre Suquet

*/
/*######################################################################*/
int read_parameters_VOCE( FILE *f, void *p ) {
  int status;
  //  int isotropie;

  VOCE_param *x;

  x = (VOCE_param *)p;
  //  printf("ici\n");

  /*
  printf("\n enter the kind of isotropy :\n\t0 -> Cmatrix \n\t1->Isotropy ");
  printf("\n\t2->cubic symetry ");
  printf("\n\t3 -> hexagonal symetry \n\t4->Orthotropic symetry");
  */

   
  //  status=craft_fscanf(f,"%d",&isotropie);
  //  if (status!=1) return -1;
  
  /* reads elastic components */
  //  status = read_Cmatrix(f,x->C,x->C_ini,isotropie);
  //  if (status!=0) return -1;
 
  void *pel;
  int i,j;
  allocate_parameters_LE(&pel);
  status = read_parameters_LE(f, pel);
  if (status!=0) return -1;

  status=load_Cmatrix_LE( pel, x->C_ini);
  if (status!=0) return -1;

  for(i=0; i<6; i++) {
    for(j=0; j<6; j++) {
      x->C[i][j]  =  NAN;
    }
  }
  deallocate_parameters_LE(pel);

  
  /* reads viscous components */
  status=syst_cubic_voce(
			 f,
			 &(x->nsyst),
			 &(x->ngl),&(x->mgl),&(x->sgl),
			 &(x->tau_ini),&(x->tau_sta),
			 &(x->theta_ini),&(x->theta_sta),
			 &(x->gammap0),&(x->npui),
			 &(x->mat_H)
			 );  

  if (status!=0) return -1;

  return 0;
}
/*######################################################################*/
int print_parameters_VOCE( FILE *f, void *p , int flag ) {
  int status;
  int i,j;
  status=0;

    fprintf(f,"# C_matrix:\n");
    for(i=0;i<6;i++) {
      fprintf(f,"# ");
      for(j=0;j<6;j++){
        fprintf(f,"%lf ",(((VOCE_param *)p)->C_ini[i][j]));
      }
     fprintf(f,"\n"); 
    }
    fprintf(f,"#\n");
  
    fprintf(f,"# number of sliding systems  = %d \n",(((VOCE_param *)p)->nsyst));
    for(i=0;i<((VOCE_param *)p)->nsyst;i++){
      fprintf(f,"# sys %d: Schmid tensor: n= %lf %lf %lf m=%lf %lf %lf\n",i+1,
	      (((VOCE_param *)p)->ngl[i][0]),
	      (((VOCE_param *)p)->ngl[i][1]),
	      (((VOCE_param *)p)->ngl[i][2]),
	      (((VOCE_param *)p)->mgl[i][0]),
	      (((VOCE_param *)p)->mgl[i][1]),
	      (((VOCE_param *)p)->mgl[i][2]));
      
      fprintf(f,"#         tau_ini=%lf, tau_sta=%lf, gammap0=%lf, n=%lf, theta_ini=%lf, theta_sta=%lf, \n",
	      (((VOCE_param *)p)->tau_ini[i]),
	      (((VOCE_param *)p)->tau_sta[i]),
	      (((VOCE_param *)p)->gammap0[i]),
	      (((VOCE_param *)p)->npui[i]),
	      (((VOCE_param *)p)->theta_ini[i]),
	      (((VOCE_param *)p)->theta_sta[i])
	      );

      fprintf(f,"#         H =");
      for(j=0;j<((VOCE_param *)p)->nsyst;j++) fprintf(f,"%lf ",(((VOCE_param *)p)->mat_H[i][j])); 
      fprintf(f,"\n");
      fprintf(f,"#\n");
  }
  return status;
}

/*######################################################################*/
int allocate_parameters_VOCE( void **p ) {

  VOCE_param *x;
  int status;



  status=0;

  x=(VOCE_param *)malloc(sizeof(VOCE_param));

  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  
  return status;

}

/*######################################################################*/
int deallocate_parameters_VOCE( void *param ) {

  int status;
  VOCE_param p;
  
  p = *((VOCE_param *)param);
  
  status=0;
  
  free(p.ngl);
  free(p.mgl);
  free(p.sgl);
    
  free(p.tau_ini);
  free(p.tau_sta);
  free(p.gammap0);
  free(p.npui); 
  free(&p.mat_H[0][0]);   
  free(p.mat_H);
  free(p.theta_ini);
  free(p.theta_sta);
  
  free(param);
  
  return status;

}
/*######################################################################*/
int copy_parameters_VOCE( void *param_dest, void *param_src) {
  
  int i,j;
  double *tmp;
  VOCE_param *p,*q;
  
  p = (VOCE_param *)param_dest;
  q = (VOCE_param *)param_src;
  
  *p = *q;
  
  p->ngl = (double (*)[3])malloc(p->nsyst * sizeof(*(p->ngl)));
  p->mgl = (double (*)[3])malloc(p->nsyst * sizeof(*(p->mgl)));
  p->sgl = (double (*)[6])malloc(p->nsyst * sizeof(*(p->sgl)));
  
  p->tau_ini  = (double *)malloc(p->nsyst * sizeof(*(p->tau_ini)));
  p->tau_sta  = (double *)malloc(p->nsyst * sizeof(*(p->tau_sta)));  

  p->theta_ini  = (double *)malloc(p->nsyst * sizeof(*(p->theta_ini)));
  p->theta_sta  = (double *)malloc(p->nsyst * sizeof(*(p->theta_sta)));  

  p->gammap0  = (double *)malloc(p->nsyst * sizeof(*(p->gammap0)));
  p->npui     = (double *)malloc(p->nsyst * sizeof(*(p->npui)));

  
  p->mat_H    = (double **)malloc(p->nsyst*sizeof(*(p->mat_H)));
  tmp    = (double *)malloc(p->nsyst*p->nsyst*sizeof(*tmp));
  
  for(i=0;i<p->nsyst;i++)
  {
    p->mat_H[i]   = tmp + i*(p->nsyst);
  }
    
  for(i=0;i<p->nsyst;i++)
  {
    for(j=0;j<3;j++)
    {
      p->ngl[i][j] = q->ngl[i][j];
      p->mgl[i][j] = q->mgl[i][j];
    }
    for(j=0;j<6;j++)
    {
      p->sgl[i][j]   = q->sgl[i][j];
    }
    for(j=0;j<p->nsyst;j++)
    {
      p->mat_H[i][j] = q->mat_H[i][j];
    } 
    p->tau_ini[i]  = q->tau_ini[i];
    p->tau_sta[i]  = q->tau_sta[i];

    p->theta_ini[i]  = q->theta_ini[i];
    p->theta_sta[i]  = q->theta_sta[i];

    p->gammap0[i]  = q->gammap0[i];
    p->npui[i]     = q->npui[i];

  }

  return 0;
}
/*######################################################################*/
int  allocate_variables_VOCE( int N, Variables *sv, void *param) {
    
  VOCE_param *p;

  
  int status;

  int type[3]={UNKNOWN,0,0};
  int ivar;




  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status=0;
  sv->np = N;
  sv->nvar = 8; 
  sv->vars = malloc( sv->nvar * sizeof(*(sv->vars)));
  if ( sv->vars == NULL ) return -1;
  p = (VOCE_param *)param;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ivar = 0;

  /* strain tensor */
  type[0]=TENSOR2; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "strain", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /* stress tensor */
  type[0]=TENSOR2; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "stress", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /* strain at the previous time step */
  type[0]=TENSOR2; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "previous strain", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /* stress at the previous time step */
  type[0]=TENSOR2; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "previous stress", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /* threshold stress */
  type[0]=VECTOR; type[1]=p->nsyst; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "threshold stress", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /* threshold stress at the previous time step*/
  type[0]=VECTOR; type[1]=p->nsyst; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "previous threshold stress", N, type, 0);
  ivar++;
  if (status<0) return -1;

  
  /* Gamma */
  type[0]=SCALAR; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "Gamma", N, type, 0);
  ivar++;
  if (status<0) return -1;


  /* previous Gamma */
  type[0]=SCALAR; type[1]=0; type[2]=0;
  status = allocate_variable(&sv->vars[ivar], "previous Gamma", N, type, 0);
  ivar++;
  if (status<0) return -1;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  return status;

}
/*######################################################################*/
int  init_variables_VOCE( Variables *sv, void *param) {
    
  VOCE_param *p;
  
  int i,j;

  double *sigma, *sigma_p;
  double *tau0, *tau0_p;
  double *gamma, *gamma_p;

  int ii;

  sigma   = (double *)(ptr_variable("stress", sv, param)->values);
  sigma_p   = (double *)(ptr_variable("previous stress", sv, param)->values);

  tau0   = (double *)(ptr_variable("threshold stress", sv, param)->values);
  tau0_p = (double *)(ptr_variable("previous threshold stress", sv, param)->values);
  
  gamma = (double *)(ptr_variable("Gamma", sv, param)->values);
  gamma_p = (double *)(ptr_variable("previous Gamma", sv, param)->values);

  if( 
     (sigma==(double *)0) || (sigma_p==(double *)0) || 
     (tau0==(double *)0) || (tau0_p==(double *)0) || 
     (gamma==(double *)0) || (gamma_p==(double *)0)
      ) {
    fprintf(stderr,"error: ptr_variable return NULL pointer.\n");
    return -1;
  }

  p  = ((VOCE_param *)param);

  for(i=0;i<sv->np;i++) {
    for(j=0;j<6;j++) {
      ii = i*6+j;
      sigma[ii] = 0.;
      sigma_p[ii] = 0.;
    }
  }
  
  for(i=0;i<sv->np;i++){
    for(j=0;j<p->nsyst;j++){
      ii = i*p->nsyst+j;
      tau0[ii]   = (p->tau_ini)[j];
      tau0_p[ii] = (p->tau_ini)[j];

    }

  }    


  for(i=0;i<sv->np;i++) {
    gamma[i]= 0.;
    gamma_p[i]= 0.;
  }
  
  
  return 0;
}

/*######################################################################*/
int extrapolate_variables_VOCE( 
			       void *param, Euler_Angles orientation, 
			       Variables *sv,
			       double t1, double t2, double t3
				) {
  int status;
  double (*epsilon) [6], (*epsilon_p) [6];
  Variable *sigma , *sigma_p;
  Variable *tau0, *tau0_p;
  Variable *gamma, *gamma_p;

  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  if ( (epsilon==(double (*)[6])0) || (epsilon_p==(double (*)[6])0) ) {
    fprintf(stderr,"error: ptr_variable return NULL pointer.\n");
    status=-1;
    return status;
  }
								     
  extrapol2( 6*sv->np, t1, *epsilon_p, t2, *epsilon, t3 );

  sigma   = ptr_variable("stress", sv, param);
  sigma_p = ptr_variable("previous stress", sv, param);


  tau0   = ptr_variable("threshold stress", sv, param);
  tau0_p = ptr_variable("previous threshold stress", sv, param);

  gamma = ptr_variable("Gamma", sv, param);
  gamma_p = ptr_variable("previous Gamma", sv, param);

  if( 
     (sigma==(Variable *)0) || (sigma_p==(Variable *)0) || 
     (tau0==(Variable *)0) || (tau0_p==(Variable *)0) || 
     (gamma==(Variable *)0) || (gamma_p==(Variable *)0)
      ) {
    fprintf(stderr,"error: ptr_variable return NULL pointer.\n");
    status=-1;
    return status;
  }

  status = copy_variable_values( sigma_p, sigma, sv->np);
  status = copy_variable_values( tau0_p, tau0, sv->np);
  status = copy_variable_values( gamma_p, gamma, sv->np);

  
  return status;
}

/*######################################################################*/
int load_Cmatrix_VOCE(void *param, double C[6][6])
{

int i,j;
VOCE_param p;

p = *((VOCE_param *)param);

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = p.C_ini[i][j];
 }
}
 
return(0);
} 


/*######################################################################*/
void ldcpt_cristal_voce_(
			    double *dt, 
			    double *e11, double *e22, double *e33, double *e12, double *e13, double *e23,
			    double *s11, double *s22, double *s33, double *s12, double *s13, double *s23,
			    double *ep11, double *ep22, double *ep33, double *ep12, double *ep13, double *ep23,
			    double *sp11, double *sp22, double *sp33, double *sp12, double *sp13, double *sp23,
			    double *lC,
			    int *nsys,double *sgli,
			    double *GGamma, double *GGamap,
			    double *tau0,double *tau0_p,
			    double *gammap0,double *npui,
			    double *tau_ini,double *tau_sta,
			    double *theta_ini,double *theta_sta,
			    double *mat_H);

/*********************************************************************************/
int behavior_VOCE( 
		   void *param, Euler_Angles orientation, 
		   Variables *sv,double dt ){

  int status;
  int i, ii;


  
  VOCE_param *p;
  int N;

  status = 0;
  p  = ((VOCE_param *)param);

  double *epsilon, *epsilon_p;
  double *sigma, *sigma_p;
  double *tau0, *tau0_p;
  double *gamma, *gamma_p;

  epsilon = (double *)(ptr_variable("strain", sv, param)->values);
  epsilon_p = (double *)(ptr_variable("previous strain", sv, param)->values);

  sigma   = (double *)(ptr_variable("stress", sv, param)->values);
  sigma_p   = (double *)(ptr_variable("previous stress", sv, param)->values);

  tau0   = (double *)(ptr_variable("threshold stress", sv, param)->values);
  tau0_p = (double *)(ptr_variable("previous threshold stress", sv, param)->values);

  gamma = (double *)(ptr_variable("Gamma", sv, param)->values);
  gamma_p = (double *)(ptr_variable("previous Gamma", sv, param)->values);

  if( 
     (epsilon==(double *)0) || (epsilon_p==(double *)0) || 
     (sigma==(double *)0) || (sigma_p==(double *)0) || 
     (tau0==(double *)0) || (tau0_p==(double *)0) || 
     (gamma==(double *)0) || (gamma_p==(double *)0)
      ) {
    fprintf(stderr,"error: ptr_variable return NULL pointer.\n");
    status=-1;
    return status;
  }

  /* if the matrix C had never been initialized */
  //  printf("C[0][0]=%lf\n",p->C[0][0]);
  if (isnan(p->C[0][0])){
    //    printf("rotate\n");
    status = rotate_Cmatrix(p->C,p->C_ini,orientation);     
  }
  
  /* if the Schmid's tensor had never been innitialized */
  //  if (p->sgl[0][0] == HUGE_VAL){
  if (isnan(p->sgl[0][0])) {
  
    status = rotate_sgl(p->sgl,p->ngl,p->mgl,p->nsyst,orientation);     
  }
  
  N=sv->np;
  /* application of the behavior law   */
#pragma omp parallel for			\
  default(none)					\
  private(i,ii)					\
  shared(dt,N)					\
  shared(epsilon, epsilon_p, sigma, sigma_p, tau0, tau0_p, gamma, gamma_p, p )

  for(i=0; i<N; i++) {
    ii = 6*i;
    ldcpt_cristal_voce_(
			&dt,
			

			&epsilon[ii+0], &epsilon[ii+1], &epsilon[ii+2], &epsilon[ii+5], &epsilon[ii+4], &epsilon[ii+3], 
			//			&epsilon[i][0], &epsilon[i][1], &epsilon[i][2], &epsilon[i][5], &epsilon[i][4], &epsilon[i][3], 
			&sigma[ii+0], &sigma[ii+1], &sigma[ii+2], &sigma[ii+5], &sigma[ii+4], &sigma[ii+3], 			
			//			&sigma[i][0], &sigma[i][1], &sigma[i][2], &sigma[i][5], &sigma[i][4], &sigma[i][3], 
			&epsilon_p[ii+0], &epsilon_p[ii+1], &epsilon_p[ii+2], &epsilon_p[ii+5], &epsilon_p[ii+4], &epsilon_p[ii+3], 
			&sigma_p[ii+0], &sigma_p[ii+1], &sigma_p[ii+2], &sigma_p[ii+5], &sigma_p[ii+4], &sigma_p[ii+3], 			
			//			&sv->epsilon_p[i][0], &sv->epsilon_p[i][1], &sv->epsilon_p[i][2], &sv->epsilon_p[i][5], &sv->epsilon_p[i][4], &sv->epsilon_p[i][3], 
			//			&sv->sigma_p[i][0], &sv->sigma_p[i][1], &sv->sigma_p[i][2], &sv->sigma_p[i][5], &sv->sigma_p[i][4], &sv->sigma_p[i][3], 
			&p->C[0][0],
			&p->nsyst,
			&p->sgl[0][0],

			&gamma[i], &gamma_p[i],
			&tau0[i*p->nsyst], &tau0_p[i*p->nsyst], 
			//			&sv->Gamma[i], &sv->Gamma_p[i],
			//			&sv->tau0[i][0], &sv->tau0_p[i][0],

			&p->gammap0[0], &p->npui[0], 
			&p->tau_ini[0], &p->tau_sta[0],
			&p->theta_ini[0], &p->theta_sta[0],
			&p->mat_H[0][0]
			);
  }



  return status;
}

/*######################################################################*/
/* enters the material parameter concerning crystallographic stuff */
int syst_cubic_voce(
		    FILE *f,int *nsys,
		    double (**ngl)[3],double (**mgl)[3],double (**sgl)[6],
		    double **tau_ini,double **tau_sta,
		    double **theta_ini,double **theta_sta,
		    double **gammap0, double **npui,
		    double ***mat_H
		    ){

  
  int status;
  int i,j;

  double *tmp;
  

  /*-----------------------------------------------------------------------*/  
  status = read_crystal_parameters(f,nsys,ngl,mgl,sgl);
  if(status!=0) return status;
  /*-----------------------------------------------------------------------*/  
  /* material parameters */

  *tau_ini = (double *)malloc((*nsys)*sizeof(**tau_ini));
  *tau_sta = (double *)malloc((*nsys)*sizeof(**tau_sta));

  *theta_ini = (double *)malloc((*nsys)*sizeof(**theta_ini));
  *theta_sta = (double *)malloc((*nsys)*sizeof(**theta_sta));

  *gammap0 = (double *)malloc((*nsys)*sizeof(**gammap0));
  *npui    = (double *)malloc((*nsys)*sizeof(**npui));
  
  *mat_H   = (double **)malloc((*nsys)*sizeof(double *));
  tmp = (double *)malloc((*nsys)*(*nsys)*sizeof(*tmp));
  
  for(i=0;i<*nsys;i++) {
    (*mat_H)[i]   =  tmp + i*(*nsys); 
  }
  
  
  
  for(i=0;i<*nsys;i++) {
    status=craft_fscanf(f,"%lf %lf",&(*tau_ini)[i],&(*tau_sta)[i]);
    if (status!=2) return -1;
    status=craft_fscanf(f,"%lf %lf",&(*theta_ini)[i],&(*theta_sta)[i]);
    if (status!=2) return -1;
    status=craft_fscanf(f,"%lf",&(*gammap0)[i]);
    if (status!=1) return -1;
    status=craft_fscanf(f,"%lf",&(*npui)[i]);
    if (status!=1) return -1;
    
  }

  for(i=0;i<*nsys;i++) {
    for(j=0;j<*nsys;j++){
      status=craft_fscanf(f,"%lf",&(*mat_H)[i][j]);
      if (status!=1) return -1;
    }
  }
  
  /*-----------------------------------------------------------------------*/  
 

  return 0;

}
