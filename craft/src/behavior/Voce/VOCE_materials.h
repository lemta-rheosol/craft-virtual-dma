/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   14/06/2011

   
   header file for Voce law for crystalline materials


*/
/**************************************************************************/
/* environment variable defining the type of behavior
   the value that is given to it MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef VOCE_MATERIAL
#define VOCE_MATERIAL 50    /* Voce law */



#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>


/**************************************************************************/
typedef struct {

  /* elasticity parameters */
  double E;     /* Young's modulus      */
  double nu;    /* Poisson coefficient  */
  double lb;    /* 1st Lam� coefficient */
  double mu;    /* 2d Lam� coefficient  */
  double k;     /* bulk modulus  */
  
  double C_ini[6][6];  /* Elastic Rigidity matrix in the cristal axis*/
  double C[6][6];      /* Elastic Rigidity matrix in the laboratory axis*/
  
  /* viscosity parameters */
  int nsyst;         /* number of sliding systems  */
  double (*ngl)[3];  /* Miller's indices           */
  double (*mgl)[3];      
  double (*sgl)[6];  /* Schmid's tensor           */
  

  double *tau_ini;  
  double *tau_sta; 

  double *theta_ini;
  double *theta_sta;
  
  double *gammap0;
  double *npui;

  double **mat_H;


} VOCE_param;


/*------------------------------------------------------------------------*/
int read_parameters_VOCE( FILE *f, void *p );
int print_parameters_VOCE( FILE *f, void *p , int flag);

int allocate_parameters_VOCE( void **p );
int deallocate_parameters_VOCE( void *p );
int copy_parameters_VOCE( void *param_dest, void *param_src);


int behavior_VOCE( void *param, Euler_Angles orientation,  Variables *sv, double dt );

int allocate_variables_VOCE( int N, Variables *sv, void *param);

int extrapolate_variables_VOCE( 
				    void *param, Euler_Angles orientation, 
				    Variables *sv,
				    double t1, double t2, double t3
				     ) ;
int load_Cmatrix_VOCE(void *param, double C[6][6]);

int init_variables_VOCE( Variables *sv, void *param) ;
/*
int load_param_VOCE(int N, void *param,void *state_variables, 
                      double load_sgl[12][6],
		      double load_n[12],
		      double load_Xp[N][12],
		      double load_tau0p[N][12]);				     
*/

/**************************************************************************/
#endif
/**************************************************************************/




