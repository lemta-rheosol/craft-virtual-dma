******************************************************************
* PS 
* 12 juin 2011
*
******************************************************************  
********************************************************************
      subroutine ldcpt_cristal_Voce(
     &     dt, 
     &     e11, e22, e33, e12, e13, e23,
     &     s11, s22, s33, s12, s13, s23,
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     sp11, sp22, sp33, sp12, sp13, sp23, 
     &     lC,
     &     nsys, sgli, GGamma, GGammap,
     &     tauc,taucp, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )
*-------------------------------------------------------------------
      implicit none
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* increment de temps:
      real*8 dt
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* grandeurs mecaniques:
*
* deformation:
      real*8 e11, e22, e33, e12, e13, e23
* contrainte:
      real*8 s11, s22, s33, s12, s13, s23
* deformation au chargement precedent:
      real*8 ep11, ep22, ep33, ep12, ep13, ep23
* contrainte au chargement precedent:
      real*8 sp11, sp22, sp33, sp12, sp13, sp23
*
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* loi de comportement:
      real*8 lC(6,6)
* nb de systemes de glissement:
      integer nsys,i
* systemes de glissement:
      real*8 sgli(6,nsys)
* cisaillements critiques:
      real*8 tauc(nsys),taucp(nsys),tauct(nsys)
      real*8 tau_ini(nsys),tau_sta(nsys),theta_ini(nsys),theta_sta(nsys)
* Glissement cumule 
      real*8 GGamma,GGammap
* gamma point 0
      real*8 gammap0(nsys)
* facteur de puissance: 
      real*8 npui(nsys)
      real*8 mat_H(nsys*nsys)
*-------------------------------------------------------------------
* variables necessaires a la resolution par la methode de Runge-Kutta 
* d'ordre 4:
      real*8 k1(6), k2(6), k3(6), k4(6)
      real*8 ktau1(nsys),ktau2(nsys),ktau3(nsys),ktau4(nsys)
      real*8 kGGamma1,kGGamma2,kGGamma3,kGGamma4
      real*8 st(6)


*-------------------------------------------------------------------

* Resolution de l'equation differentielle ds/dt = f(s, ...) par la 
* methode de Runge-Kutta d'ordre 4:
*
* 1er pas
      call fVoce(
     &     k1(1), k1(2), k1(3), k1(4), k1(5), k1(6),
     &     ktau1,kGGamma1,
     &     sp11, sp22, sp33, sp12, sp13, sp23,
     &     dt,
     &     e11, e22, e33, e12, e13, e23, 
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     lC,
     &     nsys, sgli, taucp, GGammap, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )

* 2e pas:
      st(1) = sp11 + k1(1) * dt * 0.5d0
      st(2) = sp22 + k1(2) * dt * 0.5d0
      st(3) = sp33 + k1(3) * dt * 0.5d0
      st(4) = sp12 + k1(4) * dt * 0.5d0
      st(5) = sp13 + k1(5) * dt * 0.5d0
      st(6) = sp23 + k1(6) * dt * 0.5d0

      GGamma = GGammap + kGGamma1 * dt * 0.5d0
      
      do i=1,nsys
         tauct(i) = taucp(i) + ktau1(i) * dt * 0.5d0 
      end do
 
            
      call fVoce(
     &     k2(1), k2(2), k2(3), k2(4), k2(5), k2(6),
     &     ktau2,kGGamma2,
     &     st(1), st(2), st(3), st(4), st(5), st(6),
     &     dt,
     &     e11, e22, e33, e12, e13, e23, 
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     lC,
     &     nsys, sgli, tauct, GGamma, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )

* 3e pas:
      st(1) = sp11 + k2(1) * dt * 0.5d0
      st(2) = sp22 + k2(2) * dt * 0.5d0
      st(3) = sp33 + k2(3) * dt * 0.5d0
      st(4) = sp12 + k2(4) * dt * 0.5d0
      st(5) = sp13 + k2(5) * dt * 0.5d0
      st(6) = sp23 + k2(6) * dt * 0.5d0

      GGamma = GGammap + kGGamma2 * dt * 0.5d0
      
      do i=1,nsys
         tauct(i) = taucp(i) + ktau2(i) * dt * 0.5d0 
      end do
      
      call fVoce(
     &     k3(1), k3(2), k3(3), k3(4), k3(5), k3(6),
     &     ktau3,kGGamma3,
     &     st(1),st(2),st(3),st(4),st(5),st(6),
     &     dt,
     &     e11, e22, e33, e12, e13, e23, 
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     lC,
     &     nsys, sgli, tauct,GGamma, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )

* 4e pas:
      st(1) = sp11 + k3(1) * dt 
      st(2) = sp22 + k3(2) * dt 
      st(3) = sp33 + k3(3) * dt 
      st(4) = sp12 + k3(4) * dt 
      st(5) = sp13 + k3(5) * dt 
      st(6) = sp23 + k3(6) * dt 
      
      GGamma = GGammap + kGGamma3 * dt 

      do i=1,nsys
         tauct(i) = taucp(i) + ktau3(i) * dt 
      end do
      
      
      call fVoce(
     &     k4(1), k4(2), k4(3), k4(4), k4(5), k4(6),
     &     ktau4,kGGamma4,
     &     st(1),st(2),st(3),st(4),st(5),st(6),
     &     dt,
     &     e11, e22, e33, e12, e13, e23,
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     lC,
     &     nsys, sgli, tauct, GGamma, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )


      
* solution:
      s11 = sp11 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(1) + k4(1) ) + k2(1) + k3(1) )
      s22 = sp22 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(2) + k4(2) ) + k2(2) + k3(2) )
      s33 = sp33 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(3) + k4(3) ) + k2(3) + k3(3) )
      s12 = sp12 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(4) + k4(4) ) + k2(4) + k3(4) )
      s13 = sp13 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(5) + k4(5) ) + k2(5) + k3(5) )
      s23 = sp23 + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( k1(6) + k4(6) ) + k2(6) + k3(6) )

      GGamma = GGammap + 
     &     dt/3.d0 * 
     &     ( 0.5d0 * ( kGGamma1 + kGGamma4 ) + kGGamma2 + kGGamma3)
     
      do i=1,nsys
         tauc(i) = taucp(i) + 
     &      dt/3.d0 * 
     &     ( 0.5d0 * (ktau1(i) + ktau4(i) ) + ktau2(i) + ktau3(i)) 
	 
      end do
      

*-------------------------------------------------------------------


      
*-------------------------------------------------------------------
      end

********************************************************************
* routine calculant sigma point:
*       - ds11, ds22, ds33, ds12, 
* partant de:
*       - e11, e22, e33, e12      : epsilon au temps courant
*       - ep11, ep22, ep33, ep12  : epsilon du pas de temps precedent
*       - dt                      : increment de temps entre le pas de temps
*                                   precedent et le pas de temps courant
*       - sp11, sp22, sp33, sp12 : sigma au pas de temps precedent
*
*       - les grandeurs lie au materiau: lb, mu, nsys, ...
*


********************************************************************
********************************************************************
         subroutine fVoce(
     &     ds11, ds22, ds33, ds12, ds13, ds23,
     &     dtauc,dGGamma,
     &     sp11, sp22, sp33, sp12, sp13, sp23,
     &     dt,
     &     e11, e22, e33, e12, e13, e23, 
     &     ep11, ep22, ep33, ep12, ep13, ep23,
     &     lC,
     &     nsys, sgli, taucp, GGammap, gammap0, npui,
     &     tau_ini,tau_sta,theta_ini,theta_sta,mat_H
     &     )


      implicit none
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* increment de temps:
      real*8 dt
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* grandeurs mecaniques:
*
* deformation:
      real*8 e11, e22, e33, e12, e13, e23
* deformation au chargement precedent:
      real*8 ep11, ep22, ep33, ep12, ep13, ep23
* contrainte au chargement precedent:
      real*8 sp11, sp22, sp33, sp12, sp13, sp23
* sigma point :
      real*8 ds11, ds22, ds33, ds12, ds13, ds23
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* loi de comportement:
      real*8 lC(6,6)
* nb de systemes de glissement:
      integer nsys
* systemes de glissement:
      real*8 sgli(6,nsys)
* cisaillements critiques:
      real*8 taucp(nsys)
      real*8 tau_ini(nsys),tau_sta(nsys),theta_ini(nsys),theta_sta(nsys)
* glissements cumules
      real*8 GGammap
* gamma point 0 :
      real*8 gammap0(nsys)
* facteur de puissance: 
      real*8 npui(nsys)
      real*8 mat_H(nsys*nsys)
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -      
* tauc point , GGamma point, gamma point:
      real*8 dtauc(nsys),dGGamma,ppoint(nsys),pgamma(nsys)
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      integer isys,i,l
      real*8 devp11, devp22, devp33, devp12, devp13, devp23
* autres variables intermediaires , etc.
      real*8 sm
      real*8 a,b,tau1
      

* epsilon elastique point (derivee temporelle de la composante elastique de
* la deformation)
      real*8 dee11, dee22, dee33, dee12, dee13, dee23
*-------------------------------------------------------------------    
      
*     1- calcul de epsilon point viscoplastique
      devp11 = 0.d0
      devp22 = 0.d0
      devp33 = 0.d0
      devp12 = 0.d0
      devp13 = 0.d0
      devp23 = 0.d0 
      dGGamma = 0.d0

      do isys = 1,nsys

* cission sur ce systeme
        sm = sp11*sgli(1,isys)+ sp22*sgli(2,isys) +
     &	     sp33*sgli(3,isys)+ 2.d0*(sp23*sgli(4,isys) +
     &       sp13*sgli(5,isys)+ sp12*sgli(6,isys))


     
* gamma point
         pgamma(isys) = sign(1.d0,sm)*gammap0(isys)* 
     &            (dabs(sm)/taucp(isys))**npui(isys)
        

         devp11 = devp11 + pgamma(isys) * sgli(1,isys) 
         devp22 = devp22 + pgamma(isys) * sgli(2,isys)
         devp33 = devp33 + pgamma(isys) * sgli(3,isys)
         devp23 = devp23 + pgamma(isys) * sgli(4,isys)
         devp13 = devp13 + pgamma(isys) * sgli(5,isys)
         devp12 = devp12 + pgamma(isys) * sgli(6,isys)
	 dGGamma = dGGamma +  dabs(pgamma(isys))
 
      end do 
	 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*     2- calcul de epsilon point elastique
      dee11 = (e11-ep11)/dt - devp11
      dee22 = (e22-ep22)/dt - devp22
      dee33 = (e33-ep33)/dt - devp33
      dee23 = (e23-ep23)/dt - devp23
      dee13 = (e13-ep13)/dt - devp13
      dee12 = (e12-ep12)/dt - devp12
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*     3- calcul de sigma point
* POUR L'INSTANT LIMITE A PHASES ISOTROPES
* A CHANGER
*
*      ds11 = lb*(dee11+dee22+dee33) + 2.d0*mu*dee11
*      ds22 = lb*(dee11+dee22+dee33) + 2.d0*mu*dee22
*      ds33 = lb*(dee11+dee22+dee33) + 2.d0*mu*dee33
*
*      ds23 = 2.d0*mu*dee23
*      ds13 = 2.d0*mu*dee13
*      ds12 = 2.d0*mu*dee12

       call el3D(lC,
     &           dee11,dee22,dee33,dee12,dee13,dee23,
     &           ds11,ds22,ds33,ds12,ds13,ds23)

*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* calcul de tauc point et GGamma point

      do isys = 1,nsys

        ppoint(isys)=0.d0
          do l=1,nsys
             ppoint(isys)=ppoint(isys)
     $            +mat_H( (isys-1)*nsys + l)*dabs(pgamma(l))
          enddo
	 
        tau1=tau_sta(isys)-tau_ini(isys)
        a=dabs(theta_ini(isys)/tau1)
        b=dexp(-GGammap*a)
        dtauc(isys)=ppoint(isys)*(theta_sta(isys)+
     $       b*(theta_ini(isys)+a*theta_sta(isys)*GGammap
     $       -theta_sta(isys)))
     
	 end do
	 
      
      end

********************************************************************
!------------------------------------------------------------------
! AL
! 28/05/2009
!
! loi de comportement elastique lineaire 3d
!------------------------------------------------------------------
      subroutine el3D( 
     &     C,  
     &     e11, e22, e33, e12, e13, e23,
     &     s11, s22, s33, s12, s13, s23
     &     )
      
      implicit none
      
      integer n1, n2, n3
      integer nd1, nd2, nd3

      real*8 e11, e22, e33
      real*8 e12, e13, e23

      real*8 s11, s22, s33
      real*8 s12, s13, s23
      real*8 rac2,urac2
      
      real*8 C(6,6)
      
      
      rac2=sqrt(2.d0)
      urac2=1.d0/rac2
      
      s11 = C(1,1)*e11+C(1,2)*e22+C(1,3)*e33
     &  +rac2*C(1,4)*e23+rac2*C(1,5)*e13+rac2*C(1,6)*e12
      
      s22 = C(2,1)*e11+C(2,2)*e22+C(2,3)*e33
     &  +rac2*C(2,4)*e23+rac2*C(2,5)*e13+rac2*C(2,6)*e12
     
      s33 = C(3,1)*e11+C(3,2)*e22+C(3,3)*e33
     &  +rac2*C(3,4)*e23+rac2*C(3,5)*e13+rac2*C(3,6)*e12
     
      s23 = urac2*C(4,1)*e11+urac2*C(4,2)*e22+urac2*C(4,3)*e33
     &  +C(4,4)*e23+C(4,5)*e13+C(4,6)*e12
      
      s13 = urac2*C(5,1)*e11+urac2*C(5,2)*e22+urac2*C(5,3)*e33
     &  + C(5,4)*e23+C(5,5)*e13+C(5,6)*e12
      
      s12 = urac2*C(6,1)*e11+urac2*C(6,2)*e22+urac2*C(6,3)*e33
     &  + C(6,4)*e23+C(6,5)*e13+C(6,6)*e12
      
       
      end

