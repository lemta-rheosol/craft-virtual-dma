#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>

#include <PRESSURIZED_GAS_materials.h>
#include <variables.h>
#include <LE_materials.h>
#include <LE_Cmatrix.h>

#ifdef _OPENMP
#include <omp.h>
#endif
#define NEGLIGIBLE 1.e-15
/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage voids

*/
/*######################################################################*/
int read_parameters_PRESSURIZED_GAS( FILE *f, void *p ) {
  int status,Nt,it;
  PRESSURIZED_GAS_param *x;
  double tim, pb;
  x = (PRESSURIZED_GAS_param *)p;

    x->tpb = malloc(1*sizeof(*x->tpb));
    if (x->tpb==NULL) return -1;

    status=craft_fscanf(f,"%d",&(Nt));
    if (status!=1) return -1;
    x->tpb->Nt = Nt;
    x->tpb->tim  = malloc(Nt*sizeof(*x->tpb->tim));
    x->tpb->pb = malloc(Nt*sizeof(*x->tpb->pb));
    if (x->tpb->tim ==NULL) return -1;
    if (x->tpb->pb==NULL) return -1;


    for(it=0; it<(Nt); it++){
      status=craft_fscanf(f,"%lf",&(tim));
      if (status!=1) return -1;
      x->tpb->tim[it] = tim;

      status=craft_fscanf(f,"%lf",&(pb));
      if (status!=1) return -1;
      x->tpb->pb[it] = pb;
    }
    
  if (status!=1) return -1;
  return 0;
}

/*######################################################################*/
int print_parameters_PRESSURIZED_GAS( FILE *f, void *p , int flag ) {
  int status,it;

  PRESSURIZED_GAS_param *x;
  status=0;
  x = (PRESSURIZED_GAS_param *)p;
  fprintf(f,"# tabulated pressure  ");
  for(it=0; it<(x->tpb->Nt); it++){
        fprintf(f,"# \t %lf \t %lf", 
          x->tpb->tim[it], x->tpb->pb[it]);
  }

  fprintf(f,"\n");
  return status;
}


/*######################################################################*/
int allocate_parameters_PRESSURIZED_GAS( void **p ) {
  int status;

  PRESSURIZED_GAS_param *x;
  status=0;
  x=(PRESSURIZED_GAS_param *)malloc(sizeof(PRESSURIZED_GAS_param));

  *p = (void *)x;

  if( p == (void *)0 ) {
    status=-1;
  }
  return status;

}

/*######################################################################*/
int deallocate_parameters_PRESSURIZED_GAS( void *p ) {

  int status;
  free(p);

  status = 0;

  return status;
}
/*######################################################################*/
int copy_parameters_PRESSURIZED_GAS( void *param_dest, void *param_src) {
  *((PRESSURIZED_GAS_param *)param_dest) = *((PRESSURIZED_GAS_param *)param_src);

  return 0;
}

/*######################################################################*/
int behavior_PRESSURIZED_GAS(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt){


  int i;
  PRESSURIZED_GAS_param *p;
  int N;

  N = sv->np;

  p  = ((PRESSURIZED_GAS_param *)param);

  double (*sigma) [6];
  
  sigma = (double (*)[6])(sv->vars[1].values);
  
#pragma omp parallel for                        \
  private(i) shared (N,p)                         \
  shared (sigma) 

  for(i=0;i<N;i++){
    sigma[i][0] = -p->tpb->pressure_actu;
    sigma[i][1] = -p->tpb->pressure_actu;
    sigma[i][2] = -p->tpb->pressure_actu;
    
    sigma[i][3] = 0.;
    sigma[i][4] = 0.;
    sigma[i][5] = 0.;
  }
  
  return 0;
}

/*######################################################################*/
int  allocate_variables_PRESSURIZED_GAS( int N, Variables *sv, void *param) {
  int status;
  int tensor_t[3]={TENSOR2,0,0};
  status = 0;
  sv->np = N;
  sv->nvar = 3;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if (sv->vars==NULL) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;

#ifdef DEBUG
  printf("Allocated Variables for PRESSURIZED_GAS.\n",status);
#endif
  return 0;
}

/*######################################################################*/
int extrapolate_variables_PRESSURIZED_GAS(
                               void *param,
                               Euler_Angles orientation,
                               Variables *sv,
                               double t1, double t2, double t3) {
  int status;

  status = extrapol2( 6*sv->np, t1,
                      *((double (*)[6]) sv->vars[2].values), t2,
                      *((double (*)[6]) sv->vars[0].values), t3 );
  PRESSURIZED_GAS_param *x;
  x = (PRESSURIZED_GAS_param *)param;
  double coef;
  int it,k; it=k=0;
  while((it<(x->tpb->Nt)) && (!k)){
          if (t2>(x->tpb->tim[it])){it++;}
          else{
            k=1;
            if (t3<(x->tpb->tim[it])){
               coef=(t3-t2)/
               ((x->tpb->tim[it])-(x->tpb->tim[it-1]));
                double new_pb=x->tpb->pressure_actu+
                       coef*(x->tpb->pb[it]-
                             x->tpb->pb[it-1]);
                x->tpb->pressure_actu=new_pb;
            }
            else {
               coef=(t3-(x->tpb->tim[it]))/
               ((x->tpb->tim[it+1])-(x->tpb->tim[it]));
                printf("\nt3=%g,t2=%g,coef=%g\n",t3,t2,coef); 
                double new_pb=x->tpb->pb[it]+
                       coef*(x->tpb->pb[it+1]-
                             x->tpb->pb[it]);
                x->tpb->pressure_actu=new_pb;
               printf("newpb=%g, status=%i,k=%i\n",new_pb,status,k); 
            }
              
          }//end else
  }//end while
  if (t2>=(x->tpb->tim[(x->tpb->Nt)-1])){
               it =(x->tpb->Nt)-2;
               coef=(t3-(x->tpb->tim[it+1]))/
               ((x->tpb->tim[it+1])-(x->tpb->tim[it]));
                double new_pb=x->tpb->pb[it+1]+
                       coef*(x->tpb->pb[it+1]-
                             x->tpb->pb[it]);
                x->tpb->pressure_actu=new_pb;
  }

  return status;
}
/*######################################################################*/
int  special_init_variables_PRESSURIZED_GAS( Variables *sv, void *param,double (**phase_i_sigma)[6],double (**phase_i_epsilon)[6]) {
  int status=0;
  double (*sigma) [6];
  double (*epsilon) [6];
  double (*epsilon_p) [6];
  int i,j;
  
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  epsilon   = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  epsilon_p  = (double (*)[6])(ptr_variable("previous strain", sv, param)->values);
  for (i=0;i<sv->np;i++){
    for (j=0;j<6;j++){
    sigma[i][j]=(*phase_i_sigma)[i][j];
    epsilon[i][j]=(*phase_i_epsilon)[i][j];
    epsilon_p[i][j]=(*phase_i_epsilon)[i][j];
    }
  }

  return status;
}
/*######################################################################*/
/*######################################################################*/
int load_Cmatrix_PRESSURIZED_GAS(void *param, double C[6][6])
{

int i,j;

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
return(0);
} 
/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
void inversion_(double *a, double *b, int *n, double *err);
int solve_spc0e_PRESSURIZED_GAS( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double (*tau)[6]
		 ){
  
  int status;
  int N;
  
  double (*epsilon) [6];
  double (*sigma) [6];

  int i,j;


  status = 0;
  N = sv->np;
  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  

  /* anisotropic case */
  if ( (L0->isotropy == ANISOTROPIC) ) {

    double C0[6][6];
    double CC[6][6];


    status = load_Cmatrix_LE( L0, C0);
    for(i=0;i<6;i++) {
      for(j=0;j<6;j++) {
	CC[i][j] = C0[i][j];
      }
    }
    double err;
    int n=6;
    inversion_(&CC[0][0],&CC[0][0],&n,&err); /* inversion of C+C0 matrix */
    if(err > NEGLIGIBLE ) {
      fprintf(stderr,"CraFT error: matrix inversion failed in solve_spc0e\n");
      status = -1;
      return status;
    }

  PRESSURIZED_GAS_param *p;
  p  = ((PRESSURIZED_GAS_param *)param);

  double (*sigma) [6];
  
  sigma = (double (*)[6])(sv->vars[1].values);
#pragma omp parallel for			\
  default(none)					\
  private(i)					\
  shared(N,epsilon,sigma,tau,status,CC,p)
    for(i=0;i<N;i++){
      status = LE_behavior_Cmatrix(tau[i],epsilon[i],CC);

      sigma[i][0] = -p->tpb->pressure_actu;
      sigma[i][1] = -p->tpb->pressure_actu;
      sigma[i][2] = -p->tpb->pressure_actu;
      sigma[i][3] = 0.;
      sigma[i][4] = 0.;
      sigma[i][5] = 0.;

    }



  }
  /* isotropic case */
  else if( (L0->isotropy == ISOTROPIC) ) {


    double lb2,mu2;


    double h;
    double coef1, coef2;
    double h2;




    lb2 = L0->sub.ile->lb;
    mu2 = L0->sub.ile->mu;

    coef1 = 1./(3.*lb2+2.*mu2);
    coef2 = 0.5/mu2;

    PRESSURIZED_GAS_param *p;
    p  = ((PRESSURIZED_GAS_param *)param);

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2,p)
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3. ;
      h2 = coef1 * (h + p->tpb->pressure_actu);

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

 

      sigma[i][0] = -p->tpb->pressure_actu;
      sigma[i][1] = -p->tpb->pressure_actu;
      sigma[i][2] = -p->tpb->pressure_actu;
      sigma[i][3] = 0.;
      sigma[i][4] = 0.;
      sigma[i][5] = 0.;



    }

      
      
  }
			  
  return status;
}
/*######################################################################*/
