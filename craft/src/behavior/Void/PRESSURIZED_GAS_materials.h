/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   header file for Void behavior.

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   IT MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef PRESSURIZED_GAS_MATERIAL
#define PRESSURIZED_GAS_MATERIAL 70

#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>

/**************************************************************************/

typedef struct {
  int Nt;       /* number of times on which porosity evolution is tabulated */
  double *tim;    /* tabulated times */
  double *pb;   /* tabulated plastic stresses */
  double pressure_actu;
} tab_pb_param;

/**************************************************************************/
typedef struct {
  //double Pressure;     /* Pressure      */
  tab_pb_param* tpb;
} PRESSURIZED_GAS_param;

/*------------------------------------------------------------------------*/
int read_parameters_PRESSURIZED_GAS( FILE *f, void *p );
int print_parameters_PRESSURIZED_GAS( FILE *f, void *p , int flag);

int allocate_parameters_PRESSURIZED_GAS( void **p );
int deallocate_parameters_PRESSURIZED_GAS( void *p );
int copy_parameters_PRESSURIZED_GAS( void *param_dest, void *param_src);
int  special_init_variables_PRESSURIZED_GAS( Variables *sv, void *param,double (**phase_i_sigma)[6],double (**phase_i_epsilon)[6]);

int behavior_PRESSURIZED_GAS( 
                  void *param, Euler_Angles orientation,
                  Variables *sv,
                  double dt);	

int allocate_variables_PRESSURIZED_GAS(int N, Variables *sv, void *param);
int extrapolate_variables_PRESSURIZED_GAS( 
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3);
int load_Cmatrix_PRESSURIZED_GAS(void *param, double C[6][6]);
int solve_spc0e_PRESSURIZED_GAS( 
  void *param, Euler_Angles orientation, Variables *sv, double dt,
  LE_param *L0, 
  double (*tau)[6] );
/**************************************************************************/
#endif
