#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


#include <meca.h>
#include <materials.h>
#include <utils.h>

#include <VOID_materials.h>
#include <variables.h>
#include <LE_materials.h>
#include <LE_Cmatrix.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#define NEGLIGIBLE 1.e-12

/*######################################################################*/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   functions to manage voids

*/
/*######################################################################*/
int read_parameters_VOID( FILE *f, void *p ) {

  return 0;
}

/*######################################################################*/
int print_parameters_VOID( FILE *f, void *p , int flag ) {
  int status;
  status=0;

  
  return status;
}


/*######################################################################*/
int allocate_parameters_VOID( void **p ) {
  int status;

  status=0;

  return status;

}

/*######################################################################*/
int deallocate_parameters_VOID( void *p ) {

  int status;

  status = 0;

  return status;
}
/*######################################################################*/
int copy_parameters_VOID( void *param_dest, void *param_src) {

  return 0;
}

/*######################################################################*/
int behavior_VOID(
    void *param, Euler_Angles orientation,
    Variables *sv,
    double dt){
  
  int i,N;
  double (*sigma) [6];
  
  N = sv->np;
  
  sigma = (double (*)[6])(sv->vars[1].values);
  
#pragma omp parallel for                        \
  private(i) shared (N)                         \
  shared (sigma) 
  for(i=0;i<N;i++){
    sigma[i][0] = 0.;
    sigma[i][1] = 0.;
    sigma[i][2] = 0.;
    
    sigma[i][3] = 0.;
    sigma[i][4] = 0.;
    sigma[i][5] = 0.;
  }
  
  return 0;
}

/*######################################################################*/
int  allocate_variables_VOID( int N, Variables *sv, void *param) {
  int status;
  int tensor_t[3]={TENSOR2,0,0};
  status = 0;
  sv->np = N;
  sv->nvar = 3;
  sv->vars = malloc(sv->nvar*sizeof(*(sv->vars)));
  if (sv->vars==NULL) return -1;

  status = allocate_variable(&sv->vars[0], "strain", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[1], "stress", N, tensor_t, 0);
  if (status<0) return -1;
  status = allocate_variable(&sv->vars[2], "previous strain", N, tensor_t, 0);
  if (status<0) return -1;

#ifdef DEBUG
  printf("Allocated Variables for VOID.\n",status);
#endif
  return 0;
}

/*######################################################################*/
int extrapolate_variables_VOID(
                               void *param,
                               Euler_Angles orientation,
                               Variables *sv,
                               double t1, double t2, double t3) {
  int status;

  status = extrapol2( 6*sv->np, t1,
                      *((double (*)[6]) sv->vars[2].values), t2,
                      *((double (*)[6]) sv->vars[0].values), t3 );
  return status;
}

/*######################################################################*/
int load_Cmatrix_VOID(void *param, double C[6][6])
{

int i,j;

for (i=0;i<6;i++){
 for (j=0;j<6;j++){
 C[i][j] = 0.;
 }
}
 
return(0);
} 
/*######################################################################*/
/* function which computes sigma and epsilon verifying:
   sigma + C0:epsilon = tau
*/
void inversion_(double *a, double *b, int *n, double *err);
int solve_spc0e_VOID( void *param, Euler_Angles orientation, Variables *sv, double dt,
		 LE_param *L0, 
		 double (*tau)[6]
		 ){
  
  int status;
  int N;
  
  double (*epsilon) [6];
  double (*sigma) [6];

  int i,j;


  status = 0;
  N = sv->np;
  epsilon = (double (*)[6])(ptr_variable("strain", sv, param)->values);
  sigma   = (double (*)[6])(ptr_variable("stress", sv, param)->values);
  

  /* anisotropic case */
  if ( (L0->isotropy == ANISOTROPIC) ) {
    //    double C[6][6];
    double C0[6][6];
    double CC[6][6];


    status = load_Cmatrix_LE( L0, C0);
    for(i=0;i<6;i++) {
      for(j=0;j<6;j++) {
	CC[i][j] = C0[i][j];
      }
    }
    double err;
    int n=6;
    inversion_(&CC[0][0],&CC[0][0],&n,&err); /* inversion of C+C0 matrix */
    //    if(err > 1.e-15 ) {
    if(err > NEGLIGIBLE ) {    
      fprintf(stderr,"CraFT error: matrix inversion failed in solve_spc0e\n");
      status = -1;
      return status;
    }

#pragma omp parallel for			\
  default(none)					\
  private(i)					\
  shared(N,epsilon,sigma,tau,status,CC)
    for(i=0;i<N;i++){
      status = LE_behavior_Cmatrix(tau[i],epsilon[i],CC);

      sigma[i][0] = 0.;
      sigma[i][1] = 0.;
      sigma[i][2] = 0.;
      sigma[i][3] = 0.;
      sigma[i][4] = 0.;
      sigma[i][5] = 0.;

    }



  }
  /* isotropic case */
  else if( (L0->isotropy == ISOTROPIC) ) {


    double lb2,mu2;


    double h;
    double coef1, coef2;
    double h2;




    lb2 = L0->sub.ile->lb;
    mu2 = L0->sub.ile->mu;

    coef1 = 1./(3.*lb2+2.*mu2);
    coef2 = 0.5/mu2;

#pragma omp parallel for				\
  default(none)						\
  private(i,h2,h)					\
  shared(N,sigma,epsilon,tau,coef1,coef2)
    for(i=0;i<N;i++) {

      h = (tau[i][0]+tau[i][1]+tau[i][2]) / 3.;
      h2 = coef1 * h;

      epsilon[i][0] = coef2*(tau[i][0] - h) + h2;
      epsilon[i][1] = coef2*(tau[i][1] - h) + h2;
      epsilon[i][2] = coef2*(tau[i][2] - h) + h2;
      epsilon[i][3] = coef2*(tau[i][3]);
      epsilon[i][4] = coef2*(tau[i][4]);
      epsilon[i][5] = coef2*(tau[i][5]);

 

      sigma[i][0] = 0.;
      sigma[i][1] = 0.;
      sigma[i][2] = 0.;
      sigma[i][3] = 0.;
      sigma[i][4] = 0.;
      sigma[i][5] = 0.;



    }

      
      
  }
			  
  return status;
}
/*######################################################################*/
