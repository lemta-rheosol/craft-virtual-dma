/**************************************************************************/
/* H. Moulinec
   LMA/CNRS
   october 1st 2009
   
   header file for Void behavior.

*/
/**************************************************************************/
/* environment variable defining the type of behavior
   IT MUST BE UNIQUE
   (anyway: this will be verified in compilation of init_materials.)
*/
#ifndef VOID_MATERIAL
#define VOID_MATERIAL 0

#define _ISOC99_SOURCE

#include <variables.h>
#include <euler.h>

/**************************************************************************/
/* empty structure are theoretically forbidden. Thus, to avoid a warning message
   such as:
           warning: struct has no members [-Wpedantic]
   during compilation stage, one replaces the definitions of  VOID_param structure:
           typedef struct {} VOID_param;
   by:
*/
typedef struct _VOID_param VOID_param;

/*------------------------------------------------------------------------*/
int read_parameters_VOID( FILE *f, void *p );
int print_parameters_VOID( FILE *f, void *p , int flag);

int allocate_parameters_VOID( void **p );
int deallocate_parameters_VOID( void *p );
int copy_parameters_VOID( void *param_dest, void *param_src);


int behavior_VOID( 
                  void *param, Euler_Angles orientation,
                  Variables *sv,
                  double dt);	

int allocate_variables_VOID(int N, Variables *sv, void *param);
int extrapolate_variables_VOID( 
    void *param,
    Euler_Angles orientation,
    Variables *sv,
    double t1, double t2, double t3);
int load_Cmatrix_VOID(void *param, double C[6][6]);
int solve_spc0e_VOID( 
  void *param, Euler_Angles orientation, Variables *sv, double dt,
  LE_param *L0, 
  double (*tau)[6] );
/**************************************************************************/
#endif
