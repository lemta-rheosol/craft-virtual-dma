#include <stdio.h>
#include <variables.h>
#include <materials.h>

#include <VOID_materials.h>
#include <VOIDH_materials.h>

#include <PRESSURIZED_GAS_materials.h>
#include <ILE_materials.h>
#include <LE_materials.h>
#include <EPP_materials.h>
#include <EPIH_materials.h>

#include <EVP_materials.h>
#include <EVP_H_materials.h>

#include <VE_materials.h>
#include <VEH_materials.h>
#include <OSVEH_materials.h>
#include <VEHT_materials.h>

#include <LEH_materials.h>

#include <VOCE_materials.h>
#include <GURSON_materials.h>
//#include <GURSON_COEF_materials.h>

/*****************************************************************************/
/*
  return value:
  0 : succesful return
  -1: unknown behavior type

 */
/*****************************************************************************/
int Init_Material( int type , Material *zmat ){

  /*-------------------------------------------------------------------------*/
  /* declarations */
  /*-------------------------------------------------------------------------*/

  int status;

  /*-------------------------------------------------------------------------*/

  status=0;

  zmat->type = type;

  zmat->parameters=(void *)NULL;
  zmat->deallocate_variables=&deallocate_variables;
  zmat->init_variables=&init_variables;
  zmat->ptr_variable=&ptr_variable;

  int solve_spc0e_notimplemented(void *param,
                                 Euler_Angles orientation,
                                 Variables *sv,
                                 double dt,
                                 LE_param *C0,
                                 double (*tau)[6]);
  zmat->solve_spc0e = &solve_spc0e_notimplemented;

  /*-------------------------------------------------------------------------*/
  /* switch among the different behaviors implemented
   */
  /*-------------------------------------------------------------------------*/
  
  switch(zmat->type){

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case VOID_MATERIAL:
    zmat->typename="void"; /* VOID */
    zmat->read_parameters=&read_parameters_VOID;
    zmat->print_parameters=&print_parameters_VOID;
    zmat->allocate_parameters=&allocate_parameters_VOID;
    zmat->deallocate_parameters=&deallocate_parameters_VOID;
    zmat->copy_parameters=&copy_parameters_VOID;

    zmat->behavior=&behavior_VOID;
    zmat->allocate_variables=&allocate_variables_VOID;
    zmat->extrapolate_variables=&extrapolate_variables_VOID;
    zmat->load_Cmatrix=&load_Cmatrix_VOID;
    zmat->solve_spc0e = &solve_spc0e_VOID;
    break; /* VOID */
  case VOIDH_MATERIAL:
    zmat->typename="void harmonic"; /* VOIDH */
    zmat->read_parameters=&read_parameters_VOIDH;
    zmat->print_parameters=&print_parameters_VOIDH;
    zmat->allocate_parameters=&allocate_parameters_VOIDH;
    zmat->deallocate_parameters=&deallocate_parameters_VOIDH;
    zmat->copy_parameters=&copy_parameters_VOIDH;

    zmat->behavior=&behavior_VOIDH;
    zmat->allocate_variables=&allocate_variables_VOIDH;
    zmat->extrapolate_variables=&extrapolate_variables_VOIDH;
    zmat->load_Cmatrix=&load_Cmatrix_VOIDH;
    zmat->solve_spc0e = &solve_spc0e_VOIDH;
    break; /* VOIDH */
  case PRESSURIZED_GAS_MATERIAL:
    zmat->typename="void"; /* VOID */
    zmat->read_parameters=&read_parameters_PRESSURIZED_GAS;
    zmat->print_parameters=&print_parameters_PRESSURIZED_GAS;
    zmat->allocate_parameters=&allocate_parameters_PRESSURIZED_GAS;
    zmat->deallocate_parameters=&deallocate_parameters_PRESSURIZED_GAS;
    zmat->copy_parameters=&copy_parameters_PRESSURIZED_GAS;

    zmat->behavior=&behavior_PRESSURIZED_GAS;
    zmat->allocate_variables=&allocate_variables_PRESSURIZED_GAS;
    zmat->extrapolate_variables=&extrapolate_variables_PRESSURIZED_GAS;
    zmat->load_Cmatrix=&load_Cmatrix_PRESSURIZED_GAS;
    zmat->solve_spc0e = &solve_spc0e_PRESSURIZED_GAS;
    break; /* PRESSURIZED_GAS */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case ILE_MATERIAL:
    //    fprintf(stderr,"Warning: obsolescent material id %d. ",zmat->type);
    //    fprintf(stderr,"You can use id 10 with flag 1 instead.\n");

    zmat->typename="isotropic linear elastic"; /* ILE */
    zmat->read_parameters=&read_parameters_ILE;
    zmat->print_parameters=&print_parameters_LE;
    zmat->allocate_parameters=&allocate_parameters_LE;
    zmat->deallocate_parameters=&deallocate_parameters_LE;
    zmat->copy_parameters=&copy_parameters_LE;
    //zmat->special_init_variables=&special_init_variables_LE;
    
    
    zmat->behavior=&behavior_LE;
    zmat->allocate_variables=&allocate_variables_LE;
    zmat->extrapolate_variables=&extrapolate_variables_LE;
    zmat->load_Cmatrix=&load_Cmatrix_LE;
    zmat->solve_spc0e = &solve_spc0e_LE;
    break;  /* ILE */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  case LE_MATERIAL_OLD:
    //    fprintf(stderr,"Warning: obsolescent material id %d. ",zmat->type);
    //    fprintf(stderr,"You can use id 10 instead.\n");
  case LE_MATERIAL:
    zmat->typename="Linear elastic"; /* LE */
    zmat->read_parameters=&read_parameters_LE;
    zmat->print_parameters=&print_parameters_LE;
    zmat->allocate_parameters=&allocate_parameters_LE;
    zmat->deallocate_parameters=&deallocate_parameters_LE;
    zmat->copy_parameters=&copy_parameters_LE;
    //zmat->special_init_variables=&special_init_variables_LE;

    zmat->behavior=&behavior_LE;
    zmat->allocate_variables=&allocate_variables_LE;
    zmat->extrapolate_variables=&extrapolate_variables_LE;
    zmat->load_Cmatrix=&load_Cmatrix_LE;
    zmat->solve_spc0e = &solve_spc0e_LE;
    break; /* LE */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case EPP_MATERIAL:
    zmat->typename="elastic perfectly plastic"; /* EPP */
    zmat->read_parameters=&read_parameters_EPP;
    zmat->print_parameters=&print_parameters_EPP;
    zmat->allocate_parameters=&allocate_parameters_EPP;
    zmat->deallocate_parameters=&deallocate_parameters_EPP;
    zmat->copy_parameters=&copy_parameters_EPP;

    zmat->behavior=&behavior_EPP;
    zmat->allocate_variables=&allocate_variables_EPP;
    zmat->extrapolate_variables=&extrapolate_variables_EPP;
    zmat->load_Cmatrix=&load_Cmatrix_EPP;
    break; /* EPP */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case EPIH_MATERIAL:
    zmat->typename="elastic plastic isotrope hardening"; /* EPIH */
    zmat->read_parameters=&read_parameters_EPIH;
    zmat->print_parameters=&print_parameters_EPIH;
    zmat->allocate_parameters=&allocate_parameters_EPIH;
    zmat->deallocate_parameters=&deallocate_parameters_EPIH;
    zmat->copy_parameters=&copy_parameters_EPIH;

    zmat->behavior=&behavior_EPIH;
    zmat->allocate_variables=&allocate_variables_EPIH;
    zmat->extrapolate_variables=&extrapolate_variables_EPIH;
    zmat->load_Cmatrix=&load_Cmatrix_EPIH;
    zmat->solve_spc0e = &solve_spc0e_EPIH;
    break; /* EPIH */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    //    printf("in init material evpc_h\n");
    //    printf("in init material evpc_h2\n");

//    printf("in init material evpc_h3\n");
    //    printf("in init material evpc_h4\n");

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case EVP_MATERIAL:
    zmat->typename="power law Elastic Visco-Plastic behavior"; /* EVP */
    zmat->read_parameters=&read_parameters_EVP;
    zmat->print_parameters=&print_parameters_EVP;
    zmat->allocate_parameters=&allocate_parameters_EVP;
    zmat->deallocate_parameters=&deallocate_parameters_EVP;
    zmat->copy_parameters=&copy_parameters_EVP;

    zmat->behavior=&behavior_EVP;
    zmat->allocate_variables=&allocate_variables_EVP;
    zmat->extrapolate_variables=&extrapolate_variables_EVP;
    zmat->load_Cmatrix=&load_Cmatrix_EVP;
    break; /* EVP */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case EVP_H_MATERIAL:
    zmat->typename=" power law Elastic Visco-Plastic behavior with kinematic linear hardening"; /* EVP_H */
    zmat->read_parameters=&read_parameters_EVP_H;
    zmat->print_parameters=&print_parameters_EVP_H;
    zmat->allocate_parameters=&allocate_parameters_EVP_H;
    zmat->deallocate_parameters=&deallocate_parameters_EVP_H;
    zmat->copy_parameters=&copy_parameters_EVP_H;

    zmat->behavior=&behavior_EVP_H;
    zmat->allocate_variables=&allocate_variables_EVP_H;
    zmat->extrapolate_variables=&extrapolate_variables_EVP_H;
    zmat->load_Cmatrix=&load_Cmatrix_EVP_H;

    zmat->ptr_variable=&ptr_variable_EVP_H;

    break; /* EVP_H */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/



    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case VE_MATERIAL:
    //printf("VE_Material\n");
    zmat->typename="visco elastic"; /* VE */
    zmat->read_parameters=&read_parameters_VE;
    zmat->print_parameters=&print_parameters_VE;
    zmat->allocate_parameters=&allocate_parameters_VE;
    zmat->deallocate_parameters=&deallocate_parameters_VE;
    zmat->copy_parameters=&copy_parameters_VE;

    zmat->behavior=&behavior_VE;
    zmat->allocate_variables=&allocate_variables_VE;
    zmat->extrapolate_variables=&extrapolate_variables_VE;
    zmat->load_Cmatrix=&load_Cmatrix_VE;
    break; /* VE */


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case VEH_MATERIAL:
    //printf("VEH_Material\n");
    zmat->typename="harmonic visco elastic"; /* VEH */
    zmat->read_parameters=&read_parameters_VEH;
    zmat->print_parameters=&print_parameters_VEH;
    zmat->allocate_parameters=&allocate_parameters_VEH;
    zmat->deallocate_parameters=&deallocate_parameters_VEH;
    zmat->copy_parameters=&copy_parameters_VEH;

    zmat->behavior=&behavior_VEH;
    zmat->allocate_variables=&allocate_variables_VEH;
    zmat->extrapolate_variables=&extrapolate_variables_VEH;
    zmat->load_Cmatrix=&load_Cmatrix_VEH;
    zmat->update_Cmatrix=&update_Cmatrix_VEH;
    zmat->solve_spc0e = &solve_spc0e_VEH;
    break; /* VEH */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case LEH_MATERIAL:
    //printf("LEH_Material\n");
    zmat->typename="harmonic linear elastic"; /* LEH */
    zmat->read_parameters=&read_parameters_LEH;
    zmat->print_parameters=&print_parameters_LEH;
    zmat->allocate_parameters=&allocate_parameters_LEH;
    zmat->deallocate_parameters=&deallocate_parameters_LEH;
    zmat->copy_parameters=&copy_parameters_LEH;

    zmat->behavior=&behavior_LEH;
    zmat->allocate_variables=&allocate_variables_LEH;
    zmat->extrapolate_variables=&extrapolate_variables_LEH;
    zmat->load_Cmatrix=&load_Cmatrix_LEH;
    zmat->solve_spc0e = &solve_spc0e_LEH;
    break; /* LEH */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case OSVEH_MATERIAL:
    //printf("VEH_Material\n");
    zmat->typename="harmonic visco elastic orthotropic"; /* OSVEH */
    zmat->read_parameters=&read_parameters_OSVEH;
    zmat->print_parameters=&print_parameters_OSVEH;
    zmat->allocate_parameters=&allocate_parameters_OSVEH;
    zmat->deallocate_parameters=&deallocate_parameters_OSVEH;
    zmat->copy_parameters=&copy_parameters_OSVEH;

    zmat->behavior=&behavior_OSVEH;
    zmat->allocate_variables=&allocate_variables_OSVEH;
    zmat->extrapolate_variables=&extrapolate_variables_OSVEH;
    zmat->load_Cmatrix=&load_Cmatrix_OSVEH;
    zmat->update_Cmatrix=&update_Cmatrix_OSVEH;
    break; /* OSVEH */

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case VEHT_MATERIAL:
    //printf("VEHT_Material\n");
    zmat->typename="harmonic visco elastic with tabulated values"; /* VEHT */
    zmat->read_parameters=&read_parameters_VEHT;
    zmat->print_parameters=&print_parameters_VEHT;
    zmat->allocate_parameters=&allocate_parameters_VEHT;
    zmat->deallocate_parameters=&deallocate_parameters_VEHT;
    zmat->copy_parameters=&copy_parameters_VEHT;

    zmat->behavior=&behavior_VEHT;
    zmat->allocate_variables=&allocate_variables_VEHT;
    zmat->extrapolate_variables=&extrapolate_variables_VEHT;
    zmat->load_Cmatrix=&load_Cmatrix_VEHT;
    //zmat->update_Cmatrix=&update_Cmatrix_VEHT;
    zmat->solve_spc0e = &solve_spc0e_VEHT;
    break; /* VEHT */




    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/




    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/




    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/





    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case VOCE_MATERIAL:
    zmat->typename="Elasto-Visco-Plastic behavior Voce law"; /* VOCE */
    zmat->read_parameters=&read_parameters_VOCE;
    zmat->print_parameters=&print_parameters_VOCE;
    zmat->allocate_parameters=&allocate_parameters_VOCE;
    zmat->deallocate_parameters=&deallocate_parameters_VOCE;
    zmat->copy_parameters=&copy_parameters_VOCE;

    zmat->behavior=&behavior_VOCE;
    zmat->allocate_variables=&allocate_variables_VOCE;
    zmat->extrapolate_variables=&extrapolate_variables_VOCE;
    zmat->load_Cmatrix=&load_Cmatrix_VOCE;
    //    zmat->load_param=&load_param_VOCE;
    zmat->init_variables=&init_variables_VOCE;
    break;  /* VOCE */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case GURSON_MATERIAL:
    zmat->typename="Elasto-Visco-Plastic behavior with Gurson law"; /* GURSON */
    zmat->read_parameters=&read_parameters_GURSON;
    zmat->print_parameters=&print_parameters_GURSON;
    zmat->allocate_parameters=&allocate_parameters_GURSON;
    zmat->deallocate_parameters=&deallocate_parameters_GURSON;
    zmat->copy_parameters=&copy_parameters_GURSON;

    zmat->behavior=&behavior_GURSON;
    zmat->allocate_variables=&allocate_variables_GURSON;
        zmat->extrapolate_variables=&extrapolate_variables_GURSON;
    zmat->load_Cmatrix=&load_Cmatrix_GURSON;
    //    zmat->load_param=&load_param_GURSON;
    zmat->init_variables=&init_variables_GURSON;
    zmat->solve_spc0e = &solve_spc0e_GURSON;
    break;  /* GURSON */

/*  case GURSON_COEF_MATERIAL:
    zmat->typename="Elasto-Visco-Plastic behavior with Gurson law"; 
    zmat->read_parameters=&read_parameters_GURSON_COEF;
    zmat->print_parameters=&print_parameters_GURSON_COEF;
    zmat->allocate_parameters=&allocate_parameters_GURSON_COEF;
    zmat->deallocate_parameters=&deallocate_parameters_GURSON_COEF;
    zmat->copy_parameters=&copy_parameters_GURSON_COEF;

    zmat->behavior=&behavior_GURSON_COEF;
    zmat->allocate_variables=&allocate_variables_GURSON_COEF;
        zmat->extrapolate_variables=&extrapolate_variables_GURSON_COEF;
    zmat->load_Cmatrix=&load_Cmatrix_GURSON_COEF;
    //    zmat->load_param=&load_param_GURSON_COEF;
    zmat->init_variables=&init_variables_GURSON_COEF;
    zmat->solve_spc0e = &solve_spc0e_GURSON_COEF;
    break;*/  /* GURSON_COEF */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/




    
  default:
    status=-1;
    fprintf(stderr,"failure in Init_Material: \n   type material %d is not known.\n",zmat->type);
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  }
/*    printf("in init material end\n");
      printf("status=%i\n",status);
  FILE *fres;
    fres = fopen("monfichier_test.txt","a");
  if (status == 0) {
    printf("coucou=%i\n",status);
    zmat->print_parameters(  fres, &(zmat->parameters) , 1);
  }*/
  /*-------------------------------------------------------------------------*/
  /* one allocates memory for parameters field */
  if (status == 0) {
    zmat->allocate_parameters( &(zmat->parameters) );
  }
  //    printf("in init material parameters allocated\n");
  //    printf("status=%i\n",status);

  /*-------------------------------------------------------------------------*/
  return status;
  /*-------------------------------------------------------------------------*/
}

int solve_spc0e_notimplemented( void *param,
                                Euler_Angles orientation,
                                Variables *sv,
                                double dt,
                                LE_param *C0,
                                double (*tau)[6]){
  fprintf(stderr, "The choosen iterative scheme can currently not be used "
                  "with one of the materials\npresent in the microstructure.\n");
  return -1;
}

