#define _DEFAULT_SOURCE

#include <io_variables.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utils.h>
#include <craft.h>

#ifdef hdf5
#pragma message("Using HDF5 standard format.")
#include <hdf5.h>
char io_variables_built_with_H5_lib_vers_info_g[] = "io_variables built with " H5_VERS_INFO;
#else
#pragma message("Non-standard format for saving Variables.")
#endif

#ifndef hdf5

/**********************************************************************************************/
int write_variables_all_phases(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  char *cfn,      /* Filename where microstructure is stored */
  int *phase,     /* Array of phase ids                 */
  int *material_in_phase, /* Array of material ids      */
  int it, double time, /* Time stamp                    */
  double E_cur[6], double E_prev[6], /* Current and previous macro strain */
  int binary      /* True if arrays data is to be saved in binary form */
  ){
  int iph,ivar,j,k,Nc,Nl;
  char *fname_ext;
  FILE *f;
  char *tmp;

  /* Filename with extension */
  tmp = strstr(fname, ".dat");
  if (tmp!=NULL){
    fname_ext = fname;
  }
  else{
    fname_ext = malloc(strlen(fname)+strlen(".dat")+1);
    sprintf(fname_ext, "%s.dat", fname);
  }

  /* Opening file */
  f = fopen(fname_ext, "w");

  /* Specifying save mode for Variable values. */
  if (binary){
    fprintf(f, "BINARY\n");
  }
  else{
    fprintf(f, "ASCII\n");
  }
  /* Writing Craft version */
  fprintf(f, "Craft version %s\n", CRAFT_VERSION);

  /* Writing microstructure location */
  fprintf(f, "#Characteristic file\n");
  fprintf(f, "%s\n", cfn);

  /* Writing time stamp */
  fprintf(f, "#Time  index and time value\n");
  fprintf(f, "%d %015.8e\n", it, time);

  /* Writing current and previous macroscopic strain */
  fprintf(f, "# Current macroscopic strain\n");
  fprintf(f, "#%15s %15s %15s %15s %15s %15s\n","E11","E22","E33","E12","E13","E23");
  fprintf(f, " %15.8lg %15.8lg %15.8lg %15.8lg %15.8lg %15.8lg \n",
          E_cur[0], E_cur[1], E_cur[2], E_cur[5], E_cur[4], E_cur[3] );
  fprintf(f, "# Previous macroscopic strain\n");
  fprintf(f, "#%15s %15s %15s %15s %15s %15s\n","E11","E22","E33","E12","E13","E23");
  fprintf(f, " %15.8lg %15.8lg %15.8lg %15.8lg %15.8lg %15.8lg \n",
          E_prev[0], E_prev[1], E_prev[2], E_prev[5], E_prev[4], E_prev[3] );

  /* Writing Variables for each phase */
  for(iph=0;iph<Nph; iph++){
    fprintf(f, "\n");
    fprintf(f, "%s", Phase_header);
    fprintf(f, "%d %d %d %d %d\n",
      iph, phase[iph], material_in_phase[iph], sv[iph].np, sv[iph].nvar);
    for(ivar=0; ivar<sv[iph].nvar; ivar++){
      Variable *var = &(sv[iph].vars[ivar]);
      Nc = var->type[1];
      Nl = var->type[2];
      fprintf(f, "\n");
      fprintf(f, Var_header);
      fprintf(f, "%d \"%s\" %d %d %d\n", ivar, var->label, var->type[0], Nc,Nl);

      switch(var->type[0]){
      case SCALAR:
        if (binary){
          fwrite(var->values, sizeof(double), sv[iph].np, f);
        }
        else{
          for(j=0;j<sv[iph].np;j++){
            fprintf(f, "%-15.8lg\n", ((double*)var->values)[j]);
          }
        }
        break;
      case VECTOR3D:
        if (binary){
          fwrite(var->values, sizeof(double [3]), sv[iph].np, f);
        }
        else{
          for(j=0;j<sv[iph].np;j++){
            fprintf(f, "%-15.8lg %-15.8lg %-15.8lg\n",
              ((double (*)[3])var->values)[j][0],
              ((double (*)[3])var->values)[j][1],
              ((double (*)[3])var->values)[j][2]);
          }
        }
        break;
      case TENSOR2:
        if (binary){
          fwrite(var->values, sizeof(double [6]), sv[iph].np, f);
        }
        else{
          for(j=0;j<sv[iph].np;j++){
            fprintf(f, "%-15.8lg %-15.8lg %-15.8lg %-15.8lg %-15.8lg %-15.8lg\n",
              ((double (*)[6])var->values)[j][0],
              ((double (*)[6])var->values)[j][1],
              ((double (*)[6])var->values)[j][2],
              ((double (*)[6])var->values)[j][3],
              ((double (*)[6])var->values)[j][4],
              ((double (*)[6])var->values)[j][5]);
          }
        }
        break;
      case VECTOR:
        if (binary){
          fwrite(var->values, sizeof(double), Nc*sv[iph].np, f);
        }
        else{
          for(j=0;j<sv[iph].np;j++){
            for(k=0;k<Nc; k++){
              fprintf(f, "%-15.8lg ", ((double**)var->values)[j][k]);
            }
            fprintf(f, "\n");
          }
        }
        break;
      case MATRIX:
        /* TODO: check that array is always contiguous */
        if (binary){
          fwrite(var->values, sizeof(double), Nc*Nl*sv[iph].np, f);
        }
        else{
          for(j=0;j<sv[iph].np;j++){
            for(k=0;k<Nc*Nl; k++){
              fprintf(f, "%-15.8lg ", ((double**)var->values)[j][k]);
            }
            fprintf(f, "\n");
          }
        }
        break;
      default:
        fprintf(stderr, "Unable to write variable %s of phase %d.\n",
          var->label, iph);
      }
    }
  }
  fclose(f);
  fprintf(stderr, "State written in %s for time %f (#%d).\n",
    fname_ext, time, it);
  
  return 0;
}

#endif

int read_variables_all_phases_nonstd(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  int *it, double *time, /* Time stamp                  */
  double *E_cur, double *E_prev /* Current and previous macro strain */
  ){
  int iph,idvar,status,np,nvar,Nel,binary;
  int type[3];
  size_t nchar;
  int j,k;
  char *fname_ext;
  char label[1024];
  char *buffer=NULL;
  Variable *var;
  FILE *f;

  /* Filename with extension */
  if (strstr(fname, ".dat")==NULL){
    fname_ext = malloc(strlen(fname)+strlen(".dat"+1));
    sprintf(fname_ext, "%s.dat", fname);
  }
  else
  {
    fname_ext = fname;
  }

  /* Opening file */
  f = fopen(fname_ext, "r");
  status = getline(&buffer, &nchar, f);
  if (status==-1){
    fprintf(stderr, "Unable to determine save mode (BINARY/ASCII).\n");
    exit(0);
  }
  if (strstr(buffer, "BINARY")!=NULL){
    binary = 1;
  }
  else if  (strstr(buffer, "ASCII")!=NULL){
    binary = 0;
  }
  else{
    fprintf(stderr, "Unable to determine save mode (BINARY/ASCII).\n");
    exit(0);
  }

  /* Read Craft version used when storing */
  status = getline(&buffer, &nchar, f);
  if (strcmp(CRAFT_VERSION_BROKEN_COMPATIBILITY, buffer)>1){
    fprintf(stderr,
        "%s was written with Craft version %s, which is incompatible\n"
        "with current version (%s). Consider converting file to the new standard.\n",
        fname_ext, buffer, CRAFT_VERSION);
    exit(0);
  }
  free(buffer);
  buffer = NULL;

  /* Read microstructure location */
  status = craft_fscanf(f, "%s\n", label);
  /* No check, may fail */

  /* Read time stamp (if pointers have been provided) */
  {
    int idx;
    double value;
    status = craft_fscanf(f,"%d %lf\n", &idx, &value);
    if (status!=2){
      fprintf(stderr, "Unable to parse time information in %s.\n", fname_ext);
      exit(0);
    }
    if (it!=NULL) *it=idx;
    if (time!=NULL) *time=value;
  }

  /* Read macro strains (if pointers have been provided) */
  {
    double Ecur[6], Eprev[6];
    status = craft_fscanf(f, "  %lf %lf %lf %lf %lf %lf\n",
      &(Ecur[0]), &(Ecur[1]), &(Ecur[2]),
      &(Ecur[5]), &(Ecur[4]), &(Ecur[3]));
    if ( status!=6 ) {
      fprintf(stderr,"Unable to parse current macro strain (errno %d).\n", status);
      return -1;
    }
    if (E_cur!=NULL){
      for(j=0;j<6;j++) E_cur[j] = Ecur[j];
    }
    status = craft_fscanf(f, "  %lf %lf %lf %lf %lf %lf\n",
      &(Eprev[0]), &(Eprev[1]), &(Eprev[2]),
      &(Eprev[5]), &(Eprev[4]), &(Eprev[3]));
    if ( status!=6 ) {
      fprintf(stderr,"Unable to parse previous macro strain (errno %d).\n", status);
      return -1;
    }
    if (E_prev!=NULL){
      for(j=0;j<6;j++) E_prev[j] = Eprev[j];
    }
  }

  while(1){
    int phase, mat;
    status = getline(&buffer, &nchar, f);
    if (status==-1) return 0;
    if (strcmp(buffer, "\n")==0) continue;
    if (strcmp(buffer, Phase_header)==0) continue;

    status = sscanf(buffer, "%d %d %d %d %d\n", &iph, &phase, &mat, &np, &nvar);
    if (status!=5){
      fprintf(stderr, "Unable to parse new Variables instance in %s.\n", fname_ext);
      return -2;
    }
    /* Another phase Variables */
    /* Check number of points */
    if ( sv[iph].np != np){
      fprintf(stderr, "Number of point in phase %d does not match (%d-%d).\n",
        iph, np, sv[iph].np);
      return -3;
    };
    /* Do not check number of variables, as not all may be stored */
    idvar = 0;
    while(1){
      {
        status = getline(&buffer, &nchar, f);
        if (status==-1) break;
        if (strcmp(buffer, "\n")==0) continue;
        if (strcmp(buffer, "")==0) continue;
        if (strcmp(buffer, Var_header)!=0) break;
        status = getline(&buffer, &nchar, f);

        /* Reading idvar */
        status = sscanf(buffer, "%d", &idvar);
        if (status==-1){
          fprintf(stderr, "parsing error:\n%s\n", buffer);
          exit(0);
        }

        /* Searching variable label (may contain spaces) */
        char *sub1, *sub2;
        sub1 = sub2 = NULL;
        int lenlabel;
        sub1 = strstr(buffer, "\"");
        if (sub1==NULL){
          fprintf(stderr, "Failed to find \" in %s: %s.\n", buffer, sub1);
          exit(0);
        }
        sub1 += 1;
        sub2 = strstr(sub1, "\"");
        lenlabel = strlen(sub1)-strlen(sub2);
        strncpy(label, sub1, lenlabel);
        label[lenlabel] = '\0';

        if (strcmp(sv[iph].vars[idvar].label, &(label[0]))!=0){
          fprintf(stderr, "Variable label in phase %d does not match (%s..%s).\n",
            iph, label, sv[iph].vars[idvar].label);
          return -5;
        }

        status = sscanf(sub2+1, "%d %d %d\n",&type[0],&type[1],&type[2]);
        if (status!=3){
          fprintf(stderr, "Unable to parse new Variable (%s) type in %s.\n",
            label, fname_ext);
          return -4;
        }
      }

      if (compare_type_variable(type, sv[iph].vars[idvar].type)!=0){
        fprintf(stderr, "Variable len in phase %d does not match.\n", iph);
        return -5;
      }

      var = &sv[iph].vars[idvar];
      Nel = size_of_variable_value(type)*sizeof(double);
      if (binary){
        fread(var->values, Nel, sv[iph].np, f);
      }
      else{
        switch(type[0]) {
        case SCALAR:
          for(j=0; j<sv[iph].np; j++){
            status = craft_fscanf(f, "%lg\n", &((double *)var->values)[j]);
            if (status!=1) return -1;
          }
          break;
        case VECTOR3D:
          for(j=0; j<sv[iph].np; j++){
            status = craft_fscanf(f, "%lg %lg %lg\n",
                &((double (*)[3])var->values)[j][0],
                &((double (*)[3])var->values)[j][1],
                &((double (*)[3])var->values)[j][2]);
            if (status!=1) return -1;
          }
          break;
        case TENSOR2:
          for(j=0; j<sv[iph].np; j++){
            status = craft_fscanf(f, "%lg %lg %lg %lg %lg %lg\n",
                &((double (*)[6])var->values)[j][0],
                &((double (*)[6])var->values)[j][1],
                &((double (*)[6])var->values)[j][2],
                &((double (*)[6])var->values)[j][3],
                &((double (*)[6])var->values)[j][4],
                &((double (*)[6])var->values)[j][5]);
            if (status!=1) return -1;
          }
          break;
        case VECTOR:
          for(j=0; j<sv[iph].np; j++){
            status = getline(&buffer, &nchar, f);
            if (status==-1) return -1;
            for(k=0; k<type[1]; k++){
              status = sscanf(buffer, "%lg ", &((double **)var->values)[j][k]);
              if (status!=1) return -1;
            }
            if (craft_fscanf(f, "\n")!=1) return -1;
          }
          break;
        case MATRIX:
          for(j=0; j<sv[iph].np; j++){
            status = getline(&buffer, &nchar, f);
            if (status==-1) return -1;
            for(k=0; k<type[1]*type[2]; k++){
              status = sscanf(buffer, "%lg ", &((double **)var->values)[j][k]);
              if (status!=1) return -1;
            }
            if (craft_fscanf(f, "\n")!=1) return -1;
          }
          break;
        }
      }
    }
    if (status==-1) break;
  }
  if (buffer!=NULL) {free(buffer); buffer=NULL;}
  fprintf(stderr, "Restored values from %s.\n", fname_ext);
  return 0;
}


#ifdef hdf5
/**********************************************************************************************/
/* HM 8 mars 2013

   Determines whether the version of the library being used is greater than or equal to the specified version.
   Returns:
    -1  	If the library version is less than the version number specified
    0     	If the library version is the equal to the version number specified
    1           If the library version is greater than the version number specified

 */
/**********************************************************************************************/
int compare_version_hdf5( unsigned majnum, unsigned minnum, unsigned relnum  ){
  unsigned maj, min, rel;
  H5get_libversion( &maj, &min, &rel);


  if (maj < majnum ) return -1;
  else if (maj > majnum ) return 1;
  else{
    if(min<minnum) return -1;
    else if (min>minnum) return 1;
    else{
      if(rel<relnum) return -1;
      else if(rel>relnum) return 1;
      else return 0;
    }
  }
}
#endif


#ifdef hdf5
/**********************************************************************************************/
int write_variables_all_phases(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  char *cfn,      /* Filename where microstructure is stored */
  int *phase,     /* Array of phase ids                 */
  int *material_in_phase, /* Array of material ids      */
  int it, double time, /* Time stamp                    */
  double E_cur[6], double E_prev[6], /* Current and previous macro strain */
  int binary      /* Useless when using HDF5 format */
  ){
  int iph,ivar;
  char *fname_ext;
  hid_t h5file, h5tensor2d, h5char16, h5str, single_sp, h5def, att;
  hsize_t dims[2];

  /* Check HDF5 version (new API required), abort if  */
  //  if ( H5_VERSION_GE(1, 8, 0) <= 0 ){
  if (compare_version_hdf5(1,8,0) <= 0) {
    fprintf(stderr, "HDF5: Need to compile with HDF version 1.8 or higher.\n");
    return -1;
  }

  /* Convenience alias */
  h5def = H5P_DEFAULT;
  /* Tensor 2d type */
  dims[0] = 6; dims[1] = 0;
  h5tensor2d = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, dims);
  /* 15-length string (+term) */
  h5char16 = H5Tcopy(H5T_C_S1);
  H5Tset_size(h5char16, 16);
  /* General string */
  h5str = H5Tcopy(H5T_C_S1);
  if ( ( h5tensor2d<0 ) || ( h5char16<0 ) || ( h5str<0 ) ){
    fprintf(stderr, "HDF5: Unable to create data types.\n");
    return -1;
  }
  /* Single dataspace */
  dims[0] = 1; dims[1] = 0;
  if ( ( single_sp=H5Screate_simple(0, dims, NULL) ) < 0 ){
    fprintf(stderr, "HDF5: Unable to create dataspace.\n");
    return -1;
  }

  /* Filename with extension */
  if ( strstr(fname, ".h5") != NULL ){
    fname_ext = fname;
  }
  else{
    fname_ext = malloc(strlen(fname)+strlen(".h5")+1);
    sprintf(fname_ext, "%s.h5", fname);
  }

  /* Opening file */
  if ( ( h5file=H5Fcreate(fname_ext, H5F_ACC_TRUNC, h5def, h5def) ) < 0){
      fprintf(stderr, "HDF5: Unable to create file %s.\n", fname_ext);
      return -1;
  }

  /* Characteristic file name */
  H5Tset_size(h5str, strlen(cfn)+1);
  if ( ( (att=H5Acreate(h5file, "Characteristic file", h5str, single_sp, h5def, h5def))<0)
    || ( H5Awrite(att, h5str, cfn) < 0 )
    || ( H5Aclose(att) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to write characteristic file name.\n");
    return -1;
  }

  /* Craft version */
  if ( H5Tset_size(h5str, strlen(CRAFT_VERSION)+1) < 0 ){
    fprintf(stderr, "HDF5: Unable to write Craft version.\n");
    return -1;
  }
  if ( ( (att=H5Acreate(h5file, "CraFT version", h5str, single_sp, h5def, h5def)) < 0 )
    || ( H5Awrite(att, h5str, CRAFT_VERSION) < 0)
    || ( H5Aclose(att) < 0) ){
    fprintf(stderr, "HDF5: Unable to write Craft version.\n");
    return -1;
  }

  /* Writing time stamp */
  if ( ( (att=H5Acreate(h5file, "Time value", H5T_NATIVE_DOUBLE, single_sp, h5def, h5def)) < 0 )
    || ( H5Awrite(att, H5T_NATIVE_DOUBLE, &time) < 0 )
    || ( H5Aclose(att) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to write time label.\n");
    return -1;
  }
  if ( ( (att=H5Acreate(h5file, "Time index", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
    || ( H5Awrite(att, H5T_NATIVE_INT, &it) < 0 )
    || ( H5Aclose(att) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to write time index.\n");
    return -1;
  }

  /* Writing current and previous macroscopic strain */
  {
    hsize_t count[1] = {1};
    hsize_t offset[1] = {0};
    hid_t dset, sp, memsp;
    dims[0] = 2; dims[1] = 0;
    if ( ( (sp=H5Screate_simple(1, dims, dims)) < 0 )
      || ( (dset = H5Dcreate(h5file, "/macrostrain", h5tensor2d, sp, h5def, h5def, h5def)) < 0 )
      || ( H5Sclose(sp) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to create /macrostrain dataspace.\n");
      return -1;
    }

    /* Subspace for single macro strain */
    dims[0] = 1;; dims[1] = 0;
    if ( (memsp=H5Screate_simple(1, dims, dims)) < 0 ){
      fprintf(stderr, "HDF5: Unable to create memory space for /macrostrain.\n");
      return -1;
    }

    /* Current macro strain */
    offset[0] = 0;
    if ( ( (sp=H5Dget_space(dset)) < 0 )
      || ( H5Sselect_hyperslab(sp, H5S_SELECT_SET, offset, NULL, count, NULL) < 0 )
      || ( H5Dwrite(dset, h5tensor2d, memsp, sp, h5def, E_cur) < 0 )
      || ( H5Sclose(sp) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write current macro strain.\n");
      return -1;
    }

    /* Previous macro strain */
    offset[0] = 1;
    if ( ( (sp=H5Dget_space(dset)) < 0 )
      || ( H5Sselect_hyperslab(sp, H5S_SELECT_SET, offset, NULL, count, NULL) < 0 )
      || ( H5Dwrite(dset, h5tensor2d, memsp, sp, h5def, E_prev) < 0 )
      || ( H5Sclose(sp) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write previous macro strain.\n");
      return -1;
    }

    if ( ( H5Sclose(memsp) < 0 ) || ( H5Dclose(dset) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to close /macrostrain dataspace.\n");
      return -1;
    }
  }

  /* Number of phases */
  if ( ( (att=H5Acreate(h5file, "Number of phases", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
    || ( H5Awrite(att, H5T_NATIVE_INT, &Nph) < 0 )
    || ( H5Aclose(att) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to write number of phases.\n");
    return -1;
  }

  /* Writing Variables for each phase */
  for(iph=0;iph<Nph; iph++){
    hid_t phase_gr;
    char phase_str[20];
    /* Group of each phase */
    /* The written index is internal to craft and does not reflect phase file. */
    sprintf(phase_str, "/Phase%d", iph);

    if ( ( phase_gr=H5Gcreate(h5file, &phase_str[0], h5def, h5def, h5def) ) < 0 ){
      fprintf(stderr, "HDF5: Unable to create group for phase %d.\n", iph);
      return -1;
    }

    /* Phase Id (specified in phase file, may be different from iph) */
    if ( ( (att=H5Acreate(phase_gr, "PhaseId", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
      || ( H5Awrite(att, H5T_NATIVE_INT, &(phase[iph])) < 0 )
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write phase id in phase %d.\n", iph);
      return -1;
    }

    /* Material of phase */
    if ( ( (att=H5Acreate(phase_gr, "MaterialId", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
      || ( H5Awrite(att, H5T_NATIVE_INT, &(material_in_phase[iph])) < 0 )
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write material id in phase %d.\n", iph);
      return -1;
    }

    /* Number of points in phase */
    if ( ( (att=H5Acreate(phase_gr, "Np", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
      || ( H5Awrite(att, H5T_NATIVE_INT, &(sv[iph].np)) < 0 )
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write number of points in phase %d.\n", iph);
      return -1;
    }

    /* Number of variables in phase */
    if ( ( (att=H5Acreate(phase_gr, "Nvar", H5T_NATIVE_INT, single_sp, h5def, h5def)) < 0 )
      || ( H5Awrite(att, H5T_NATIVE_INT, &(sv[iph].nvar)) < 0 )
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to write number of variables in phase %d.\n", iph);
      return -1;
    }

    for(ivar=0; ivar<sv[iph].nvar; ivar++){
      Variable *var = &(sv[iph].vars[ivar]);
      hid_t dset, sp, type;

      dims[0] = sv[iph].np; dims[1] = 0;
      if ( (sp=H5Screate_simple(1, (const hsize_t *)&dims, NULL)) < 0 ){
        fprintf(stderr, "HDF5: Unable to create dataspace in phase %d.\n", iph);
        return -1;
      }

      switch(var->type[0]){
      case SCALAR:
        type = H5Tcopy(H5T_NATIVE_DOUBLE);
        break;
      case VECTOR3D:
        dims[0] = 3;
        type = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, dims);
        break;
      case TENSOR2:
        dims[0] = 6;
        type = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, dims);
        break;
      case VECTOR:
        dims[0] = var->type[1];
        type = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, dims);
        break;
      case MATRIX:
        dims[0] = var->type[1];
        dims[1] = var->type[2];
        type = H5Tarray_create(H5T_NATIVE_DOUBLE, 2, dims);
        break;
      default:
        fprintf(stderr, "HDF5: Unknown data type.\n");
        return -1;
      }
      if ( ( type < 0 )
        || ( (dset=H5Dcreate(phase_gr, var->label, type, sp, h5def, h5def, h5def)) < 0 )
        || ( H5Dwrite(dset, type, H5S_ALL, H5S_ALL, h5def, (double *)var->values) < 0 )
        || ( H5Tclose(type) < 0 ) ){
        fprintf(stderr, "HDF5: Unable to write variable %s of phase %d.\n", var->label, iph);
        return -1;
      }

      /* Type of variable */
      dims[0] = 3;
      if ( ( (type=H5Tarray_create(H5T_NATIVE_INT, 1, dims)) < 0 )
        || ( (att=H5Acreate(dset, "type", type, single_sp, h5def, h5def)) < 0 )
        || ( H5Awrite(att, type, &(var->type)) < 0 )
        || ( H5Aclose(att) < 0 )
        || ( H5Tclose(type) < 0 ) ){
        fprintf(stderr, "HDF5: Unable to write vartype of phase %d.\n", iph);
        return -1;
      }

      /* Name of variable */
      if ( ( H5Tset_size(h5str, strlen(var->label)+1) < 0 )
        || ( (att=H5Acreate(dset, "Name", h5str, single_sp, h5def, h5def)) < 0 )
        || ( H5Awrite(att, h5str, var->label) < 0 )
        || ( H5Aclose(att) < 0 ) ) {
        fprintf(stderr, "HDF5: Unable to write varname of phase %d.\n", iph);
        return -1;
      }

      if ( ( H5Dclose(dset) < 0 ) || ( H5Sclose(sp) < 0 ) ){
        fprintf(stderr, "HDF5: Unable to close dataset and dataspace of phase %d.\n", iph);
        return -1;
      }
    }
    if ( H5Gclose(phase_gr) < 0 ){
      fprintf(stderr, "HDF5: Unable to close group of phase %d.\n", iph);
      return -1;
    }
  }
  if ( H5Fclose(h5file) < 0 ){
    fprintf(stderr, "HDF5: Unable to close file.\n");
    return -1;
  }
  if ( ( H5Tclose(h5tensor2d) < 0 )
    || ( H5Tclose(h5char16) < 0 )
    || ( H5Tclose(h5str) < 0 )
    || ( H5Sclose(single_sp) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to close data types.\n");
  }

  fprintf(stderr, "HDF5: State written in %s for time %f (#%d).\n", fname_ext, time, it);
  if (fname!=fname_ext) free(fname_ext);
  return 0;
}

/**********************************************************************************************/
/* Convenience function to retrieve a field stored in HDF5 file */
int get_variable_value_hdf5(
        hid_t h5container,  /* In: HDF5 object containing data    */
        Variable *var,      /* InOut: Variable to fill            */
        char *groupname
        ){
  int dtype[3];
  hid_t dset, h5def, h5type, att;
  hsize_t h5dims[2];
  /* Convenience alias */
  h5def = H5P_DEFAULT;

  /* Open dataset if possible */
  if ( ( H5Lexists(h5container, var->label, h5def) <= 0 )
    || ( (dset=H5Dopen(h5container, var->label, h5def)) < 0 ) ){
    fprintf(stderr, "Dataset \"%s\" seems to be absent in %s.\n", var->label, groupname+1);
    return -1;
  }

  /* Get and check datatype */
  h5dims[0] = 3; h5dims[1] = 0;
  if ( ( (h5type=H5Tarray_create(H5T_NATIVE_INT, 1, h5dims)) < 0 )
    || ( H5Aexists(dset, "type") <= 0)
    || ( (att=H5Aopen(dset, "type", h5def)) < 0 )
    || ( H5Aread(att, h5type, (int *)dtype) < 0 )
    || ( H5Aclose(att) < 0 )
    || ( H5Tclose(h5type) < 0 ) ){
    fprintf(stderr, "HDF5: Unable to read type of %s in %s.\n", var->label, groupname+1);
    return -1;
  }
  if ( compare_type_variable(var->type, dtype)!=0 ){
    fprintf(stderr, "Types do not match (%d,%d,%d)-(%d,%d,%d).\n",
      var->type[0], var->type[1], var->type[2],
      dtype[0], dtype[1], dtype[2]);
    return -1;
  }

  /* Retrieve data */
  switch(var->type[0]){
  case SCALAR:
    if ( H5Dread(dset,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,h5def,var->values) < 0 ){
      fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
      return -1;
    }
    break;
  case VECTOR3D:
    h5dims[0] = 3; h5dims[1] = 0;
    if ( ( (h5type=H5Tarray_create(H5T_NATIVE_DOUBLE, 1, h5dims)) < 0 )
      || ( H5Dread(dset,h5type,H5S_ALL,H5S_ALL,h5def,var->values) < 0 )
      || ( H5Tclose(h5type) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
      return -1;
    }
    break;
  case TENSOR2:
    h5dims[0] = 6; h5dims[1] = 0;
    if ( ( (h5type=H5Tarray_create(H5T_NATIVE_DOUBLE, 1, h5dims)) < 0 )
      || ( H5Dread(dset,h5type,H5S_ALL,H5S_ALL,h5def,var->values) < 0 )
      || ( H5Tclose(h5type) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
      return -1;
    }
    break;
  case VECTOR:
    h5dims[0] = var->type[1]; h5dims[1] = 0;
    if ( ( (h5type=H5Tarray_create(H5T_NATIVE_DOUBLE, 1, h5dims)) < 0 )
      || ( H5Dread(dset,h5type,H5S_ALL,H5S_ALL,h5def,var->values) < 0 )
      || ( H5Tclose(h5type) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
      return -1;
    }
    break;
  case MATRIX:
    h5dims[0] = var->type[1]; h5dims[1] = var->type[2];
    if ( ( (h5type=H5Tarray_create(H5T_NATIVE_DOUBLE, 2, h5dims)) < 0 )
      || ( H5Dread(dset,h5type,H5S_ALL,H5S_ALL,h5def,var->values) < 0 )
      || ( H5Tclose(h5type) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
      return -1;
    }
    break;
  default:
    fprintf(stderr, "HDF5: Unable to read data of %s in %s.\n", var->label, groupname+1);
    return -1;
    break;
  }
  if ( H5Dclose(dset) < 0 ){
    fprintf(stderr, "HDF5: Unable to close dataset of %s in %s.\n", var->label, groupname+1);
    return -1;
  }
  return 0;
}

/**********************************************************************************************/
int read_variables_all_phases_hdf5(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  int *it, double *time, /* Time stamp                  */
  double *E_cur, double *E_prev /* Current and previous macro strain */
  ){
  int iph,status;
  char *fname_ext;
  hid_t h5file, h5tensor2d, h5char16, h5str, single_sp, h5def, att;
  hsize_t h5dims[2];


  /* Check HDF5 version (new API required), abort if  */
  //  if ( H5_VERSION_GE(1, 8, 0) <= 0 ){
  if (compare_version_hdf5(1,8,0) <= 0) {
    fprintf(stderr, "HDF5: Need to compile with HDF version 1.8 or higher.\n");
    return -1;
  }

  /* Convenience alias */
  h5def = H5P_DEFAULT;
  /* Tensor 2d type */
  h5dims[0] = 6; h5dims[1] = 0;
  h5tensor2d = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, h5dims);
  /* 15-length string (+term) */
  h5char16 = H5Tcopy(H5T_C_S1);
  H5Tset_size(h5char16, 16);
  /* General string */
  h5str = H5Tcopy(H5T_C_S1);
  if ( ( h5tensor2d<0 ) || ( h5char16<0 ) || ( h5str<0 ) ){
    fprintf(stderr, "HDF5: Unable to create data types.\n");
    return -1;
  }

  /* Single dataspace */
  h5dims[0] = 1; h5dims[1] = 0;
  if ( ( single_sp=H5Screate_simple(0, h5dims, NULL) ) < 0 ){
    fprintf(stderr, "HDF5: Unable to create dataspace.\n");
    return -1;
  }

  /* Filename with extension */
  if ( strstr(fname, ".h5") == NULL ){
    fname_ext = malloc(strlen(fname)+strlen(".h5"+1));
    sprintf(fname_ext, "%s.h5", fname);
  }
  else
  {
    fname_ext = fname;
  }

  /* Opening file */
  if ( (h5file=H5Fopen( fname_ext, H5F_ACC_RDONLY, h5def)) < 0 ){
    fprintf(stderr, "HDF5: File \"%s\" can not be opened by HDF5.\n", fname_ext);
    return -1;
  }

  /* Read time stamps (if pointers has been provided) */
  if ( (it!=NULL) && (time!=NULL) ) {
    /* Time step index */
    if ( ( H5Aexists(h5file, "Time index") <= 0 )
      || ( (att=H5Aopen(h5file, "Time index", h5def)) < 0 )
      || ( H5Aread(att, H5T_NATIVE_INT, it) < 0)
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse time index.\n");
      return -1;
    }

    /* Time label */
    if ( ( H5Aexists(h5file, "Time value") <= 0 )
      || ( (att=H5Aopen(h5file, "Time value", h5def)) < 0 )
      || ( H5Aread(att, H5T_NATIVE_DOUBLE, time) < 0)
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse time value.\n");
      return -1;
    }
  }

  /* Read macro strains (if pointers has been provided) */
  if ( (E_cur!=NULL) && (E_prev!=NULL) ){
    hsize_t count;
    hsize_t offset[1];
    hid_t dset, sp;
    if ( ( H5Lexists(h5file, "/macrostrain", h5def) <= 0 )
      || ( (dset=H5Dopen(h5file, "/macrostrain", h5def)) < 0 ) ){
      fprintf(stderr, "HDF5: Dataset \"/macrostrain\" seems to be absent.\n");
      return -1;
    }

    count = 1;
    if ( (sp=H5Dget_space(dset)) < 0 ){
      fprintf(stderr, "HDF5: Unable to open \"/macrostrain\".\n");
      return -1;
    }

    /* Current macro strain */
    offset[0] = 0;
    if ( ( H5Sselect_hyperslab(sp, H5S_SELECT_SET, offset, NULL, &count, NULL) < 0 )
      || ( H5Dread(dset, h5tensor2d, single_sp, sp, h5def, E_cur) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse current macro strain.\n");
      return -1;
    }

    /* Previous macro strain */
    offset[0] = 1;
    if ( ( H5Sselect_hyperslab(sp, H5S_SELECT_SET, offset, NULL, &count, NULL) < 0 )
      || ( H5Dread(dset, h5tensor2d, single_sp, sp, h5def, E_prev) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse previous macro strain.\n");
      return -1;
    }

    if ( ( H5Sclose(sp) < 0 ) || ( H5Dclose(dset) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to close /macrostrain dataset.\n");
      return -1;
    }
  }

  for(iph=0;iph<Nph;iph++){
    hid_t phase_gr;
    char phase_str[20];
    int np, nvar,ivar;

    sprintf(phase_str, "/Phase%d", iph);
    if ( ( H5Lexists(h5file, (char *)phase_str, h5def) <= 0 )
      || ( (phase_gr=H5Gopen(h5file, (char *)phase_str, h5def)) < 0) ){
      continue;
    }

    /* Number of points in phase */
    if ( ( H5Aexists(phase_gr, "Np") <= 0 )
      || ( (att=H5Aopen(phase_gr, "Np", h5def)) < 0 )
      || ( H5Aread(att, H5T_NATIVE_INT, &np) < 0)
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse number of points in phase %d.\n", iph);
      return -1;
    }
    if ( np != sv[iph].np ){
      fprintf(stderr, "HDF5: Number of points in phase %d does not match (%d!=%d).\n",
          iph, sv[iph].np, np);
      /* Do not return, in order to get a full list of lacking variables */
      /*return -1;*/
    }



    /* Number of variables in phase */
    if ( ( H5Aexists(phase_gr, "Nvar") <= 0 )
      || ( (att=H5Aopen(phase_gr, "Nvar", h5def)) < 0 )
      || ( H5Aread(att, H5T_NATIVE_INT, &nvar) < 0)
      || ( H5Aclose(att) < 0 ) ){
      fprintf(stderr, "HDF5: Unable to parse number of variables in phase %d.\n", iph);
      return -1;
    }
    if ( nvar != sv[iph].nvar ){
      fprintf(stderr, "HDF5: Number of variables in phase %d does not match (%d!=%d).\n",
          iph, sv[iph].np, np);
      /* Do not return, in order to get a full list of lacking variables */
      /*return -1;*/
    }

    for(ivar=0; ivar<sv[iph].nvar; ivar++){
      status = get_variable_value_hdf5(phase_gr, &(sv[iph].vars[ivar]), &phase_str[0]);
      if ( status < 0 ){
        fprintf(stderr, "HDF5: Failed to get field %s in phase %d.\n",
          sv[iph].vars[ivar].label, iph);
        return -1;
      }
    }
  }
  fprintf(stderr, "HDF5: Restored values from %s.\n", fname_ext);
  return 0;
}
#endif

/**********************************************************************************************/
int read_variables_all_phases(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  int *it, double *time, /* Time stamp                  */
  double *E_cur, double *E_prev /* Current and previous macro strain */
  ){
  int status;

  if (strstr(fname, ".dat")!=NULL){
    status = read_variables_all_phases_nonstd(Nph, sv, fname, it, time, E_cur, E_prev);
  }
  else if (strstr(fname, "h5")!=NULL){
#ifdef hdf5
    status = read_variables_all_phases_hdf5(Nph, sv, fname, it, time, E_cur, E_prev);
#else
    fprintf(stderr, "You need to compile with -Dhdf5 (or set STD_FORMAT=yes in options.in).\n");
    exit(0);
#endif
  }
  else{
#ifdef hdf5
    status = read_variables_all_phases_hdf5(Nph, sv, fname, it, time, E_cur, E_prev);
    if (status==0) return 0;
#endif
    /* fallback if read_variables_all_phases_hdf5 fails or HDF5 undefined */
    /* in case of filename without extension */
    status = read_variables_all_phases_nonstd(Nph, sv, fname, it, time, E_cur, E_prev);
  }
  return status;
}
