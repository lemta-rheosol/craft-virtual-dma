#ifndef __IO_VARIABLES__
#define __IO_VARIABLES__

#include <variables.h>

/* Defines a critical version where compatibility in Variables IO is not garanteed. */
#define CRAFT_VERSION_BROKEN_COMPATIBILITY "1.0.12"

/* Strings needed in io_variables.c and variables2image.c */
#define Phase_header "#iph, Phase id, Phase material id, #points, #variables\n"
#define Var_header "# Variable #, label, type[3]\n"

/* Save Variables of all phases at once */
int write_variables_all_phases(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  char *cfn,      /* Filename where microstructure is stored */
  int *phase,     /* Array of phase ids                 */
  int *material_in_phase, /* Array of material ids      */
  int it, double time, /* Time stamp                    */
  double E_cur[6], double E_prev[6], /* Current and previous macro strain */
  int binary      /* True if arrays data is to be saved in binary form */
  );

/* Parse (partially) saved Variables */
int read_variables_all_phases(
  int Nph,        /* Number of phases                   */
  Variables *sv,  /* Array of Variables (one per phase) */
  char *fname,    /* Filename where data is stored      */
  int *it, double *time, /* Time stamp                    */
  double E_cur[6], double E_prev[6] /* Current and previous macro strain */
  );
#endif
