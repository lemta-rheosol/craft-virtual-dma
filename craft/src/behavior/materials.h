/**************************************************************************/
/*
  Hervé Moulinec
  LMA/CNRS

  september 16th 2009

  definition of the different material behaviors

*/
/**************************************************************************/
#ifndef __MATERIALS__
#define __MATERIALS__

#include <stdio.h>
#include <euler.h>
#include <variables.h>

#include <LE_materials.h>

#define UNKNOWN_MAT -1            /* unknown behavior */

/**************************************************************************/
typedef struct {
  int type;                   /* behavior type */
  char *typename;             /* name of the type of behavior:
                                 "elastic", "elastic-perfectly-plastic", ...
                                 it should be what is defined by type field */
  char *name;                 /* name of the material: it can be whatever you want:
                                 "Al", "rubber", ...*/
  void *parameters;           /* pointer to the structure of parameters describing
                                 the behavior of the material */

  /* pointer to function applying behavior */
  int (*behavior)( void *param, Euler_Angles orientation, Variables *sv, double dt);

  /* pointer to function reading the parameters of the material */
  int (*read_parameters)( FILE *f, void *param);

  /* pointer to function printing the parameters of the material */
  int (*print_parameters)( FILE *f, void *param, int flag);

  /* pointer to function allocating memory for parameters field */
  int (*allocate_parameters)(void **param);

  /* pointer to function deallocating memory for parameters field */
  int (*deallocate_parameters)(void *param);

  /* pointer to function copying paramaters param_src to param_dest */
  int (*copy_parameters)( void *param_dest, void *param_src);

  /* pointer to function allocating / deallocating variables */
  int (*allocate_variables)( int N, Variables *sv, void *param);
  int (*deallocate_variables)( Variables *sv );
  /*  remark: one needs param as it can contain information useful for state_variables allocation
      (exemple: number of slip systems in the case of polycrystalline material) */

  /* pointer to function initializing variables */
  int  (*init_variables)( Variables *sv, void *param );

  /* "extrapolation" of variables when going from time t2 to time t3
      (t1 is the time before t2) */
  int (*extrapolate_variables)(void *param, Euler_Angles orientation, Variables *sv, double t1, double t2, double t3);

  /* pointer to function loading C matrix */
  int  (*load_Cmatrix)( void *param, double C[6][6]);

  /* pointer to function loading C matrix */
  int  (*update_Cmatrix)( void *param, double C[6][6], double omega);

  int  (*load_param)( int N, void *param, Variables *sv, double load_sgl[12][6], 
		      double load_n[12], double load_Xp[N][12],
		      double load_tau0p[N][12]);

  /* pointer to function returning a given variable (or NULL) */
  Variable *(*ptr_variable)( char *variable_name, Variables *sv, void *param);

  /* function which computes sigma and epsilon verifying:
     sigma + C0:epsilon = tau
  */
  int (*solve_spc0e)( void *param, Euler_Angles orientation, Variables *sv, double dt,
		   LE_param *C0, 
		   double (*tau)[6]
		   );

} Material;
/**************************************************************************/

int Init_Material( int type , Material *zmat );

/* Copy data organized in phase (and provided through Variables instances) 
   into data organized in image.
   See also phase_to_image. */
int phase_to_image_variables( int Nph, int *index[Nph], Variables *sv, char *varname, 
			      Material *material, CraftImage *store_image);

/* added for harmonic implementation 2017/07/05  J. Boisse */
int phase_to_image_harmonic_variables( int Nph, int *index[Nph], Variables *sv, char *varname, 
			      Material *material, CraftImage *store_image);

#endif
