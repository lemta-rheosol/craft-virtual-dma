#include <stdio.h>
#include <string.h>

#include <craft.h>
#include <materials.h>
#include <utils.h>
/************************************************************************/
/* Hervé Moulinec
   CNRS/LMA
   september 16th 2009

   reads the file which describes the behavior of the materials.
   This file is organized as follows:
   Several material can be described in this file, one following eachother.
   For a given material:
   - in the first line:
       index of the material, behavior type (as defined in materials.h)
   - in the following lines: the different parameters required for the
     behavior, one parameter per line.

   Example:
     The file containing:

       #-------
       2 1
       12.
       0.2
       #-------
       6 2
       20.
       0.25
       5.

     means:
       material number 2 has an isotropic linear elastic behavior (behavior=1)
       its Young's modulus = 12.GPa
       its Poisson coefficient = 0.2

       material number 6 has an elastic perfectly pastic behavior
       (behavior 2),
       Young's modulus=20. GPa
       Poisson coeffcicient=0.25
       yoeld stress=5. GPa


  

   input:
   char *filename : the name of the file describing the phases
   int Nph : total number of phases
   int material_in_phase[Nph] : material_in_phase[iph] gives the index of the material of phase iph

   output: 
   Material material[Nph] : material[iph] will describe behavior of material of phase iph
                            (material properties, constitutive law, ...)

   The material properties pointed by "parameters" field are specific to each 
   material[] : even if the material ids (given by "filename" file) of two different phases
   are the same, the "parameters" fields are different (it's a copy of memory pointer
   by parameter, not an assignment).
  
   return value:
   0  : successful return
   -1 : file does not exist 
   -2 : error while reading file
   -3 : unknown behavior type
   -4 : materials not well described

*/     
/************************************************************************/
int read_material_parameters( 
			     char *filename,		
			     int Nph ,			
			     int material_in_phase[Nph] ,		
			     Material material[Nph]) {
  
  int iph;
  FILE *f;
  char buffer[1024];
  int n;

  int mat,b;

  int status;



  Material zmat;
  /*--------------------------------------------------------------------*/
  status=0;
  /*--------------------------------------------------------------------*/
  /* opens the file                                                     */

  f = fopen(filename,"r");

  if ( f == (FILE *)NULL ) {
    fprintf(stderr,"error in %s\n",__func__);
    fprintf(stderr,"problems encountered while opening %s\n",filename);
    return -1;
  }
  
  
  /*--------------------------------------------------------------------*/
  for(iph=0;iph<Nph;iph++) {
    material[iph].type=UNKNOWN_MAT;
    material[iph].parameters=(void *)NULL;
  }
  /*--------------------------------------------------------------------*/
  while (1) {
    

    /* first line should give an index of material and a behavior type  */
    /*
    n=craft_fscanf(f,"%d %d ",&mat,&b);
    if (n==EOF) break;
    */
    n=craft_fscanf(f,"%d",&mat);
    if (n==EOF) break;
    n+=craft_fscanf(f,"%d",&b);

    if(n!=2) {
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"read error while reading %s : incorrect format \n",filename);
      fprintf(stderr,"  (line contains: %s)\n",buffer);
      status =-2;    
    }
    

    /* "construction" of Material structure zmat with is proper type*/
    status=Init_Material( b, &zmat );
    if (status!=0) {
      fprintf(stderr,"read_material: a problem occurred while initializing material.\n");
      status=-5;
      return status;
    }


    /* one reads material parameters in file f */
    //    zmat.allocate_parameters( &zmat.parameters );
    status=zmat.read_parameters(f, zmat.parameters );
    if (status != 0 ) {
      fprintf(stderr,"error %d in %s\n",status,__func__);
      fprintf(stderr,"  unknown behavior type\n");
      status = -3;
      return status;
    }

    /* does this material appear in the micostructure? */
    for(iph=0; iph<Nph; iph++) {
      /* yes! => one constructs material[iph] with what has been found in the file  */      
      if( material_in_phase[iph]==mat) {
	/* one tests if this material has already been specified. If yes : error    */
	if (material[iph].type != UNKNOWN_MAT ) {
	  fprintf(stderr,"error in %s\n",__func__);
	  fprintf(stderr,"   material %d has been specified more than once.\n",
		  material_in_phase[iph]);
	  status = -4;
	  return status;
	}
	status=Init_Material(zmat.type,&material[iph]);
	if (status!=0) {
	  fprintf(stderr,"read_material: a problem occurred while initializing material.\n");
	  status=-5;
	  return status;
	}

	/* copies memory area pointed to by zmat.parameters to the buffer pointed to by 
	   material[iph].parameters */
	material[iph].copy_parameters( material[iph].parameters , zmat.parameters );
      }
    }

    zmat.deallocate_parameters(zmat.parameters);


  }

  /*--------------------------------------------------------------------*/
  /* close file                                                         */
  /*--------------------------------------------------------------------*/
  fclose(f);

  /*--------------------------------------------------------------------*/
  /* one verifies that every phase has been described                   */
  /*--------------------------------------------------------------------*/
  for(iph=0; iph<Nph; iph++) {
    if (material[iph].type == UNKNOWN_MAT) {
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"   material %d has not been described in %s\n",
	      material_in_phase[iph], filename);
      status = -4;
    }
  }      
  /*--------------------------------------------------------------------*/
  return status;
  /*--------------------------------------------------------------------*/
  
  
}


