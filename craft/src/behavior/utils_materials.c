#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <complex.h>
#include <utils.h>
#include <utils_materials.h>

#ifdef _OPENMP
#include <omp.h>
#endif


/*######################################################################*/
/* G. Boittin
   LMA/CNRS
   March 8th 2017
   
   Muller and Newton methods to find zero of function
*/
/*######################################################################*/

/*######################################################################*/

int muller(double *x, mon_pointeur_fonction f, 
                 double *paramg, double h, double eps){

      int stat=0;
      double v_ini=*x;
       //      fprintf(stderr,"dans Muller ini: v_ini=%g, h=%g, x=%g, f(xr)=%g\n",v_ini,h,*x,f(*x,paramg));

      if (h<0.0) h=-h;
      if (fabs(h)<TINY) {
           double f2=f(*x,paramg);
	   if (fabs(f2)<eps){
	     return stat;
	   }
	   else {
             stat=-1;
             fprintf(stderr,"dans Muller: v_ini=%g, h=%g, xr=%g, f(xr)=%g\n",v_ini,h,*x,f2);
	     return stat;	     
	   }
      }
      else {
        double x1,x2,x3;
        double f1,f2,f3,fr;
        double h1,h2,g;
        double a,b,c,dis2;
        double dis;
        int iter,maxiter;
        maxiter=20000; 
        iter=0; 
	      
	double xrc;
        xrc=*x;
        x1 = xrc-h;
        x2 = xrc;
        x3 = xrc+h;
        f1 = f(x1,paramg);
        f2 = f(x2,paramg);
        f3 = f(x3,paramg);
        while (iter<maxiter){
         iter = iter+1;
         h1   = x2-x1;
         h2   = x3-x2;
         double test=fabs(h1/x2); if (h1>h2) test=fabs(h2/x2);
         if (test<(TINY)){
	   if (fabs(f2)<1.0e-5){
             *x=x2;
           }
           else{ 
             *x=xrc;
             stat=4;
             fprintf(stderr,"muller : stat=%i\n",stat);
             fprintf(stderr,"x=%g\n",*x);
             fprintf(stderr,"fonction muller_complexe did not converged");
             fprintf(stderr,": x3=x2 or x1=x2,TINY=%g\n",TINY);
             fprintf(stderr,"(x1)=%g\n",x1);
             fprintf(stderr,"(x2)=%g\n",(x2));
             fprintf(stderr,"(x3)=%g\n",(x3));
             fprintf(stderr,"f(x1)=%g\n",(f1));
             fprintf(stderr,"f(x2)=%g\n",(f2));
             fprintf(stderr,"f(x3)=%g\n",(f3));
             fprintf(stderr,"iter=%i\n",iter);
	   }
	
           return stat;
         }
         g    = h1/h2;
//        L(x) = a(x-x2)**2+b*(x-x2)+c
         a    = (f3*g-f2*(1+g)+f1)/(h1*(h1+h2));
         b    = (f3-f2-a*h2*h2)/h2;
         c    = f2;
         dis2=b*b-4*a*c;
         dis  = sqrt(dis2);
         if ((dis2<0.0)){
             fprintf(stderr,"Muller method uses complex numbers at point.\n");
             fprintf(stderr, "iter=%i\n",iter);
             fprintf(stderr,"iter=%i\n",iter);
             fprintf(stderr,"(x1)=%g\n",(x1));
             fprintf(stderr,"(x2)=%g\n",(x2));
             fprintf(stderr,"(x3)=%g\n",(x3));
             fprintf(stderr,"f(x1)=%g\n",(f1));
             fprintf(stderr,"f(x2)=%g\n",(f2));
             fprintf(stderr,"f(x3)=%g\n",(f3));
	     if (fabs(f2)<1.0e-5){
	      *x=x2;
              return stat;
	     }
             stat=1;
             *x=x1;
             if (f3<f1){stat=2;
             *x=x3;}
             return stat;
//           Ensures the greatest denominator |b+dis| (modulus)
//             if (b <0.0) {dis = -dis;}
         }
         else{
//           Ensures the greatest denominator |b+dis| (modulus)
             if (b <0.0) {dis = -dis;}
         }
         h = -2.0*c/(b+dis);


//        Root of the parabola going through (x1,f1), (x2,f2), (x3,f3)
//        (the closest to x2).
         xrc   = x2+h;
         fr   = f((xrc),paramg);
         test=fabs(fr);
//         printf("test : =%g\n",test);
         if (test<eps) {
//            printf("Muller method converged in %i iterations.\n",iter);
//            printf("Last evaluation: xrc=%g and fr=%g\n",xrc,fr);
//            printf("eps=%g:\n",eps);
            test=fabs(xrc);
            if (test<eps) fprintf(stderr,"Muller method converged to 0.0\n");
            *x=xrc;
            return stat;
         }
//        Ensuring new 3-tuple is sorted and composed by the parabola root and
//        the two closest previous points.
         if (h >0.0) {
//           Old x1 discarded
            x1 = x2;
            f1 = f2;
            if (h>(h2)) {
               x2 = x3;
               f2 = f3;
               x3 = xrc;
               f3 = fr;
            }
            else {
               x2 = xrc;
               f2 = fr;
            }
         }
         else {
//           Old x3 discarded
            x3 = x2;
            f3 = f2;
            if (-h>(h1)) {
               x2 = x1;
               f2 = f1;
               x1 = xrc;
               f1 = fr;
            }
            else {
               x2 = xrc;
               f2 = fr;
            }
         }
         if ((isnan(x2))||(x3 !=x3)||(x1!=x1)){
             fprintf(stderr," muller complexe did not converged, iter=%i\n",iter);
             fprintf(stderr,"(x1)=%g\n",(x1));
             fprintf(stderr,"(x2)=%g\n",(x2));
             fprintf(stderr,"(x3)=%g\n",(x3));
             fprintf(stderr,"f(x1)=%g\n",(f1));
             fprintf(stderr,"f(x2)=%g\n",(f2));
             fprintf(stderr,"f(x3)=%g\n",(f3));
             *x=xrc;
             stat=5;
             return stat;
	 }
      }//end while

      fprintf(stderr," muller complexe did not converged, iter> %i\n",maxiter);
      fprintf(stderr,"Last evaluation: xrc=%g and fr=%g\n",(xrc),(fr));

/*            printf("param[10]=%g\n",paramg[10]);
           printf("param[11]=%g\n",paramg[11]);
           printf("param[12]=%g\n",paramg[12]);*/
             fprintf(stderr,"(x1)=%g\n",(x1));
             fprintf(stderr,"(x2)=%g\n",(x2));
             fprintf(stderr,"(x3)=%g\n",(x3));
             fprintf(stderr,"f(x1)=%g\n",(f1));
             fprintf(stderr,"f(x2)=%g\n",(f2));
             fprintf(stderr,"f(x3)=%g\n",(f3));
      *x=xrc;
      stat=5;
      return stat;
   }	

}


//*********************************************************************************
//dichotomy adapted for convex function (Gurson for example)

   int dicho(double *x, double h, mon_pointeur_fonction f,
                 double* paramg, double eps){



      double x1,x2,f1,f2,xmilieu,fmilieu; 
      int iter,maxiter;
      maxiter=1000;  
      iter=0;
      x1=*x;
      x2=*x+h;
      f1=f(x1,paramg);
      f2=f(x2,paramg);
      x1=*x;
      int signe=0;
      int stat=0;
      if (f2<0) {
        fprintf(stderr,"dicho_gurson bad initialised\n");
        fprintf(stderr,"x=%g, h=%g, f(x2)=%gand f(x2+1)=%g\n",*x,h,f1,f2);
        return -1;
      }

      while (iter<maxiter){

        xmilieu=(x1+x2)/2.0;
        fmilieu=f(xmilieu,paramg);
        if (fmilieu<eps){
          *x=xmilieu;
          return stat;
        }
        if ((fmilieu<f1)){  
            x1=xmilieu;
            f1=fmilieu;
            signe=1;
        }
        else {
          if (signe==0){
            f2=fmilieu;
            x2=xmilieu;
          }
          if (signe==1){
            if (fmilieu>0.0){
              f2=fmilieu;
              x2=xmilieu;
            }
            if (fmilieu<0.0){
              f1=fmilieu;
              x1=xmilieu;
            }
          }
        }
      }
      fprintf(stderr,"dicho did not converged, iter >%i\n",maxiter);
      stat=1;
      return stat;

   }


//*********************************************************************************
//newton

   int newton(double *xr, mon_pointeur_fonction f, mon_pointeur_fonction df,
                 double* paramg, double eps){

      double fr,frprime; 
      int iter,maxiter;
      maxiter=10000;  
      iter=0;
      int stat=0;
      while (iter<maxiter){
        fr=f(*xr,paramg);
        if (fr<eps){
          return stat;
        }
        frprime=df(*xr,paramg);
        *xr=*xr-fr/frprime;
      }
      fprintf(stderr,"newton did not converged, iter >%i\n",maxiter);
      stat=-1;
      return stat;

   }

//*********************************************************************************
//newton system of two equations, yr>0

   int newton_2eq(double *xr,double *yr, mon_pointeur_fonction_2var f1, mon_pointeur_fonction_2var df11,
                 mon_pointeur_fonction_2var f2, mon_pointeur_fonction_2var df22, mon_pointeur_fonction_2var df12, mon_pointeur_fonction_2var df21,
                 double *paramf1,double eps){


      double fr,f1r,df11r,f2r,df22r,df12r,df21r,xr_ini,yr_ini;
      xr_ini=*xr;
      yr_ini=*yr;
      int iter,maxiter;
      maxiter=20000000;  
      iter=0;
      int status=0;
      if ((yr_ini>1.0)||(yr_ini<0.0)) printf("yr is a porosity!!! y_r_ini=%g\n",yr_ini);

      while ((iter<maxiter)&&(*xr==*xr)&&(*yr==*yr)){
        f1r=f1(*xr,*yr,paramf1);
        f2r=f2(*xr,*yr,paramf1);
        fr=fabs(f1r);
        if ((fabs(f2r))>fr) fr=fabs(f2r);
        if ((fr<eps)&&(*yr>0.0)){
          return status;
        }
        if (*yr<0.0){
          fprintf(stderr,"porosity=0, yr_ini=%g, yr=%g, xr_ini=%g, xr=%g\n",yr_ini,*yr,xr_ini,*xr);
          *xr=xr_ini;*yr=0.0;status=-4;
          return status;
        }
        df11r=df11(*xr,*yr,paramf1);
        df22r=df22(*xr,*yr,paramf1);
        df12r=df12(*xr,*yr,paramf1);
        df21r=df21(*xr,*yr,paramf1);

        double yr1=(f2r*df11r-f1r*df21r)/(df21r*df12r-df11r*df22r);
        double xr1=(-f1r-df12r*yr1)/df11r;

        iter++;
        *xr=*xr+xr1;
        *yr=*yr+yr1;
        //printf("yr_new=%g,xr_new=%g, f1=%g, f2=%g\n",*yr,*xr,f1r,f2r);
        if (*yr>0.995){
          fprintf(stderr,"porosity=1, yr_ini=%g, yr=%g, xr_ini=%g, xr=%g\n",yr_ini,*yr,xr_ini,*xr);
          *yr=0.995;status=0;
          return status;
        }
   //     printf("xr=%g,yr=%g,f1r=%g,f2r=%g\n",xr,yr,f1r,f2r);
      }
      if ((*xr==*xr)&&(*yr==*yr)) {status=-1;fprintf(stderr,"Newton did not converged, iter >%i\n",maxiter);}
      else {status=-2;fprintf(stderr,"A parameter is not a number in Newton, i=%i\n",iter);}
      
      fprintf(stderr,"iter=%i,xr=%g,yr=%g,f1r=%gf2r=%g\n",iter,*xr,*yr,f1r,f2r);
      fprintf(stderr,"xr_ini=%g,yr_ini=%g,f1r_ini=%g,f2r_ini=%g\n",xr_ini,yr_ini,f1(xr_ini,yr_ini,paramf1),f2(xr_ini,yr_ini,paramf1));
      
      return status;
   }



//*********************************************************************************
//newton system of three equations zr<0

   int newton_3eq(double *xr,double *yr,double *zr, mon_pointeur_fonction_3var f1,  mon_pointeur_fonction_3var f2, mon_pointeur_fonction_3var f3, 
                 mon_pointeur_fonction_3var df11, mon_pointeur_fonction_3var df12, mon_pointeur_fonction_3var df13, 
                 mon_pointeur_fonction_3var df21, mon_pointeur_fonction_3var df22,mon_pointeur_fonction_3var df23,
                 mon_pointeur_fonction_3var df31, mon_pointeur_fonction_3var df32, mon_pointeur_fonction_3var df33,
                 double* paramf1,double eps){



      double fr,f1r,f2r,f3r,df11r,df12r,df13r,df21r,df22r,df23r,df31r,df32r,df33r,xr_ini,yr_ini,zr_ini;
      xr_ini=*xr;
      yr_ini=*yr;
      zr_ini=*zr;
      int iter,maxiter;
      maxiter=20000000;  
      iter=0;
      int status=0;

      while ((iter<maxiter)&&(*xr==*xr)&&(*yr==*yr)&&(*zr==*zr)){
        f1r=f1(*xr,*yr,*zr,paramf1);
        f2r=f2(*xr,*yr,*zr,paramf1);
        f3r=f2(*xr,*yr,*zr,paramf1);
  //      printf("xr=%g,yr=%g,f1r=%gf2r=%g\n",xr,yr,f1r,f2r);
        fr=fabs(f1r);
        if ((fabs(f2r))>fr) fr=fabs(f2r);
        if ((fabs(f3r))>fr) fr=fabs(f3r);
        if ((fr<eps)&&(*yr>0.0)){
          return status;
        }
        if (*zr<0.0){
          fprintf(stderr,"porosity=0, yr_ini=%g, yr=%g, xr_ini=%g, xr=%g, zr=%g\n",yr_ini,*yr,xr_ini,*xr,*zr);
          status=-4;
          return status;
        }
        df11r=df11(*xr,*yr,*zr,paramf1);
        df12r=df12(*xr,*yr,*zr,paramf1);
        df13r=df13(*xr,*yr,*zr,paramf1);
        df21r=df21(*xr,*yr,*zr,paramf1);
        df22r=df22(*xr,*yr,*zr,paramf1);
        df23r=df23(*xr,*yr,*zr,paramf1);
        df31r=df31(*xr,*yr,*zr,paramf1);
        df32r=df32(*xr,*yr,*zr,paramf1);
        df33r=df33(*xr,*yr,*zr,paramf1);
  //      printf("df11r=%g,df12r=%g,df22r=%g,df21r=%g\n",df11r,df12r,df22r,df21r);
        double zr1=((df11r*df23r-df21r*df13r)*(df11r*df32r-df12r*df31r)-(df11r*df22r-df21r*df12r)*(df11r*df33r-df13r*df31r))/((df11r*df23r-df21r*df13r)*(f1r*df12r-df11r*f2r)+(df11r*df22r-df21r*df12r)*(df11r*f3r-df13r*f1r));
        double yr1=((df11r*df32r-df31r*df12r)*(df11r*df23r-df13r*df21r)-(df11r*df33r-df31r*df13r)*(df11r*df22r-df12r*df21r))/((df11r*df32r-df31r*df12r)*(f1r*df13r-df11r*f3r)+(df11r*df33r-df31r*df13r)*(df11r*f2r-df12r*f1r));
        double xr1=((df22r*df31r-df32r*df21r)*(df22r*df13r-df23r*df12r)-(df22r*df33r-df32r*df23r)*(df22r*df11r-df21r*df12r))/((df22r*df31r-df32r*df21r)*(f1r*df23r-df22r*f3r)+(df22r*df33r-df32r*df23r)*(df22r*f1r-df21r*f2r));

        //double yr1=(f2r*df11r-f1r*df22r)/(df21r*df12r-df11r*df22r);
       // double xr1=(-f1r-df12r*yr1)/df11r;
        iter++;
        *xr=*xr+1.0/xr1;
        *yr=*yr+1.0/yr1;
        *zr=*zr+1.0/zr1;
        if (*zr>0.995){
          *zr=0.995;status=0;
          return status;
        }
   //     printf("xr=%g,yr=%g,f1r=%g,f2r=%g\n",xr,yr,f1r,f2r);
      }
      if ((*xr==*xr)&&(*yr==*yr)&&(*zr==*zr)) {status=-1;fprintf(stderr,"Newton did not converged, iter >%i\n",maxiter);}
      else {status=-2;fprintf(stderr,"A parameter is not a number in Newton, i=%i\n",iter);}
      fprintf(stderr,"xr=%g, yr=%g, zr=%g, f1r=%g, f2r=%g, f3r=%g\n",*xr,*yr,*zr, f1r,f2r,f3r);
      fprintf(stderr,"xr_ini=%g,yr_ini=%g,zr_ini=%g,f1r_ini=%g,f2r_ini=%g,f3r_ini=%g\n",xr_ini,yr_ini,zr_ini,f1(xr_ini,yr_ini,zr_ini,paramf1),f2(xr_ini,yr_ini,zr_ini,paramf1),f3(xr_ini,yr_ini,zr_ini,paramf1));

      
          return status;
   }


//*********************************************************************************

