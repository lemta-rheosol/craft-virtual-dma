#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <complex.h>
#include <utils.h>

#ifdef _OPENMP
#include <omp.h>
#endif

typedef double mon_pointeur_fonction (double,double[*]);
typedef double mon_pointeur_fonction_2var (double,double,double[*]);
typedef double mon_pointeur_fonction_3var (double,double,double,double[*]);

int muller(double *x, mon_pointeur_fonction f, double *paramg, double h, double eps);
int dicho(double *x, double h, mon_pointeur_fonction f, double* paramg, double eps);
int newton(double *xr, mon_pointeur_fonction f, mon_pointeur_fonction df, double* paramg, double eps);
int newton_2eq(double *xr,double *yr, mon_pointeur_fonction_2var f1, mon_pointeur_fonction_2var df11,
                 mon_pointeur_fonction_2var f2, mon_pointeur_fonction_2var df22, mon_pointeur_fonction_2var df12, mon_pointeur_fonction_2var df21,
                 double* paramf1,double eps);


int newton_3eq(double *xr,double *yr,double *zr, mon_pointeur_fonction_3var f1,  mon_pointeur_fonction_3var f2, mon_pointeur_fonction_3var f3, 
                 mon_pointeur_fonction_3var df11, mon_pointeur_fonction_3var df12, mon_pointeur_fonction_3var df13, 
                 mon_pointeur_fonction_3var df21, mon_pointeur_fonction_3var df22,mon_pointeur_fonction_3var df23,
                 mon_pointeur_fonction_3var df31, mon_pointeur_fonction_3var df32, mon_pointeur_fonction_3var df33,
                 double* paramf1,double eps);
