#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <variables.h>
#include <materials.h>
#include <utils.h>

#include <meca.h>


/********************************************************************************************/
int size_of_variable_value( int type[3] ){
  int n;
  switch(type[0]){
  case SCALAR:
    n=1;
    break;
  case TENSOR2:
    n=6;
    break;
  case VECTOR3D:
    n=3;
    break;
  case VECTOR:
    n=type[1];
    break;
  case MATRIX:
    n=type[2]*type[1];
    break;
  default:
    fprintf(stderr,"impossible case in %s\n",__func__);
    return -1;
  }
  return n;
}

/********************************************************************************************/
int compare_type_variable(int a[3], int b[3]){
  if (a[0]!=b[0]) return -1;
  if ( (a[0]>3) && (a[1]!=b[1]) ) return -1;
  if ( (a[0]>4) && (a[2]!=b[2]) ) return -1;
  return 0;
}

/********************************************************************************************/
int check_finite(Variables *sv, int Nph, int all){
  int iph,i,j,Nel,flag;

  flag = 0;
  /* Loop on phases */
  for(iph=0; iph<Nph; iph++){
    /* Loop on variables */
    for(i=0;i<sv->nvar;i++){
      Nel = size_of_variable_value(sv->vars[i].type);
      /* Loop on values */
      flag=0;
      for(j=0;j<Nel*sv->np;j++){
	//        if (finite(((double *) sv->vars[i].values)[j])==0){
        if (
	    (isnan(((double *) sv->vars[i].values)[j])!=0) ||
	    (isinf(((double *) sv->vars[i].values)[j])!=0)
	    ){
          flag++;
	  //	  printf(">>> %f\n",
	  //		 ((double *)sv->vars[i].values)[j] );
          if (all==0){
            return -1;
          }
        }
      }
      if (flag!=0) {
	fprintf(stderr,
		"Variable %d (field %s) of phase %d has %d non-finite values.\n",
		i, sv->vars[i].label, iph, flag
		);
      }
    }
  }
  return flag;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
int check_finite_harmonic(Variables *sv, int Nph, int all){
  int iph,i,j,Nel,flag;

  flag = 0;
  /* Loop on phases */
  for(iph=0; iph<Nph; iph++){
    /* Loop on variables */
    for(i=0;i<sv->nvar;i++){
      Nel = size_of_variable_value(sv->vars[i].type);
      /* Loop on values */
      flag=0;
      for(j=0;j<Nel*sv->np;j++){
	//        if (finite(((double complex *) sv->vars[i].values)[j])==0){
        if (
	    (isnan(((double *) sv->vars[i].values)[j])!=0) ||
	    (isinf(((double *) sv->vars[i].values)[j])!=0)
	    ){
          flag++;
	  //	  printf(">>> %f\n",
	  //		 ((double complex *)sv->vars[i].values)[j] );
          if (all==0){
            return -1;
          }
        }
      }
      if (flag!=0) {
	fprintf(stderr,
		"Variable %d (field %s) of phase %d has %d non-finite values.\n",
		i, sv->vars[i].label, iph, flag
		);
      }
    }
  }
  return flag;
}

/********************************************************************************************/
int copy_variable_values( Variable *dest, Variable *src, int N ){
  int i,Nel;
  if ( compare_type_variable(dest->type, src->type) != 0 ) return -1;
  if ( (dest->values==NULL) || (src->values ==NULL) ) return -1;
  
  Nel = size_of_variable_value(src->type);
  if (Nel<0) return -1;

#pragma omp parallel for \
  default(none) \
  shared(N, Nel, dest, src) \
  private(i)
  for(i=0; i<N*Nel; i++){
    ((double *) dest->values)[i] = ((double *) src->values)[i];
  }

  return 0;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
int copy_harmonic_variable_values( Variable *dest, Variable *src, int N ){
  int i,Nel;
  if ( compare_type_variable(dest->type, src->type) != 0 ) return -1;
  if ( (dest->values==NULL) || (src->values ==NULL) ) return -1;
  
  Nel = size_of_variable_value(src->type);
  if (Nel<0) return -1;

#pragma omp parallel for \
  default(none) \
  shared(N, Nel, dest, src) \
  private(i)
  for(i=0; i<N*Nel; i++){
    ((double complex *) dest->values)[i] = ((double complex *) src->values)[i];
  }

  return 0;
}

/********************************************************************************************/
int allocate_variable(
      Variable *var,
      char *label,
      int N,
      int *type,
      int releasable
      ){
  int Nel;
  if ( (var->label = malloc(strlen(label)+1)) == NULL ){
    fprintf(stderr, "Cannot not allocate label for %s", label);
    return -1;
  }
  if ( strcpy(var->label, label) == NULL) {
    fprintf(stderr, "Cannot copy label for %s", label);
    free(var->label);
    return -1;
  }
  var->type[0] = type[0];
  var->type[1] = type[1];
  var->type[2] = type[2];
  var->releasable = (releasable!=0);
  
  Nel = size_of_variable_value(var->type);
  if (Nel<0){
    free(var->label);
    return -1;
  }
  
  var->values = malloc(N*Nel*sizeof(double));
  if ( var->values == NULL ){
    fprintf(stderr, "Cannot allocate space for values of %s", label);
    free(var->label);
    return -1;
  }
  return 0;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
int allocate_harmonic_variable(
      Variable *var,
      char *label,
      int N,
      int *type,
      int releasable
      ){
  int Nel;
  if ( (var->label = malloc(strlen(label)+1)) == NULL ){
    fprintf(stderr, "Cannot not allocate label for %s", label);
    return -1;
  }
  if ( strcpy(var->label, label) == NULL) {
    fprintf(stderr, "Cannot copy label for %s", label);
    free(var->label);
    return -1;
  }
  var->type[0] = type[0];
  var->type[1] = type[1];
  var->type[2] = type[2];
  var->releasable = (releasable!=0);
  
  Nel = size_of_variable_value(var->type);
  if (Nel<0){
    free(var->label);
    return -1;
  }
  
  var->values = malloc(N*Nel*sizeof(double complex));
  if ( var->values == NULL ){
    fprintf(stderr, "Cannot allocate space for values of %s", label);
    free(var->label);
    return -1;
  }
  return 0;
}

int set_variables( Variables *sv, double fill_value ){
  int i,j,Nel;
  for(i=0; i<sv->nvar; i++){
    Nel = size_of_variable_value(sv->vars[i].type);
    for(j=0; j<Nel*sv->np; j++)
      ((double *)(sv->vars[i].values))[j] = fill_value;
  }
  return 0;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
int set_harmonic_variables( Variables *sv, double complex fill_value ){
  int i,j,Nel;
  for(i=0; i<sv->nvar; i++){
    Nel = size_of_variable_value(sv->vars[i].type);
    for(j=0; j<Nel*sv->np; j++)
      ((double complex *)( sv->vars[i].values))[j] = fill_value;
  }
  return 0;
}

int init_variables( Variables *sv, void *param ){
  set_variables(sv, 0.);
  return 0;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
int init_harmonic_variables( Variables *sv, void *param ){
  set_harmonic_variables(sv, 0.);
  return 0;
}

/********************************************************************************************/
int  deallocate_variables( Variables *sv ){
  int i;
  for(i=0; i<sv->nvar; i++){
    free(sv->vars[i].label);
    free(sv->vars[i].values);
  }
  free(sv->vars);
  return 0;
}

/********************************************************************************************/
Variable *ptr_variable(
      char *var_name,
      Variables *sv,
      void *param
      ){
  int i,status;
  int scalar_t[3] = {SCALAR, 0, 0};
  char *sub=NULL;
  char *equi=NULL;
  char *trace=NULL;
  char *label;
  Variable *var=NULL;
  Variable *var_calc=NULL;

  /* First, try direct search of var_name */
  for (i=0; i<sv->nvar; i++){
    if ( strcasecmp_ignorespace(var_name , sv->vars[i].label) == 0 ){
      var = &sv->vars[i];
      return var;
    }
  }
  
  /* if it did not match any variable label, */
  /* try looking for equivalent or trace */
  label=malloc(strlen(var_name)+1);
  if ( label == NULL ){
    fprintf(stderr,"craft: Malloc error in ptr_variable (%s).\n", var_name);
    exit(1);
  }
  strcpy(label, var_name);

  /* Is an equivalent quantity required ? */
  sub = malloc(strlen("equivalent")+1);
  strcpy(sub, "equivalent");
  if ( (equi=strstr( label, sub )) != NULL){
    /* Remove "equivalent" from label. */
    memmove(equi, equi+strlen(sub), strlen(equi+strlen(sub))+1);
  }
  else{
    free(sub);
    sub = malloc(strlen("trace")+1);
    strcpy(sub, "trace");
    if ( (trace=strstr( label, sub )) != NULL ){
      memmove(trace, trace+strlen(sub), strlen(trace+strlen(sub))+1);
    }
  }
  free(sub);

  /* Get the required variable (or the related one if equivalent required) */
  for (i=0; i<sv->nvar; i++){
    if ( strcasecmp_ignorespace(label , sv->vars[i].label) == 0 ){
      var = &sv->vars[i];
      break;
    }
  }
  free(label);

  if ( (equi==NULL) && (trace==NULL) ) return var;

  if ( ( ((var->type[0]==MATRIX)&&(var->type[1]==var->type[2]))
         || (var->type[0]==TENSOR2) ) == 0){
    fprintf(stderr, "Can only compute invariant on tensor or square matrix.\n");
    return (Variable *)NULL;
  }
  var_calc = malloc(sizeof(*var_calc));
  if (var_calc==NULL){
    fprintf(stderr,"craft: error (malloc varequi) in ptr_variable (%s).\n",
            var_name);
    exit(1);
  }
  status = allocate_variable(var_calc, var_name, sv->np, scalar_t, 1);
  if (status!=0){
    free(var_calc);
    return (Variable *)NULL;
  }

  /* Compute equivalent if required */
  if (equi!=NULL){
    if ( (strstr( var_calc->label, "stress" )) != NULL){
      status=equivalent("stress", sv->np, var->values, var_calc->values);
    }
    else if ( (strstr( var_calc->label, "strain" )) != NULL){
      status=equivalent("strain", sv->np, var->values, var_calc->values);
    }
    else{
      status = -1;
    }
    if ( status!=0 ){
      free(var_calc->values);
      free(var_calc->label);
      free(var_calc);
      return (Variable *)NULL;
    }
    return var_calc;
  }

  /* Compute trace if required */
  if (trace!=NULL){
    int j;
    /* trace on tensor2 */
    if (var->type[0]==TENSOR2){
      double (*tensor)[6];
      tensor = (double (*)[6])var->values;
      for(i=0; i<sv->np; i++){
        ((double *)(var_calc->values))[i] = tensor[i][0]+tensor[i][1]+tensor[i][2];
      }
    }
    /* trace on matrix */
    if (var->type[0]==MATRIX){
      int Nc;
      double *tmp;
      tmp = (double *)var_calc->values;
      Nc = var->type[1];
      for(i=0; i<sv->np; i++){
        tmp[i] = 0;
        for(j=0; j<Nc; j++){
          /* tmp[i] = tmp[i]+ ((double (*)[Nc])var->values)[j][j]; */
          tmp[i] = tmp[i]+ ((double *)var->values)[j+j*Nc];
        }
      }
    }
  }
  return var_calc;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
Variable *ptr_harmonic_variable(
      char *var_name,
      Variables *sv,
      void *param
      ){
  int i,status;
  int scalar_t[3] = {SCALAR, 0, 0};
  char *sub=NULL;
  char *equi=NULL;
  char *trace=NULL;
  char *label;
  Variable *var=NULL;
  Variable *var_calc=NULL;

  /* First, try direct search of var_name */
  for (i=0; i<sv->nvar; i++){
    if ( strcasecmp_ignorespace(var_name , sv->vars[i].label) == 0 ){
      var = &sv->vars[i];
      return var;
    }
  }
  
  /* if it did not match any variable label, */
  /* try looking for equivalent or trace */
  label=malloc(strlen(var_name)+1);
  if ( label == NULL ){
    fprintf(stderr,"craft: Malloc error in ptr_variable (%s).\n", var_name);
    exit(1);
  }
  strcpy(label, var_name);

  /* Is an equivalent quantity required ? */
  sub = malloc(strlen("equivalent")+1);
  strcpy(sub, "equivalent");
  if ( (equi=strstr( label, sub )) != NULL){
    /* Remove "equivalent" from label. */
    memmove(equi, equi+strlen(sub), strlen(equi+strlen(sub))+1);
  }
  else{
    free(sub);
    sub = malloc(strlen("trace")+1);
    strcpy(sub, "trace");
    if ( (trace=strstr( label, sub )) != NULL ){
      memmove(trace, trace+strlen(sub), strlen(trace+strlen(sub))+1);
    }
  }
  free(sub);

  /* Get the required variable (or the related one if equivalent required) */
  for (i=0; i<sv->nvar; i++){
    if ( strcasecmp_ignorespace(label , sv->vars[i].label) == 0 ){
      var = &sv->vars[i];
      break;
    }
  }
  free(label);

  if ( (equi==NULL) && (trace==NULL) ) return var;

  if ( ( ((var->type[0]==MATRIX)&&(var->type[1]==var->type[2]))
         || (var->type[0]==TENSOR2) ) == 0){
    fprintf(stderr, "Can only compute invariant on tensor or square matrix.\n");
    return (Variable *)NULL;
  }
  var_calc = malloc(sizeof(*var_calc));
  if (var_calc==NULL){
    fprintf(stderr,"craft: error (malloc varequi) in ptr_variable (%s).\n",
            var_name);
    exit(1);
  }

  /* modified for harmonic implementation 2017/07/05  J. Boisse */
  status = allocate_harmonic_variable(var_calc, var_name, sv->np, scalar_t, 1);
  if (status!=0){
    free(var_calc);
    return (Variable *)NULL;
  }

  /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
  /* Compute equivalent if required */
  if (equi!=NULL){
    if ( (strstr( var_calc->label, "stress" )) != NULL){
      status=equivalent("stress", sv->np, var->values, var_calc->values);
    }
    else if ( (strstr( var_calc->label, "strain" )) != NULL){
      status=equivalent("strain", sv->np, var->values, var_calc->values);
    }
    else{
      status = -1;
    }
    if ( status!=0 ){
      free(var_calc->values);
      free(var_calc->label);
      free(var_calc);
      return (Variable *)NULL;
    }
    return var_calc;
  }

  /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
  /* Compute trace if required */
  if (trace!=NULL){
    int j;
    /* trace on tensor2 */
    if (var->type[0]==TENSOR2){
      double (*tensor)[6];
      tensor = (double (*)[6])var->values;
      for(i=0; i<sv->np; i++){
        ((double *)(var_calc->values))[i] = tensor[i][0]+tensor[i][1]+tensor[i][2];
      }
    }
    /* trace on matrix */
    if (var->type[0]==MATRIX){
      int Nc;
      double *tmp;
      tmp = (double *)var_calc->values;
      Nc = var->type[1];
      for(i=0; i<sv->np; i++){
        tmp[i] = 0;
        for(j=0; j<Nc; j++){
          /* tmp[i] = tmp[i]+ ((double (*)[Nc])var->values)[j][j]; */
          tmp[i] = tmp[i]+ ((double *)var->values)[j+j*Nc];
        }
      }
    }
  }
  return var_calc;
}

/********************************************************************************************/
int phase_to_image_variables( int Nph, int *index[Nph], Variables *sv, char *varname, Material *material, 
      CraftImage *store_image) {
  int status, iph, count;
  int nppp[Nph];
  int type[3];
  void *x[Nph];
  Variable *var[Nph];
  status = 0;
  count = 0;
  type[0] = type[1] = type[2] = -1;
  void *param;

  for(iph=0; iph<Nph; iph++){
    nppp[iph] = sv[iph].np;

    if ( material == (Material *)0) {
      param = (void *)0;
      var[iph] = ptr_variable(varname, &sv[iph], NULL);
    }
    else {
      param = material[iph].parameters;
      var[iph] = (*material[iph].ptr_variable)(varname, &sv[iph], param);
    }

    if(var[iph] == (Variable*)NULL){
      count ++;
      x[iph] = NULL;
    }
    else{
      if (type[0]==-1){
        type[0] = var[iph]->type[0];
        type[1] = var[iph]->type[1];
        type[2] = var[iph]->type[2];
      }
      if ( compare_type_variable(type, var[iph]->type)==0 ) {
        x[iph] = var[iph]->values;
      }
      else{
        /* Type needs to be OK to take values into account */
        fprintf(stderr, "Incompatible types for %s.\n", varname);
        count ++;
        x[iph] = NULL;
        status = -1;
      }
    }
  }
  
  if (count==Nph){
    fprintf(stderr, "There is no phase with a Variable labeled '%s'.\n", varname);
    status = -1;
  }
  
  if (status==0){
    store_image->type[1] = type[1];
    store_image->type[2] = type[2];
    status = 0;
    
    if ( type[0]==VECTOR ){
      store_image->type[0] = CRAFT_IMAGE_VECTOR;
    }
    else if ( type[0]==SCALAR ){
      store_image->type[0] = CRAFT_IMAGE_DOUBLE;
    }
    else if ( type[0]==VECTOR3D ){
      store_image->type[0] = CRAFT_IMAGE_VECTOR3D;
    }
    else if ( type[0]==TENSOR2 ){
      store_image->type[0] = CRAFT_IMAGE_TENSOR2;
    }
    else{
      fprintf(stderr, "phase_to_image_variable: type %d not implemented.\n", type[0]);
      status = -1;
    }
  }
  
  if (status==0){
    status=craft_image_alloc(store_image);
    if (status!=0) fprintf(stderr,"  phase_to_image_variables: error returned by craft_image_alloc\n");
    if (status==0){
      status=phase_to_image(Nph, nppp, index, x, store_image); 
      if (status!=0) fprintf(stderr,"  phase_to_image_variables: error returned by phase_to_image\n");
    }
  }
  
  for(iph=0;iph<Nph;iph++){
    if ( (var[iph]!=(Variable *)NULL) && (var[iph]->releasable==1) ) {
      free(var[iph]->label);
      free(var[iph]->values);
      free(var[iph]);
    }
  }
  return status;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
int phase_to_image_harmonic_variables( int Nph, int *index[Nph], Variables *sv, char *varname, Material *material, 
      CraftImage *store_image) {
  int status, iph, count;
  int nppp[Nph];
  int type[3];
  void *x[Nph];
  Variable *var[Nph];
  status = 0;
  count = 0;
  type[0] = type[1] = type[2] = -1;
  void *param;

  for(iph=0; iph<Nph; iph++){
    nppp[iph] = sv[iph].np;

    if ( material == (Material *)0) {
      param = (void *)0;
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      var[iph] = ptr_harmonic_variable(varname, &sv[iph], NULL);
    }
    else {
      param = material[iph].parameters;
      var[iph] = (*material[iph].ptr_variable)(varname, &sv[iph], param);
    }

    if(var[iph] == (Variable*)NULL){
      count ++;
      x[iph] = NULL;
    }
    else{
      if (type[0]==-1){
        type[0] = var[iph]->type[0];
        type[1] = var[iph]->type[1];
        type[2] = var[iph]->type[2];
      }
      if ( compare_type_variable(type, var[iph]->type)==0 ) {
        x[iph] = var[iph]->values;
      }
      else{
        /* Type needs to be OK to take values into account */
        fprintf(stderr, "Incompatible types for %s.\n", varname);
        count ++;
        x[iph] = NULL;
        status = -1;
      }
    }
  }
  
  if (count==Nph){
    fprintf(stderr, "There is no phase with a Variable labeled '%s'.\n", varname);
    status = -1;
  }
  
  if (status==0){
    store_image->type[1] = type[1];
    store_image->type[2] = type[2];
    status = 0;
    
    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    if ( type[0]==VECTOR ){
      store_image->type[0] = CRAFT_IMAGE_HARMONIC_VECTOR;
    }
    else if ( type[0]==SCALAR ){
      store_image->type[0] = CRAFT_IMAGE_HARMONIC_DOUBLE;
    }
    else if ( type[0]==VECTOR3D ){
      store_image->type[0] = CRAFT_IMAGE_HARMONIC_VECTOR3D;
    }
    else if ( type[0]==TENSOR2 ){
      store_image->type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
    }
    else{
      /* modified for harmonic implementation 2017/07/05  J. Boisse */
      fprintf(stderr, "phase_to_image_harmonic_variable: type %d not implemented.\n", type[0]);
      status = -1;
    }
  }

  /* modified for harmonic implementation 2017/07/05  J. Boisse */  
  if (status==0){
    status=craft_image_alloc(store_image);
    if (status!=0) fprintf(stderr,"  phase_to_image_harmonic_variables: error returned by craft_image_alloc\n");
    if (status==0){
#ifdef DEBUG
    printf("variable.c : in phase_to_image_harmonic_variables(), RUN phase_to_image():\n");
#endif
      status=phase_to_image(Nph, nppp, index, x, store_image);
      if (status!=0) fprintf(stderr,"  phase_to_image_harmonic_variables: error returned by phase_to_image\n");
    }
  }
  
  for(iph=0;iph<Nph;iph++){
    if ( (var[iph]!=(Variable *)NULL) && (var[iph]->releasable==1) ) {
      free(var[iph]->label);
      free(var[iph]->values);
      free(var[iph]);
    }
  }
  return status;
}

