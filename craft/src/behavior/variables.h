/**************************************************************************/
/*
  Herv� Moulinec
  LMA/CNRS

  january 14th 2010

  definition of the different type of possible state variables

*/
/**************************************************************************/
#ifndef __STATE_VARIABLES__
#define __STATE_VARIABLES__

#include <craftimage.h>


#ifndef UNKNOWN
#define UNKNOWN 0
#endif 

/* scalar state variables are defined as a double */
#define SCALAR 1
/* 3D vectors are described by 3 doubles (their 3 components) */
#define VECTOR3D 2
/* 2d order tensors are described by 6 components which are doubles, in order:
   11, 22, 33, 23, 13, 12 */
#define TENSOR2 3
/* a vector is descibed by its N components which are doubles. */
/* N must specified somewhere */
#define VECTOR 4
/* a M*N matrix is described by its M*N components which are doubles;
   M and N must be must specified somewhere */
#define MATRIX 5
/* a good idea to describe a given state variable is to use a
      int type[3]
   in which:
      type[0] contains the data type ( SCALAR, VECTOR3D, ...)
      type[1] contains
                the number N of components of the vector if the data is a VECTOR
                or the 1st dimension M of the matrix, if it's a MATRIX
      type[2] contains the 2d dimension of the matrix, if the data a MATRIX
*/

typedef struct {
  char *label;
  int type[3];    /* see above about the description of the kind of data */
  int releasable; /* whether it is safe to free this Variable */
  void *values;
} Variable;

typedef struct {
  int nvar;
  int np;       /* number of point in the phase. */
  Variable *vars;
  int isextrapolated; /* added by Louis Joessel */
} Variables;

int size_of_variable_value( int typeofvariable[3] );

/* Compares the types of two variables */
int compare_type_variable(int a[3], int b[3]);

/* Copy values of variable from src to dest (after checking type) */
int copy_variable_values( Variable *dest, Variable *src, int N );

/* Check that all values of all variables of all phases are finite.        */
/* Argument check_all controls whether the function returns when the first */
/* invalid Variable is found or whether all the Variable's are examined    */
int check_finite(Variables *sv, int Nph, int check_all);

/* Helper function to allocate and fill a variable object */
int allocate_variable(
      Variable *var,            /* Variable instance to be filled     */
      char *label,              /* Label of the Variable to allocate  */
      int N,                    /* Number of pixels concerned         */
      int *type,                /* type information                   */
      int releasable            /* Flag telling persistence of variable */
      );

/* set the values of the variables to a given value */
int set_variables( Variables *sv, double fill_value );

/* Initialize all variables to what requires the material they belong to*/
int init_variables( Variables *sv, void *param );

/* Deallocate a Variables instance */
int  deallocate_variables( Variables *sv );

/* Retrieve a pointer to a given field (in one phase). */
Variable *ptr_variable(
      char *var_name,   /* Name of the field to retrieve        */
      Variables *sv,    /* Variables structure for given phase  */
      void *param       /* material parameters (if necessary) */
      );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int copy_harmonic_variable_values( Variable *dest, Variable *src, int N );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int check_finite_harmonic(Variables *sv, int Nph, int check_all);

/* added for harmonic implementation 2017/07/05  J. Boisse */
int allocate_harmonic_variable(
      Variable *var,            /* Variable instance to be filled     */
      char *label,              /* Label of the Variable to allocate  */
      int N,                    /* Number of pixels concerned         */
      int *type,                /* type information                   */
      int releasable            /* Flag telling persistence of variable */
      );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int set_harmonic_variables( Variables *sv, double complex fill_value );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int init_harmonic_variables( Variables *sv, void *param );

/* added for harmonic implementation 2017/07/05  J. Boisse */
Variable *ptr_harmonic_variable(
      char *var_name,   /* Name of the field to retrieve        */
      Variables *sv,    /* Variables structure for given phase  */
      void *param       /* material parameters (if necessary) */
      );

#endif
