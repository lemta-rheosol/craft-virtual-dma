#define __MAIN__

#define _ISOC99_SOURCE
#define _DEFAULT_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>


#include <craft.h>
#include <craftimage.h>
#include <materials.h>
#include <LE_materials.h>
#include <LE_Cmatrix.h>
#include <loading.h>
#include <craftoutput.h>
#include <variables.h>
#include <io_variables.h>
#include <utils.h>
#include <meca.h>
#include <craft_options.h>
#include <euler.h>
#include <moments.h>
#include <craft_compatibility_equilibrium.h>

#include <craft_timer.h>
#include <craft_schemes.h>
#include <craft_lsp.h>
#include <craft_log.h>
#include <craft_temperature.h>

#ifdef _OPENMP
#include <omp.h>
#endif

int read_material_parameters( 
                             char *filename,            
                             int Nph ,                  
                             int material_in_phase[Nph] ,               
                             Material material[Nph]);

int read_phase_file( 
                    char *filename,
                    int Nph,
                    int *phase,
                    Euler_Angles *orientation,
                    int *material_in_phase ) ;

int enter_C0parameters(char *C0_line_command, LE_param *C0, Material *material,Euler_Angles *orientation,int Nph, int scheme);



/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   15 september 2009

   CraFT version 1.0
*/
/*********************************************************************************/
int main(int argc, char **argv){

  /*-----------------------------------------------------------------------------*/
  char *cfn;      /* name of the image file describing the characteristic
                     function                                                    */
  char *tfn;      /* name of the image file describing the thermal strain field
                     (if any)                                                    */

  CraftImage cf;  /* image of charact. function */

  int Nph;        /* total number of phases in the microstructure */
  int *phase;     /* indices of the phases in the microstructure  */
                  /* phase[iph] is the phase index as given in the microstructure image
                     of the i-th phase (indexed form 0 to Nph-1)                 */
  int *nppp;      /* number of points per phase */
  int **index;    /* index[iph][ipt] gives the index in the tensor image (used in
                     the Lippmann-Schwinger algorithm) of the point number ipt
                     in the phase number iph                                     */

  char *pfn;      /* name of the file describing the phases of the material      */
  Euler_Angles *orientation; /* orientation[iph] gives the crystallographic
                     orientation (or the orientation of its anistropy)
                     of phase number iph.
                     If iph is an isotropic material, orientation is useless     */
  int *material_in_phase;
                  /* material_in_phase[iph] gives the number of the material
                     that the phase iph is made of */

  char *mfn;      /* name of the file describing the the materials composing
                     the microstructure */

  Variables *variables;
                  /* variables[iph] : pointer to the structure describing
                     the variables in phase iph                            */

#ifdef HARMONIC
  /* added for harmonic implementation 2017/07/05  J. Boisse */
  double complex (**phase_epsilon)[6]=(double complex (**)[6])0;
  double complex (**phase_previous_epsilon)[6]=(double complex (**)[6])0;
  double complex (**phase_sigma)[6]=(double complex (**)[6])0;
#else
  double (**phase_epsilon)[6]=(double (**)[6])0;
                  /* phase_epsilon[iph][ipt] gives the strain tensor of point
                     number ipt of phase number iph                              */


  double (**phase_previous_epsilon)[6]=(double (**)[6])0;
                  /* phase_previous_epsilon[iph][ipt] gives the strain tensor
                     of point number ipt of phase number iph at previous loading
                     step.                                                        */
  double (**phase_sigma)[6]=(double (**)[6])0;
                  /* phase_sigma[iph][ipt] gives the stress tensor of point
                     number ipt of phase number iph                        */
#endif

  double (**phase_epsilon_thermal)[6]=(double (**)[6])0;
                  /* phase_epsilon_thermal[iph][ipt] gives the thermal strain tensor of point
                     number ipt of phase number iph                              */

  /* NOTE:
     phase_epsilon , phase_sigma and phase_previous_epsilon are now useless since
     they are mandatorily to be put in the "state variables" of any behavior
     (as "epsilon" , "sigma" and "epsilon_p" varaibles)
     BUT phase_epsilon_thermal is still necessary because it has not been implemented 
     in the state variables of the behaviors (HM 20th April 2015)
  */



  Material *material;
                  /* material[iph] gives every information about the material
                     in phase iph */

  CraftImage image;  /* image of a 2d order tensor, used in the iterative
                        relation                                                 */
  CraftImage geometry; /* describe the geometry of the volume. Contains no pixel data */

  char *lfn;      /* name of the file describing the loading conditions          */
  char *tlfn;     /* name of the file describing the temperature loading conditions          */

  Loading load;   /* loading conditions                                          */
  Temperature_Loading temperature_loading; /* temperature loading conditions */
  
  char *rfn;      /* name of the file containing a saved state of variables */
  int it_save;  /* first instant to handle */
  double t_save;

#ifdef HARMONIC
  /* added for harmonic implementation 2017/07/05  J. Boisse */
  double complex E_cur[6], E_prev[6];
#else
  double E_cur[6], E_prev[6];
#endif

  Material C0;    /* "reference material"                                        */

  double compatibility_error;  /* error on the compatibility of the strain       */
  double divs_error;  /* error on divergence of the stress                       */
  double macro_error; /* error on macroscopic stress or direction of macroscopic
                         stress (depending on loading conditions)                */
  double compatibility_precision;   /* precision required by user on strain 
                                                                   compatibility */
  double divs_precision;   /* precision required by user on divergence of the
                     stress                                                      */
  double macro_precision;  /* precision required by user on macroscopic stress
                     or direction of macroscopic
                     stress (depending on loading conditions)                    */
  int convergence_test_method; /* method to be employed for convergence test  */
  int maxiterations; /* maximum number of authorized iterations for solving Lippmann-Schwinger equation */

  char *ofn;          /* name of the file describing required outputs            */
  Craft_Output_Description cod; /* output description */
  /*-----------------------------------------------------------------------------*/
#ifdef HARMONIC
  /* added for harmonic implementation 2017/07/05  J. Boisse */
  double complex (*E)[6];
  double complex (*S)[6];
#else
  /* macroscopic strain and stress for every time step                           */
  double (*E)[6];
  double (*S)[6];
#endif
  /*-----------------------------------------------------------------------------*/
  /* increment of time                                                           */
  double dt;

  /* index of the iterative process                                              */
  int niter;

  /* alpha beta parameters */
  double alpha;
  double beta;

  int iph;

  int status;
  int i,j;
  int it;

  /*-----------------------------------------------------------------------------*/
  /* number of threads used by OpenMP                                            */
  /* either specified by -n option
     or read in OMP_NUM_THREADS environment variable
     if not specified, considered as equal to 1 (no parallelism)                 */
  int nr_threads=-1;
  /*-----------------------------------------------------------------------------*/
  char *file=(char *)0;
  /* type of data */
  int type[3];

#ifdef DEBUG
  printf("craft.c : CraFT built with DEBUG option ON\n");
#endif

  /*#############################################################################*/
  /*                               INPUTS                                        */
  /*#############################################################################*/

  /*=============================================================================*/
  /* reads everything necessary for problem description                          */
  /*=============================================================================*/
  char *C0_line_command;
  int scheme;

#ifdef DEBUG
  printf("craft.c : RUN craft_options()\n");
#endif

  status=craft_options(
    argc, argv,
    &cfn, &tfn, &pfn, &mfn, &lfn,
    &tlfn,
    &ofn, &rfn, &C0_line_command,
    &divs_precision,  &compatibility_precision, &macro_precision,
    & convergence_test_method,
    &maxiterations,
    &scheme, &alpha, &beta, 
    &nr_threads,
    &verbose);

  if(status!=0) {
    usage();
    exit(1);
  }
  
  if(verbose){
    printf("div(sigma) precision=%lg\n",divs_precision);
    printf("macro precision=%lg\n",macro_precision);
    printf("compatibility precision=%lg\n",compatibility_precision);
    printf("convergence test method=%d\n", convergence_test_method);
    if(tfn==(char *)0) {
      printf("no thermal strain image entered\n");
    }
    else{
      printf("thermal strain image=%s\n",tfn);
    }

  }

  if(status!=0) {
    fprintf(stderr,"CraFT exit on error\n");
    exit(1);
  }

#ifdef DEBUG
  printf("craft.c : INITIALIZATION\n");
#endif

  /*#############################################################################*/
  /*                            INITIALIZATION                                   */
  /*#############################################################################*/

  /*=============================================================================*/
  /* performance timers initialization */
  //  for(i=0;i<NTIMER;i++) timer[i]=0.;
  init_timers();
  start_timer(CRAFT_TIMER_TOTAL);
  /*=============================================================================*/
  /* number of threads used by OpenMP                                            */
#ifdef _OPENMP
  if (nr_threads==-1) {
    /* if no number of threads has been specified by craft option -n,
       it has to be read in OMP_NUM_TREADS environment variable
    */
    char *env;
    int status;

    env = getenv("OMP_NUM_THREADS");

    if( env == (char *)0 ) {
      nr_threads = -1;
    }
    else{
      status=sscanf(env,"%d",&nr_threads);
      if (status!=1) nr_threads=-1;
    }
    if (verbose) printf("OMP_NUM_THREADS=%d\n", nr_threads);
  }

  /* if no number of threads has been specified, it is supposed to equal
     to 1
  */
  if (nr_threads==-1) nr_threads=1;

  /* set number of threads to be used by OpenMP */
  omp_set_num_threads(nr_threads);
#endif


#ifdef _OPENMP
  if (verbose) {
    fprintf(stdout, "OpenMP parallel version\n  number of threads=%d\n",
      omp_get_max_threads());
  }
#else
  if (verbose) {
    fprintf(stdout, "non OpenMP parallel version\n ");
  }

#endif

#ifdef DEBUG
  printf("craft.c : creation of image\n");
#endif

  /*=============================================================================*/
  start_timer(CRAFT_TIMER_READ_DATA);
  /*=============================================================================*/
  /* opens the image of the characteristic function                              */
  /*=============================================================================*/

  cf.type[0] = CRAFT_IMAGE_INT;
  cf.pxl.v = NULL;
  status=read_image( cfn, &cf );
  if(status!=0) {
    fprintf(stderr,"error in craft: impossible to read %s file\n", cfn);
    exit(1);
  }
  /*=============================================================================*/
  /* creation of image                                                           */
  {
    int i;

#ifdef HARMONIC
/* added for harmonic implementation 2017/07/05  J. Boisse */
    image.type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2;
#else
    image.type[0] = CRAFT_IMAGE_TENSOR2;
#endif
    image.type[1] = 6;
    image.type[2] = 0;
    image.name = NULL;
    for(i=0; i<3; i++){
      image.n[i] = cf.n[i];
      image.nd[i]= cf.n[i];
      image.s[i] = cf.s[i];
      image.p[i] = cf.p[i];
    }
#if defined FFTW2 || defined FFTW3

#ifdef HARMONIC
    /* added for harmonic implementation 2017/07/05  J. Boisse */
    /* TO CHECK */
    image.nd[0] = image.n[0];
#else
    image.nd[0] = 2*(image.n[0]/2+1);   // see in FFTW 3.3.2 documentation: 4.3.4 Real-data DFT Array Format
#endif

#else
    image.nd[0] = image.n[0]+2;
#endif
    
    status=craft_image_alloc(&image);
    
    if(status!=0) {
      fprintf(stderr,"craft: error while creating image\n");
      exit(1);
    }

    /* store the geometry of the volume discretization */
    geometry = image;
    geometry.name = (char *)0;
    geometry.type[0] = CRAFT_IMAGE_UNKNOWN;
    geometry.type[1] = 0;
    geometry.type[2] = 0;
    geometry.pxl.v = (void *)0;

  }

#ifdef DEBUG
  printf("craft.c : RUN parse_characteristic_file()\n");
#endif

  /*=============================================================================*/
  /* Determine Nph, phases indices, nppp, initialize image and constructs index. */
  status = parse_characteristic_file(&cf, &Nph, &phase, &nppp, &index, image);
  if(status != 0) {
    fprintf(stderr,"Craft: error while parsing characteristic file.\n");
    exit(1);
  }
  /* cf not needed anymore, can be free'd */
  type[0] = CRAFT_IMAGE_UNKNOWN;
  //  craft_image_reset( &cf, type );
  /* the pixels memory space is to be freed, but one keeps the geometry of the
     microstructure in cf structure */
  craft_image_free( &cf );

#ifdef DEBUG
  printf("craft.c : choice of the method for convergence test\n");
#endif

  /*=============================================================================*/
  /* choice of the method for convergence test   */
  switch(convergence_test_method){
  case CRAFT_TEST_NOT_SET:
    fprintf(stderr," CraFT error: error test not set.\n");
    exit(1);
    break;
  case CRAFT_TEST_DIVS_SUM:
    equilibrium = &equilibrium_divs_sum;
    break;
  case CRAFT_TEST_DIVS_MAX:
    equilibrium = &equilibrium_divs_max;
    break;
  case CRAFT_TEST_MONCHIET_BONNET:
    equilibrium = &equilibrium_MB2;
    break;
  default:
    fprintf(stderr,"CraFT error: invalid error test method\n");
    exit(1);
    break;  
    
  }  
  /*=============================================================================*/
  /* initialization of FFTs                                                      */
  /*=============================================================================*/

#ifdef DEBUG
  printf("craft.c : initialization of FFTs\n");
#endif

#ifdef HARMONIC
/* added for harmonic implementation 2017/07/05  J. Boisse */
  status=craft_fft_image_harmonic( &image, 0 );
#else
  status=craft_fft_image( &image, 0 );
#endif

  if(status != 0) {
    fprintf(stderr,"Craft: error while initializing FFTs\n");
    exit(1);
  }

#ifdef DEBUG
  printf("craft.c : END initialization of FFTs\n");
#endif

#ifdef DEBUG
  printf("craft.c : phase properties\n");
#endif

  /*=============================================================================*/
  /* phase properties                                                            */
  /*=============================================================================*/
  /*-----------------------------------------------------------------------------*/
  /* allocate memory for orientation (saying crystallographic orientation of     */
  /* each phase)                                                                 */
  /*-----------------------------------------------------------------------------*/
  orientation = (Euler_Angles *)malloc(sizeof(*orientation)*Nph);
  for(iph=0; iph<Nph; iph++){
    orientation[iph] = (Euler_Angles){0., 0., 0.};
  }

  /* allocate the table giving the material that each phase is made of */
  material_in_phase = (int *)malloc(Nph*sizeof(*material_in_phase));

  /*-----------------------------------------------------------------------------*/
  /* opens and reads the file describing the phases
     (fills material_in_phase and orientation)                                   */
  /*-----------------------------------------------------------------------------*/
  status = read_phase_file(pfn,Nph,phase,orientation,material_in_phase);
  if(status!=0) exit(1);

#ifdef DEBUG
  for(iph=0;iph<Nph;iph++) {
    fprintf(stdout,"phase=%d %d material=%d Euler angles=%f %f %f\n",
      iph,phase[iph],material_in_phase[iph],
      orientation[iph].phi1, orientation[iph].Phi, orientation[iph].phi2);
  }
#endif

#ifdef DEBUG
  printf("craft.c : material properties\n");
#endif

  /*=============================================================================*/
  /* material properties                                                         */
  /*=============================================================================*/
  material = (Material *)malloc(Nph*sizeof(*material));

  status = read_material_parameters( mfn, Nph, material_in_phase, material);
  if(status!=0) {
    fprintf(stderr,"craft: error while reading material properties.\n exit\n");
    exit(1);
  }

#ifdef DEBUG
  printf("craft.c : allocate and initialize state variables of each phase\n");
#endif

  /*=============================================================================*/
  /* allocate and initialize state variables of each phase                       */
  /*=============================================================================*/

  variables = malloc(Nph*sizeof(*variables));
  for(iph=0; iph<Nph; iph++) {
    material[iph].allocate_variables( nppp[iph], &variables[iph], material[iph].parameters);
  }

#ifdef HARMONIC
  /* added for harmonic implementation 2017/07/05  J. Boisse */
  phase_epsilon = (double complex (**)[6])malloc(Nph*sizeof(*phase_epsilon));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "strain", &variables[iph], material[iph].parameters);
    phase_epsilon[iph] = (double complex (*)[6])var->values;
  }

  /* added for harmonic implementation 2017/07/05  J. Boisse */
  phase_previous_epsilon = (double complex (**)[6])malloc(Nph*sizeof(*phase_previous_epsilon));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "previous strain", &variables[iph], material[iph].parameters);
    phase_previous_epsilon[iph] = (double complex (*)[6])var->values;
  }

  /* added for harmonic implementation 2017/07/05  J. Boisse */
  phase_sigma = (double complex (**)[6])malloc(Nph*sizeof(*phase_sigma));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "stress", &variables[iph], material[iph].parameters);
    phase_sigma[iph] = (double complex (*)[6])var->values;
  }
#else
  phase_epsilon = (double (**)[6])malloc(Nph*sizeof(*phase_epsilon));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "strain", &variables[iph], material[iph].parameters);
    phase_epsilon[iph] = (double (*)[6])var->values;
  }

  phase_previous_epsilon = (double (**)[6])malloc(Nph*sizeof(*phase_previous_epsilon));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "previous strain", &variables[iph], material[iph].parameters);
    phase_previous_epsilon[iph] = (double (*)[6])var->values;
  }

  phase_sigma = (double (**)[6])malloc(Nph*sizeof(*phase_sigma));
  for(iph=0;iph<Nph;iph++) {
    Variable *var;
    var = (*material[iph].ptr_variable)( "stress", &variables[iph], material[iph].parameters);
    phase_sigma[iph] = (double (*)[6])var->values;
  }
#endif

  phase_epsilon_thermal = (double (**)[6])malloc(Nph*sizeof(*phase_epsilon_thermal));
  for(iph=0;iph<Nph;iph++) {
    phase_epsilon_thermal[iph] = (double (*)[6])0;
  }

#ifdef DEBUG
  printf("craft.c : open and store thermal strain (if any)\n");
#endif

  /*=============================================================================*/
  /* open and store thermal strain (if any)                                       */
  /*=============================================================================*/
  if(tfn!=(char *)0){
    /* allocate memory for the thermal strain in phase format */
    for(iph=0;iph<Nph;iph++) {
      phase_epsilon_thermal[iph] =
        (double (*)[6])malloc(nppp[iph]*sizeof(*phase_epsilon_thermal[iph]));
    }
    
    /* read the image of thermal strain */
    status = read_image(tfn, &image);
    /* one checks if the thermal strain image has the same geometry as the one of
       the characteristic function */
    if ( !(
        (image.n[0]==geometry.n[0]) &&
        (image.n[1]==geometry.n[1]) &&
        (image.n[2]==geometry.n[2]) &&
        (fabs(image.p[0]-geometry.p[0])-TINY) &&
        (fabs(image.p[1]-geometry.p[1])-TINY) &&
        (fabs(image.p[2]-geometry.p[2])-TINY) &&
        (fabs(image.s[0]-geometry.s[0])-TINY) &&
        (fabs(image.s[1]-geometry.s[1])-TINY) &&
        (fabs(image.s[2]-geometry.s[2])-TINY) 
           )){
      fprintf(stderr,"CraFT error:\n");
      fprintf(stderr,"  microstructure image: %s and thermal strain image %s\n",cfn,tfn);
      fprintf(stderr,"  have different sizes or format.\n");
      exit(1);
    }
      
      
    
    /* and store it in phase format */
    printf("status of the reading of the thermal strain image is %d\n",status);
    status = image_to_phase( Nph, nppp, index, (void *)phase_epsilon_thermal, &image);
    
  }

#ifdef DEBUG
  printf("craft.c : reference material C0\n");
#endif

 /*=============================================================================*/
  /* reference material C0                                                       */
  /*=============================================================================*/
  Init_Material( LE_MATERIAL, &C0) ;
  status = enter_C0parameters(C0_line_command,(LE_param *)C0.parameters,material,orientation,Nph,scheme);
  if(status!=0) exit(1);

  //  C0.print_parameters(stdout,C0.parameters,1);

#ifdef DEBUG
  printf("craft.c : reads the file describing the loading conditions 0\n");
#endif

  /*=============================================================================*/
  /* reads the file describing the loading conditions                            */
  /*=============================================================================*/
  status=read_load_file( lfn, &load) ;
  if(status!=0) exit(1);

#ifdef DEBUG
  printf("craft.c : reads the file describing the temperature loading conditions\n");
#endif

  /*=============================================================================*/
  /* reads the file describing the temperature loading conditions                            */
  /*=============================================================================*/
  status=read_temperature_file( tlfn, &temperature_loading) ;
  if(status!=0) exit(1);

#ifdef DEBUG
  printf("craft.c : reads the list of required outputs\n");
#endif

  /*=============================================================================*/
  /* reads the list of required outputs                                          */
  /*=============================================================================*/
  status = init_output_description( &cod, &load);
  status = read_output_description( &cod, ofn);
  if(status!=0) exit(1);

#ifdef DEBUG
  printf("craft.c : reads the file containing a previously computed state of variables\n");
#endif

  /*=============================================================================*/
  /* reads the file containing a previously computed state of variables          */
  /*=============================================================================*/
  it_save = 1;
  t_save = 0.;
  if (rfn != NULL){
    for(iph=0; iph<Nph; iph++){
      set_variables( &variables[iph], NAN );
    }
    status = read_variables_all_phases(
          Nph,
          variables,
          rfn,
          &it_save, &t_save,
          E_cur, E_prev);
    if (status==-1){
      fprintf(stderr, "Failed to read saved state file %s.\n", rfn);
      exit(0);
    }
    

    /* Look for save time in the (possibly different) current load */
    int flag;
    flag = 0;
    //    for(it=0; (it<load.Nt) && (t_save>load.step[it].time-TINY); it++){
    for(it=0; (it<load.Nt)&&(flag==0); it++){
      if ( fabs(load.step[it].time-t_save)<TINY ){
        if (it_save!=it){
          fprintf(stderr, "Time index for t=%15.8f was %d "
            "when the file was saved and %d in the current load.\n",
            t_save, it_save, it);
        }
        it_save = it;
        flag = 1;
        break;
      }
    }
    
    if (flag==0){
      fprintf(stderr, "Time t=%15.8f was not found in load time steps.\n", t_save);
      exit(0);
    }
    if (it_save==load.Nt-1){
      fprintf(stderr, "Restored variables correspond to the last step in loading" 
                      "(no more work to do).\n");
      exit(0);
    }
    
    /* Used as flag for NAN variables checking */
    t_save = NAN;
  }
  else{
    for(iph=0; iph<Nph; iph++){
      material[iph].init_variables( &variables[iph], material[iph].parameters );
    }
  }

  /*=============================================================================*/
  stop_timer(CRAFT_TIMER_READ_DATA);
  /*=============================================================================*/

#ifdef DEBUG
  printf("craft.c : REPORT ABOUT THE COMPUTATION, RUN craft_log()\n");
#endif

  /*#############################################################################*/
  /*                      REPORT ABOUT THE COMPUTATION                           */
  /*#############################################################################*/
  status = craft_log(
                     cfn, tfn, pfn, mfn, lfn, tlfn, rfn, ofn, 
                     load, it_save,
                     C0,
                     Nph, phase, nppp,
                     material,
                     scheme, alpha, beta,
                     compatibility_precision, divs_precision, macro_precision,
                     convergence_test_method,
                     maxiterations,
                     cod);

  /*-----------------------------------------------------------------------------*/
  /* one deleters the temporary files that had been used to specify the problem   */
  /* and that are no longer useful                                               */
  /*-----------------------------------------------------------------------------*/

  if (test_temporary_option_file(cfn)) remove(cfn);
  if (test_temporary_option_file(tfn)) remove(tfn);
  if (test_temporary_option_file(tlfn)) remove(tlfn);
  if (test_temporary_option_file(mfn)) remove(mfn);
  if (test_temporary_option_file(pfn)) remove(pfn);
  if (test_temporary_option_file(lfn)) remove(lfn);
  if (test_temporary_option_file(ofn)) remove(ofn);

  /*#############################################################################*/
  /*                            INITIAL CONDITIONS                               */
  /*#############################################################################*/
  /*=============================================================================*/
  /* initial conditions                                                          */
  /*                                                                             */
  /* there are 2 possibilities:                                                  */
  /* 1-one supposes that everithing is null at time t=0s, the first step given   */
  /*   in load file has to be at a time t > 0                                    */
  /*   A time preceding t=0 is introduced to ensure that "extrapolation"         */
  /*   at very beginning of load step will give a "good" initialization of       */
  /*   the iterative process.                                                    */
  /* 2-the computation is supposed to have been interrupted, the two last steps  */
  /*   must have been stored. One enters the name of a saving file to re-start.  */
  /*   The re-start point can be anywhere in the loading process entered with    */
  /*   the file entered above.                                                   */
  /*   (THE 2d CASE HAS NOT BEEN YET IMPLEMENTED)                                */
  /*=============================================================================*/
  /* CASE 1  
                                                                    */
#ifdef DEBUG
  printf("craft.c : enter time_zero, it_save= %d\n", it_save);
#endif

/* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef HARMONIC
  time_zero_harmonic( &load, &E, &S , Nph, nppp,
    phase_epsilon, phase_previous_epsilon,
    ((LE_param *)(C0.parameters))->sub.ile->E, 
    ((LE_param *)(C0.parameters))->sub.ile->nu,
    (rfn==NULL), it_save-1);
  if (rfn != NULL){

#ifdef DEBUG
  printf("craft.c : from time_zero_harmonic(), rfn != NULL\n");
#endif

    for(i=0;i<6;i++) {
      E[it_save][i]   = E_cur[i];
      E[it_save-1][i] = E_prev[i];
      S[it_save][i]   = 0.;
    }
  }
#else
  time_zero( &load, &E, &S , Nph, nppp,
    phase_epsilon, phase_previous_epsilon,
    ((LE_param *)(C0.parameters))->sub.ile->E, 
    ((LE_param *)(C0.parameters))->sub.ile->nu,
    (rfn==NULL), it_save-1);
  if (rfn != NULL){
    for(i=0;i<6;i++) {
      E[it_save][i]   = E_cur[i];
      E[it_save-1][i] = E_prev[i];
      S[it_save][i]   = 0.;
    }
  }
#endif

#ifdef DEBUG
  printf("craft.c : exit time_zero\n");
#endif

  /*=============================================================================*/
  /* header of output file "*.res"                                               */
  /*-----------------------------------------------------------------------------*/
  char *fileres;
  FILE *fres;
  fileres=(char *)malloc(strlen(cod.generic_name)+strlen(".res")+1);
  strcpy(fileres,cod.generic_name);
  strcat(fileres,".res");


  fres=fopen(fileres,"w");

  fprintf(fres,"#\n");

  fprintf(fres,"#");
  for(i=0;i<1+1+3+6+6+1;i++) {
    for(j=0;j<35+1;j++) {
      fprintf(fres,"=");
    }
  }
  fprintf(fres,"\n#");
  fprintf(fres,"%-19s ","load step");
  fprintf(fres,"%-15s ","iterations");
  fprintf(fres,"%-20s ","div(s) error");
  fprintf(fres,"%-20s ","compatibility error");
  fprintf(fres,"%-20s ","E-S error");
#ifdef HARMONIC
  fprintf(fres,"%-41s %-41s %-41s %-41s %-41s %-41s ","E11","E22","E33","E12","E13","E23");
  fprintf(fres,"%-41s %-41s %-41s %-41s %-41s %-41s ","S11","S22","S33","S12","S13","S23");
#else
  fprintf(fres,"%-20s %-20s %-20s %-20s %-20s %-20s ","E11","E22","E33","E12","E13","E23");
  fprintf(fres,"%-20s %-20s %-20s %-20s %-20s %-20s ","S11","S22","S33","S12","S13","S23");
#endif
  fprintf(fres,"%-20s ","T");
  fprintf(fres,"\n");

  fprintf(fres,"#");
  for(i=0;i<1+1+3+6+6+1;i++) {
    for(j=0;j<35+1;j++) {
      fprintf(fres,"-");
    }
  }
  fprintf(fres,"\n");
  fclose(fres);

#ifdef DEBUG
  printf("craft.c : START OF THE LOADING\n");
#endif

  /*#############################################################################*/
  /*                            START OF THE LOADING                             */
  /*#############################################################################*/

  /*=============================================================================*/
  for(it=it_save+1; it < load.Nt; it++) {
    /* one starts loading at step 2 as step 0 and 1 are supposed to be "pre-loading"
       steps, chosen to give a right initialization of iterative process when
       extrapolated                                                              */

    /*---------------------------------------------------------------------------*/
    /* update C0 parameter if requiered                                          */
    /*---------------------------------------------------------------------------*/
    if (C0_line_command[0]=='u'){
//      printf("update C0\n");
      status = update_C0parameters((LE_param *)C0.parameters,material,orientation,Nph,scheme,load.step[it]);
    }

#ifdef DEBUG
  printf("\n###\n");
  printf("craft.c : LOOP for it = %d  t=%f \n",it,load.step[it].time);
#endif
    /*---------------------------------------------------------------------------*/
    dt = load.step[it].time-load.step[it-1].time;

    /*---------------------------------------------------------------------------*/
    /*  extrapolation of data for a better initalization of iterative process    */
    /*---------------------------------------------------------------------------*/
    start_timer(CRAFT_TIMER_EXTRAPOL);

#ifdef DEBUG
    printf("craft.c : RUN material[iph].extrapolate_variables(), it= %d\n",it);
#endif

    if ( fabs((load.step[it].time-load.step[it-1].time)
             -(load.step[it-1].time-load.step[it-2].time)) > 1.e-10 ) {
      //      printf("coucou\n");
    }
    for(iph=0; iph<Nph; iph++) {
      status=material[iph].extrapolate_variables(
              material[iph].parameters,
              orientation[iph],
              &variables[iph],
              load.step[it-2].time, load.step[it-1].time, load.step[it].time);
    }

#ifdef HARMONIC

#ifdef DEBUG
    printf("craft.c : RUN extrapol_harmonic(), it= %d\n",it);
#endif
    /* added for harmonic implementation 2017/07/05  J. Boisse */
    extrapol_harmonic( 6, load.step[it-2].time, E[it-2],
                 load.step[it-1].time, E[it-1],
                 load.step[it].time,   E[it]);    

#ifdef DEBUG
  printf("E[it-2]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[it-2][i]),cimag(E[it-2][i]));
  }
  printf("E[it-1]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[it-1][i]),cimag(E[it-1][i]));
  }
  printf("E[it]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[it][i]),cimag(E[it][i]));
  }
#endif

#else
    extrapol( 6, load.step[it-2].time, E[it-2],
                 load.step[it-1].time, E[it-1],
                 load.step[it].time,   E[it]);
#endif
    stop_timer(CRAFT_TIMER_EXTRAPOL);

#ifdef DEBUG
    printf("craft.c : Set the temperature field to the prescribed overall temperature \n");
#endif

    /*---------------------------------------------------------------------------*/
    /* Set the temperature field to the prescribed overall temperature.          */
    /* Ractically, it consists in setting the variable "temperature" to the      */
    /* prescribed overall temperature in every phases where it has been defined. */
    /*                                                                           */
    /*---------------------------------------------------------------------------*/
    {
      double *phase_temperature[Nph];

      for(iph=0;iph<Nph;iph++) {
        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        Variable *var;
        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        var = (*material[iph].ptr_variable)( "temperature", &variables[iph], material[iph].parameters);
        /* in the case where a variable temperature has been defined, the local field
           of temperature is set to the overall prescribed temperature.
        */
        if (var != (Variable *)0){
          phase_temperature[iph] = (double *)var->values;
          
          /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
          double T =  Temperature(load.step[it].time, &temperature_loading);
#pragma omp parallel for                          \
  shared(iph)                                     \
  shared(phase_temperature, nppp, T)              \
  private(i)
          for(i=0; i<nppp[iph]; i++) {
            phase_temperature[iph][i] = T;
          }
          /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        }
        
        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      }

    }

#ifdef DEBUG
    printf("craft.c : applies constitutive law \n");
#endif

    /*---------------------------------------------------------------------------*/
    /* applies constitutive law                                                  */
    /*---------------------------------------------------------------------------*/
    start_timer(CRAFT_TIMER_BEHAVIOR);

    for(iph=0; iph<Nph; iph++) {
      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] -= phase_epsilon_thermal[iph][i][j];
          }
        }
        
      }


#ifdef HARMONIC
    /* added for harmonic implementation 2017/07/05  J. Boisse */
      material[iph].behavior(
          material[iph].parameters,
          orientation[iph],
          &variables[iph],
          load.step[it].time);
#else
      material[iph].behavior(
          material[iph].parameters,
          orientation[iph],
          &variables[iph],
          dt);
#endif

      if ( phase_epsilon_thermal[iph]!=(double (*)[6])0 ){
#pragma omp parallel for			\
  shared(iph)					\
  shared(phase_epsilon, phase_epsilon_thermal, nppp)	\
  shared(phase_previous_epsilon)	\
  private(i, j)
        for(i=0; i<nppp[iph]; i++) {
          for(j=0;j<6;j++) {
            phase_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
            phase_previous_epsilon[iph][i][j] += phase_epsilon_thermal[iph][i][j];
          }
        }

      }
      
    }
 
#ifdef DEBUG
    printf("craft.c : Check that variables has no NAN value anymore\n");
#endif   
    
    /* Check that variables has no NAN value anymore */
    /* (ie they have all been provided for all phases) */
    if (isnan(t_save)){
      status = check_finite(variables, Nph, 1);
      if (status!=0){
        fprintf(stderr, "Should not be NAN here!\n");
        exit(0);
      }
      t_save = 0;
    }
    
    stop_timer(CRAFT_TIMER_BEHAVIOR);

#ifdef DEBUG
    printf("craft.c : iterates until convergence has been reached\n");
#endif

    /*---------------------------------------------------------------------------*/
    /* iterates until convergence has been reached                               */
    /*---------------------------------------------------------------------------*/
    //    fprintf(stdout,"scheme=%d\n",scheme);
    switch(scheme){
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case CRAFT_BASIC_SCHEME:

/* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef HARMONIC

#ifdef DEBUG
    printf("craft.c : RUN basic_scheme_harmonic()\n");
#endif

      status = basic_scheme_harmonic( 
                            Nph,
                            nppp,
                            index,
                            orientation,
                            material,

                            variables,

                            load.type,
                            load.step[it],
                            E[it], S[it],
                            (LE_param *)C0.parameters,

                            dt,
                            phase_epsilon,
                            phase_previous_epsilon,
                            phase_sigma,
                            phase_epsilon_thermal,
                            
                            &image,

                            &compatibility_error,
                            &divs_error,
                            &macro_error,
                            compatibility_precision,
                            divs_precision,
                            macro_precision,
                            maxiterations,
                            
                            &niter
                             );
#else
      status = basic_scheme( 
                            Nph,
                            nppp,
                            index,
                            orientation,
                            material,

                            variables,

                            load.type,
                            load.step[it],
                            E[it], S[it],
                            (LE_param *)C0.parameters,

                            dt,
                            phase_epsilon,
                            phase_previous_epsilon,
                            phase_sigma,
                            phase_epsilon_thermal,
                            
                            &image,

                            &compatibility_error,
                            &divs_error,
                            &macro_error,
                            compatibility_precision,
                            divs_precision,
                            macro_precision,
                            maxiterations,
                            
                            &niter
                             );
#endif
      if (status<0) {
        fprintf(stderr,"Convergence problems in basic iterative scheme. \n");
        switch(status){
        case -1:
          fprintf(stderr,"craft error: \n");
          fprintf(stderr,"    div(sigma)=%f\n",divs_error);
          break;
        case -2:
          fprintf(stderr,"craft error: \n");
          fprintf(stderr,"    macro error=%f\n",macro_error);
          break;
        }
        fprintf(stderr,"CraFT stops.\n");
        exit(EXIT_FAILURE);
      }
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case CRAFT_LAGRANGIAN_SCHEME:
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case CRAFT_MILTON_EYRE_SCHEME:
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case CRAFT_ALPHA_BETA_SCHEME:
/* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef HARMONIC
      status = alpha_beta_scheme_harmonic( 
                            Nph,
                            nppp,
                            index,
                            orientation,
                            material,

                            variables,

                            load.type,
                            load.step[it],
                            E[it], S[it],
                            (LE_param *)C0.parameters,

                            phase_epsilon,
                            dt,
                            phase_previous_epsilon,
                            phase_sigma,

                            &image,

                            &compatibility_error,
                            &divs_error,
                            &macro_error,
                            compatibility_precision,
                            divs_precision,
                            macro_precision,
                            maxiterations, 

                            &niter,
                            &alpha,
                            &beta
                             );
#else
      status = alpha_beta_scheme( 
                            Nph,
                            nppp,
                            index,
                            orientation,
                            material,

                            variables,

                            load.type,
                            load.step[it],
                            E[it], S[it],
                            (LE_param *)C0.parameters,

                            phase_epsilon,
                            dt,
                            phase_previous_epsilon,
                            phase_sigma,

                            &image,

                            &compatibility_error,
                            &divs_error,
                            &macro_error,
                            compatibility_precision,
                            divs_precision,
                            macro_precision,
                            maxiterations, 

                            &niter,
                            &alpha,
                            &beta
                             );
#endif

      if (status<0) {
        fprintf(stderr,"Convergence problems in alpha-beta iterative scheme. \n");
        switch(status){
        case -1:
          fprintf(stderr,"craft error: \n");
          fprintf(stderr,"    div(sigma)=%f\n",divs_error);
          break;
        case -2:
          fprintf(stderr,"craft error: \n");
          fprintf(stderr,"    macro error=%f\n",macro_error);
          break;
        case -3:
          fprintf(stderr,"craft error: \n");
          fprintf(stderr,"    compatibility error=%f\n",compatibility_error);
          break;
        }
        fprintf(stderr,"CraFT stops.\n");
        exit(EXIT_FAILURE);
      }
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    default:
      fprintf(stderr,"CraFT error: invalid iterative scheme %d\n",scheme);
      exit(EXIT_FAILURE);
      break;
    }

#ifdef DEBUG
    printf("craft.c : convergence has been reached  \n");
#endif

    /*---------------------------------------------------------------------------*/
    /* convergence has been reached                                              */
    /* macroscopic strain and stress are stored in ".res" file                   */
    /*---------------------------------------------------------------------------*/

#ifdef DEBUG
    printf("craft.c : macroscopic strain and stress are stored in *.res file  \n");
#endif

/* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef HARMONIC
    /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
    /* CAN BE BETTER */
    {
      double T =  Temperature(load.step[it].time, &temperature_loading);
      fres=fopen(fileres,"a");
      
      fprintf(fres, "%-20.13g %-15d %-20.13g %-20.13g %-20.13g ",
              load.step[it].time, niter,divs_error, compatibility_error, macro_error );
      fprintf(fres, "%-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g ",
              creal(E[it][0]), cimag(E[it][0]), creal(E[it][1]), cimag(E[it][1]), creal(E[it][2]), cimag(E[it][2]), creal(E[it][5]), cimag(E[it][5]), creal(E[it][4]), cimag(E[it][4]), creal(E[it][3]), cimag(E[it][3]) );
      fprintf(fres, "%-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g ",
              creal(S[it][0]), cimag(S[it][0]), creal(S[it][1]), cimag(S[it][1]), creal(S[it][2]), cimag(S[it][2]), creal(S[it][5]), cimag(S[it][5]), creal(S[it][4]), cimag(S[it][4]), creal(S[it][3]), cimag(S[it][3]) );
      fprintf(fres, "%-20.13g",T);
      fprintf(fres,"\n");
      
      fclose(fres);
    }

    printf("omega= %f, niter= %d\n",load.step[it].time, niter);
#else
    {
      double T =  Temperature(load.step[it].time, &temperature_loading);
      fres=fopen(fileres,"a");
      
      fprintf(fres, "%-20.13g %-15d %-20.13g %-20.13g %-20.13g ",
              load.step[it].time, niter,divs_error, compatibility_error, macro_error );
      fprintf(fres, "%-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g ",
              E[it][0], E[it][1], E[it][2], E[it][5], E[it][4], E[it][3] );
      fprintf(fres,"%-20.13g %-20.13g %-20.13g %-20.13g %-20.13g %-20.13g ",
              S[it][0], S[it][1], S[it][2], S[it][5], S[it][4], S[it][3] );
      fprintf(fres,"%-20.13g",T);
      fprintf(fres,"\n");
      
      fclose(fres);
    }
#endif


/* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */

    /*-----------------------------------------------------------------------------*/
    /* computes and displays 1st and 2d moments if required                        */
    /*                                                                             */
    /*-----------------------------------------------------------------------------*/
    start_timer(CRAFT_TIMER_POST);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    {
      /* one lists every variable whose moments are required to be stored */
      for(i=0; i<cod.number_of_moments; i++) {

#ifdef DEBUG
    printf("craft.c : computes and displays 1st and 2d moments  \n");
#endif

        if ( !cod.clicmoment[i][it] ) continue;

        int counting=0;
        Variable *tmpvar;
        void *tmp[Nph]; /* isn't it? */
        Variable var[Nph];
        /*
        void **tmp=(void **)0;
        tmp = (void **)malloc(Nph*sizeof(*tmp));
        */

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        if ( fabs(load.step[it].time-cod.time[it])>TINY) {
          fprintf(stderr,"Craft ERROR: an unwaited error occured. Contact developper(s).\n");
          exit(1);
        }

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        /* we look at the state variable (whose name is given by cod.moment_name[i])
           in each phase
        */
        for(iph=0;iph<Nph;iph++) {
          tmpvar = (*material[iph].ptr_variable)(cod.moment_name[i], &variables[iph], material[iph].parameters);

          /* we count the number of empty field */
          if(tmpvar==(Variable *)NULL){
            tmp[iph] = NULL;
            counting++;
          }
          else{
            var[iph] = *tmpvar;
            tmp[iph] = var[iph].values;
          }
        }

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        /* if the field is empty in every phase, it is not worth storing it */
        if (counting==Nph) {
          fprintf(stderr,"warning: field %s is empty \n",cod.moment_name[i]);
        }
        /* name of the file in which moments are to be stored */
        else{
          char *file;
          file=(char *)malloc(
            strlen(cod.generic_name)+
            strlen("_")+
            strlen(cod.moment_name[i])+
            strlen(".mom")+
            1
            );
          sprintf(file,"%s_%s.mom",cod.generic_name,cod.moment_name[i]);
          suppress_string_in_string(file," "); /* cancel every spaces in file string */
          /* is it the first time this file is open?
             if yes, open it as new, otherwise open is as append.
          */
          int asnew=1;
          for(j=0;j<it;j++) {
            if (cod.clicmoment[i][j]==1) asnew=0;
          }
          type[0] = var[0].type[0];
          type[1] = var[0].type[1];
          type[2] = var[0].type[2];

          save_moments( Nph, nppp, phase, orientation, cod.time[it], tmp, type, file , asnew, 1, 1 );

          free(file);
          file=(char *)0;
        }

        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        /* release tmp if necessary */
        for(iph=0;iph<Nph;iph++){
          if( (tmp[iph]!=NULL) && (var[iph].releasable==1) ){
            free(var[iph].label);
            free(var[iph].values);
          }
        }
        /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

      } /* end of loop on moments to be saved */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    } /* end of "save moment" block */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    stop_timer(CRAFT_TIMER_POST);


/* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */

    /*-----------------------------------------------------------------------------*/
    /* creates image file of state variable field, if required                        */
    /*                                                                             */
    /*-----------------------------------------------------------------------------*/
    start_timer(CRAFT_TIMER_POST);

    /*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -*/
    {
      int ii,status;
      int type[3]={0,0,0};
      CraftImage store_image;
      
      type[0]=CRAFT_IMAGE_UNKNOWN;
      craft_image_init( &store_image, type);
      for(ii=0;ii<3;ii++) {
        store_image.s[ii] = geometry.s[ii];
        store_image.p[ii] = geometry.p[ii];
        store_image.n[ii] = geometry.n[ii];
        store_image.nd[ii] = geometry.nd[ii];
      }


      for(i=0; i< cod.number_of_images; i++) {
        if (!cod.clicimage[i][it]) continue;

#ifdef DEBUG
    printf("craft.c : creates image file of state variable field\n");
#endif

        if ( fabs(load.step[it].time-cod.time[it])>TINY) {
          fprintf(stderr,"Craft ERROR: an unwaited error occured. Contact developper(s).\n");
          exit(1);
        }
#ifdef HARMONIC
        store_image.name = (char *)malloc(strlen(cod.generic_name) + strlen("_t=") + 16
                             +strlen("_") + strlen(cod.image_name[i])+1);
        sprintf(store_image.name,"%s_t=%015.8e_%s",
                cod.generic_name, cod.time[it], cod.image_name[i]);
        suppress_string_in_string(store_image.name," "); /* cancel every spaces in file string */
#else
        store_image.name = (char *)malloc(strlen(cod.generic_name) + strlen("_t=") + 16
                             +strlen("_") + strlen(cod.image_name[i])+1);
        sprintf(store_image.name,"%s_t=%015.8e_%s",
                cod.generic_name, cod.time[it], cod.image_name[i]);
        suppress_string_in_string(store_image.name," "); /* cancel every spaces in file string */
#endif
        if( strcmp(cod.image_name[i],"rotation")==0){
          /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
          /* case of the image of the rotation                                                             */
          /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#ifdef DEBUG
    printf("craft.c : case of the image of the rotation\n");
#endif

          double (**phase_tau)[6]=(double (**)[6])0;
          phase_tau = (double (**)[6])malloc(Nph*sizeof(*phase_tau));
          for(iph=0;iph<Nph;iph++) {
            phase_tau[iph] =(double (*)[6])malloc(nppp[iph]*sizeof(*phase_tau[iph]));
          }

          for(iph=0;iph<Nph;iph++) {
            phase_tau[iph] =(double (*)[6])malloc(nppp[iph]*sizeof(*phase_tau[iph]));
          }

          for(iph=0; iph<Nph; iph++) {
            status = linear_elastic( (LE_param *)C0.parameters, &orientation[iph], 
                                     nppp[iph],
                                     phase_epsilon[iph], phase_tau[iph]);
            if(status!=0){
              fprintf(stderr,"craft failed to store image %s\n",store_image.name);
              continue;
            }
          }

          for(iph=0; iph<Nph; iph++) {
#pragma omp parallel for                        \
  default(none)                                 \
  shared(iph)                                   \
  shared(phase_epsilon, phase_sigma, phase_tau, nppp)    \
  private(i, j)                                 
            for(i=0; i<nppp[iph]; i++) {
              for(j=0;j<6;j++) {
                phase_tau[iph][i][j] = phase_sigma[iph][i][j]-phase_tau[iph][i][j];
              }
            }
          }

          
          status = phase_to_image( Nph, nppp, index, (void *)phase_tau, &image);
          if(status!=0){
            fprintf(stderr,"craft failed to store image %s\n",store_image.name);
            continue;
          }

          status = craft_fft_image( &image, -1);
          if(status!=0){
            fprintf(stderr,"craft failed to store image %s\n",store_image.name);
            continue;
          }


          store_image.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D;
          store_image.type[1] = 3;
          store_image.type[2] = 0;

          status=craft_image_alloc( &store_image );
          if(status!=0){
            fprintf(stderr,"craft failed to store image %s\n",store_image.name);
            continue;
          }

          store_image.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D;
          store_image.nd[0] = store_image.nd[0]/2;
          for(i=0;i<3;i++) store_image.p[i] = 1. / ( store_image.p[i]*store_image.n[i] );
          
          double divs_error;
          status=lsp2( &image, &store_image, (LE_param *)C0.parameters , &divs_error );
          if(status!=0){
            fprintf(stderr,"craft failed to store image %s\n",store_image.name);
            continue;
          }
          
          status = craft_fft_image( &store_image, +1);
          if(status!=0){
            fprintf(stderr,"craft failed to store image %s\n",store_image.name);
            continue;
          }

          /* reset image to real space 2d order tensor type */
          image.type[0] = CRAFT_IMAGE_TENSOR2;
          image.type[1] = 6;
          image.type[2] = 0;
          image.nd[0] = image.nd[0]*2;
          for(i=0;i<3;i++) image.p[i] = 1. / ( image.p[i]*image.n[i] );

          
          /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
        }
        else{

#ifdef DEBUG
          printf("craft.c : no rotation\n");
#endif

/* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef HARMONIC
#ifdef DEBUG
          printf("craft.c : phase_to_image_harmonic_variables()\n");
#endif
          status = phase_to_image_harmonic_variables(Nph, index, variables, cod.image_name[i], material, &store_image);
#ifdef DEBUG
          printf("craft.c : %s\n",cod.image_name[i]);
          for(j=0;j<6;j++) {  
            printf("%f+%fi (j,i)=(%d,%d)\n",creal(store_image.pxl.ht2[0][j]),cimag(store_image.pxl.ht2[0][j]),0,j);
            printf("%f+%fi (j,i)=(%d,%d)\n",creal(store_image.pxl.ht2[1][j]),cimag(store_image.pxl.ht2[1][j]),1,j);
            printf("%f+%fi (j,i)=(%d,%d)\n",creal(store_image.pxl.ht2[2][j]),cimag(store_image.pxl.ht2[2][j]),2,j);
          }
#endif


#else
#ifdef DEBUG
    printf("craft.c : phase_to_image_variables()\n");
#endif
          status = phase_to_image_variables(Nph, index, variables, cod.image_name[i], material, &store_image);
#endif
        }

        if (status!=0) {
          fprintf(stderr, "Failed to convert \"%s\" to image.\n", cod.image_name[i]);
          fprintf(stderr, "        ---> image %s not created.\n",cod.image_name[i]);
        }
        else{
#ifdef HARMONIC
          /* i3d format */
          if (cod.im_format == 1 || cod.im_format == 2){
            /* TO BE ADDED for harmonic implementation 2017/10/03 J. Boisse */
          }
          /* vtk format */
          if (cod.im_format == 0 || cod.im_format == 2){

            /* real part */    
            store_image.name = (char *)malloc(strlen(cod.generic_name) + strlen("_t=") + 16
                +strlen("_") + strlen(cod.image_name[i])+1);            
            sprintf(store_image.name,"%s_t=%015.8e_%s_creal",
                cod.generic_name, cod.time[it], cod.image_name[i]);
            suppress_string_in_string(store_image.name," "); /* cancel every spaces in file string */
            store_image.type[0]=CRAFT_IMAGE_HARMONIC_TENSOR2_CREAL;
            write_image(store_image.name, &store_image,"vtk");
            
            /* imaginary part */
            store_image.name = (char *)malloc(strlen(cod.generic_name) + strlen("_t=") + 16
                +strlen("_") + strlen(cod.image_name[i])+1);            
            sprintf(store_image.name,"%s_t=%015.8e_%s_cimag",
                cod.generic_name, cod.time[it], cod.image_name[i]);
            suppress_string_in_string(store_image.name," "); /* cancel every spaces in file string */
            store_image.type[0]=CRAFT_IMAGE_HARMONIC_TENSOR2_CIMAG;
            write_image(store_image.name, &store_image,"vtk");            
            
          }
#else
          /* i3d format */
          if (cod.im_format == 1 || cod.im_format == 2){
            write_image(store_image.name, &store_image,"i3d");
          }
          /* vtk format */
          if (cod.im_format == 0 || cod.im_format == 2){
            write_image(store_image.name, &store_image,"vtk");
          }
#endif
        }
/*        type[0] = CRAFT_IMAGE_UNKNOWN;*/
        craft_image_free( &store_image);
      }  /* end of loop on images to be saved */
    }
    /*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -*/

    if ( (cod.clicvariables!=NULL) && (cod.clicvariables[it]) ){
      file = malloc(strlen(cod.generic_name)+strlen("_t=")+16+strlen("_variables")+1);
      sprintf(file,"%s_t=%015.8e_variables", cod.generic_name, cod.time[it]);
      suppress_string_in_string(file," "); /* cancel every spaces in file string */
      write_variables_all_phases( Nph, variables, file, cfn,
        phase, material_in_phase,
        it, cod.time[it],
        E[it], E[it-1], 1);
    }
    stop_timer(CRAFT_TIMER_POST);

    /*#############################################################################*/
    /*                          END OF THE LOADING                                 */
    /*#############################################################################*/
    /*---------------------------------------------------------------------------*/

    /*---------------------------------------------------------------------------*/
  }
#ifdef DEBUG
    printf("\n###\n");
    printf("craft.c : END OF THE LOADING LOOP\n");
#endif
#ifdef DEBUG
    printf("craft.c : SOME POST-PROCESSINGS\n");
#endif

  /*#############################################################################*/
  /*                         SOME POST-PROCESSINGS                               */
  /*#############################################################################*/
  /*-----------------------------------------------------------------------------*/
  /* display statistics about function calls                                     */
  /*                                                                             */
  /*-----------------------------------------------------------------------------*/
  {
    char *file;
    FILE *f;

    file=(char *)malloc(strlen(cod.generic_name)+strlen(".perf")+1);
    strcpy(file,cod.generic_name);
    strcat(file,".perf");

    if (it_save>1){
      f = fopen(file,"a");
      fprintf(f,"#======================================================================\n");
      fprintf(f,"# Performances concerning the continuation of the previous computation \n");
      fprintf(f,"# from file %s\n", rfn);
      fprintf(f,"# corresponding to a starting time of instant %lf (index %d)\n",
        load.step[it_save].time, it_save);
    }
    else{
      f = fopen(file,"w");
    }


    stop_timer(CRAFT_TIMER_TOTAL);
    print_timers(f);

    fclose(f);
    free(file);
  }
 

  /*=============================================================================*/
  /* We free memory which has been allocated for image */
  status = craft_image_free( &image );
  /* (but we still need image  as it describes size and dimension of the volume )*/
  //  fclose(fres);

  for(iph=0;iph<Nph;iph++){
    material[iph].deallocate_variables(&variables[iph]);
    material[iph].deallocate_parameters(material[iph].parameters);
  }
  free(cfn);
  free(pfn);
  free(ofn);
  free(mfn);
  free(lfn);
  free(fileres);
  
  free(cod.generic_name);
  free(cod.time);
  for(it=0;it<cod.number_of_images;it++){
    free(cod.clicimage[it]);
    free(cod.image_name[it]);
  }
  free(cod.clicimage);
  free(cod.image_name);
  for(it=0;it<cod.number_of_moments;it++){
    free(cod.clicmoment[it]);
    free(cod.moment_name[it]);
  }
  free(cod.clicmoment);
  free(cod.moment_name);
  
  free(C0_line_command);
  C0.deallocate_parameters(C0.parameters);
  
  for(iph=0;iph<Nph;iph++){
    free(index[iph]);
  }
  free(E);
  free(S);
  free(index);
  free(load.step);
  free(material);
  free(material_in_phase);
  free(nppp);
  free(orientation);
  free(phase);
  free(phase_epsilon);
  free(phase_previous_epsilon);
  free(phase_sigma);
  free(variables);
  fftw_cleanup();
#ifdef hdf5
  H5close();
#endif

#ifdef DEBUG
    printf("\n###\nEND (Bye !)\n###\n\n");
#endif

  /*=============================================================================*/
  if (verbose) {
    printf("Bye\n");
  }
  /*=============================================================================*/
  
 return 0;
}
