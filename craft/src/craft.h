#ifndef __CRAFT__
#define __CRAFT__

#define CRAFT_VERSION "1.1.0"


/* level of verbosity of CraFT program */
#ifdef __MAIN__
int verbose=0;
#else
extern int verbose;
#endif


#endif
