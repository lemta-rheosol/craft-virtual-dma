#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <craftimage.h>
#include <materials.h>
#include <LE_materials.h>
#include <loading.h>

#include <variables.h>
#include <io_variables.h>
#include <utils.h>
#include <meca.h>

#include <euler.h>
#include <moments.h>

#include <craft_compatibility_equilibrium.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/*********************************************************************************/
/* 
   error on equilibrium calculated as:
   square root of the sum of all frequencies (except 0) of the square of divergence of the stress,
   normalized by the Frobenius norm of the macroscopic stress
   sqrt( Sum( | ksi.sigma(ksi) |**2 ) ) / sqrt( Sij.Sij )
   
 */
int equilibrium_divs_sum( CraftImage *image, double *error){
  int status;

  double complex divs[3];
  int i0,i1,i2;

  int i,j,k;

  
  double *ksi[3];

  double complex *sigma;
  int ic;
  double err;
  double errsum;

  *error=0.;
  status=0;

  double S[6];
  //  printf(" equilibrium_divs_sum\n");
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"equilibrium_divs_sum error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*image->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= image->n[i]/2; k++) {
      ksi[i][k] = image->p[i]*k;
    }
    for(k=image->n[i]/2+1; k<image->n[i]; k++) {
      ksi[i][k] = image->p[i]*(k-image->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* macroscopic stress */
  for(j=0;j<6;j++) {
    S[j] = creal(image->pxl.ft2[0][j]);
  }
  /*-----------------------------------------------------------------------------*/
#pragma omp parallel                                    \
  default(none)                                         \
  shared(image,ksi,error)                               \
  private(i0,i1,i2,i,err,errsum,ic, sigma,divs)
  {
    errsum = 0.;
    
#pragma omp for
    for(i=0; i< (image->n[0]/2+1) * image->n[1] * image->n[2] ; i++) {
      i2 = i / ( (image->n[0]/2+1) * image->n[1] );
      i1 = ( i - i2 * (image->n[0]/2+1) * image->n[1] ) /  (image->n[0]/2+1) ;
      i0 =  i - i2 * (image->n[0]/2+1) * image->n[1] - i1 * (image->n[0]/2+1);

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      sigma = image->pxl.ft2[image->nd[0]*(image->nd[1]*i2+i1) + i0];
      
      divs[0] = 
        ksi[0][i0] * sigma[0] + 
        ksi[1][i1] * sigma[5] +
        ksi[2][i2] * sigma[4] ;
      divs[1] = 
        ksi[0][i0] * sigma[5] + 
        ksi[1][i1] * sigma[1] +
        ksi[2][i2] * sigma[3] ;
      divs[2] = 
        ksi[0][i0] * sigma[4] + 
        ksi[1][i1] * sigma[3] +
        ksi[2][i2] * sigma[2] ;
      
      err = creal(divs[0]*conj(divs[0]))+creal(divs[1]*conj(divs[1]))+creal(divs[2]*conj(divs[2]));
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      ic=0;
      if( (image->n[0]%2==0) && (i0==image->n[0]/2) ) ic++;
      if( (image->n[1]%2==0) && (i1==image->n[1]/2) ) ic++;
      if( (image->n[2]%2==0) && (i2==image->n[2]/2) ) ic++;
      switch(ic){
      case 3:
        err *= 0.125;
        break;
      case 2:
        err *= 0.25;
        break;
      case 1:
        err *= 0.5;
        break;
      }
      
      if (
          (i0!=0) &&
          !( (image->n[0]%2==0) && (i0==image->n[0]/2) ) 
          ) {
        err *= 2.;
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      errsum += err;
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    }
#pragma omp critical
    {
      *error += errsum;
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* divs_error contains in fact div(sigma) squared, on has to compute its
     square root and to normalize it                                         */
  double norm;
  norm =
    S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
  
  *error = sqrt( *error / norm );
  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;
}

/* TO MODIFY for harmonic implementation 2017/07/05 J. Boisse */
/* MODIFIED for harmonic implementation 2018/07/09 J. Boisse */
int equilibrium_divs_sum_harmonic( CraftImage *image, double *error){
  int status;

  double complex divs[3];
  int i0,i1,i2;

  int i,j,k;

  
  double *ksi[3];

  double complex *sigma;
  int ic;
  double err;
  double errsum;

  double dx,dy,dz;/* 2019-06-08 norm computed the same way than in amitex */
  double nrm;     /* 2019-06-08 norm computed the same way than in amitex */
  double nrmsum;  /* 2019-06-08 norm computed the same way than in amitex */
  double norm;    /* 2019-06-08 norm computed the same way than in amitex */

  *error=0.;
  norm=0.;    /* 2019-06-08 norm computed the same way than in amitex */
  status=0;

  double S[6];
  //  printf(" equilibrium_divs_sum\n");
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"equilibrium_divs_sum error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  /*-----------------------------------------------------------------------------*/

  dx = 1. / ( image->p[0]*image->n[0] );     /* 2019-06-08 norm computed the same way than in amitex */
  dy = 1. / ( image->p[1]*image->n[1] );     /* 2019-06-08 norm computed the same way than in amitex */
  dz = 1. / ( image->p[2]*image->n[2] );     /* 2019-06-08 norm computed the same way than in amitex */ 
  //printf("dx = %g ; dy = %g ; dz = %g \n",dx,dy,dz);

  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*image->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= image->n[i]/2; k++) {
      ksi[i][k] = image->p[i]*k;
    }
    for(k=image->n[i]/2+1; k<image->n[i]; k++) {
      ksi[i][k] = image->p[i]*(k-image->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* macroscopic stress */
  for(j=0;j<6;j++) {
    /* MODIFIED for harmonic implementation 2018/07/09 J. Boisse */
    //S[j] = creal(image->pxl.ft2[0][j]);
    S[j] = image->pxl.ft2[0][j];
  }
  /*-----------------------------------------------------------------------------*/
#pragma omp parallel                                    \
  default(none)                                         \
  shared(image,ksi,error)                               \
  private(i0,i1,i2,i,err,errsum,ic, sigma,divs)         \
  shared(norm,dx,dy,dz)                                 \
  private(nrm,nrmsum)
  {
    errsum = 0.;
    nrmsum = 0.;    /* 2019-06-08 norm computed the same way than in amitex */
    
#pragma omp for
    for(i=0; i< image->n[0] * image->n[1] * image->n[2] ; i++) {
      i2 = i / ( image->n[0] * image->n[1] );
      i1 = ( i - i2 * image->n[0] * image->n[1] ) /  (image->n[0]) ;
      i0 =  i - i2 * image->n[0] * image->n[1] - i1 * (image->n[0]);
/* MODIFIED for harmonic implementation 2018/07/09 J. Boisse */

//    for(i=0; i< (image->n[0]/2+1) * image->n[1] * image->n[2] ; i++) {
//      i2 = i / ( (image->n[0]/2+1) * image->n[1] );
//      i1 = ( i - i2 * (image->n[0]/2+1) * image->n[1] ) /  (image->n[0]/2+1) ;
//      i0 =  i - i2 * (image->n[0]/2+1) * image->n[1] - i1 * (image->n[0]/2+1);

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      sigma = image->pxl.ft2[image->nd[0]*(image->nd[1]*i2+i1) + i0];
      
      divs[0] = 
        ksi[0][i0] * sigma[0] + 
        ksi[1][i1] * sigma[5] +
        ksi[2][i2] * sigma[4] ;
      divs[1] = 
        ksi[0][i0] * sigma[5] + 
        ksi[1][i1] * sigma[1] +
        ksi[2][i2] * sigma[3] ;
      divs[2] = 
        ksi[0][i0] * sigma[4] + 
        ksi[1][i1] * sigma[3] +
        ksi[2][i2] * sigma[2] ;
      
      err = creal(divs[0]*conj(divs[0]))+creal(divs[1]*conj(divs[1]))+creal(divs[2]*conj(divs[2]));

      /* 2019-06-08 norm computed the same way than in amitex */
      nrm = (creal(sigma[0]*conj(sigma[0])) + creal(sigma[5]*conj(sigma[5])) + creal(sigma[4]*conj(sigma[4])) ) *pow(dy*dz,2) +
            (creal(sigma[5]*conj(sigma[5])) + creal(sigma[1]*conj(sigma[1])) + creal(sigma[3]*conj(sigma[3])) ) *pow(dx*dz,2) +
            (creal(sigma[4]*conj(sigma[4])) + creal(sigma[3]*conj(sigma[3])) + creal(sigma[2]*conj(sigma[2])) ) *pow(dx*dy,2) ;
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      ic=0;
      if( (image->n[0]%2==0) && (i0==image->n[0]/2) ) ic++;
      if( (image->n[1]%2==0) && (i1==image->n[1]/2) ) ic++;
      if( (image->n[2]%2==0) && (i2==image->n[2]/2) ) ic++;
      switch(ic){
      case 3:
        err *= 0.125;
        nrm *= 0.125;  /* 2019-06-08 norm computed the same way than in amitex */
        //printf("coucou err case 3\n");
        break;
      case 2:
        err *= 0.25;
        nrm *= 0.25;  /* 2019-06-08 norm computed the same way than in amitex */
        //printf("coucou err case 2\n");
        break;
      case 1:
        err *= 0.5;
        nrm *= 0.5;  /* 2019-06-08 norm computed the same way than in amitex */
        //printf("coucou err case 1\n");
        break;
      }
 
      /* MODIFIED for harmonic implementation 2018/07/09 J. Boisse */     
      //if (
      //    (i0!=0) &&
      //    !( (image->n[0]%2==0) && (i0==image->n[0]/2) ) 
      //    ) {
      //  err *= 2.;
      //}
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      errsum += err;
      nrmsum += nrm;  /* 2019-06-08 norm computed the same way than in amitex */
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    }
#pragma omp critical
    {
      *error += errsum;
      norm += nrmsum;  /* 2019-06-08 norm computed the same way than in amitex */
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* divs_error contains in fact div(sigma) squared, on has to compute its
     square root and to normalize it     
                                    */
  //double norm;   /* commented : 2019-06-08 norm computed the same way than in amitex */
  /* MODIFIED for harmonic implementation 2018/07/09 J. Boisse */
  //norm = creal(S[0])*conj(S[0]) + creal(S[1])*conj(S[1]) + creal(S[2])*conj(S[2]) + 2.*( creal(S[3])*conj(S[3]) + creal(S[4])*conj(S[4]) + creal(S[5])*conj(S[5]) );
  //*error = sqrt( *error / norm );   /* commented : 2019-06-08 norm computed the same way than in amitex */

  *error = 2.*acos(-1.) * sqrt( *error / norm ) * dx*dy*dz;    /* 2019-06-08 norm computed the same way than in amitex */
  //printf("error = %g\n",*error);
  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;
}

/*********************************************************************************/
/* 
   error on equilibrium calculated as:
   max of all frequencies (except 0) of the module of the divergence of the stress,
   normalized by the Frobenius norm of the macroscopic stress
   sqrt( max( | ksi.sigma(ksi) |**2 ) ) / sqrt( Sij.Sij )
   
 */
int equilibrium_divs_max( CraftImage *image, double *error){
  int status;

  double complex divs[3];
  int i0,i1,i2;

  int i,j,k;
  
  double *ksi[3];

  double complex *sigma;
  int ic;
  double err;
  double errsum;

  *error=0.;
  status=0;

  double S[6];
  //  printf(" equilibrium_divs_max\n");
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"equilibrium_divs_max error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*image->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= image->n[i]/2; k++) {
      ksi[i][k] = image->p[i]*k;
    }
    for(k=image->n[i]/2+1; k<image->n[i]; k++) {
      ksi[i][k] = image->p[i]*(k-image->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* macroscopic stress */
  for(j=0;j<6;j++) {
    S[j] = creal(image->pxl.ft2[0][j]);
  }
  /*-----------------------------------------------------------------------------*/
#pragma omp parallel                                    \
  default(none)                                         \
  shared(image,ksi,error)                               \
  private(i0,i1,i2,i,err,errsum,ic, sigma,divs)
  {
    errsum = 0.;
    
#pragma omp for
    for(i=0; i< (image->n[0]/2+1) * image->n[1] * image->n[2] ; i++) {
      i2 = i / ( (image->n[0]/2+1) * image->n[1] );
      i1 = ( i - i2 * (image->n[0]/2+1) * image->n[1] ) /  (image->n[0]/2+1) ;
      i0 =  i - i2 * (image->n[0]/2+1) * image->n[1] - i1 * (image->n[0]/2+1);
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      sigma = image->pxl.ft2[image->nd[0]*(image->nd[1]*i2+i1) + i0];
      
      divs[0] = 
        ksi[0][i0] * sigma[0] + 
        ksi[1][i1] * sigma[5] +
        ksi[2][i2] * sigma[4] ;
      divs[1] = 
        ksi[0][i0] * sigma[5] + 
        ksi[1][i1] * sigma[1] +
        ksi[2][i2] * sigma[3] ;
      divs[2] = 
        ksi[0][i0] * sigma[4] + 
        ksi[1][i1] * sigma[3] +
        ksi[2][i2] * sigma[2] ;
      
      err = creal(divs[0]*conj(divs[0]))+creal(divs[1]*conj(divs[1]))+creal(divs[2]*conj(divs[2]));
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      ic=0;
      if( (image->n[0]%2==0) && (i0==image->n[0]/2) ) ic++;
      if( (image->n[1]%2==0) && (i1==image->n[1]/2) ) ic++;
      if( (image->n[2]%2==0) && (i2==image->n[2]/2) ) ic++;
      switch(ic){
      case 3:
        err *= 0.125;
        break;
      case 2:
        err *= 0.25;
        break;
      case 1:
        err *= 0.5;
        break;
      }
      
      if (
          (i0!=0) &&
          !( (image->n[0]%2==0) && (i0==image->n[0]/2) ) 
          ) {
        err *= 2.;
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      errsum = (err>errsum? err : errsum);
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    }
#pragma omp critical
    {
      *error = (errsum>*error? errsum: *error);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* divs_error contains in fact div(sigma) squared, on has to compute its
     square root and to normalize it                                         */
  double norm;
  norm =
    S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
  
  *error = sqrt( *error / norm );
  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;
}
/*********************************************************************************/
/* Error on equilibrium following Monchiet & Bonnet

 */
int equilibrium_MB( CraftImage *image, double *error){
  int status;

  double complex divs[3];
  double complex dd;
  int i0,i1,i2;

  int i,j,k;
  
  double *ksi[3];
  double xi2;

  double complex *sigma;
  int ic;
  double err;
  double errsum;

  *error=0.;
  status=0;

  double S[6];
  //  printf("coucou\n");
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"euilibrium_MB error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*image->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= image->n[i]/2; k++) {
      ksi[i][k] = image->p[i]*k;
    }
    for(k=image->n[i]/2+1; k<image->n[i]; k++) {
      ksi[i][k] = image->p[i]*(k-image->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* macroscopic stress */
  for(j=0;j<6;j++) {
    S[j] = creal(image->pxl.ft2[0][j]);
  }
  /*-----------------------------------------------------------------------------*/
#pragma omp parallel                                    \
  default(none)                                         \
  shared(image,ksi,error)                               \
  private(i0,i1,i2,i,err,errsum,ic, sigma,divs, dd, xi2)
  {
    errsum = 0.;
    
#pragma omp for
    for(i=1; i< (image->n[0]/2+1) * image->n[1] * image->n[2] ; i++) {
      i2 = i / ( (image->n[0]/2+1) * image->n[1] );
      i1 = ( i - i2 * (image->n[0]/2+1) * image->n[1] ) /  (image->n[0]/2+1) ;
      i0 =  i - i2 * (image->n[0]/2+1) * image->n[1] - i1 * (image->n[0]/2+1);
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      //      if ( (i0==0)&&(i1==0)&&(i2==0)) continue;
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      sigma = image->pxl.ft2[image->nd[0]*(image->nd[1]*i2+i1) + i0];
      
      divs[0] = 
        ksi[0][i0] * sigma[0] + 
        ksi[1][i1] * sigma[5] +
        ksi[2][i2] * sigma[4] ;
      divs[1] = 
        ksi[0][i0] * sigma[5] + 
        ksi[1][i1] * sigma[1] +
        ksi[2][i2] * sigma[3] ;
      divs[2] = 
        ksi[0][i0] * sigma[4] + 
        ksi[1][i1] * sigma[3] +
        ksi[2][i2] * sigma[2] ;
      
      xi2 =  ksi[0][i0]*ksi[0][i0] + ksi[1][i1]*ksi[1][i1] + ksi[2][i2]*ksi[2][i2] ;
      xi2 = 1./xi2;
      
      dd = ksi[0][i0]*divs[0] + ksi[1][i1]*divs[1] + ksi[2][i2]*divs[2];
      
      err = xi2 * 
        ( 2. * 
          (
           creal(divs[0]*conj(divs[0]))+creal(divs[1]*conj(divs[1]))+creal(divs[2]*conj(divs[2]))
           )
          -
          xi2 * dd * conj(dd)
          )
        ;
      
      
      err = err /
        ( sigma[0]*conj(sigma[0]) + sigma[1]*conj(sigma[1]) + sigma[2]*conj(sigma[2]) 
          + 2.*(
                sigma[3]*conj(sigma[3]) + sigma[4]*conj(sigma[4]) + sigma[5]*conj(sigma[5]) 
                )
          );
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      ic=0;
      if( (image->n[0]%2==0) && (i0==image->n[0]/2) ) ic++;
      if( (image->n[1]%2==0) && (i1==image->n[1]/2) ) ic++;
      if( (image->n[2]%2==0) && (i2==image->n[2]/2) ) ic++;
      switch(ic){
      case 3:
        err *= 0.125;
        break;
      case 2:
        err *= 0.25;
        break;
      case 1:
        err *= 0.5;
        break;
      }
      
      if (
          (i0!=0) &&
          !( (image->n[0]%2==0) && (i0==image->n[0]/2) ) 
          ) {
        err *= 2.;
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      errsum = (err>errsum? err : errsum);
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    }
#pragma omp critical
    {
      *error = (*error > errsum ? *error : errsum );
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* divs_error contains in fact div(sigma) squared, on has to compute its
     square root and to normalize it                                         */

  double norm;

  /* Monchiet & Bonnet do not normalize by the macroscopic stress */
  /*
  norm =
    S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
  */

  norm=1.;

  *error = sqrt( *error / norm );

  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;
}
/*********************************************************************************/
/* Error on equilibrium following Monchiet & Bonnet (version corrigee)

 */
int equilibrium_MB2( CraftImage *image, double *error){
  int status;

  double complex divs[3];
  double complex dd;
  int i0,i1,i2;

  int i,j,k;
  
  double *ksi[3];
  double xi2;

  double complex *sigma;
  int ic;
  double err;
  double errsum;
  double s2, s2sum;
  double ss2;

  *error=0.;
  ss2=0.;
  status=0;

  double S[6];
  //  printf(" equilibrium_MB2\n");
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"equilibrium_MB2 error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*image->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= image->n[i]/2; k++) {
      ksi[i][k] = image->p[i]*k;
    }
    for(k=image->n[i]/2+1; k<image->n[i]; k++) {
      ksi[i][k] = image->p[i]*(k-image->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* macroscopic stress */
  for(j=0;j<6;j++) {
    S[j] = creal(image->pxl.ft2[0][j]);
  }
  /*-----------------------------------------------------------------------------*/
#pragma omp parallel                                    \
  default(none)                                         \
  shared(image,ksi,error,ss2)                                              \
  private(i0,i1,i2,i,err,errsum,ic, sigma,divs, dd, xi2, s2, s2sum)
  {
    errsum = 0.;
    s2sum = 0.;
#pragma omp for
    for(i=0; i< (image->n[0]/2+1) * image->n[1] * image->n[2] ; i++) {
      i2 = i / ( (image->n[0]/2+1) * image->n[1] );
      i1 = ( i - i2 * (image->n[0]/2+1) * image->n[1] ) /  (image->n[0]/2+1) ;
      i0 =  i - i2 * (image->n[0]/2+1) * image->n[1] - i1 * (image->n[0]/2+1);
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      
      sigma = image->pxl.ft2[image->nd[0]*(image->nd[1]*i2+i1) + i0];
      
      divs[0] = 
        ksi[0][i0] * sigma[0] + 
        ksi[1][i1] * sigma[5] +
        ksi[2][i2] * sigma[4] ;
      divs[1] = 
        ksi[0][i0] * sigma[5] + 
        ksi[1][i1] * sigma[1] +
        ksi[2][i2] * sigma[3] ;
      divs[2] = 
        ksi[0][i0] * sigma[4] + 
        ksi[1][i1] * sigma[3] +
        ksi[2][i2] * sigma[2] ;
      
      if ( (i0==0)&&(i1==0)&&(i2==0)) err=0.;
      else{
        xi2 =  ksi[0][i0]*ksi[0][i0] + ksi[1][i1]*ksi[1][i1] + ksi[2][i2]*ksi[2][i2] ;
        xi2 = 1./xi2;
        
        
        
        dd = ksi[0][i0]*divs[0] + ksi[1][i1]*divs[1] + ksi[2][i2]*divs[2];
        
        err = xi2 * 
          ( 2. * 
            (
             creal(divs[0]*conj(divs[0]))+creal(divs[1]*conj(divs[1]))+creal(divs[2]*conj(divs[2]))
             )
            -
            xi2 * dd * conj(dd)
            )
        ;

      }
      s2 = 
        ( sigma[0]*conj(sigma[0]) + sigma[1]*conj(sigma[1]) + sigma[2]*conj(sigma[2]) 
          + 2.*(
                sigma[3]*conj(sigma[3]) + sigma[4]*conj(sigma[4]) + sigma[5]*conj(sigma[5]) 
                )
          );
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      ic=0;
      if( (image->n[0]%2==0) && (i0==image->n[0]/2) ) ic++;
      if( (image->n[1]%2==0) && (i1==image->n[1]/2) ) ic++;
      if( (image->n[2]%2==0) && (i2==image->n[2]/2) ) ic++;
      switch(ic){
      case 3:
        err *= 0.125;
        break;
      case 2:
        err *= 0.25;
        break;
      case 1:
        err *= 0.5;
        break;
      }
      
      if (
          (i0!=0) &&
          !( (image->n[0]%2==0) && (i0==image->n[0]/2) ) 
          ) {
        err *= 2.;
        s2 *= 2.;
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      //          printf("error=%f %f\n",errsum,err);
      
      errsum += err;
      s2sum += s2;
      
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    }
#pragma omp critical
    {
      *error += errsum;
      //  printf("error======%f\n",*error);
      ss2 += s2sum;
    }
  }
  /*-----------------------------------------------------------------------------*/
  /* divs_error contains in fact div(sigma) squared, on has to compute its
     square root and to normalize it                                         */

  double norm;

  /* Monchiet & Bonnet do not normalize by the macroscopic stress */
  /*
  norm =
    S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
  */

  norm=ss2;;

  *error = sqrt( *error / norm );

  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;
}
/*********************************************************************************/
int compatibility( CraftImage *strain, double *error){

  double *ksi[3];
  double k1,k2,k3;

  int status=0;
  int i1,i2,i3;
  int offset1;
  double complex (*elfin)[6];
  double mksi,mstrain;

  double lmstrain;
  double lerror;

  int i,j,k;
  double complex tmp[6];
  double complex coef1;
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (strain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"error in function compatibility: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*strain->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= strain->n[i]/2; k++) {
      ksi[i][k] = strain->p[i]*k;
    }
    for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
      ksi[i][k] = strain->p[i]*(k-strain->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
 /* Computing the energy of the strain field (signal theory definition) */
  mstrain = 0.;
  *error = 0.;

#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3)                             \
  private(i)                                    \
  shared(strain)                                \
  private(offset1, elfin, coef1)                \
  private(lmstrain)                             \
  shared(mstrain)                               \
  private(k1,k2,k3)                             \
  shared(ksi)                                   \
  private(mksi)                                 \
  private(tmp)                                  \
  private(lerror)                               \
  shared(error)                                 \
  private(j)
  {
    lmstrain=0.;
#pragma omp for
    for(i=0; i< (strain->n[0]/2+1) * strain->n[1] * strain->n[2] ; i++) {
      i3 = i / ( (strain->n[0]/2+1) * strain->n[1] );
      i2 = ( i - i3 * (strain->n[0]/2+1) * strain->n[1] ) /  (strain->n[0]/2+1) ;
      i1 =  i - i3 * (strain->n[0]/2+1) * strain->n[1] - i2 * (strain->n[0]/2+1);

      offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
      elfin = strain->pxl.ft2 + offset1;
      coef1 = (*elfin)[0]*conj((*elfin)[0]) + (*elfin)[1]*conj((*elfin)[1]) + (*elfin)[2]*conj((*elfin)[2])
        +2*((*elfin)[3]*conj((*elfin)[3]) + (*elfin)[4]*conj((*elfin)[4]) + (*elfin)[5]*conj((*elfin)[5]));
      /* if i1 != 0 it has to be counted twice because of negative ksi1 frequencies */
      lmstrain += coef1 * (1.+(i1!=0)) ;
    }
    
#pragma omp critical
    {
      mstrain += lmstrain;
    }
#pragma omp barrier
    //  mstrain = sqrt(mstrain / (strain->n[0]*strain->n[1]*strain->n[2]) );
#pragma omp single
    {
      mstrain = sqrt(mstrain);   /* one MUST NOT divide by n1*n2*n3 because we are in Fourier space */
    }

    /* Checking compatibility equations (in the frequency domain) */
    lerror=0.;
#pragma omp for
    for(i=1; i< (strain->n[0]/2+1) * strain->n[1] * strain->n[2] ; i++) {
      i3 = i / ( (strain->n[0]/2+1) * strain->n[1] );
      i2 = ( i - i3 * (strain->n[0]/2+1) * strain->n[1] ) /  (strain->n[0]/2+1) ;
      i1 =  i - i3 * (strain->n[0]/2+1) * strain->n[1] - i2 * (strain->n[0]/2+1);

      /* Strain is known to be incompatible at the Nyquist frequency
         for each axes which has an even number of pixels
         (TODO : special handle of Nyquist frequency in lspp may not be the best) */
      if (
          ( (strain->n[0]%2 == 0) && (i1==strain->n[0]/2) ) ||
          ( (strain->n[1]%2 == 0) && (i2==strain->n[1]/2) ) ||
          ( (strain->n[2]%2 == 0) && (i3==strain->n[2]/2) ) 
          ) {
        continue;
      }
      
      
      offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
      elfin = (strain->pxl.ft2 + offset1);
      k1 = ksi[0][i1];
      k2 = ksi[1][i2];
      k3 = ksi[2][i3];
      
      /* tolerance eps * |ksi|^2 * |strain(ksi=0)| */
      mksi = k1 * k1 + k2 * k2 + k3 * k3;
      //        relerr = ((eps * mstrain) * mksi);
      
      /* Strain is known to be incompatible at the Nyquist frequency
         for each axes which has an even number of pixels
         (TODO : special handle of Nyquist frequency in lspp may not be the best) */
      /*
        if ((i1==strain->nd[0]-1) && (strain->n[0]>1) && (strain->n[0]%2 == 0))
        continue;
        if ((i2==strain->nd[1]/2) && (strain->n[1]>1) && (strain->n[1]%2 == 0))
        continue;
        if ((i3==strain->nd[2]/2) && (strain->n[2]>1) && (strain->n[2]%2 == 0))
        continue;
      */
      
      /* These 6 expressions should be "equal" to zero to ensure compatibility.
         They are redundant, but no subset is sufficient for all cases. */
      tmp[0] = k2*k2*(*elfin)[0] + k1*k1*(*elfin)[1] - 2*k1*k2*(*elfin)[5]; /* 12 */
      tmp[1] = k3*k3*(*elfin)[1] + k2*k2*(*elfin)[2] - 2*k2*k3*(*elfin)[3]; /* 23 */
      tmp[2] = k1*k1*(*elfin)[2] + k3*k3*(*elfin)[0] - 2*k3*k1*(*elfin)[4]; /* 13 */
      
      tmp[3] = k2*k3*(*elfin)[0] - k1*k2*(*elfin)[4] - k1*k3*(*elfin)[5] + k1*k1*(*elfin)[3]; /* 11 */
      tmp[4] = k3*k1*(*elfin)[1] - k2*k3*(*elfin)[5] - k2*k1*(*elfin)[3] + k2*k2*(*elfin)[4]; /* 22 */
      tmp[5] = k1*k2*(*elfin)[2] - k3*k1*(*elfin)[3] - k3*k2*(*elfin)[4] + k3*k3*(*elfin)[5]; /* 33 */
      
      /* normalization of the error */
      for (j = 0; j < 6; j++) {
        tmp[j] = tmp[j] / (mstrain);
      }
      
      for (j = 0; j < 6; j++) {
        lerror = (cabs(tmp[j])>lerror?  cabs(tmp[j]) : lerror);
      }
      
      
    }
#pragma omp critical
    {
      *error =(lerror>*error? lerror : *error);
    }


  }
  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;

}

/* added for harmonic implementation 2018/07/09  J. Boisse */
int compatibility_harmonic( CraftImage *strain, double *error){

  double *ksi[3];
  double k1,k2,k3;

  int status=0;
  int i1,i2,i3;
  int offset1;
  double complex (*elfin)[6];
  double mksi,mstrain;

  double lmstrain;
  double lerror;

  int i,j,k;
  double complex tmp[6];
  double complex coef1;
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if (strain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"error in function compatibility: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  for(i=0;i<3;i++) {
    ksi[i] = (double *)malloc(sizeof(*ksi[i])*strain->n[i]);
  }

  for(i=0;i<3;i++) {
    for(k=0;k<= strain->n[i]/2; k++) {
      ksi[i][k] = strain->p[i]*k;
    }
    for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
      ksi[i][k] = strain->p[i]*(k-strain->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
 /* Computing the energy of the strain field (signal theory definition) */
  mstrain = 0.;
  *error = 0.;

#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3)                             \
  private(i)                                    \
  shared(strain)                                \
  private(offset1, elfin, coef1)                \
  private(lmstrain)                             \
  shared(mstrain)                               \
  private(k1,k2,k3)                             \
  shared(ksi)                                   \
  private(mksi)                                 \
  private(tmp)                                  \
  private(lerror)                               \
  shared(error)                                 \
  private(j)
  {
    lmstrain=0.;
/* modified for harmonic implementation 2018/07/09  J. Boisse */
#pragma omp for
    for(i=0; i< strain->n[0] * strain->n[1] * strain->n[2] ; i++) {
      i3 = i / ( strain->n[0] * strain->n[1] );
      i2 = ( i - i3 * strain->n[0] * strain->n[1] ) /  (strain->n[0]) ;
      i1 =  i - i3 * strain->n[0] * strain->n[1] - i2 * (strain->n[0]);

      offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
      elfin = strain->pxl.ft2 + offset1;
      coef1 = (*elfin)[0]*conj((*elfin)[0]) + (*elfin)[1]*conj((*elfin)[1]) + (*elfin)[2]*conj((*elfin)[2])
        +2*((*elfin)[3]*conj((*elfin)[3]) + (*elfin)[4]*conj((*elfin)[4]) + (*elfin)[5]*conj((*elfin)[5]));
	//      /* if i1 != 0 it has to be counted twice because of negative ksi1 frequencies */
	//      lmstrain += coef1 * (1.+(i1!=0)) ;
        /* modified for harmonic implementation 2018/07/09  J. Boisse */
      lmstrain += coef1 ;
    }
    
#pragma omp critical
    {
      mstrain += lmstrain;
    }
#pragma omp barrier
    //  mstrain = sqrt(mstrain / (strain->n[0]*strain->n[1]*strain->n[2]) );
#pragma omp single
    {
      mstrain = sqrt(mstrain);   /* one MUST NOT divide by n1*n2*n3 because we are in Fourier space */
    }

    /* Checking compatibility equations (in the frequency domain) */
    lerror=0.;
#pragma omp for
    for(i=1; i< strain->n[0] * strain->n[1] * strain->n[2] ; i++) {
      i3 = i / ( strain->n[0] * strain->n[1] );
      i2 = ( i - i3 * strain->n[0] * strain->n[1] ) /  strain->n[0] ;
      i1 =  i - i3 * strain->n[0] * strain->n[1] - i2 * strain->n[0] ;

      /* Strain is known to be incompatible at the Nyquist frequency
         for each axes which has an even number of pixels
         (TODO : special handle of Nyquist frequency in lspp may not be the best) */
      if (
          ( (strain->n[0]%2 == 0) && (i1==strain->n[0]/2) ) ||
          ( (strain->n[1]%2 == 0) && (i2==strain->n[1]/2) ) ||
          ( (strain->n[2]%2 == 0) && (i3==strain->n[2]/2) ) 
          ) {
        continue;
      }
      
      
      offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
      elfin = (strain->pxl.ft2 + offset1);
      k1 = ksi[0][i1];
      k2 = ksi[1][i2];
      k3 = ksi[2][i3];
      
      /* tolerance eps * |ksi|^2 * |strain(ksi=0)| */
      mksi = k1 * k1 + k2 * k2 + k3 * k3;
      //        relerr = ((eps * mstrain) * mksi);
      
      /* Strain is known to be incompatible at the Nyquist frequency
         for each axes which has an even number of pixels
         (TODO : special handle of Nyquist frequency in lspp may not be the best) */
      /*
        if ((i1==strain->nd[0]-1) && (strain->n[0]>1) && (strain->n[0]%2 == 0))
        continue;
        if ((i2==strain->nd[1]/2) && (strain->n[1]>1) && (strain->n[1]%2 == 0))
        continue;
        if ((i3==strain->nd[2]/2) && (strain->n[2]>1) && (strain->n[2]%2 == 0))
        continue;
      */
      
      /* These 6 expressions should be "equal" to zero to ensure compatibility.
         They are redundant, but no subset is sufficient for all cases. */
      tmp[0] = k2*k2*(*elfin)[0] + k1*k1*(*elfin)[1] - 2*k1*k2*(*elfin)[5]; /* 12 */
      tmp[1] = k3*k3*(*elfin)[1] + k2*k2*(*elfin)[2] - 2*k2*k3*(*elfin)[3]; /* 23 */
      tmp[2] = k1*k1*(*elfin)[2] + k3*k3*(*elfin)[0] - 2*k3*k1*(*elfin)[4]; /* 13 */
      
      tmp[3] = k2*k3*(*elfin)[0] - k1*k2*(*elfin)[4] - k1*k3*(*elfin)[5] + k1*k1*(*elfin)[3]; /* 11 */
      tmp[4] = k3*k1*(*elfin)[1] - k2*k3*(*elfin)[5] - k2*k1*(*elfin)[3] + k2*k2*(*elfin)[4]; /* 22 */
      tmp[5] = k1*k2*(*elfin)[2] - k3*k1*(*elfin)[3] - k3*k2*(*elfin)[4] + k3*k3*(*elfin)[5]; /* 33 */
      
      /* normalization of the error */
      for (j = 0; j < 6; j++) {
        tmp[j] = tmp[j] / (mstrain);
      }
      
      for (j = 0; j < 6; j++) {
        lerror = (cabs(tmp[j])>lerror?  cabs(tmp[j]) : lerror);
      }
      
      
    }
#pragma omp critical
    {
      *error =(lerror>*error? lerror : *error);
    }


  }
  /*-----------------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    free(ksi[i]);
  }
  /*-----------------------------------------------------------------------------*/
  return status;

}


/*********************************************************************************/
/* error on compatibility conditions determnied by measuring 
   || epsilon - Gamma*C0*epsilon ||
 */
int compatibility2( CraftImage *strain, LE_param *C0, double *error){
  int status;
  double complex E[6];
  double Een;
  int k;
  double energy;

  for(k=0;k<6;k++) {
    E[k]=strain->pxl.ft2[0][k];
  }
  Een = (double)( E[0]*conj(E[0]) + E[1]*conj(E[1]) + E[2]*conj(E[2]) + 
                  2.*(E[3]*conj(E[3]) + E[4]*conj(E[4]) + E[5]*conj(E[5])) );

  //  printf("ici\n");
  status = IminusGamma0xC0( strain, strain, (double *)0, (double *)0, (double *)0, (void *)C0);
  if(status!=0){
    fprintf(stderr,"compatibility2: error in IminusGamma0xC0\n");
    exit(1);
  }

  for(k=0;k<6;k++) {
    strain->pxl.ft2[0][k] = 0.+I*0.;
  }

  //  printf("l\E0\n");
  status=image_energy( strain, &energy);
  if(status!=0){
    fprintf(stderr,"compatibility2: error in image_energy\n");
    exit(1);
  }
  /* normalization of error */
  *error = sqrt(energy / Een);
  //  printf("l\E0 bas\n");

  return 0;
}
  

/*********************************************************************************/
/* applies I-Gamma0:C0  on a tensor.

   "In place" computation is available (image and imageout can be the same pointer)

*/
/*********************************************************************************/
int IminusGamma0xC0( CraftImage *imain, CraftImage *imaout, 
                     double *ksi1, double *ksi2, double *ksi3,
                     void *pC0){

  double k1,k2,k3,kk;
  
  int status=0;
  int i1,i2,i3;
  int offset1;
  double complex (*elfin)[6];
  double mksi,mstrain;
  int i,j,k;

  LE_param *C0;
  
  int ii;
  double complex e11,e22,e33,e12,e13,e23;
  double complex tre;
  double complex v1,v2,v3;
  double complex dv;
  int flag1,flag2,flag3;
  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if( imain == (CraftImage *)0) return -1;
  if( imaout == (CraftImage *)0) imaout = imain;
  if (imain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"error in function Gamma0xC0: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  C0 = (LE_param *)pC0;
  if (C0->isotropy != ISOTROPIC ) {
    fprintf(stderr,"error in function Gamma0xC0: applying an anisotropic C0 is not yet implemented.\n");
    fprintf(stderr,"           Sorry!\n");
    return -1;
  }
  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies if necessary */
  flag1=0;
  flag2=0;
  flag3=0;
  if(ksi1 == (double *)0){
    flag1=1;
    i=0;

    ksi1 = (double *)malloc(sizeof(*ksi1)*imain->n[i]);

    for(k=0;k<= imain->n[i]/2; k++) {
      ksi1[k] = imain->p[i]*k;
    }
    for(k=imain->n[i]/2+1; k<imain->n[i]; k++) {
      ksi1[k] = imain->p[i]*(k-imain->n[i]);
    }
  }

  if(ksi2 == (double *)0){
    flag2=1;
    i=1;

    ksi2 = (double *)malloc(sizeof(*ksi2)*imain->n[i]);

    for(k=0;k<= imain->n[i]/2; k++) {
      ksi2[k] = imain->p[i]*k;
    }
    for(k=imain->n[i]/2+1; k<imain->n[i]; k++) {
      ksi2[k] = imain->p[i]*(k-imain->n[i]);
    }
  }

  if(ksi3 == (double *)0){
    flag3=1;
    i=2;

    ksi3 = (double *)malloc(sizeof(*ksi3)*imain->n[i]);

    for(k=0;k<= imain->n[i]/2; k++) {
      ksi3[k] = imain->p[i]*k;
    }
    for(k=imain->n[i]/2+1; k<imain->n[i]; k++) {
      ksi3[k] = imain->p[i]*(k-imain->n[i]);
    }
  }
  /*-----------------------------------------------------------------------------*/
  double lb, mu;
  double coef1, coef2;
  
  lb = C0->sub.ile->lb;
  mu = C0->sub.ile->mu;
  coef1 = -lb/(lb+2.*mu);
  coef2 = 2.*(lb+mu)/(lb+2.*mu);
  
  double complex xx;
  double complex E[6];
  for(k=0;k<6;k++) E[k] = imain->pxl.ft2[0][k];

  /* the case i1=i2=i3=0 : */
  for(k=0;k<6;k++) imaout->pxl.ft2[0][k]=E[k];


#pragma omp parallel for                        \
  default(none)                                 \
  private(i1,i2,i3,i,ii,k)                      \
  private(k1,k2,k3,kk)                          \
  private(e11,e22,e33,e23,e13,e12)              \
  private(tre,v1,v2,v3,dv,xx)                   \
  shared(imain, imaout, ksi1, ksi2, ksi3)       \
  shared(E, coef1, coef2)
  for(ii=1; ii< (imain->n[0]/2+1) * imain->n[1] * imain->n[2] ; ii++) {
    i3 = ii / ( (imain->n[0]/2+1) * imain->n[1] );
    i2 = ( ii - i3 * (imain->n[0]/2+1) * imain->n[1] ) /  (imain->n[0]/2+1) ;
    i1 =  ii - i3 * (imain->n[0]/2+1) * imain->n[1] - i2 * (imain->n[0]/2+1);
    
    k3 = ksi3[i3];
    k2 = ksi2[i2];
    k1 = ksi1[i1];
    
    i = i1 + imain->nd[0] * (i2 + imain->nd[1] * i3);
    
    if (
             ( (imain->n[0]%2==0) && (i1==imain->n[0]/2) ) ||
             ( (imain->n[1]%2==0) && (i2==imain->n[1]/2) ) ||
             ( (imain->n[2]%2==0) && (i3==imain->n[2]/2) ) 
             ){
      
      imaout->pxl.ft2[i][0] = 0.+I*0.;
      imaout->pxl.ft2[i][1] = 0.+I*0.;
      imaout->pxl.ft2[i][2] = 0.+I*0.;
      imaout->pxl.ft2[i][3] = 0.+I*0.;
      imaout->pxl.ft2[i][4] = 0.+I*0.;
      imaout->pxl.ft2[i][5] = 0.+I*0.;
      
      /*
        imaout->pxl.ft2[i][0] = imain->pxl.ft2[i][0];
        imaout->pxl.ft2[i][1] = imain->pxl.ft2[i][1];
        imaout->pxl.ft2[i][2] = imain->pxl.ft2[i][2];
        imaout->pxl.ft2[i][3] = imain->pxl.ft2[i][3];
        imaout->pxl.ft2[i][4] = imain->pxl.ft2[i][4];
        imaout->pxl.ft2[i][5] = imain->pxl.ft2[i][5];
      */
    }
    /* the other cases: */
    
    else{
      
      kk = 1./(k1*k1+k2*k2+k3*k3);
      
      
      e11=imain->pxl.ft2[i][0];
      e22=imain->pxl.ft2[i][1];
      e33=imain->pxl.ft2[i][2];
      e23=imain->pxl.ft2[i][3];
      e13=imain->pxl.ft2[i][4];
      e12=imain->pxl.ft2[i][5];
      
      
      
      tre = e11+e22+e33;
      v1 = k1*e11 + k2*e12 + k3*e13;
      v2 = k1*e12 + k2*e22 + k3*e23;
      v3 = k1*e13 + k2*e23 + k3*e33;
      
      dv = k1*v1+k2*v2+k3*v3;
      
      xx = ( coef1*tre + coef2*dv*kk);
      
      e11 = kk * ( k1*k1*xx - 2.*k1*v1);
      e22 = kk * ( k2*k2*xx - 2.*k2*v2);
      e33 = kk * ( k3*k3*xx - 2.*k3*v3);
      e23 = kk * ( k2*k3*xx - k2*v3 - k3*v2);
      e13 = kk * ( k1*k3*xx - k1*v3 - k3*v1);
      e12 = kk * ( k1*k2*xx - k1*v2 - k2*v1);
      
      imaout->pxl.ft2[i][0] = imain->pxl.ft2[i][0] + e11;
      imaout->pxl.ft2[i][1] = imain->pxl.ft2[i][1] + e22;
      imaout->pxl.ft2[i][2] = imain->pxl.ft2[i][2] + e33;
      imaout->pxl.ft2[i][3] = imain->pxl.ft2[i][3] + e23;
      imaout->pxl.ft2[i][4] = imain->pxl.ft2[i][4] + e13;
      imaout->pxl.ft2[i][5] = imain->pxl.ft2[i][5] + e12;
      
    }
    
    //      }
    //    }
  }
  
  /*-----------------------------------------------------------------------------*/
  if(flag1) free(ksi1);
  if(flag2) free(ksi2);
  if(flag3) free(ksi3);
  return 0;
}
/*********************************************************************************/
/* 
   "Energy" in the signal processing sense of an image

*/
/*********************************************************************************/
int image_energy( CraftImage *image, double *energy){

  int i1, i2, i3, i, ii;
  int k;

  double *t;
  double complex *ft;
  double en, xen;
  /*-----------------------------------------------------------------------------*/
  if ( image == (CraftImage *)0) return -1;

  /*-----------------------------------------------------------------------------*/
  *energy=0.;

  switch (image->type[0]) {
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UNKNOWN:
    return -1;
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_INT :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en)                       \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){

        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];

        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);

        en += (double)image->pxl.i[i]*(double)image->pxl.i[i];
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FLOAT :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en)                     \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){

        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];

        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);

        en += (double)image->pxl.f[i]*(double)image->pxl.f[i];
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_DOUBLE :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en)                       \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){

        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];

        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);

        en += (double)image->pxl.d[i]*(double)image->pxl.d[i];
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_TENSOR2 :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en,t)                   \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){

        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];

        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);

        t=image->pxl.t2[i];
        en += 
          t[0]*t[0] + t[1]*t[1] + t[2]*t[2] +
          2.*( t[3]*t[3] + t[4]*t[4] + t[5]*t[5] );

      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FOURIER_TENSOR2 :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en,xen,ft)                \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){
        
        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];
        
        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);
        
        ft=image->pxl.ft2[i];
        xen = (double)(
                       ft[0]*conj(ft[0]) + ft[1]*conj(ft[1]) + ft[2]*conj(ft[2]) +
                       2.*( ft[3]*conj(ft[3]) + ft[4]*conj(ft[4]) + ft[5]*conj(ft[5]) )
                       );
        if( (i1!=0) && ( (image->n[0]%2!=0)||(i1!=image->n[0]/2) ) ){
          xen *= 2.;
        }
        /*
          if ((i1==image->nd[0]-1) && (image->n[0]>1) && (image->n[0]%2 == 0))
          xen=0.;
          if ((i2==image->nd[1]/2) && (image->n[1]>1) && (image->n[1]%2 == 0))
          xen=0.;
          if ((i3==image->nd[2]/2) && (image->n[2]>1) && (image->n[2]%2 == 0))
          xen=0.;
        */
        
        en += xen;
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    /* remark: energy must NOT be divided by the number of pixels as in real space */
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR3D :
#pragma omp parallel                               \
  default(none)                                    \
  private(i1,i2,i3,i,ii,en,xen,t)                  \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){
        
        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];
        
        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);
        
        t=image->pxl.v3d[i];
        en += t[0]*t[0]+t[1]*t[1]+t[2]*t[2];
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR :
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,en,xen,t,k)             \
  shared(image,energy)
    {
      en=0.;
#pragma omp for
      for(ii=0; ii<image->n[2]*image->n[1]*image->n[0]; ii++){
        
        i3 = ii / (image->n[0]*image->n[1]);
        i2 = (ii - i3*(image->n[0]*image->n[1]))/image->n[0];
        i1 = ii - i3*(image->n[0]*image->n[1]) - i2*image->n[0];
        
        i = i1 + image->nd[0] * (i2 + image->nd[1] * i3);
        
        t=image->pxl.a+i;
        for(k=0;k<image->type[1];k++){
          en += t[k]*t[k];
        }
      }
#pragma omp critical
      {
        *energy += en;
      }
    }
    *energy /= (double)image->n[0]*(double)image->n[1]*(double)image->n[2];
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  default:
    return -2;
  }

  return 0;
}





/*********************************************************************************/
/* error on compatibility conditions following Monchiet & Bonnet:

 
 */
int compatibility_MB( CraftImage *strain, double *error){

  double *ksi1, *ksi2, *ksi3;
  double k1,k2,k3,kk;
  
  int status=0;
  int i1,i2,i3;
  int offset1;
  double complex (*elfin)[6];
  double mksi,mstrain;
  int i,j,k;

  int ii;
  double complex e11,e22,e33,e12,e13,e23;
  double complex tre;
  double complex v1,v2,v3;
  double complex dv;

  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if( strain == (CraftImage *)0) return -1;
  if (strain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"error in function compatibility_MB: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  i=0;
  ksi1 = (double *)malloc(sizeof(*ksi1)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi1[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi1[k] = strain->p[i]*(k-strain->n[i]);
  }

  ksi2 = (double *)malloc(sizeof(*ksi2)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi2[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi2[k] = strain->p[i]*(k-strain->n[i]);
  }
  
  ksi3 = (double *)malloc(sizeof(*ksi3)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi3[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi3[k] = strain->p[i]*(k-strain->n[i]);
  }

  /*-----------------------------------------------------------------------------*/
  double lb, mu;
  double coef1, coef2;
  
  lb = 0.;
  mu = 0.5;
  coef1 = -lb/(lb+2.*mu);
  coef2 = 2.*(lb+mu)/(lb+2.*mu);
  
  double complex xx;
  double complex E[6];
  for(k=0;k<6;k++) E[k] = strain->pxl.ft2[0][k];

  double complex t[6];
  double complex tt[6];

  double lerr;
  double lerrsum;

  *error=0.;
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,k)                      \
  private(k1,k2,k3,kk)                          \
  private(e11,e22,e33,e23,e13,e12)              \
  private(tre,v1,v2,v3,dv,xx)                   \
  shared(strain, ksi1, ksi2, ksi3)       \
  shared(E, coef1, coef2)                        \
  private(lerr,lerrsum)                          \
  shared(error)                                  \
  private(tt,t)
  {
    lerrsum = 0.;
#pragma omp for
    for(ii=1; ii< (strain->n[0]/2+1) * strain->n[1] * strain->n[2] ; ii++) {
      i3 = ii / ( (strain->n[0]/2+1) * strain->n[1] );
      i2 = ( ii - i3 * (strain->n[0]/2+1) * strain->n[1] ) /  (strain->n[0]/2+1) ;
      i1 =  ii - i3 * (strain->n[0]/2+1) * strain->n[1] - i2 * (strain->n[0]/2+1);

      k3 = ksi3[i3];
      k2 = ksi2[i2];
      k1 = ksi1[i1];
      i = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);

          
      t[0] = strain->pxl.ft2[i][0];
      t[1] = strain->pxl.ft2[i][1];
      t[2] = strain->pxl.ft2[i][2];
      t[3] = strain->pxl.ft2[i][3];
      t[4] = strain->pxl.ft2[i][4];
      t[5] = strain->pxl.ft2[i][5];
      

      if (
          ( (strain->n[0]%2==0) && (i1==strain->n[0]/2) ) ||
          ( (strain->n[1]%2==0) && (i2==strain->n[1]/2) ) ||
          ( (strain->n[2]%2==0) && (i3==strain->n[2]/2) ) 
          ){
        
        for(k=0;k<6;k++) tt[k]=0.+I*0.;
        
      }
      /* the other cases: */
      
      else{
        
        kk = 1./(k1*k1+k2*k2+k3*k3);
        
        
        e11=t[0];
        e22=t[1];
        e33=t[2];
        e23=t[3];
        e13=t[4];
        e12=t[5];
        
        
        
        tre = e11+e22+e33;
        v1 = k1*e11 + k2*e12 + k3*e13;
        v2 = k1*e12 + k2*e22 + k3*e23;
        v3 = k1*e13 + k2*e23 + k3*e33;
        
        dv = k1*v1+k2*v2+k3*v3;
        
        xx = ( coef1*tre + coef2*dv*kk);
        
        e11 = kk * ( k1*k1*xx - 2.*k1*v1);
        e22 = kk * ( k2*k2*xx - 2.*k2*v2);
        e33 = kk * ( k3*k3*xx - 2.*k3*v3);
        e23 = kk * ( k2*k3*xx - k2*v3 - k3*v2);
        e13 = kk * ( k1*k3*xx - k1*v3 - k3*v1);
        e12 = kk * ( k1*k2*xx - k1*v2 - k2*v1);
        
        tt[0] = t[0] + e11;
        tt[1] = t[1] + e22;
        tt[2] = t[2] + e33;
        tt[3] = t[3] + e23;
        tt[4] = t[4] + e13;
        tt[5] = t[5] + e12;
        
        lerr = 
          (
           tt[0]*conj(tt[0]) + tt[1]*conj(tt[1]) + tt[2]*conj(tt[2]) + 2.* ( tt[3]*conj(tt[3]) + tt[4]*conj(tt[4]) + tt[5]*conj(tt[5]) )
           )
          /
          (
           t[0]*conj(t[0]) + t[1]*conj(t[1]) + t[2]*conj(t[2]) + 2.* ( t[3]*conj(t[3]) + t[4]*conj(t[4]) + t[5]*conj(t[5]) )
           );
        /*
          lerr = 
          (
          tt[0]*conj(tt[0]) + tt[1]*conj(tt[1]) + tt[2]*conj(tt[2]) + 2.* ( tt[3]*conj(tt[3]) + tt[4]*conj(tt[4]) + tt[5]*conj(tt[5]) )
          );
        */
        lerrsum = (lerr > lerrsum ? lerr : lerrsum);
        
      }
      
    }
#pragma omp critical
    {
      *error = (*error > lerrsum ? *error : lerrsum );
    }
  }
  /*-----------------------------------------------------------------------------*/
  *error = sqrt(*error);
  /*-----------------------------------------------------------------------------*/
  free(ksi1);
  free(ksi2);
  free(ksi3);
  /*-----------------------------------------------------------------------------*/
  return 0;
}
/*********************************************************************************/
/* error on compatibility conditions following Monchiet & Bonnet:
   (version corrigee)
 
 */
int compatibility_MB2( CraftImage *strain, double *error){

  double *ksi1, *ksi2, *ksi3;
  double k1,k2,k3,kk;
  
  int status=0;
  int i1,i2,i3;
  int offset1;
  double complex (*elfin)[6];
  double mksi,mstrain;
  int i,j,k;

  int ii;
  double complex e11,e22,e33,e12,e13,e23;
  double complex tre;
  double complex v1,v2,v3;
  double complex dv;

  /*-----------------------------------------------------------------------------*/
  /* some verifications */
  if( strain == (CraftImage *)0) return -1;
  if (strain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"error in function compatibility_MB: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  /*-----------------------------------------------------------------------------*/
  /* computation of frequencies */
  i=0;
  ksi1 = (double *)malloc(sizeof(*ksi1)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi1[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi1[k] = strain->p[i]*(k-strain->n[i]);
  }

  ksi2 = (double *)malloc(sizeof(*ksi2)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi2[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi2[k] = strain->p[i]*(k-strain->n[i]);
  }
  
  ksi3 = (double *)malloc(sizeof(*ksi3)*strain->n[i]);

  for(k=0;k<= strain->n[i]/2; k++) {
    ksi3[k] = strain->p[i]*k;
  }
  for(k=strain->n[i]/2+1; k<strain->n[i]; k++) {
    ksi3[k] = strain->p[i]*(k-strain->n[i]);
  }

  /*-----------------------------------------------------------------------------*/
  double lb, mu;
  double coef1, coef2;
  
  lb = 0.;
  mu = 0.5;
  coef1 = -lb/(lb+2.*mu);
  coef2 = 2.*(lb+mu)/(lb+2.*mu);
  
  double complex xx;
  double complex E[6];
  for(k=0;k<6;k++) E[k] = strain->pxl.ft2[0][k];

  double complex t[6];
  double complex tt[6];
  double ls2, ls2sum, s2sum;

  double lerr;
  double lerrsum;

  *error=0.;
  s2sum=0.;
#pragma omp parallel                            \
  default(none)                                 \
  private(i1,i2,i3,i,ii,k)                      \
  private(k1,k2,k3,kk)                          \
  private(e11,e22,e33,e23,e13,e12)              \
  private(tre,v1,v2,v3,dv,xx)                   \
  shared(strain, ksi1, ksi2, ksi3)               \
  shared(E, coef1, coef2)                        \
  private(lerr,lerrsum, ls2, ls2sum)               \
  shared(error,s2sum)                              \
  private(tt,t)
  {
    lerrsum = 0.;
    ls2sum=0.;
#pragma omp for
    for(ii=1; ii< (strain->n[0]/2+1) * strain->n[1] * strain->n[2] ; ii++) {
      i3 = ii / ( (strain->n[0]/2+1) * strain->n[1] );
      i2 = ( ii - i3 * (strain->n[0]/2+1) * strain->n[1] ) /  (strain->n[0]/2+1) ;
      i1 =  ii - i3 * (strain->n[0]/2+1) * strain->n[1] - i2 * (strain->n[0]/2+1);

      k3 = ksi3[i3];
      k2 = ksi2[i2];
      k1 = ksi1[i1];
      i = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);

      t[0] = strain->pxl.ft2[i][0];
      t[1] = strain->pxl.ft2[i][1];
      t[2] = strain->pxl.ft2[i][2];
      t[3] = strain->pxl.ft2[i][3];
      t[4] = strain->pxl.ft2[i][4];
      t[5] = strain->pxl.ft2[i][5];
      
      if (
          ( (strain->n[0]%2==0) && (i1==strain->n[0]/2) ) ||
          ( (strain->n[1]%2==0) && (i2==strain->n[1]/2) ) ||
          ( (strain->n[2]%2==0) && (i3==strain->n[2]/2) ) 
          ){
        
        for(k=0;k<6;k++) tt[k]=0.+I*0.;
        
      }
      /* the other cases: */
      
      else{
        
        kk = 1./(k1*k1+k2*k2+k3*k3);
        
        
        e11=t[0];
        e22=t[1];
        e33=t[2];
        e23=t[3];
        e13=t[4];
        e12=t[5];
        
        
        
        tre = e11+e22+e33;
        v1 = k1*e11 + k2*e12 + k3*e13;
        v2 = k1*e12 + k2*e22 + k3*e23;
        v3 = k1*e13 + k2*e23 + k3*e33;
        
        dv = k1*v1+k2*v2+k3*v3;
        
        xx = ( coef1*tre + coef2*dv*kk);
        
        e11 = kk * ( k1*k1*xx - 2.*k1*v1);
        e22 = kk * ( k2*k2*xx - 2.*k2*v2);
        e33 = kk * ( k3*k3*xx - 2.*k3*v3);
        e23 = kk * ( k2*k3*xx - k2*v3 - k3*v2);
        e13 = kk * ( k1*k3*xx - k1*v3 - k3*v1);
        e12 = kk * ( k1*k2*xx - k1*v2 - k2*v1);
        
        tt[0] = t[0] + e11;
        tt[1] = t[1] + e22;
        tt[2] = t[2] + e33;
        tt[3] = t[3] + e23;
        tt[4] = t[4] + e13;
        tt[5] = t[5] + e12;
      }
      
      ls2 = 
        (
         t[0]*conj(t[0]) + t[1]*conj(t[1]) + t[2]*conj(t[2]) + 2.* ( t[3]*conj(t[3]) + t[4]*conj(t[4]) + t[5]*conj(t[5]) )
         );
      
      lerr = 
        (
         tt[0]*conj(tt[0]) + tt[1]*conj(tt[1]) + tt[2]*conj(tt[2]) + 2.* ( tt[3]*conj(tt[3]) + tt[4]*conj(tt[4]) + tt[5]*conj(tt[5]) )
         );
      
      if (
          (i1!=0) &&
          !( (strain->n[0]%2==0) && (i1==strain->n[0]/2) ) 
          ) {
        lerr *= 2.;
        ls2 *= 2.;
      }            
      
      lerrsum += lerr;
      ls2sum += ls2;
      
    }
#pragma omp critical
    {
      *error += lerrsum;
      s2sum += ls2sum;
    }
  }
  /*-----------------------------------------------------------------------------*/
  *error = sqrt(*error/s2sum);
  /*-----------------------------------------------------------------------------*/
  free(ksi1);
  free(ksi2);
  free(ksi3);
  /*-----------------------------------------------------------------------------*/
  return 0;
}
  

