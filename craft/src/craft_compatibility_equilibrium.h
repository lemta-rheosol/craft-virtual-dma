#ifndef __COMPATIBILITY_EQUILIBRIUM__
#define __COMPATIBILITY_EQUILIBRIUM__

#include <craftimage.h>
#include <LE_materials.h>

/* the different methods of convergence test */
#define CRAFT_TEST_NOT_SET -1
#define CRAFT_TEST_DIVS 0
#define CRAFT_TEST_DIVS_SUM 0
#define CRAFT_TEST_DIVS_MAX 1
#define CRAFT_TEST_MONCHIET_BONNET 2

int (*equilibrium)( CraftImage *image, double *error);
int equilibrium_divs_sum( CraftImage *image, double *error);
int equilibrium_divs_max( CraftImage *image, double *error);
int equilibrium_MB2( CraftImage *image, double *error);

/* added for harmonic implementation 2017/07/05 J. Boisse */
int equilibrium_divs_sum_harmonic( CraftImage *image, double *error);

int compatibility( CraftImage *image, double *error);

/* added for harmonic implementation 2017/07/05 J. Boisse */
int compatibility_harmonic( CraftImage *image, double *error); 

int compatibility2( CraftImage *strain, LE_param *C0, double *error);
int IminusGamma0xC0( CraftImage *imain, CraftImage *imaout, 
                     double *ksi1, double *ksi2, double *ksi3,
                     void *pC0);

int image_energy( CraftImage *imain, double *energy);
#endif
