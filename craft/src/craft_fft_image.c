#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <craftimage.h>

/* filtered frequencies */
/* added for harmonic implementation 2017/10/13 J. Boisse */
#include <math.h>

/*************************************************************************************/
/* HM
   November 15th 2011

   craft_fft_image applies FFT on a CraftImage, using FFTW3 library.

   It applies direct or inverse FFT depending on the flag argument:
   * flag = CRAFT_FFT_DIRECT  -> direct Fourier transform applied
   * flag = CRAFT_FFT_INVERSE -> inverse Fourier transform applied
   * flag = CRAFT_FFT_INIT_*  -> initialization of fft algorithm

   It applies the FFT "in place"; i.e. the same CraftImage entered
   as argument is used as input ans as output.


   - Direct FFT (flag=CRAFT_FFT_DIRECT):
   The type of the CraftImage in input must be one of
   CRAFT_IMAGE_DOUBLE :  image of double values in real space
   CRAFT_IMAGE_TENSOR2 : image of 2d order symmetric tensor in real space, the data
                         for each pixel consists in the 6 components of the tensor
                         stored as "double" values.
   CRAFT_IMAGE_VECTOR3D : image of 3D vector in real space, the data for each pixel
                         consists in the 3 components of the vector stored as "double" values.
   CRAFT_IMAGE_VECTOR : image of vector in real space, the data for each pixel
                         consists in N components of the vector stored as "double" values,
                         N being specified in the type field.
   it is changed into:
   CRAFT_IMAGE_FOURIER_DOUBLE : image of "double complex" values in Fourier space
   CRAFT_IMAGE_FOURIER_TENSOR2 :  image of 2d order symmetric tensor in Fourier space,
                         the data for each pixel consists in the 6 components of the
                         tensor stored as "double complex" values.
   CRAFT_IMAGE_FOURIER_VECTOR3D : image of 3D vector in Fourier space, the data for
                         each pixel consists in the 3 components of the tensor stored
                         as "double complex" values.
   CRAFT_IMAGE_FOURIER_VECTOR : image of vector in Fourier space, the data for each
                         pixel consists in N components of the tensor stored
                         as "double complex" values.


   - Inverse FFT (flag=CRAFT_FFT_INVERSE):
   The types of the CraftImage as input and as output are reversed.

   - Initialization of FFT environment (flag=CRAFT_FFT_INIT_*)
     The type of CraftImage as input must be one of CRAFT_IMAGE_DOUBLE, CRAFT_IMAGE_TENSOR2,
     CRAFT_IMAGE_VECTOR3D, CRAFT_IMAGE_VECTOR. It is used do describe the
     geometry of the problems to be computed further ( number of pixels in each dimension,
     step sizes, ...). Depending on the algorithm used, the input CraftImage can be changed
     by craft_fft_image call.
     CRAFT_FFT_INIT_ESTIMATE : the FFTW_ESTIMATE mode is used for the plan creation
     CRAFT_FFT_INIT_MEASURE  : the FFTW_MEASURE  mode is used for the plan creation
     CRAFT_FFT_INIT_PATIENT  : the FFTW_PATIENT  mode is used for the plan creation

     Please note that CRAFT_FFT_INIT_MEASURE and CRAFT_FFT_INIT_PATIENT will
     overwrite the data in the CraftImage!

*/
/*************************************************************************************/
int craft_fft_image( CraftImage *image, int flag ){
  int i,ncomp,mode;
  /*=================================================================================*/
  /* there are 3 plans for direct Fourier transforms, one for each dimension,
     and 3 plans for inverse transforms */
  static fftw_plan pland[3];
  static fftw_plan plani[3];
  /* rimap resumes the geometry of the FFT problem of the previous call, in real space */
  static CraftImage rimap = NullCraftImage;
  CraftImage rima, fima;

  /*=================================================================================*/
  /* verification of inputs                                                          */
  /*---------------------------------------------------------------------------------*/
#if defined FFTW2 || defined FFTW3
#else
  fprintf(stderr,"craft_fft_image: impossible case. Contact CraFT developpers.\n");
  return -1;
#endif

  switch( flag ){
  case CRAFT_FFT_INIT_ESTIMATE:
  case CRAFT_FFT_INIT_MEASURE:
  case CRAFT_FFT_INIT_PATIENT:

    switch( image->type[0] ){
    case CRAFT_IMAGE_DOUBLE:
    case CRAFT_IMAGE_TENSOR2:
    case CRAFT_IMAGE_VECTOR3D:
    case CRAFT_IMAGE_VECTOR:
    if ( (image->nd[0] != 2*(image->n[0]/2+1))
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;

    case CRAFT_IMAGE_FOURIER_DOUBLE:
    case CRAFT_IMAGE_FOURIER_TENSOR2:
    case CRAFT_IMAGE_FOURIER_VECTOR3D:
    case CRAFT_IMAGE_FOURIER_VECTOR:
    if ( (image->nd[0] != (image->n[0]/2+1))
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when initializing FFT, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_DOUBLE,\n\tCRAFT_IMAGE_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_VECTOR3D,\n\tCRAFT_IMAGE_VECTOR");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_DOUBLE,\n\tCRAFT_IMAGE_FOURIER_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_VECTOR3D,\n\tCRAFT_IMAGE_FOURIER_VECTOR");
      return -1;
    }

    break;

  case CRAFT_FFT_DIRECT:

    switch( image->type[0] ){
    case CRAFT_IMAGE_DOUBLE:
    case CRAFT_IMAGE_TENSOR2:
    case CRAFT_IMAGE_VECTOR3D:
    case CRAFT_IMAGE_VECTOR:
      break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when direct FFT is applied, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_DOUBLE,\n\tCRAFT_IMAGE_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_VECTOR3D,\n\tCRAFT_IMAGE_VECTOR");
      return -1;
    }

    if ( (image->nd[0] != 2*(image->n[0]/2+1))
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }

    break;

  case CRAFT_FFT_INVERSE:

    switch( image->type[0] ){
    case CRAFT_IMAGE_FOURIER_DOUBLE:
    case CRAFT_IMAGE_FOURIER_TENSOR2:
    case CRAFT_IMAGE_FOURIER_VECTOR3D:
    case CRAFT_IMAGE_FOURIER_VECTOR:
      break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when inverse FFT is applied, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_DOUBLE,\n\tCRAFT_IMAGE_FOURIER_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_VECTOR3D,\n\tCRAFT_IMAGE_FOURIER_VECTOR");
      return -1;
    }

    if ( (image->nd[0] != (image->n[0]/2+1))
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;

  default:
    fprintf(stderr,"craft_fft_image error: flag %d is not allowed.\n", flag);
    return -1;
  }


  /*=================================================================================*/
  /* FFT initialization                                                              */
  /*---------------------------------------------------------------------------------*/
  if ( (flag == CRAFT_FFT_INIT_ESTIMATE)
    || (flag == CRAFT_FFT_INIT_MEASURE)
    || (flag == CRAFT_FFT_INIT_PATIENT) ){
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* if the required initialization is identical as the previous one -> nothing has to be done */
    if ( cmp_size_and_type_craft_images(*image , rimap)==1 ) {
      return 0;
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* craft_image has already been called, thus plans have been made.
     Therefeore, they must be destroyed before re-initialization: */
    if ( cmp_size_and_type_craft_images( rimap , (CraftImage)NullCraftImage) != 1 ) {
      for(i=0;i<3;i++) {
        fftw_destroy_plan(pland[i]);
        fftw_destroy_plan(plani[i]);
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* plans for direct transforms */

    rima = *image;
    fima = *image;

    {
      switch (image->type[0]){
      case CRAFT_IMAGE_DOUBLE:
        fima.type[0] = CRAFT_IMAGE_FOURIER_DOUBLE;
        fima.type[1] = 1;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]/2;
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_TENSOR2:
        fima.type[0] = CRAFT_IMAGE_FOURIER_TENSOR2;
        fima.type[1] = 6;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]/2;
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_VECTOR3D:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D;
        fima.type[1] = 3;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]/2;
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_VECTOR:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR;
        fima.type[1] = rima.type[1];
        fima.type[2] = 0;
        fima.nd[0] = 2*image->nd[0];
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;

      case CRAFT_IMAGE_FOURIER_DOUBLE:
        rima.type[0] = CRAFT_IMAGE_DOUBLE;
        rima.type[1] = 1;
        rima.type[2] = 0;
        rima.nd[0] = 2*image->nd[0];
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_TENSOR2:
        rima.type[0] = CRAFT_IMAGE_TENSOR2;
        rima.type[1] = 6;
        rima.type[2] = 0;
        rima.nd[0] = 2*image->nd[0];
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR3D:
        rima.type[0] = CRAFT_IMAGE_VECTOR3D;
        rima.type[1] = 3;
        rima.type[2] = 0;
        rima.nd[0] = 2*image->nd[0];
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR:
        rima.type[0] = CRAFT_IMAGE_VECTOR;
        rima.type[1] = rima.type[1];
        rima.type[2] = 0;
        rima.nd[0] = 2*image->nd[0];
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      }


    }
    ncomp = fima.type[1];

    switch(flag){
    case CRAFT_FFT_INIT_ESTIMATE:
      mode = FFTW_ESTIMATE;
      break;
    case CRAFT_FFT_INIT_MEASURE:
      mode = FFTW_MEASURE;
      break;
    case CRAFT_FFT_INIT_PATIENT:
      mode = FFTW_PATIENT;
      break;
    }

    pland[0] = fftw_plan_many_dft_r2c(
              1,                  /* rank= 1 */
              &(rima.n[0]),       /* size of dimension */
              ncomp,              /* howmany */

              rima.pxl.d,         /* input array */
              &rima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist=1 */

              mode
              );

    pland[1] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(fima.n[1]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* input array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              FFTW_FORWARD,
              mode
              );

    pland[2] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &fima.n[2] ,        /* size of dimension */
              ncomp,              /* howmany=1 */

              fima.pxl.fd,        /* input array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              FFTW_FORWARD,
              mode
              );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* plans for inverse transforms */
    plani[0] = fftw_plan_many_dft_c2r(
              1,                  /* rank= 1 */
              &(rima.n[0]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* output array */
              &fima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist=1 */

              rima.pxl.d,         /* input array */
              &rima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist */

              mode
              );

    plani[1] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(fima.n[1]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* input array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              FFTW_BACKWARD,
              mode
              );

    plani[2] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &fima.n[2] ,        /* size of dimension */
              ncomp,              /* howmany=1 */

              fima.pxl.fd,        /* input array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              FFTW_BACKWARD,
              mode
              );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    rimap = rima;
    rimap.pxl.v = (void *)0;
  }
  /*---------------------------------------------------------------------------------*/
  /* direct FFT                                                                      */
  /*---------------------------------------------------------------------------------*/
  else if( flag == CRAFT_FFT_DIRECT ) {
    int ii, i0, i1, i2, k,icomp;
    int kr, kf;

    rima = *image;
    {
      fima = rima;
      switch (rima.type[0]){
      case CRAFT_IMAGE_DOUBLE:
        fima.type[0] = CRAFT_IMAGE_FOURIER_DOUBLE;
        fima.type[1] = 1;
        break;
      case CRAFT_IMAGE_TENSOR2:
        fima.type[0] = CRAFT_IMAGE_FOURIER_TENSOR2;
        fima.type[1] = 6;
        break;
      case CRAFT_IMAGE_VECTOR3D:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D;
        fima.type[1] = 3;
        break;
      case CRAFT_IMAGE_VECTOR:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR;
        fima.type[1] = rima.type[1];
        break;
      }
      ncomp = fima.type[1];
      fima.type[2] = 0;
      fima.nd[0] = rima.nd[0]/2;
      for(i=0;i<3;i++) fima.p[i] = 1. / ( fima.p[i]*fima.n[i] );

    }

    if ( cmp_size_and_type_craft_images( rima , rimap)==0 ) {
      //      fprintf(stderr,"craft_fft_image error: direct FFT called without preliminary initialization.\n");
      //      return -1;
      craft_fft_image( image, CRAFT_FFT_INIT_MEASURE );
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii, i1, i2, kr, kf)    \
  shared(pland, rima, fima, ncomp)
    for (ii=0; ii < rima.n[2]* rima.n[1]; ii++) {

      i2 = ii/rima.n[1];
      i1 = ii%rima.n[1];

      kr = (i1+ i2*rima.nd[1]) * rima.nd[0] * ncomp;
      kf = (i1+ i2*fima.nd[1]) * fima.nd[0] * ncomp;
      fftw_execute_dft_r2c( pland[0], rima.pxl.d + kr, fima.pxl.fd + kf );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i0,i2,k)            \
  shared( pland , fima, ncomp)
    for(ii=0; ii<fima.n[2]*fima.nd[0]; ii++) {

      i2 = ii/fima.nd[0];
      i0 = ii%fima.nd[0];

      k = ( i0 + i2*fima.nd[0]*fima.nd[1] ) * ncomp;
      fftw_execute_dft( pland[1], fima.pxl.fd + k, fima.pxl.fd + k );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if ( fima.n[2] > 1 ) {    /* it's useless to apply FFT on the 3d direction if its width is 1 */
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i1,i0,k)            \
  shared( pland , fima, ncomp)
      for(ii=0; ii<fima.n[1]*fima.nd[0];ii++) {

        i1 = ii/fima.nd[0];
        i0 = ii%fima.nd[0];

        k = ( i0 + i1*fima.nd[0] ) * ncomp;
        fftw_execute_dft(pland[2], fima.pxl.fd + k, fima.pxl.fd + k );
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* normalization                                                                 */
    double coef = 1./ ( (double)fima.n[0] * (double)fima.n[1] * (double)fima.n[2] );
    switch(fima.type[0]){
    case CRAFT_IMAGE_FOURIER_DOUBLE:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++)
        (fima.pxl).fd[i] *= coef;
      break;

    case CRAFT_IMAGE_FOURIER_TENSOR2:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++){
        (fima.pxl).ft2[i][0] *= coef;
        (fima.pxl).ft2[i][1] *= coef;
        (fima.pxl).ft2[i][2] *= coef;
        (fima.pxl).ft2[i][3] *= coef;
        (fima.pxl).ft2[i][4] *= coef;
        (fima.pxl).ft2[i][5] *= coef;
      }
      break;

    case CRAFT_IMAGE_FOURIER_VECTOR3D:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++){
        (fima.pxl).fv3d[i][0] *= coef;
        (fima.pxl).fv3d[i][1] *= coef;
        (fima.pxl).fv3d[i][2] *= coef;
      }
      break;

    case CRAFT_IMAGE_FOURIER_VECTOR:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++) {
        for(icomp=0; icomp<fima.type[1]; icomp++) {
          (fima.pxl).a[i*fima.type[1] + icomp] *= coef;
        }
      }
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    *image=fima;
    rimap = rima;
    rimap.pxl.v = (void *)0;
  }
  /*---------------------------------------------------------------------------------*/
  /* inverse FFT                                                                     */
  /*---------------------------------------------------------------------------------*/
  else if( flag == CRAFT_FFT_INVERSE ) {
    int ii, i0, i1, i2, k;
    int kr, kf;

    fima = *image;
    {
      rima = fima;
      switch (fima.type[0]){
      case CRAFT_IMAGE_FOURIER_DOUBLE:
        rima.type[0] = CRAFT_IMAGE_DOUBLE;
        rima.type[1] = 1;
        break;
      case CRAFT_IMAGE_FOURIER_TENSOR2:
        rima.type[0] = CRAFT_IMAGE_TENSOR2;
        rima.type[1] = 6;
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR3D:
        rima.type[0] = CRAFT_IMAGE_VECTOR3D;
        rima.type[1] = 3;
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR:
        rima.type[0] = CRAFT_IMAGE_VECTOR;
        rima.type[1] = fima.type[1];
        break;
      }
      ncomp = rima.type[1];
      rima.type[2] = 0;
      rima.nd[0] = fima.nd[0]*2;
      for(i=0;i<3;i++) rima.p[i] = 1. / ( rima.p[i]*rima.n[i] );

    }

    if ( cmp_size_and_type_craft_images( rima , rimap)==0 ) {
      //      fprintf(stderr,"craft_fft_image error: direct FFT called without preliminary initialization.\n");
      //      return -1;
      craft_fft_image( image, CRAFT_FFT_INIT_MEASURE );
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if ( fima.n[2] > 1 ) {    /* it's useless to apply FFT on the 3d direction if its width is 1 */
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i1,i0,k)            \
  shared( plani , fima, ncomp)
      for(ii=0; ii<fima.n[1]*fima.nd[0];ii++) {

        i1 = ii/fima.nd[0];
        i0 = ii%fima.nd[0];

        k = ( i0 + i1*fima.nd[0] ) * ncomp;
        fftw_execute_dft( plani[2], fima.pxl.fd + k, fima.pxl.fd + k );
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i0,i2,k)            \
  shared( plani , fima, ncomp)
    for(ii=0; ii<fima.n[2]*fima.nd[0]; ii++) {

      i2 = ii/fima.nd[0];
      i0 = ii%fima.nd[0];

      k = ( i0 + i2*fima.nd[0]*fima.nd[1] ) * ncomp;
      fftw_execute_dft(plani[1], fima.pxl.fd + k, fima.pxl.fd + k );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii, i1, i2, kr, kf)    \
  shared(plani, rima, fima, ncomp)
    for (ii=0; ii < rima.n[2]* rima.n[1]; ii++) {

      i2 = ii/rima.n[1];
      i1 = ii%rima.n[1];

      kr = (i1+ i2*rima.nd[1]) * rima.nd[0] * ncomp;
      kf = (i1+ i2*fima.nd[1]) * fima.nd[0] * ncomp;
      fftw_execute_dft_c2r(plani[0], fima.pxl.fd + kf, rima.pxl.d + kr );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    *image=rima;
  }
  /*---------------------------------------------------------------------------------*/

  return 0;
}

/* added for harmonic implementation 2017/07/05 J. Boisse */
int craft_fft_image_harmonic( CraftImage *image, int flag ){
  int i,ncomp,mode;
  /*=================================================================================*/
  /* there are 3 plans for direct Fourier transforms, one for each dimension,
     and 3 plans for inverse transforms */
  static fftw_plan pland[3];
  static fftw_plan plani[3];
  /* rimap resumes the geometry of the FFT problem of the previous call, in real space */
  static CraftImage rimap = NullCraftImage;
  CraftImage rima, fima;

  /*=================================================================================*/
  /* verification of inputs                                                          */
  /*---------------------------------------------------------------------------------*/
#if defined FFTW2 || defined FFTW3
#else
  fprintf(stderr,"craft_fft_image: impossible case. Contact CraFT developpers.\n");
  return -1;
#endif

  switch( flag ){
  case CRAFT_FFT_INIT_ESTIMATE:
  case CRAFT_FFT_INIT_MEASURE:
  case CRAFT_FFT_INIT_PATIENT:

    switch( image->type[0] ){
    if ( (image->nd[0] != image->n[0]) /* modified for harmonic implementation 2017/09/07 J. Boisse */
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;
    case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_FOURIER_DOUBLE:
    case CRAFT_IMAGE_FOURIER_TENSOR2:
    case CRAFT_IMAGE_FOURIER_VECTOR3D:
    case CRAFT_IMAGE_FOURIER_VECTOR:
    if ( (image->nd[0] != image->n[0]) /* modified for harmonic implementation 2017/09/07 J. Boisse */
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when initializing FFT, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_DOUBLE,\n\tCRAFT_IMAGE_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_VECTOR3D,\n\tCRAFT_IMAGE_VECTOR");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_DOUBLE,\n\tCRAFT_IMAGE_FOURIER_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_VECTOR3D,\n\tCRAFT_IMAGE_FOURIER_VECTOR");
      return -1;
    }

    break;

  case CRAFT_FFT_DIRECT:

    switch( image->type[0] ){
    case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/09/07 J. Boisse */
    case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/09/07 J. Boisse */
      break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when direct FFT is applied, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_DOUBLE,\n\tCRAFT_IMAGE_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_VECTOR3D,\n\tCRAFT_IMAGE_VECTOR");
      return -1;
    }

    if ( (image->nd[0] != image->n[0]) /* modified for harmonic implementation 2017/09/07 J. Boisse */
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }

    break;

  case CRAFT_FFT_INVERSE:

    switch( image->type[0] ){
    case CRAFT_IMAGE_FOURIER_DOUBLE:
    case CRAFT_IMAGE_FOURIER_TENSOR2:
    case CRAFT_IMAGE_FOURIER_VECTOR3D:
    case CRAFT_IMAGE_FOURIER_VECTOR:
      break;
    default:
      fprintf(stderr,"craft_fft_image error: \n");
      fprintf(stderr,"when inverse FFT is applied, input image type must be one of");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_DOUBLE,\n\tCRAFT_IMAGE_FOURIER_TENSOR2");
      fprintf(stderr,"\n\tCRAFT_IMAGE_FOURIER_VECTOR3D,\n\tCRAFT_IMAGE_FOURIER_VECTOR");
      return -1;
    }

    if ( (image->nd[0] != image->n[0]) /* modified for harmonic implementation 2017/09/07 J. Boisse */
      || (image->nd[1] != image->n[1])
      || (image->nd[2] != image->n[2]) ) {
      fprintf(stderr,"craft_fft_image error: inadequacy of image dimensions:\n");
      fprintf(stderr,"      nd=%d %d %d\n",image->nd[0],image->nd[1],image->nd[2]);
      fprintf(stderr,"      n =%d %d %d\n",image->n[0] ,image->n[1] ,image->n[2] );
      return -1;
    }
    break;

  default:
    fprintf(stderr,"craft_fft_image error: flag %d is not allowed.\n", flag);
    return -1;
  }


  /*=================================================================================*/
  /* FFT initialization                                                              */
  /*---------------------------------------------------------------------------------*/
  if ( (flag == CRAFT_FFT_INIT_ESTIMATE)
    || (flag == CRAFT_FFT_INIT_MEASURE)
    || (flag == CRAFT_FFT_INIT_PATIENT) ){
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* if the required initialization is identical as the previous one -> nothing has to be done */
    if ( cmp_size_and_type_craft_images(*image , rimap)==1 ) {
      return 0;
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* craft_image has already been called, thus plans have been made.
     Therefeore, they must be destroyed before re-initialization: */
    if ( cmp_size_and_type_craft_images( rimap , (CraftImage)NullCraftImage) != 1 ) {
      for(i=0;i<3;i++) {
        fftw_destroy_plan(pland[i]);
        fftw_destroy_plan(plani[i]);
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* plans for direct transforms */

    rima = *image;
    fima = *image;

    {
      switch (image->type[0]){
      case CRAFT_IMAGE_HARMONIC_DOUBLE:
        fima.type[0] = CRAFT_IMAGE_FOURIER_DOUBLE;
        fima.type[1] = 1;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_HARMONIC_TENSOR2:
        fima.type[0] = CRAFT_IMAGE_FOURIER_TENSOR2;
        fima.type[1] = 6;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_HARMONIC_VECTOR3D:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D;
        fima.type[1] = 3;
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_HARMONIC_VECTOR:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR;
        fima.type[1] = rima.type[1];
        fima.type[2] = 0;
        fima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) fima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;

      case CRAFT_IMAGE_FOURIER_DOUBLE:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_DOUBLE; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 1;
        rima.type[2] = 0;
        rima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_TENSOR2:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 6;
        rima.type[2] = 0;
        rima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR3D:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_VECTOR3D; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 3;
        rima.type[2] = 0;
        rima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_VECTOR; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = rima.type[1];
        rima.type[2] = 0;
        rima.nd[0] = image->nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        for(i=0;i<3;i++) rima.p[i] = 1. / ( image->p[i]*image->n[i] );
        break;
      }


    }
    ncomp = fima.type[1];

    switch(flag){
    case CRAFT_FFT_INIT_ESTIMATE:
      mode = FFTW_ESTIMATE;
      break;
    case CRAFT_FFT_INIT_MEASURE:
      mode = FFTW_MEASURE;
      break;
    case CRAFT_FFT_INIT_PATIENT:
      mode = FFTW_PATIENT;
      break;
    }

    /* modified for harmonic implementation 2017/09/07 J. Boisse */
    pland[0] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(rima.n[0]),       /* size of dimension */
              ncomp,              /* howmany */

              rima.pxl.hd,        /* input array */
              &rima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist=1 */

              FFTW_FORWARD,
              mode
              );


    pland[1] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(fima.n[1]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* input array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              FFTW_FORWARD,
              mode
              );

    pland[2] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &fima.n[2] ,        /* size of dimension */
              ncomp,              /* howmany=1 */

              fima.pxl.fd,        /* input array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              FFTW_FORWARD,
              mode
              );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* plans for inverse transforms */

    /* modified for harmonic implementation 2017/09/07 J. Boisse */
    plani[0] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(rima.n[0]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* output array */
              &fima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist=1 */

              rima.pxl.hd,         /* input array */
              &rima.nd[0],        /* size of dimension as it has been allocated */
              ncomp,              /* istride */
              1,                  /* idist */

              FFTW_BACKWARD,
              mode
              );

    plani[1] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &(fima.n[1]),       /* size of dimension */
              ncomp,              /* howmany */

              fima.pxl.fd,        /* input array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[1],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0],   /* istride */
              1,                  /* idist */

              FFTW_BACKWARD,
              mode
              );

    plani[2] = fftw_plan_many_dft(
              1,                  /* rank= 1 */
              &fima.n[2] ,        /* size of dimension */
              ncomp,              /* howmany=1 */

              fima.pxl.fd,        /* input array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              fima.pxl.fd,        /* output array */
              &fima.nd[2],        /* size of dimension as it has been allocated */
              ncomp*fima.nd[0]*fima.nd[1], /* istride */
              1,                  /* idist */

              FFTW_BACKWARD,
              mode
              );

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    rimap = rima;
    rimap.pxl.v = (void *)0;
  }
  /*---------------------------------------------------------------------------------*/
  /* direct FFT                                                                      */
  /*---------------------------------------------------------------------------------*/
  else if( flag == CRAFT_FFT_DIRECT ) {
    int ii, i0, i1, i2, k,icomp;
    int kr, kf;

    rima = *image;
    {
      fima = rima;
      switch (rima.type[0]){
      case CRAFT_IMAGE_HARMONIC_DOUBLE: /* modified for harmonic implementation 2017/09/07 J. Boisse */
        fima.type[0] = CRAFT_IMAGE_FOURIER_DOUBLE;
        fima.type[1] = 1;
        break;
      case CRAFT_IMAGE_HARMONIC_TENSOR2:
        fima.type[0] = CRAFT_IMAGE_FOURIER_TENSOR2; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        fima.type[1] = 6;
        break;
      case CRAFT_IMAGE_HARMONIC_VECTOR3D:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR3D; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        fima.type[1] = 3;
        break;
      case CRAFT_IMAGE_HARMONIC_VECTOR:
        fima.type[0] = CRAFT_IMAGE_FOURIER_VECTOR; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        fima.type[1] = rima.type[1];
        break;
      }
      ncomp = fima.type[1];
      fima.type[2] = 0;
      fima.nd[0] = rima.nd[0];
      for(i=0;i<3;i++) fima.p[i] = 1. / ( fima.p[i]*fima.n[i] );

    }

    if ( cmp_size_and_type_craft_images( rima , rimap)==0 ) {
      //      fprintf(stderr,"craft_fft_image error: direct FFT called without preliminary initialization.\n");
      //      return -1;
      craft_fft_image( image, CRAFT_FFT_INIT_MEASURE );
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii, i1, i2, kr, kf)    \
  shared(pland, rima, fima, ncomp)
    for (ii=0; ii < rima.n[2]* rima.n[1]; ii++) {

      i2 = ii/rima.n[1];
      i1 = ii%rima.n[1];

      kr = (i1+ i2*rima.nd[1]) * rima.nd[0] * ncomp;
      kf = (i1+ i2*fima.nd[1]) * fima.nd[0] * ncomp;
      fftw_execute_dft( pland[0], rima.pxl.hd + kr, fima.pxl.fd + kf ); /* modified for harmonic implementation 2017/09/07 J. Boisse */
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i0,i2,k)            \
  shared( pland , fima, ncomp)
    for(ii=0; ii<fima.n[2]*fima.nd[0]; ii++) {

      i2 = ii/fima.nd[0];
      i0 = ii%fima.nd[0];

      k = ( i0 + i2*fima.nd[0]*fima.nd[1] ) * ncomp;
      fftw_execute_dft( pland[1], fima.pxl.fd + k, fima.pxl.fd + k );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if ( fima.n[2] > 1 ) {    /* it's useless to apply FFT on the 3d direction if its width is 1 */
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i1,i0,k)            \
  shared( pland , fima, ncomp)
      for(ii=0; ii<fima.n[1]*fima.nd[0];ii++) {

        i1 = ii/fima.nd[0];
        i0 = ii%fima.nd[0];

        k = ( i0 + i1*fima.nd[0] ) * ncomp;
        fftw_execute_dft(pland[2], fima.pxl.fd + k, fima.pxl.fd + k );
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* normalization                                                                 */
    double coef = 1./ ( (double)fima.n[0] * (double)fima.n[1] * (double)fima.n[2] );
    switch(fima.type[0]){
    case CRAFT_IMAGE_FOURIER_DOUBLE:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++)
        (fima.pxl).fd[i] *= coef;
      break;

    case CRAFT_IMAGE_FOURIER_TENSOR2:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++){
        (fima.pxl).ft2[i][0] *= coef;
        (fima.pxl).ft2[i][1] *= coef;
        (fima.pxl).ft2[i][2] *= coef;
        (fima.pxl).ft2[i][3] *= coef;
        (fima.pxl).ft2[i][4] *= coef;
        (fima.pxl).ft2[i][5] *= coef;
      }
      break;

    case CRAFT_IMAGE_FOURIER_VECTOR3D:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++){
        (fima.pxl).fv3d[i][0] *= coef;
        (fima.pxl).fv3d[i][1] *= coef;
        (fima.pxl).fv3d[i][2] *= coef;
      }
      break;

    case CRAFT_IMAGE_FOURIER_VECTOR:
#pragma omp parallel for         \
  default(none)                  \
  private(i, icomp)              \
  shared( coef , fima)
      for(i=0; i< fima.nd[0] * fima.nd[1] * fima.nd[2] ; i++) {
        for(icomp=0; icomp<fima.type[1]; icomp++) {
          (fima.pxl).a[i*fima.type[1] + icomp] *= coef;
        }
      }
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    *image=fima;
    rimap = rima;
    rimap.pxl.v = (void *)0;
  }
  /*---------------------------------------------------------------------------------*/
  /* inverse FFT                                                                     */
  /*---------------------------------------------------------------------------------*/
  else if( flag == CRAFT_FFT_INVERSE ) {
    int ii, i0, i1, i2, k;
    int kr, kf;

    fima = *image;
    {
      rima = fima;
      switch (fima.type[0]){
      case CRAFT_IMAGE_FOURIER_DOUBLE:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_DOUBLE; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 1;
        break;
      case CRAFT_IMAGE_FOURIER_TENSOR2:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_TENSOR2; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 6;
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR3D:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_VECTOR3D; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = 3;
        break;
      case CRAFT_IMAGE_FOURIER_VECTOR:
        rima.type[0] = CRAFT_IMAGE_HARMONIC_VECTOR; /* modified for harmonic implementation 2017/09/07 J. Boisse */
        rima.type[1] = fima.type[1];
        break;
      }
      ncomp = rima.type[1];
      rima.type[2] = 0;
      rima.nd[0] = fima.nd[0]; /* modified for harmonic implementation 2017/09/07 J. Boisse */
      for(i=0;i<3;i++) rima.p[i] = 1. / ( rima.p[i]*rima.n[i] );

    }

    if ( cmp_size_and_type_craft_images( rima , rimap)==0 ) {
      //      fprintf(stderr,"craft_fft_image error: direct FFT called without preliminary initialization.\n");
      //      return -1;
      craft_fft_image( image, CRAFT_FFT_INIT_MEASURE );
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    if ( fima.n[2] > 1 ) {    /* it's useless to apply FFT on the 3d direction if its width is 1 */
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i1,i0,k)            \
  shared( plani , fima, ncomp)
      for(ii=0; ii<fima.n[1]*fima.nd[0];ii++) {

        i1 = ii/fima.nd[0];
        i0 = ii%fima.nd[0];

        k = ( i0 + i1*fima.nd[0] ) * ncomp;
        fftw_execute_dft( plani[2], fima.pxl.fd + k, fima.pxl.fd + k );
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii,i0,i2,k)            \
  shared( plani , fima, ncomp)
    for(ii=0; ii<fima.n[2]*fima.nd[0]; ii++) {

      i2 = ii/fima.nd[0];
      i0 = ii%fima.nd[0];

      k = ( i0 + i2*fima.nd[0]*fima.nd[1] ) * ncomp;
      fftw_execute_dft(plani[1], fima.pxl.fd + k, fima.pxl.fd + k );
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel for         \
  default(none)                  \
  private(ii, i1, i2, kr, kf)    \
  shared(plani, rima, fima, ncomp)
    for (ii=0; ii < rima.n[2]* rima.n[1]; ii++) {

      i2 = ii/rima.n[1];
      i1 = ii%rima.n[1];

      kr = (i1+ i2*rima.nd[1]) * rima.nd[0] * ncomp;
      kf = (i1+ i2*fima.nd[1]) * fima.nd[0] * ncomp;
      fftw_execute_dft(plani[0], fima.pxl.fd + kf, rima.pxl.hd + kr ); /* modified for harmonic implementation 2017/09/07 J. Boisse */
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    *image=rima;
  }
  /*---------------------------------------------------------------------------------*/

  return 0;
}

/* Compute the discrete spatial frequencies (with units 1/m, not rad/m)
   The input CraftImage must be a CRAFT_IMAGE_FOURIER_TENSOR2,
   so that its dimensions (nd) and steps (p) have been updated to take into
   account the complex datatype.
*/
int craft_fft_frequency_vectors( CraftImage *image, double **ksi1, double **ksi2, double **ksi3){
  int i;

  if (  (image->type[0] != CRAFT_IMAGE_FOURIER_DOUBLE)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_VECTOR3D)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_VECTOR) ){
    fprintf(stderr, "Image has not the CRAFT_IMAGE_FOURIER_* type (is %d).\n", 
      image->type[0]);
    return -1;
  }

  *ksi1 = malloc(image->nd[0]*sizeof(double));
  *ksi2 = malloc(image->n[1]*sizeof(double));
  *ksi3 = malloc(image->n[2]*sizeof(double));

  if ( (*ksi1==NULL) || (*ksi2==NULL) || (*ksi3==NULL) ){
    free(ksi1);
    free(ksi2);
    free(ksi3);
    ksi1 = ksi2 = ksi3 = NULL;
    fprintf(stderr, "Unable to allocate frequency vectors.\n");
    return -1;
  }

  for(i=0; i<=image->n[0]/2; i++){
    (*ksi1)[i] = image->p[0]*(double)(i);
  }
  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[0]/2+1; i<image->nd[0]; i++){
    (*ksi1)[i] = 1.0;
  }

  for(i=0; i<=image->n[1]/2; i++){
    (*ksi2)[i] = image->p[1]*(double)(i);
  }
  for(i=image->n[1]/2+1; i<image->n[1]; i++){
    (*ksi2)[i] = image->p[1]*(double)(i-image->n[1]);
  }
  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[1]; i<image->nd[1]; i++){
    (*ksi2)[i] = 1.;
  }

  for(i=0; i<=image->n[2]/2; i++){
    (*ksi3)[i] = image->p[2]*(double)(i);
  }
  for(i=image->n[2]/2+1; i<image->n[2]; i++){
    (*ksi3)[i] = image->p[2]*(double)(i-image->n[2]);
  }
  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[2]; i<image->nd[2]; i++){
    (*ksi3)[i] = 1.;
  }

  return 0;
}

 /* added for harmonic implementation 2017/09/07 J. Boisse */
int craft_fft_frequency_vectors_harmonic( CraftImage *image, double **ksi1, double **ksi2, double **ksi3){

  int i;

  if (  (image->type[0] != CRAFT_IMAGE_FOURIER_DOUBLE)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_VECTOR3D)
     && (image->type[0] != CRAFT_IMAGE_FOURIER_VECTOR) ){
    fprintf(stderr, "Image has not the CRAFT_IMAGE_FOURIER_* type (is %d).\n", 
      image->type[0]);
    return -1;
  }

//  *ksi1 = malloc(image->nd[0]*sizeof(double));
  *ksi1 = malloc(image->n[0]*sizeof(double)); /* added for harmonic implementation 2018/07/10 J. Boisse */
  *ksi2 = malloc(image->n[1]*sizeof(double));
  *ksi3 = malloc(image->n[2]*sizeof(double));

  if ( (*ksi1==NULL) || (*ksi2==NULL) || (*ksi3==NULL) ){
    free(ksi1);
    free(ksi2);
    free(ksi3);
    ksi1 = ksi2 = ksi3 = NULL;
    fprintf(stderr, "Unable to allocate frequency vectors.\n");
    return -1;
  }

  for(i=0; i<=image->n[0]/2; i++){
    (*ksi1)[i] = image->p[0]*(double)(i);
    //printf("k[%d] = %f\n",i,(*ksi1)[i]);
  }

  /* added for harmonic implementation 2017/09/07 J. Boisse */
  for(i=image->n[0]/2+1; i<image->n[0]; i++){
    (*ksi1)[i] = image->p[0]*(double)(i-image->n[0]);
  }

  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[0]; i<image->nd[0]; i++){  /* modified for harmonic implementation 2017/09/07 J. Boisse */
    (*ksi1)[i] = 1.0;
  }

  for(i=0; i<=image->n[1]/2; i++){
    (*ksi2)[i] = image->p[1]*(double)(i);
  }
  for(i=image->n[1]/2+1; i<image->n[1]; i++){
    (*ksi2)[i] = image->p[1]*(double)(i-image->n[1]);
  }
  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[1]; i<image->nd[1]; i++){
    (*ksi2)[i] = 1.;
  }

  for(i=0; i<=image->n[2]/2; i++){
    (*ksi3)[i] = image->p[2]*(double)(i);
  }
  for(i=image->n[2]/2+1; i<image->n[2]; i++){
    (*ksi3)[i] = image->p[2]*(double)(i-image->n[2]);
  }
  /* The extra elements depend on the implementation of FFT
     Should not enter here when using FFTw3 */
  for(i=image->n[2]; i<image->nd[2]; i++){
    (*ksi3)[i] = 1.;
  }

  /* filtered frequencies */
  /* added for harmonic implementation 2017/10/13 J. Boisse */
/*
  const double pi = 4. * atan(1.);
  double pi_on_n_p[3];
  for(i=0;i<3;i++) pi_on_n_p[i] = pi/((image->n[i])*image->p[i]);
  printf("pi = %f\n", pi);
  printf("p[0] = %f\n", image->p[0]);
  printf("p[1] = %f\n", image->p[1]);
  printf("p[2] = %f\n", image->p[2]);
  for(i=0; i<=image->n[0]; i++){
    (*ksi1)[i] = sin(pi_on_n_p[0]*(*ksi1)[i])/pi_on_n_p[0];
  }  
  for(i=0; i<=image->n[1]; i++){
    (*ksi2)[i] = sin(pi_on_n_p[1]*(*ksi1)[i])/pi_on_n_p[1];
  }  
  for(i=0; i<=image->n[2]; i++){
    (*ksi3)[i] = sin(pi_on_n_p[2]*(*ksi1)[i])/pi_on_n_p[2];
  }  
*/
  return 0;
}
