#define _ISOC99_SOURCE
#define _DEFAULT_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>


#include <craft.h>
#include <craftimage.h>
#include <materials.h>
#include <loading.h>
#include <craftoutput.h>
#include <variables.h>
#include <io_variables.h>
#include <utils.h>
#include <craft_schemes.h>
#include <craft_compatibility_equilibrium.h>
#include <craft_log.h>
#include <craft_options.h>
#include <craft_time.h>

#ifdef _OPENMP
#include <omp.h>
#endif

int craft_log(
              char *cfn,      /* name of the image file describing the characteristic function */
              char *tfn,      /* name of the image file describing the thermal strain field (if any)  */
              char *pfn,      /* name of the file describing the phases of the material      */
              char *mfn,      /* name of the file describing the the materials composing the microstructure */
              char *lfn,      /* name of the file describing the loading conditions */
              char *tlfn,      /* name of the file describing the temperature loading conditions */
              char *rfn,      /* name of the file containing a saved state of variables */
              char *ofn,      /* name of the file describing required outputs            */

              Loading load,   /* loading conditions                                          */
              
              int it_save,    /* first instant to handle */
              
              Material C0,    /* "reference material"                                        */
              
              int Nph,        /* total number of phases in the microstructure */             
              int *phase,     /* indices of the phases in the microstructure  */
                              /* phase[iph] is the phase index as given in the microstructure image
                                 of the i-th phase (indexed form 0 to Nph-1)                 */
              int *nppp,      /* number of points per phase */
              
              Material *material,          /* material[iph] gives every information about the material
                                              in phase iph */
              int scheme,
              double alpha, double beta,
              
              double compatibility_precision,   /* precision required by user on strain 
                                                   compatibility */
              double divs_precision,   /* precision required by user on divergence of the
                                          stress                                                      */
              double macro_precision,  /* precision required by user on macroscopic stress
                                          or direction of macroscopic
                                          stress (depending on loading conditions)                    */
              int convergence_test_method, /* method to be employed for convergence test  */
              int maxiterations, /* maximum number of authorized iterations for solving Lippmann-Schwinger equation */
              
              Craft_Output_Description cod /* output description */
              ){
    /*=============================================================================*/
  /* fills header of output file                                                 */
  /*=============================================================================*/
  char hostname[50];
  char *filelog;
  FILE *flog;

  int status;
  int iph;
  
#ifdef CRAFT_VERSION
  char *craft_version=CRAFT_VERSION;
#else
  char *craft_version="?";
#endif

  
  filelog=(char *)malloc(strlen(cod.generic_name)+strlen(".log")+1);
  strcpy(filelog,cod.generic_name);
  strcat(filelog,".log");

  if (it_save>1){
    flog = fopen(filelog,"a");
    fprintf(flog,"#===================================================================\n");
    fprintf(flog,"# Continuing previous computation from file %s\n", rfn);
    fprintf(flog,"# at instant %lf (index %d)\n", load.step[it_save].time, it_save);
    if (verbose){
      fprintf(stdout,"Continuing previous computation from file %s\n", rfn);
      fprintf(stdout," at instant %lf (index %d)\n", load.step[it_save].time, it_save);
    }
  }
  else{
    flog = fopen(filelog,"w");
    fprintf(flog,"#===================================================================\n");
    fprintf(flog,"# Craft version %s\n",craft_version);
#ifdef _OPENMP
    fprintf(flog, "# OpenMP parallel version\n#  number of threads=%d\n",
      omp_get_max_threads());
#else
    fprintf(flog,"# non OpenMP parallel version\n ");
#endif
    fprintf(flog,"# \n");
    {
      char *date;
      date = craft_date(1);
      fprintf(flog,"# date: %s\n",date);
      free(date);
    }

    gethostname(hostname,sizeof(hostname));
    fprintf(flog,"# host : %s\n",hostname);
    fprintf(flog,"# \n");
    fprintf(flog,"#===================================================================\n");
    fprintf(flog,"# Inputs: \n\n");

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (test_temporary_option_file(cfn)==0) {
      fprintf(flog,"# microstructure name : %s\n",cfn);
    }
    else{
      fprintf(flog,"# microstructure: \n");
      {
        FILE *f;
        char c;
        f = fopen(cfn,"r");
        fprintf(flog,"#");
        do{
          c=fgetc(f);
          if(c=='\0') fprintf(flog,"#%c",c);
          else fprintf(flog,"%c",c);
        } while(c!=EOF);
        fclose(f);
      }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    fprintf(flog,"#    ( => number of phases = %d ) \n",Nph);
    if(verbose){
      for(iph=0;iph<Nph;iph++) {
        fprintf(flog,"#    number of pixels in phase %d (%d) = %d\n",iph,phase[iph],nppp[iph]);
      }
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if(tfn!=(char *)0){
      status=test_temporary_option_file(tfn);
      switch(test_temporary_option_file(tfn)){
      case -1:
        fprintf(stderr,"CraFT error: file: %s does not exist.\n",tfn);
        fprintf(stderr,"exit\n");
        exit(1);
        break;
      case 0:
        fprintf(flog,"# thermal strain image : %s\n",tfn);
        break;
      case 1:
        fprintf(flog,"# thermal strain image: \n");
        {
          FILE *f;
          char c;
          f = fopen(tfn,"r");
          fprintf(flog,"#");
          do{
            c=fgetc(f);
            if(c=='\0') fprintf(flog,"#%c",c);
            else fprintf(flog,"%c",c);
          } while(c!=EOF);
          fclose(f);
        }
      default:
        fprintf(stderr,"CraFT error: problem trying to read file: %s\n",tfn);
        fprintf(stderr,"exit\n");
        exit(1);
        break;
      }
    }

    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (test_temporary_option_file(pfn)==0) {
      fprintf(flog,"# file describing phase properties: %s\n",pfn);
    }
    else{
      {
        FILE *f;
        unsigned char c;
        f = fopen(pfn,"r");
        fprintf(flog,"#");
        do{
          status=fread(&c,1,1,f);
          if(c == '\n') fprintf(flog,"%c#",c);
          else fprintf(flog,"%c",c);
        } while(status==1);
        fclose(f);
      }
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if (test_temporary_option_file(mfn)==0) {
      fprintf(flog,"# file describing material properties: %s\n",mfn);
    }
    else{
      {
        FILE *f;
        unsigned char c;
        f = fopen(mfn,"r");
        fprintf(flog,"#");
        do{
          status=fread(&c,1,1,f);
          if(c == '\n') fprintf(flog,"%c#",c);
          else fprintf(flog,"%c",c);
        } while(status==1);
        fclose(f);
      }
    }

    fprintf(flog,"#\n");
    if(verbose){
      for(iph=0; iph<Nph; iph++) {
#ifdef DEBUG
        fprintf(stdout, "phase=%d material=%d behavior=%s \nparameters:\n",
            iph,phase[iph],material[iph].typename ) ;
        material[iph].print_parameters(stdout, material[iph].parameters,1);
#endif
        fprintf(flog,"#phase %d :\n",phase[iph]);
        material[iph].print_parameters(flog, material[iph].parameters,1);
      }
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    fprintf(flog,"# reference material : \n");
    C0.print_parameters(flog, C0.parameters ,1);
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    fprintf(flog,"# loading conditions: \n");
    if (test_temporary_option_file(lfn)==0) {
      fprintf(flog,"#  file name: %s\n",lfn);
    }
    else
    {
      {
        FILE *f;
        unsigned char c;
        f = fopen(lfn,"r");
        fprintf(flog,"#");
        do{
          status=fread(&c,1,1,f);
          if(c == '\n') fprintf(flog,"%c#",c);
          else fprintf(flog,"%c",c);
        } while(status==1);
        fclose(f);
      }
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    fprintf(flog,"# temperature loading conditions: \n");
    if (test_temporary_option_file(tlfn)==0) {
      fprintf(flog,"#  file name: %s\n",tlfn);
    }
    else
    {
      {
        FILE *f;
        unsigned char c;
        f = fopen(tlfn,"r");
        fprintf(flog,"#");
        do{
          status=fread(&c,1,1,f);
          if(c == '\n') fprintf(flog,"%c#",c);
          else fprintf(flog,"%c",c);
        } while(status==1);
        fclose(f);
      }
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fprintf(flog,"# convergence test method: ");
    switch(convergence_test_method){
    case CRAFT_TEST_NOT_SET:
      fprintf(flog," not set -> error\n");
      exit(1);
      break;
    case CRAFT_TEST_DIVS_SUM:
      fprintf(flog," |div(sigma)|/|Sigma|  (L2 norm) \n");
      break;
    case CRAFT_TEST_DIVS_MAX:
      fprintf(flog," |div(sigma)|/|Sigma|  (max norm) \n");
      break;
    case CRAFT_TEST_MONCHIET_BONNET:
      fprintf(flog," Monchiet-Bonnet 2011 \n");
      break;
    default:
      fprintf(stderr,"CraFT error: invalid error test method\n");
      exit(1);
    break;  
      
    }
    fprintf(flog,"# precision required for div(sigma): %g\n",divs_precision);
    fprintf(flog,"# precision required for strain field compatibility: %g\n",compatibility_precision);
    switch( load.type){
    case STRESS_PRESCRIBED:
      fprintf(flog,"# precision required for macroscopic stress: %g\n",macro_precision);
      break;
    case STRESS_DIRECTION_PRESCRIBED:
      fprintf(flog,"# precision required for direction of macroscopic stress: %g\n",macro_precision);
      break;
    }
    fprintf(flog,"#\n");
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fprintf(flog,"# Iterative scheme: %d\n", scheme);
    switch(scheme){
    case CRAFT_SCHEME_NOT_SET:
      fprintf(flog,"#  (scheme has not been set)\n");
      break;
    case CRAFT_BASIC_SCHEME:
      fprintf(flog,"#  (Basic scheme)\n");
      break;
    case CRAFT_MILTON_EYRE_SCHEME:
      fprintf(flog,"#  (Milton Eyre scheme, alpha=%g beta=%g)\n",alpha,beta);
      break;
    case CRAFT_LAGRANGIAN_SCHEME:
      fprintf(flog,"#  (Augmented Lagrangian scheme, alpha=%g beta=%g)\n",alpha,beta);
      break;
    case CRAFT_ALPHA_BETA_SCHEME:
      fprintf(flog,"#  (Monchiet Bonnet scheme, alpha=%g beta=%g)\n",alpha,beta);
      break;
    default:
      fprintf(flog,"#  (invalid scheme)\n");
      break;
    }
    fprintf(flog,"#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
  }
  fclose(flog);
  free(filelog);
  /*-----------------------------------------------------------------------------*/
  /* one deleters the temporary files that had been used to specify the problem   */
  /* and that are no longer useful                                               */
  /*-----------------------------------------------------------------------------*/

  if (test_temporary_option_file(cfn)) remove(cfn);
  if (test_temporary_option_file(tfn)) remove(tfn);
  if (test_temporary_option_file(mfn)) remove(mfn);
  if (test_temporary_option_file(pfn)) remove(pfn);
  if (test_temporary_option_file(lfn)) remove(lfn);
  if (test_temporary_option_file(tlfn)) remove(tlfn);
  if (test_temporary_option_file(ofn)) remove(ofn);

}
