#ifndef __CRAFT_LOG__
#define __CRAFT_LOG__

int craft_log(
              char *cfn,                      /* name of the image file describing the characteristic
                                                 function                                                    */
              char *tfn,                      /* name of the image file describing the thermal strain field
                                                 (if any)                                                    */
              char *pfn,      /* name of the file describing the phases of the material      */
              
              char *mfn,      /* name of the file describing the the materials composing
                                 the microstructure */
              char *lfn,      /* name of the file describing the loading conditions          */
              char *tlfn,      /* name of the file describing the temperature loading conditions          */
              
              char *rfn,      /* name of the file containing a saved state of variables */

              char *ofn,          /* name of the file describing required outputs            */

              Loading load,   /* loading conditions                                          */
              
              int it_save,    /* first instant to handle */
              
              Material C0,    /* "reference material"                                        */
              
              int Nph,        /* total number of phases in the microstructure */             
              int *phase,     /* indices of the phases in the microstructure  */
                              /* phase[iph] is the phase index as given in the microstructure image
                                 of the i-th phase (indexed form 0 to Nph-1)                 */
              int *nppp,      /* number of points per phase */
              
              Material *material,          /* material[iph] gives every information about the material
                                              in phase iph */
              int scheme,
              double alpha, double beta,
              
              double compatibility_precision,   /* precision required by user on strain 
                                                   compatibility */
              double divs_precision,   /* precision required by user on divergence of the
                                          stress                                                      */
              double macro_precision,  /* precision required by user on macroscopic stress
                                          or direction of macroscopic
                                          stress (depending on loading conditions)                    */
              int convergence_test_method, /* method to be employed for convergence test  */
              int maxiterations, /* maximum number of authorized iterations for solving Lippmann-Schwinger equation */
              
              Craft_Output_Description cod /* output description */
              );

#endif
