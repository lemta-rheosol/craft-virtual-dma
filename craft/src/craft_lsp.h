#ifndef __CRAFT_LSP__
#define __CRAFT_LSP__
/***********************************************************************************************/
void lspp(
           int *n1, int *n2, int *n3,
           int *nd1, int *nd2, int *nd3, 
           double *ksi1, double *ksi2, double *ksi3, 
           double *lb0, double *mu0,
           double complex *t,
           double complex *u,
           int *flag,
           double *error);
int lsp( CraftImage *image, LE_param *C0, double *divs);
int lsp2( CraftImage *tau, CraftImage *rotation, LE_param *C0, double *divs);

/* added for harmonic implementation 2017/07/05 J. Boisse */
int lsp_harmonic( CraftImage *image, LE_param *C0, double *divs);
void lspp_harmonic(
           int *n1, int *n2, int *n3,
           int *nd1, int *nd2, int *nd3, 
           double *ksi1, double *ksi2, double *ksi3, 
           double *lb0, double *mu0,
           double complex *t,
           double complex *u,
           int *flag,
           double *error);
/***********************************************************************************************/
#endif
