#define _ISOC99_SOURCE
#define _DEFAULT_SOURCE

#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <limits.h>

#include <errno.h>

#include <utils.h>
#include <craft.h>
#include <craft_options.h>
#include <craft_compatibility_equilibrium.h>
#include <craft_schemes.h>
#include <craft_time.h>

/* the keywords that can be used in input specifications file (the option "-f" file) */
char *keyword[]={
  "microstructure",
  "phases",
  "materials",
  "loading",
  "output",
  "C0",
  "precision",
  "scheme",
  "thermalstrain",
  "temperature"
};
int nkw=10;


/* the date at which temporary files are created */
char *date;


/*************************************************************************************************/

int craft_options(
    int argc,
    char *const argv[],
    char **cfn, /* file name of the characteristic function */
    char **tfn, /* file name of the image of the thermal strain (if any) */
    char **pfn, /* name of the file describing the phases */
    char **mfn, /* name of the file describing the materials */
    char **lfn, /* name of the file describing the loading conditions */
    char **tlfn, /* name of the file describing the temperature loading conditions */
    char **ofn, /* name of the file describing the outputs required */
    char **rfn, /* name of the file containing a saved state */
    char **C0_line_command, /* command line saying how to choose C0 */
    double *divs_precision, /* precision required for divergence of stress */
    double *compatibility_precision, /* precision required for compatibility conditions of the strain field*/
    double *macro_precision, /* precision required for macroscopic stress or
                direction of macroscopic stress (depending on
                required loading conditions)  */
    int *convergence_test_method, /* method to be employed for convergence test*/
    int *maxiterations, /* maximum authorized number of iterations for solving 
                              Lippmann-Schwinger equation */
    int *scheme, /* scheme used */
    double *alpha, double *beta, /* alpha & beta parameters of Monchiet & Bonnet accelerated scheme */
    int *nr_threads, /* number of OMP threads to be used */
    int *verbose /* verbosity */
    ) {

  int opt;
  FILE *f;
  char *ifn;
  int status;

  /* initialization of all the options */
  ifn=(char *)0;
  *cfn=(char *)0;
  *tfn=(char *)0;
  *pfn=(char *)0;
  *mfn=(char *)0;
  *lfn=(char *)0;
  *ofn=(char *)0;
  *rfn=(char *)0;
  *tlfn=(char *)0;

  *C0_line_command=(char *)0;

  *divs_precision=NAN;
  *compatibility_precision=NAN;
  *macro_precision=NAN;
  *convergence_test_method=CRAFT_TEST_NOT_SET;
  *maxiterations=INT_MAX;

  *scheme=CRAFT_SCHEME_NOT_SET;
  *alpha=NAN;
  *beta=NAN;

  *nr_threads=-1;
  *verbose=0;
  int ask=-1; /*
                 ask=-1   : inputs have still not been specified
                 ask=0    : inputs are specified by options -c -p -m -l -o -C and -e
                 ask=1    : inputs are to be got interactively
                 ask=2    : inputs are to be read in a file (with -f option)
              */
              
  *divs_precision=-1.;
  while ( (opt=getopt(argc,argv,"hvVic:p:m:l:o:C:e:f:n:r:s:t:")) != -1 ) {
    switch(opt){
    case 'h':
      usage();
      exit(0);
      break;
    case 'V':
#ifdef HARMONIC
      fprintf(stderr,"CraFT version ");
      fprintf(stderr,CRAFT_VERSION);
      fprintf(stderr," Harmonic\n");
#else
      fprintf(stderr,"CraFT version ");
      fprintf(stderr,CRAFT_VERSION);
      fprintf(stderr,"\n");
#endif
      exit(0);
      break;
    case 'v':
      *verbose=1;
      break;
    case 'i':
      if ( (ask!=-1)&&(ask!=1) ) {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -i incompatible with options -f -c -m -p -o -l -r -C and -e \n");
        exit(1);
      }
      ask=1;
      break;
    case 'f':
      if(ask!=-1){
        fprintf(stderr,"CraFT error: \n");
        if(ask==2) {
          fprintf(stderr," option -f cannot be invoked more then once.\n");
        }
        else{
          fprintf(stderr,"  option -f incompatible with options -i -c -m -p -o -l -C and -e \n");
        }
        exit(1);
      }
      ifn = malloc(strlen(optarg)+1);
      if (ifn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(ifn,optarg);
      ask=2;
      break;
    case 'c':
      if ( (ask!=-1) && (ask!=0) ) {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -c incompatible with options -i or -f\n");
        exit(1);
      }
      *cfn = malloc(strlen(optarg)+1);
      if (*cfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*cfn,optarg);
      ask=0;
      break;
    case 't':
      if ( (ask!=-1) && (ask!=0) ) {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -t incompatible with options -i or -f\n");
        exit(1);
      }
      *tfn = malloc(strlen(optarg)+1);
      if (*tfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*tfn,optarg);
      ask=0;
      break;
    case 'p':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -p incompatible with options -i or -f\n");
        exit(1);
      }
      *pfn = malloc(strlen(optarg)+1);
      if (*pfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*pfn,optarg);
      ask=0;
      break;
    case 'm':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -m incompatible with options -i or -f\n");
        exit(1);
      }
      *mfn = malloc(strlen(optarg)+1);
      if (*mfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*mfn,optarg);
      ask=0;
      break;
    case 'l':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -l incompatible with options -i or -f\n");
        exit(1);
      }
      *lfn = malloc(strlen(optarg)+1);
      if (*lfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*lfn,optarg);
      ask=0;
      break;
    case 'T':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -l incompatible with options -i or -f\n");
        exit(1);
      }
      *tlfn = malloc(strlen(optarg)+1);
      if (*tlfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*tlfn,optarg);
      ask=0;
      break;
    case 'o':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -o incompatible with options -i or -f\n");
        exit(1);
      }
      *ofn = malloc(strlen(optarg)+1);
      if (*ofn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*ofn,optarg);
      ask=0;
      break;
    case 'r':
      if ( (ask!=-1) && (ask!=0) && (ask!=2) ) {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -r incompatible with option -i\n");
        exit(1);
      }
      *rfn = malloc(strlen(optarg)+1);
      if (*rfn == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*rfn,optarg);
      /* ask=0; the -r option in kept compatible with -f */
      break;
     case 'C':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -C incompatible with options -i or -f\n");
        exit(1);
      }
      *C0_line_command = malloc(strlen(optarg)+1);
      if (*C0_line_command == (char *)0 ){
        fprintf(stderr,"CraFT error: problems in reading %s\n",optarg);
        exit(1);
      }
      strcpy(*C0_line_command,optarg);
      ask=0;
      break;
    case 'e':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -e incompatible with options -i or -f\n");
        exit(1);
      }
      printf("optarg=%s\n",optarg);
      status=sscanf(optarg,"%lf,%lf,%lf,%d,%d",
                    divs_precision, macro_precision, compatibility_precision,
                    convergence_test_method,maxiterations);
      /* if the user does not specify the test error method, 
         |div(sigma)|/|S| is chosen */
      /* if the user specifies just one precision, it is supposed to be the same for 
         divs_precision, macro_precision and compatibility_precision*/
      switch(status){
      case 1:
        *compatibility_precision=*divs_precision;
        *macro_precision=*divs_precision;
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
        break;
      case 2:
      /* if the user specifies just two values precision, it is supposed to be the same for 
         divs_precision, and compatibility_precision*/
        *compatibility_precision=*macro_precision;
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
        break;
      case 3:
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
	break;
      case 4:
        *maxiterations=INT_MAX;
        break;
      case 5:
	break;
      default:
        fprintf(stderr,"CraFT error in reading precision required \n");
        exit(1);
      }
      ask=0;
      break;
    case 's':
      if ( (ask!=-1) && (ask!=0) ) {
        //      if (ask!=0)  {
        fprintf(stderr,"CraFT error: \n");
        fprintf(stderr,"  option -s incompatible with options -i or -f\n");
        exit(1);
      }
      printf("optarg=%s\n",optarg);
      status=sscanf(optarg,"%d %lf %lf",scheme,alpha,beta);
      ask=0;
      break;
    case 'n':
      if ( sscanf(optarg,"%d",nr_threads) != 1 ) {
        fprintf(stderr,"CraFT error in reading number of OMP threads to be used \n");
      }
      break;
    default:
      fprintf(stderr,"CraFT error: invalid option -%c\n",opt);
      usage();
      exit(1);
      break;
    }
  }

  /*------------------------------------------------------------------------------*/
  /* in the case when an input file has been entered without key "-f": */
  if (optind<argc) {

    if(ask!=-1){
      fprintf(stderr,"CraFT error: \n");
      if(ask==2) {
        fprintf(stderr," option -f cannot be invoked more then once.\n");
      }
      else{
        fprintf(stderr,"  option -f incompatible with options -i -c -m -p -o -l -C and -e \n");
      }
      exit(1);
    }
    ifn = malloc(strlen(argv[optind])+1);
    if (ifn == (char *)0 ){
      fprintf(stderr,"CraFT error: problems in reading %s\n",argv[optind]);
      exit(1);
    }
    strcpy(ifn,argv[optind]);
    ask=2;
    
  }

  /*------------------------------------------------------------------------------*/

  if (ask==-1) ask=1;
  /*------------------------------------------------------------------------------*/
  /* if option -i has been ivoked, one reads all the options in the standard input
   */
  /*------------------------------------------------------------------------------*/
  if (ask==1) {
    char buff[1024];
    f=stdin;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* enters the name of the image of the characteristic function                 */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the image of the characteristic function: ");
    craft_fgets(buff,1024,f);
    *cfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*cfn);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* enters the name of the file describing the phases */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the file describing the phases: ");
    craft_fscanf(f,"%s",buff);
    *pfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*pfn);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* enters the name of the file describing the materials */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the file describing the materials: ");
    craft_fgets(buff,1024,f);
    *mfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*mfn);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Enter the name of the file describing the loading conditions                */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the file describing the loading conditions : ");
    craft_fgets(buff,1024,f);
    *lfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*lfn);


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Enter the name of the file describing the temperature loading conditions    */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the file describing the temperature loading conditions : ");
    craft_fgets(buff,1024,f);
    *tlfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*tlfn);


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* reads file containg the list of required outputs                            */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of the file describing required outputs: ");
    craft_fgets(buff,1024,f);
    *ofn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*ofn);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* reads file containing a saved state (restoring previous calculations)       */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the name of a file containing a previous computation: ");
    craft_fgets(buff,1024,f);
    *rfn = (char *)malloc(strlen(buff)+1);
    sscanf(buff,"%s",*rfn);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* reference material C0                                                       */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter C0: \n");
    fprintf(stdout,"          auto \n");
    fprintf(stdout,"          param   lb0 mu0 \n");

    craft_fgets(buff,1024,f);
    *C0_line_command = (char *)malloc(strlen(buff)+1);
    //    strcpy(*C0_line_command , buff);
    sscanf(buff,"%s",*C0_line_command);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Required precision                                                          */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter required precision: ");
    craft_fgets(buff,1024,f);
      status=sscanf(optarg,"%lf,%lf,%lf,%d,%d",
                    divs_precision, macro_precision, compatibility_precision,
                    convergence_test_method,maxiterations);
      /* if the user does not specify the test error method, 
         |div(sigma)|/|S| is chosen */
      /* if the user specifies just one precision, it is supposed to be the same for 
         divs_precision, macro_precision and compatibility_precision*/
      switch(status){
      case 1:
        *compatibility_precision=*divs_precision;
        *macro_precision=*divs_precision;
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
        break;
      case 2:
      /* if the user specifies just two values precision, it is supposed to be the same for 
         divs_precision, and compatibility_precision*/
        *compatibility_precision=*macro_precision;
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
        break;
      case 3:
        *convergence_test_method=CRAFT_TEST_DIVS_SUM;
        *maxiterations=INT_MAX;
	break;
      case 4:
        *maxiterations=INT_MAX;
        break;
      case 5:
	break;
      default:
        fprintf(stderr,"CraFT error in reading precision required \n");
        exit(1);
      }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* iterative scheme used                                                       */
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    fprintf(stdout,"Enter the iterative scheme to be used: ");
    craft_fgets(buff,1024,f);
    //    status=sscanf(buff,"%d",scheme);
    status=sscanf(buff,"%d %lf %lf",scheme,alpha,beta);

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  }

  /*-------------------------------------------------------------------------------*/
  /* if option -f has been invoked, one reads the describtion of the problem
     in a file
   */
  /*-------------------------------------------------------------------------------*/
  if (ask==2) {
    status = read_craft_input_file(ifn, cfn, tfn, pfn, mfn, lfn, tlfn, ofn,
				   C0_line_command, 
				   divs_precision, compatibility_precision, macro_precision, 
                                   convergence_test_method, maxiterations,
				   scheme,alpha,beta);
    if(status!=0) {
      fprintf(stderr,"CraFT error while reading input file %s \n",ifn);
      return -1;
    }
    free(ifn);
  }
  /*-------------------------------------------------------------------------------*/

  /* ultimate verification */
  int error=0;
  //  printf("ici %d\n",*scheme);
  if (*cfn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file describing characteristic function is missing\n");
    error++;
  }

  if (*pfn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file describing phases is missing\n");
    error++;
  }
  if (*mfn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file describing materials is missing\n");
    error++;
  }
  if (*lfn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file giving the loading conditions is missing\n");
    error++;
  }
  if (*ofn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file describing required outputs is missing\n");
    error++;
  }
  if (*C0_line_command==(char *)0) {
    /*
    fprintf(stderr,"CraFT error: line describing how to choose C0 is missing\n");
    error++;
    */
    *C0_line_command=malloc(strlen("auto")+1);
    strcat(*C0_line_command,"auto");
  }
  if (*mfn==(char *)0) {
    fprintf(stderr,"CraFT error: name of file describing materials is missing\n");
    error++;
  }
  if ( isnan(*divs_precision) || (*divs_precision< TINY ) ) {
    fprintf(stderr,"CraFT error: invalid choice for \"div(sigma) precision\" or precision not specified\n");
    error++;
  }
if ( isnan(*compatibility_precision) || (*compatibility_precision< TINY ) ) {
    fprintf(stderr,"CraFT error: invalid choice for \"compatibility precision \" or precision not specified\n");
    error++;
  }
if ( isnan(*macro_precision) || (*macro_precision< TINY ) ) {
    fprintf(stderr,"CraFT error: invalid choice for precision or precision not specified\n");
    error++;
  }

  switch(*convergence_test_method){
  case CRAFT_TEST_NOT_SET:
    *convergence_test_method=CRAFT_TEST_DIVS;
    break;
  case CRAFT_TEST_DIVS_SUM:
    break;
  case CRAFT_TEST_DIVS_MAX:
    break;
  case CRAFT_TEST_MONCHIET_BONNET:
    break;
  default:
    fprintf(stderr,"CraFT error: invalid error test method\n");
    error++;
    break;  
  }
  
  switch(*scheme){
  case CRAFT_SCHEME_NOT_SET:
    /* if scheme has not be set, one set it to CRAFT_BASIC_SCHEME: */
    *scheme = CRAFT_BASIC_SCHEME;
    break;
  case CRAFT_BASIC_SCHEME:
    break;
  case CRAFT_MILTON_EYRE_SCHEME:
    //    printf("sloopy1 \n");
    if ( isnan(*alpha) ) *alpha=2.;
    if ( isnan(*beta) ) *beta=*alpha;
    if ( (*alpha<0) || (*beta<0) ){
      fprintf(stderr,"Invalid alpha & beta parameters have been entered: %f %f.\n",*alpha,*beta);
      fprintf(stderr,"alpha and beta must be > 0 .\n\n");
      error++;
    }
    break;
  case CRAFT_LAGRANGIAN_SCHEME:
    if ( isnan(*alpha) ) *alpha=1.;
    if ( isnan(*beta) ) *beta=*alpha;
    if ( (fabs(*alpha-1.)>TINY) || (fabs(*beta-1.)>TINY) ){
      fprintf(stderr,"Invalid alpha & beta parameters have been entered: %f %f.\n",*alpha,*beta);
      fprintf(stderr,"alpha and beta must be equal to 1 with augmented Lagrangian scheme.\n\n");
      error++;
    }
    break;
  case CRAFT_ALPHA_BETA_SCHEME:
    //    printf("sloopy1 \n");
    printf("alpha=%f\n",*alpha);
    if ( isnan(*alpha) ) *alpha=1.5;
    if ( isnan(*beta) ) *beta=*alpha;
    if ( (*alpha<0) || (*beta<0) ){
      fprintf(stderr,"Invalid alpha & beta parameters have been entered: %f %f.\n",*alpha,*beta);
      fprintf(stderr,"alpha and beta must be > 0 .\n\n");
      error++;
    }
    break;
  default:
    fprintf(stderr,"CraFT error: invalid iterative scheme\n");
    error++;
    break;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* At this time, entering a thermal strain field has only been implemented for the basic scheme  */
  if( (*tfn != (char *)0) && (*scheme != CRAFT_BASIC_SCHEME) ){
    error++;
    fprintf(stderr,"CraFT error: thermal strain can be entered only when using the basic scheme.\n\n");
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* test if the image entered exists and is a 2d order tensor image */
  if( *tfn != (char *)0){
    CraftImage *ima;
    ima = (CraftImage *)malloc(sizeof(CraftImage));
    
    ima->type[0] = CRAFT_IMAGE_UNKNOWN;
    ima->type[1] = 0;
    ima->type[2] = 0;
    craft_image_init(ima, ima->type);
    
    status = read_image(*tfn, ima);
    if(status==-2){
      error++;
      fprintf(stderr,"CraFT error: file of thermal strain: %s does not exist.\n\n",*tfn);
    }
    else{
      if( ima->type[0] != CRAFT_IMAGE_TENSOR2){
        error++;
        fprintf(stderr,"CraFT error: the image of thermal strain MUST be a VTK file of 2d order tensors\n\n");
      }
    }
    
    craft_image_delete(ima);
    

  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* in the case when no loading for temperature as been entered                                   */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (*tlfn==(char *)0) {
    char *filename;
    FILE *f2;
    filename=malloc(1+strlen(CRAFT_TMPNAME)+strlen("XXXXXX"));
    strcpy(filename,CRAFT_TMPNAME);
    strcat(filename,"XXXXXX");
    *tlfn = filename;
    int id;
    id = mkstemp(filename);
    f2 = fdopen(id,"w");
    
    fprintf(f2,"0.              20.\n");
    
    fclose(f2);
    close(id);
         
    
 
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  if(error) return -1;

  return 0;
}

/*************************************************************************************************/
void usage() {
#ifdef HARMONIC
  fprintf(stderr,"CraFT version ");
  fprintf(stderr,CRAFT_VERSION);
  fprintf(stderr," Harmonic\n");
#else
  fprintf(stderr,"CraFT version ");
  fprintf(stderr,CRAFT_VERSION);
  fprintf(stderr,"\n");
#endif
  fprintf(stdout,"CraFT is a program which computes mechanical behavior of heterogeneous materials\n");
  fprintf(stdout,"Usage:\n");
  fprintf(stdout,"   craft [options]\n");
  fprintf(stdout,"Options:\n");
  fprintf(stdout,"   -h                display help (this information)\n");
  fprintf(stdout,"   -V                displays CraFT version number\n");
  fprintf(stdout,"   -v                verbose mode\n");
  fprintf(stdout,"                     (default: non verbose)\n");
  fprintf(stdout,"   -i                interactive mode: inputs are asked to the user\n");
  fprintf(stdout,"                     (it is the default mode)\n");
  fprintf(stdout,"   [-f <file>]       read inputs in file <file> \n");
  fprintf(stdout,"   -c <file>         characteristic function is given in file <file>\n");
  fprintf(stdout,"   -p <file>         phases described in file <file>\n");
  fprintf(stdout,"   -m <file>         materials described is given in file <file>\n");
  fprintf(stdout,"   -l <file>         loading conditions described in file <file>\n");
  fprintf(stdout,"   -o <file>         outputs described in file <file>\n");
  fprintf(stdout,"   -r <file>         restore a previous result stored in <file>\n");
  fprintf(stdout,"   -C <line>         C0 specified in command line <line> \n");
  fprintf(stdout,"                     -C auto : C0 is computed by craft (default)\n");
  fprintf(stdout,"                     -C param <lambda> <mu> : Lame coefficients of C0 set to <lambda> and <mu>\n");
  fprintf(stdout,"   -e <prec1[,prec2[,prec3]>    \n");
  fprintf(stdout,"                     precision required:\n");
  fprintf(stdout,"                       * prec1: precision required for stress divergence\n");
  fprintf(stdout,"                       * prec2: precision required for loading conditions\n");
  fprintf(stdout,"                       * prec3: precision required for compatibility \n");
  fprintf(stdout,"                     If prec3 is omitted, it is set to be equal to prec2\n");
  fprintf(stdout,"                     If prec2 is omitted, it is set to be equal to prec1\n");
  fprintf(stdout,"   -s <scheme> [<alpha> [<beta>]] \n");
  fprintf(stdout,"                     iterative scheme to be used:\n");
  fprintf(stdout,"                       %d: basic scheme (Moulinec Suquet 1998) (default)\n", CRAFT_BASIC_SCHEME);
  fprintf(stdout,"                       %d: accelerated scheme (Eyre Milton 1999)\n", CRAFT_MILTON_EYRE_SCHEME);
  fprintf(stdout,"                       %d: augmented lagrangian scheme (Michel Moulinec Suquet 2000)\n", CRAFT_LAGRANGIAN_SCHEME);
  fprintf(stdout,"                       %d: Monchiet-Bonnet scheme (Monchiet Bonnet 2011)\n", CRAFT_ALPHA_BETA_SCHEME);
  fprintf(stdout,"                     alpha and beta are valid only for Monchiet-Bonnet scheme\n");
  fprintf(stdout,"                     if beta is omitted, it is set to be equal to alpha\n");
  fprintf(stdout,"                     if alpha is omitted, it is set to be equal to 1.5\n");
  fprintf(stdout,"   -n <threads>      number of threads used by OpenMP\n");
  fprintf(stdout,"   [-t <file>]       thermal strain image is entered in file <file> (optional)\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"options -c -p -m -l -o -C -e -t are to be used together and exclude usage of -i or -f\n");
  fprintf(stdout,"option -i exclude usage of -r\n");
  fprintf(stdout,"\n");
}

/*************************************************************************************************/
int read_craft_input_file(
                          char *ifn,
                          char **cfn, /* file name of the characteristic function */
                          char **tfn, /* file name of the "tau" image, i.e. the image of the pre-stress field (if any) */
                          char **pfn, /* name of the file describing the phases */
                          char **mfn, /* name of the file describing the materials */
                          char **lfn, /* name of the file describing the loading condtions */
                          char **tlfn, /* name of the file describing the temperature loading conditions */
                          char **ofn, /* name of the file describing the outputs required */
                          char **C0_line_command, /* command line saying how to choose C0 */
                          double *divs_precision, /* precision required for divergence of stress */
                          double *compatibility_precision, /* precision required for compatibility of the strain*/
                          double *macro_precision, /* precision required for macroscopic stress or
                                            direction of macroscopic stress (depending on
                                            required loading conditions)*/
                          int *convergence_test_method, /* method to be employed for convergence test*/
                          int *maxiterations, /* max number of iterations per solving of Lippmann-Schwinger equation */
			  int *scheme, /* scheme used */
                          double *alpha, double *beta /* alpha beta parameters of accelerated schemes */
                          ){

  /* there are two different modes to enter the specifications of the problem:
   - the "old one": one enters the specification one after each other in the same way
     as in the interactive mode (option -i)

     Remark:
     This is obsolescent.
     I refuse to try to implement the case of the thermal strain image in this manner of 
     entering the data!
     (HM 20th april 2015)

   - one enters the spec by using keywords followed by the specifications which
     can be a file name or the detailed spec (= content of the spec file)

  The two modes cannot be mixed.
  */

   char buff[1024];
   char *buff2;

   /* the mode the inputs are entered:
    0 : inputs are entered as in interactive mode
    1 : inputs are entered with keywords
   */
   int mode=-1;

   int ikw;
   int i;

   int ii;
   FILE *f;


   char c;
   int nbaccolades;
   FILE *f2;
   char *filename;

   int status;


   /*========================================================================================*/
   /* some initializations */
   date = craft_date(0);
   filename=(char *)0;
   ii=-1;

   /*========================================================================================*/
   f = fopen(ifn,"r");
   if(f==(FILE *)0) {
     fprintf(stderr,"problem while opening %s\n",ifn);
     return -1;
   }

   /*========================================================================================*/
   while ( craft_fgets(buff, 1024,f) != (char *)NULL ){

     /* the line is being cleaned */
     clean_string(buff);
     // suppress_string_in_string(buff," ");


     /* one searches if a keyword has been written: */
     ikw=-1;
     for(i=0;i<nkw;i++) {
       if( strncasecmp(buff,keyword[i],strlen(keyword[i])) == 0 ) {
         ikw=i;
         break;
       }
     }
     if (
         ( (ikw==-1) && (mode==1) )
         ||
         ( (ikw!=-1) && (mode==0) )
          ) {
       fprintf(stderr,"CraFT error: invalid input specifications\n");
       fprintf(stderr,"faulty command: \n     %s\n",buff);
       fprintf(stderr,"(keyword mode and non-keyword mode of input spec have been mixed)\n");

       return -1;
     }

     if ( (ikw==-1) && (mode==-1) ) {
       mode = 0;
     }
     if ( (ikw!=-1) && (mode==-1) ) {
       mode = 1;
     }
     /*---------------------------------------------------------------------------------*/
     /* input specification as in the interactive mode */
     if(mode==0) {
       ii++;

       switch(ii){
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* enters the name of the image of the characteristic function                 */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 0:
         *cfn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*cfn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* enters the name of the file describing the phases */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 1:
         *pfn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*pfn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* enters the name of the file describing the materials */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 2:
         *mfn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*mfn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* Enter the name of the file describing the loading conditions                */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 3:
         *lfn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*lfn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* reads file containg the list of required outputs                            */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 4:
         *ofn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*ofn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* reference material C0                                                       */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 5:
         *C0_line_command = (char *)malloc(strlen(buff)+1);
         //         strcpy(*C0_line_command , buff);
         sscanf(buff,"%s",*C0_line_command);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* Required precision                                                          */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 6:
         //         printf("buff=%s\n",buff);
         status=sscanf(buff,"%lf,%lf,%lf,%d,%d",
                       divs_precision, macro_precision, compatibility_precision,
                       convergence_test_method,maxiterations);
         /* if the user does not specify the test error method, 
            |div(sigma)|/|S| is chosen */
         /* if the user specifies just one precision, it is supposed to be the same for 
            divs_precision, macro_precision and compatibility_precision*/
         switch(status){
         case 1:
           *compatibility_precision=*divs_precision;
           *macro_precision=*divs_precision;
           *convergence_test_method=CRAFT_TEST_DIVS_SUM;
           *maxiterations=INT_MAX;
           break;
         case 2:
           /* if the user specifies just two values precision, it is supposed to be the same for 
              divs_precision, and compatibility_precision*/
           *compatibility_precision=*macro_precision;
           *convergence_test_method=CRAFT_TEST_DIVS_SUM;
           *maxiterations=INT_MAX;
           break;
         case 3:
           *convergence_test_method=CRAFT_TEST_DIVS_SUM;
           *maxiterations=INT_MAX;
           break;
         case 4:
           *maxiterations=INT_MAX;
           break;
         case 5:
           break;
         default:
           fprintf(stderr,"CraFT error in reading precision required \n");
           exit(1);
         }
         printf("precision on divergence of the stress=%lf\n",*divs_precision);
         printf("precision on compatibility of the strain=%lf\n",*compatibility_precision);
         printf("precision on macroscopic stress or direction of the macroscopic stress =%lf\n",*macro_precision);
         printf("convergence test method=%d\n",*convergence_test_method);
         printf("max number of iterations per solving of LS equation=%d\n",*maxiterations);
         break;
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	 /* iterative scheme to be used                                                 */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 7:
         //	 status=sscanf(buff,"%d",scheme);
         status=sscanf(buff,"%d %lf %lf",scheme,alpha,beta);
	 break;
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         /* Enter the name of the file describing the temperature loading conditions    */
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       case 9:
         *tlfn = (char *)malloc(strlen(buff)+1);
         sscanf(buff,"%s",*tlfn);
         break;

         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       default:
         /* impossible case */
         return -1;
         break;
         /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
       }
     }
     /*----------------------------------------------------------------------------*/
     /* input specification by keywords */
     if(mode==1) {

       buff2 = buff+strlen(keyword[ikw]);
       clean_string(buff2);

       /*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */
       /* one has entered a file name */
       if ( buff2[0] == '='){

         buff2++;
         clean_string(buff2);

         switch(ikw){
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the image of the characteristic function                 */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 0:
           *cfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*cfn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the file describing the phases */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 1:
           *pfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*pfn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the file describing the materials */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 2:
           *mfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*mfn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Enter the name of the file describing the loading conditions                */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 3:
           *lfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*lfn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* reads file containg the list of required outputs                            */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 4:
           *ofn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*ofn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* reference material C0                                                       */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 5:
           *C0_line_command = (char *)malloc(strlen(buff2)+1);
           strcpy(*C0_line_command , buff2);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Required precision                                                          */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 6:
           //           sscanf(buff2,"%lf",precision);
         
           status=sscanf(buff2,"%lf,%lf,%lf,%d,%d",
                         divs_precision, macro_precision, compatibility_precision,
                         convergence_test_method,maxiterations);
           /* if the user does not specify the test error method, 
              |div(sigma)|/|S| is chosen */
           /* if the user specifies just one precision, it is supposed to be the same for 
              divs_precision, macro_precision and compatibility_precision*/
           switch(status){
           case 1:
             *compatibility_precision=*divs_precision;
             *macro_precision=*divs_precision;
             *convergence_test_method=CRAFT_TEST_DIVS_SUM;
             *maxiterations=INT_MAX;
             break;
           case 2:
             /* if the user specifies just two values precision, it is supposed to be the same for 
                divs_precision, and compatibility_precision*/
             *compatibility_precision=*macro_precision;
             *convergence_test_method=CRAFT_TEST_DIVS_SUM;
             *maxiterations=INT_MAX;
             break;
           case 3:
             *convergence_test_method=CRAFT_TEST_DIVS_SUM;
             *maxiterations=INT_MAX;
             break;
           case 4:
             *maxiterations=INT_MAX;
             break;
           case 5:
             break;
           default:
             fprintf(stderr,"CraFT error in reading precision required \n");
             exit(1);
           }
           
           
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	   /* iterative scheme to be used                                                 */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	 case 7:
           //	   status=sscanf(buff2,"%d",scheme);
           status=sscanf(buff2,"%d %lf %lf",scheme,alpha,beta);
	   break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the image of the thermal strain                          */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 8:
           *tfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*tfn);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Enter the name of the file describing the temperature loading conditions                */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 9:
           *tlfn = (char *)malloc(strlen(buff2)+1);
           sscanf(buff2,"%s",*tlfn);
           break;


           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         default:
           /* impossible case */
           return -1;
           break;
         }


       }

       /*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */
       /* one has entered the full spec corresponding to the keyword */
       else if (buff2[0]=='{') {
	 filename=malloc(1+strlen(CRAFT_TMPNAME)+strlen("XXXXXX"));
	 strcpy(filename,CRAFT_TMPNAME);
	 strcat(filename,"XXXXXX");

         /*
	 char *p;
	 p = (char *)mktemp(filename);
	 if (p == (char *)0) return -1;
         
         nbaccolades=1;
         f2 = fopen(filename,"w");

         */
         int id;
         id = mkstemp(filename);
         nbaccolades=1;
         f2 = fdopen(id,"w");
         

         fread(&c,1,1,f);
         if (c=='{') nbaccolades++;
         if (c=='}') nbaccolades--;

         while(nbaccolades > 0 ){
           fwrite(&c,1,1,f2);
           fread(&c,1,1,f);
           if (c=='{') nbaccolades++;
           if (c=='}') nbaccolades--;
         }

         fclose(f2);
         close(id);
         
         switch(ikw){
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the image of the characteristic function                 */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 0:
           *cfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*cfn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the file describing the phases */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 1:
           *pfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*pfn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the file describing the materials */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 2:
           *mfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*mfn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Enter the name of the file describing the loading conditions                */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 3:
           *lfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*lfn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* reads file containg the list of required outputs                            */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 4:
           *ofn = (char *)malloc( strlen(filename)+1 );
           strcpy(*ofn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* reference material C0                                                       */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 5:
           fprintf(stderr,"CraFT: invalid specifications %s\n",buff);
           return -1;
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Required precision                                                          */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 6:
           fprintf(stderr,"CraFT: invalid specifications %s\n",buff);
           return -1;

           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	   /* iterative scheme to be used                                                 */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	 case 7:
           //	   status=sscanf(buff2,"%d",scheme);
           fprintf(stderr,"CraFT: invalid specifications %s\n",buff);
	   break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* enters the name of the image of the thermal strain                          */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 8:
           *tfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*tfn,filename);
           break;

           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
           /* Enter the name of the file describing the temperature loading conditions    */
           /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
         case 9:
           *tlfn = (char *)malloc( strlen(filename)+1 );
           strcpy(*tlfn,filename);
           break;



         default:
           /* impossible case */
           fprintf(stderr,"CraFT: invalid specifications %s\n",buff);
           return -1;
           break;
         }

         free(filename);
       }


     }

   }

   fclose(f);

   return 0;

}
/*************************************************************************************************/
/* HM 01/11/2011
   tests if a given file is a temporary option file or not

   return value:
   1 : the file is a temporary option file
   0 : the file is not a temporary option file
   -1: the file does not exist
   -2: error

*/
/*************************************************************************************************/
int test_temporary_option_file(const char *filename){
  int i;
  int status;

  status = access(filename,F_OK);
  if( status != 0) return -1;

  
  status = strncmp(filename,CRAFT_TMPNAME,strlen(CRAFT_TMPNAME));
  if (status == 0 ) return 1;

  
  return 0;

}


