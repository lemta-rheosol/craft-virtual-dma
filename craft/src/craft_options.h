#ifndef __CRAFT_OPTIONS__
#define __CRAFT_OPTIONS__

#define CRAFT_TMPNAME "/tmp/craft_temporary"

int craft_options(
    int argc,
    char *const argv[],
    char **cfn, /* file name of the characteristic function */
    char **tfn, /* file name of the "tau" image, i.e. the image of the pre-stress field (if any) */
    char **pfn, /* name of the file describing the phases */
    char **mfn, /* name of the file describing the materials */
    char **lfn, /* name of the file describing the loading condtions */ 
    char **tlfn, /* name of the file describing the temperature loading conditions */
    char **ofn, /* name of the file describing the outputs required */
    char **rfn, /* name of the file containing a saved state */
    char **C0_line_command, /* command line saying how to choose C0 */
    double *divs_precision, /* precision required for divergence of stress */
    double *compatibility_precision, /* precision required for compatibility of the strain field */
    double *macro_precision, /* precision required for macroscopic stress or
    	    direction of macroscopic stress (depending on
    	    erquired boundary conditions)*/
    int *convergence_test_method, /* method to be employed for convergence test*/
    int *maxiterations, /* maximum authorized number of iterations for solving 
                              Lippmann-Schwinger equation */
    int *scheme, /* scheme used */
    double *alpha, double *beta, /* alpha & beta parameters of Monchiet & Bonnet accelerated scheme */
    int *nr_threads, /* number of OMP threads to be used */
    int *verbose /* verbosity */
    ) ;

int read_craft_input_file(
                          char *ifn,
                          char **cfn, /* file name of the characteristic function */
                          char **tfn, /* file name of the "tau" image, i.e. the image of the pre-stress field (if any) */
                          char **pfn, /* name of the file describing the phases */
                          char **mfn, /* name of the file describing the materials */
                          char **lfn, /* name of the file describing the loading condtions */
                          char **tlfn, /* name of the file describing the temperature loading conditions */
                          char **ofn, /* name of the file describing the outputs required */
                          char **C0_line_command, /* command line saying how to choose C0 */
                          double *divs_precision, /* precision required for divergence of stress */
                          double *compatibility_precision, /* precision required for compatibility of the strain*/
                          double *macro_precision, /* precision required for macroscopic stress or
                                            direction of macroscopic stress (depending on
                                            required loading conditions)*/
                          int *convergence_test_method, /* method to be employed for convergence test*/
                          int *maxiterations, /* max number of iterations per solving of LS equation */
			  int *scheme, /* scheme used */
                          double *alpha, double *beta /* alpha beta parameters of accelerated schemes */
                          );

int test_temporary_option_file(const char *filename);
void usage();
#endif
