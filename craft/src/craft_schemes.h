/*------------------------------------------------------------------------------------------------------------------------------------------*/
#ifndef __CRAFT_SCHEMES__
#define __CRAFT_SCHEMES__

#include <loading.h>
#include <euler.h>
#include <behavior/materials.h>

/* the different iterative schemes */
#define CRAFT_SCHEME_NOT_SET -1
#define CRAFT_BASIC_SCHEME 0
#define CRAFT_MILTON_EYRE_SCHEME 1
#define CRAFT_LAGRANGIAN_SCHEME 2
#define CRAFT_ALPHA_BETA_SCHEME 3

/*------------------------------------------------------------------------------------------------------------------------------------------*/
/* Basic scheme */
/*------------------------------------------------------------------------------------------------------------------------------------------*/
int basic_scheme(
                 int Nph,        /* total number of phases in the microstructure */
		 int *nppp,      /* number of points per phase */
		 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
				    the Lippmann-Schwinger algorithm) of the point number ipt
				    in the phase number iph                                     */
		 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
					       orientation (or the orientation of its anistropy)
					       of phase number iph.
					       If iph is an isotropic material, orientation is useless     */
		 
		 Material *material,
		 /* material[iph] gives every information about the material
		    in phase iph */
		 
		 Variables *variables,
		 /* variables[iph] : pointer to the structure describing
		    the variables in phase iph                            */
		 
		 int load_type, /* loading conditions                                          */
		 Load_Step load_step,   /* load step */
		 double E[6], double S[6], /* macroscopic strain and stress */
		 
		 LE_param *C0,    /* "reference material"                                        */

		 double dt,  /* time step between current step and previous step */

		 double (**phase_epsilon)[6],
		 /* phase_epsilon[iph][ipt] gives the strain tensor of point
		    number ipt of phase number iph                              */
		 
		 
		 double (**phase_previous_epsilon)[6],
		 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
		    of point number ipt of phase number iph at previous loading
		    step.                                                        */
		 
		 double (**phase_sigma)[6],
		 /* phase_sigma[iph][ipt] gives the stress tensor of point
		    number ipt of phase number iph                              */

		 double (**phase_epsilon_thermal)[6],
		 /* phase_epsilon_thermal[iph][ipt] gives the thermal strain tensor of point
		    number ipt of phase number iph, if any.
                    In the case when no thermal strain has been applied,
                    phase_epsilon_thermal[iph] is equal to null for any iph
                 */
		 
		 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                        relation                                                 */
		 
		 double *compatibility_error,  /* error on comptatibility of the strain */
		 double *divs_error,  /* error on divergence of the stress                       */
		 double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                         stress (depending on loading conditions)                */
		 double compatibility_precision, /* precision required on strain compatibility */
		 double divs_precision,   /* precision required by user on divergence of the
					     stress                                                      */
		 double macro_precision,  /* precision required by user on macroscopic stress
					     or direction of macroscopic
					     stress (depending on loading conditions)                    */
                 int maxiter, /* max number of iterations allowed */

		 
		 int *niter

		 );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int basic_scheme_harmonic(
                 int Nph,        /* total number of phases in the microstructure */
		 int *nppp,      /* number of points per phase */
		 int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
				    the Lippmann-Schwinger algorithm) of the point number ipt
				    in the phase number iph                                     */
		 Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
					       orientation (or the orientation of its anistropy)
					       of phase number iph.
					       If iph is an isotropic material, orientation is useless     */
		 
		 Material *material,
		 /* material[iph] gives every information about the material
		    in phase iph */
		 
		 Variables *variables,
		 /* variables[iph] : pointer to the structure describing
		    the variables in phase iph                            */
		 
		 int load_type, /* loading conditions                                          */
		 Load_Step load_step,   /* load step */
		 double complex E[6], double complex S[6], /* macroscopic strain and stress */
		 
		 LE_param *C0,    /* "reference material"                                        */

		 double dt,  /* time step between current step and previous step */

		 double complex (**phase_epsilon)[6],
		 /* phase_epsilon[iph][ipt] gives the strain tensor of point
		    number ipt of phase number iph                              */
		 
		 
		 double complex (**phase_previous_epsilon)[6],
		 /* phase_previous_epsilon[iph][ipt] gives the strain tensor
		    of point number ipt of phase number iph at previous loading
		    step.                                                        */
		 
		 double complex (**phase_sigma)[6],
		 /* phase_sigma[iph][ipt] gives the stress tensor of point
		    number ipt of phase number iph                              */

		 double (**phase_epsilon_thermal)[6],
		 /* phase_epsilon_thermal[iph][ipt] gives the thermal strain tensor of point
		    number ipt of phase number iph, if any.
                    In the case when no thermal strain has been applied,
                    phase_epsilon_thermal[iph] is equal to null for any iph
                 */
		 
		 CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                        relation                                                 */
		 
		 double *compatibility_error,  /* error on comptatibility of the strain */
		 double *divs_error,  /* error on divergence of the stress                       */
		 double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                         stress (depending on loading conditions)                */
		 double compatibility_precision, /* precision required on strain compatibility */
		 double divs_precision,   /* precision required by user on divergence of the
					     stress                                                      */
		 double macro_precision,  /* precision required by user on macroscopic stress
					     or direction of macroscopic
					     stress (depending on loading conditions)                    */
                 int maxiter, /* max number of iterations allowed */

		 
		 int *niter

		 );

/*------------------------------------------------------------------------------------------------------------------------------------------*/
/* Monchiet & Bonnet's scheme */
/*------------------------------------------------------------------------------------------------------------------------------------------*/
int alpha_beta_scheme(
                      int Nph,        /* total number of phases in the microstructure */
                      int *nppp,      /* number of points per phase */
                      int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
                                         the Lippmann-Schwinger algorithm) of the point number ipt
                                         in the phase number iph                                     */
                      Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
                                                    orientation (or the orientation of its anistropy)
                                                    of phase number iph.
                                                    If iph is an isotropic material, orientation is useless     */
                 
                      Material *material,
                      /* material[iph] gives every information about the material
                         in phase iph */
                 
                      Variables *variables,
                      /* variables[iph] : pointer to the structure describing
                         the variables in phase iph                            */
                 
                      int load_type, /* loading conditions                                          */
                      Load_Step load_step,   /* load step */
                      double E[6], double S[6], /* macroscopic strain and stress */
                 
                      LE_param *C0,    /* "reference material"                                        */
                 
                      double (**phase_epsilon)[6],
                      /* phase_epsilon[iph][ipt] gives the strain tensor of point
                         number ipt of phase number iph                              */
                 
                 
                      double dt,  /* time step between current step and previous step */
                      double (**phase_previous_epsilon)[6],
                      /* phase_previous_epsilon[iph][ipt] gives the strain tensor
                         of point number ipt of phase number iph at previous loading
                         step.                                                        */
                 
                      double (**phase_sigma)[6],
                      /* phase_sigma[iph][ipt] gives the stress tensor of point
                         number ipt of phase number iph                              */

                      CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                             relation                                                 */
                 
                      double *compatibility_error,  /* error on comptatibility of the strain */
                      double *divs_error,  /* error on divergence of the stress                       */
                      double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                              stress (depending on loading conditions)                */
                      double compatibility_precision, /* precision required on strain compatibility */
                      double divs_precision,   /* precision required by user on divergence of the
                                                  stress                                                      */
                      double macro_precision,  /* precision required by user on macroscopic stress
                                                  or direction of macroscopic
                                                  stress (depending on loading conditions)                    */
                 
                      int maxiter, /* max number of iterations allowed */                 
                      int *niter,

                      double *alpha,
                      double *beta
                 
                      );

/* added for harmonic implementation 2018/07/09  J. Boisse */
int alpha_beta_scheme_harmonic(
                      int Nph,        /* total number of phases in the microstructure */
                      int *nppp,      /* number of points per phase */
                      int **index,    /* index[iph][ipt] gives the index in the tensor image (used in
                                         the Lippmann-Schwinger algorithm) of the point number ipt
                                         in the phase number iph                                     */
                      Euler_Angles *orientation, /* orientation[iph] gives the crystallographic
                                                    orientation (or the orientation of its anistropy)
                                                    of phase number iph.
                                                    If iph is an isotropic material, orientation is useless     */
                 
                      Material *material,
                      /* material[iph] gives every information about the material
                         in phase iph */
                 
                      Variables *variables,
                      /* variables[iph] : pointer to the structure describing
                         the variables in phase iph                            */
                 
                      int load_type, /* loading conditions                                          */
                      Load_Step load_step,   /* load step */
                      double complex E[6], double complex S[6], /* macroscopic strain and stress */
                 
                      LE_param *C0,    /* "reference material"                                        */
                 
                      double complex (**phase_epsilon)[6],
                      /* phase_epsilon[iph][ipt] gives the strain tensor of point
                         number ipt of phase number iph                              */
                 
                 
                      double dt,  /* time step between current step and previous step */
                      double complex (**phase_previous_epsilon)[6],
                      /* phase_previous_epsilon[iph][ipt] gives the strain tensor
                         of point number ipt of phase number iph at previous loading
                         step.                                                        */
                 
                      double complex (**phase_sigma)[6],
                      /* phase_sigma[iph][ipt] gives the stress tensor of point
                         number ipt of phase number iph                              */

                      CraftImage *image,  /* image of a 2d order tensor, used in the iterative
                                             relation                                                 */
                 
                      double *compatibility_error,  /* error on comptatibility of the strain */
                      double *divs_error,  /* error on divergence of the stress                       */
                      double *macro_error, /* error on macroscopic stress or direction of macroscopic
                                              stress (depending on loading conditions)                */
                      double compatibility_precision, /* precision required on strain compatibility */
                      double divs_precision,   /* precision required by user on divergence of the
                                                  stress                                                      */
                      double macro_precision,  /* precision required by user on macroscopic stress
                                                  or direction of macroscopic
                                                  stress (depending on loading conditions)                    */
                 
                      int maxiter, /* max number of iterations allowed */                 
                      int *niter,

                      double *alpha,
                      double *beta
                 
                      );
#endif
