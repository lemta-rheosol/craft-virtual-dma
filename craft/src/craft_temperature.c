#include <stdio.h>
#include <stdlib.h>
#include <utils.h>
#include <craft_temperature.h>

/* Temperature is a function giving the uniform temperature prescibed by the loading at time t.
   The data concerning the temperature loading are stored in tl stucture.
   In tl, the successive time values are supposed to be stored in ascending order.
   
   Return value:
   The input time t is compared to the time values of table tl. 3 cases are possible:
   * case 1:
   If t is lower than the lowest value of the temperatures in tl (i.e. tl->tls[0].T), the return value 
   is set to this value.   
   * case 2:
   If t is bigger than the highest value of the temperatures in tl (i.e. tl->tls[tl->Nt-1].T), the return value 
   is set to this value.
   * case 3:
   In the other cases, the return value is a linear interpolation of the two successive values between which 
   t takes place:
   tl->tls[i-1] <= t <  tl->tls[i]


*/
double Temperature( const double t, const Temperature_Loading *tl){
  int i;
  for( i=0; i<tl->Nt; i++){
    if(t < tl->tls[i].t) break;
  }

  if(i==0){
    return tl->tls[0].T;
  }
  else if (i==tl->Nt){
    return tl->tls[tl->Nt-1].T;
  }
  else{
    return tl->tls[i-1].T +
      (t-tl->tls[i-1].t) *
      ( tl->tls[i].T - tl->tls[i-1].T)/( tl->tls[i].t - tl->tls[i-1].t);
  }
  
}
/*************************************************************************************************/
/* function reading a file containing successive couple of time temperatures values
   These values can be entered in any order, they are reorganized in ascending order of time.
 */
static int compare_time (void const *a, void const *b){
  Temperature_Loading_Step *pa = (Temperature_Loading_Step *)a;
  Temperature_Loading_Step *pb = (Temperature_Loading_Step *)b;
  return pa->t -  pb->t;
}
  
int read_temperature_file(char *filename, Temperature_Loading *tl){

  FILE *f;
  f = fopen(filename,"r");
  if(f==(FILE *)0){
    fprintf(stderr,"Problem occurred in function read_temperature_file\n");
    fprintf(stderr,"when trying to open %s file.\n",filename);
    return -1;
  }
    
  tl->Nt=0;
  tl->tls=(Temperature_Loading_Step *)0;
  double t,T;
  int status;
  while( (status=craft_fscanf(f,"%lf %lf",&t,&T)) != EOF) {
    if(status!=2){
      fprintf(stderr,"Problem occurred in function read_temperature_file\n");
      fprintf(stderr,"invalid data in file %s\n",filename);
      fclose(f);
      return -2;
    }
      
    tl->Nt++;
    tl->tls = realloc( tl->tls, tl->Nt * sizeof(*(tl->tls)) );
    tl->tls[tl->Nt-1].t = t;
    tl->tls[tl->Nt-1].T = T;
  }

  qsort( tl->tls, tl->Nt , sizeof(*tl->tls), compare_time);
                     
}
