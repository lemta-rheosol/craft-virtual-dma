#ifndef _CRAFT_TEMPERATURE_
#define _CRAFT_TEMPERATURE_

/* structure defining all what is necessary for dealing with loading of temperature */
typedef struct _temperature_loading_      Temperature_Loading;
typedef struct _temperature_loading_step_ Temperature_Loading_Step;

struct _temperature_loading_step_{
  double t; // time
  double T; // temperature
};

struct _temperature_loading_{
  int Nt; // number of step
  Temperature_Loading_Step *tls; // the loading steps as they are entered by the user
};

double Temperature( const double t, const Temperature_Loading *tl);
int read_temperature_file(char *filename, Temperature_Loading *tl);
#endif
