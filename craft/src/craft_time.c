#define _DEFAULT_SOURCE
#define __USE_BSD

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

/********************************************************************************************/
/* 

   return the time in seconds since the Epoch

 */
/********************************************************************************************/
double craft_clocktime()  {

  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  
  return(  (double)(ts.tv_sec) + (double)(ts.tv_nsec)/1000000000. );
}
/********************************************************************************************/
/* returns current date in a string formatted as 
   YYYY_MM_DD_HH:MN:SS
*/
/********************************************************************************************/
char *craft_date(int flag){
  
  struct tm *t;
  char *s;

  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  t=localtime(&(ts.tv_sec));

  if (flag==0) {
    s = (char *)malloc(4+2+2+2+2+2+6);
    sprintf(s,"%4.4d_%2.2d_%2.2d_%2.2d:%2.2d.%2.2d",
	    t->tm_year+1900,
	    t->tm_mon+1,
	    t->tm_mday,
	    t->tm_hour,
	    t->tm_min,
	    t->tm_sec);
  }
  else {
    s = (char *)malloc(4+2+2+2+2+2+6);
    sprintf(s,"%4.4d %2.2d %2.2d %2.2d:%2.2d.%2.2d",
	    t->tm_year+1900,
	    t->tm_mon+1,
	    t->tm_mday,
	    t->tm_hour,
	    t->tm_min,
	    t->tm_sec);
  }
  return s;
}
  

