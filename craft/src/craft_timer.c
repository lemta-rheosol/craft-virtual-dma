#include <stdio.h>
#include <craft_timer.h>
#include <craft_time.h>


/**********************************************************************/
int init_timers(void){
  int i;


  craft_timer_scheme=0;
  for (i=0; i<NB_CRAFT_TIMERS; i++) {
    ct[i].time=0.;
  }

  return 0;
}
/**********************************************************************/
int start_timer(int i){
  
  /* this flag helps to determine which is the scheme 
     used: basic or accelerated */
  if (
      ( i == CRAFT_TIMER_EQUILIBRIUM ) ||
      ( i == CRAFT_TIMER_COMPATIBILITY ) ||
      ( i == CRAFT_TIMER_SOLVE ) ||
      ( i == CRAFT_TIMER_ALPHA_BETA_MISC ) 
      ) {
    craft_timer_scheme=1;
  }

  ct[i].t0=craft_clocktime();

  return 0;
}
/**********************************************************************/
int stop_timer(int i){
  
  ct[i].time += craft_clocktime()-ct[i].t0;
  ct[i].t0=0.;

  return 0;
}
/**********************************************************************/
int print_timers(FILE *f){
  int i;
  
  double stimer=0.;
  double total_time;

  total_time=ct[CRAFT_TIMER_TOTAL].time;

  for(i=1;i<NB_CRAFT_TIMERS;i++) stimer += ct[i].time;

  fprintf(f,"-----------------------------------------------------------------------------------\n");
  fprintf(f,"                                             time (s)     %%age                    \n");
  fprintf(f,"-----------------------------------------------------------------------------------\n");

  if(craft_timer_scheme==0){
    fprintf(f,"Basic scheme\n");
  }
  else {
    fprintf(f,"Accelerated scheme\n");
  }

  i=CRAFT_TIMER_READ_DATA;
  fprintf(f," time spent in reading input data        : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_INIT;
  fprintf(f," time spent in initalizing variables     : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_BEHAVIOR;
  fprintf(f," time spent in behavior                  : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_FFTD;
  fprintf(f," time spent in FFT                       : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_FFTI;
  fprintf(f," time spent in FFT-1                     : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_GAMMA0;
  fprintf(f," time spent in applying Gamma0           : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_UPDATE;
  fprintf(f," time spent in update epsilon            : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_EXTRAPOL;
  fprintf(f," time spent in extrapolate state variable: %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_PHASE_TO_IMAGE;
  fprintf(f," time spent in phase to image            : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_IMAGE_TO_PHASE;
  fprintf(f," time spent in image to phase            : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  i=CRAFT_TIMER_POST;
  fprintf(f," time spent in saving output results     : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);

  if(craft_timer_scheme==0) {
    i=CRAFT_TIMER_BASIC_MISC;
    fprintf(f," time spent in miscellaneous operations  : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  }
  else{
    i=CRAFT_TIMER_EQUILIBRIUM;
    fprintf(f," time spent in equilibrium test          : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
    i=CRAFT_TIMER_COMPATIBILITY;
    fprintf(f," time spent in compatibility test        : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
    i=CRAFT_TIMER_SOLVE;
    fprintf(f," time spent in solving s+c0:e = tau      : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
    i=CRAFT_TIMER_ALPHA_BETA_MISC;
    fprintf(f," time spent in miscellaneous operations  : %8.3f     %5.2f\n",ct[i].time,ct[i].time/total_time*100);
  }

  fprintf(f," other                                   : %8.3f     %5.2f\n",total_time-stimer,(total_time-stimer)/total_time*100);
  

  fprintf(f,"                                            --------------------\n");
  fprintf(f," total time                              : %8.3f\n",total_time);

  return 0;
  
}
/**********************************************************************/
