#ifndef CRAFT_TIMER_H
#define CRAFT_TIMER_H

/*============================================*/
/* general timers */
#define CRAFT_TIMER_TOTAL              0
#define CRAFT_TIMER_FFTD               1
#define CRAFT_TIMER_FFTI               2
#define CRAFT_TIMER_GAMMA0             3
#define CRAFT_TIMER_BEHAVIOR           4
#define CRAFT_TIMER_EXTRAPOL           5
#define CRAFT_TIMER_UPDATE             6
#define CRAFT_TIMER_PHASE_TO_IMAGE     7
#define CRAFT_TIMER_IMAGE_TO_PHASE     8
#define CRAFT_TIMER_READ_DATA          9
#define CRAFT_TIMER_INIT              10
#define CRAFT_TIMER_POST              11

/* miscellaneous calculations */
#define CRAFT_TIMER_MISC              12

/*--------------------------------------------*/
/* miscellaneous calculations for basic scheme */
#define CRAFT_TIMER_BASIC_MISC        13
/*--------------------------------------------*/
/* timers specific to alpha beta scheme */
#define CRAFT_TIMER_EQUILIBRIUM       14
#define CRAFT_TIMER_COMPATIBILITY     15
#define CRAFT_TIMER_SOLVE             16
/* miscellaneous calculations for alpha beta scheme */
#define CRAFT_TIMER_ALPHA_BETA_MISC   17

/*--------------------------------------------*/
#define NB_CRAFT_TIMERS               19

/*============================================*/
typedef struct{
  double t0;
  double time;
  char *text;
} CraftTimer;

CraftTimer ct[NB_CRAFT_TIMERS];

int craft_timer_scheme;
/*============================================*/
int init_timers();
int start_timer(int t);
int stop_timer(int t);

int print_timers(FILE *f);
/*============================================*/
#endif
