#define _ISOC99_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>

#include <craftimage.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef TINY
#define TINY 1.e-15
#endif
/*----------------------------------------------------------------------------*/
/* Inialization (with null values) of all fields in CraftImage structure      */
void craft_image_init( CraftImage *image, int *type ){


  image->name = (char *)0;

  image->n[0]=0;
  image->n[1]=0;
  image->n[2]=0;

  image->nd[0]=0;
  image->nd[1]=0;
  image->nd[2]=0;

  image->s[0] = 0.;
  image->s[1] = 0.;
  image->s[2] = 0.;

  image->p[0] = 0.;
  image->p[1] = 0.;
  image->p[2] = 0.;

  image->pxl.v=(void *)NULL;


  if (*type != 0) {
    image->type[0] = type[0];
    image->type[2] = type[2];
  }
  else{
    image->type[0]=CRAFT_IMAGE_UNKNOWN;
    image->type[2]=0;
  }


  switch (image->type[0]) {

  case CRAFT_IMAGE_UNKNOWN:
    image->type[1]=0;
    break;

  case CRAFT_IMAGE_CHAR:
  case CRAFT_IMAGE_UCHAR:
  case CRAFT_IMAGE_INT:
  case CRAFT_IMAGE_UINT:
  case CRAFT_IMAGE_FLOAT:
  case CRAFT_IMAGE_DOUBLE:
  case CRAFT_IMAGE_FOURIER_DOUBLE:
  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->type[1]=1;
    break;

  case CRAFT_IMAGE_TENSOR2:
  case CRAFT_IMAGE_FOURIER_TENSOR2:
  case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->type[1]=6;
    break;

  case CRAFT_IMAGE_VECTOR3D:
  case CRAFT_IMAGE_FOURIER_VECTOR3D:
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->type[1]=3;
    break;

  case CRAFT_IMAGE_VECTOR:
  case CRAFT_IMAGE_FOURIER_VECTOR:
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->type[1]=type[1];
    break;

  default:
    fprintf(stderr,"craft_image_init: invalide case.\n");
    break;
  }

}

/*----------------------------------------------------------------------------*/
/* CraftImage constructor.

   It returns a pointer to a newly created CraftImage.

   inputs:
   * CraftImage model: 
     the image to be built will have the same fields as model:
     except:
     - type which is entered as 2d argument 
     - pxl which is allocated
     - name which is set to null

     * int *type
     gives the type of the image to be built.
     When type is a null pointer, the type field of model image is used instead
   
 */
CraftImage *craft_image_new( CraftImage model, int *type ) {

  CraftImage *result;
  int status;

  result = malloc(sizeof(CraftImage ));

  /* every field of model is copied except:
     pxl
     type
   */
  *result = model;

  (*result).pxl.v=(void *)0;
  (*result).name=(char *)0;
 
  if( type != (int *)0 ) {
    (*result).type[0] = type[0];
    (*result).type[1] = type[1];
    (*result).type[2] = type[2];
  }

  
  status = craft_image_alloc( result );
  
  if (status<0) {
    fprintf(stderr,"craft_image_new error: impossible to alloc image\n");
    free(result);
    return (CraftImage *)0;
  }

  return result;
    
}

/*----------------------------------------------------------------------------*/
/* CraftImage destructor                                                      */
void craft_image_delete(CraftImage *image){
  /* deallocation of "name"  string : */
  free(image->name);
  /* deallocation of pxl */
  craft_image_free(image);
  /* free image pointer */
  free(image);
}
/*----------------------------------------------------------------------------*/
/* Re-inialization of a CraftImage structure                                  */
void craft_image_reset( CraftImage *image, int type[3]){

  /* deallocation of pixel data */
  if ( (image->type[0]) != CRAFT_IMAGE_UNKNOWN ) free(image->pxl.v);
  free(image->name);

  /* re-initialization of all fields */
  craft_image_init( image , type);
}

/*----------------------------------------------------------------------------*/
/* Allocation of pxl in an image                                              */
/* return value:
   0 :  allocation successfull
   -1:  failure
   1 :  type image required is CRAFT_IMAGE_UNKNOWN no allocation made
 */
int craft_image_alloc( CraftImage *image ){
  int nt,status;

  nt = image->nd[0]*image->nd[1]*image->nd[2];
  if ( nt<=0 ) return -1;

  status = 0;

  switch (image->type[0]) {
  case CRAFT_IMAGE_UNKNOWN:
    return 1;
    break;
  case CRAFT_IMAGE_CHAR :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.c)) * nt );
    break;
  case CRAFT_IMAGE_UCHAR :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.uc)) * nt );
    break;
  case CRAFT_IMAGE_INT :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.i)) * nt );
    break;
  case CRAFT_IMAGE_UINT :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.ui)) * nt );
    break;
  case CRAFT_IMAGE_FLOAT :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.f)) * nt );
    break;
  case CRAFT_IMAGE_DOUBLE :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.d)) * nt );
    break;
  case CRAFT_IMAGE_FOURIER_DOUBLE :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.fd)) * nt );
    break;
  case CRAFT_IMAGE_HARMONIC_DOUBLE : /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.hd)) * nt );
    break;
  case CRAFT_IMAGE_TENSOR2 :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.t2)) * nt );
    break;
  case CRAFT_IMAGE_FOURIER_TENSOR2 :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.ft2)) * nt );
    break;
  case CRAFT_IMAGE_HARMONIC_TENSOR2 : /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.ht2)) * nt );
    break;
  case CRAFT_IMAGE_VECTOR3D :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.v3d)) * nt );
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR3D :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.fv3d)) * nt );
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR3D : /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.hv3d)) * nt );
    break;
  case CRAFT_IMAGE_VECTOR :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.a)) * image->type[1] * nt );
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR :
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.fa)) * image->type[1] * nt );
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR : /* added for harmonic implementation 2017/07/05  J. Boisse */
    image->pxl.v = (void *)malloc( sizeof(*(image->pxl.ha)) * image->type[1] * nt );
    break;
  default:
    return -2;
  }
  if(image->pxl.v == (void *)0 ) {
    fprintf(stderr,"craft_image_alloc: unsuccessfull memory allocation.\n");
    status=-1;
  }
  return status;
}

/*----------------------------------------------------------------------------*/
/* Deallocation of pxl in an image                                            */
int craft_image_free( CraftImage *image ){

  /* deallocation of pixel data */
  if ( (image->type[0]) != CRAFT_IMAGE_UNKNOWN ) {
    free(image->pxl.v);
  }
  return 0;
}
/*----------------------------------------------------------------------------*/
/* Comparison of two images:                                                  */
/* return 1 if n, p, s, type and nd field are identical in both images
   return 0 elesewhere.
*/
int cmp_size_and_type_craft_images( CraftImage ima1, CraftImage ima2) {

  if( 

     (ima1.n[0]==ima2.n[0]) && 
     (ima1.n[1]==ima2.n[1]) && 
     (ima1.n[2]==ima2.n[2]) && 

     /*
     (ima1.s[0]==ima2.s[0]) && 
     (ima1.s[1]==ima2.s[1]) && 
     (ima1.s[2]==ima2.s[2]) && 

     (ima1.p[0]==ima2.p[0]) && 
     (ima1.p[1]==ima2.p[1]) && 
     (ima1.p[2]==ima2.p[2]) && 
     */

     (fabs(ima1.s[0]-ima2.s[0])<TINY) && 
     (fabs(ima1.s[1]-ima2.s[1])<TINY) && 
     (fabs(ima1.s[2]-ima2.s[2])<TINY) && 

     (fabs(ima1.p[0]-ima2.p[0])<TINY) && 
     (fabs(ima1.p[1]-ima2.p[1])<TINY) && 
     (fabs(ima1.p[2]-ima2.p[2])<TINY) && 

     (ima1.nd[0]==ima2.nd[0]) && 
     (ima1.nd[1]==ima2.nd[1]) && 
     (ima1.nd[2]==ima2.nd[2]) &&

     (ima1.type[0]==ima2.type[0]) && 
     (ima1.type[1]==ima2.type[1]) && 
     (ima1.type[2]==ima2.type[2])


      ) {
    return 1;
  }
  else {
    return 0;
  }

  return 0;
}


/*----------------------------------------------------------------------------*/
/* Comparison of two images:                                                  */
/* return 1 if n, p, s and nd field are identical in both images
   return 0 elesewhere.
*/
int cmp_size_craft_images( CraftImage ima1, CraftImage ima2) {

  if( 

     (ima1.n[0]==ima2.n[0]) && 
     (ima1.n[1]==ima2.n[1]) && 
     (ima1.n[2]==ima2.n[2]) && 

     /*
     (ima1.s[0]==ima2.s[0]) && 
     (ima1.s[1]==ima2.s[1]) && 
     (ima1.s[2]==ima2.s[2]) && 

     (ima1.p[0]==ima2.p[0]) && 
     (ima1.p[1]==ima2.p[1]) && 
     (ima1.p[2]==ima2.p[2]) && 
     */

     (fabs(ima1.s[0]-ima2.s[0])<TINY) && 
     (fabs(ima1.s[1]-ima2.s[1])<TINY) && 
     (fabs(ima1.s[2]-ima2.s[2])<TINY) && 

     (fabs(ima1.p[0]-ima2.p[0])<TINY) && 
     (fabs(ima1.p[1]-ima2.p[1])<TINY) && 
     (fabs(ima1.p[2]-ima2.p[2])<TINY) && 

     (ima1.nd[0]==ima2.nd[0]) && 
     (ima1.nd[1]==ima2.nd[1]) && 
     (ima1.nd[2]==ima2.nd[2]) 


      ) {
    return 1;
  }
  else {
    return 0;
  }

  return 0;
}


/******************************************************************************/
/* Herve Moulinec LMA/CNRS
   23 september 2009

   phase_to_image copy data organized in phase into data organized in image.

   In both representations (phase or image), data are supposed to be of the same
   type (double scalars or double 2d order tensor ...) The image type is
   supposed to give information about the type of the data to be transferred.

   Inputs:
   int Nph       : number of phases
   int nppp[Nph] : number of points in each phase
   void *x:      : x[i][j] gives the data of the j-th point in i-th phase.
                   i=0-Nph j=0-nppp[i]

    Output:
    CraftImage image :
                   image

    Return value:
     0 : succesful return
    -1 : image is empty
    -2 : image of unknown data type

*/
/*********************************************************************************/
int phase_to_image( int Nph, int nppp[Nph], int *index[Nph], void *x[Nph],
                    CraftImage *image) {
  int status;
  int iph, ipt, i;
  size_t len;

  status = 0;

 /* if ( (image->n[0] <=0) || (image->n[1] <=0) || (image->n[2] <=0)
      || (image->nd[0]<=0) || (image->nd[1]<=0) || (image->nd[2]<=0) ){
    status = -1;
    return status;
  }*/

  /*-------------------------------------------------------------------------------*/
  switch (image->type[0]){
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UNKNOWN:
    status = -2;
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_INT:
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, image, index, nppp)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.i)[index[iph][ipt]] = 0;
        }
      }
      else {
        int *x_i;
        x_i = (int *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_i, image, index, nppp)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.i)[index[iph][ipt]] = x_i[ipt];
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FLOAT:
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, image, index, nppp)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.f)[index[iph][ipt]] = NAN;
        }
      }
      else {
        float *x_f;
        x_f = (float *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_f, image, index, nppp)
        for (ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.f)[index[iph][ipt]] = x_f[ipt];
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_DOUBLE :
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, image, index, nppp)
        for (ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.d)[index[iph][ipt]] = NAN;
        }
      }
      else {
        double *x_d;
        x_d = (double *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_d, image, index, nppp)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.d)[index[iph][ipt]] = x_d[ipt];
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_DOUBLE : /* added for harmonic implementation 2017/07/05  J. Boisse */
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, image, index, nppp)
        for (ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.hd)[index[iph][ipt]] = NAN;
        }
      }
      else {
        double complex *x_hd;
        x_hd = (double complex *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_hd, image, index, nppp)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          (image->pxl.hd)[index[iph][ipt]] = x_hd[ipt];
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_TENSOR2:
    len = 6*sizeof(double);
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
        double x_t2[6];
        for(i=0; i<6; i++) x_t2[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_t2, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.t2 + index[iph][ipt], x_t2, len);
          /* for(i=0; i<6; i++) (image->pxl.t2)[index[iph][ipt]][i] = NAN; */
        }
      }
      else{
        double (*x_t2)[6];
        x_t2 = (double (*)[6])x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_t2, image, index, nppp, len, stderr)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.t2 + index[iph][ipt], x_t2+ipt, len);
          /* for(i=0; i<6; i++) (image->pxl.t2)[index[iph][ipt]][i] = ((double *)x[iph])[i+ipt*6]; */
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FOURIER_TENSOR2:
    len = 6*sizeof(double complex);
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
        double complex x_ft2[6];
        for(i=0; i<6; i++) x_ft2[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_ft2, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.ft2 + index[iph][ipt], x_ft2, len);
          /* for(i=0; i<6; i++) (image->pxl.t2)[index[iph][ipt]][i] = NAN; */
        }
      }
      else{
        double complex *x_ft2;
        x_ft2 = (double complex *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_ft2, image, index, nppp, len, stderr)
        for(ipt=0; ipt<nppp[iph]; ipt++){
	  //          if (ipt<100){fprintf(stderr, "iph %d\t ipt %d\t index[iph][ipt] %d\n", iph, ipt, index[iph][ipt]);}
          memcpy(image->pxl.ft2 + index[iph][ipt], x_ft2+6*ipt, len);
          /* for(i=0; i<6; i++) (image->pxl.t2)[index[iph][ipt]][i] = ((double *)x[iph])[i+ipt*6]; */
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/07/05  J. Boisse */
#ifdef DEBUG
  printf("craftimage.c : in phase_to_image(), case CRAFT_IMAGE_HARMONIC_TENSOR2:\n");
#endif
    len = 6*sizeof(double complex);
    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
        double complex x_ht2[6];
        for(i=0; i<6; i++) x_ht2[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_ht2, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.ht2 + index[iph][ipt], x_ht2, len);
          /* for(i=0; i<6; i++) (image->pxl.ht2)[index[iph][ipt]][i] = NAN; */
        }
      }
      else{
        double complex *x_ht2;
        x_ht2 = (double complex *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_ht2, image, index, nppp, len, stderr)
        for(ipt=0; ipt<nppp[iph]; ipt++){
	  //          if (ipt<100){fprintf(stderr, "iph %d\t ipt %d\t index[iph][ipt] %d\n", iph, ipt, index[iph][ipt]);}
          memcpy(image->pxl.ht2 + index[iph][ipt], x_ht2+6*ipt, len);
          /* for(i=0; i<6; i++) (image->pxl.ht2)[index[iph][ipt]][i] = ((double complex *)x[iph])[i+ipt*6]; */
        }
      }
    }
#ifdef DEBUG
  printf("mean(image->pxl.ht2[:][j])=  (after)\n");
  double somme[6];
  int j;
  for(i=0;i<6;i++) {
    somme[i]=0.+I*0.;
    for(j=0;j<image->n[0]*image->n[1]*image->n[2];j++) {
      somme[i]+=image->pxl.ht2[j][i];
    }
    somme[i]=somme[i]/(double)(image->n[0]*image->n[1]*image->n[2]);
  }
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(somme[i]),cimag(somme[i]));
  }
#endif
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR3D:
    len = 3*sizeof(double);

    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
        double x_v3d[3];
        for(i=0; i<3; i++) x_v3d[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_v3d, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.v3d + index[iph][ipt], x_v3d, len);
          /* for(i=0; i<3; i++) (image->pxl.v3d)[index[iph][ipt]][i] = NAN;*/
        }
      }
      else{
        double (*x_v3d)[3];
        x_v3d = (double (*)[3])x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_v3d, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.v3d + index[iph][ipt], x_v3d+ipt, len);
          /* for(i=0; i<3; i++) (image->pxl.v3d)[index[iph][ipt]][i] = ((double *)x[iph])[i+3*ipt]; */
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    len = 3*sizeof(double complex);

    for(iph=0; iph<Nph; iph++){
      if ( x[iph] == (void *)0 ){
        double complex x_hv3d[3];
        for(i=0; i<3; i++) x_hv3d[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_hv3d, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.hv3d + index[iph][ipt], x_hv3d, len);
          /* for(i=0; i<3; i++) (image->pxl.hv3d)[index[iph][ipt]][i] = NAN;*/
        }
      }
      else{
        double complex (*x_hv3d)[3];
        x_hv3d = (double complex (*)[3])x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_hv3d, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.hv3d + index[iph][ipt], x_hv3d+ipt, len);
          /* for(i=0; i<3; i++) (image->pxl.hv3d)[index[iph][ipt]][i] = ((double complex *)x[iph])[i+3*ipt]; */
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR:
    len = image->type[1]*sizeof(double);
    for(iph=0; iph<Nph; iph++){

      if ( x[iph] == (void *)0 ){
        double *x_v;
        x_v = malloc(len);
        if ( x_v==NULL ){
          fprintf(stderr, "Error in phase_to_image.\n");
          return -1;
        }
        for(i=0; i<image->type[1]; i++) x_v[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_v, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.a + image->type[1]*index[iph][ipt], x_v, len);
          /* for(i=0; i<image->type[1]; i++) (image->pxl.a)[i+image->type[1]*index[iph][ipt]] = NAN; */
        }
        free(x_v);
      }
      else{
        double *x_v;
        x_v = (double *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_v, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.a + image->type[1]*index[iph][ipt], x_v+image->type[1]*ipt, len);
          /* for(i=0; i<image->type[1]; i++)
               (image->pxl.a)[i+image->type[1]*index[iph][ipt]] = ((double *)x[iph])[i+image->type[1]*ipt];
          */
        }
      }
    }
    break;

   /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    len = image->type[1]*sizeof(double complex);
    for(iph=0; iph<Nph; iph++){

      if ( x[iph] == (void *)0 ){
        double complex *x_hv;
        x_hv = malloc(len);
        if ( x_hv==NULL ){
          fprintf(stderr, "Error in phase_to_image.\n");
          return -1;
        }
        for(i=0; i<image->type[1]; i++) x_hv[i] = NAN;
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_hv, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.ha + image->type[1]*index[iph][ipt], x_hv, len);
          /* for(i=0; i<image->type[1]; i++) (image->pxl.ha)[i+image->type[1]*index[iph][ipt]] = NAN; */
        }
        free(x_hv);
      }
      else{
        double complex *x_hv;
        x_hv = (double complex *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_hv, image, index, nppp, len)
        for(ipt=0; ipt<nppp[iph]; ipt++){
          memcpy(image->pxl.ha + image->type[1]*index[iph][ipt], x_hv+image->type[1]*ipt, len);
          /* for(i=0; i<image->type[1]; i++)
               (image->pxl.ha)[i+image->type[1]*index[iph][ipt]] = ((double complex *)x[iph])[i+image->type[1]*ipt];
          */
        }
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  default:
    status=-2;
    break;
  }
  return status;
}

/******************************************************************************/
/* Herve Moulinec LMA/CNRS
   23 september 2009

   image_to_phase copy data organized in image into data organized in phases.

   In both representations (phase or image), data are supposed to be of the
   same type (double scalars or double 2d order tensor ...). The image type is
   supposed to give information about the type of the data to be transferred.

   Inputs:
   int Nph       : number of phases
   int nppp[Nph] : number of points in each phase
   CraftImage image : image

   Output:
   void *x       : x[i][j] gives the data of the j-th point in i-th phase.
                   i=0-Nph j=0-nppp[i]
                   in the case where x[i] is equal to (void *)0, the phase is supposed
                   not to store the data involved.

    Return value:
     0 : succesful return
    -1 : image is empty
    -2 : image of unknown data type

*/
/******************************************************************************/
int image_to_phase( int Nph, int nppp[Nph], int *index[Nph], void *x[Nph],
                    CraftImage *image) {
  int status;
  int iph, ipt;
  size_t len;
  status = 0;

  if ((image->n[0]  <=0) || (image->n[1]  <=0) || (image->n[2] <=0) ||
      (image->nd[0] <=0) || (image->nd[1] <=0) || (image->nd[2]<=0) ||
      (image->pxl.v==NULL) ){
    status = -1;
    return status;
  }

  switch (image->type[0]){
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UNKNOWN:
    status = -2;
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_INT:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      int *x_i;
      x_i = (int *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_i, image, index,nppp)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        x_i[ipt] = (image->pxl.i)[index[iph][ipt]] ;
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FLOAT:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      float *x_f;
      x_f = (float *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_f, image, index,nppp)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        x_f[ipt] = (image->pxl.f)[index[iph][ipt]];
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_DOUBLE:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double *x_d;
      x_d = (double *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_d, image, index,nppp)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        x_d[ipt] = (image->pxl.d)[index[iph][ipt]];
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double complex *x_hd;
      x_hd = (double complex *)x[iph];
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_hd, image, index,nppp)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        x_hd[ipt] = (image->pxl.hd)[index[iph][ipt]];
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_TENSOR2:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double (*x_t2)[6];
      x_t2 = (double (*)[6])x[iph];
      len = 6*sizeof(double);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_t2, image, index, nppp, len)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_t2+ipt, image->pxl.t2 + index[iph][ipt], len);
        /* for (i=0; i<6; i++) ((double *)x[iph])[6*ipt+i] = (image->pxl.t2)[i][index[iph][ipt]]; */
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FOURIER_TENSOR2:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double complex *x_ft2;
      x_ft2 = (double complex *)x[iph];
      len = 6*sizeof(double complex);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_ft2, image, index, nppp, len)
      for(ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_ft2+6*ipt, image->pxl.ft2 + 6*index[iph][ipt], len);
      }
    }
    break;


    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/07/05  J. Boisse */

#ifdef DEBUG
  printf("craftimage.c : in image_to_phase(), case CRAFT_IMAGE_HARMONIC_TENSOR2:\n");
  printf("mean(image->pxl.ht2[:][j])=\n");
  double complex somme[6];
  int ii,j;
  for(ii=0;ii<6;i++) {
    somme[ii]=0.+I*0.;
    for(j=0;j<image->n[0]*image->n[1]*image->n[2];j++) {
      somme[ii]+=image->pxl.ht2[j][ii];
//      printf("%f+%fi (j,i)=(%d,%d)\n",creal(image->pxl.ht2[j][i]),cimag(image->pxl.ht2[j][i]),j,i);
    }
    somme[ii]=somme[ii]/(double)(image->n[0]*image->n[1]*image->n[2]);
  }
  for(ii=0;ii<6;ii++) {
    printf("%f+%fi\n",creal(somme[ii]),cimag(somme[ii]));
  }
  for(ii=0;ii<6;ii++) {  
    printf("%f+%fi (j,i)=(%d,%d)\n",creal(image->pxl.ht2[0][ii]),cimag(image->pxl.ht2[0][ii]),0,ii);
    printf("%f+%fi (j,i)=(%d,%d)\n",creal(image->pxl.ht2[1][ii]),cimag(image->pxl.ht2[1][ii]),1,ii);
    printf("%f+%fi (j,i)=(%d,%d)\n",creal(image->pxl.ht2[2][ii]),cimag(image->pxl.ht2[2][ii]),2,ii);
  }
#endif

    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double complex *x_ht2;
      int i;
      x_ht2 = (double complex *)x[iph];
      len = 6*sizeof(double complex);
#pragma omp parallel for \
  default(none) \
  private(ipt,i) \
  shared(iph, x_ht2,x, image, index, nppp, len)
      for(ipt=0; ipt<nppp[iph]; ipt++){
//        memcpy(x_ht2+ipt, image->pxl.ht2 + index[iph][ipt], len);
        for (i=0; i<6; i++) ((double complex *)x[iph])[6*ipt+i] = (image->pxl.ht2)[index[iph][ipt]][i];       
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR3D:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double (*x_v3d)[3];
      x_v3d = (double (*)[3])x[iph];
      len = 3*sizeof(double);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_v3d, image, index, nppp, len)
      for (ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_v3d+ipt, image->pxl.v3d + index[iph][ipt], len);
        /* for (i=0; i<3; i++) ((double *)x[iph])[3*ipt+i] = (image->pxl.v3d)[i][index[iph][ipt]]; */
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double complex (*x_hv3d)[3];
      x_hv3d = (double complex (*)[3])x[iph];
      len = 3*sizeof(double complex);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_hv3d, image, index, nppp, len)
      for (ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_hv3d+ipt, image->pxl.hv3d + index[iph][ipt], len);
        /* for (i=0; i<3; i++) ((double *)x[iph])[3*ipt+i] = (image->pxl.hv3d)[i][index[iph][ipt]]; */
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_VECTOR:
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double *x_v;
      x_v = (double *)x[iph];
      len = image->type[1]*sizeof(double);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_v, image, index, nppp, len)
      for (ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_v+image->type[1]*ipt, image->pxl.a + image->type[1]*index[iph][ipt], len);
        /* for (i=0; i<image->type[1]; i++)
          ((double *)x[iph])[i+image->type[1]*ipt] = (image->pxl.a)[i*nt+index[iph][ipt]]; */
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    for(iph=0; iph<Nph; iph++){
      if (x[iph]==(void *)0) continue; // in the case where the phase does not store the data involved
      double complex *x_hv;
      x_hv = (double complex *)x[iph];
      len = image->type[1]*sizeof(double complex);
#pragma omp parallel for \
  default(none) \
  private(ipt) \
  shared(iph, x_hv, image, index, nppp, len)
      for (ipt=0; ipt<nppp[iph]; ipt++){
        memcpy(x_hv+image->type[1]*ipt, image->pxl.ha + image->type[1]*index[iph][ipt], len);
        /* for (i=0; i<image->type[1]; i++)
          ((double complex *)x[iph])[i+image->type[1]*ipt] = (image->pxl.ha)[i*nt+index[iph][ipt]]; */
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  default:
    status=-2;
    break;
  }

  return status;
}

/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   14 september 2009

   This function counts the number of phases Nph in a given image.

   The input image contains pixels of int values, supposed
   to indicate the phase index of each pixel.
   This values can be any integer.
   The function counts the actual number of phases Nph,
   and fills a table phase[Nph] which gives the phase index
   in input image for any phase from O to Nph-1:
   ex:
         if phase[10] contains -123 , it means that the phase with
         value -123 appearing in input image has been the 11-th (10+1)
         phase detected in input image.

   At the end, the image will contain the index of the phase
   reordered by order of appearance.
   It is also verified that the type of data in input image is int

   Return value:
    0 : successful return
   -1 : invalid type of data in input image
   -2 : one or several pixels contain values < 0

*/
int number_of_phases_in_image( CraftImage *image, int *Nph, int **phase ) {

  int i[3];
  int ii,value,iph,deja;

  *Nph=0;
  *phase=(int *)NULL;

  /* one verifies that input image contains pixels of int */
  if ( image->type[0] !=  CRAFT_IMAGE_INT ) {
    fprintf(stderr,
            "%s : invalid type of data in input image\n should be int\n",
            __func__);
    return -1;
  }

  for ( i[2]=0; i[2]< image->n[2] ; i[2]++) {
    for ( i[1]=0; i[1]< image->n[1] ; i[1]++) {
      for ( i[0]=0; i[0]< image->n[0] ; i[0]++) {
        ii = i[0] + image->nd[0]*(i[1]+image->nd[1]*i[2]);
        value = image->pxl.i[ii];

        /* looks if this index phase has already been encountered and stored in phase[] */
        deja=0;
        for(iph=0; iph<*Nph; iph++) {
          if((*phase)[iph]==value) {
            deja=1;
            /* renumbers pxl by order of appearance of phase */
            image->pxl.i[ii] = iph;
            break;
          }
        }

        /* "Baby it's a new age ... we can start a new phase */
        if(!deja) {
          (*Nph)++;
          *phase=(int *)realloc(*phase,(*Nph)*sizeof(**phase));
          iph = *Nph-1;
          (*phase)[iph]=value;
          /* renumbers pxl whith order of appearance of phase */
          image->pxl.i[ii] = iph;
        }
      }
    }
  }
  return 0;
}

/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   14 september 2009

   Returns number of points per phase

   Inputs:
   int Nph           : number of phases in the input image
   CraftImage *image : input image, every pixel contains the number of phase
                       it belongs to

   Output:
   int nppp[Nph]     : number of pixels in input image
                       nppp is supposed to have been properly dimensioned
                       belonging to given phase

   Return value:
     0 : successful return
    -1 : invalid type of data in input image
*/
int number_of_points_per_phase( int Nph, CraftImage *image, int nppp[Nph] ) {
  int i0,i1,i2;
  int ii;
  int iph;

  /* one verifies that input image contains pixels of int */
  if ( image->type[0] !=  CRAFT_IMAGE_INT ) {
    fprintf(stderr,
            "%s : invalid type of data in input image\n should be int\n",
            __func__);
    return -1;
  }

  /* remark on parallelization with OpenMP:
     when the number of phases is small compared to the number of available
     threads, parallelizing iph loop is not very efficient, but parallelizing
     i2 loop is even worse: to protect from conflicting writes in index table
     one has to put
        index[iph][iind[iph]]=ipxl2;
        iind[iph]++;
     lines in a critical section that considerably reduce the efficiency of
     the parallelization (in given case it can be worse than no parallelization
     at all). Thus, until we re-write the following lines as a loop
     following iph (indice of phase) we do not parallelize them.
   */
#pragma omp parallel for\
  private(iph) \
  shared(Nph,nppp)
  for(iph=0; iph<Nph; iph++) nppp[iph]=0;

  for ( i2=0; i2< image->n[2] ; i2++) {
    for ( i1=0; i1< image->n[1] ; i1++) {
      for ( i0=0; i0< image->n[0] ; i0++) {
        ii = i0 + image->nd[0]*(i1+image->nd[1]*i2);
        iph = image->pxl.i[ii];
        nppp[iph]++;
      }
    }
  }
  return 0;
}

/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   14 september 2009

   Fills the table giving, for each point of a given phase, its index
   in image image2.
   The phase it belongs to, is obtained from image1.

   image1 and image2 can be different: their number of pixels
   in each direction MUST be the same, BUT the way they are stored
   can be different ( i.e. : nd fields can be different in image1 and image2).



   All pixels belonging to a given phase are gathered in a
   given table, as it is easier to apply their behavior law
   to all of them at once.

   Nevertheless it is necessary to remind their position
   in the image to be able to construct an image of stress or strain field.


   input:
   * int Nph : number of phases in input image
   * int nppp[Nph] : number of points per phase
   * CraftImage *image1 : each pixel contains the number of the phase it belongs to
   * CraftImage *image2 : image in which indices of pixels are calculated


   output:
   * int *index[Nph]  : table of indices (has to be properly dimensioned)

   return value:
   0  : successful return
   -1 : image1 has not pixels of int value
   -2 : image1 and image2 do not have the same number of pixels ( n[3] fields differ)

 */

int pxl_in_phase(
        int Nph, int nppp[Nph],
        CraftImage *image1, CraftImage *image2,
        int *index[Nph]
        ) {
  int i0,i1,i2;
  int iph;
  int ipxl1, ipxl2;
  int ii;

  /* one verifies that input image contains pixels of int */
  if ( image1->type[0] !=  CRAFT_IMAGE_INT ) {
    fprintf(stderr,
            "%s : invalid type of data in input image\n should be int\n",
            __func__);
    return -1;
  }

  /* one verifies that both input images have same n[3] FIELDS */
  if (
      ( image1->n[0] != image2->n[0] ) ||
      ( image1->n[1] != image2->n[1] ) ||
      ( image1->n[2] != image2->n[2] ) ) {
    return -2;
  }

  /* remark on parallelization with OpenMP:
     when the number of phases is small compared to the number of available
     threads, parallelizing iph loop is not very efficient, but parallelizing
     i2 loop is even worse: to protect from conflicting writes in index table
     one has to put
        index[iph][iind[iph]]=ipxl2;
        iind[iph]++;
     lines in a critical section that considerably reduce the efficiency of
     the parallelization (in given case it can be worse than no parallelization
     at all). Thus, until we re-write the following lines as a loop
     following iph (indice of phase) we do not parallelize them.
  */

  /*
  iind = malloc(sizeof(*iind)*Nph);
#pragma omp parallel for\
  private(iph) \
  shared(Nph,iind)
  for (iph=0; iph<Nph; iph++) iind[iph]=0;
  for ( i2=0; i2< image1->n[2] ; i2++) {
    for ( i1=0; i1< image1->n[1] ; i1++) {
      for ( i0=0; i0< image1->n[0] ; i0++) {
        ipxl1 = i0 + image1->nd[0]*(i1+image1->nd[1]*i2);
        iph = image1->pxl.i[ipxl1];
        ipxl2 = i0 + image2->nd[0]*(i1+image2->nd[1]*i2);
        index[iph][iind[iph]]=ipxl2;
        iind[iph]++;
      }
    }
  }
  free(iind);
  */

  /* is this better ? */
#pragma omp parallel for \
  default(none) \
  private(iph,i0,i1,i2,ii) \
  private(ipxl1,ipxl2) \
  shared(Nph,index,image1,image2)
  for(iph=0;iph<Nph;iph++) {
    ii=0;
    for ( i2=0; i2< image1->n[2] ; i2++) {
      for ( i1=0; i1< image1->n[1] ; i1++) {
        for ( i0=0; i0< image1->n[0] ; i0++) {
          ipxl1 = i0 + image1->nd[0]*(i1+image1->nd[1]*i2);
          if (iph == image1->pxl.i[ipxl1]) {
            ipxl2 = i0 + image2->nd[0]*(i1+image2->nd[1]*i2);
            index[iph][ii]=ipxl2;
            ii++;
          }
        }
      }
    }
  }

  return 0;
}

/*********************************************************************************/
/* Fabrice Silva LMA/CNRS
   22 september 2011

    Parses the characteristic image to get the number of phases, the number of
    points per phase, initialize the fft-handling image with correct size
    and creates the associated index.
 */

int parse_characteristic_file(CraftImage *cf,
        int* Nph,
        int** phase,
        int** nppp,
        int*** index,
        CraftImage image
        ){
  int iph,Npt,status;

  /* This function:
     - counts the number of phase in the microstructure: Nph
     - fills the phase index table: phase[]
     - renumbers the image with the order of appearance of its phases */
  number_of_phases_in_image(cf, Nph, phase);

#ifdef VERBOSE
  fprintf(stdout,"Number of phases = %d \n",*Nph);
#endif

  /* Allocation of nppp and index */
  *nppp  = malloc((*Nph)*sizeof(int));
  if (*nppp==NULL) return -1;
  *index = malloc((*Nph)*sizeof(int*));
  if (*index==NULL) return -1;

  /* Counts the number of points in each phase */
  status = number_of_points_per_phase( *Nph, cf, *nppp);
  if (status==-1) return -1;

  /* Total number of points */
  Npt=0;
  for(iph=0; iph<(*Nph); iph++) Npt += (*nppp)[iph];
  if ( Npt != cf->n[0] * cf->n[1] * cf->n[2] ){
    fprintf(stderr,"craft: error in pixels count \n");
    exit(1);
  }

#ifdef VERBOSE
  for(iph=0;iph<(*Nph);iph++) {
    fprintf(stdout,"number of pixels in phase %d (%d) = %d\n",
      iph, (*phase)[iph], (*nppp)[iph]);
  }
#endif

  /* Final allocation of index */
  for(iph=0;iph<(*Nph);iph++) {
    (*index)[iph] = malloc((*nppp)[iph]*sizeof(int));
    if ((*index)[iph]==NULL) return -1;
  }

  /* Initialize image */
  /*
  image->type[0] = CRAFT_IMAGE_UNKNOWN;
  image->type[1] = 0;
  image->type[2] = 0;
  image->name = NULL;
  for(i=0; i<3; i++){
    image->n[i] = cf->n[i];
    image->nd[i]= cf->n[i];
    image->s[i] = cf->s[i];
    image->p[i] = cf->p[i];
  }
#if defined FFTW2 || defined FFTW3
  image->nd[0] = 2*(image->n[0]/2+1);   // see in FFTW 3.3.2 documentation: 4.3.4 Real-data DFT Array Format
#else
  image->nd[0] = image->n[0]+2;
#endif
  */

  /* Fills the table index which gives the index in the image of every point of every phase */
  status=pxl_in_phase(*Nph, *nppp, cf, &image, *index);
  return 0;
}

/******************************************************************************************************/
/* return the size in bytes of the values stored in a pixel of a craft image                          */
/* (caution: this size must be multiplied by the number of components (in type[1]) to give            */
/* the actual size of a data stored in a pixel)                                                       */
/******************************************************************************************************/
int craft_image_data_size(CraftImage ima){
  switch (ima.type[0]) {

  case CRAFT_IMAGE_CHAR:
    return sizeof(char);
  case CRAFT_IMAGE_UCHAR:
    return sizeof(unsigned char);
  case CRAFT_IMAGE_INT:
    return sizeof(int);
  case CRAFT_IMAGE_UINT:
    return sizeof(unsigned int);
  case CRAFT_IMAGE_FLOAT:
    return sizeof(float);
  case CRAFT_IMAGE_DOUBLE:
    return sizeof(double);

  case CRAFT_IMAGE_TENSOR2:
  case CRAFT_IMAGE_VECTOR3D:
  case CRAFT_IMAGE_VECTOR:
  case CRAFT_IMAGE_FOURIER_DOUBLE:
  case CRAFT_IMAGE_FOURIER_TENSOR2:
  case CRAFT_IMAGE_FOURIER_VECTOR3D:
  case CRAFT_IMAGE_FOURIER_VECTOR:
  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
  case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/07/05  J. Boisse */
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    return sizeof(double);
    
    
  case CRAFT_IMAGE_UNKNOWN:
    fprintf(stderr,"craft_image_data_size: warning: unknown image file format in %s\n",ima.name);
    return 0;
  default:
    fprintf(stderr,"craft_image_data_size: warning: unknown image file format in %s\n",ima.name);
    return 0;
};


}


/******************************************************************************************************/
/* tells the data format of the image */
/******************************************************************************************************/
int craft_image_data_format(char *fmt, CraftImage ima){
  if(fmt==(char *)0){
    fmt=malloc(50);
  }
  
  switch(ima.type[0]){
  case CRAFT_IMAGE_CHAR:
    sprintf(fmt,"char");
    break;
  case CRAFT_IMAGE_UCHAR:
    sprintf(fmt,"unsigned char");
    break;
  case CRAFT_IMAGE_INT:
    sprintf(fmt,"int");
    break;
  case CRAFT_IMAGE_UINT:
    sprintf(fmt,"unsigned int");
    break;
  case CRAFT_IMAGE_FLOAT:
    sprintf(fmt,"float");
    break;
  case CRAFT_IMAGE_DOUBLE:
    sprintf(fmt,"double");
    break;
  case CRAFT_IMAGE_TENSOR2:
    sprintf(fmt,"2d order tensor");
    break;
  case CRAFT_IMAGE_VECTOR3D:
    sprintf(fmt,"3D vector");
    break;
  case CRAFT_IMAGE_VECTOR:
    sprintf(fmt,"vector of size %d",ima.type[1]);
    break;
  case CRAFT_IMAGE_FOURIER_DOUBLE:
    sprintf(fmt,"double (in Fourier space)");
    break;
  case CRAFT_IMAGE_FOURIER_TENSOR2:
    sprintf(fmt,"2d order tensor (in Fourier space)");
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR3D:
    sprintf(fmt,"3D vector (in Fourier space)");
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR:
    sprintf(fmt,"vector of size %d (in Fourier space)",ima.type[1]);
    break;
  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
    sprintf(fmt,"double (harmonic, in direct space)");
    break;
  case CRAFT_IMAGE_HARMONIC_TENSOR2: /* added for harmonic implementation 2017/07/05  J. Boisse */
    sprintf(fmt,"2d order tensor (harmonic, in direct space)");
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    sprintf(fmt,"3D vector (harmonic, in direct space)");
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    sprintf(fmt,"vector of size %d (harmonic, in direct space)",ima.type[1]);
    break;
  case CRAFT_IMAGE_UNKNOWN:
    sprintf(fmt,"unknown");
    break;
  default:
    sprintf(fmt,"invalid");
    break;
  }
  return 0;
}
