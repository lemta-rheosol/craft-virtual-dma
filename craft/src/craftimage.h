#ifndef __CRAFTIMAGE__
#define __CRAFTIMAGE__
/*-----------------------------------------------------------*/
#ifndef _ISOC99_SOURCE
#define _ISOC99_SOURCE
#endif
#include <complex.h>

#if defined FFTW2 || defined FFTW3
#include <fftw3.h>
#endif
/*-----------------------------------------------------------*/

enum Craft_Image_Type{
  CRAFT_IMAGE_UNKNOWN = 0,
  CRAFT_IMAGE_CHAR    = 1,
  CRAFT_IMAGE_UCHAR   = 2,
  CRAFT_IMAGE_INT     = 3,
  CRAFT_IMAGE_UINT    = 4,
  CRAFT_IMAGE_FLOAT   = 5,
  CRAFT_IMAGE_DOUBLE  = 6,
  CRAFT_IMAGE_TENSOR2 = 10,
  CRAFT_IMAGE_VECTOR3D= 15,
  CRAFT_IMAGE_VECTOR  = 20,

  /* added for harmonic implementation 2017/07/05  J. Boisse */
  CRAFT_IMAGE_HARMONIC_DOUBLE   = 8,
  CRAFT_IMAGE_HARMONIC_TENSOR2  = 12,
  CRAFT_IMAGE_HARMONIC_TENSOR2_CREAL  = 13,
  CRAFT_IMAGE_HARMONIC_TENSOR2_CIMAG  = 14,
  CRAFT_IMAGE_HARMONIC_VECTOR3D = 17,
  CRAFT_IMAGE_HARMONIC_VECTOR   = 22,

  CRAFT_IMAGE_FOURIER_DOUBLE   = 7,
  CRAFT_IMAGE_FOURIER_TENSOR2  = 11,
  CRAFT_IMAGE_FOURIER_VECTOR3D = 16,
  CRAFT_IMAGE_FOURIER_VECTOR   = 21
};

enum Craft_FFT_mode{
  CRAFT_FFT_INIT_ESTIMATE=2,
  CRAFT_FFT_INIT_MEASURE=0,
  CRAFT_FFT_INIT_PATIENT=3,
  CRAFT_FFT_DIRECT=-1,
  CRAFT_FFT_INVERSE=1
};


/*-----------------------------------------------------------*/
typedef struct{
  char *name;       /* image name */
  int n[3];         /* number of pixels in each direction */
  double s[3];      /* physical coordinates of start pixel */
  double p[3];      /* step sizes in each direction */
  int type[3];      /* data type in pixels */
  int nd[3];        /* number of pixels of the data as they are
                       stored in pxl_d, or pxl_i, ...
                       ( nd[i] MUST be greater than or equal to n[i] ) */
  union pxl{
    void *v;           /* pixels of unknown type */
    char *c;           /* pixels of char type */
    unsigned char *uc; /* pixels of unsigned char type */
    int *i;            /* pixels of integer values */
    unsigned int *ui;  /* pixels of unsigned integer values */
    float *f;          /* pixels of float values */
    double *d;         /* pixels of double values */
    double (*t2)[6];   /* pixels of double second order tensor pixels
                          organized following Voight convention */
    double *a;         /* pixels of array data */
    double (*v3d)[3];  /* pixels of double 3d vector */
    /*
       Next fields concern pixels of doubles arrays (1, 3, 6 or variable-length)
       in Fourier space: the components of each array are "double complex values.
       As these data are supposed to be the result of an FFT applied to real
       value, they have a conjugate symmetry, i.e.:

                I(-ksi1,ksi2,ksi3)= conjugate( I(ksi1,ksi2,ksi3) )

       and negative frequencies of 1st dimension are not stored,

       2017/07/05 J. Boisse
       in the harmonic implementation direct and Fourier space
       the components of each array are both "double complex values".
       In this case negative frequencies of 1st dimension are taken into account
    */
    double complex *fd;        /* pixels of double values in Fourier space */
    double complex (*ft2)[6];  /* pixels of 2d order tensor in Fourier space */
    double complex (*fv3d)[3]; /* pixels of double 3d vector in Fourier space */
    double complex *fa;        /* pixels of array data in Fourier space */
    
    /* added for harmonic implementation 2017/07/05 J. Boisse */
    double complex *hd;        /* pixels of double values in direct space for harmonic case*/
    double complex (*ht2)[6];  /* pixels of 2d order tensor in direct space for harmonic case */
    double complex (*hv3d)[3]; /* pixels of double 3d vector in direct space for harmonic case */
    double complex *ha;        /* pixels of array data in direct space for harmonic case */
    
  } pxl;
} CraftImage;

/* null constant for CraftImage structure:                           */
#define NullCraftImage                          \
  {                                             \
    (char *)0,                                  \
    {0, 0, 0},				        \
    {0., 0., 0.},				\
    {0., 0., 0.},				\
    {CRAFT_IMAGE_UNKNOWN, 0, 0},	        \
    {0, 0, 0},                                  \
    { (void *)0 }			        \
  }





/*-----------------------------------------------------------*/
CraftImage *craft_image_new( CraftImage model, int type[3] );
void craft_image_delete(CraftImage *image);
void craft_image_init( CraftImage *image, int type[3]);
void craft_image_reset( CraftImage *image, int type[3]);
int craft_image_alloc( CraftImage *image );
int craft_image_free( CraftImage *image );
int craft_fft_image( CraftImage *image,  int isign);
int craft_fft_frequency_vectors(
        CraftImage *image,
        double **ksi1,
        double **ksi2,
        double **ksi3);

/* added for harmonic implementation 2017/07/05 J. Boisse */
int craft_fft_image_harmonic( CraftImage *image,  int isign);
int craft_fft_frequency_vectors_harmonic(
        CraftImage *image,
        double **ksi1,
        double **ksi2,
        double **ksi3);

int cmp_size_craft_images(CraftImage ima1, CraftImage ima2);
int cmp_size_and_type_craft_images(CraftImage ima1, CraftImage ima2);
int craft_image_data_size(CraftImage ima);

int image_to_phase( int Nph, int nppp[Nph], int *index[Nph], void *x[Nph],
        CraftImage *image);
int phase_to_image( int Nph, int nppp[Nph], int *index[Nph], void *x[Nph],
        CraftImage *image);
int number_of_phases_in_image(
        CraftImage *image,
        int *Nph,
        int **phase
        ) ;
int number_of_points_per_phase(
        int Nph,
        CraftImage *image,
        int nppp[Nph] ) ;
int pxl_in_phase(
        int Nph,
        int nppp[Nph],
        CraftImage *image1,
        CraftImage *image2,
        int *index[Nph]
        );
int parse_characteristic_file(CraftImage *cf,
        int* Nph,
        int** phase,
        int** nppp,
        int*** index,
        CraftImage image
        );


int read_image( char *name, CraftImage *cima );
int write_image( char *name, CraftImage *cima , char *fmt );
int wvtk_vectorial( char *fname, CraftImage *cima );
int rvtk_vectorial( char *fname, CraftImage *cima );

int craft_image_data_format(char *fmt, CraftImage ima);

#endif
