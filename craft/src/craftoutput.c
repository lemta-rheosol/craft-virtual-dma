#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <craftoutput.h>
#include <stdlib.h>
#include <math.h>

#include <utils.h>
#include <loading.h>
#include <craft_time.h>


/*************************************************************************************/
/* parses string blabla in times specifications.
   Results:
   nlist : gives number of time spec
   list : gives for each time spec begin, end, step of time

   Attention:
   list is reallocated, thus it needs to be set to 0 when never used before
*/
int enter_output_times(char *blabla, int *nlist, char *(**list)[3]){
  char *p1, *p2, *s1, *s2;
  int i,i2,n;

  n=0;
  p1 = strtok_r(blabla,", ",&s1);
  /* Parse several time specifications */
  while ( p1 != NULL ){
    n++;
    *list = realloc(*list, n*sizeof(**list));
    (*list)[n-1][0] = (*list)[n-1][1] = (*list)[n-1][2] = NULL;

    /* Is this a range ? then fill end (and step if provided) */
    i2 = 0;
    p2 = strtok_r(p1,":",&s2);
    while ( p2 != NULL ){
      (*list)[n-1][i2]=malloc(strlen(p2)+1);
      strcpy((*list)[n-1][i2],p2);
      i2++;
      if(i2>3){
        fprintf(stderr,"too many \":\" delimiters in %s\n",p1);
        return -1;
      }
      p2 = strtok_r(NULL, ":", &s2);
    }
    p1=strtok_r(NULL, ", ", &s1);
  }
  *nlist = n;

  /* if no time list is given, last time will be stored */
  if( *nlist == 0 ){
    *nlist=1;
    *list = malloc( sizeof(*(*list)) );
    (*list)[0][0]=(*list)[0][1]=(*list)[0][2]=(char *)0;
    (*list)[0][0]=malloc(strlen("end")+1);
    strcpy((*list)[0][0],"end");
  }
  return 0;
}

/*************************************************************************************/
int read_output_line(char *line, const char *keyword, char **name, int *nlist, char *(**list)[3]){
  int i, n, cas, status;
  char *p, *blabla;
  blabla = NULL;

  /*---------------------------------------------------------------------------------*/
  /* Image */
  /*---------------------------------------------------------------------------------*/
  /* firstly, one searches keyword keyword */
  i = strstr_nr_appearances(line,keyword);
  //  printf("nr of times the keyword \"%s\"appears = %d\n",keyword,i);

  if (i>1) {
    fprintf(stderr,"keyword \"%s\" appears more than once\n",keyword);
    return -1 ;
  }
  else if (i<1) {
    fprintf(stderr,"keyword \"%s\"does not appear\n",keyword);
    return -1 ;
  }

  /* - - - - - - - - - - - - - - - - - - - - - - */
  /* image field case */
  if ( (i=strstr_position(line,keyword))!= -1 ) {

    /* 012345678901234567890 */
    /*     image012345678901 */
    // printf("image en position %d\n",i);

    n = i;
    (*name) = malloc( n+1 );
    strncpy((*name),line,n);
    (*name)[n]='\0';
    clean_string((*name));
    // printf("nom de l'image: %d \"%s\"\n",n,*name);

    p = line+i+strlen(keyword);
    n = strlen(p);
    blabla = malloc( n+1 );
    strncpy(blabla,p,n);
    blabla[n]='\0';
    // printf("blabla: %d %s\n",n,blabla);
    suppress_string_in_string(blabla," ");
    // printf("blabla (apres suppression): %d %s\n",n,blabla);
    if ( blabla[0]!= '=' ) {
      cas=1;
    }

    if(blabla[0]=='='){
      // printf("on a trouve un = \n");
      if ( (strncmp(blabla+1,"yes",3)==0) || (strncmp(blabla+1,"y",1)==0) ){
        suppress_string_in_string(blabla,"=yes");
        suppress_string_in_string(blabla,"=y");
        // printf("la fin:%s\n",blabla);
        cas=1;
      }
      else if( (strncasecmp(blabla+1,"no",2)==0) || (strncasecmp(blabla+1,"n" ,1)==0) ){
        /* in that case, there is nothing to do */
        cas=0;
      }
      else{
        fprintf(stderr,"syntax error in line %s\n",line);
        cas=-1;
      }
    }

    if (cas<=0) {
      free(blabla);
      return cas;
    }

    if (cas==1) {
      status=enter_output_times(blabla,nlist,list);
      if (status<0) {
        fprintf(stderr,"syntax error in line %s \n",line);
        free(blabla);
        return -1;
      }
    }
  }

  free(blabla);
  return 0;
}

/*************************************************************************************/
/* search a given value in a list of n values 
   returns the index of matching if found or -1 if not found */
int search_x_in_list(int n, double list[n], double x){
  int i;
  for(i=0;i<n;i++){
    //    fprintf(stderr,"           %f %f %e \n",x,list[i],fabs(x-list[i]));
    if( fabs(x-list[i]) < TINY ) {
      return i;
    }
  }
  return -1;
}

/*************************************************************************************/
int fill_clic(int nt, int clic[nt], double times[nt], int nl, char *list[nl][3]){
  int i, j, il, ito, itf, idt, it, status;
  double t, to, tf, dt;

  /*
    for(i=0;i<nl;i++){
    for(j=0;j<3;j++) {
    printf("%s ",list[i][j]);
    }
    printf("\n");
    }
  */

  /* initializes clic table */
  for (i=0;i<nt;i++) clic[i]=0;
  for(il=0;il<nl;il++){
    //    printf("il=%d %d\n",il,nl);

    /* first time */
    if ( list[il][0] == (char *)0 ){
      to=0.;
    }
    else if ( (strcmp(list[il][0],"first")==0) || (strcmp(list[il][0],"begin")==0) ){
      ito = 2;
      to = times[2];
      /* remind that times[0] and times[1] refer to non-real loading steps
         made for initialization purpose */
    }
    else if ( (strcmp(list[il][0],"last")==0) || (strcmp(list[il][0],"end")==0) ){
      ito = nt-1;
      to = times[nt-1];
    }
    else if (list[il][0][0]=='@' ){
      status=sscanf(list[il][0]+1,"%d",&ito);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][0]);
        return -1;
      }
      ito += 2;
      /* remind that times[0] and times[1] refer to non-real loading steps
         made for initialization purpose */
      ito -= 1;
      /* steps are numbered form 1 to N (and not from 0 to N-1 as in C language) */
      to=times[ito];
    }
    else {
      status=sscanf(list[il][0],"%lg",&to);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][0]);
        return -1;
      }
      ito = search_x_in_list(nt,times,to);
    }

    if ( (ito<2) || (ito>(nt-1)) ){
      fprintf(stderr,"no corresponding time in loading steps for t=%s\n",list[il][0]);
      return -1;
    }

    // printf("ito=%d\n",ito);

    /* final time */
    if ( list[il][1] == (char *)0 ) {
      itf=ito;
      tf=times[itf];
    }
    else if ( (strcmp(list[il][1],"first")==0) || (strcmp(list[il][1],"begin")==0) ){
      itf = 2;
      tf = times[2];
      /* remind that times[0] and times[1] refer to non-real loading steps
         made for initialization purpose */
    }
    else if ( (strcmp(list[il][1],"last")==0) || (strcmp(list[il][1],"end")==0) ){
      itf = nt-1;
      tf = times[nt-1];
    }
    else if (list[il][1][0]=='@' ){
      status=sscanf(list[il][1]+1,"%d",&itf);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][1]);
        return -1;
      }      
      itf += 2;
      /* remind that times[0] and times[1] refer to non-real loading steps
         made for initialization purpose */
      itf -= 1;
      /* steps are numbered form 1 to N (and not from 0 to N-1 as in C language) */
      tf=times[itf];
    }
    else{
      status=sscanf(list[il][1],"%lg",&tf);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][1]);
        return -1;
      }
      itf = search_x_in_list(nt,times,tf);
    }

    if ( (itf<2) || (itf>nt-1) ){
      fprintf(stderr,"no corresponding time in loading steps for t=%s\n",list[il][1]);
      return -1;
    }

    //    printf("itf=%d\n",itf);

    /* step */
    /* if no step is specified one fills clic table from first to last time */
    if ( list[il][2] == (char *)0 ){
      for(it=ito;it<=itf;it++){
        clic[it]=1;
      }
    }
    else if ( list[il][2][0] == '@' ){
      status=sscanf(list[il][2]+1,"%d",&idt);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][2]);
        return -1;
      }
      for(it=ito;it<=itf;it+=idt){
        clic[it]=1;
      }
    }
    else{
      status=sscanf(list[il][2],"%lg",&dt);
      if (status != 1) {
        fprintf(stderr,"invalid time specification: %s\n",list[il][2]);
        return -1;
      }
      for(t=to;t<=tf;t+=dt){
        it=search_x_in_list(nt,times,t);
        if(it==-1) {
          fprintf(stderr,"no corresponding time in loading steps for t=%lg\n",t);
          return -1;
        }
        else{
          clic[it]=1;
        }
      }
    }
  }

  return 0;
}

/********************************************************************************************/
/* initializaion of "craft output descriptor"                                               */
int init_output_description( Craft_Output_Description *cod, Loading *load){
  int i;
  char *date;

  /* initialization of craft outputs structure*/
  cod->number_of_moments=0;
  cod->moment_name=(char **)0;
  cod->clicmoment=(int **)0;

  cod->im_format=0;
  cod->generic_name=(char *)0;

  cod->number_of_images=0;
  cod->image_name=(char **)0;
  cod->clicimage=(int **)0;

  cod->clicvariables=(int *)0;

  date = craft_date(0);
  cod->generic_name = (char *)malloc(strlen("craft_output_")+strlen(date)+1);
  strcpy(cod->generic_name,"craft_output_");
  strcat(cod->generic_name, date);
  free(date);

  /* we need to fill the list of possible times.
     It is copied from loading conditions */

  cod->Nt=load->Nt;
  cod->time = malloc( cod->Nt * sizeof(*(cod->time)) );
  for(i=0;i<cod->Nt;i++) cod->time[i]=load->step[i].time;

  return 0;
}

/********************************************************************************************/
int read_output_description( Craft_Output_Description *cod, char *ofn){
  int status, i, j, ilen, nl;
  FILE *f;
  char line[1024];
  char *tampon=(char *)0;
  char response[1024];
  status=0;
  /* the list of times (entered as strings) at which data has to be saved */
  /* for each time, 3 strings are given and describe:
     first time to be saved, last time to be saved, step between to successive
     time from beginning till end.
     If final time is entered as an empty string, it is considered equal than
     first time (otherly said: just one time has to be saved).
     If time step is entered as an empty step, it is considered as "as much steps 
     as possible between first time and last time, otherly said saving time step is 
     equal to loading step."
  */
  char *(*list)[3]=(char *(*)[3])0;
  nl=0; /* number of times specified */

  if(ofn==(char *)NULL) {
    return status;
  }

  f = fopen(ofn,"r");
  if( f == (FILE *)0){
    status=-1;
    fprintf(stderr,"problem occured while opening file %s\n",ofn);
    return status;
  }

  /*----------------------------------------------------------------------------------------*/
  while (craft_fgets(line,1024,f)) {
    /* eliminates last character when \n (which has almost certainly been put by fgets) */
    if( line[strlen(line)-1]=='\n' ) line[strlen(line)-1]='\0';
    /* remove repeated spaces, spaces at the beginning and spaces at the end of string line */
    clean_string(line);

    if ( (strstr(line,"genericname") != (char *)0)
      || (strstr(line,"generic name") != (char *)0) ){
      /* command line speciying generic name of output files */

      /* looking for = delimiter */
      if (strstr(line,"=")==(char *)0) {
        fprintf(stderr,"Error output file description %s : \n",ofn);
        fprintf(stderr,
            "\"=\" delimiter is missing in generic name specification: %s\n",
            line);
        status=-2;
        return status;
      }
      i=sscanf(strstr(line,"=") + 1,"%s",response);
      if(i!=1) {
        status=-2;
        fprintf(stderr,"invalid response in command \n%s in file %s\n",line,ofn);
        return status;
      }
      free(cod->generic_name);
      cod->generic_name = (char *)malloc(strlen(response)+1);
      strcpy(cod->generic_name,response);
      // printf("%s %s\n",response,cod->generic_name);
      continue;
    }
    else if ( strstr( line, "moment") != (char *)0 ){
      /* command line containing "moment" keyword */
      cod->number_of_moments++;
      i = cod->number_of_moments - 1;
      cod->clicmoment = realloc(cod->clicmoment,
                        cod->number_of_moments * sizeof(*(cod->clicmoment)));
      (cod->clicmoment)[i] = malloc( cod->Nt * sizeof(*((cod->clicmoment)[i])));
      cod->moment_name=realloc(cod->moment_name,
                        cod->number_of_moments*sizeof(*(cod->moment_name)));

      status=read_output_line(line,"moment",&(cod->moment_name)[i], &nl, &list);
      if(status<0) {
        printf("invalid line command %s\n",line);
        return -1;
      }

      /* fill the list */
      status=fill_clic(cod->Nt,(cod->clicmoment)[i],cod->time,nl,list);
      /* one does not need list anymore */
      for(i=0; i< nl; i++){
        free( list[i][0] );
        free( list[i][1] );
        free( list[i][2] );
      }
      free(list);
      list=(char *(*)[3])0;
      nl=0;
      if(status<0){
        fprintf(stderr,"invalid spec for output \n");
        return -1;
      }
      continue;
    }
    else if ( strstr( line, "image") != (char *)0 ){
      /* command line containing "image" keyword */
      cod->number_of_images++;
      i = cod->number_of_images - 1;
      cod->clicimage = realloc(cod->clicimage, 
                       cod->number_of_images * sizeof(*(cod->clicimage)));
      (cod->clicimage)[i] = malloc( cod->Nt * sizeof(*((cod->clicimage)[i])));
      cod->image_name=realloc(cod->image_name,
                      cod->number_of_images*sizeof(*(cod->image_name)));

      status=read_output_line(line,"image",&(cod->image_name)[i], &nl, &list);
      if(status<0) {
        printf("invalid line command %s\n",line);
        return -1;
      }

      /* fill the list */
      status=fill_clic(cod->Nt,(cod->clicimage)[i],cod->time,nl,list);
      /* one does not need list anymore */
      for(i=0; i< nl; i++){
        free( list[i][0] );
        free( list[i][1] );
        free( list[i][2] );
      }
      free(list);
      list=(char *(*)[3])0;
      nl=0;
      if(status<0){
        fprintf(stderr,"invalid spec for output \n");
        return -1;
      }
      continue;
    }
    else if ( strncmp( line, "im_format=",strlen("im_format="))==0 ){
      /* command line containing "im_format" keyword */
      i=sscanf(strstr(line,"=") + 1,"%s",response);
      if(i!=1) {
        fprintf(stderr,"invalid response in command:\n\t%s in file: %s\n",line,ofn);
        status=-2;
        return status;
      }
      if( (strcmp(response , "i3d")==0) ||(strcmp(response , ".i3d")==0) ){
        cod->im_format=1;
      }
      else if( (strcmp(response , "vtk")==0) || (strcmp(response , ".vtk")==0) ){
        cod->im_format=0;
      }
      else if( (strcmp(response , "all")==0) || (strcmp(response , "ALL")==0) ){
        cod->im_format=2;
      }
      else {
        fprintf(stderr,"invalid response in command:\n\t%s \nin file %s\n",line,ofn);
        status=-2;
      }
      continue;
    }
    else if ( strncmp(line, "variables", strlen("variables"))==0 ){
      char *name;
      /* command line containing "variables" keyword */
      if ( cod->clicvariables != NULL){
        fprintf(stderr, "\"variables\" has already been specified in %s\n", ofn);
        return -1;
      }
      name = NULL;
      status=read_output_line(line, "variables", &name, &nl, &list);
      free(name);
      if (status!=0)
        fprintf(stderr, "Error in read_output_line for variables.\n");
      /* fill the list */
      cod->clicvariables = malloc( cod->Nt * sizeof(*cod->clicvariables) );
      status=fill_clic(cod->Nt, cod->clicvariables, cod->time, nl, list);
      /* one does not need list anymore */
      for(i=0; i< nl; i++){
        free( list[i][0] );
        free( list[i][1] );
        free( list[i][2] );
      }
      free(list);
      list=(char *(*)[3])0;
      nl=0;
      if(status<0){
        fprintf(stderr,"invalid spec for output \n");
        return -1;
      }
      continue;
    }

    /* line content not recognized */
    fprintf(stderr,"invalid command in file %s: \n%s ",ofn,line);
    status=-3;
    return status;
  }
  return status;
}

/********************************************************************************************/
