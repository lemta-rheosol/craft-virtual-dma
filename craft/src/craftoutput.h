#ifndef __CRAFT_OUTPUT__
#define __CRAFT_OUTPUT__

#define _ISOC99_SOURCE
#include <loading.h>

typedef struct{
  /* generic name of all output files */
  char *generic_name;

  /* list of possibles times, i.e. time steps that will be encountered during
     loading and at which one can save moments or images of mechanical variables */
  double *time;
  int Nt;

  /* number of variables whose moments have to be stored */
  int number_of_moments;
  /* lists of the names of the variables whose moments must be stored */
  char **moment_name;
  /* list of loading steps at which a given variable has to be saved */
  int **clicmoment;

  /*
    if moment_name[10] is the 11th name of moment to be saved,
    clicmoment[10] is the list of steps at which moment are to be saved,
    more precisely if clicmomennt[10][123] is equal to 1
    an moment nr 10 has to be saved at step 123

    moment_name[i] is a char string giving the name of the i-th variable
    to be stored as moment, with 0 <= i < number_of_moments
  */


  /* number of variables whose images has to be stored */
  int number_of_images;
  /* lists of the names of the variables to be stored as images */
  char **image_name;
  /* list of loading steps at which a given variable has to be saved */
  int **clicimage;

  /*
    if image_name[10] is the 11th name of image to be saved,
    clic[10] is the list of steps at which image are to be saved,
    more precisely if clic[10][123] is equal to 1
    an image nr 10 has to be saved at step 123

    image_name[i] is a char string giving the name of the i-th variable
    to be stored as image, with 0 <= i < number_of_images
  */
  
  /* list of loading steps at which the variables have to be saved */
  int *clicvariables;

  /* image format used (vtk and/or i3d):
     - im_format = 0  :  vtk
     - im_format = 1  : i3d
     - im_format = 2  : vtk and i3d */
  int im_format;
} Craft_Output_Description;

int read_output_description( Craft_Output_Description *cod, char *ofn);
int init_output_description( Craft_Output_Description *cod, Loading *load);
char *cdate(int flag);


#endif
