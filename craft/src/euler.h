#ifndef __EULER__
#define __EULER__
/*-----------------------------------------------------------*/
/* structure giving 3 Euler angles                           */

typedef struct{
  double phi1;
  double Phi;
  double phi2;
}
  Euler_Angles;
/*-----------------------------------------------------------*/
#endif
