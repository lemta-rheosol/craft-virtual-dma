#include <math.h>
#include <utils.h>
#include <complex.h>
/* complex.h => added for harmonic implementation 2017/07/05  J. Boisse */
/***************************************************************************************/
/*

  compute the error on loading conditions

*/
/***************************************************************************************/
int compute_loading_error(
                          int load_type,
                          //                          double alpha, double beta,
                          double k0, double mu0, 
                          double t,
                          double D[6],
                          double E[6], double S[6],
                          double *error
                          ){
  int status=0;
  double norm;
  double d[6];
  int i;

  double x[6];
  double y[6];
  double trs;
  double tre;
  double xs;
  double ys;
  double k;

  /*--------------------------------------------------------------------------------------*/
  switch(load_type){
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRAIN */
  /*--------------------------------------------------------------------------------------*/
  case 0:

    //    norm = E[0]*E[0] + E[1]*E[1] + E[2]*E[2] + 2.*( E[3]*E[3] + E[4]*E[4] + E[5]*E[5] );
    // HM 2017 april 20
    // This seems to me to be better to normalize by the prescribed value rather than
    // by the current calculated value:
    norm = t*t*
      ( D[0]*D[0] + D[1]*D[1] + D[2]*D[2] + 2.*( D[3]*D[3] + D[4]*D[4] + D[5]*D[5] ) );

    for(i=0;i<6;i++){
      d[i] = E[i]-t*D[i];
    }

    *error= d[0]*d[0] + d[1]*d[1] + d[2]*d[2] + 2.*( d[3]*d[3] + d[4]*d[4] + d[5]*d[5] );

    printf("error : %lf \n",*error);

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }
    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 1:
    //    norm = S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
    // HM 2017 april 20
    // This seems to me to be better to normalize by the prescribed value rather than
    // by the current calculated value:
    norm = t*t*
      ( D[0]*D[0] + D[1]*D[1] + D[2]*D[2] + 2.*( D[3]*D[3] + D[4]*D[4] + D[5]*D[5] ) );

    for(i=0;i<6;i++){
      d[i] = S[i]-t*D[i];
    }

    *error= d[0]*d[0] + d[1]*d[1] + d[2]*d[2] + 2.*( d[3]*d[3] + d[4]*d[4] + d[5]*d[5] );

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }

    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed DIRECTION of macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 2:

    norm = S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );

    trs = S[0]+S[1]+S[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (S[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = S[i]/(2.*mu0);
    }
    for(i=0;i<6;i++) y[i]=y[i]-E[i];

    trs = D[0]+D[1]+D[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      x[i] = (D[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      x[i] = D[i]/(2.*mu0);
    }


    xs = x[0]*D[0] + x[1]*D[1] + x[2]*D[2] + 2.*( x[3]*D[3] + x[4]*D[4] + x[5]*D[5] );
    ys = y[0]*D[0] + y[1]*D[1] + y[2]*D[2] + 2.*( y[3]*D[3] + y[4]*D[4] + y[5]*D[5] );

    k = (t + ys)/xs;

    for(i=0;i<6;i++){
      d[i] = S[i] - k*D[i];
    }
    *error= d[0]*d[0] + d[1]*d[1] + d[2]*d[2] + 2.*( d[3]*d[3] + d[4]*d[4] + d[5]*d[5] );

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }

    

    break;

  /*--------------------------------------------------------------------------------------*/
  /* other cases (impossible!) */
  /*--------------------------------------------------------------------------------------*/
  default:
    fprintf(stderr,"CraFT error in compute_loading_error.\n");
    fprintf(stderr,"impossible loading conditions.\n");
    status=-1;
    break;
  }
  /*--------------------------------------------------------------------------------------*/
  
  return status;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
/***************************************************************************************/
int compute_loading_error_harmonic(
                          int load_type,
                          //                          double alpha, double beta,
                          double k0, double mu0, 
                          double t,
                          double D[6],
                          double complex E[6], double complex S[6],
                          double *error
                          ){
  int status=0;
  double norm;
  double complex d[6];
  int i;

  double complex x[6]; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex y[6]; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex trs; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex tre; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex xs; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex ys; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double complex k; /* modified for harmonic implementation 2017/07/05  J. Boisse */

  /*--------------------------------------------------------------------------------------*/
  switch(load_type){
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRAIN */
  /*--------------------------------------------------------------------------------------*/
  case 0:

#ifdef DEBUG
  printf("gdrmacro.c : in compute_loading_error_harmonic(), case of prescribed macroscopic STRAIN:\n");
#endif

    //    norm = E[0]*E[0] + E[1]*E[1] + E[2]*E[2] + 2.*( E[3]*E[3] + E[4]*E[4] + E[5]*E[5] );
    // HM 2017 april 20
    // This seems to me to be better to normalize by the prescribed value rather than
    // by the current calculated value:
    norm = t*t*
      ( D[0]*D[0] + D[1]*D[1] + D[2]*D[2] + 2.*( D[3]*D[3] + D[4]*D[4] + D[5]*D[5] ) );

    for(i=0;i<6;i++){
      d[i] = E[i]-t*D[i];
    }

    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /* is it the right formula ? */
    *error= creal(d[0])*conj(d[0]) + creal(d[1])*conj(d[1]) + creal(d[2])*conj(d[2]) + 2.*( creal(d[3])*conj(d[3]) + creal(d[4])*conj(d[4]) + creal(d[5])*conj(d[5]) );

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }
#ifdef DEBUG
  printf("*error= %lf\n",*error);
#endif
    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 1:
    //    norm = S[0]*S[0] + S[1]*S[1] + S[2]*S[2] + 2.*( S[3]*S[3] + S[4]*S[4] + S[5]*S[5] );
    // HM 2017 april 20
    // This seems to me to be better to normalize by the prescribed value rather than
    // by the current calculated value:
    norm = t*t*
      ( D[0]*D[0] + D[1]*D[1] + D[2]*D[2] + 2.*( D[3]*D[3] + D[4]*D[4] + D[5]*D[5] ) );

    for(i=0;i<6;i++){
      d[i] = S[i]-t*(D[i]+0.*I);
    }

    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /* is it the right formula ? */
    *error= creal(d[0])*conj(d[0]) + creal(d[1])*conj(d[1]) + creal(d[2])*conj(d[2]) + 2.*( creal(d[3])*conj(d[3]) + creal(d[4])*conj(d[4]) + creal(d[5])*conj(d[5]) );

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }

    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed DIRECTION of macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 2:

    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /* is it the right formula ? */
    norm = creal(S[0])*conj(S[0]) + creal(S[1])*conj(S[1]) + creal(S[2])*conj(S[2]) + 2.*( creal(S[3])*conj(S[3]) + creal(S[4])*conj(S[4]) + creal(S[5])*conj(S[5]));

    trs = S[0]+S[1]+S[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (S[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = S[i]/(2.*mu0);
    }
    for(i=0;i<6;i++) y[i]=y[i]-E[i];

    trs = D[0]+D[1]+D[2]+0.*I;
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      x[i] = (D[i]-trs/3.+0.*I)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      x[i] = (D[i]+0.*I)/(2.*mu0);
    }


    xs = x[0]*D[0] + x[1]*D[1] + x[2]*D[2] + 2.*( x[3]*D[3] + x[4]*D[4] + x[5]*D[5] );
    ys = y[0]*D[0] + y[1]*D[1] + y[2]*D[2] + 2.*( y[3]*D[3] + y[4]*D[4] + y[5]*D[5] );

    k = (t + ys)/xs;

    for(i=0;i<6;i++){
      d[i] = S[i] - k*D[i];
    }

    /* modified for harmonic implementation 2017/07/05  J. Boisse */
    /* is it the right formula ? */
    *error= creal(d[0])*conj(d[0]) + creal(d[1])*conj(d[1]) + creal(d[2])*conj(d[2]) + 2.*( creal(d[3])*conj(d[3]) + creal(d[4])*conj(d[4]) + creal(d[5])*conj(d[5]) );

    if (norm<TINY){
      *error = sqrt(*error);
    }
    else{
      *error = sqrt(*error/norm);
    }

    

    break;

  /*--------------------------------------------------------------------------------------*/
  /* other cases (impossible!) */
  /*--------------------------------------------------------------------------------------*/
  default:
    fprintf(stderr,"CraFT error in compute_loading_error.\n");
    fprintf(stderr,"impossible loading conditions.\n");
    status=-1;
    break;
  }
  /*--------------------------------------------------------------------------------------*/
  
  return status;
}

/***************************************************************************************/
/*
  Compute what has to be put in the null frequency component of the "-Gammao:tau" .
  It depends on the loading conditions.

  Explanation:
  
  the alpha-beta scheme can be summarized as follows:

  1) prescribed macroscopic STRAIN :       E
      s(i+1) + c0:e(i+1) = s(i) + c0:e(i) 
                          - alpha * Gamma0:s(i) 
                          - beta  * Delta0:e(i)
                          - beta  * C0: ( <e(i)> - E )

  2) prescribed macroscopic STRESS :       S
      s(i+1) + c0:e(i+1) = s(i) + c0:e(i) 
                          - alpha * Gamma0:s(i) 
                          - beta  * Delta0:e(i)
                          - alpha  * ( <s(i)> - S )

  2) prescribed DIRECTION of macroscopic STRESS :       D
      s(i+1) + c0:e(i+1) = s(i) + c0:e(i) 
                          - alpha * Gamma0:s(i) 
                          - beta  * Delta0:e(i)
                          - alpha  * ( <s(i)> - k*D )

      with k = ( t + c0^(-1):( <s(i) - c0:e(i) ) : D )  /  c0^(-1):D:D
  
  where e(i) is the strain field at iterate i
        s(i) is the stress field at iterate i

  The algorithm used in CraFT can be summarized as

      s(i+1) + c0:e(i+1) = sa 
                           + c0 : (-Gamma0) : sb
                           + c0 : X

      with:
      sa = s(i) + (1-beta)*c0:e(i)
      sb = alpha*s(i) - beta*c0*e(i)

   X is a tensor which depends on the loading conditions:
   1) prescribed macroscopic strain
     X = beta*E
   2) prescribed macroscopic stress
     X = beta * <e(i)> - alpha*c0^(-1):( <s(i)> - S )
   3) prescribed direction of macroscopic stress
     X = beta * <e(i)> - alpha*c0^(-1):( <s(i)> - k*D )
     

      
  

*/
/***************************************************************************************/
int compute_macro(
                  int load_type,
                  double alpha, double beta,
                  double k0, double mu0, 
                  double t,
                  double D[6],double E[6], double S[6],
                  double X[6]
                  ){
  int status=0;
  int i;
    double x[6], y[6];
    double trs;
    double tre;
    double xs;
    double ys;
    double k;

  

  /*--------------------------------------------------------------------------------------*/
  switch(load_type){
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRAIN */
  /*--------------------------------------------------------------------------------------*/
  case 0:

    for(i=0;i<6;i++){
      X[i] = beta*t*D[i];
    }
 
    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 1:

    for(i=0;i<6;i++){
      x[i] = S[i]-t*D[i];
    }

    trs = x[0]+x[1]+x[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (x[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = x[i]/(2.*mu0);
    }

    for(i=0;i<6;i++){
      X[i] = beta*E[i] - alpha*y[i];
    }

    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed DIRECTION of macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 2:


    trs = S[0]+S[1]+S[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (S[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = S[i]/(2.*mu0);
    }
    for(i=0;i<6;i++) y[i]=y[i]-E[i];

    trs = D[0]+D[1]+D[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      x[i] = (D[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      x[i] = D[i]/(2.*mu0);
    }


    xs = x[0]*D[0] + x[1]*D[1] + x[2]*D[2] + 2.*( x[3]*D[3] + x[4]*D[4] + x[5]*D[5] );
    ys = y[0]*D[0] + y[1]*D[1] + y[2]*D[2] + 2.*( y[3]*D[3] + y[4]*D[4] + y[5]*D[5] );

    k = (t + ys)/xs;

    for(i=0;i<6;i++){
      x[i] = S[i]-k*D[i];
    }

    trs = x[0]+x[1]+x[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (x[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = x[i]/(2.*mu0);
    }

    for(i=0;i<6;i++){
      X[i] = beta*E[i] - alpha*y[i];
    }
    

    break;

  /*--------------------------------------------------------------------------------------*/
  /* other cases (impossible!) */
  /*--------------------------------------------------------------------------------------*/
  default:
    fprintf(stderr,"CraFT error in compute_loading_error.\n");
    fprintf(stderr,"impossible loading conditions.\n");
    status=-1;
    break;
  }
  /*--------------------------------------------------------------------------------------*/
  
  return status;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
/***************************************************************************************/
int compute_macro_harmonic(
                  int load_type,
                  double alpha, double beta,
                  double k0, double mu0, 
                  double t,
                  double D[6],double complex E[6], double complex S[6],
                  double complex X[6]
                  ){
  int status=0;
  int i;
    double complex x[6], y[6]; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex trs; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex tre; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex xs; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex ys; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    double complex k; /* modified for harmonic implementation 2017/07/05  J. Boisse */

  /*--------------------------------------------------------------------------------------*/
  switch(load_type){
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRAIN */
  /*--------------------------------------------------------------------------------------*/
  case 0:

#ifdef DEBUG
  printf("gdrmacro.c : in compute_macro_harmonic(), case of prescribed macroscopic STRAIN, E, X:\n");
  printf("E[i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(E[i]),cimag(E[i]));
  }
#endif  

    for(i=0;i<6;i++){
      X[i] = beta*t*D[i] + 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    }

#ifdef DEBUG
  printf("X[i]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal(X[i]),cimag(X[i]));
  }
#endif  
 
    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 1:

    for(i=0;i<6;i++){
      x[i] = S[i]-t*(D[i] + 0. * I); /* modified for harmonic implementation 2017/07/05  J. Boisse */
    }

    trs = x[0]+x[1]+x[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (x[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = x[i]/(2.*mu0);
    }

    for(i=0;i<6;i++){
      X[i] = beta*E[i] - alpha*y[i];
    }

    break;
  /*--------------------------------------------------------------------------------------*/
  /* in the case of prescribed DIRECTION of macroscopic STRESS */
  /*--------------------------------------------------------------------------------------*/
  case 2:


    trs = S[0]+S[1]+S[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (S[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = S[i]/(2.*mu0);
    }
    for(i=0;i<6;i++) y[i]=y[i]-E[i];

    trs = D[0]+D[1]+D[2]+ 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      x[i] = ((D[i] + 0. * I)-trs/3.)/(2.*mu0) + tre/3.; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    }
    for(i=3;i<6;i++){
      x[i] = (D[i] + 0. * I)/(2.*mu0); /* modified for harmonic implementation 2017/07/05  J. Boisse */
    }

    /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
    xs = x[0]*D[0] + x[1]*D[1] + x[2]*D[2] + 2.*( x[3]*D[3] + x[4]*D[4] + x[5]*D[5] );
    ys = y[0]*D[0] + y[1]*D[1] + y[2]*D[2] + 2.*( y[3]*D[3] + y[4]*D[4] + y[5]*D[5] );

    k = (t+0.*I + ys)/xs;

    for(i=0;i<6;i++){
      x[i] = S[i]-k*(D[i] + 0. * I); /* modified for harmonic implementation 2017/07/05  J. Boisse */
    }

    trs = x[0]+x[1]+x[2];
    tre = trs/(3.*k0);
    for(i=0;i<3;i++){
      y[i] = (x[i]-trs/3.)/(2.*mu0) + tre/3.;
    }
    for(i=3;i<6;i++){
      y[i] = x[i]/(2.*mu0);
    }

    for(i=0;i<6;i++){
      X[i] = beta*E[i] - alpha*y[i];
    }
    

    break;

  /*--------------------------------------------------------------------------------------*/
  /* other cases (impossible!) */
  /*--------------------------------------------------------------------------------------*/
  default:
    fprintf(stderr,"CraFT error in compute_loading_error.\n");
    fprintf(stderr,"impossible loading conditions.\n");
    status=-1;
    break;
  }
  /*--------------------------------------------------------------------------------------*/
  
  return status;
}
