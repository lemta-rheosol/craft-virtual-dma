#ifndef __CRAFT_GDRMACRO__
int compute_loading_error(
                          int load_type,
                          //                          double alpha, double beta,
                          double k0, double mu0, 
                          double t,
                          double D[6],
                          double E[6], double S[6],
                          double *error
                          );
int compute_macro(
                  int load_type,
                  double alpha, double beta,
                  double k0, double mu0, 
                  double t,
                  double D[6],double E[6], double S[6],
                  double X[6]
                  );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int compute_loading_error_harmonic(
                          int load_type,
                          //                          double alpha, double beta,
                          double k0, double mu0, 
                          double t,
                          double D[6],
                          double complex E[6], double complex S[6],
                          double *error
                          );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int compute_macro_harmonic(
                  int load_type,
                  double alpha, double beta,
                  double k0, double mu0, 
                  double t,
                  double D[6],double complex E[6], double complex S[6],
                  double complex X[6]
                  );

#endif
