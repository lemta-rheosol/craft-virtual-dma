C                                                                         
C  Decomposition LU :                                   
C                                                                         
      subroutine inversion(a,b,n,err)

      implicit none

      integer n
      real*8 a(n,n)
      real*8 b(n,n)

      real*8 aa(n,n)
      real*8 bb(n,n)

      real*8 err
      real*8 x

      integer i,j,k,p
      real*8 pivot

      
C on sauvegarde a dans aa pour le cas ou on utiliserait le meme tableau en entree et en sortie
      do j=1,n
         do i = 1,n
            aa(i,j) = a(i,j)
            bb(i,j) = a(i,j)
         end do
      end do

      Do k = 1 , n-1 
         Pivot = 1.0d0/bb(k,k)
         Do i = k+1 , n 
            bb(i,k) = bb(i,k)*Pivot
            Do j = k+1 , n 
               bb(i,j) = bb(i,j) - bb(i,k)*bb(k,j)
            end do
         End do
      End do
c      print *,' '
c      do i = 1,4
c         print *,(bb(i,j),j=1,4)
c      end do
c      print *,' '

      Do p = 1 , n 
         Do i = 1, n 
            b(i,p) = 0.d0
         end do
         b(p,p) = 1.d0

C     Calcul de la colonne p de la matrice inverse                	    
         Do i =1 , n 
            Do k = 1 , (i - 1) 
               b(i,p) = b(i,p) - bb(i,k)*b(k,p)
            end do
         end do
         Do i = n , 1 ,-1
            Do k = (i + 1) , n 
               b(i,p) = b(i,p) - bb(i,k)*b(k,p)
            end do
            b(i,p) = b(i,p)/bb(i,i)
         End do
      end do
      
C     Fin inversion         
c      print *,'Matrice inverse:'
c      Do i = 1 , n 
c         print *,(b(i,j),j=1,4)
c      End do

c      Print *,'Et produit A*A^{-1} pour verif:'
      err = 0.d0
      Do i = 1 , n 
         Do j = 1 , n 
            X  = 0.d0
            Do k = 1 , n 
               X = X + aa(i,k)*b(k,j)
            end do
            if (i.ne.j) then
               err = err + x*x 
            else
               err = err + (1.-x)*(1.-x)
            end if
         end do
      end do

      err = sqrt(err)/4.d0
      
      end
