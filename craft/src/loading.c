#define _ISOC99_SOURCE
#include <math.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <loading.h>
#include <utils.h>
#include <craft.h>
#include <meca.h>
#include <gdrmacro.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/************************************************************************/
/* Herve Moulinec
   CNRS/LMA
   september 22th 2009

   reads the file which describes the loading.

   input:
   char *filename : the name of the file describing the loading
   output:
   Loading *loading : the loading structure giving every detail on
                      loading conditions

   return value:
   0  : successful return
   -1 : file does not exist
   -2 : error while reading file

*/
/************************************************************************/
int read_load_file(
           char *filename,
           Loading *l ){


  FILE *f;
  char buffer[1024];
  char buffer2[1024];
  int i,j;
  double eq;
  int n;
  int status;

  Load_Step load_step;
  Load_Step load_step0;

  char mot[200];
  /*--------------------------------------------------------------------*/
  status=0;
  /*--------------------------------------------------------------------*/
  l->Nt=0;
  l->type=UNKNOWN_PRESCRIPTION;
  l->step=(Load_Step *)0;

  /*--------------------------------------------------------------------*/
  /* opens the file                                                     */

  f = fopen(filename,"r");

  if ( f == (FILE *)NULL ) {
    fprintf(stderr,"error in %s\n",__func__);
    fprintf(stderr,"problems encountered while opening %s\n",filename);
    return -1;
  }


  /*--------------------------------------------------------------------*/
  /* read the first line of loading file that tells:
     - if macroscopic strain has been prescribed,
     - or if macroscopic stress has been prescribed,
     - or if direction of stress has been prescribed.
  */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  status = craft_fscanf(f,"%s",buffer);
  /* macroscopic stress prescribed */
  if ( (buffer[0]=='C' ) || (buffer[0]=='c' ) ) {
    if( l->type ==  UNKNOWN_PRESCRIPTION) {
      l->type=STRESS_PRESCRIBED;
    }
    else{
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"loading prescription in %s\n",filename);
      fprintf(stderr,"has been specified more than once.\n");

      status =-2;
      return status;
    }
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* macroscopic strain prescribed */
  else if ( (buffer[0]=='D' ) || (buffer[0]=='d' ) ) {
    if( l->type ==  UNKNOWN_PRESCRIPTION) {
      l->type=STRAIN_PRESCRIBED;
    }
    else{
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"loading prescription in %s\n",filename);
      fprintf(stderr,"has been specified more than once.\n");

      status =-2;
      return status;
    }
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* direction of macroscopic stress prescribed */
  else if ( (buffer[0]=='S' ) || (buffer[0]=='s' ) ) {
    if( l->type ==  UNKNOWN_PRESCRIPTION) {
      l->type=STRESS_DIRECTION_PRESCRIBED;
    }
    else{
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"loading prescription in %s\n",filename);
      fprintf(stderr,"has been specified more than once.\n");

      status =-2;
      return status;
    }
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* macroscopic polarization prescribed */
  else if ( (buffer[0]=='P' ) || (buffer[0]=='p' ) ) {
    if( l->type ==  UNKNOWN_PRESCRIPTION) {
      l->type=POLARIZATION_PRESCRIBED;
    }
    else{
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"loading prescription in %s\n",filename);
      fprintf(stderr,"has been specified more than once.\n");

      status =-2;
      return status;
    }
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  else{
    fprintf(stderr,"Craft\n");
    fprintf(stderr,"    error in prescription of loading conditions:\n");
    fprintf(stderr,"    macroscopic condition %s is unknown\n",buffer);
    status = -2;
    return status;
  }


  /*--------------------------------------------------------------------*/
  /* reading the lines describing loading steps */
  /*--------------------------------------------------------------------*/
  l->Nt=0;
  while (fgets(buffer,1024,f)) {

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* remove any white spaces at the beginning of the line             */
    while(buffer[0] == ' '){
      memmove( buffer, buffer+1,strlen(buffer)-1);
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* empty line */
    if(strlen(buffer)==1) {
      continue;
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* commentar line */
    else if(buffer[0]=='#') {
      continue;
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* line defining multiple step: it begins either with : or with %   */
    else if( (buffer[0]==':') || (buffer[0]=='%')) {
      int separators;
      int ind_separator[3];
      int nt;
      double dt;
      /* clic stuff is now useless as storing images is assumed somewhere else now */
      /*
      int nclics;
      double dclic;
      */

      /* the line MUST contain 2 or 3 separator ':' or '%' */
      separators=0;
      for(i=0;i<strlen(buffer);i++) {
        if ( (buffer[i]==':') || (buffer[i]=='%')) {
          ind_separator[separators]=i;
          separators++;
        }
      }
      if ( (separators!=2)&&(separators!=3) ) {
        fprintf(stderr,"error in %s\n",__func__);
        fprintf(stderr,"loading prescription in %s\n",filename);
        fprintf(stderr,"incorrect line [1]: %s \n",buffer);

        status =-2;
        return status;
      }

      n = strlen(buffer)-(ind_separator[separators-1]+1);
      strncpy( buffer2 , &buffer[ind_separator[separators-1]+1] , n );
      buffer2[n]='\0';
      status=readline_load_file( buffer2, &load_step );
      if(status !=0) {
        fprintf(stderr,"error in %s\n",__func__);
        fprintf(stderr,"read error with in %s\n",filename);
        fprintf(stderr,"incorrect line [2]: %s \n",buffer);
        status =-2;
        return status;
      }

      if( l->Nt == 0 ) {
        load_step0.time=0;
        load_step0.load=0;
        for(i=0;i<6;i++) load_step0.direction[i]=0.;
        /* load_step0.clic=0; */
      }
      else {
        load_step0.time = l->step[l->Nt-1].time;
        load_step0.load = l->step[l->Nt-1].load;
        for(i=0;i<6;i++) load_step0.direction[i] = l->step[l->Nt-1].direction[i];
        /* load_step0.clic = l->step[l->Nt-1].clic; */
      }

      n = ind_separator[1]-ind_separator[0]-1;
      strncpy(buffer2 , &buffer[ind_separator[0]+1] , n );  buffer2[n]='\0';
      if (buffer[ind_separator[0]] ==':') {
        sscanf(buffer2,"%d",&nt);
        dt = (load_step.time-load_step0.time)/(double)nt;
      }
      else if (buffer[ind_separator[0]] =='%') {
        sscanf(buffer2,"%lf",&dt);
        nt = (load_step.time-load_step0.time) / dt;
      }

      /*
      n = ind_separator[2]-ind_separator[1]-1;
      strncpy( buffer2 , &buffer[ind_separator[1]+1] , n );
      buffer2[n]='\0';
      if (buffer[ind_separator[1]] ==':') {
        sscanf(buffer2,"%d",&nclics);
        dclic = nclics*dt;
      }
      else if (buffer[ind_separator[1]] =='%') {
        sscanf(buffer2,"%lf",&dclic);
      }
      */

      for(i=1; i<=nt; i++) {
        l->Nt++;
        l->step = (Load_Step *)realloc(l->step,sizeof(*(l->step)) * l->Nt);

        (l->step)[l->Nt-1].time = load_step0.time + dt*i;
        (l->step)[l->Nt-1].load =
          load_step0.load +
          (load_step.load - load_step0.load) / (load_step.time-load_step0.time) * i*dt;

        for(j=0;j<6;j++) ((l->step)[l->Nt-1]).direction[j] = load_step.direction[j];

        /* ((l->step)[l->Nt-1]).clic = (int)(i*dt / dclic) != (int)((i-1)*dt / dclic); */
      }

    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* non empty line */
    else{
      l->Nt++;
      //    printf("Nt=%d\n",l->Nt);
      l->step = (Load_Step *)realloc(l->step,sizeof(*(l->step))*l->Nt);

      status=readline_load_file( buffer, &((l->step)[l->Nt-1]) );
      if(status !=0) {
        fprintf(stderr,"error in %s\n",__func__);
        fprintf(stderr,"read error with in %s\n",filename);
        fprintf(stderr,"incorrect line [3]: %s \n",buffer);
        status =-2;
        return status;
      }
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  }


  /*--------------------------------------------------------------------*/
  fclose(f);
  /*--------------------------------------------------------------------*/
  /* differently from 1.0.5 version and former, one does not
     normalize direction of loading anymore .
     One just verifies that direction has a non null modulus.
  */

  for(i=0; i<l->Nt; i++) {
    eq = sqrt(
        (l->step)[i].direction[0] * (l->step)[i].direction[0] +
        (l->step)[i].direction[1] * (l->step)[i].direction[1] +
        (l->step)[i].direction[2] * (l->step)[i].direction[2] +
        2. * (
        (l->step)[i].direction[3] * (l->step)[i].direction[3] +
        (l->step)[i].direction[4] * (l->step)[i].direction[4] +
        (l->step)[i].direction[5] * (l->step)[i].direction[5]
        )
        );

    if (eq<TINY) {
      fprintf(stderr,"craft: invalid loading: null direction\n");
      status= -1;
      return status;
    }

    /* normalization of direction of loading, if required */
#ifdef NORMALIZATION
    for(j=0;j<6;j++) {
      (l->step)[i].direction[j] /= eq;
    }
#endif

  }

  /*--------------------------------------------------------------------*/
  /* finally we add two steps at the beginning of the load table for
     process initialization  purpose */
  if ( (l->step)[0].time <= 0. ) {
    fprintf(stderr,"error in %s\n",__func__);
    fprintf(stderr,"first step given for a time %lf <=0 \n", (l->step)[0].time );
  }
  l->Nt= l->Nt+2;
  l->step = (Load_Step *)realloc(l->step,l->Nt * sizeof(Load_Step));

  /* shift data in l to insert the two first steps */
  for(i=l->Nt-1; i>=2; i--) {
    (l->step)[i].time = (l->step)[i-2].time;
    (l->step)[i].load = (l->step)[i-2].load;
    /*    (l->step)[i].clic = (l->step)[i-2].clic; */

    for(j=0;j<6;j++) {
      (l->step)[i].direction[j] = (l->step)[i-2].direction[j];
    }
  }

  /* the two new first steps */
  (l->step)[1].time = 0.;
  (l->step)[1].load = 0.;
  for(j=0;j<6;j++) {
    (l->step)[1].direction[j]=0.;
  }

  (l->step)[0].time = -(l->step)[2].time;
  (l->step)[0].load = -(l->step)[2].load;

  for(j=0;j<6;j++) {
    (l->step)[0].direction[j] = -(l->step)[2].direction[j];
  }



  /*--------------------------------------------------------------------*/
  return status;
  /*--------------------------------------------------------------------*/
}
/************************************************************************/
/* Herve Moulinec
   CNRS/LMA
   september 29th 2009

   initializes the loading process.

   input/output:
   Loading *loading : the loading structure giving every detail on
                      loading conditions

                      In input, the structure is as it has been described
                      by the file of loading conditions.
                      In output, two steps are added
   return value:
   0  : successful return
   -1 : unsucessful return


*/
/************************************************************************/
int time_zero(Loading *l,
        double (**E)[6],
        double (**S)[6],
        int Nph,
        int nppp[Nph],
        double (**epsilon)[6],
        double (**previous_epsilon)[6],
        double E0, double nu0,
        int abinitio_flag, int it0
        ){

  int i,j;
  int iph;
  int it;

  double erreur;
  int status;
  /*--------------------------------------------------------------------*/
  status = 0;
  /*--------------------------------------------------------------------*/
  /* allocation of E and S: macroscopic strain and stress for every time step    */
  *E = (double (*)[6])malloc(l->Nt*sizeof(**E));
  *S = (double (*)[6])malloc(l->Nt*sizeof(**S));

  for (it=it0; it<l->Nt;it++) {
    for(i=0;i<6;i++) {
      (*E)[it][i]=NAN;
    }
  }
  for (it=it0; it<l->Nt;it++) {
    for(i=0;i<6;i++) {
      (*S)[it][i]=NAN;
    }
  }
  /*--------------------------------------------------------------------*/
  /* what is the "good" E for initialization of first step
     (i.e. t=l.time[2])                                                 */

  for(i=0;i<6;i++) {
      (*E)[it0+2][i]=0.;
      (*S)[it0+2][i]=0.;
  }

  i = it0+2;
  /*
  double beta=1.;
  gdrmacro_( &l->type,
             &beta,
             &E0, &nu0,
             &(l->step)[i].load,
             &(l->step)[i].direction[0], &(l->step)[i].direction[1],
             &(l->step)[i].direction[2], &(l->step)[i].direction[5],
             &(l->step)[i].direction[4], &(l->step)[i].direction[3],
             &(*E)[i][0], &(*E)[i][1], &(*E)[i][2], &(*E)[i][5], &(*E)[i][4], &(*E)[i][3],
             &(*S)[i][0], &(*S)[i][1], &(*S)[i][2], &(*S)[i][5], &(*S)[i][4], &(*S)[i][3],
             &erreur
             );
  */

  double X[6];
  double lb0, k0, mu0;
  lame(&E0, &nu0, &lb0, &mu0, &k0);
  
  compute_macro(
                l->type,
                1., 1.,
                k0, mu0, 
                (l->step)[i].load,
                (l->step)[i].direction,
                (*E)[i], (*S)[i],
                X
                );
  for(int j=0; j<6; ++j) (*E)[i][j]=X[j];
  
  if (abinitio_flag==0){
    /* We still need (in main) to retrieve macroscopic strains */
    /* for the two previous time steps (done in main after   */
    /* reading stored file and retrieving macro strains).*/

    /* Do not update epsilon and previous_epsilon */
    /* They were restored from previous computation */
    return status;
  }

  for(j=0;j<6;j++) {
    (*E)[0][j] = -(*E)[2][j];
    (*E)[1][j] = 0.;
    (*S)[1][j] = 0.;
  }
  /*--------------------------------------------------------------------*/
#ifdef DEBUG
  printf("loading.c : in time_zero(), initialization of epsilon\n");
  printf("E[0]=    ");
  for(i=0;i<6;i++) {
    printf("%f ",(*E)[0][i]);
  }
  printf("E[1]=    ");
  for(i=0;i<6;i++) {
    printf("%f ",(*E)[1][i]);
  }
  printf("E[2]=    ");
  for(i=0;i<6;i++) {
    printf("%f ",(*E)[2][i]);
  }
  printf("\n");
#endif

#pragma omp parallel for \
  default(none) \
  shared(Nph) \
  private(iph,i) \
  shared(nppp,E,epsilon)
   for(iph=0; iph<Nph; iph++) {
    for (i=0;i<nppp[iph]; i++) {
      epsilon[iph][i][0]=(*E)[1][0];
      epsilon[iph][i][1]=(*E)[1][1];
      epsilon[iph][i][2]=(*E)[1][2];
      epsilon[iph][i][3]=(*E)[1][3];
      epsilon[iph][i][4]=(*E)[1][4];
      epsilon[iph][i][5]=(*E)[1][5];
    }
  }

#ifdef DEBUG
  printf("loading.c : in time_zero(), initialization of previous epsilon\n");
#endif

#pragma omp parallel for \
  default(none) \
  shared(Nph) \
  private(iph,i) \
  shared(nppp,E,previous_epsilon)
   for(iph=0; iph<Nph; iph++) {
    for (i=0;i<nppp[iph]; i++) {
      previous_epsilon[iph][i][0]=(*E)[0][0];
      previous_epsilon[iph][i][1]=(*E)[0][1];
      previous_epsilon[iph][i][2]=(*E)[0][2];
      previous_epsilon[iph][i][3]=(*E)[0][3];
      previous_epsilon[iph][i][4]=(*E)[0][4];
      previous_epsilon[iph][i][5]=(*E)[0][5];
    }
  }
   return status;

}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/************************************************************************/
int time_zero_harmonic(Loading *l,
        double complex (**E)[6],
        double complex (**S)[6],
        int Nph,
        int nppp[Nph],
        double complex (**epsilon)[6],
        double complex (**previous_epsilon)[6],
        double E0, double nu0,
        int abinitio_flag, int it0
        ){

  int i,j;
  int iph;
  int it;

  double erreur;
  int status;
  /*--------------------------------------------------------------------*/
  status = 0;
  /*--------------------------------------------------------------------*/
  /* allocation of E and S: macroscopic strain and stress for every time step    */

  /* modified for harmonic implementation 2017/07/05  J. Boisse */
  *E = (double complex (*)[6])malloc(l->Nt*sizeof(**E));
  *S = (double complex (*)[6])malloc(l->Nt*sizeof(**S));

#ifdef DEBUG
  printf("loading.c : in time_zero_harmonic(), it0 = %d\n",it0);
#endif

  for (it=it0; it<l->Nt;it++) {
    for(i=0;i<6;i++) {
      (*E)[it][i]=NAN; /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
    }
  }
  for (it=it0; it<l->Nt;it++) {
    for(i=0;i<6;i++) {
      (*S)[it][i]=NAN; /* TO BE MODIFIED for harmonic implementation 2017/07/05  J. Boisse */
    }
  }
  /*--------------------------------------------------------------------*/
  /* what is the "good" E for initialization of first step
     (i.e. t=l.time[2])                                                 */

  for(i=0;i<6;i++) {
      (*E)[it0+2][i]=0. + 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
      (*S)[it0+2][i]=0. + 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  }

  i = it0+2;
  /*
  double beta=1.;
  gdrmacro_( &l->type,
             &beta,
             &E0, &nu0,
             &(l->step)[i].load,
             &(l->step)[i].direction[0], &(l->step)[i].direction[1],
             &(l->step)[i].direction[2], &(l->step)[i].direction[5],
             &(l->step)[i].direction[4], &(l->step)[i].direction[3],
             &(*E)[i][0], &(*E)[i][1], &(*E)[i][2], &(*E)[i][5], &(*E)[i][4], &(*E)[i][3],
             &(*S)[i][0], &(*S)[i][1], &(*S)[i][2], &(*S)[i][5], &(*S)[i][4], &(*S)[i][3],
             &erreur
             );
  */

  double complex X[6]; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  double lb0, k0, mu0;
  lame(&E0, &nu0, &lb0, &mu0, &k0);

#ifdef DEBUG
  printf("loading.c : in time_zero_harmonic(), RUN compute_macro_harmonic(), i = %d\n",i);
#endif
  
  /* modified for harmonic implementation 2017/07/05  J. Boisse */
  compute_macro_harmonic(
                l->type,
                1., 1.,
                k0, mu0, 
                (l->step)[i].load,
                (l->step)[i].direction,
                (*E)[i], (*S)[i],
                X
                );
  for(int j=0; j<6; ++j) (*E)[i][j]=X[j];
  
  if (abinitio_flag==0){
    /* We still need (in main) to retrieve macroscopic strains */
    /* for the two previous time steps (done in main after   */
    /* reading stored file and retrieving macro strains).*/

    /* Do not update epsilon and previous_epsilon */
    /* They were restored from previous computation */
    return status;
  }

  for(j=0;j<6;j++) {
    (*E)[0][j] = -(*E)[2][j];
    (*E)[1][j] = 0. + 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
    (*S)[1][j] = 0. + 0. * I; /* modified for harmonic implementation 2017/07/05  J. Boisse */
  }
  /*--------------------------------------------------------------------*/
#ifdef DEBUG
  printf("loading.c : in time_zero_harmonic(), initialization of epsilon, sigma:\n");
  printf("E[0]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*E)[0][i]),cimag((*E)[0][i]));
  }
  printf("E[1]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*E)[1][i]),cimag((*E)[1][i]));
  }
  printf("E[2]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*E)[2][i]),cimag((*E)[2][i]));
  }
  printf("S[0]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*S)[0][i]),cimag((*S)[0][i]));
  }
  printf("S[1]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*S)[1][i]),cimag((*S)[1][i]));
  }
  printf("S[2]=\n");
  for(i=0;i<6;i++) {
    printf("%f+%fi\n",creal((*S)[2][i]),cimag((*S)[2][i]));
  }
#endif

#pragma omp parallel for \
  default(none) \
  shared(Nph) \
  private(iph,i) \
  shared(nppp,E,epsilon)
   for(iph=0; iph<Nph; iph++) {
    for (i=0;i<nppp[iph]; i++) {
      epsilon[iph][i][0]=(*E)[1][0];
      epsilon[iph][i][1]=(*E)[1][1];
      epsilon[iph][i][2]=(*E)[1][2];
      epsilon[iph][i][3]=(*E)[1][3];
      epsilon[iph][i][4]=(*E)[1][4];
      epsilon[iph][i][5]=(*E)[1][5];
    }
  }

#ifdef DEBUG
  printf("loading.c : in time_zero_harmonic(), initialization of previous epsilon\n");
#endif

#pragma omp parallel for \
  default(none) \
  shared(Nph) \
  private(iph,i) \
  shared(nppp,E,previous_epsilon)
   for(iph=0; iph<Nph; iph++) {
    for (i=0;i<nppp[iph]; i++) {
      previous_epsilon[iph][i][0]=(*E)[0][0];
      previous_epsilon[iph][i][1]=(*E)[0][1];
      previous_epsilon[iph][i][2]=(*E)[0][2];
      previous_epsilon[iph][i][3]=(*E)[0][3];
      previous_epsilon[iph][i][4]=(*E)[0][4];
      previous_epsilon[iph][i][5]=(*E)[0][5];
    }
  }
   return status;

}

/************************************************************************/
int readline_load_file( char *buffer, Load_Step *ls ) {

  int n;
  int status;

  status=0;
  //  printf("buffer=%s",buffer);
  n=sscanf(buffer,
     "%lf %lf %lf %lf %lf %lf %lf %lf",
     &(ls->time),
     &(ls->direction[0]),
     &(ls->direction[1]),
     &(ls->direction[2]),
     &(ls->direction[5]),
     &(ls->direction[4]),
     &(ls->direction[3]),
     &(ls->load)
     );
#ifdef DEBUG
  printf("loading.c : in readline_load_file(), ls->time, ls->direction, ls->load:\n");
  printf(  "%lf, %lf %lf %lf %lf %lf %lf, %lf\n",
     (ls->time),
     (ls->direction[0]),
     (ls->direction[1]),
     (ls->direction[2]),
     (ls->direction[5]),
     (ls->direction[4]),
     (ls->direction[3]),
     (ls->load)
     );
#endif

  if (n<=7) {
    status =-2;
  }
  return status;
}
