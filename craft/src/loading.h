#ifndef __LOADING__
#define __LOADING__

/*------------------------------------------------------------------------------*/
#define UNKNOWN_PRESCRIPTION -1
#define STRAIN_PRESCRIBED 0
#define STRESS_PRESCRIBED 1
#define STRESS_DIRECTION_PRESCRIBED 2
#define POLARIZATION_PRESCRIBED 3

#include <complex.h>
/* => added for harmonic implementation 2017/07/05  J. Boisse */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
typedef struct{
  double time;             /* time at each step                                */
  double load;             /* load parameter                                   */
  double direction[6];     /* direction of loading at each step                */
} Load_Step;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
typedef struct{
  int type;          /* tells if:
                        macroscopic strain is prescribed (STRAIN_PRESCRIBED)
                        or if direction of macroscopic stress is prescribed (STRESS_PRESCIBED)
                        or if macroscopic stress is prescribed (STRESS_DIRECTION_PRESCRIBED) 
                        or if macroscopic polarization is prescribed (POLARIZATION_PRESCRIBED) 
                     */
  int Nt;            /* number of loading steps */
  Load_Step *step;   /* load steps of the loading */
} Loading;

/*------------------------------------------------------------------------------*/
int read_load_file(char *filename, Loading *l );

int initialization(Loading *l,
       double (**E)[6],
       double (**S)[6],
       int Nph,
       int nppp[Nph],
       double (**epsilon)[6],
       double (**previous_epsilon)[6],
       double (**sigma)[6],
       double E0, double nu0
       );

int time_zero(Loading *l,
        double (**E)[6],
        double (**S)[6],
        int Nph,
        int nppp[Nph],
        double (**epsilon)[6],
        double (**previous_epsilon)[6],
        double E0, double nu0,
        int abinitio_flag, int it
        );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int initialization_harmonic(Loading *l,
       double complex (**E)[6],
       double complex (**S)[6],
       int Nph,
       int nppp[Nph],
       double complex (**epsilon)[6],
       double complex (**previous_epsilon)[6],
       double complex (**sigma)[6],
       double E0, double nu0
       );

/* added for harmonic implementation 2017/07/05  J. Boisse */
int time_zero_harmonic(Loading *l,
        double complex (**E)[6],
        double complex (**S)[6],
        int Nph,
        int nppp[Nph],
        double complex (**epsilon)[6],
        double complex (**previous_epsilon)[6],
        double E0, double nu0,
        int abinitio_flag, int it
        );

int readline_load_file( char *buffer, Load_Step *ls );

/*------------------------------------------------------------------------------*/
#endif
