#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>

#include <craftimage.h>
#include <LE_materials.h>

#include <craft_lsp.h>
/********************************************************************************************/
int lsp( CraftImage *image, LE_param *C0, double *divs) {
  int status;
  static double *ksi1=NULL;
  static double *ksi2=NULL;
  static double *ksi3=NULL;
  static CraftImage imap = NullCraftImage;

  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"lsp error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  if (C0->isotropy != ISOTROPIC ) {
    fprintf(stderr,"lsp error: applying an anisotropic C0 is not yet implemented in lsp.\n");
    fprintf(stderr,"           Sorry!\n");
    return -1;
  }
  
  /* If problem size changed, or the frequency vectors have not been computed yet */
  if ( (cmp_size_and_type_craft_images(imap, *image)!=1) || (ksi1==NULL) || (ksi2==NULL) || (ksi3==NULL) ){
    free(ksi1);
    free(ksi2);
    free(ksi3);
    
    status = craft_fft_frequency_vectors(image, &ksi1, &ksi2, &ksi3);
    if (status!=0) return -1;
    imap = *image;
    imap.pxl.v = (void *)0;
  }

  int flag;
  double error;
  if (divs!=(double *)0) flag=1;
  else flag=0;
  lspp(
       &image->n[0], &image->n[1], &image->n[2],
       &image->nd[0], &image->nd[1], &image->nd[2],
       ksi1, ksi2, ksi3,
       &( C0->sub.ile->lb),
       &( C0->sub.ile->mu),
       image->pxl.ft2[0],
       (double complex *)0,
       &flag,
       &error
       );
  if(flag==1) *divs=error;
  return 0;
}

/* TO MODIFY for harmonic implementation 2017/07/05  J. Boisse */
/********************************************************************************************/
int lsp_harmonic( CraftImage *image, LE_param *C0, double *divs) {
  int status;
  static double *ksi1=NULL;
  static double *ksi2=NULL;
  static double *ksi3=NULL;
  static CraftImage imap = NullCraftImage;
 
//  printf(" %f %f \n", C0->sub.ile->lb, C0->sub.ile->mu);

  /* some verifications */
  if (image->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"lsp error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  if (C0->isotropy != ISOTROPIC ) {
    fprintf(stderr,"lsp error: applying an anisotropic C0 is not yet implemented in lsp.\n");
    fprintf(stderr,"           Sorry!\n");
    return -1;
  }
  
  /* If problem size changed, or the frequency vectors have not been computed yet */
  if ( (cmp_size_and_type_craft_images(imap, *image)!=1) || (ksi1==NULL) || (ksi2==NULL) || (ksi3==NULL) ){
    free(ksi1);
    free(ksi2);
    free(ksi3);
    
    status = craft_fft_frequency_vectors_harmonic(image, &ksi1, &ksi2, &ksi3);
    if (status!=0) return -1;
    imap = *image;
    imap.pxl.v = (void *)0;
  }

  int flag;
  double error;
  if (divs!=(double *)0) flag=1;
  else flag=0;
  /* modified for harmonic implementation 2017/07/05  J. Boisse */
  lspp_harmonic(
       &image->n[0], &image->n[1], &image->n[2],
       &image->nd[0], &image->nd[1], &image->nd[2],
       ksi1, ksi2, ksi3,
       &( C0->sub.ile->lb),
       &( C0->sub.ile->mu),
       image->pxl.ft2[0],
       (double complex *)0,
       &flag,
       &error
       );
  if(flag==1) *divs=error;
  return 0;
}
/********************************************************************************************/
/* similar to lsp but additionally commputes the image of the displacement */
int lsp2( CraftImage *tau, CraftImage *rotation, LE_param *C0, double *divs) {
  int status;
  static double *ksi1=NULL;
  static double *ksi2=NULL;
  static double *ksi3=NULL;
  static CraftImage imap = NullCraftImage;
 
  /* some verifications */
  if (tau->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) {
    fprintf(stderr,"lsp error: image as input is not a field of 2d o. tensors in Fourier space.\n");
    return -1;
  }
  if ( (rotation != (CraftImage *)0) && (rotation->type[0] != CRAFT_IMAGE_FOURIER_VECTOR3D) ) {
    fprintf(stderr,"lsp error: image as input is not a field of 3D vectors in Fourier space.\n");
    return -1;
  }

  if( (rotation != (CraftImage *)0) && (cmp_size_craft_images(*tau, *rotation) != 1) ) {

    fprintf(stderr, "lsp error: images of polarization field and of displacement field do not have the same format.\n");
    return -1;
  }
  
  if (C0->isotropy != ISOTROPIC ) {
    fprintf(stderr,"lsp error: applying an anisotropic C0 is not yet implemented in lsp.\n");
    fprintf(stderr,"           Sorry!\n");
    return -1;
  }
  
  /* If problem size changed, or the frequency vectors have not been computed yet */
  if ( (cmp_size_and_type_craft_images(imap, *tau)!=1) || (ksi1==NULL) || (ksi2==NULL) || (ksi3==NULL) ){
    free(ksi1);
    free(ksi2);
    free(ksi3);
    
    status = craft_fft_frequency_vectors(tau, &ksi1, &ksi2, &ksi3);
    if (status!=0) return -1;
    imap = *tau;
    imap.pxl.v = (void *)0;
  }

  int flag;
  double error;
  if (divs!=(double *)0) flag=1;
  else flag=0;
  
  double complex *r;
  if ( rotation==(CraftImage *)0 ){
    r = (double complex *)0;
  }
  else{
    r = rotation->pxl.fv3d[0];
  }
  lspp(
       &tau->n[0], &tau->n[1], &tau->n[2],
       &tau->nd[0], &tau->nd[1], &tau->nd[2],
       ksi1, ksi2, ksi3,
       &( C0->sub.ile->lb),
       &( C0->sub.ile->mu),
       tau->pxl.ft2[0],
       r,
       &flag,
       &error
       );
  if(flag==1) *divs=error;
  return 0;
}

/*******************************************************************************************************************/
/* lspp translated from Fortran 77 to C
   
   Lspp applies Gamma0 opertor on a tensor field "t"
   According to the flag entry, the error on equilibrium (as div(t)) is calculated or not.

   The rotation vector is calculated.
   It is defined as:
                [du3/dx2 - du2/dx3]
   [r] =  1/2 * [du1/dx3 - du3/dx1]
                [du2/dx1 - du1/dx2]

*/
/*******************************************************************************************************************/
void lspp(
          int *n1, int *n2, int *n3,
          int *nd1, int *nd2, int *nd3,
          double *ksi1, double *ksi2, double *ksi3,
          double *lb0, double *mu0,
          double complex *t,
          double complex *r,
          int *flag,
          double *error) {
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  int i;
  int i1,i2,i3;

  double coef1, coef2, coef3, coef4;
  double complex divt[3];

  double localerror;
  double err0;
  int ic;
  double complex tre, trs, dd;
  double xi2, xi4;
  double complex u1, u2, u3;
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  coef1 = -1. / ( 3.* (*lb0) + 2.*(*mu0) ) / 3.;
  coef2 = -0.5 / (*mu0);
  coef3 = -1. / (*mu0);
  coef4 = ((*lb0)+(*mu0)) / (*mu0) / ((*lb0)+2.*(*mu0));
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  if (*flag==1) {
    *error = 0.;
  }
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
#pragma omp parallel \
  default(none) \
  shared( n1, n2, n3, nd1, nd2, nd3) \
  shared(r,t)                        \
  shared( ksi1, ksi2, ksi3) \
  shared(coef1, coef2, coef3, coef4) \
  shared( error, flag ) \
  private(i, i1, i2, i3) \
  private( divt, ic, trs, tre, xi2, xi4, dd, u1, u2, u3, err0, localerror)
  {
    if (*flag==1) localerror=0.;
    
#pragma omp for
    for (i=0; i<(*n1/2+1)*(*n2)*(*n3); i++) {
      i3 = i / ( ((*n1)/2+1)*(*n2) ) ;
      i2 = (i - i3*((*n1)/2+1)*(*n2) ) / ((*n1)/2+1);
      i1 = i - i3*((*n1)/2+1)*(*n2) - i2*((*n1)/2+1);
      
      /*
        i1 = i1+1;
        i2 = i2+1;
        i3 = i3+1;
      */
      
      divt[0] = ksi1[i1] * t[6*i+0] + ksi2[i2]*t[6*i+5] + ksi3[i3]*t[6*i+4];
      divt[1] = ksi1[i1] * t[6*i+5] + ksi2[i2]*t[6*i+1] + ksi3[i3]*t[6*i+3];
      divt[2] = ksi1[i1] * t[6*i+4] + ksi2[i2]*t[6*i+3] + ksi3[i3]*t[6*i+2];
      
      /* error evaluation */
      if (*flag==1){
        err0 = (double)(divt[0]*conj(divt[0]) + divt[1]*conj(divt[1]) + divt[2]*conj(divt[2]));
      }
      
      /* "last frequency" cases */
      ic = 0;
      if ( (i1==(*n1)/2) && ( (*n1)%2 == 0 ) ) ic++;
      if ( (i2==(*n2)/2) && ( (*n2)%2 == 0 ) ) ic++;
      if ( (i3==(*n3)/2) && ( (*n3)%2 == 0 ) ) ic++;
      
      
      if ((*flag)==1){
        switch(ic){
        case 3:
          err0 = err0 * 0.125;
          break;
        case 2:
          err0 = err0 * 0.25;
          break;
        case 1:
          err0 = err0 * 0.5;
          break;
        }

        /* because of hermitian property, non-null frequency must be double counted (in the 1st direction) */
        if(i1 != 0){
          if ( (i1 != (*n1)/2) || ( (*n1)%2 != 0) ){
            err0 = 2. * err0;
          }
        }
        
        localerror += err0;
      }
      /*----------------------------------------------------------------------------------------------------------------------------------------------*/
      /* Applying Gamma0 */
      
      /* last frequency case */
      if(ic != 0) {
        trs = t[6*i+0] + t[6*i+1] + t[6*i+2] ;
        tre = coef1 * trs;
        trs = trs / 3.;
        
        t[6*i+0] = coef2 * (t[6*i+0]-trs) + tre;
        t[6*i+1] = coef2 * (t[6*i+1]-trs) + tre;
        t[6*i+2] = coef2 * (t[6*i+2]-trs) + tre;
        t[6*i+3] = coef2 * t[6*i+3];
        t[6*i+4] = coef2 * t[6*i+4];
        t[6*i+5] = coef2 * t[6*i+5];

        /* IT SHOULD BE IMPROVED */
        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.+0.*I;
          r[3*i+1] = 0.+0.*I;
          r[3*i+2] = 0.+0.*I;
        }
      }
      /* null frequency */
      else if ( (i1==0) && (i2==0) && (i3==0) ) {
        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.+0.*I;
          r[3*i+1] = 0.+0.*I;
          r[3*i+2] = 0.+0.*I;
        }
        t[6*i+0] = 0.+0.*I;
        t[6*i+1] = 0.+0.*I;
        t[6*i+2] = 0.+0.*I;
        t[6*i+3] = 0.+0.*I;
        t[6*i+4] = 0.+0.*I;
        t[6*i+5] = 0.+0.*I;
      }
      /* other cases (general case) */
      else{
        
        xi2 = ksi1[i1]*ksi1[i1] + ksi2[i2]*ksi2[i2] + ksi3[i3]*ksi3[i3];
        xi2 = 1./xi2;
        xi4 = xi2*xi2;
        
        dd = ksi1[i1]*divt[0] + ksi2[i2]*divt[1] + ksi3[i3]*divt[2] ;
        
        u1 = coef3*xi2*divt[0] + coef4*xi4*ksi1[i1]*dd;
        u2 = coef3*xi2*divt[1] + coef4*xi4*ksi2[i2]*dd;
        u3 = coef3*xi2*divt[2] + coef4*xi4*ksi3[i3]*dd;

        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.5*(ksi2[i2]*u3-ksi3[i3]*u2);
          r[3*i+1] = 0.5*(ksi3[i3]*u1-ksi1[i1]*u3);
          r[3*i+2] = 0.5*(ksi1[i1]*u2-ksi2[i2]*u1);
        }
        
        t[6*i+0] = ksi1[i1]*u1;
        t[6*i+1] = ksi2[i2]*u2;
        t[6*i+2] = ksi3[i3]*u3;
        t[6*i+3] = 0.5 * ( ksi2[i2]*u3 + ksi3[i3]*u2 );
        t[6*i+4] = 0.5 * ( ksi1[i1]*u3 + ksi3[i3]*u1 );
        t[6*i+5] = 0.5 * ( ksi1[i1]*u2 + ksi2[i2]*u1 );
      }
      
    }
    

    if((*flag)==1){
#pragma omp critical
      {
        *error = *error + localerror;
      }
    }
  }

}

/* TO MODIFY for harmonic implementation 2017/07/05  J. Boisse */
/*******************************************************************************************************************/
void lspp_harmonic(
          int *n1, int *n2, int *n3,
          int *nd1, int *nd2, int *nd3,
          double *ksi1, double *ksi2, double *ksi3,
          double *lb0, double *mu0,
          double complex *t,
          double complex *r,
          int *flag,
          double *error) {
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  int i;
  int i1,i2,i3;

  double coef1, coef2, coef3, coef4;
  double complex divt[3];

  double localerror;
  double err0;
  int ic;
  double complex tre, trs, dd;
  double xi2, xi4;
  double complex u1, u2, u3;
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  coef1 = -1. / ( 3.* (*lb0) + 2.*(*mu0) ) / 3.;
  coef2 = -0.5 / (*mu0);
  coef3 = -1. / (*mu0);
  coef4 = ((*lb0)+(*mu0)) / (*mu0) / ((*lb0)+2.*(*mu0));
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
  if (*flag==1) {
    *error = 0.;
  }
  /*------------------------------------------------------------------------------------------------------------------------------------------------*/
#pragma omp parallel \
  default(none) \
  shared( n1, n2, n3, nd1, nd2, nd3) \
  shared(r,t)                        \
  shared( ksi1, ksi2, ksi3) \
  shared(coef1, coef2, coef3, coef4) \
  shared( error, flag ) \
  private(i, i1, i2, i3) \
  private( divt, ic, trs, tre, xi2, xi4, dd, u1, u2, u3, err0, localerror)
  {
    if (*flag==1) localerror=0.;
    
#pragma omp for
    for (i=0; i<(*n1)*(*n2)*(*n3); i++) {
      i3 = i / ( (*n1)*(*n2) ) ;
      i2 = (i - i3*(*n1)*(*n2) ) / (*n1);
      i1 = i - i3*(*n1)*(*n2) - i2*(*n1);
      /* => mofified for harmonic implementation 2017/07/05 J. Boisse */
      
      /*
        i1 = i1+1;
        i2 = i2+1;
        i3 = i3+1;
      */
      
      divt[0] = ksi1[i1] * t[6*i+0] + ksi2[i2]*t[6*i+5] + ksi3[i3]*t[6*i+4];
      divt[1] = ksi1[i1] * t[6*i+5] + ksi2[i2]*t[6*i+1] + ksi3[i3]*t[6*i+3];
      divt[2] = ksi1[i1] * t[6*i+4] + ksi2[i2]*t[6*i+3] + ksi3[i3]*t[6*i+2];
      
      /* error evaluation */
      if (*flag==1){
        err0 = (double)(divt[0]*conj(divt[0]) + divt[1]*conj(divt[1]) + divt[2]*conj(divt[2]));
      }
      
      /* "last frequency" cases */
      ic = 0;
      if ( (i1==(*n1)/2) && ( (*n1)%2 == 0 ) ) ic++;
      if ( (i2==(*n2)/2) && ( (*n2)%2 == 0 ) ) ic++;
      if ( (i3==(*n3)/2) && ( (*n3)%2 == 0 ) ) ic++;
      
      
      if ((*flag)==1){
        switch(ic){
        case 3:
          err0 = err0 * 0.125;
          break;
        case 2:
          err0 = err0 * 0.25;
          break;
        case 1:
          err0 = err0 * 0.5;
          break;
        }

        /* mofified for harmonic implementation 2017/07/05 J. Boisse / not necessary */
        /* because of hermitian property, non-null frequency must be double counted (in the 1st direction) */
        // if(i1 != 0){
        //   if ( (i1 != (*n1)/2) || ( (*n1)%2 != 0) ){
        //     err0 = 2. * err0;
        //   }
        // }
        
        localerror += err0;
      }
      /*----------------------------------------------------------------------------------------------------------------------------------------------*/
      /* Applying Gamma0 */
      
      /* last frequency case */
      if(ic != 0) {
        trs = t[6*i+0] + t[6*i+1] + t[6*i+2] ;
        tre = coef1 * trs;
        trs = trs / 3.;
        
        t[6*i+0] = coef2 * (t[6*i+0]-trs) + tre;
        t[6*i+1] = coef2 * (t[6*i+1]-trs) + tre;
        t[6*i+2] = coef2 * (t[6*i+2]-trs) + tre;
        t[6*i+3] = coef2 * t[6*i+3];
        t[6*i+4] = coef2 * t[6*i+4];
        t[6*i+5] = coef2 * t[6*i+5];

        /* IT SHOULD BE IMPROVED */
        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.+0.*I;
          r[3*i+1] = 0.+0.*I;
          r[3*i+2] = 0.+0.*I;
        }
      }
      /* null frequency */
      else if ( (i1==0) && (i2==0) && (i3==0) ) {
        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.+0.*I;
          r[3*i+1] = 0.+0.*I;
          r[3*i+2] = 0.+0.*I;
        }
        t[6*i+0] = 0.+0.*I;
        t[6*i+1] = 0.+0.*I;
        t[6*i+2] = 0.+0.*I;
        t[6*i+3] = 0.+0.*I;
        t[6*i+4] = 0.+0.*I;
        t[6*i+5] = 0.+0.*I;
      }
      /* other cases (general case) */
      else{
        
        xi2 = ksi1[i1]*ksi1[i1] + ksi2[i2]*ksi2[i2] + ksi3[i3]*ksi3[i3];
        xi2 = 1./xi2;
        xi4 = xi2*xi2;
        
        dd = ksi1[i1]*divt[0] + ksi2[i2]*divt[1] + ksi3[i3]*divt[2] ;
        
        u1 = coef3*xi2*divt[0] + coef4*xi4*ksi1[i1]*dd;
        u2 = coef3*xi2*divt[1] + coef4*xi4*ksi2[i2]*dd;
        u3 = coef3*xi2*divt[2] + coef4*xi4*ksi3[i3]*dd;

        if( r != (double complex *)0 ) {
          r[3*i+0] = 0.5*(ksi2[i2]*u3-ksi3[i3]*u2);
          r[3*i+1] = 0.5*(ksi3[i3]*u1-ksi1[i1]*u3);
          r[3*i+2] = 0.5*(ksi1[i1]*u2-ksi2[i2]*u1);
        }
        
        t[6*i+0] = ksi1[i1]*u1;
        t[6*i+1] = ksi2[i2]*u2;
        t[6*i+2] = ksi3[i3]*u3;
        t[6*i+3] = 0.5 * ( ksi2[i2]*u3 + ksi3[i3]*u2 );
        t[6*i+4] = 0.5 * ( ksi1[i1]*u3 + ksi3[i3]*u1 );
        t[6*i+5] = 0.5 * ( ksi1[i1]*u2 + ksi2[i2]*u1 );
      }
      
    }
    

    if((*flag)==1){
#pragma omp critical
      {
        *error = *error + localerror;
      }
    }
  }

}
