#define _ISOC99_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <euler.h>


#include <variables.h>
#include <moments.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/******************************************************************************/
/* computes and stores first and second moments a given second order tensor
   in each phase of the material

   int Nph           : number of phases
   int nppp[Nph]     : number of points in each phase
   double (**x)[6]   : x[iph][ipt]  is the tensor in Voight notations 
                       of point ipt of phase iph

   char *filename    : name of the file in which to store the results
   int flag1          : flag1 = 0   -> nothing to be done
                        flag1 = 1   -> first moment to be saved
   int flag2          : flag2 = 0   -> nothing to be done
                        flag2 = 1   -> second moment to be saved


 */
/******************************************************************************/
int save_moments(
		 int Nph,
		 int nppp[Nph],
		 int phase[Nph],
		 Euler_Angles orientation[Nph],
		 double t,
		 void *x[Nph],
		 int type[3],
		 char *filename,
		 int flagopen,
		 int flag1, int flag2) {


  FILE *f;
  int status;

  int iph;
  int i,j;


  int ntp;  /* total number of pixels */

  /* number of scalars / point in x */
  int n;
  /*--------------------------------------------------------------------------------*/
  status=0;

  ntp=0;
  for(iph=0;iph<Nph;iph++) ntp += nppp[iph];

  if ( (flag1==0)&&(flag2==0)) return status;


  n = size_of_variable_value(type);

  double m1[Nph][n];
  double m2[Nph][n][n];

  /*

  double **m1, double ***m2;

  m1 = (double **)malloc(Nph*sizeof(m1));
  for(i=0;i<Nph) m1[i] = malloc(n*sizeof(*m1));

  m2 = (double **)malloc(Nph*sizeof(m2));
  for(i=0;i<Nph) m2[i] = malloc(n*sizeof(*m2));
  for(i=0;i<Nph) {
    for(j=0;j<n;j++){
      m2[i][j] = malloc(n*sizeof(*m2[i]));
    }
  }
  */

  //  printf("nom du fichier=%s\n",filename);
  if ( flag1 ) {
    for(iph=0; iph<Nph; iph++) {
      status = first_moment_of_array( nppp[iph], n, x[iph], m1[iph] );
    }
  }
  
#ifdef DEBUG
  printf("------------------------------\n");
  printf("first moment in %s\n",filename);
  for(iph=0;iph<Nph;iph++) {
    printf("----> %d ",iph);
    for(i=0; i<n;i++) {
      printf("%lf ",m1[iph][i]);
    }
    printf("\n");
  }
#endif

  if ( flag2 ) {
    for(iph=0; iph<Nph; iph++) {
      status = second_moment_of_array( nppp[iph], n, x[iph], m2[iph] );
    }
  }

  /*------------------------------------------------------------------------------------*/
  /* depending on flagopen, the file is open as new or as to be appended */
  if (flagopen) {
    f = fopen(filename,"w");
  }
  else{
    f = fopen(filename,"a");
  }

  /*--------------------------------------------------------------------------------*/
  /* two cases are managed differently:
     if the data is a 2d tensor of not */

  /*====================================================================================*/
  if (type[0]==TENSOR2) {

    if(flagopen){

      /*------------------------------------------------------------------------------------*/
      fprintf(f,"#");
      
      for(i=0;i<7;i++) for(j=0;j<16;j++) fprintf(f,"-");
      
      if ( flag1 ) {
	for(i=0;i<6;i++) for(j=0;j<16;j++) fprintf(f,"-");
      }
      
      if ( flag2 ) {
	for(i=0;i<21;i++) for(j=0;j<16;j++) fprintf(f,"-");
      }

      fprintf(f,"\n");
      
      /*------------------------------------------------------------------------------------*/
      fprintf(f,"%-15s ","# line nr ");
      fprintf(f,"%-15s ","phase nr      ");
      fprintf(f,"%15s ","volume fraction");
      fprintf(f,"%15s ","time");
      
      fprintf(f,"%15s ","phi1");
      fprintf(f,"%15s ","Phi");
      fprintf(f,"%15s ","phi2");
      
      if ( flag1 ) {
	
	fprintf(f,"%15s ","<t11>");  
	fprintf(f,"%15s ","<t22>");  
	fprintf(f,"%15s ","<t33>");  
	fprintf(f,"%15s ","<t23>");  
	fprintf(f,"%15s ","<t13>");  
	fprintf(f,"%15s ","<t12>");  
      }
      
      if ( flag2 ) {
	
	fprintf(f,"%15s ","<t11 * t11>");  
	fprintf(f,"%15s ","<t11 * t22>");  
	fprintf(f,"%15s ","<t11 * t33>");  
	fprintf(f,"%15s ","<t11 * t23>");  
	fprintf(f,"%15s ","<t11 * t13>");  
	fprintf(f,"%15s ","<t11 * t12>");  
	
	//    fprintf(f,"%15s ","<t22 * t11>");  
	fprintf(f,"%15s ","<t22 * t22>");  
	fprintf(f,"%15s ","<t22 * t33>");  
	fprintf(f,"%15s ","<t22 * t23>");  
	fprintf(f,"%15s ","<t22 * t13>");  
	fprintf(f,"%15s ","<t22 * t12>");  
	
	//    fprintf(f,"%15s ","<t33 * t11>");  
	//    fprintf(f,"%15s ","<t33 * t22>");  
	fprintf(f,"%15s ","<t33 * t33>");  
	fprintf(f,"%15s ","<t33 * t23>");  
	fprintf(f,"%15s ","<t33 * t13>");  
	fprintf(f,"%15s ","<t33 * t12>");  
	
	//    fprintf(f,"%15s ","<t23 * t11>");  
	//    fprintf(f,"%15s ","<t23 * t22>");  
	//    fprintf(f,"%15s ","<t23 * t33>");  
	fprintf(f,"%15s ","<t23 * t23>");  
	fprintf(f,"%15s ","<t23 * t13>");  
	fprintf(f,"%15s ","<t23 * t12>");  
	
	//    fprintf(f,"%15s ","<t13 * t11>");  
	//    fprintf(f,"%15s ","<t13 * t22>");  
	//    fprintf(f,"%15s ","<t13 * t33>");  
	//    fprintf(f,"%15s ","<t13 * t23>");  
	fprintf(f,"%15s ","<t13 * t13>");  
	fprintf(f,"%15s ","<t13 * t12>");  
	
	//    fprintf(f,"%15s ","<t12 * t11>");  
	//    fprintf(f,"%15s ","<t12 * t22>");  
	//    fprintf(f,"%15s ","<t12 * t33>");  
	//    fprintf(f,"%15s ","<t12 * t23>");  
	//    fprintf(f,"%15s ","<t12 * t13>");  
	fprintf(f,"%15s ","<t12 * t12>");  
      }
      
      fprintf(f,"\n");
    }  
    /*------------------------------------------------------------------------------------*/
    fprintf(f,"#");
    
    for(i=0;i<7;i++) for(j=0;j<16;j++) fprintf(f,"-");
    
    if ( flag1 ) {
      for(i=0;i<6;i++) for(j=0;j<16;j++) fprintf(f,"-");
    }
    
    if ( flag2 ) {
      for(i=0;i<21;i++) for(j=0;j<16;j++) fprintf(f,"-");
    }
    
    fprintf(f,"\n");
    
    /*------------------------------------------------------------------------------------*/

    for(iph=0; iph<Nph; iph++) {

      fprintf(f,"%-15d ",iph);
      fprintf(f,"%-15d ",phase[iph]);
      fprintf(f,"%15.8g ", (double)nppp[iph]/(double)ntp);

      fprintf(f,"%15.8g ", t);

      fprintf(f,"%15.8g ", orientation[iph].phi1);
      fprintf(f,"%15.8g ", orientation[iph].Phi);
      fprintf(f,"%15.8g ", orientation[iph].phi2);
      
      if ( flag1 ) {
	for(i=0;i<6;i++) {
	  fprintf(f,"%15.8g ",m1[iph][i]);
	}
      }

      if ( flag2 ) {
	for(i=0;i<6;i++) {
	  for(j=i;j<6;j++) {
	    fprintf(f,"%15.8g ",m2[iph][i][j]);
	  }
	}
      }

      fprintf(f,"\n");
    }

  }
  /*====================================================================================*/
  else{

    if (flagopen) {

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      fprintf(f,"#");
      
      for(i=0;i<7;i++) for(j=0;j<16;j++) fprintf(f,"-");
      
      if ( flag1 ) {
	for(i=0;i<n;i++) for(j=0;j<16;j++) fprintf(f,"-");
      }
      
      if ( flag2 ) {
	for(i=0;i<n;i++) for(j=0;j<16;j++) fprintf(f,"-");
      }
      
      fprintf(f,"\n");
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


      fprintf(f,"%-15s ","# line nr ");
      fprintf(f,"%-15s ","phase nr      ");
      fprintf(f,"%15s ","volume fraction");

      fprintf(f,"%15s ","time");

      fprintf(f,"%15s ","phi1");
      fprintf(f,"%15s ","Phi");
      fprintf(f,"%15s","phi2");


      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
      if ( flag1 ) {

	for(i=0;i<n;i++){
	  fprintf(f,"          <t%3.3d>",i+1);  
	}
      }

      if ( flag2 ) {
	for(i=0;i<n;i++) {
	  fprintf(f,"   <t%3.3d * t%3.3d>",i+1,i+1);  
	}
      }
    

      fprintf(f,"\n");
  
    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fprintf(f,"#");
    
    for(i=0;i<7;i++) for(j=0;j<16;j++) fprintf(f,"-");
    
    if ( flag1 ) {
      for(i=0;i<n;i++) for(j=0;j<16;j++) fprintf(f,"-");
    }
    
    if ( flag2 ) {
      for(i=0;i<n;i++) for(j=0;j<16;j++) fprintf(f,"-");
    }
    
    fprintf(f,"\n");
  
    /*------------------------------------------------------------------------------------*/
    for(iph=0; iph<Nph; iph++) {

      fprintf(f,"%-15d ",iph);
      fprintf(f,"%-15d ",phase[iph]);
      fprintf(f,"%15.8g ", (double)nppp[iph]/(double)ntp);

      fprintf(f,"%15.8g ", t);

      fprintf(f,"%15.8g ", orientation[iph].phi1);
      fprintf(f,"%15.8g ", orientation[iph].Phi);
      fprintf(f,"%15.8g ", orientation[iph].phi2);
      
      if ( flag1 ) {
	for(i=0;i<n;i++) {
	  fprintf(f,"%15.8g ",m1[iph][i]);
	}
      }

      if ( flag2 ) {
	for(i=0;i<n;i++) {
	  fprintf(f,"%15.8g ",m2[iph][i][i]);
	}
      }

      fprintf(f,"\n");
    }

  }
  /*====================================================================================*/


  fclose(f);
  
  return status;
}

/******************************************************************************/
/* computes the first moment of a second order in Voight notation               */
/*
  inputs:
    int N:            number of data
    double x[N][6] :  table of N second order tensor

  output:
    double m[6]    :  moment

    m = < x >
*/
/******************************************************************************/
int first_moment_of_second_order_tensor(
				    int N,
				    double x[N][6],
				    double m[6]) {
    

  int i;
  int status;
  double lm[6];


  status = 0;

  m[0]=0.;
  m[1]=0.;
  m[2]=0.;
  m[3]=0.;
  m[4]=0.;
  m[5]=0.;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel \
  default(none) \
  private (i) \
  private (lm) \
  shared(x,m,N)
  {
    
    lm[0]=0.;
    lm[1]=0.;
    lm[2]=0.;
    lm[3]=0.;
    lm[4]=0.;
    lm[5]=0.;
    
#pragma omp for
    for(i=0; i<N; i++) {
      lm[0] += x[i][0];
      lm[1] += x[i][1];
      lm[2] += x[i][2];
      lm[3] += x[i][3];
      lm[4] += x[i][4];
      lm[5] += x[i][5];
    }

#pragma omp critical
    {
      m[0] += lm[0];
      m[1] += lm[1];
      m[2] += lm[2];
      m[3] += lm[3];
      m[4] += lm[4];
      m[5] += lm[5];
    }

  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  m[0] /= (double)N;
  m[1] /= (double)N;
  m[2] /= (double)N;
  m[3] /= (double)N;
  m[4] /= (double)N;
  m[5] /= (double)N;

  return status;


}    

/******************************************************************************/
/* computes the second moment of a second order in Voight notation               */
/*
  inputs:
    int N:            number of data
    double x[N][6] :  table of N second order tensor

  output:
    double m[6][6] :  moment

    m[i][j] = < x[i] * x[j] >
*/
/******************************************************************************/
int second_moment_of_second_order_tensor(
				    int N,
				    double x[N][6],
				    double m[6][6]) {
    

  int ii,i,j;
  int status;
  double lm[6][6];


  status = 0;

  for(i=0;i<6;i++) {
    for(j=i;j<6;j++) {
      m[i][j] = 0.;
    }
  }

  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel \
  private (i,j,ii)     \
  private (lm) \
  shared(x,m)
  {
    
    for(i=0;i<6;i++) {
      for(j=i;j<6;j++) {
	lm[i][j] = 0.;
      }
    }
    
    
    
#pragma omp for
    for(ii=0; ii<N; ii++) {

      for(i=0;i<6;i++) {
	for(j=i;j<6;j++) {
	  lm[i][j] += x[ii][i]*x[ii][j];
	}
      }

    }
    
#pragma omp critical
    {
      for(i=0;i<6;i++) {
	for(j=i;j<6;j++) {
	  m[i][j] += lm[i][j];
	}
      }
    }
    

  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  for(i=0;i<6;i++) {
    for(j=i;j<6;j++) {
      m[i][j] /= (double)N;
    }
    for(j=0;j<i;j++) {
      m[i][j] = m[j][i];
    }


  }
  

  return status;


}    
    
/******************************************************************************/
/* computes the first moment of an array of scalars                             */
/*
  inputs:
    int N:            number of data
    int n:            size of array
    double x[N][n] :  table of N array of n scalars

  output:
    double m[n]    :  moment

    m = < x >
*/
/******************************************************************************/
int first_moment_of_array(
		      int N,
		      int n,
		      double x[N][n],
		      double m[n]
		      ){
  
  
  int i,j;
  int status;
  double lm[n];

  
  
  status = 0;
  
  if( x == 0 ) {
    for(j=0; j<n; j++) m[j] = NAN;
    return 0;
  }

  for(j=0;j<n;j++) {
    m[j]=0.;
  }
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel \
  shared(m,n,N)	     \
  shared(x)	     \
  private (i,j)	     \
  private (lm)	     
  {
    
    for(j=0;j<n;j++) {
      lm[j]=0.;
    }
    
#pragma omp for
    for(i=0; i<N; i++) {
      for(j=0; j<n; j++) {
	lm[j] += x[i][j];
      }
    }
    
#pragma omp critical
    {
      for(j=0;j<n;j++) {
	m[j] += lm[j];
      }
    }
    
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for(j=0;j<n;j++) {
    m[j] /= (double)N;
  }
  
  return status;
  
  
}    
/******************************************************************************/
int second_moment_of_array(
			   int N ,
			   int n ,
			   double x[N][n] ,
			   double m[n][n] ){

  int ii,i,j;
  int status;
  double lm[n][n];


  status = 0;

  if( x == 0 ) {
    for(i=0;i<n;i++) {
      for(j=i;j<n;j++) {
	m[i][j] = NAN;
      }
    }
  }

  for(i=0;i<n;i++) {
    for(j=i;j<n;j++) {
      m[i][j] = 0.;
    }
  }

  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#pragma omp parallel \
  private (i, j, ii)     \
  private (lm) \
  shared(x, m, n, N)
  {
    
    for(i=0;i<n;i++) {
      for(j=i;j<n;j++) {
	lm[i][j] = 0.;
      }
    }
    
    
    
#pragma omp for
    for(ii=0; ii<N; ii++) {

      for(i=0;i<n;i++) {
	for(j=i;j<n;j++) {
	  lm[i][j] += x[ii][i]*x[ii][j];
	}
      }

    }
    
#pragma omp critical
    {
      for(i=0;i<n;i++) {
	for(j=i;j<n;j++) {
	  m[i][j] += lm[i][j];
	}
      }
    }
    

  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  for(i=0;i<n;i++) {
    for(j=i;j<n;j++) {
      m[i][j] /= (double)N;
    }
    for(j=0;j<i;j++) {
      m[i][j] = m[j][i];
    }


  }
  

  return status;


}    
    
