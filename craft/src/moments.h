#ifndef _MOMENTS_H_
#define _MOMENTS_H_

int save_moments(
		 int Nph,
		 int nppp[Nph],
		 int phase[Nph],
		 Euler_Angles orientation[Nph],
		 double t,
		 void *x[Nph],
		 int type[3],
		 char *filename,
		 int flagopen,
		 int flag1, int flag2);

int first_moment_of_2d_order_tensor(
				    int N,
				    double x[N][6],
				    double m[6]);


int second_moment_of_2d_order_tensor(
				    int N,
				    double x[N][6],
				    double m[6][6]);

int first_moment_of_array(
		      int N,
		      int n,
		      double x[N][n],
		      double m[n]
			  );

int second_moment_of_array(
			   int N ,
			   int n ,
			   double x[N][n] ,
			   double m[n][n] );

#endif
