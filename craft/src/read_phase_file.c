#include <stdio.h>
#include <string.h>

#include <craft.h>
#include <materials.h>
/************************************************************************/
/* Herve Moulinec
   CNRS/LMA
   september 16th 2009

   reads the file which describes the phases.
   This file gives for each phase:
   - the number of the material it is made of
   - (if necessary: ) the orientation of the phase with Euler angles

   input:
   char *filename : the name of the file describing the phases
   int Nph : total number of phases
   output: 
   Euler_Angles orientation[Nph] : (crystallographic) orientation of phases
   int material[Nph] : materials the phases are composed of
  
   return value:
   0  : successful return
   -1 : file does not exist 
   -2 : error while reading file
   
*/     
/************************************************************************/
int read_phase_file( 
		    char *filename,
		    int Nph,
		    int *phase,
		    Euler_Angles *orientation,
		    int *material_in_phase ) {

  int iph;
  FILE *f;
  char buffer[1024];
  int n;
  Euler_Angles a;
  int ph,mat;

  int status;

  /*--------------------------------------------------------------------*/
  status=0;
  /*--------------------------------------------------------------------*/
  for(iph=0; iph<Nph; iph++){
    orientation[iph].phi1=0.;
    orientation[iph].Phi=0.;
    orientation[iph].phi2=0.;
  }
  for (iph=0; iph<Nph; iph++) {
    material_in_phase[iph]=UNKNOWN_MAT;
  }
  /*--------------------------------------------------------------------*/
  /* opens the file                                                     */

  f = fopen(filename,"r");

  if ( f == (FILE *)NULL ) {
    fprintf(stderr,"error in %s\n",__func__);
    fprintf(stderr,"problems encountered while opening %s\n",filename);
    return -1;
  }
  
  
  /*--------------------------------------------------------------------*/
  while (fgets(buffer,1024,f)) {
    
    /* empty line */
    if(strlen(buffer)==1) continue;
    
    /* commentar line */
    if(buffer[0]=='#') continue;

    /* a must be set to 0 in the case that no (or incomplete) orientation 
       has been entered, in that case sscanf does not fill orientation 
    */
    a.phi1=0.;     a.Phi=0.;     a.phi2=0.;
    n=sscanf(buffer,"%d %d %lf %lf %lf",&ph,&mat,&a.phi1,&a.Phi,&a.phi2);

    if(n<=1) {
	  fprintf(stderr,"error in %s\n",__func__);
	  fprintf(stderr,"read error with phase %d in %s\n",iph,filename);
	  status =-2;
    }      

    /* looks if phase ph is present table phase (i.e. : is present in   */
    /* the microstructure)                                              */
    for(iph=0; iph<Nph; iph++) {

      if( phase[iph] == ph ) {
	/* if yes, registers it */
	if ( material_in_phase[iph] == UNKNOWN_MAT) {
	  material_in_phase[iph] = mat;
	  orientation[iph]=a;
	}
	else {
	  fprintf(stderr,"error in %s\n",__func__);
	  fprintf(stderr,"phase %d has been specified twice in %s\n",iph,filename);
	  status =-2;
	}
	/* one assumes that a given index of phase can appear in phase[] 
	   just once */
	break;
      }
    }

  }
  /*--------------------------------------------------------------------*/
  fclose(f);
  /*-----------------------------------------------------------------------------*/
  /* one verifies that every USEFUL phase (i.e. non empty phase) has been 
     properly described */
  /*-----------------------------------------------------------------------------*/
  for(iph=0; iph<Nph; iph++) {
    if (material_in_phase[iph]==UNKNOWN_MAT) {
      fprintf(stderr,"error in %s\n",__func__);
      fprintf(stderr,"phase %d has not been specified in %s\n",iph,filename);
      status =-2;
    }
  }
  /*--------------------------------------------------------------------*/


  return status;
  /*--------------------------------------------------------------------*/
    
    
}
