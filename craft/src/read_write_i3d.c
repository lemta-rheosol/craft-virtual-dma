#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <craftimage.h>
#include <i3d.h>
/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   15 september 2009

   reads an image file in "i3d" format and stores it in a CraftImage

   input:
   char *name : i3d image file name

   output:
   CraftImage *cima : image structure in which the image will be stored

   The type of image one wants cima to be, has to be set in cima before calling read_i3d.

*/
/*********************************************************************************/
int  read_i3d( char *name, CraftImage *cima ){
  H2Image *ima;
  int i[3];
  int ii;
  int status=0;
  /*-----------------------------------------------------------------------------*/
  craft_image_init( cima, cima->type );

  ima = malloc(sizeof(H2Image));
  construct_h2image( ima, HIMAUNKNOWN );

  ri3d( name, (H2Image *)ima );

  for(ii=0;ii<3;ii++) {
    cima->n[ii]=ima->n[ii];
    cima->p[ii]=ima->p[ii][ii];
    cima->s[ii]=ima->s[ii];
    cima->nd[ii]=ima->nd[ii];
  }


  switch (cima->type[0]){
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_INT:
    cima->pxl.v = (void *)malloc(sizeof(int)*cima->n[0]*cima->n[1]*cima->n[2]);
    //    cima->pxl.i = (int *)cima->pxl;

    switch (ima->type){

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMADOUBLE:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((double *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAFLOAT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((float *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((unsigned int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMACHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((char *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUCHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.i[ii] = (int)((unsigned char *)ima->pxl)[ii];

          }
        }
      }
      break;
    }

    break;
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_FLOAT:
    cima->pxl.v = (void *)malloc(sizeof(float)*cima->n[0]*cima->n[1]*cima->n[2]);

    switch (ima->type){

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMADOUBLE:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((double *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAFLOAT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((float *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((unsigned int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMACHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((char *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUCHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.f[ii] = (float)((unsigned char *)ima->pxl)[ii];

          }
        }
      }
      break;
    }

    break;
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_DOUBLE:
    cima->pxl.v = (void *)malloc(sizeof(double)*cima->n[0]*cima->n[1]*cima->n[2]);

    switch (ima->type){

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMADOUBLE:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((double *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAFLOAT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((float *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUINT:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((unsigned int *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMACHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((char *)ima->pxl)[ii];

          }
        }
      }
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    case HIMAUCHAR:
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {

            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            cima->pxl.d[ii] = (double)((unsigned char *)ima->pxl)[ii];

          }
        }
      }
      break;
    }

    break;
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_TENSOR2:
    fprintf(stderr,"error in read_i3d: incompatible types of data\n");
    return -1;
    break;
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_VECTOR3D:
    fprintf(stderr,"error in read_i3d: incompatible types of data\n");
    return -1;
    break;
    /*---------------------------------------------------------------------------*/
  case CRAFT_IMAGE_VECTOR:
    fprintf(stderr,"error in read_i3d: incompatible types of data\n");
    return -1;
    break;
    /*---------------------------------------------------------------------------*/
  default:
    fprintf(stderr,"error in read_i3d: unknown type of craft image!\n");
    return -1;
    break;
  }


  free(ima->pxl);
  free(ima);

  /*-----------------------------------------------------------------------------*/
  return status;
  /*-----------------------------------------------------------------------------*/



}
/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   28 octobre 2009

   writes a CraftImage into one or several image files in "i3d" format of float scalars

   input/output:

   CraftImage *cima : image structure in which the image will be stored
   char *name : i3d image file name

   when cima is a 2d order tensor image, it is stored has 6 image files, for each
   of its componenents, that are called as specified in name followed by 11.ima, 22.ima,
   33.ima, 12.ima, 13.ima, 23.ima

   when cima is a 3D vector image, it is stored has 3 image files, for each
   of its componenents, that are called as specified in name followed by 1.ima, 2.ima, 3.ima

   when cima is aa array image, it is stored has as many image files as it has components,
   that are called as specified in name followed by 1.ima, 2.ima, 3.ima, ...


*/
/*********************************************************************************/
int  write_i3d( char *name, CraftImage *cima ){
  H2Image image;
  int i[3];
  int ii;
  int status=0;

  /*-----------------------------------------------------------------------------*/

  image.type=HIMAFLOAT;
  construct_h2image( &image , image.type);

  for(ii=0;ii<3;ii++) {
    image.p[ii][0] = image.p[ii][1] = image.p[ii][2] = 0.;
    image.p[ii][ii] = cima->p[ii];
    image.n[ii]     = cima->n[ii];
    image.s[ii]     = cima->s[ii];
    image.nd[ii]    = cima->nd[ii];
  }

  image.pxl = (void *)malloc(sizeof(float)*image.nd[0]*image.nd[1]*image.nd[2]);

  switch( cima->type[0]) {
  case CRAFT_IMAGE_INT:
    for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
      for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
        for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
          ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
          image.pxl.f[ii] = (float)(cima->pxl.i[ii]);
        }
      }
    }
    wi3d( name, &image );
    break;

  case CRAFT_IMAGE_FLOAT:
    for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
      for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
        for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
          ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
          image.pxl.f[ii] = (float)(cima->pxl.f[ii]);
        }
      }
    }
    wi3d( name, &image );
    break;

  case CRAFT_IMAGE_DOUBLE:
    for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
      for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
        for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
          ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
          image.pxl.f[ii] = (float)(cima->pxl.d[ii]);
        }
      }
    }
    wi3d( name, &image );
    break;

  case CRAFT_IMAGE_TENSOR2:
    {
      char *s[6];
      char *file;
      int j;
      s = (char *[6]){"11.ima", "22.ima", "33.ima", "23.ima", "13.ima", "12.ima"};
/*      s[0]="11.ima";*/
/*      s[1]="22.ima";*/
/*      s[2]="33.ima";*/
/*      s[3]="23.ima";*/
/*      s[4]="13.ima";*/
/*      s[5]="12.ima";*/
      for(j=0;j<6;j++) {
        file=(char *)malloc(strlen(name)+strlen(s[j])+1);
        strcpy(file,name);
        strcat(file,s[j]);
        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              image.pxl.f[ii] = (float)(cima->pxl.t2[ii][j]);
            }
          }
        }
        wi3d( file, &image );
        free(file);
      }
    }
    break;

  case CRAFT_IMAGE_VECTOR3D:
    {
      char *s[3];
      char *file;
      int j;
      s[0]="1.ima";
      s[1]="2.ima";
      s[2]="3.ima";
      for(j=0;j<3;j++) {
        file=(char *)malloc(strlen(name)+strlen(s[j])+1);
        strcpy(file,name);
        strcat(file,s[j]);
        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              image.pxl.f[ii] = (float)(cima->pxl.t2[ii][j]);
            }
          }
        }
        wi3d( file, &image );
        free(file);
      }
    }
    break;

  case CRAFT_IMAGE_VECTOR:
    {
      char *file;
      int j;
      int nt;
      nt = cima->nd[0]*cima->nd[1]*cima->nd[2];
      for(j=0; j < cima->type[1] ; j++) {
        file=(char *)malloc(strlen(name)+3+4+1);
        sprintf(file,"%s%3.3d.ima",name,j+1);
        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              image.pxl.f[ii] = (float)(cima->pxl.a[j+cima->type[1]*ii]);
            }
          }
        }
        wi3d( file, &image );
        free(file);
      }
    }
    break;

  default:
    fprintf(stderr,"%s : invalid case\n",__func__);
    fprintf(stderr,"image type : %d\n",image.type);
    status=-1;
    return status;
    break;
  }
  free(image.pxl);
  return status;
}

