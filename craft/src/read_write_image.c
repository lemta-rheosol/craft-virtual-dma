#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <craftimage.h>
#include <i3d.h>
#include <vtk.h>
#include <utils.h>
#include <math.h>
#include <complex.h>

#include <unistd.h>

#ifndef TINY
#define TINY 1.e-20
#endif

/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   15 september 2009

   reads an image file in "i3d" format or legacy VTK file format
   and stores it in a CraftImage

   input:
   char *name : image file name

   output:
   CraftImage *cima : image structure in which the image will be stored

   The type of image one wants cima to be, can be set in cima before calling read_image.

   Another feature is the possibility to preset nd and the pxl array. The
   dimensions in cima->nd are taken into account if pxl does not point to NULL.
   A sanity check is nevertheless performed to ensure the pxl array is large enough.

*/
/*********************************************************************************/
int  read_image( char *name, CraftImage *cima ){
  H2Image *ima;
  int ii;
  int status;

  if( access( name, F_OK ) == -1 ) {
    // file doesn't exist
    return -2;
  }
  
  if (cima->pxl.v==NULL)
    craft_image_init( cima, cima->type );

  switch (i3d_or_vtk_file_format( name ) ){
  case 1:
  case 2:
    /* Handling of i3d done below */
    break;
  case 3:
    /* Does not use rvtk anymore */
    status = rvtk_vectorial(name, cima);
    return status;
  default:
    fprintf(stderr, "read_image: %s has an unknown image file format.\n",name);
    return -1;
  }

  /* Construction of a H2Image image */
  ima = malloc(sizeof(H2Image));

  switch(cima->type[0]) {
  case CRAFT_IMAGE_UNKNOWN:
    ima->type = HIMAUNKNOWN ;
    break;
  case CRAFT_IMAGE_INT:
    ima->type=HIMAINT;
    break;
  case CRAFT_IMAGE_UINT:
    ima->type=HIMAUINT;
    break;
  case CRAFT_IMAGE_CHAR:
    ima->type=HIMACHAR;
    break;
  case CRAFT_IMAGE_UCHAR:
    ima->type=HIMAUCHAR;
    break;
  case CRAFT_IMAGE_FLOAT:
    ima->type=HIMAFLOAT;
    break;
  case CRAFT_IMAGE_DOUBLE:
    ima->type=HIMADOUBLE;
    break;
  }
  construct_h2image( ima, ima->type );
  /* Need to transfer the "pre-initialised" state from cima to ima */
  if (cima->pxl.v!=NULL){
    for (ii=0; ii<3; ii++) ima->nd[ii] = cima->nd[ii];
    /* Trivial allocation, just as a flag */
    ima->pxl = malloc(1);
  }

  /* reads i3d file and stores it into H2Image ima */
/*  ima->name = (char *)malloc(strlen(name)+1);*/
/*  strncpy(ima->name,name,strlen(name)+1);*/
  status = ri3d( name, (H2Image *)ima );
  /* copies H2Image ima to CraftImage cima */
  cima->name = (char *)malloc(strlen(name)+1);
  strcpy(cima->name,name);

  for(ii=0;ii<3;ii++) {
    cima->n[ii]=ima->n[ii];
    cima->p[ii]=ima->p[ii][ii];
    cima->s[ii]=ima->s[ii];
    if (cima->pxl.v==NULL) cima->nd[ii]=ima->nd[ii];
  }

  if (cima->type[0] == CRAFT_IMAGE_UNKNOWN ){
    switch(ima->type) {
    case HIMACHAR :
      cima->type[0]=CRAFT_IMAGE_CHAR;
      break;
    case HIMAUCHAR :
      cima->type[0]=CRAFT_IMAGE_UCHAR;
      break;
    case HIMAINT :
      cima->type[0]=CRAFT_IMAGE_INT;
      break;
    case HIMAUINT :
      cima->type[0]=CRAFT_IMAGE_UINT;
      break;
    case HIMAFLOAT :
      cima->type[0]=CRAFT_IMAGE_FLOAT;
      break;
    case HIMADOUBLE :
      cima->type[0]=CRAFT_IMAGE_DOUBLE;
      break;
    }
  }

  cima->type[1] = 1;
  craft_image_free(cima);
  craft_image_alloc(cima);

  /* from here, cima and ima contains data of same type
     so one can simply copy data from cima to ima using memcpy */
  memcpy( (void *)cima->pxl.v , (void *)ima->pxl ,
         data_length(ima->type)*ima->nd[0]*ima->nd[1]*ima->nd[2] );

  free(ima->pxl);
  free(ima->name);
  free(ima);
  return status;
}

/*********************************************************************************/
/* Herve Moulinec LMA/CNRS
   28 octobre 2009

   writes a CraftImage into one or several image files in "i3d" or vtk
   format of float scalars

   input/output:

   CraftImage *cima : image structure in which the image will be stored
   char *name : i3d (resp. vtk) image file name

   *Concerning the i3d format:*
   When cima is a 2d order tensor image, it is stored has 6 image files,
   for each of its components, that are called as specified in name followed by 
   11.ima, 22.ima, 33.ima, 12.ima, 13.ima, 23.ima.
   When cima is a 3D vector image, it is stored has 3 image files, 
   for each of its components, that are called as specified in name followed by 
   1.ima, 2.ima, 3.ima
   When cima is a vector image, it is stored has as many image files as it has components,
   that are called as specified in name followed by 1.ima, 2.ima, 3.ima.

   Note that write_image always stores i3d files using float data type.
   
   *Concerning the vtk format:*
   it can store vector, and tensor objects, so that this splitting is not needed.


*/
/*********************************************************************************/
int  write_image( char *name, CraftImage *cima , char *fmt){
  int i[3];
  int ii;
  int status=0;

  /*-----------------------------------------------------------------------------*/
  if ( (strcmp(fmt,"vtk") !=0 ) && (strcmp(fmt,"i3d") !=0 ) ){
    fprintf(stderr,"write_image: invalid fmt spec\n");
    return -1;
  }

  if (strcmp(fmt,"vtk")==0){
    char *file;
    file = (char *)malloc(strlen(name)+1+strlen(fmt)+1);
    sprintf(file,"%s.%s",name,fmt);
    suppress_string_in_string(file," ");
    status = wvtk_vectorial(file, cima);
    free(file);
    return status;
  }
  else{
    H2Image image;
    image.type=HIMAFLOAT;
    construct_h2image( &image , image.type);

    for(ii=0;ii<3;ii++) {
      image.n[ii]     = cima->n[ii];
      image.p[ii][0]  = image.p[ii][1] = image.p[ii][2] = 0.;
      image.p[ii][ii] = cima->p[ii];
      image.s[ii]     = cima->s[ii];
      image.nd[ii]    = cima->nd[ii];
    }

    image.pxl = (void *)malloc(sizeof(float)*image.nd[0]*image.nd[1]*image.nd[2]);

    switch( cima->type[0]) {
    case CRAFT_IMAGE_INT:
    {
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            ((float *)image.pxl)[ii] = (float)(cima->pxl.i[ii]);
          }
        }
      }

      char *file;
      file = (char *)malloc(strlen(name)+1+strlen(fmt)+1);
      sprintf(file,"%s.%s",name,fmt);
      suppress_string_in_string(file," ");
      wi3d( file, &image );
      free(file);
      break;
    }

    case CRAFT_IMAGE_FLOAT:
    {
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            ((float *)image.pxl)[ii] = (float)(cima->pxl.f[ii]);
          }
        }
      }

      char *file;
      file = (char *)malloc(strlen(name)+1+strlen(fmt)+1);
      sprintf(file,"%s.%s",name,fmt);
      suppress_string_in_string(file," ");
      wi3d( file, &image );
      free(file);
      break;
    }

    case CRAFT_IMAGE_DOUBLE:
    {
      for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
        for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
          for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
            ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
            ((float *)image.pxl)[ii] = (float)(cima->pxl.d[ii]);
          }
        }
      }

      char *file;
      file = (char *)malloc(strlen(name)+1+strlen(fmt)+1);
      sprintf(file,"%s.%s",name,fmt);
      suppress_string_in_string(file," ");
      wi3d( file, &image );
      free(file);
      break;
    }

    case CRAFT_IMAGE_TENSOR2:
    {
      char *s[6];
      char *file;
      int j;

      s[0]="11.";
      s[1]="22.";
      s[2]="33.";
      s[3]="23.";
      s[4]="13.";
      s[5]="12.";

      for(j=0;j<6;j++) {
        file=(char *)malloc(strlen(name)+strlen(s[j])+strlen(fmt)+1);
        strcpy(file,name);
        strcat(file,s[j]);
        strcat(file,fmt);

        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              ((float *)image.pxl)[ii] = (float)(cima->pxl.t2[ii][j]);
            }
          }
        }

        wi3d( file, &image );
        free(file);
      }
      break;
    }

    case CRAFT_IMAGE_VECTOR3D:
    {
      char *s[3];
      char *file;
      int j;

      s[0]="1.";
      s[1]="2.";
      s[2]="3.";

      for(j=0;j<3;j++) {
        file=(char *)malloc(strlen(name)+strlen(s[j])+strlen(fmt)+1);
        strcpy(file,name);
        strcat(file,s[j]);
        strcat(file,fmt);

        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              ((float *)image.pxl)[ii] = (float)(cima->pxl.v3d[ii][j]);
            }
          }
        }

        wi3d( file, &image );
        free(file);
      }
      break;
    }

    case CRAFT_IMAGE_VECTOR:
    {
      char *file;
      char dstr[20];
      int j,d;
      int nt;

      /* Length of string representation of components # */
      d = (cima->type[1] == 0 ? 1 : (int)(log10((double)(cima->type[1]))+1.));
      sprintf(dstr, "%%s%%0%dd.%%s", d);

      nt = cima->nd[0]*cima->nd[1]*cima->nd[2];

      for(j=0; j < cima->type[1] ; j++) {
        file=(char *)malloc(strlen(name)+d+strlen(".")+strlen(fmt)+1);
        sprintf(file, dstr, name, j+1, fmt);
        for ( i[2]=0; i[2]< cima->n[2] ; i[2]++) {
          for ( i[1]=0; i[1]< cima->n[1] ; i[1]++) {
            for ( i[0]=0; i[0]< cima->n[0] ; i[0]++) {
              ii = i[0] + cima->nd[0]*(i[1]+cima->nd[1]*i[2]);
              ((float *)image.pxl)[ii] = (float)cima->pxl.a[j+nt*ii];
            }
          }
        }

        wi3d( file, &image );
        free(file);
      }
      break;
    }

    default:
      fprintf(stderr,"%s : invalid case\n",__func__);
      fprintf(stderr,"image type : %d\n",image.type);
      status=-1;
      return status;
      break;
    }

    free(image.pxl);
  }

  return status;
}

/*******************************************************************************************/
/* Read a VTK file.
   Stored data may be a scalar or a vectorial field.
   File format may be ASCII (scalar fields only) or binary.
*/
/*******************************************************************************************/
int rvtk_vectorial(char *fname, CraftImage *cima){
  FILE *f;
  char line[256];
  int i,j,k,status, inversion_necessaire,offset,flag, n, npt,binary;
  size_t length;
  char dataname[200];
  char dt[200];
  int datatype_r; /* the required type */
  int datatype_f; /* the read type (the type of the values stored in the file */

  cima->name = (char *)malloc(strlen(fname)+1);
  strcpy(cima->name, fname);

  f = fopen(fname, "r");
  if (f==(FILE *)0) {
    fprintf(stderr,"error in rvtk_vectorial when opening %s\n", fname);
    return -1;
  }

  /* check whether the header indicate a legacy VTK file */
  fgets(line, 256, f);
  status=strncmp(line, "# vtk DataFile Version ", strlen("# vtk DataFile Version "));
  if(status!=0) {
    fprintf(stderr,"error in rvtk_vectorial:\n");
    fprintf(stderr,"%s does not seem to be a VTK legacy file\n", fname);
    return -1;
  }
  /* 2d line contains just a small description header */
  fgets(line, 256, f);

  /* tells wheter data are in ASCII or BINARY */
  fgets(line, 256, f);
  if ( strncmp(line,"BINARY",strlen("BINARY"))==0 ) {
    binary = 1;
  }
  else if ( strncmp(line,"ASCII",strlen("ASCII"))==0 ) {
    binary = 0;
  }
  else{
    fprintf(stderr,"error in rvtk_vectorial: %s not using the simple legacy VTK format.\n", fname);
    return -1;
  }

  /* dataset structure */
  fgets(line, 256, f);
  if ( strncmp(line,"DATASET STRUCTURED_POINTS",strlen("DATASET STRUCTURED_POINTS")) !=0){
    fprintf(stderr,"error in rvtk_vectorial: only accepts DATASET STRUCTURED_POINTS\n");
    fprintf(stderr,"(and not %s)\n",line);
  }
  flag=0;
  do{
    fgets(line, 256, f);
    if ( strncmp(line,"DIMENSIONS",strlen("DIMENSIONS")) == 0 ) {
      status=sscanf(line+strlen("DIMENSIONS"),"%d %d %d",
                    &(cima->n[0]), &(cima->n[1]), &(cima->n[2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk_vectorial when reading line:\n %s\n",line);
        return -1;
      }
      flag |= 1;
    }
    else if( strncmp(line,"ORIGIN",strlen("ORIGIN")) == 0 ) {
      status=sscanf(line+strlen("ORIGIN"),"%lf %lf %lf",
                    &(cima->s[0]), &(cima->s[1]), &(cima->s[2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk_vectorial when reading line:\n %s\n",line);
        return -1;
      }
      flag |= 2;
    }
    else if(strncmp(line,"SPACING",strlen("SPACING"))==0) {
      status=sscanf(line+strlen("SPACING"),"%lf %lf %lf",
                    &(cima->p[0]), &(cima->p[1]), &(cima->p[2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk_vectorial when reading line:\n %s\n",line);
        return -1;
      }
      flag |= 4;
    }
  } while ( ( strncmp(line,"POINT_DATA", strlen("POINT_DATA"))!=0 )
         && ( strncmp(line,"CELL_DATA",  strlen("CELL_DATA"))!=0  ) );

  /* dimensions has not been specified */
  if( (flag & 1)!=1 ) {
    fprintf(stderr,"error in rvtk_vectorial: no dimensions specified in %s\n", fname);
    return -1;
  }
  /* origin has not been specified */
  if( (flag & 2)!=2 ) {
    fprintf(stderr,"warning in rvtk_vectorial: no origin specified in %s \n", fname);
    fprintf(stderr,"set to 0. 0. 0.\n");
    for(i=0;i<3;i++) cima->s[i]=0.;
  }
  /* spacing has not been specified */
  if( (flag & 4)!=4 ) {
    fprintf(stderr,"warning in rvtk_vectorial: no spacing specified in %s \n", fname);
    fprintf(stderr,"set to: [[1. 0. 0.][0. 1. 0.][0. 0. 1.]] \n");
    for(i=0;i<3;i++) cima->p[i]=1.;
  }

  /* point data */
  if ( strncmp(line,"CELL_DATA", strlen("CELL_DATA"))==0 ){
    fprintf(stderr,"error in rvtk_vectorial: can not read CELL_DATA\n");
    return -1;
  }
  status=sscanf( line+strlen("POINTS_DATA"),"%d",&npt);
  if (status!=1){
    fprintf(stderr,"error in rvtk_vectorial: invalid line %s in %s", line, fname);
    return -1;
  }
  if (npt != cima->n[0]*cima->n[1]*cima->n[2] ){
    fprintf(stderr,"warning in rvtk_vectorial: non consistent number of points\n");
    fprintf(stderr,"%d vs %d\n", npt, cima->n[0]*cima->n[1]*cima->n[2]);
  }

  /* type of data (float, int, ...) */
  switch (cima->type[0]){
  case CRAFT_IMAGE_UNKNOWN:
    datatype_r = HIMAUNKNOWN;
    break;
  case CRAFT_IMAGE_CHAR:
    datatype_r = HIMACHAR;
    break;
  case CRAFT_IMAGE_UCHAR:
    datatype_r = HIMAUCHAR;
    break;
  case CRAFT_IMAGE_INT:
    datatype_r = HIMAINT;
    break;
  case CRAFT_IMAGE_UINT:
    datatype_r = HIMAUINT;
    break;
  case CRAFT_IMAGE_FLOAT:
    datatype_r = HIMAFLOAT;
    break;
  case CRAFT_IMAGE_DOUBLE:
    datatype_r = HIMADOUBLE;
    break;
  default:
    datatype_r = HIMAUNKNOWN;
    break;
  }

  if (test_endian()==LITTLEENDIAN){
    inversion_necessaire = 1;
  }else{
    inversion_necessaire = 0;
  }

  {
    fgets(line, 256, f);
    if ( strncmp(line,"SCALARS",strlen("SCALARS")) == 0 ){
      status = sscanf(line+strlen("SCALARS"),"%s %s %d", dataname, dt, &n);
      if ( status<2 ){
        fprintf(stderr,"error in rvtk_vectorial: invalid specification in %s\n",line);
        return -1;
      }
      if (status==2) n=1;
      if (n==1){
        /* True scalar field */
        if (strncmp(dt,"char",strlen("char"))==0){
          length = sizeof(char);
          datatype_f = HIMACHAR;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_CHAR;
        }
        else if (strncmp(dt,"unsigned_char",strlen("unsigned_char"))==0){
          length = sizeof(unsigned char);
          datatype_f = HIMAUCHAR;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_UCHAR;
        }
        else if (strncmp(dt,"int",strlen("int"))==0){
          length = sizeof(int);
          datatype_f = HIMAINT;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_INT;
        }
        else if (strncmp(dt,"unsigned_int",strlen("unsigned_int"))==0){
          length = sizeof(unsigned int);
          datatype_f = HIMAUINT;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_UINT;
        }
        else if (strncmp(dt,"float",strlen("float"))==0){
          length = sizeof(float);
          datatype_f = HIMAFLOAT;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_FLOAT;
        }
        else if (strncmp(dt,"double",strlen("double"))==0){
          length = sizeof(double);
          datatype_f = HIMADOUBLE;
          if (cima->type[0]==CRAFT_IMAGE_UNKNOWN)
            cima->type[0] = CRAFT_IMAGE_DOUBLE;
        }
        else{
          fprintf(stderr,"error in rvtk_vectorial: can not parse %s datatype\n", dt);
          return -1;
        }

        /* lookup table */
        fgets(line, 256, f);
        if ( strncmp(line,"LOOKUP_TABLE",strlen("LOOKUP_TABLE")) != 0 ){
          fprintf(stderr,"error in rvtk_vectorial: can not read line %s\n",line);
          return -1;
        }

        /* memory allocation if necessary */
        if ( cima->pxl.v == NULL ){
          for(i=0;i<3;i++) cima->nd[i] = cima->n[i];
        }
        cima->type[1] = 1;
        cima->type[2] = 0;
        cima->pxl.v = realloc(cima->pxl.v, length*cima->nd[0]*cima->nd[1]*cima->nd[2]);
        if ( cima->pxl.v == NULL ){
          fprintf(stderr,"error in rvtk_vectorial: can not allocate memory for pixels data\n");
          return -1;
        }
        if ( datatype_r == HIMAUNKNOWN ) datatype_r = datatype_f;
        
        if (binary==1){
          read_pixels_t( cima->n[0], cima->n[1], cima->n[2],
            datatype_f, BIGENDIAN,
            cima->nd[0], cima->nd[1], cima->nd[2],
            datatype_r, &cima->pxl.v, f);
        }
        else{ /* ASCII */
          switch(cima->type[0]){
          case CRAFT_IMAGE_DOUBLE:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%lf", cima->pxl.d+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          case CRAFT_IMAGE_FLOAT:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%f", cima->pxl.f+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          case CRAFT_IMAGE_INT:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%d", cima->pxl.i+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          case CRAFT_IMAGE_UINT:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%ud", (unsigned int *)(cima->pxl.i)+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          case CRAFT_IMAGE_CHAR:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%hhd", (char *)(cima->pxl.v)+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          case CRAFT_IMAGE_UCHAR:
            offset = 0;
            for(k=0; k<cima->n[2]; k++){
              for(j=0; j<cima->n[1]; j++){
                for(i=0; i<cima->n[0]; i++){
                  status = fscanf(f, "%hhud", (unsigned char *)(cima->pxl.v)+offset);
                  if (status!=1) return -1;
                  offset ++;
                }
                offset += (cima->nd[0]-cima->n[0]);
              }
            }
            break;
          }
        }
      }
      else{
        /* "SCALAR" appears to have several components... make it an array! */
        sprintf(line, "FIELD false_scalar 1\n array %d %d double\n", n, npt);
      }
    }

    /* Parsing vector3d */
    if ( strncmp(line,"VECTORS", strlen("VECTORS")) == 0 ){
      size_t nel, nel_read;
      double *buf;
      status = sscanf(line+strlen("VECTORS"),"%s %s", dataname, dt);
      if ( status<2 ){
        fprintf(stderr,"error in rvtk_vectorial: invalid specification in %s\n",line);
        return -1;
      }
      if ( strncmp(dt, "double", strlen("double")) != 0 ){
        fprintf(stderr, "error in rvtk_vectorial: datatype %s not handled for vectors.", dt);
        return -1;
      }
      /* Reallocate vector3d pointers */

      /* memory allocation if necessary */
      if ( cima->pxl.v == NULL ){
        for(i=0;i<3;i++) cima->nd[i] = cima->n[i];
        length = cima->nd[0]*cima->nd[1]*cima->nd[2]*3*sizeof(double);
        cima->pxl.v = malloc(length);
      }
      else{
        length = cima->nd[0]*cima->nd[1]*cima->nd[2]*3*sizeof(double);
        cima->pxl.v = realloc(cima->pxl.v, length);
      }

      nel = 3*cima->n[0];
      buf = malloc(nel*sizeof(double));
      for(k=0; k<cima->nd[2]; k++){
        for(j=0; j<cima->nd[1]; j++){
          offset = cima->nd[0]*(j+cima->nd[1]*k);
          nel_read = fread(buf, sizeof(double), nel, f);
          if (nel!=nel_read){
            fprintf(stderr, "Failed to read at (j=%d, k=%d).\n", j, k);
            exit(0);
          }
          if (inversion_necessaire)
            invpds2(buf, buf, sizeof(double), nel);
          memcpy(cima->pxl.v3d[offset], buf, sizeof(double)*nel);
        /*for(i=0; i<cima->n[0]; i++){
            cima->pxl.v3d[i+offset][0] = buf[3*i];
            cima->pxl.v3d[i+offset][1] = buf[3*i+1];
            cima->pxl.v3d[i+offset][2] = buf[3*i+2];
          }*/
        }
      }
      cima->type[0] = CRAFT_IMAGE_VECTOR3D;
      cima->type[1] = 3;
      cima->type[2] = 0;
    }
    /* Parsing tensor2 */
    if ( strncmp(line,"TEXTURE_COORDINATES tensor2",
               strlen("TEXTURE_COORDINATES tensor2")) == 0 ){
      size_t nel, nel_read;
      int n1;
      double *buf;
      status = sscanf(line+strlen("TEXTURE_COORDINATES tensor2"),"%d %s", &n1, dt);
      if ( status<2 ){
        fprintf(stderr,"error in rvtk_vectorial: invalid specification in %s\n",line);
        return -1;
      }
      if ( strncmp(dt, "double", strlen("double")) != 0 ){
        fprintf(stderr, "error in rvtk_vectorial: datatype %s not handled for vectors.", dt);
        return -1;
      }
      if (n1==6){
        /* Reallocate tensor pointers */
        /* memory allocation if necessary */
        if ( cima->pxl.v == NULL ){
          for(i=0;i<3;i++) cima->nd[i] = cima->n[i];
          length = cima->nd[0]*cima->nd[1]*cima->nd[2]*6*sizeof(double);
          cima->pxl.v = malloc(length);
        }
        else{
          length = cima->nd[0]*cima->nd[1]*cima->nd[2]*6*sizeof(double);
          cima->pxl.v = realloc(cima->pxl.v, length);
        }

        nel = 6*cima->n[0];
        buf = malloc(nel*sizeof(double));
        for(k=0; k<cima->nd[2]; k++){
          for(j=0; j<cima->nd[1]; j++){
            offset = cima->nd[0]*(j+cima->nd[1]*k);
            nel_read = fread(buf, sizeof(double), nel, f);
            if (nel!=nel_read){
              fprintf(stderr, "Failed to read at (j=%d, k=%d).\n", j, k);
              exit(0);
            }
            if (inversion_necessaire)
              invpds2(buf, buf, sizeof(double), nel);
            memcpy(cima->pxl.t2+offset, buf, sizeof(double)*nel);
          /*for(i=0; i<cima->n[0]; i++){
              cima->pxl.t2[i+offset][0] = buf[6*i];
              cima->pxl.t2[i+offset][1] = buf[6*i+1];
              cima->pxl.t2[i+offset][2] = buf[6*i+2];
              cima->pxl.t2[i+offset][3] = buf[6*i+3];
              cima->pxl.t2[i+offset][4] = buf[6*i+4];
              cima->pxl.t2[i+offset][5] = buf[6*i+5];
            }*/
          }
        }
        cima->type[0] = CRAFT_IMAGE_TENSOR2;
        cima->type[1] = 6;
        cima->type[2] = 0;
      }
      else{
        sprintf(line, "TEXTURE_COORDINATES vector %d %s", n1, dt);
      }
    }

    /* Parsing vector */
    if ( strncmp(line,"TEXTURE_COORDINATES vector",
               strlen("TEXTURE_COORDINATES vector")) == 0 ){
      int ncomp;
      size_t nel, nel_read;
      double *buf;
      status = sscanf(line+strlen("TEXTURE_COORDINATES vector"),"%d %s", &ncomp, dt);
      if ( status<2 ){
        fprintf(stderr,"error in rvtk_vectorial: invalid specification in %s\n",line);
        return -1;
      }
      if ( strncmp(dt, "double", strlen("double")) != 0 ){
        fprintf(stderr, "error in rvtk_vectorial: datatype %s not handled for vectors.", dt);
        return -1;
      }
      cima->type[0] = CRAFT_IMAGE_VECTOR;
      cima->type[1] = ncomp;
      cima->type[2] = 0;

      if ( cima->pxl.v == NULL ){
          for(i=0;i<3;i++) cima->nd[i] = cima->n[i];
          length = cima->nd[0]*cima->nd[1]*cima->nd[2]*cima->type[1]*sizeof(double);
          cima->pxl.a = malloc(length);
        }
        else{
          length = cima->nd[0]*cima->nd[1]*cima->nd[2]*cima->type[1]*sizeof(double);
          cima->pxl.a = realloc(cima->pxl.a, length);
        }

      nel = ncomp*cima->n[0];
      buf = malloc(nel*sizeof(double));
      for(k=0; k<cima->nd[2]; k++){
        for(j=0; j<cima->nd[1]; j++){
          offset = cima->nd[0]*(j+cima->nd[1]*k);
          nel_read = fread(buf, sizeof(double), nel, f);
          if (nel!=nel_read){
            fprintf(stderr, "Failed to read at (j=%d, k=%d).\n", j, k);
            exit(0);
          }
          if (inversion_necessaire)
            invpds2(buf, buf, sizeof(double), nel);
          memcpy(cima->pxl.a+ncomp*offset, buf, sizeof(double)*nel);
        /*for(i=0; i<cima->n[0]; i++){
            for(icomp=0; icomp<ncomp; icomp++){
              cima->pxl.a[icomp+ncomp*(i+offset)] = buf[icomp+ncomp*i];
            }
          }*/
        }
      }
    }
  }

  /*----------------------------------------------------------------------------*/
  fclose(f);
  return 0;
}

/*********************************************************************************/
int wvtk_vectorial(char *fname, CraftImage *cima){
  FILE *f;
  void *buf;
  size_t nel, nel2;
  int j,k, Npt, endianness, inversion_necessaire;
  size_t length;
  int offset;
  int ncomp;
  double *dbuf;
  int i, icomp;

  f = fopen(fname, "w");
  if (f==(FILE *)0) {
    fprintf(stderr,"error in wvtk_vectorial when opening %s\n", fname);
    return -1;
  }
  cima->name = realloc(cima->name, strlen(fname)+1);
  if (cima->name==NULL){
    fprintf(stderr,"error in wvtk_vectorial when allocating filename.\n");
    return -1;
  }
  strcpy(cima->name, fname);

  /* Header */
  fprintf(f,"# vtk DataFile Version 3.0\n");
  fprintf(f,"craft output\n");
  fprintf(f,"BINARY\n");
  fprintf(f,"DATASET STRUCTURED_POINTS\n");
  fprintf(f,"DIMENSIONS %d %d %d\n",cima->n[0],cima->n[1],cima->n[2]);
  fprintf(f,"ORIGIN %lf %lf %lf\n",cima->s[0],cima->s[1],cima->s[2]);
  fprintf(f,"SPACING  %lf %lf %lf\n",cima->p[0],cima->p[1],cima->p[2]);
  Npt = cima->n[0]*cima->n[1]*cima->n[2];
  fprintf(f,"POINT_DATA %d\n", Npt);

  switch(cima->type[0]){
  case CRAFT_IMAGE_CHAR:
    fprintf(f,"SCALARS scalars char\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(char);
    break;
  case CRAFT_IMAGE_UCHAR:
    fprintf(f,"SCALARS scalars unsigned_char\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(unsigned char);
    break;
  case CRAFT_IMAGE_INT:
    fprintf(f,"SCALARS scalars int\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(int);
    break;
  case CRAFT_IMAGE_UINT:
    fprintf(f,"SCALARS scalars unsigned_int\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(unsigned int);
    break;
  case CRAFT_IMAGE_FLOAT:
    fprintf(f,"SCALARS scalars float\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(float);
    break;
  case CRAFT_IMAGE_DOUBLE:
    fprintf(f,"SCALARS scalars double\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(double);
    break;
  case CRAFT_IMAGE_TENSOR2:
    fprintf(f,"TEXTURE_COORDINATES tensor2 6 double\n");
    length = 6*sizeof(double);
    break;
  case CRAFT_IMAGE_VECTOR3D:
    fprintf(f,"VECTORS vector3d double\n");
    length = 3*sizeof(double);
    break;
  case CRAFT_IMAGE_VECTOR:
    fprintf(f,"TEXTURE_COORDINATES vector %d double\n", cima->type[1]);
    length = cima->type[1]*sizeof(double);
    break;
  case CRAFT_IMAGE_FOURIER_DOUBLE:
    fprintf(f,"SCALARS scalars fourier_double 2\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(double complex);
    break;
  case CRAFT_IMAGE_FOURIER_TENSOR2:
    fprintf(f,"TEXTURE_COORDINATES fourier_tensor2 12 double\n");
    length = 6*sizeof(double complex);
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR3D:
    fprintf(f,"TEXTURE_COORDINATES fourier_vector3d 6 double\n");
    length = 3*sizeof(double);
    break;
  case CRAFT_IMAGE_FOURIER_VECTOR:
    fprintf(f,"TEXTURE_COORDINATES fourier_vector %d double\n", 2*cima->type[1]);
    length = cima->type[1]*sizeof(double);
    break;
  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
    fprintf(f,"SCALARS scalars harmonic_double 2\n");
    fprintf(f,"LOOKUP_TABLE default\n");
    length = sizeof(double complex);
    break;
  case CRAFT_IMAGE_HARMONIC_TENSOR2_CREAL: /* added for harmonic implementation 2017/07/05  J. Boisse */
    fprintf(f,"TEXTURE_COORDINATES tensor2 6 double\n");
    length = 6*sizeof(double);
    break;
  case CRAFT_IMAGE_HARMONIC_TENSOR2_CIMAG: /* added for harmonic implementation 2017/07/05  J. Boisse */
    fprintf(f,"TEXTURE_COORDINATES tensor2 6 double\n");
    length = 6*sizeof(double);
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    fprintf(f,"TEXTURE_COORDINATES harmonic_vector3d 6 double\n");
    length = 3*sizeof(double);
    break;
  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    fprintf(f,"TEXTURE_COORDINATES harmonic_vector %d double\n", 2*cima->type[1]);
    length = cima->type[1]*sizeof(double);
    break;
  default:
    fprintf(stderr, "wvtk_vectorial: Unable to understand CraftImage type (code %d).\n", cima->type[0]);
    return -1;
  }


  /* VTK legacy format requires big endian data encoding, so :*/
  endianness=test_endian();
  if ( (endianness==LITTLEENDIAN) && (length>1) ){
      inversion_necessaire=1;
  }
  else if(endianness==BIGENDIAN){
    inversion_necessaire=0;
  }
  else{
    fprintf(stderr,"error in wvtk with endianness\n");
    return -1;
  }

  buf = malloc(length*cima->n[0]);
  switch(cima->type[0]){
  case CRAFT_IMAGE_CHAR:
  case CRAFT_IMAGE_UCHAR:
  case CRAFT_IMAGE_INT:
  case CRAFT_IMAGE_UINT:
  case CRAFT_IMAGE_FLOAT:
  case CRAFT_IMAGE_DOUBLE:
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = length * cima->nd[0] * ( j+cima->nd[1]*k );
        memcpy(buf, (char *)(cima->pxl.v)+offset, cima->n[0]*length);
        if (inversion_necessaire){
          invpds2(buf, buf, length, cima->n[0]);
        }
        fwrite(buf, length, cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_FOURIER_DOUBLE:
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = length * cima->nd[0] * ( j+cima->nd[1]*k );
        memcpy(buf, cima->pxl.fd+offset, cima->n[0]*length);
        if (inversion_necessaire){
          invpds2(buf, buf, length, cima->n[0]);
        }
        fwrite(buf, length, cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_HARMONIC_DOUBLE: /* added for harmonic implementation 2017/07/05  J. Boisse */
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = length * cima->nd[0] * ( j+cima->nd[1]*k );
        memcpy(buf, cima->pxl.hd+offset, cima->n[0]*length);
        if (inversion_necessaire){
          invpds2(buf, buf, length, cima->n[0]);
        }
        fwrite(buf, length, cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_TENSOR2:
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.t2+offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<6; icomp++){
            dbuf[icomp+6*i] = cima->pxl.t2[i+offset][icomp];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 6*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 6*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_FOURIER_TENSOR2:
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.ft2+offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<6; icomp++){
            dbuf[icomp+6*i] = cima->pxl.t2[i+offset][icomp];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double ), 12*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 12*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_HARMONIC_TENSOR2_CREAL: /* added for harmonic implementation 2017/10/03  J. Boisse */
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<6; icomp++){
            dbuf[icomp+6*i] = creal(cima->pxl.ht2[i+offset][icomp]);
          }
        }
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 6*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 6*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_HARMONIC_TENSOR2_CIMAG: /* added for harmonic implementation 2017/10/03  J. Boisse */
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<6; icomp++){
            dbuf[icomp+6*i] = cimag(cima->pxl.ht2[i+offset][icomp]);
          }
        }
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 6*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 6*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_VECTOR3D:
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.v3d+offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<3; icomp++){
            dbuf[icomp+3*i] = cima->pxl.v3d[i+offset][icomp];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 3*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 3*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_FOURIER_VECTOR3D:
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.fv3d+offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<3; icomp++){
            dbuf[icomp+3*i] = cima->pxl.v3d[i+offset][icomp];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 3*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 3*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_HARMONIC_VECTOR3D: /* added for harmonic implementation 2017/07/05  J. Boisse */
    dbuf = (double *)buf;
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.hv3d+offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<3; icomp++){
            dbuf[icomp+3*i] = cima->pxl.hv3d[i+offset][icomp];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), 3*cima->n[0]);
        }
        fwrite(buf, sizeof(double), 3*cima->n[0], f);
      }
    }
    break;

  case CRAFT_IMAGE_VECTOR:
    dbuf = (double *)buf;
    ncomp = cima->type[1];
    Npt = cima->nd[0]*cima->nd[1]*cima->nd[2];
    nel = ncomp*cima->n[0];
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.a+ncomp*offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<ncomp; icomp++){
            dbuf[icomp+ncomp*i] = cima->pxl.a[icomp+ncomp*(i+offset)];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), nel);
        }
        nel2 = fwrite(buf, sizeof(double), nel, f);
        if (nel!=nel2){
          fprintf(stderr, "Failed to write at (j=%d, k=%d).\n", j, k);
        }
      }
    }
    break;

  case CRAFT_IMAGE_FOURIER_VECTOR:
    dbuf = (double *)buf;
    ncomp = cima->type[1];
    Npt = cima->nd[0]*cima->nd[1]*cima->nd[2];
    nel = ncomp*cima->n[0];
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.fa+ncomp*offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<ncomp; icomp++){
            dbuf[icomp+ncomp*i] = cima->pxl.a[icomp+ncomp*(i+offset)];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), nel);
        }
        nel2 = fwrite(buf, sizeof(double), nel, f);
        if (nel!=nel2){
          fprintf(stderr, "Failed to write at (j=%d, k=%d).\n", j, k);
        }
      }
    }
    break;

  case CRAFT_IMAGE_HARMONIC_VECTOR: /* added for harmonic implementation 2017/07/05  J. Boisse */
    dbuf = (double *)buf;
    ncomp = cima->type[1];
    Npt = cima->nd[0]*cima->nd[1]*cima->nd[2];
    nel = ncomp*cima->n[0];
    for(k=0; k<cima->n[2]; k++) {
      for(j=0; j<cima->n[1]; j++) {
        offset = cima->nd[0]*(j+cima->nd[1]*k);
        memcpy(dbuf, cima->pxl.ha+ncomp*offset, length*cima->n[0]);
      /*for(i=0; i<cima->n[0]; i++){
          for(icomp=0; icomp<ncomp; icomp++){
            dbuf[icomp+ncomp*i] = cima->pxl.ha[icomp+ncomp*(i+offset)];
          }
        }*/
        if (inversion_necessaire){
          invpds2(buf, buf, sizeof(double), nel);
        }
        nel2 = fwrite(buf, sizeof(double), nel, f);
        if (nel!=nel2){
          fprintf(stderr, "Failed to write at (j=%d, k=%d).\n", j, k);
        }
      }
    }
    break;

  }
  free(buf);
  fclose(f);
  return 0;
}
