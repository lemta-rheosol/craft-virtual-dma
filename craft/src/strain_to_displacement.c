#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <complex.h>
#include "craftimage.h"

static int strain_to_displacement(CraftImage *strain, CraftImage *displacement, double *ksi1, double *ksi2, double *ksi3, double eps) {
  int i1,i2,i3,j,offset1;
  double k1,k2,k3,mksi,mstrain,relerr;
  double complex coef1,coef2;
  double complex (*elfout)[3];
  double complex (*elfin)[6];
  double complex tmp[6];

  if (strain->type[0] != CRAFT_IMAGE_FOURIER_TENSOR2) return -1;
  if (displacement->type[0] != CRAFT_IMAGE_FOURIER_VECTOR3D) return -1;

 /* Computing the energy of the strain field (signal theory definition) */
  mstrain = 0.;

  for (i3 = 0; i3 < strain->nd[2];i3++) {
    for (i2 = 0; i2 < strain->nd[1];i2++) {
      for (i1 = 0; i1 < strain->nd[0];i1++) {
        offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
        elfin = strain->pxl.ft2 + offset1;
        coef1 = (*elfin)[0]*conj((*elfin)[0]) + (*elfin)[1]*conj((*elfin)[1]) + (*elfin)[2]*conj((*elfin)[2])
            +2*((*elfin)[3]*conj((*elfin)[3]) + (*elfin)[4]*conj((*elfin)[4]) + (*elfin)[5]*conj((*elfin)[5]));
        mstrain += coef1;
      }
    }
  }

  mstrain = sqrt(mstrain / (strain->nd[0]*strain->nd[1]*strain->nd[2]) );

  /* Checking compatibility equations (in the frequency domain) */
  for (i3 = 0; i3 < strain->nd[2];i3++) {
    for (i2 = 0; i2 < strain->nd[1];i2++) {
      for (i1 = 0; i1 < strain->nd[0];i1++) {
        offset1 = i1 + strain->nd[0] * (i2 + strain->nd[1] * i3);
        elfin = (strain->pxl.ft2 + offset1);
        k1 = ksi1[i1];
        k2 = ksi2[i2];
        k3 = ksi3[i3];

        /* tolerance eps * |ksi|^2 * |strain(ksi=0)| */
        mksi = k1 * k1 + k2 * k2 + k3 * k3;
        relerr = ((eps * mstrain) * mksi);

        /* Strain is known to be incompatible at the Nyquist frequency
           for each axes which has an even number of pixels
           (TODO : special handle of Nyquist frequency in lspp may not be the best) */
        if ((i1==strain->nd[0]-1) && (strain->n[0]>1) && (strain->n[0]%2 == 0))
          continue;
        if ((i2==strain->nd[1]/2) && (strain->n[1]>1) && (strain->n[1]%2 == 0))
          continue;
        if ((i3==strain->nd[2]/2) && (strain->n[2]>1) && (strain->n[2]%2 == 0))
          continue;
        
        /* These 6 expressions should be "equal" to zero to ensure compatibility.
           They are redundant, but no subset is sufficient for all cases. */
        tmp[0] = k2*k2*(*elfin)[0] + k1*k1*(*elfin)[1] - 2*k1*k2*(*elfin)[5]; /* 12 */
        tmp[1] = k3*k3*(*elfin)[1] + k2*k2*(*elfin)[2] - 2*k2*k3*(*elfin)[3]; /* 23 */
        tmp[2] = k1*k1*(*elfin)[2] + k3*k3*(*elfin)[0] - 2*k3*k1*(*elfin)[4]; /* 13 */
        tmp[3] = k2*k3*(*elfin)[0] - k1*(k2*(*elfin)[4] + k3*(*elfin)[5] - k1*(*elfin)[3]); /* 11 */
        tmp[4] = k3*k1*(*elfin)[1] - k2*(k3*(*elfin)[5] + k1*(*elfin)[3] - k2*(*elfin)[4]); /* 22 */
        tmp[5] = k1*k2*(*elfin)[2] - k3*(k1*(*elfin)[3] + k2*(*elfin)[4] - k3*(*elfin)[5]); /* 33 */

        for (j = 0; j < 6; j++) {
          if (cabs(tmp[j]) > relerr) {
            fprintf(stderr, 
              "Strain field incompatible (%d) for frequency (%d,%d,%d): %.4e>%.4e.",
              j,i1,i2,i3, abs(tmp[j])/(mstrain*mksi), eps);
          }
        }
      }
    }
  }

  /* Computing displacement field */
  coef1 = 0. - I*1./(2.*M_PI);
  coef2 = 0. - I*1./M_PI;
  
  for (i3 = 0; i3 < displacement->nd[2]; i3++) {
    for (i2 = 0; i2 < displacement->nd[1]; i2++) {
      for (i1 = 0; i1 < displacement->nd[0]; i1++) {
        offset1 = i1 + strain->nd[0] * ( i2 + strain->nd[1] * i3 );
        elfin = strain->pxl.ft2 + offset1;
        elfout = displacement->pxl.fv3d + offset1;
        k1 = ksi1[i1];
        k2 = ksi2[i2];
        k3 = ksi3[i3];

        /* Component 1 */
        if (i1!=0){
          (*elfout)[0] = (*elfin)[0]*(coef1/k1); /* from eps_11 */
        }
        else if (i2!=0){
          (*elfout)[0] = (*elfin)[5]*(coef2/k2); /* from eps_12 */
        }
        else if (i3!=0){
          (*elfout)[0] = (*elfin)[4]*(coef2/k3); /* from eps_13 */
        }
        else{
          (*elfout)[0] = 0.; /* Zero frequency : solid body motion */
        }

        /* Component 2 */
        if (i2!=0){
          (*elfout)[1] = (*elfin)[1]*coef1/k2; /* from eps_22 */
        }
        else if (i3!=0){
          (*elfout)[1] = (*elfin)[3]*coef2/k3; /* from eps_23 */
        }
        else if(i1!=0){
          (*elfout)[1] = (*elfin)[5]*coef2/k1; /* from eps_12 */
        }
        else{
          (*elfout)[1] = 0.; /* Zero frequency : solid body motion */
        }

        /* Component 3 */
        if (i3!=0){
          (*elfout)[2] = (*elfin)[2]*coef1/k3; /* from eps_33 */
        }
        else if (i1!=0){
          (*elfout)[2] = (*elfin)[4]*coef2/k1; /* from eps_13 */
        }
        else if(i2!=0){
          (*elfout)[2] = (*elfin)[3]*coef2/k2; /* from eps_23 */
        }
        else{
          (*elfout)[2] = 0.; /* Zero frequency : solid body motion */
        }
      }
    }
  }
  
  return 0;
}
  
