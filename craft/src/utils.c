#define _DEFAULT_SOURCE
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <stdarg.h>




#include <utils.h>

#include <complex.h>
/* => added for harmonic implementation 2017/07/05  J. Boisse */

/****************************************************************************/
/* parse a sentence in words, n gives the number of words found in sentence */
char **parse_sentence( char *sentence, int *n){
  int i;

  char **words=(char **)0;
  
  clean_string(sentence);
  
  char *ptr1, *ptr2;
  ptr1 = sentence;

  i=0;
  int goon=1;
  while( goon ) {
    int length;

    ptr2=strstr(ptr1," ");    
    if(ptr2==(char *)0){
      length=strlen( ptr1 );
      if(length==0){
        return words;
      }
      goon=0;
    }
    else{
      length=ptr2-ptr1;
    }
    
    words = realloc(words, (i+1)*sizeof(*words));
    words[i]=malloc( length+1 );
    strncpy(words[i],ptr1, length);
    ptr1 = ptr2+1;
    i++;
  }

  *n=i;
  return words;
}
/********************************************************************************************/
/* returns current date in a string formatted as 
   YYYY_MM_DD_HH:MN:SS
*/

char *cdate(int flag){
  
  struct tm *t;
  struct timeval tv;
  struct timezone tz;
  // void *tz;
  
  char *s;

  gettimeofday(&tv, &tz);
  //  gettimeofday(&tv, tz);
  t=localtime(&(tv.tv_sec));

  if (flag==0) {
    s = (char *)malloc(4+2+2+2+2+2+6);
    sprintf(s,"%4.4d_%2.2d_%2.2d_%2.2d:%2.2d.%2.2d",
	    t->tm_year+1900,
	    t->tm_mon+1,
	    t->tm_mday,
	    t->tm_hour,
	    t->tm_min,
	    t->tm_sec);
  }
  else {
    s = (char *)malloc(4+2+2+2+2+2+6);
    sprintf(s,"%4.4d %2.2d %2.2d %2.2d:%2.2d.%2.2d",
	    t->tm_year+1900,
	    t->tm_mon+1,
	    t->tm_mday,
	    t->tm_hour,
	    t->tm_min,
	    t->tm_sec);
  }
  return s;
}
  
#define _ISOC99_SOURCE
/*************************************************************************************/
/* 
   HM 7th january 2010

   craft_fscanf

   this function works like fscanf except that:
   * it skips empty lines 
   * it skips comments ( # character and all characters following in the line)

   */
/*************************************************************************************/
int craft_fscanf(FILE *f, const char *format, ...){

  va_list ap;
  int status;
  int c;
  int goon;

  /*---------------------------------------------------------------------------------*/
  va_start(ap, format );
  /*---------------------------------------------------------------------------------*/
  /* skips empty lines and comments */

  do{
    goon=0;
    c=getc(f);

    if (c==' ') goon=1; /* white space are ingnored */

    if (c==13) goon=1; /* carriage control characters are ignored */

    if (c=='\n') goon=1; /* line feed characters are ignored */

    if (c=='#') {
      goon=1;
      do {
        c = getc(f);
        if(c==EOF) {
          goon=0;
          break;
        }
      } while(c!='\n');

    }
  } while(goon);

  ungetc(c,f);

  
  /*---------------------------------------------------------------------------------*/
  //  status=sscanf(buffer, format, p[0], p[1], p[2]);
  //  printf("les p = %d %d %d %d\n",p[0],*p[0],*p[1],*p[2]);

  status=vfscanf(f,format, ap );
  va_end(ap);
  /*---------------------------------------------------------------------------------*/
  return status;



}
/*************************************************************************************/
/* 
   HM 21th october 2010

   craft_fgets

   this function works like fgets except that:
   * it skips empty lines 
   * it skips comments ( # character and all characters following in the line)

   */
/*************************************************************************************/
char *craft_fgets(char *s, int size, FILE *stream) {

  int c;
  int goon;
  int c2;

  /*---------------------------------------------------------------------------------*/
  /* skips empty lines and comments */

  do{
    goon=0;
    c=getc(stream);
    if (c==' ') goon=1;
    if (c=='\n') goon=1;
    if (c=='#') {
      do {
	c2=getc(stream);
      } while( (c2!='\n') && (c2!=EOF) );
      goon=1;
    }
  } while(goon);

  ungetc(c,stream);

  
  /*---------------------------------------------------------------------------------*/
  return fgets( s, size, stream);




}

/************************************************************************/
/* Herve Moulinec
   CNRS/LMA
   september 30th 2009


   input:
   int n              number of points to be extrapolated
   double t1          time t1
   double t2          time t2
   double t3          time t3
   double x1[n]       x at time t1 
   double x2[n]       x at time t2

   input/output
   double x3[n]      x extrapolated at time t3

*/     
/************************************************************************/
int extrapol( int n, 
	      double t1,
	      double x1[n],
	      double t2,
	      double x2[n],
	      double t3,
	      double x3[n]) {


  int i;
  double k1,k2;
  int status;

  status=0;

  k2 = (t3-t1)/(t2-t1);
  k1 = 1. - k2;


#pragma omp parallel for \
  default(none)		 \
  private(i)		 \
  shared(n)		 \
  shared(k1,k2)		 \
  shared(x3,x2,x1)
  for(i=0; i<n; i++) {
    x3[i] = k1*x1[i] + k2*x2[i];
  }

  return status;
}

/* added for harmonic implementation 2017/07/05  J. Boisse */
/************************************************************************/
int extrapol_harmonic( int n, 
	      double t1,
	      double complex x1[n],
	      double t2,
	      double complex x2[n],
	      double t3,
	      double complex x3[n]) {


  int i;
  double k1,k2;
  int status;

  status=0;

  k2 = (t3-t1)/(t2-t1);
  k1 = 1. - k2;


#pragma omp parallel for \
  default(none)		 \
  private(i)		 \
  shared(n)		 \
  shared(k1,k2)		 \
  shared(x3,x2,x1)
  for(i=0; i<n; i++) {
    x3[i] = k1*x1[i] + k2*x2[i];
  }

  return status;
}

/************************************************************************/
/* Herve Moulinec
   CNRS/LMA
   september 30th 2009


   input:
   int n              number of points to be extrapolated
   double t1          time t1
   double t2          time t2
   double t3          time t3

   input/output
   double x1[n]     x at time t1 in input,  x at time t2 in output
   double x2[n]     x at time t2 in input, extrapolated value at time t3 in output

*/     
/************************************************************************/
int extrapol2( int n, 
	      double t1,
	      double x1[n],
	      double t2,
	      double x2[n],
	      double t3) {


  int i;
  double k1,k2;
  double buff;
  int status;

  status=0;

  k2 = (t3-t1)/(t2-t1);
  k1 = 1. - k2;

#pragma omp parallel for \
  default(none)		 \
  private(i)		 \
  private(buff)		 \
  shared(n)		 \
  shared(k1,k2)		 \
  shared(x2,x1)
  for(i=0; i<n; i++) {
    buff = x2[i];
    x2[i] = k1*x1[i] + k2*x2[i];
    x1[i] = buff;
    
  }

  return status;

}


/* added for harmonic implementation 2017/07/05  J. Boisse */
/************************************************************************/
int extrapol2_harmonic( int n, 
	      double t1,
	      double complex x1[n],
	      double t2,
	      double complex x2[n],
	      double t3) {


  int i;
  double k1,k2;
  double complex buff;
  int status;

  status=0;

  k2 = (t3-t1)/(t2-t1);
  k1 = 1. - k2;

#pragma omp parallel for \
  default(none)		 \
  private(i)		 \
  private(buff)		 \
  shared(n)		 \
  shared(k1,k2)		 \
  shared(x2,x1)
  for(i=0; i<n; i++) {
    buff = x2[i];
    x2[i] = k1*x1[i] + k2*x2[i];
    x1[i] = buff;
    
  }

  return status;

}

/*************************************************************************************/
/* gives the position of a string (needle) in a string (haystack)
   as an integer (from 0 to strlen(haystack) )
*/
int strstr_position(const char *haystack, const char *needle){
  char *p;
  p = strstr(haystack,needle);
  if (p == (char *)0 ){
    return -1;
  }
  else{
    return (int)(p-haystack);
  }
}

/*******************************************************************************/
/* tells how many times a given string (needle) appears in a string (haystack) */
int strstr_nr_appearances(const char *haystack, const char *needle){
  int i;
  char *pp;

  i = 0;
  pp = (char *)haystack;
  while( (pp=strstr(pp,needle)) != (char *)0 ) {
    pp += strlen(needle);
    i++;
  }
  return i;
}

/*************************************************************************************/
/* suppress any appearance of string c in string s */
void suppress_string_in_string(char *s, char *c){
  int i;
  int ii;
  int n;
  while ( (ii=strstr_position(s,c)) >= 0){
    n = strlen(s)-strlen(c);
    for( i=ii; i<=n ; i++){
      s[i]=s[i+strlen(c)];
    }
  }
}

/*************************************************************************************/
/* eliminates spaces at the begining, repeated spaces and spaces at the end of a string, */
void clean_string(char *s){
  char *p;
  int i;

 /* eliminates spaces at the begining */
  while(s[0]==' '){
    for(i=0;i<=strlen(s);i++) s[i]=s[i+1];
  }  
 
  /* eliminates repeated spaces */
  while( (p=strstr(s,"  ")) ){
    if (p!=(char *)0) {
      for(i=p-s+1; i<=strlen(s); i++) s[i]=s[i+1];
    }
  }

  /* eliminates spaces at the end */
  while ( ( (i=strlen(s)-1)>0) && (s[i]==' ') ) s[i]='\0';



}

/*************************************************************************************/
/* works almost like strcmp except that it ignores repeated spaces (considered as one space)
   and ignores spaces at the beginning or end of strings */
int sentence_cmp(const char *s1, const char *s2){

  char *ss1, *ss2;
  size_t size;

  int status;
  
  size = strlen(s1);
  ss1 = malloc(size+1);  

  size = strlen(s2);
  ss2 = malloc(size+1);
  
  strcpy(ss1, s1);
  strcpy(ss2, s2);


 
  clean_string(ss1);
  clean_string(ss2);

  printf("s1=%s\n",ss1);
  printf("s2=%s\n",ss2);

  status = strcmp(ss1,ss2);
  free(ss1);
  free(ss2);

  return status;
  
}

/*************************************************************************************/
/* works almost like strcasecmp except that it ignores spaces */
int strcasecmp_ignorespace(const char *s1, const char *s2){

  char *ss1, *ss2;
  size_t size;

  int status;
  
  size = strlen(s1);
  ss1 = malloc(size+1);  

  size = strlen(s2);
  ss2 = malloc(size+1);
  
  strcpy(ss1, s1);
  strcpy(ss2, s2);


 
  suppress_string_in_string(ss1," ");
  suppress_string_in_string(ss2," ");

  /*
  printf("s1=%s\n",ss1);
  printf("s2=%s\n",ss2);
  */

  status = strcasecmp(ss1,ss2);
  free(ss1);
  free(ss2);

  return status;
  
}

