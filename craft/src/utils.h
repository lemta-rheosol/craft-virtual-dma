#ifndef __CRAFTUTILS__
#define __CRAFTUTILS__
/*-----------------------------------------------------------*/
#define _ISOC99_SOURCE
/*-----------------------------------------------------------*/
#include <stdio.h>
#define TINY 3.e-16

#include <complex.h>
/* complex.h => added for harmonic implementation 2017/07/05  J. Boisse */

int craft_fscanf(FILE *f, const char *format, ...);
char *craft_fgets(char *s, int size, FILE *stream);

int extrapol( int n, 
	      double t1,
	      double x1[n],
	      double t2,
	      double x2[n],
	      double t3,
	      double x3[n]) ;

/* added for harmonic implementation 2017/07/05  J. Boisse */
int extrapol_harmonic( int n, 
	      double t1,
	      double complex x1[n],
	      double t2,
	      double complex x2[n],
	      double t3,
	      double complex x3[n]) ;

int extrapol2( int n, 
	       double t1,
	       double x1[n],
	       double t2,
	       double x2[n],
	       double t3);

/* added for harmonic implementation 2017/07/05  J. Boisse */
int extrapol2_harmonic( int n, 
	      double t1,
	      double complex x1[n],
	      double t2,
	      double complex x2[n],
	      double t3) ;
char *cdate(int flag);
int strstr_position(const char *haystack, const char *needle);
int strstr_nr_appearances(const char *haystack, const char *needle);
void suppress_string_in_string(char *s, char *c);
void clean_string(char *s);
int strcasecmp_ignorespace(const char *s1, const char *s2);
int sentence_cmp(const char *s1, const char *s2);
char **parse_sentence( char *sentence, int *n);

/*************************************************************************************/
/* power function when n is an integer value */
/* this function in inlined for performance purpose */
static inline double intpow(double x, double p){
    register double  x2, x4, x8, y;
    register int n = p;
  switch(n){
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 0:
    return 1.;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 1:
    return x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 2:
    return x*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 3:
    return x*x*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 4:
    x2=x*x;
    return x2*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 5:
    x2=x*x;
    return x*x2*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 6:
    x2=x*x;
    return x2*x2*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 7:
    x2=x*x;
    x4=x2*x2;
    return x4*x2*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 8:
    x2=x*x;
    x4=x2*x2;
    return x4*x4;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 9:
    x2=x*x;
    x4=x2*x2;
    return x*x4*x4;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 10:
    x2=x*x;
    x4=x2*x2;
    return x4*x4*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 11:
    x2=x*x;
    x4=x2*x2;
    //    x8=x4*x4;
    return x4*x4*x2*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 12:
    x2=x*x;
    x4=x2*x2;
    //    x8=x4*x4;
    return x4*x4*x4;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 13:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x4*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 14:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x4*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 15:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x4*x2*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 16:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 17:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 18:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 19:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x2*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 20:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x4;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 21:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x4*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 22:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x4*x2;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 23:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x4*x2*x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case 24:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return x8*x8*x8;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -1:
    return 1./x;
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -2:
    return 1./(x*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -3:
    return 1./(x*x*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -4:
    x2=x*x;
    return 1./(x2*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -5:
    x2=x*x;
    return 1./(x*x2*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -6:
    x2=x*x;
    return 1./(x2*x2*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -7:
    x2=x*x;
    x4=x2*x2;
    return 1./(x4*x2*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -8:
    x2=x*x;
    x4=x2*x2;
    return 1./(x4*x4);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -9:
    x2=x*x;
    x4=x2*x2;
    return 1./(x*x4*x4);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -10:
    x2=x*x;
    x4=x2*x2;
    return 1./(x4*x4*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -11:
    x2=x*x;
    x4=x2*x2;
    //    x8=x4*x4;
    return 1./(x4*x4*x2*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -12:
    x2=x*x;
    x4=x2*x2;
    //    x8=x4*x4;
    return 1./(x4*x4*x4);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -13:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x4*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -14:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x4*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -15:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x4*x2*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -16:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -17:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -18:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -19:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x2*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -20:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x4);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -21:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x4*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -22:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x4*x2);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -23:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x4*x2*x);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  case -24:
    x2=x*x;
    x4=x2*x2;
    x8=x4*x4;
    return 1./(x8*x8*x8);
    /*- - - - - - - - - - - - - - - - - - - - - - -*/
  default:
    if(n>=0){
      y=1.;
      while (n)
        {
          if (n & 1){
            y *= x;
          }
          n >>= 1;
          x *= x;
        }
      
      return y;
    }
    else{
      y=1.;
      n = -n;
      while (n)
        {
          if (n & 1){
            y *= x;
          }
          n >>= 1;
          x *= x;
        }
      
      return 1./y;
    }
  }
}
/*************************************************************************************/
#endif
