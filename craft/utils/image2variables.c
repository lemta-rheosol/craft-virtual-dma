/******************************************************************************/
/*  Fabrice Silva
    LMA/CNRS
    September 30th 2011

    Converter utility to create a Variables file from images
*/
/******************************************************************************/
#define _ISOC99_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <locale.h>

#include <materials.h>
#include <craft.h>
#include <variables.h>
#include <io_variables.h>
#include <craftimage.h>

#ifndef H2IMAGE
#include <h2image.h>
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef hdf5
#include<hdf5.h>
char images2var_built_with_H5_lib_vers_info_g[] = "images2var built with " H5_VERS_INFO;
#endif

void usage(){
  fprintf(stderr, "Usages of var2image:\n");
  fprintf(stderr, "--------------------\n");
  fprintf(stderr, "images2var (-h|--help)\n");
  fprintf(stderr, "    print this help message and exit\n");
  fprintf(stderr, "images2var -c cfn -o variablesfile -t time "
          "--field1 spec1 [--field2 spec2 ... ]\n");
  fprintf(stderr, "    Extract fields from images file and store the created\n");
  fprintf(stderr, "    Variables structure, with time stamp set to the given value.\n");
  fprintf(stderr, "    Each field may be specified by an imagefile name:\n");
  fprintf(stderr, "        --stress example_stress.vtk\n");
  fprintf(stderr, "    or, in order to create an uniform field, by an explicit definition\n");
  fprintf(stderr, "        --plastic \"SCALAR 0.\"\n");
  fprintf(stderr, "        --displacement \"VECTOR3D 0. 1. 0.\"\n");
  fprintf(stderr, "        --stress \"TENSOR2 1. 2. 3. 4. 5. 6.\"\n");
  fprintf(stderr, "        --backstress \"VECTOR 4 0. 0. 0. 0.\"\n");
  fprintf(stderr, "          (with the mention to the number of components)\n");
  fprintf(stderr, "    Please note that the quote are mandatory.\n");
}

int main( int argc, char **argv){
  char *cfn, *ofn, *label, *ifn, *buffer;
  char buffer2[1024];
  int status,c,iph,Nph,ivar;
  int type[3]={0,0,0};
  int *nppp, *phase;
  int **index;
  double t_save;
  void **x;
  CraftImage image;
  Variable *var;
  Variables *variables;

  cfn = ofn = label = ifn = buffer = NULL;
  nppp = phase = NULL;
  index = NULL;
  t_save = NAN;

#ifdef _OPENMP
  {
    int nr_threads;
    nr_threads = 1;
    /* trying to parse OMP_NUM_THREADS environment variable */
    char *env;
    int status;

    env = getenv("OMP_NUM_THREADS");
    if( env != (char *)0 ) {
      status=sscanf(env,"%d",&nr_threads);
      if (status!=1) nr_threads=1;
    }
    /* set number of threads to be used by OpenMP */
    omp_set_num_threads(nr_threads);
  }
#endif

  /* When entering numeric values, ensure decimal separator is well parsed */
  setlocale(LC_NUMERIC, "C");

  /* Parsing commandline arguments */
  c = 0;
  do{
    if (strcmp(argv[optind], "-c")==0){
      cfn=argv[optind+1];
      optind += 2;
      c = 1;
    }
    else if (strcmp(argv[optind], "-o")==0){
      ofn=argv[optind+1];
      optind += 2;
      c = 1;
    }
    else if (strcmp(argv[optind], "-t")==0){
      sscanf(argv[optind+1], "%3lf", &t_save);
      optind += 2;
      c = 1;
    }
    else{
      c = 0;
    }
  } while(c==1);

  if ( (cfn==NULL) || (ofn==NULL) || (isnan(t_save)) ){
    usage();
    exit(EXIT_FAILURE);
  }

  /* Parsing characteristic file */
  {
    CraftImage cf;
    cf.type[0] = CRAFT_IMAGE_INT;
    cf.pxl.v = NULL;
    read_image(cfn, &cf);

    {
      int i;
      image.type[0] = CRAFT_IMAGE_UNKNOWN;
      image.type[1] = 0;
      image.type[2] = 0;
      image.name = NULL;
      for(i=0; i<3; i++){
	image.n[i] = cf.n[i];
	image.nd[i]= cf.n[i];
	image.s[i] = cf.s[i];
	image.p[i] = cf.p[i];
      }
#if defined FFTW2 || defined FFTW3
      image.nd[0] = 2*(image.n[0]/2+1);   // see in FFTW 3.3.2 documentation: 4.3.4 Real-data DFT Array Format
#else
      image.nd[0] = image.n[0]+2;
#endif
    }
    
    parse_characteristic_file(&cf, &Nph, &phase, &nppp, &index, image);

    type[0] = CRAFT_IMAGE_UNKNOWN;
    craft_image_reset( &cf, type );
  }

  variables = malloc(Nph*sizeof(*variables));
  if (variables==NULL) exit(EXIT_FAILURE);
  for(iph=0; iph<Nph; iph++){
    variables[iph].np = nppp[iph];
    variables[iph].nvar = 0;
    variables[iph].vars = NULL;
  }

  x = malloc(Nph*sizeof(*x));
  if (x==NULL) exit(EXIT_FAILURE);
  for(iph=0; iph<Nph; iph++) x[iph] = NULL;

  ivar = 0;
  while(optind<argc){
    struct stat buf;

    buffer = argv[optind++];
    if (strncmp(buffer, "--", 2)!=0){
      fprintf(stderr, "New field label required here (got %s).\n", buffer);
      exit(EXIT_FAILURE);
    }
    if (optind==argc){
      fprintf(stderr, "Image file required here (after %s).\n", buffer);
      exit(EXIT_FAILURE);
    }
    label = buffer+2;
    ifn = argv[optind++];

    /* Allocating phase variables */
    for(iph=0; iph<Nph; iph++){
      variables[iph].nvar = ivar+1;
      variables[iph].vars = realloc(variables[iph].vars, (ivar+1)*sizeof(Variable));
      if (variables[iph].vars==NULL){
        fprintf(stderr, "Unable to allocate Variables[%d].vars.\n", iph);
        exit(EXIT_FAILURE);
      }
    }

    if ( (stat(ifn, &buf)!=-1) && (S_ISREG(buf.st_mode)==1) ){
      char *desc;
      /* Regular file */

      /* Need to put ima.pxl to something different from NULL */
      /* so that read_image does not overwrite nd. */
      /* An option is to allocate a CRAFT_IMAGE_DOUBLE array */
      image.type[0] = CRAFT_IMAGE_DOUBLE;
      status = craft_image_alloc(&image);
      if (status!=0){
        fprintf(stderr, "Unable to allocate image.pxl.v.\n");
        exit(EXIT_FAILURE);
      }

      /* Read image and convert to phase organization */
      image.type[0] = CRAFT_IMAGE_UNKNOWN;
      read_image(ifn, &image);

      /* Casting scalar Image to double, as image_to_phase requires same types. */
      if (image.type[0]<CRAFT_IMAGE_DOUBLE){
        image.type[0] = CRAFT_IMAGE_DOUBLE;
        read_image(ifn, &image);
      }

      /* Converting type from Craft Image types to variables types*/
      switch (image.type[0]){
      case CRAFT_IMAGE_DOUBLE:
        type[0] = SCALAR;
        type[1] = 0;
        type[2] = 0;
        desc = malloc(strlen("scalar")+1);
        strcpy(desc, "scalar");
        break;
      case CRAFT_IMAGE_VECTOR3D:
        type[0] = VECTOR3D;
        type[1] = 0;
        type[2] = 0;
        desc = malloc(strlen("3d-vector")+1);
        strcpy(desc, "3d-vector");
        break;
      case CRAFT_IMAGE_TENSOR2:
        type[0] = TENSOR2;
        type[1] = 0;
        type[2] = 0;
        desc = malloc(strlen("rank-2 tensor")+1);
        strcpy(desc, "rank-2 tensor");
        break;
      case CRAFT_IMAGE_VECTOR:
        type[0] = VECTOR;
        type[1] = image.type[1];
        type[2] = 0;
        desc = malloc(strlen("vector")+1);
        strcpy(desc, "vector");
        break;
      default:
        fprintf(stderr, "Error converting type of filed %s.\n", label);
        exit(EXIT_FAILURE);
      }

      /* Copying data from image to Variable instance */
      for(iph=0; iph<Nph; iph++){
        var = &(variables[iph].vars[ivar]);
        if (allocate_variable(var, label, nppp[iph], type, 0)!=0){
          fprintf(stderr, "Unable to allocate Variable (iph=%d, ivar=%d).\n", iph, ivar);
          exit(EXIT_FAILURE);
        }
        x[iph] = var->values;
      }
      image_to_phase(Nph, nppp, index, x, &image);
      fprintf(stderr, "Read %s field \"%s\" from file \"%s\".\n", desc, label, ifn);
      free(desc);
      free(image.name);
      craft_image_free(&image);
    }
    else if (sscanf(ifn, "%s", buffer2)==1){
      int ipt;
      char buffer3[1024];
      double val_scalar;
      double val_vector3D[3];
      double val_tensor2[6];
      double *val_vector;
      double **val_matrix;

      /* Uniform field: type... */
      type[1] = 0;
      type[2] = 0;
      if (strncmp(buffer2, "SCALAR", strlen("SCALAR"))==0){
        type[0] = SCALAR;
        ifn += strlen("SCALAR");
        strcpy(buffer2, "SCALAR");
      }
      else if (strncmp(buffer2, "VECTOR3D", strlen("VECTOR3D"))==0){
        type[0] = VECTOR3D;
        ifn += strlen("VECTOR3D");
        strcpy(buffer2, "VECTOR3D");
      }
      else if (strncmp(buffer2, "TENSOR2", strlen("TENSOR2"))==0){
        type[0] = TENSOR2;
        ifn += strlen("TENSOR2");
        strcpy(buffer2, "TENSOR2");
      }
      else if (strncmp(buffer2, "VECTOR", strlen("VECTOR"))==0){
        int i;
        status = sscanf(ifn, "%d%n", &type[1], &i);
        if (status!=1){ status=-1; break;}
        ifn += i;
        type[0] = VECTOR;
        type[2] = 0;
        ifn += strlen("VECTOR");
        strcpy(buffer2, "VECTOR");
      }
      else if (strncmp(buffer2, "MATRIX", strlen("MATRIX"))==0){
        int i;
        status = sscanf(ifn, "%d %d%n", &type[1], &type[2], &i);
        if (status!=2){ status=-1; break;}
        ifn += i;
        type[0] = MATRIX;
        ifn += strlen("MATRIX");
        strcpy(buffer2, "MATRIX");
      }
      else{
        fprintf(stderr, "Unable to understand type of field \"%s\": %s.\n",
          label, buffer2);
        exit(EXIT_FAILURE);
      }
      for(iph=0; iph<Nph; iph++){
        var = &(variables[iph].vars[ivar]);
        if (allocate_variable(var, label, nppp[iph], type, 0)!=0){
          fprintf(stderr, "Unable to allocate Variable (iph=%d, ivar=%d).\n", iph, ivar);
          exit(EXIT_FAILURE);
        }
        x[iph] = var->values;
      }

      /* ... and value */
      status = 0;
      ifn += strspn(ifn, " =");
      switch(type[0]){
      case SCALAR:
        status = sscanf(ifn, "%lf", &val_scalar);
        if (status!=1){ status=-1; break; } else { status=0; }
        for(iph=0; iph<Nph; iph++){
          double *val;
          val = (double *)(variables[iph].vars[ivar].values);
          for(ipt=0; ipt<nppp[iph]; ipt++){
            val[ipt] = val_scalar;
          }
        }
        sprintf(buffer3, "%lf", val_scalar);
        break;
      case VECTOR3D:
        status = sscanf(ifn, "%lf %lf %lf",
          &val_vector3D[0], &val_vector3D[1], &val_vector3D[2]);
        if (status!=3){ status=-1; break; } else { status=0; }
        for(iph=0; iph<Nph; iph++){
          double (*val) [3];
          val = (double (*)[3])(variables[iph].vars[ivar].values);
          for(ipt=0; ipt<nppp[iph]; ipt++){
            val[ipt][0] = val_vector3D[0];
            val[ipt][1] = val_vector3D[1];
            val[ipt][2] = val_vector3D[2];
          }
        }
        /* Formatting of output msg */
        sprintf(buffer3, "[%lf, %lf, %lf]",
                val_vector3D[0], val_vector3D[1], val_vector3D[2]);
        break;
      case TENSOR2:
        status = sscanf(ifn, "%lf %lf %lf %lf %lf %lf",
          &val_tensor2[0], &val_tensor2[1], &val_tensor2[2],
          &val_tensor2[3], &val_tensor2[4], &val_tensor2[5]);
        if (status!=6){ status=-1; break; } else { status=0; }
        for(iph=0; iph<Nph; iph++){
          double (*val) [6];
          val = (double (*)[6])(variables[iph].vars[ivar].values);
          for(ipt=0; ipt<nppp[iph]; ipt++){
            val[ipt][0] = val_tensor2[0];
            val[ipt][1] = val_tensor2[1];
            val[ipt][2] = val_tensor2[2];
            val[ipt][3] = val_tensor2[3];
            val[ipt][4] = val_tensor2[4];
            val[ipt][5] = val_tensor2[5];
          }
        }
        /* Formatting of output msg */
        sprintf(buffer3, "\n[[%lf, %lf, %lf]\n [%lf, %lf, %lf]\n [%lf, %lf, %lf]]",
                val_tensor2[0], val_tensor2[5], val_tensor2[4],
                val_tensor2[5], val_tensor2[1], val_tensor2[3],
                val_tensor2[4], val_tensor2[3], val_tensor2[2]);
        break;
      case VECTOR:
      {
        int i,j;
        char *ptr;
        val_vector = malloc(type[1]*sizeof(double));
        for(i=0; i<type[1]; i++){
          val_vector[i] = strtod(ifn, &ptr);
          if (ptr==ifn){ status=-1; break;}
          ifn = ptr;
        }
        for(iph=0; iph<Nph; iph++){
          double *val;
          val = (double *)(variables[iph].vars[ivar].values);
          for(ipt=0; ipt<nppp[iph]; ipt++){
            for(i=0; i<var->type[1]; i++){
              val[ipt*var->type[1]+i] = val_vector[i];
            }
          }
          /* Formatting of output msg */
          sprintf(buffer3, "[ %n", &i);
          ptr = &(buffer3[0])+i;
          j = 0;
          do {
            sprintf(ptr, "%lf %n", val_vector[j++], &i);
            ptr += i;
          } while((ptr-&(buffer3[0])<80) && (j<var->type[1]));
          sprintf(ptr, "]");
        }
        free(val_vector);
        break;
      }
      case MATRIX:
      {
        int i,j,k;
        char *ptr;
        val_matrix = malloc(type[1]*type[2]*sizeof(double));
        for(i=0; i<type[1]*type[2]; i++){
          ((double *)val_matrix)[i] = strtod(ifn, &ptr);
          if (ptr==ifn){ status=-1; break;}
          ifn = ptr;
        }
        for(iph=0; iph<Nph; iph++){
          double *val;
          val = (double *)(variables[iph].vars[ivar].values);
          for(ipt=0; ipt<nppp[iph]; ipt++){
            for(i=0; i<var->type[1]*var->type[2]; i++){
              val[ipt*var->type[1]*var->type[2]+i] = ((double *)val_matrix)[i];
            }
          }
          /* Formatting of output msg */
          sprintf(buffer3, "\n[%n", &i);
          ptr = &(buffer3[0])+i;
          j = k = 0;
          do {
            if ( (var->type[1]>3) && ((j>1) || (j<var->type[1]-2)) ){
              /* Line ellipsis */
              continue;
            }
            if (k==0) {
              /* New line */
              sprintf(ptr, "[%n", &i);
              ptr += i;
            }
            sprintf(ptr, "%lf %n", ((double *)val_matrix)[j*var->type[2] + k ], &i);
            ptr += i;
            k ++;
            if (ptr-&(buffer3[0])>60){
              /* InLine ellipsis */
              sprintf(ptr, " ...]\n %n", &i);
              ptr += i;
              k = 0;
              j ++;
            }
            else if (k==var->type[2]){
              sprintf(ptr, " ]\n %n", &i);
              ptr += i;
              k = 0;
              j ++;
            }
          } while((j<var->type[1]));
          sprintf(ptr-2, "]\n");
        }
        free(val_matrix);
        break;
      }
      }
      if (status!=0){
        fprintf(stderr, "Unable to parse field \"%s\": %s (%s).\n",
          label, ifn, buffer2);
        exit(EXIT_FAILURE);
      }
      fprintf(stderr, "Filled field \"%s\" with %s %s.\n", label, buffer2, buffer3);

    }
    else{
      fprintf(stderr, "%s is neither a regular file, nor a interpretable value.\n", ifn);
      exit(EXIT_FAILURE);
    }
    ivar++;
  }

  /* TODO: sanity check of Variable's (remove dummy phase fields) */
  /* check_finite(variables, Nph, 1); */



  /* write Variables file */
  {
    double Ecur[6], Eprev[6];
    int *materials;
  /* TODO: Get correct values for time, materials and macro strains */
    materials = malloc(Nph*sizeof(*materials));
    for(iph=0;iph<Nph;iph++){ materials[iph] = UNKNOWN_MAT; }
    for(c=0;c<6;c++){
      Ecur[c] = NAN;
      Eprev[c] = NAN;
    }
    write_variables_all_phases(Nph, variables, ofn, cfn, phase,
         materials, -1, t_save, Ecur, Eprev, 0);
    free(materials);
  }
  free(x);
  for(iph=0; iph<Nph; iph++){
    free(index[iph]);
    deallocate_variables(&variables[iph]);
  }
  free(index);
  free(phase);
  free(nppp);
  free(variables);
#ifdef hdf5
  H5close();
#endif

  return 0;
}
