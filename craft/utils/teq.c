#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <craftimage.h>
#include <utils.h>

void usage(){
  fprintf(stderr,"teq usage:\n");
  fprintf(stderr,"");
  fprintf(stderr,"teq [-d] [-s] ");
  fprintf(stderr,"-f image11,image22,image33,image12,image13,image23");
  fprintf(stderr," -e equivalent_image \n");
  fprintf(stderr," -t hydrostatic_image \n");


  fprintf(stderr,"-d : input images describe strain field\n");
  fprintf(stderr,"-s : input images describe stress field\n");

  fprintf(stderr,"-t : image name of hydrostatic field \n");
  fprintf(stderr,"-e : image name of equivalent field \n");


	  
  fprintf(stderr,"teq -h \n");
}

int main( int argc , char **argv){

  CraftImage image[6], equivalent, hydrostatic;
  CraftImage tensor;
  int opt;
  int i,j;
  char *p1,*p2;
  double coef=-1.;
  int status;

  int type[3];

  int ii[3];
  int jj;
  
  type[0] = CRAFT_IMAGE_DOUBLE;
  type[1]=0;
  type[2]=0;

  for(i=0;i<6;i++){
    craft_image_init( &image[i] , type );
  }
  craft_image_init( &equivalent , type );
  craft_image_init( &hydrostatic , type );


  while( (opt=getopt(argc,argv,"hf:dse:t:"))!=-1){

    switch(opt) {
  
      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 'h':
      usage();
      exit(0);
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 's':
      coef=3./2.;
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 'd':
      coef=2./3.;
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 'e':
      equivalent.name=malloc(strlen(optarg)+1);
      status=sscanf(optarg,"%s",equivalent.name);
      if(status!=1){
	fprintf(stderr, "error in teq: invalid syntax\n");
	usage();
	exit(1);
      }
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 't':
      hydrostatic.name=malloc(strlen(optarg)+1);
      status=sscanf(optarg,"%s",hydrostatic.name);
      if(status!=1){
	fprintf(stderr, "error in teq: invalid syntax\n");
	usage();
	exit(1);
      }
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    case 'f':
      //      printf("optard=%s\n",optarg);
      p1=optarg;
      p2=optarg;
      for(i=0;i<6;i++){
	if(i<5){
	  p2 = strstr((const char *)p1,",");
	}
	else{
	  p2 = p1+strlen(p1);
	}

	j = (int)(p2-p1);
	if ((p2==0)||(j==0)) {
	  fprintf(stderr, "error in teq: invalid syntax\n");
	  usage();
	  exit(1);
	}
	image[i].name=(char *)malloc(j+1);
	//	printf("%d %d %d [%s] [%s]\n",p1,p2,j,p1,p2);
	strncpy(image[i].name,p1,j);
        image[i].name[j]=0;
	p1 = p2+1;
      }
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - -*/
    }  

  }  
  /*-------------------------------------------------------*/
  /* verification */
  if(optind<argc){
    fprintf(stderr, "error in teq: invalid syntax\n");
    usage();
    exit(1);
  }

  if(coef<0.){
    fprintf(stderr,"teq error: it must be said if equivalent strain or equivalent stress is required\n");
    usage();
    exit(1);
  }

  for(i=0;i<6;i++){
    if(image[i].name==(char *)0) {
      fprintf(stderr, "error in teq: invalid syntax\n");
      usage();
      exit(1);
    }
  }
  if( (equivalent.name==(char *)0) && (hydrostatic.name==(char *)0) ) {
    fprintf(stderr, "error in teq: invalid syntax\n");
    fprintf(stderr, "               at least one of -e or -t option must be set\n");
    usage();
    exit(1);
  }
  
  
  /*-------------------------------------------------------*/
  for(i=0;i<6;i++){
    //    printf("ouverture de: %s\n",image[i].name);
    read_image(image[i].name, &image[i]);
  }

  status=0;
  for(i=1;i<6;i++){
    for(j=0;j<3;j++){
      if( image[i].type[j] != image[0].type[j] ) status=-1;
      if( image[i].n[j] != image[0].n[j] ) status=-2;
      if( image[i].nd[j] != image[0].nd[j] ) status=-3;
      if( fabs(image[i].s[j]-image[0].s[j])>TINY ) status=-4;
      if( fabs(image[i].p[j]-image[0].p[j])>TINY ){
        //        printf("%lf %lf %lf\n",image[i].p[j],image[0].p[j], fabs(image[i].p[j]-image[0].p[j]) );
        status=-5;
      }
    }
    if(status!=0){
      fprintf(stderr,"teq: (%d) images %s and %s do not match their frame\n",status,image[0].name,image[i].name);
    }
  }
  if(status!=0) exit(1);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(equivalent.name!=(char *)0) {
    for(j=0;j<3;j++){
      equivalent.type[j]=image[0].type[j];
      equivalent.n[j]=image[0].n[j];
      equivalent.nd[j]=image[0].nd[j];
      equivalent.s[j]=image[0].s[j];
      equivalent.p[j]=image[0].p[j];
    }
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for(j=0;j<3;j++){
    hydrostatic.type[j]=image[0].type[j];
    hydrostatic.n[j]=image[0].n[j];
    hydrostatic.nd[j]=image[0].nd[j];
    hydrostatic.s[j]=image[0].s[j];
    hydrostatic.p[j]=image[0].p[j];
  }
  craft_image_alloc(&hydrostatic);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  type[0]=CRAFT_IMAGE_TENSOR2;
  craft_image_init(&tensor,type);

  for(j=0;j<3;j++){
    tensor.n[j]=image[0].n[j];
    tensor.nd[j]=image[0].nd[j];
    tensor.s[j]=image[0].n[j];
    tensor.p[j]=image[0].n[j];
  }
  craft_image_alloc(&tensor);

  for(ii[2]=0;ii[2]<tensor.n[2];ii[2]++) {
    for(ii[1]=0;ii[1]<tensor.n[1];ii[1]++) {
      for(ii[0]=0;ii[0]<tensor.n[0];ii[0]++) {
	jj = 
	  ii[0]+
	  ii[1]*tensor.nd[0]+
	  ii[2]*tensor.nd[0]*tensor.nd[1];

        for(j=0;j<6;j++) {
          tensor.pxl.t2[jj][j] = image[j].pxl.d[jj];
        }

      }
    }
  }




  if(equivalent.name!=(char *)0) {
    craft_image_alloc(&equivalent);
  }

  /*-------------------------------------------------------*/
  /* hydrostatic */
  for(ii[2]=0;ii[2]<tensor.n[2];ii[2]++) {
    for(ii[1]=0;ii[1]<tensor.n[1];ii[1]++) {
      for(ii[0]=0;ii[0]<tensor.n[0];ii[0]++) {
	jj = 
	  ii[0]+
	  ii[1]*tensor.nd[0]+
	  ii[2]*tensor.nd[0]*tensor.nd[1];

	hydrostatic.pxl.d[jj] = 
	  (tensor.pxl.t2[jj][0] + 
	   tensor.pxl.t2[jj][1] + 
	   tensor.pxl.t2[jj][2] ) / 3.;
      }
    }
  }
  /*-------------------------------------------------------*/
  /* deviatoric */
  for(ii[2]=0;ii[2]<tensor.n[2];ii[2]++) {
    for(ii[1]=0;ii[1]<tensor.n[1];ii[1]++) {
      for(ii[0]=0;ii[0]<tensor.n[0];ii[0]++) {
	jj = 
	  ii[0]+
	  ii[1]*tensor.nd[0]+
	  ii[2]*tensor.nd[0]*tensor.nd[1];

	tensor.pxl.t2[jj][0] -= hydrostatic.pxl.d[jj];
	tensor.pxl.t2[jj][1] -= hydrostatic.pxl.d[jj];
	tensor.pxl.t2[jj][2] -= hydrostatic.pxl.d[jj];

      }
    }
  }
  
	  
  /*-------------------------------------------------------*/
  /* equivalent */
  if(equivalent.name!=(char *)0) {
    for(ii[2]=0;ii[2]<tensor.n[2];ii[2]++) {
      for(ii[1]=0;ii[1]<tensor.n[1];ii[1]++) {
	for(ii[0]=0;ii[0]<tensor.n[0];ii[0]++) {
	  jj = 
	    ii[0]+
	    ii[1]*tensor.nd[0]+
	    ii[2]*tensor.nd[0]*tensor.nd[1];
	  
	  equivalent.pxl.d[jj] = 
	    tensor.pxl.t2[jj][0]*tensor.pxl.t2[jj][0] +
	    tensor.pxl.t2[jj][1]*tensor.pxl.t2[jj][1] +
	    tensor.pxl.t2[jj][2]*tensor.pxl.t2[jj][2] +
	    2.*(
		tensor.pxl.t2[jj][3]*tensor.pxl.t2[jj][3] +
		tensor.pxl.t2[jj][4]*tensor.pxl.t2[jj][4] +
		tensor.pxl.t2[jj][5]*tensor.pxl.t2[jj][5] 
		);
	  
	}
      }
    }
    for(ii[2]=0;ii[2]<tensor.n[2];ii[2]++) {
      for(ii[1]=0;ii[1]<tensor.n[1];ii[1]++) {
	for(ii[0]=0;ii[0]<tensor.n[0];ii[0]++) {
	  jj = 
	    ii[0]+
	    ii[1]*tensor.nd[0]+
	    ii[2]*tensor.nd[0]*tensor.nd[1];
	  
	  equivalent.pxl.d[jj] = sqrt(coef * equivalent.pxl.d[jj] );
	  
	}
      }
    }
  }
  /*-------------------------------------------------------*/
  char fmt[4];
  /*
  if (
      (strstr(equivalent.name,".i3d")!=0)
      ||
      (strstr(equivalent.name,".ima")!=0)
      ){
  */
  if (
      ( strcmp(equivalent.name+strlen(equivalent.name)-strlen(".i3d") , ".i3d" )==0 ) ||
      ( strcmp(equivalent.name+strlen(equivalent.name)-strlen(".ima") , ".ima" )==0 ) 
      ) {
    equivalent.name[strlen(equivalent.name)-4]='\0';
    strcpy(fmt,"i3d");
  }
  else if (
	   ( strcmp(equivalent.name+strlen(equivalent.name)-strlen(".vtk") , ".vtk" )==0 ) 
	   ) {
    strcpy(fmt,"vtk");
    equivalent.name[strlen(equivalent.name)-strlen(".vtk")]='\0';
  }
    
  else{
    strcpy(fmt,"vtk");
  }
    

  printf("%s\n",equivalent.name);
  if(equivalent.name!=(char *)0) {
    write_image( equivalent.name, &equivalent, fmt);
  }

  if(hydrostatic.name!=(char *)0) {
    write_image( hydrostatic.name, &hydrostatic, fmt);
  }

}
