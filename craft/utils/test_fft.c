#include <stdio.h>
#include <math.h>

#include <complex.h>
#include <craftimage.h>
#include <craft_time.h>

int main(int argc, char **argv) {
  CraftImage image;
  
  int i,j,k;
  int ii;
  int ic;
  
  double t0,t;
  /*---------------------------------------------------------------------*/
  for(i=0;i<3;i++) {
    image.n[i]	= 512;
    image.s[i] = 0.;
    image.p[i] = 1./image.n[i];
    image.nd[i] = image.n[i];
  }
  image.nd[0] = 2*(image.n[0]/2+1);

  image.type[0]=CRAFT_IMAGE_TENSOR2;
  image.type[1]=6;
  image.type[2]=0;

  craft_image_alloc(&image);

  /*---------------------------------------------------------------------*/
  t0=craft_clocktime();
  craft_fft_image(&image,0);
  t  = craft_clocktime() - t0;
  printf("FFT initialisation t= %f\n",t);
  /*---------------------------------------------------------------------*/

  for( k=0; k<image.n[2]; k++ ) {
    for( j=0; j<image.n[1]; j++ ) {
      for( i=0; i<image.n[0]; i++ ) {
	
	ii = i + image.nd[0] * ( j + k*image.nd[1] );
	for(ic=0;ic<6;ic++) {
	  image.pxl.t2[ii][ic]=1.;
	}
      }
    }
  }
  
  /*---------------------------------------------------------------------*/
  t0=craft_clocktime();
  craft_fft_image(&image,-1);
  t  = craft_clocktime() - t0;
  printf("FFT directe t= %lf\n",t);
  /*


  for( k=0; k<image.n[2]; k++ ) {
    for( j=0; j<image.n[1]; j++ ) {
      for( i=0; i<image.nd[0]; i++ ) {
	
	ii = i + image.nd[0] * ( j + k*image.nd[1] );
	for(ic=0;ic<6;ic++) {
	  if ( cabs(image.pxl.ft2[ii][ic])>0.001) {
	    printf("%d %d %d   %d %lf+I*%lf\n",
		   i,j,k,
		   ic, 
		   creal(image.pxl.ft2[ii][ic]),
		   cimag(image.pxl.ft2[ii][ic])
		   );
	  }
	}

      }
    }
  } 
  printf("coucou\n");
  */
  /*---------------------------------------------------------------------*/

  t0  = craft_clocktime();
  craft_fft_image(&image,1);
  t  = craft_clocktime() - t0;
  printf("FFT inverse t= %f\n",t);
  /*
  for( k=0; k<image.n[2]; k++ ) {
    for( j=0; j<image.n[1]; j++ ) {
      for( i=0; i<image.n[0]; i++ ) {
	
	ii = i + image.nd[0] * ( j + k*image.nd[1] );

	for(ic=0;ic<6;ic++) {
	  if ( fabs(image.pxl.t2[ii][ic]-1.)>0.001) {
	    printf("%d %d %d   %d %lf\n",
		   i,j,k,
		   ic, 
		   image.pxl.t2[ii][ic]
		   );
	  }
	}

      }
    }
  } 
  */
  /*---------------------------------------------------------------------*/
    
}
