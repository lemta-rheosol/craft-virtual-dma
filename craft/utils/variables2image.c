/******************************************************************************/
/*  Fabrice Silva
    LMA/CNRS
    September 26th 2011

    Converter utility to create image (vtk) from Variables file.
*/
/******************************************************************************/
#define _DEFAULT_SOURCE

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include <getopt.h>
#include<math.h>
#include<materials.h>
#include<craft.h>
#include <utils.h>
#include<craftimage.h>
#include<variables.h>
#include<io_variables.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef hdf5
#include<hdf5.h>
char var2image_built_with_H5_lib_vers_info_g[] = "var2image built with " H5_VERS_INFO;

int variables2image_hdf5(
        char *vfn,
        int *Nph_p,
        int ***index_p,
        Variables **variables_p,
        CraftImage *image_p);
#endif

int variables2image_nonstd(
        char *vfn,
        int *Nph_p,
        int ***index_p,
        Variables **variables_p,
        CraftImage *image_p);

void usage(){
  fprintf(stderr, "Usages of var2image:\n");
  fprintf(stderr, "--------------------\n");
  fprintf(stderr, "var2image (-h|--help)\n");
  fprintf(stderr, "    print this help message and exit\n");
  fprintf(stderr, "var2image [-o imagefilenameprefix] variablefile field1 [field2 ... ]\n");
  fprintf(stderr, "    Extract given fields from a Variables save file\n");
  fprintf(stderr, "    and store the created images into imagefiles (default\n");
  fprintf(stderr, "    output filenames are herited from variablefile).\n");
}

/*********************************************************************************/
int main( int argc, char **argv){
  char *ofn, *vfn;
  int c, iph, Nph;
  int **index;
  Variables *variables;
  CraftImage image;

  /* Initialization */
  ofn = vfn = NULL;

  int status;
#ifdef _OPENMP
  int nr_threads=1;
  {
    /* trying to parse OMP_NUM_THREADS environment variable */
    char *env;
    int status;

    env = getenv("OMP_NUM_THREADS");
    if( env != (char *)0 ) {
      status=sscanf(env,"%d",&nr_threads);
      if (status!=1) nr_threads=1;
    }
  }
  /* set number of threads to be used by OpenMP */
  omp_set_num_threads(nr_threads);
#endif

  /* Parsing commandline arguments */
  while( (c=getopt(argc, argv, "ho:")) != -1 ){
    switch(c){
    case 'h':
      usage();
      exit(EXIT_SUCCESS);
    case 'o':
    ofn = malloc(strlen(optarg)+1);
      if (ofn == NULL ){
        fprintf(stderr,"Error parsing option -o: %s\n",optarg);
        exit(1);
      }
      strcpy(ofn,optarg);
    }
  }
  if (argc<3){
    usage();
    exit(1);
  }

  /* Variables filename */
  vfn = malloc(strlen(argv[optind])+1);
  if ( vfn == NULL ){
    fprintf(stderr,"Error parsing option -v: %s\n",optarg);
    exit(1);
  }
  strcpy(vfn,argv[optind]);
  optind ++;

  /* Default output filename prefix (same as vfn without extension) */
  if (ofn==NULL){
    char *ext;
    int lenlabel;
    ext = strrchr(vfn, '.');
    lenlabel = strlen(vfn)-strlen(ext);
    ofn = malloc(lenlabel+1);
    strncpy(ofn, vfn, lenlabel);
    ofn[lenlabel] = 0;
  }

  /* Selecting appropriate variables file reader */
  if (strstr(vfn, ".dat")!=NULL){
    status=variables2image_nonstd(vfn, &Nph, &index, &variables, &image);
    if(status!=0) return EXIT_FAILURE;
  }
  else if (strstr(vfn, ".h5")!=NULL){
#ifdef hdf5
    status=variables2image_hdf5(vfn, &Nph, &index, &variables, &image);
    if(status!=0) return EXIT_FAILURE;
#else
    fprintf(stderr, "You need to compile with -Dhdf5 (or set STD_FORMAT=yes in options.in).\n");
    exit(EXIT_FAILURE);
#endif
  }
  else{
#ifdef hdf5
    status=variables2image_hdf5(vfn, &Nph, &index, &variables, &image);
    if(status!=0) return EXIT_FAILURE;
#endif
    /* fallback if variables2image_hdf5 fails or HDF5 undefined */
    /* in case of filename without extension */
    status=variables2image_nonstd(vfn, &Nph, &index, &variables, &image);
    if(status!=0) return EXIT_FAILURE;
  }

  image.pxl.v = NULL;
  /* Retrieving field and storing images */
  {
    int ifield, status;
    char *field;
    field = NULL;

    for(ifield=optind; ifield<argc; ifield++){
      /* Getting next field to handle */
      field = realloc(field, strlen(argv[ifield])+1);
      strcpy(field, argv[ifield]);

      /* Setting image name */
      image.name = malloc(strlen(ofn)+strlen("_")+strlen(field)+1);
      sprintf(image.name,"%s_%s", ofn, field);
      image.name[strlen(ofn)+1+strlen(field)] = '\0';
      suppress_string_in_string(image.name," ");

      status = phase_to_image_variables(Nph, index, variables, field, (Material *)0, &image);
      if (status!=0) return EXIT_FAILURE;
      status = write_image(image.name, &image,"vtk");
      if (status==0)
        fprintf(stderr, "Written field \"%s\" in image %s*.vtk\n",
                field, image.name);

      /* Resetting image */
      free(image.name);
      craft_image_free(&image);
    }
    free(field);
  }

  for(iph=0; iph<Nph; iph++){
    deallocate_variables(&(variables[iph]));
    free(index[iph]);
  }
  free(variables);
  free(index);
  free(vfn);
  free(ofn);
  return EXIT_SUCCESS;
}

#ifdef hdf5
/*********************************************************************************/
int compare_version_hdf5( unsigned majnum, unsigned minnum, unsigned relnum  );

int variables2image_hdf5(
        char *vfn,
        int *Nph_p,
        int ***index_p,
        Variables **variables_p,
        CraftImage *image_p){
  hid_t h5file,h5def,h5type,att;
  char *cfn;
  char buffer[1024];
  int iph,Nph,status;
  int type[3];
  int *phase, *nppp;
  Variables *variables;

  /* Check HDF5 version (new API required), abort if  */
  //  if ( H5_VERSION_GE(1, 8, 0) <= 0 ){
  if (compare_version_hdf5(1,8,0) <= 0) {
    fprintf(stderr, "HDF5: Need to compile with HDF version 1.8 or higher.\n");
    return -1;
  }

  H5open();
  h5def = H5P_DEFAULT;

  if (access(vfn,F_OK)!=0) {
    fprintf(stderr,"variables2image_hdf5: error: file %s does not exist.\n",vfn);
    return -1;
  }
  ;
  if( (h5file=H5Fopen(vfn, H5F_ACC_RDONLY, h5def)) < 0) {
    fprintf(stderr,"HDF5: Unable to open %s\n",vfn);
    return -1;
  }

  /* Retrieve characteristic filename */
  if ( ( H5Aexists(h5file, "Characteristic file") <= 0 )
    || ( (att=H5Aopen(h5file, "Characteristic file", h5def)) < 0 )
    || ( (h5type=H5Aget_type(att)) < 0 )
    || ( H5Aread(att, h5type, buffer) < 0 )
    || ( H5Tclose(h5type) < 0 )
    || ( H5Aclose(att) < 0 )
    || ( (cfn=malloc(strlen(buffer)+1)) == NULL ) ){
    fprintf(stderr,"HDF5: Unable to read characteristic filename.\n");
    return -1;
  }
  strcpy(cfn, buffer);

  /* Build index */
  {
    CraftImage cf;
    cf.type[0] = CRAFT_IMAGE_INT;
    cf.type[1] = 1;
    cf.type[2] = 0;
    cf.pxl.v = NULL;
    read_image(cfn, &cf);

    {
      int i;
      image_p->type[0] = CRAFT_IMAGE_UNKNOWN;
      image_p->type[1] = 0;
      image_p->type[2] = 0;
      image_p->name = NULL;
      for(i=0; i<3; i++){
        image_p->n[i] = cf.n[i];
        image_p->nd[i]= cf.n[i];
        image_p->s[i] = cf.s[i];
        image_p->p[i] = cf.p[i];
      }
#if defined FFTW2 || defined FFTW3
      image_p->nd[0] = 2*(image_p->n[0]/2+1);
      // see in FFTW 3.3.2 documentation: 4.3.4 Real-data DFT Array Format
#else
      image_p->nd[0] = image_p->n[0]+2;
#endif
    }

    parse_characteristic_file(&cf, Nph_p, &phase, &nppp, index_p, *image_p);
    Nph = *Nph_p;
    type[0] = CRAFT_IMAGE_UNKNOWN;
    craft_image_reset( &cf, type );
  }

  /* Allocating array of Variables and materials */
  *variables_p = malloc(Nph*sizeof(*variables));
  if (*variables_p==NULL){
    fprintf(stderr, "Bad malloc: variables\n");
    return -1;
  }
  variables = *variables_p;

  /* Rebuild Variables array */
  {
    char phase_str[20], buffer[50];
    int ivar,Npt,Nvar;
    hid_t phase_gr, obj;
    hsize_t dimtype;

    for(iph=0; iph<Nph; iph++){
      sprintf(phase_str, "/Phase%d/", iph);
      if ( ( H5Lexists(h5file, (char *)phase_str, h5def) <= 0 )
        || ( (phase_gr=H5Gopen(h5file, (char *)phase_str, h5def)) < 0 ) ){
        continue;
      }

      /* Retrieving Number of points */
      if ( ( H5Aexists(phase_gr, "Np") <= 0 )
        || ( (att=H5Aopen(phase_gr, "Np", h5def)) < 0 )
        || ( H5Aread(att, H5T_NATIVE_INT, &Npt) < 0 )
        || ( H5Aclose(att) < 0 ) ){
        fprintf(stderr, "HDF5: Unable to parse number of points of phase %d.\n", iph);
        return -1;
      }

      /* Retrieving Number of variables */
      if ( ( H5Aexists(phase_gr, "Nvar") <= 0 )
        || ( (att=H5Aopen(phase_gr, "Nvar", h5def)) < 0 )
        || ( H5Aread(att, H5T_NATIVE_INT, &Nvar) < 0 )
        || ( H5Aclose(att) < 0 ) ){
        fprintf(stderr, "HDF5: Unable to parse number of variables of phase %d.\n", iph);
        return -1;
      }

      variables[iph].nvar = Nvar;
      variables[iph].np   = Npt;
      variables[iph].vars = malloc(Nvar*sizeof(Variable));
      if (variables[iph].vars==NULL){
        fprintf(stderr, "Unable to allocate variables[%d].vars.\n", iph);
        return -1;
      }

      /* Iterates through Variable */
      for(ivar=0; ivar<Nvar; ivar++){
        if ( ( H5Lexists(h5file, (char *)phase_str, h5def) <= 0 )
          || ( (obj=H5Oopen_by_idx(h5file, (char *)phase_str,
                    H5_INDEX_NAME, H5_ITER_NATIVE, ivar, h5def)) < 0 ) ){
          fprintf(stderr, "HDF5: Unable to open group of phase %d.\n", iph);
          return -1;
        }

        /* Get variable label */
        if ( ( H5Aexists(obj, "Name") <= 0 )
          || ( (att=H5Aopen(obj, "Name", h5def)) < 0 )
          || ( (h5type=H5Aget_type(att)) < 0 )
          || ( H5Aread(att, h5type, buffer) < 0 )
          || ( H5Tclose(h5type) < 0 )
          || ( H5Aclose(att) < 0 ) ){
          fprintf(stderr, "HDF5: Unable to parse varname %d of phase %d.\n", ivar,iph);
          return -1;
        }

        /* Get variable type */
        dimtype = 3;
        if ( ( (h5type=H5Tarray_create(H5T_NATIVE_INT, 1, &dimtype)) < 0 )
          || ( H5Aexists(obj, "type") <= 0 )
          || ( (att=H5Aopen(obj, "type", h5def)) < 0 )
          || ( H5Aread(att, h5type, (int *)type) < 0 )
          || ( H5Aclose(att) < 0 )
          || ( H5Tclose(h5type) ) ){
          fprintf(stderr,"HDF5: Unable to read type of var %d of phase %d.\n", ivar, iph);
          return -1;
        }

        variables[iph].vars[ivar].releasable = 1;
        status = allocate_variable(&(variables[iph].vars[ivar]), buffer, Npt, type, 0);
        if ( status < 0 ){
          return -1;
        }
      }
      if ( H5Gclose(phase_gr) < 0 ){
        fprintf(stderr,"HDF5: Unable to close group of phase %d.\n", iph);
        return -1;
      }
    }
  }

  /* Retrieve variables values*/
  status = read_variables_all_phases( Nph, variables, vfn, NULL, NULL, NULL, NULL);
  if ( status < 0 ){
    return -1;
  }

  free(cfn);
  free(phase);
  free(nppp);
  if ( ( H5Fclose(h5file) < 0 )
    || ( H5close() < 0 ) ){
    fprintf(stderr,"HDF5: Unable to close.\n");
    return -1;
  }
  return 0;
}
#endif


/*********************************************************************************/
int variables2image_nonstd(
        char *vfn,
        int *Nph_p,
        int ***index_p,
        Variables **variables_p,
        CraftImage *image_p){
  char *cfn, *buffer;
  int iph,Nph,status, binary;
  size_t nchar;
  int type[3];
  int *phase, *nppp;
  FILE *f;
  Variables *variables;

  if (access(vfn,F_OK)!=0) {
    fprintf(stderr,"variables2image_hdf5: error: file %s does not exist.\n",vfn);
    return -1;
  }
  f = fopen(vfn, "r");
  if(f==(FILE *)0) {
    fprintf(stderr,"variables2image_nonstd: error while opening %s.\n",vfn);
    return -1;
  }
  buffer = NULL;
  status = getline(&buffer, &nchar, f);
  if (status==-1){
    fprintf(stderr, "Unable to determine save mode (BINARY/ASCII).\n");
    exit(EXIT_FAILURE);
  }
  if (strstr(buffer, "BINARY")!=NULL){
    binary = 1;
  }
  else if  (strstr(buffer, "ASCII")!=NULL){
    binary = 0;
  }
  else{
    fprintf(stderr, "Unable to determine save mode (BINARY/ASCII).\n");
    exit(EXIT_FAILURE);
  }

  /* Read Craft version used when storing */
  status = getline(&buffer, &nchar, f);
  if (strcmp(CRAFT_VERSION_BROKEN_COMPATIBILITY, buffer)>1){
    fprintf(stderr,
        "%s was written with Craft version %s, which is incompatible\n"
        "with current version (%s). Consider converting file to the new standard.\n",
        vfn, buffer, CRAFT_VERSION);
    exit(EXIT_FAILURE);
  }
  free(buffer);
  buffer = NULL;

  /* Retrieve characteristic filename */
  {
    char buffer[2014];
    status = craft_fscanf(f, "%s\n", buffer);
    cfn = malloc(strlen(buffer)+1);
    strcpy(cfn, buffer);
  }

  /* Read (and forget) time stamp and macro strains */
  {
    int idx;
    double value;
    double Ecur[6], Eprev[6];
    status = craft_fscanf(f,"%d %lf\n", &idx, &value);
    if (status!=2) exit(EXIT_FAILURE);
    status = craft_fscanf(f, "  %lf %lf %lf %lf %lf %lf\n",
      &(Ecur[0]), &(Ecur[1]), &(Ecur[2]),
      &(Ecur[5]), &(Ecur[4]), &(Ecur[3]));
    if (status!=6) exit(EXIT_FAILURE);
    status = craft_fscanf(f, "  %lf %lf %lf %lf %lf %lf\n",
      &(Eprev[0]), &(Eprev[1]), &(Eprev[2]),
      &(Eprev[5]), &(Eprev[4]), &(Eprev[3]));
    if (status!=6) exit(EXIT_FAILURE);
  }

  /* Build index */
  {
    CraftImage cf;
    cf.type[0] = CRAFT_IMAGE_INT;
    cf.pxl.v = NULL;
    read_image(cfn, &cf);

    {
      int i;
      image_p->type[0] = CRAFT_IMAGE_UNKNOWN;
      image_p->type[1] = 0;
      image_p->type[2] = 0;
      image_p->name = NULL;
      for(i=0; i<3; i++){
	image_p->n[i] = cf.n[i];
	image_p->nd[i]= cf.n[i];
	image_p->s[i] = cf.s[i];
	image_p->p[i] = cf.p[i];
      }
#if defined FFTW2 || defined FFTW3
      image_p->nd[0] = 2*(image_p->n[0]/2+1);   // see in FFTW 3.3.2 documentation: 4.3.4 Real-data DFT Array Format
#else
      image_p->nd[0] = image_p->n[0]+2;
#endif
    }

    parse_characteristic_file(&cf, Nph_p, &phase, &nppp, index_p, *image_p);
    Nph = *Nph_p;
    type[0] = CRAFT_IMAGE_UNKNOWN;
    craft_image_reset( &cf, type );
  }

  /* Allocating array of Variables and materials */
  variables = malloc(Nph*sizeof(*variables));
  if (variables==NULL){
    fprintf(stderr, "Bad malloc: variables\n");
    exit(EXIT_FAILURE);
  }
  *variables_p = variables;

  /* Rebuild Variables array */
  /* Loop on Phase's */
  while(1){
    int idvar,Npt,Nvar,matid,phaseid,status;
    int type[3];
    buffer = NULL;

    status = getline(&buffer, &nchar, f);
    if (status==-1) break;
    if (strcmp(buffer, "\n")==0) continue;
    if (strcmp(buffer, Phase_header)==0) continue;

    status = sscanf(buffer, "%d %d %d %d %d\n",
                    &iph, &phaseid, &matid, &Npt, &Nvar);
    free(buffer);
    buffer = NULL;
    if (status!=5) exit(EXIT_FAILURE);
    variables[iph].nvar = Nvar;
    variables[iph].np   = Npt;
    variables[iph].vars = malloc(Nvar*sizeof(Variable));
    if (variables[iph].vars==NULL){
      fprintf(stderr, "Unable to allocate variables[%d].vars.\n", iph);
      exit(EXIT_FAILURE);
    }

    idvar = 0;
    /* Loop on Variable's */
    while(1){
      char *sub1, *sub2, *label;
      int lenlabel, Nel,j,k;
      Variable *var;

      status = getline(&buffer, &nchar, f);
      if (status==-1) break;
      if (strcmp(buffer, "\n")==0) continue;
      if (strcmp(buffer, "")==0) continue;
      if (strcmp(buffer, Var_header)!=0) break;
      status = getline(&buffer, &nchar, f);

      /* Reading idvar */
      status = sscanf(buffer, "%d", &idvar);
      if (status==-1) exit(EXIT_FAILURE);
      var = &(variables[iph].vars[idvar]);

      /* Get variable label */
      sub1 = sub2 = NULL;
      sub1 = strstr(buffer, "\"");
      if (sub1==NULL) exit(EXIT_FAILURE);
      sub1 += 1;
      sub2 = strstr(sub1, "\"");
      lenlabel = strlen(sub1)-strlen(sub2);
      label = malloc(lenlabel+1);
      if (label==NULL) exit(EXIT_FAILURE);
      strncpy(label, sub1, lenlabel);
      label[lenlabel] = 0;

      /* Get variable type */
      status = sscanf(sub2+1, "%d %d %d\n", &type[0], &type[1], &type[2]);
      if (status!=3) exit(EXIT_FAILURE);

      allocate_variable(var, label, Npt, type, 0);
      free(label);
      free(buffer);
      buffer = NULL;

      /* Get values */
      Nel = size_of_variable_value(type)*sizeof(double);
      if (binary){
        fread(var->values, Nel, Npt, f);
      }
      else{
        switch(type[0]) {
        case SCALAR:
          for(j=0; j<Npt; j++){
            status = craft_fscanf(f, "%lg\n", &((double *)var->values)[j]);
            if (status!=1) return -1;
          }
          break;
        case VECTOR3D:
          for(j=0; j<Npt; j++){
            status = craft_fscanf(f, "%lg %lg %lg\n",
                &((double (*)[3])var->values)[j][0],
                &((double (*)[3])var->values)[j][1],
                &((double (*)[3])var->values)[j][2]);
            if (status!=1) return -1;
          }
          break;
        case TENSOR2:
          for(j=0; j<Npt; j++){
            status = craft_fscanf(f, "%lg %lg %lg %lg %lg %lg\n",
                &((double (*)[6])var->values)[j][0],
                &((double (*)[6])var->values)[j][1],
                &((double (*)[6])var->values)[j][2],
                &((double (*)[6])var->values)[j][3],
                &((double (*)[6])var->values)[j][4],
                &((double (*)[6])var->values)[j][5]);
            if (status!=1) return -1;
          }
          break;
        case VECTOR:
          for(j=0; j<Npt; j++){
            status = getline(&buffer, &nchar, f);
            if (status==-1) return -1;
            for(k=0; k<type[1]; k++){
              status = sscanf(buffer, "%lg ", &((double **)var->values)[j][k]);
              if (status!=1) return -1;
            }
            if (craft_fscanf(f, "\n")!=1) return -1;
          }
          break;
        case MATRIX:
          for(j=0; j<Npt; j++){
            status = getline(&buffer, &nchar, f);
            if (status==-1) return -1;
            for(k=0; k<type[1]*type[2]; k++){
              status = sscanf(buffer, "%lg ", &((double **)var->values)[j][k]);
              if (status!=1) return -1;
            }
            if (craft_fscanf(f, "\n")!=1) return -1;
          }
          break;
        }
      }
    }
  }

  /* Retrieve variables values*/
  read_variables_all_phases( Nph, variables, vfn, NULL, NULL, NULL, NULL);

  free(phase);
  free(nppp);
  free(cfn);
  return 0;
}
