/******************************************************************************/
/*  Fabrice Silva
    LMA/CNRS
    November 03th 2011

    Utility to manipulate vtk images.
    Split a vectorial vtk imagefile into several files.
*/
/******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>
#include <math.h>
#include <craftimage.h>
#include <craftimage.h>

void usage(){
  fprintf(stderr, "Usages of vtk_split:\n");
  fprintf(stderr, "--------------------\n");
  fprintf(stderr, "vtk_split -h\n");
  fprintf(stderr, "    print this help message and exit\n");
  fprintf(stderr, "vtk_split [-f format] vectorial_imagefile)\n");
  fprintf(stderr, "    Extract components from a vectorial image file\n");
  fprintf(stderr, "    and save them into several scalar image files,\n");
  fprintf(stderr, "    in the given format 'i3d' or 'vtk' (default).\n");
}

int main( int argc, char **argv){
  int i, ipt, npt, status;
  char *fname_in, *fname_out, *fmt, *tmp;
  CraftImage ima_in, ima_out;

  fmt = NULL;

  while( (i=getopt(argc, argv, "hf:")) != -1 ){
    switch(i){
    case 'h':
      usage();
      exit(EXIT_SUCCESS);
      break;
    case 'f':
      fmt = malloc(strlen(optarg)+1);
      strcpy(fmt, optarg);
      break;
    }
  }
  if (optind>argc-1){
    usage();
    exit(EXIT_FAILURE);
  }
  if (fmt==NULL){
    fmt = malloc(strlen("vtk")+1);
    strcpy(fmt, "vtk");
  }

  /* Read the vectorial image file */
  fname_in = argv[argc-1];
  ima_in.type[0] = CRAFT_IMAGE_UNKNOWN;
  ima_in.type[1] = 0;
  ima_in.type[2] = 0;
  craft_image_init(&ima_in, ima_in.type);
  status = read_image(fname_in, &ima_in);
  if ((tmp=strstr(fname_in, ".vtk"))!=NULL){
    strcpy(tmp, "\0");
  }

  /* Initialize output structure */
  ima_out = ima_in;
  for(i=0; i<3; i++){
    ima_out.s[i] = ima_in.s[i];
    ima_out.p[i] = ima_in.p[i];
    ima_out.n[i] = ima_in.n[i];
    ima_out.nd[i] = ima_in.nd[i];
  }
  ima_out.type[0] = CRAFT_IMAGE_DOUBLE;
  ima_out.type[1] = 1;
  ima_out.type[2] = 0;
  npt = ima_out.nd[0]*ima_out.nd[1]*ima_out.nd[2];
  ima_out.pxl.d = malloc(npt*sizeof(double));
  if (ima_out.pxl.v == NULL){
    fprintf(stderr, "Unable to allocate memory for output image.\n");
    return -1;
  }

  /* Split it */
  switch (ima_in.type[0]) {
  case CRAFT_IMAGE_INT:
  case CRAFT_IMAGE_FLOAT:
  case CRAFT_IMAGE_DOUBLE:
    fprintf(stderr, "File %s already stores a scalar field. Nothing to split!\n", fname_in);
    break;
  case CRAFT_IMAGE_TENSOR2:
    {
      char *s[6] = {"11", "22", "33", "23", "13", "12"};
      fname_out=(char *)malloc(strlen(fname_in)+strlen("11.")+1);
      for(i=0; i<6; i++) {
        sprintf(fname_out, "%s%s", fname_in, s[i]);
        for(ipt=0; ipt<npt; ipt++)
          ima_out.pxl.d[ipt] = ima_in.pxl.t2[ipt][i];
        write_image(fname_out, &ima_out, fmt);
      }
      free(fname_out);
    }
    break;
  case CRAFT_IMAGE_VECTOR3D:
    {
      char *s[3] = {"1", "2", "3"};
      fname_out=(char *)malloc(strlen(fname_in)+strlen("1.")+1);
      for(i=0; i<3; i++) {
        sprintf(fname_out, "%s%s", fname_in, s[i]);
        for(ipt=0; ipt<npt; ipt++)
          ima_out.pxl.d[ipt] = ima_in.pxl.v3d[ipt][i];
        write_image(fname_out, &ima_out, fmt);
      }
      free(fname_out);
    }
    break;
  case CRAFT_IMAGE_VECTOR:
    {
      int d;
      char dstr[20];
      /* Length of string representation of components # */
      d = (ima_in.type[1] == 0 ? 1 : (int)(log10((double)(ima_in.type[1]))+1.));
      sprintf(dstr, "%%s%%0%dd", d);
      fname_out=(char *)malloc(strlen(fname_in)+d+strlen(".vtk")+1);
      for(i=0; i<ima_in.type[1]; i++) {
        sprintf(fname_out, dstr, fname_in, i+1);
        ima_out.name = fname_out;
        for(ipt=0; ipt<npt; ipt++)
          ima_out.pxl.d[ipt] = ima_in.pxl.a[i+ima_in.type[1]*ipt];
        write_image(fname_out, &ima_out, fmt);
      }
      free(fname_out);
      break;
    }
  default:
    fprintf(stderr,"%s : invalid case\n",__func__);
    exit(EXIT_FAILURE);
    break;
  }
  free(ima_out.pxl.d);
  exit(EXIT_SUCCESS);
}
