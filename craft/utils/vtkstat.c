/******************************************************************************/
/*  Herve Moulinec
    LMA/CNRS
    January 13th 2014

    calculates some statistics on an image
*/
/******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>
#include <math.h>
#include <craftimage.h>
#include <craftimage.h>

void usage(){
  fprintf(stderr, "Usages of vtkstat:\n");
  fprintf(stderr, "--------------------\n");
  fprintf(stderr, "vtkstat -h\n");
  fprintf(stderr, "    print this help message and exit\n");
  fprintf(stderr, "vtkstat [-f format] imagefile)\n");
}

  /*-------------------------------------------------------------------*/
int main( int argc, char **argv){
  int i, ipt, npt, status;
  char *fname_in, *fmt, *tmp;
  CraftImage ima;

  int bref=0;

  fmt = NULL;

  /*-------------------------------------------------------------------*/
  /* options */
  /*-------------------------------------------------------------------*/
  while( (i=getopt(argc, argv, "hbf:")) != -1 ){
    switch(i){
    case 'h':
      usage();
      exit(EXIT_SUCCESS);
      break;
    case 'b':
      bref=1;
      break;
    case 'f':
      fmt = malloc(strlen(optarg)+1);
      strcpy(fmt, optarg);
      break;
    }
  }
  if (optind>argc-1){
    usage();
    exit(EXIT_FAILURE);
  }
  if (fmt==NULL){
    fmt = malloc(strlen("vtk")+1);
    strcpy(fmt, "vtk");
  }

  /*-------------------------------------------------------------------*/
  /* Read the image file */
  /*-------------------------------------------------------------------*/
  fname_in = argv[argc-1];
  ima.type[0] = CRAFT_IMAGE_UNKNOWN;
  ima.type[1] = 0;
  ima.type[2] = 0;
  craft_image_init(&ima, ima.type);
  status = read_image(fname_in, &ima);
  if ((tmp=strstr(fname_in, ".vtk"))!=NULL){
    strcpy(tmp, "\0");
  }

  /*-------------------------------------------------------------------*/
  /* print image format  */
  /*-------------------------------------------------------------------*/
  printf("Image %s\n",ima.name);
  char dtf[50];
  craft_image_data_format(dtf,ima);
  printf("data type: %s\n",dtf);

  printf("number of pixels:         (%16d , %16d , %16d)\n",       ima.n[0], ima.n[1], ima.n[2]);
  printf("coordinates of 1st pixel: (%16.8g , %16.8g , %16.8g)\n", ima.s[0], ima.s[1], ima.s[2]);
  printf(
	 "sample step :             (%16.8g , %16.8g , %16.8g)\n", ima.p[0],ima.p[1],ima.p[2]
         );
  /*-------------------------------------------------------------------*/
  if(bref==1){
    exit(EXIT_SUCCESS);
  }
  printf("\n");
  /*-------------------------------------------------------------------*/
  /* statistics */
  /*-------------------------------------------------------------------*/
  switch (ima.type[0]) {
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_CHAR:
    {
      double s,s2;
      char xmin, xmax;
      
      int i;
      char value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.c[0];
      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.c[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16d\n",xmin);
      printf("  max=%16d\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));  
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UCHAR:
    {
      double s,s2;
      unsigned char xmin, xmax;
      
      int i;
      unsigned char value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.uc[0];

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.uc[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16d\n",xmin);
      printf("  max=%16d\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_INT:
    {
      double s,s2;
      int xmin, xmax;
      
      int i;
      int value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.i[0];

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.i[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16d\n",xmin);
      printf("  max=%16d\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UINT:
    {
      double s,s2;
      unsigned int xmin, xmax;
      
      int i;
      unsigned int value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.ui[0];

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.ui[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16d\n",xmin);
      printf("  max=%16d\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_FLOAT:
    {
      double s,s2;
      float xmin, xmax;
      
      int i;
      float value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.f[0];

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.f[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16.8g\n",xmin);
      printf("  max=%16.8g\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_DOUBLE:
    {
      double s,s2;
      double xmin, xmax;
      
      int i;
      double value;

      s=0.;
      s2=0.;
      xmin = xmax = ima.pxl.d[0];

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        value = ima.pxl.d[i];
        s += (double)value;
        s2 += (double)value*(double)value;
        xmax = ( value>xmax ? value : xmax);
        xmin = ( value<xmin ? value : xmin);
      }

      s /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      printf("  min=%16.8g\n",xmin);
      printf("  max=%16.8g\n",xmax);
      printf("  mean = %16.8g\n",s);
      s2 = s2  / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",s2);
      printf("  root mean square = %16.8g\n",sqrt(s2));
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_TENSOR2:
  case CRAFT_IMAGE_VECTOR3D:
  case CRAFT_IMAGE_VECTOR:
  case CRAFT_IMAGE_FOURIER_DOUBLE:
  case CRAFT_IMAGE_FOURIER_TENSOR2:
  case CRAFT_IMAGE_FOURIER_VECTOR3D:
  case CRAFT_IMAGE_FOURIER_VECTOR:
    {
      int nc;
      nc = ima.type[1];
      double s[nc],s2[nc];
      double xmin[nc], xmax[nc];
      //      printf("===========> nc=%d\n",nc);
      
      int i;
      int ic;
      double value;

      for(ic=0;ic<nc;ic++){
        s[ic]=0.;
        s2[ic]=0.;
        xmin[ic] = xmax[ic] = ima.pxl.d[0*nc+ic];
      }

      for(i=0; i<ima.n[0]*ima.n[1]*ima.n[2]; i++) {
        for(ic=0;ic<nc;ic++){
          value = ima.pxl.d[i*nc+ic];
          s[ic] += (double)value;
          s2[ic] += (double)value*(double)value;
          xmax[ic] = ( value>xmax[ic] ? value : xmax[ic]);
          xmin[ic] = ( value<xmin[ic] ? value : xmin[ic]);
        }
      }
      for(ic=0;ic<nc;ic++){
        s[ic] /= (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2];
      }
      for(ic=0;ic<nc;ic++){
        s2[ic] = s2[ic] / ( (double)ima.n[0]*(double)ima.n[1]*(double)ima.n[2] - 1.) - s[ic]*s[ic] ;
      }

      printf("  min =                ");
      for(ic=0;ic<nc;ic++) printf("%16.8g ",xmin[ic]);
      printf("\n");

      printf("  max =                ");
      for(ic=0;ic<nc;ic++) printf("%16.8g ",xmax[ic]);
      printf("\n");

      printf("  mean =               ");
      for(ic=0;ic<nc;ic++) printf("%16.8g ",s[ic]);
      printf("\n");

      printf("  variance =           ");
      for(ic=0;ic<nc;ic++) printf("%16.8g ",s2[ic]);
      printf("\n");

      printf("  standard deviation = ");
      for(ic=0;ic<nc;ic++) printf("%16.8g ",sqrt(s2[ic]) );
      printf("\n");

    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  case CRAFT_IMAGE_UNKNOWN:
  default:
    fprintf(stderr,"vtkstat: %s has an unkown image file format.\n",ima.name);
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);
}
