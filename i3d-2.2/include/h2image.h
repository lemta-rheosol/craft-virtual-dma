/* 
   HM 18 septembre 2001

   definition des structures "image" contenant
   des donnees scalaires de differents types
*/
#ifndef H2IMAGE
#define H2IMAGE
/*--------------------------------------------------*/
/* les differents type d'image */
#ifndef HIMAUNKNOWN 
#define HIMAUNKNOWN 0
#endif
#ifndef HIMACHAR
#define HIMACHAR 1
#endif
#ifndef HIMAUCHAR
#define HIMAUCHAR 2
#endif
#ifndef HIMAINT
#define HIMAINT 3
#endif
#ifndef HIMAUINT
#define HIMAUINT 4
#endif
#ifndef HIMAFLOAT
#define HIMAFLOAT 5
#endif
#ifndef HIMADOUBLE
#define HIMADOUBLE 6
#endif

/*--------------------------------------------------*/
/* type d'image generique */
typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  void *pxl;
  char *name;
}
H2Image;



typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  double *pxl;
  char *name;
}
H2Imaged;

typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  float *pxl;
  char *name;
}
H2Imagef;

typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  int *pxl;
  char *name;
}
H2Imagei;

typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  unsigned int *pxl;
  char *name;
}
H2Imageui;

typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  char *pxl;
  char *name;
}
H2Imagec;

typedef struct {
  char nom[200];
  int type;
  int n[3];
  int nd[3];
  double p[3][3];
  double s[3];
  unsigned char *pxl;
  char *name;
}
H2Imageuc;

/*--------------------------------------------------*/
/* initialisation de l'image */
int construct_h2image(H2Image *image, int type);
int construct_h2image_d(H2Imaged *imaged);
int construct_h2image_f(H2Imagef *imagef);
int construct_h2image_i(H2Imagei *imagei);
int construct_h2image_ui(H2Imageui *imageui);
int construct_h2image_c(H2Imagec *imagec);
int construct_h2image_uc(H2Imageuc *imageuc);

/* allocation du champ de pixels */
int allocate_h2image(H2Image *ima);
int reallocate_h2image(H2Image *ima);
/* qq verifications sur la taille de l'image et son type */
int verif_h2image(const H2Image *ima);
/*--------------------------------------------------*/
/* longueur des mots selon le type de donnees */
int data_length(int data_type);

/*--------------------------------------------------*/
/* copie une image i3d dans une autre */
/*
int copy_i3d( H2Image *dest, const H2Image *src, 
	      double dest_min, double dest_max,
	      double src_min, double src_max);
*/
void copy_header_h2image( H2Image *dst, H2Image *src);
int x2i( H2Image *ima, double *x, double *ix);
int i2x( H2Image *ima, double *x, double *ix);
#endif
