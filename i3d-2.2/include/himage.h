/* le himage de la version 1.0. Je le garde pour des raisons
   de compatibilitÚs.
*/
/* definition du type flottant sur 8 octets */

#ifdef CRAY
#define REAL8 float
#else	     
#define REAL8 double
#endif

#define HIMACHAR 1
#define HIMAUCHAR 2
#define HIMAINT 3
#define HIMAUINT 4
#define HIMAFLOAT 5
#define HIMADOUBLE 6

typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   void *pxl;
   int type;
 }
HImage;


typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   float *pxl;
   int type;
 }
HImagef;

typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   double *pxl;
   int type;
 }
HImaged;


typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   int *pxl; 
   int type; 
 }
HImagei;

typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   unsigned int *pxl; 
   int type; 
 }
HImageui;


typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   unsigned char *pxl; 
   int type; 
 }
HImageuc;

typedef struct {
   int dim;
   int *n;
   int *nd;
   REAL8 *p;
   REAL8 *s;
   char *pxl; 
   int type; 
 }
HImagec;


