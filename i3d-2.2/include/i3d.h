/* fichier d'include de la librairie i3d version 2.0
   HM 18/09/01

   J'ai allege et modifie pas mal de choses par rapport a la
   version 1.0. J'ai notamment vire tout ce qui concernait VMS ou Cray.
   Les versions ne sont pas compatibles a priori.
*/
#ifndef _I3D_H
#define _I3D_H 1
#include <stdio.h>
#include <h2image.h>

/* type de fichier i3d version 1 ou 2 (et au del�). */
#define I3D1 1
#define I3D2 2

/* teste si la machine sur laquelle on est, code les binaires
   au format IEEE little endian, IEEE big endian, ou autre.
*/
#ifndef BIGENDIAN
#define BIGENDIAN 0
#endif
#ifndef LITTLEENDIAN
#define LITTLEENDIAN 1
#endif
#ifndef OTHERENDIAN
#define OTHERENDIAN -1
#endif

/* teste si la machine sur laquelle on est code les binaires
 * au format IEEE big-endian ou little-endian
 */
int test_endian(void);

/* inversion des poids dans un tableau de nbelem mot de taille s */
int invpds( void *src,  void * dest, size_t s, int nbelem);
int invpds2( void *src,  void * dest, size_t s, int nbelem);

/* ecriture de l'en tete d'un fichier i3d */
int write_header_i3d(
		  int n1, int n2, int n3,
	          double s1, double s2, double s3, 
	          double *p1, double *p2, double *p3, 
	          int type_fichier, int codage, 
		  FILE *file);

/* lecture de l'en tete d'un fichier i3d */
int read_header_i3d(
	 int *n1, int *n2, int *n3,
	 double *s1, double *s2, double *s3, 
	 double *p1, double *p2, double *p3, 
	 int *type_fichier, int *codage, 
	 FILE *file);


/* ecriture des pixels ds un fichier-image */
int write_pixels(
		 int n1, int n2, int n3, 
		 int data_type, int codage,
		 int nd1, int nd2, int nd3,
		 void *pxl,
		 FILE *f);

/* lecture des pixels ds une image */
/* donnees tableau et donnees fichier de meme type (float, double, ...)*/
int read_pixels(
		int n1, int n2, int n3, 
		int data_type, int codage,
		int nd1, int nd2, int nd3,
		void *pxl,
		FILE *f);
/* donnees tableau et donnees fichier pas forcement de meme type (float, double, ...)*/
int read_pixels_t(
	       int n1, int n2, int n3, 
	       int data_type, int codage,
	       int nd1, int nd2, int nd3,
	       int table_type, void **pxl,
	       FILE *f);



/* Ecriture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 */
#define write_image_i3d write_image_i3d_
int write_image_i3d(
		    int *n1, int *n2, int *n3,
		    double *s1, double *s2, double *s3,
		    double *p1, double *p2, double *p3,
		    int *data_type, int *codage, 
		    int *nd1, int *nd2, int *nd3,
		    void *pxl,
		    char *nom);

/* Lecture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 * Si le champ pxl contient la valeur 0, on alloue l'espace necessaire
 * au stockage de l'image.
 */
int read_image_i3d(
		    int *n1, int *n2, int *n3,
		    double *s1, double *s2, double *s3,
		    double *p1, double *p2, double *p3,
		    int *data_type, int *codage, 
		    int *nd1, int *nd2, int *nd3,
		    void **pxl,
		    char *nom);

/* read_image_i3d_t: pour le cas ou les donnees fichier et les donnees tableau 
 sont de types differents */
int read_image_i3d_t(
		    int *n1, int *n2, int *n3,
		    double *s1, double *s2, double *s3,
		    double *p1, double *p2, double *p3,
		    int *data_type, int *codage, 
		    int *nd1, int *nd2, int *nd3,
		    int *pxl_type,
		    void **pxl,
		    char *nom);
#define read_image_i3d_2 read_image_i3d_2_
int read_image_i3d_2(
		    int *n1, int *n2, int *n3,
		    double *s1, double *s2, double *s3,
		    double *p1, double *p2, double *p3,
		    int *data_type, int *codage, 
		    int *nd1, int *nd2, int *nd3,
		    void *pxl,
		    char *nom);


int read_file_version_i3d(FILE *f);

/* lecture d'un fichier image vers une structure H2Image */
int ri3d( char *name, H2Image *image);
/* �criture dans un fichier image d'une structure H2Image */
int wi3d( char *name, H2Image *image);
/* lecture d'un fichier image vers une structure H2Image */
int ri3d_2( H2Image *ima );
/* �criture dans un fichier image d'une structure H2Image */
int wi3d_2(H2Image *image);
/* lecture d'un plan (en z) d'une image i3d */
int ri3dz( int strin, int iz, H2Image *image);

#endif
