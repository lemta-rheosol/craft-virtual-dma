* HM 16/11/2001
* fichier d'include pour l'interfacage Fortran 77
* de i3d version 2.0
*
      integer BIGENDIAN
      integer LITTLEENDIAN
      parameter (BIGENDIAN=0, LITTLEENDIAN=1)

      integer HIMACHAR
      integer HIMAUCHAR
      integer HIMAINT
      integer HIMAUINT
      integer HIMAFLOAT
      integer HIMADOUBLE
      parameter (HIMACHAR=1)
      parameter (HIMAUCHAR=2)
      parameter (HIMAINT=3)
      parameter (HIMAUINT=4)
      parameter (HIMAFLOAT=5)
      parameter (HIMADOUBLE=6)

      integer read_image_i3d_f
      integer write_image_i3d_f

