#ifndef _VTK_H
#define _VTK_H 1

#include <h2image.h>
/*****************************************************************/
/* write a i3d image into a legacy vtk file in BINARY */
int wvtk( H2Image *ima );
/*****************************************************************/
/* write a i3d image into a legacy vtk file either in BINARY or 
   ASCII format*/
int wvtk0( H2Image *ima , int binary );
/*****************************************************************/
/* read a i3d image from a legacy vtk file */
int rvtk( H2Image *ima );
/*********************************************************************************/
/* tests wether "filename" is in i3d format, or legacy VTK format */
int i3d_or_vtk_file_format( const char *filename);

#endif
