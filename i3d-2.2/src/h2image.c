/********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <h2image.h>
#include <errno.h>
/********************************************************************/
/* allocation de l'espace necessaire aux pixels
   d'une image.

   retourne 0 en cas de succes,
   retourne -1 en cas d'echec.
*/
int allocate_h2image( H2Image *ima ) {
  int i;
  int n=0;
  int ierr;

  ierr = verif_h2image(ima);
  if (ierr!=0) {
    fprintf(stderr,"allocate_h2image: erreur a la verif de l'image.\n");
    return -1;
  }

  for(i=0; i<3; i++) {
    if (ima->nd[i]>0) {
      if (n==0) n = ima->nd[i];
      else n *= ima->nd[i];
    }
  }

  switch(ima->type){
  case HIMADOUBLE:
    ima->pxl=(double *)malloc(n*sizeof(double));
    break;
  case HIMAFLOAT:
    ima->pxl=(float *)malloc(n*sizeof(float));
    break;
  case HIMAINT:
    ima->pxl=(int *)malloc(n*sizeof(int));
    break;
  case HIMAUINT:
    ima->pxl=(unsigned int *)malloc(n*sizeof(unsigned int));
    break;
  case HIMACHAR:
    ima->pxl=(char *)malloc(n*sizeof(char));
    break;
  case HIMAUCHAR:
    ima->pxl=(char *)malloc(n*sizeof(char));
    break;
  }

  if (ima->pxl==0) {
    return -1;
  }

  return 0;


}

/********************************************************************/
/* allocation de l'espace necessaire aux pixels
   d'une image.

   retourne 0 en cas de succes,
   retourne -1 en cas d'echec.

   en cas d'echec, l'image en entree N'EST PAS modifiee.
*/
int reallocate_h2image( H2Image *ima ) {
  int i;
  int n=0;
  int ierr;
  void *p;


  ierr = verif_h2image(ima);
  if (ierr!=0) {
    fprintf(stderr,"reallocate_h2image: erreur a la verif de l'image.\n");
    return -1;
  }

  for(i=0; i<3; i++) {
    if (ima->nd[i]>0) {
      if (n==0) n = ima->nd[i];
      else n *= ima->nd[i];
    }
  }

  switch(ima->type){
  case HIMADOUBLE:
    p=realloc(ima->pxl,n*sizeof(double));
    break;
  case HIMAFLOAT:
    p=realloc(ima->pxl,n*sizeof(float));
    break;
  case HIMAINT:
    p=realloc(ima->pxl,n*sizeof(int));
    break;
  case HIMAUINT:
    p=realloc(ima->pxl,n*sizeof(unsigned int));
    break;
  case HIMACHAR:
    p=realloc(ima->pxl,n*sizeof(char));
    break;
  case HIMAUCHAR:
    p=realloc(ima->pxl,n*sizeof(unsigned char));
    break;
  default:
    fprintf(stderr,"reallocate_h2image: unknown type (%d).\n", ima->type);
    return -1;
  }

  if (p==0) {
    fprintf(stderr,"reallocate_h2image: pb a l'allocation de memoire.\n");
    if (errno==ENOMEM) {
      fprintf(stderr,"        m�moire insuffisante.\n");
      return -1;
    }
  }
  else{
    ima->pxl=p;
  }
  return 0;


}

/********************************************************************/
/* Verification (partielle) du bon format de la h2image donnee.
   Verification:
   * que le dimensionnement de l'image est coherent
   * que le type de l'image est connu
   
   Retourne 0 si l'image est correcte,
   retourne -1 si l'image n'est pas correcte.
*/
int verif_h2image(const H2Image *ima) {
  int i;

  for(i=0; i<3; i++) {
    if ( 
	(ima->n[i]<0) || (ima->nd[i]<0) || (ima->nd[i]<ima->n[i]) 
	) {
      fprintf(stderr,"verif_h2image: taille d'image non valide.\n");
      return -1;
    }
  }

  if ( 
      (ima->type!=HIMADOUBLE) &&
      (ima->type!=HIMAFLOAT) &&
      (ima->type!=HIMAINT) &&
      (ima->type!=HIMAUINT) &&
      (ima->type!=HIMACHAR) &&
      (ima->type!=HIMAUCHAR) 
      ) {
    fprintf(stderr,"verif_h2image: type d'image non valide.\n");
    return -1;
  }

  return 0;
}
/********************************************************************/
/* longueur des mots selon le type des donnees */
int data_length(int data_type){
  
  switch(data_type) {
  case HIMAUNKNOWN:
    return 0;
  case HIMADOUBLE:
    return sizeof(double);
  case HIMAFLOAT:
    return sizeof(float);
    break;
  case HIMACHAR  : 
    return sizeof(char);
    break;
  case HIMAUCHAR :
    return sizeof(unsigned char);
    break;
  case HIMAINT   :
    return sizeof(int);
    break;
  case HIMAUINT  :
    return sizeof(unsigned int);
    break;
  default:
    fprintf(stderr,"data_length: type de donn�es impossible\n");
    return -1;
  }
}
/**********************************************************************/
/* calcule la position en coordonnees physiques a partir des indices
   de pixels d'une image.
*/
int i2x( H2Image *ima, double *x, double *ix){

  x[0] = 
    ima->s[0] +
    ima->p[0][0]*ix[0] +
    ima->p[1][0]*ix[1] +
    ima->p[2][0]*ix[2] ;

  x[1] = 
    ima->s[1] +
    ima->p[0][1]*ix[0] +
    ima->p[1][1]*ix[1] +
    ima->p[2][1]*ix[2] ;

  x[2] = 
    ima->s[2] +
    ima->p[0][2]*ix[0] +
    ima->p[1][2]*ix[1] +
    ima->p[2][2]*ix[2] ;

  return 0;
}

/**********************************************************************/
/* inversion d'une matrice 3x3 par la m�thode des cofacteurs          */
void inversion_3x3( double a[3][3], double inva[3][3], int *err){

  int i,j;
  double comatrix[3][3];
  double deta, invdeta;

  /* co matrix */
  comatrix[0][0] =  a[1][1]*a[2][2]-a[1][2]*a[2][1];
  comatrix[0][1] = -a[1][0]*a[2][2]+a[1][2]*a[2][0];
  comatrix[0][2] =  a[1][0]*a[2][1]-a[1][1]*a[2][0];

  comatrix[1][0] = -a[0][1]*a[2][2]+a[0][2]*a[2][1];
  comatrix[1][1] =  a[0][0]*a[2][2]-a[0][2]*a[2][0];
  comatrix[1][2] = -a[0][0]*a[2][1]+a[0][1]*a[2][0];

  comatrix[2][0] =  a[0][1]*a[1][2]-a[0][2]*a[1][1];
  comatrix[2][1] = -a[0][0]*a[1][2]+a[0][2]*a[1][0];
  comatrix[2][2] =  a[0][0]*a[1][1]-a[0][1]*a[1][0];

  deta = a[0][0]*comatrix[0][0]+a[0][1]*comatrix[0][1]+a[0][2]*comatrix[0][2];
  if(deta<1.e-30) {
    *err = -1;
    fprintf(stderr,"warning: invalid 3x3 matrix inversion. determinant is null\n");
    return;
  }
  else{
    *err=0;
    invdeta = 1./deta;
  }
  
  /* transposed co-matrix */
  for(i=0;i<3;i++){
    for(j=0;j<3;j++) {
      inva[i][j] = invdeta*comatrix[j][i];
    }
  }

}

/**********************************************************************/
/* calcule les indices de pixels d'une image a partir de la position 
   en coordonnees physiques.
*/
void inversion_3x3( double a[3][3], double inva[3][3], int *err);
int x2i( H2Image *ima, double *x, double *ix){
  int ierr;
    static double ips[3][3];
    static H2Image pimage={ 
    "",
    0, {0,0,0}, {0,0,0}, 
    { {     0.,0.,0.}, {0.,0.,0.}, {0.,0.,0.} }, 
    {0.,0.,0.}, 
    (float *)0 };

    int k;
    int stat=0;
    int i,j;

    if ( 
	(fabs(ima->p[0][0]-pimage.p[0][0])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[0][1])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[0][2])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[1][0])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[1][1])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[1][2])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[2][0])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[2][1])>1.e-10) ||
	(fabs(ima->p[0][0]-pimage.p[2][2])>1.e-10) 
	) {
      for(i=0;i<3;i++) {
	for(j=0; j<3;j++) {
	  pimage.p[i][j] = ima->p[i][j];
	}
      }
      /*
      inversion_(&(ima->p[0][0]),&(ips[0][0]),&n,&err);
      if (err>1.e-10) {
	fprintf(stderr,"x2i: l'erreur lors de l'inversion depasse 1.e-10.\n");
	stat=1;
      }       */
      inversion_3x3(ima->p,ips,&ierr);
      if (ierr!=0) {
	fprintf(stderr,"x2i: l'erreur lors de l'inversion depasse 1.e-10.\n");
	stat=1;
      }
    }

    for(k=0; k<3; k++) {
      ix[k] = 
	ips[0][k]*(x[0]-ima->s[0]) +
	ips[1][k]*(x[1]-ima->s[1]) +
	ips[2][k]*(x[2]-ima->s[2]) ;
      if ( (ix[k]<0) || (ix[k]>=ima->n[k]) ) stat=2;
    }

    

    return stat;
}
/***************************************************************************/
/* copie l'en tete d'une image ds une autre                                */
/* i.e. tout, sauf le type d'image (float, int, ...),                      */
/*                 le champ de pixels,                                     */
/*                 le nom de l'image                                       */
void copy_header_h2image( H2Image *dst, H2Image *src){
  int i,j;

  for(i=0; i<3; i++) {
    dst->n[i]=src->n[i];
    dst->nd[i]=src->nd[i];
    dst->s[i]=src->s[i];
    for(j=0; j<3; j++) {
      dst->p[i][j] = src->p[i][j];
    }
  }


}

/**************************************************************************************/
int construct_h2image_d(H2Imaged *image) {
  return construct_h2image( (H2Image *)image, HIMADOUBLE);
}
/**************************************************************************************/
int construct_h2image_f(H2Imagef *image) {
  return construct_h2image( (H2Image *)image, HIMAFLOAT);
}
/**************************************************************************************/
int construct_h2image_i(H2Imagei *image) {
  return construct_h2image( (H2Image *)image, HIMAINT);
}
/**************************************************************************************/
int construct_h2image_c(H2Imagec *image) {
  return construct_h2image( (H2Image *)image, HIMACHAR);
}
/**************************************************************************************/
int construct_h2image_ui(H2Imageui *image) {
  return construct_h2image( (H2Image *)image, HIMAUINT);
}
/**************************************************************************************/
int construct_h2image_uc(H2Imageuc *image) {
  return construct_h2image( (H2Image *)image, HIMAUCHAR);
}
/**************************************************************************************/
int construct_h2image(H2Image *image,int type){
  int i,j;

  image->nom[0] = 0;
  image->name = image->nom;
  image->pxl=(void *)0;
  image->type = type;

  for (i=0;i<3;i++) {
    image->n[i] = 0;
    image->nd[i] = 0;
    image->s[i] = 0.;

    for(j=0;j<3;j++) {
      image->p[i][j]=0.;
    }

  }
  return 0;
}
