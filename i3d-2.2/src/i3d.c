/*
HM

ensemble de fonctions permettant la lecture et l'ecriture de fichiers
image 3d en un format defini par moi, et ce, INDEPENDAMMENT DE LA
MACHINE HOTE.

Les valeurs sont codees en un format independant de la machine:
  les entiers sont codes en LITTLE ENDIAN
  les flottants sont codes au format IEEE

Les fonctions sont prevues pour les systemes suivants:
  Tru64Unix, Linux, IRIX, NEC (j'esp�re!).
Cela doit marcher pour toute machine utilisant le codage IEEE
des binaires (Little Endian ou Big Endian).

*/
#define _DEFAULT_SOURCE
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#include <i3d.h>
#ifndef H2IMAGE
#include <h2image.h>
#endif

/* taille d'un entier et d'un flottant ds le format little endian utilis� */
#define TE  4
#define TF  4
/* taille en octets d'un enregistrement dans un fichier-image */
#define TEFI 512u

/*****************************************************************************/
/*
   FONCTIONS D'INVERSION POIDS FORTS POIDS FAIBLES
 */
/*****************************************************************************/

/*
   From www.intel.com/design/intarch/papers/endian.pdf
   Run-Time Byte Order Test

   Returns either LITTLEENDIAN, BIGENDIAN or OTHERENDIAN.
   Only called once.
*/
int test_endian(void) {
  /* short are 2-bytes long, char 1-byte long. */
  static int endianess=-2;
  short i = 1; /* 0x01 */
  char *p = (char *) &i;

  if (endianess==-2) {
    if (p[0]==0){
      endianess=BIGENDIAN;
#ifdef DEBUG
      fprintf(stderr,"Big Endian\n");
#endif
    }
    else if (p[0]==1){
      endianess=LITTLEENDIAN;
#ifdef DEBUG
      fprintf(stderr,"Little Endian\n");
#endif
    }
    else {
      endianess=OTHERENDIAN;
      fprintf(stderr,"Other Endian\n");
    }
  }

  return(endianess);
}

/*****************************************************************************/
/*
   HM 23 octobre 1996

   inversion poids fort poids faible sur des mots de 2, 4 ou 8 octets
   la source et la destination peuvent etre la meme zone memoire

   Sur PUCK (486 DX 2 50 sous linux) invpds2 est plus rapide.
   */
/*===========================================================================*/
int invpds( void *src,  void * dest, size_t s, int nbelem) {
  char *ps, *pd, c;
  /* inversion poids fort/faible sur nbelem mots courts */
#ifdef DEBUG
  fprintf(stderr,"inversion des poids \n");
#endif
#ifdef DEBUG
  fprintf(stderr,"taille=%d nb=%d \n",s,nbelem);
#endif
  switch (s) {
  case 1:
    for(
        ps=(char *)src, pd=(char *)dest;
        ps<(char *)src+nbelem;
        ps++, pd++
        ){
      *pd = *ps;
    }
    return(0);;

  case 2:
    for (
         pd = (char *)dest,
         ps = (char *)src;
         pd < (char *)dest+s*nbelem;
         ps+=s, pd+=s) {
      c = *ps;
      *(pd) = *(ps+1);
      *(pd+1) = c;
    }
    return(0);


    /* mots longs */
  case 4:
#ifdef DEBUG
    fprintf(stderr,"invpds:%d %x %d %x\n",
            *(int *)dest,
            *(int *)dest,
            *(int *)src,
            *(int *)src
            );
#endif

    for (
         pd = (char *)dest,
         ps = (char *)src;
         pd < (char *)dest + s*nbelem;
         ps+=s, pd+=s) {
      c = *ps;
#ifdef DEBUG
      fprintf(stderr,"invpds c=%x %x\n",c,*ps);
#endif
      *(pd) = *(ps+3);
#ifdef DEBUG
      fprintf(stderr,"invpds *pd=%x *ps+3=%x\n",*pd,*(ps+3));
#endif
      *(pd+3) = c;
#ifdef DEBUG
      fprintf(stderr,"invpds *pd+3=%x c=%x\n",*(pd+3),c);
#endif
      c = *(ps+1);
#ifdef DEBUG
      fprintf(stderr,"invpds c=%x *ps+1%x\n",c,*(ps+1));
#endif

      *(pd+1) = *(ps+2);
#ifdef DEBUG
      fprintf(stderr,"invpds *pd+1=%x *ps+2%x\n",*(pd+1),*(ps+2));
#endif

      *(pd+2) = c;
#ifdef DEBUG
      fprintf(stderr,"invpds: %d %x %d %x\n",
              *(int *)pd,
              *(int *)pd,
              *(int *)ps,
              *(int *)ps
              );
#endif

    }

    return(0);

    /* mots de 8 octets */
  case 8:
    for (
         pd = (char *)dest,
         ps = (char *)src;
         pd < (char *)dest+s*nbelem;
         ps+=s, pd+=s) {

      c = *ps;
      *(pd) = *(ps+7);
      *(pd+7) = c;

      c = *(ps+1);
      *(pd+1) = *(ps+6);
      *(pd+6) = c;

      c = *(ps+2);
      *(pd+2) = *(ps+5);
      *(pd+5) = c;

      c = *(ps+3);
      *(pd+3) = *(ps+4);
      *(pd+4) = c;

    }
    return(0);

  default:
    fprintf(stderr,"Erreur: inversion des poids.\n");
    fprintf(stderr,"  Les mots doivent etre de taille = 2 , 4 ou 8 octets.\n");
    fprintf(stderr,"  Ici ils sont de %d octet(s).\n",(int)s);
    return (-1);
  }

  /* should not reach this point */
  return -1;
}



/**************************************************************************/
/*                                                                        */
/* 2eme version plus rapide                                               */
/*                                                                        */
/**************************************************************************/
int invpds2( void *src,  void * dest, size_t s, int nbelem) {
  /* inversion poids fort/faible sur nbelem mots courts */
  register unsigned long *plsrc, *pldest;
  register unsigned short *pssrc, *psdest;
  register unsigned int *pisrc,*pidest;
  register long long *pdsrc, *pddest;
  register unsigned char *pucsrc, *pucdest;

  int i;

  switch(s) {
  case 1:
    pucsrc = (unsigned char *)src;
    pucdest = (unsigned char *)dest;
#pragma vdir nodep
    for(i=0;i<nbelem;i++){
      pucdest[i] = pucsrc[i];
    }
    break;
  case 2:
    /* si les short ne font pas 2 octets */
    if(sizeof(unsigned short)!=2) {
      fprintf(stderr,"Probleme ds invpds2: contacter HM.\n");
      fprintf(stderr,"taille des unsigned short = %d au lieu de 2\n",
              (int)sizeof(unsigned short));
      exit(1);
    }

    pssrc=(unsigned short *)src;
    psdest=(unsigned short *)dest;
#pragma vdir nodep
    for (i=0; i<nbelem; i++) {
      psdest[i] = (pssrc[i] << 8) | (pssrc[i] >> 8);
    }
    return (0);

    /* mots longs */
    /* il faut chercher un type qui n'aie que 4 octets, j'essaie les long
       et les unsigned int
       */
  case 4:
    /* si les long ne font pas 4 octets */
    if(sizeof(unsigned long)==4) {

      plsrc=(unsigned long *)src;
      pldest=(unsigned long *)dest;
#pragma vdir nodep
      for(i=0; i<nbelem; i++) {
        pldest[i] = (
                     (plsrc[i] << 24) |
                     ((plsrc[i] & 0xff00) << 8) | ((plsrc[i] >> 8) & 0xff00) |
                     (plsrc[i] >> 24)
                     );
      }
    } else if( sizeof(unsigned int) == 4) {

      pisrc=(unsigned int *)src;
      pidest=(unsigned int *)dest;
#pragma vdir nodep
      for(i=0; i<nbelem; i++) {
        pidest[i] = (
                     (pisrc[i] << 24) |
                     ((pisrc[i] & 0xff00) << 8) | ((pisrc[i] >> 8) & 0xff00) |
                     (pisrc[i] >> 24)
                     );
      }
    }else {
      fprintf(stderr,"Probleme ds invpds2: contacter HM.\n");
      fprintf(stderr,"taille des unsigned long = %d au lieu de 4\n",
              (int)sizeof(unsigned long));

      exit(1);
    }



    return (0);

    /* mots de 8 octets */
  case 8:
    /* si les long long  ne font pas 8 octets */
    if(sizeof(long long)!=8) {
      fprintf(stderr,"Probleme ds invpds2: contacter HM.\n");
      fprintf(stderr,"taille des long long = %ld au lieu de 8\n",
              sizeof(long long));
      exit(1);
    }

    pdsrc=(long long *)src;
    pddest=(long long *)dest;
#pragma vdir nodep
    for(i=0; i<nbelem; i++) {
      pddest[i] = (
                   ( ((pdsrc[i]) & 0x000000ff ) << 56 ) |
                   ( ((pdsrc[i]) >> 56) & 0x000000ff  ) |
                   ( ((pdsrc[i]) & 0x0000ff00 ) << 40 ) |
                   ( ((pdsrc[i]) >> 40) & 0x0000ff00  ) |
                   ( ((pdsrc[i]) & 0x00ff0000 ) << 24 ) |
                   ( ((pdsrc[i]) >> 24) & 0x00ff0000  ) |
                   ( ((pdsrc[i]) & 0xff000000 ) << 8  ) |
                   ( ((pdsrc[i]) >>  8) & 0xff000000  )
                   );
    }

    return (0);



  default:
    fprintf(stderr,"Erreur: inversion des poids.\n");
    fprintf(stderr,"Les mots doivent etre de taille = 1, 2, 4 ou 8 octets.\n");
    return (-1);
  }

  /* should not reach this point */
  return -1;
}


/****************************************************************************
 * remplit l'en tete du fichier au format HM2 (version de i3d version 2.0
 * et au del� (???)).
 *
 * taille de l'image:        n1, n2, n3
 * coord. du pt en bas a gauche:
 *                           s1, s2, s3
 * pas d'echantillonnage:    p1, p2, p3
 * type de donnees:          data_type
 * codage des donnees:       endianness
 *
 *
 * identificateur du fichier:
 *                           f
 *
 * ATTENTION: le fichier doit avoir ete ouvert en ecriture auparavant.
 *
 * valeur retournee: 0 si succes
 *                   autre si echec
 ****************************************************************************/
int write_header_i3d(
                  int n1, int n2, int n3,
                  double s1, double s2, double s3,
                  double *p1, double *p2, double *p3,
                  int data_type, int endianness,
                  FILE *f)
{
  char ctype[10];
  char ccode[20];
  int ibuf;
  double dbuf;

  unsigned char inversion_necessaire;
  /* on ecrit le type de fichier dont il s'agit */
  switch(data_type){
  case HIMADOUBLE:
    memcpy(ctype,"HM2RD     ",10);
    break;
  case HIMAFLOAT:
    memcpy(ctype,"HM2RS     ",10);
    break;
  case HIMACHAR  :
    memcpy(ctype,"HM2C      ",10);
    break;
  case HIMAUCHAR :
    memcpy(ctype,"HM2UC     ",10);
    break;
  case HIMAINT   :
    memcpy(ctype,"HM2I      ",10);
    break;
  case HIMAUINT  :
    memcpy(ctype,"HM2UI     ",10);
    break;
  default:
    fprintf(stderr,"write_header_i3d: type de fichier impossible\n");
    return -1;
  }
  /*
  switch(data_type){
  case HIMADOUBLE:
    strcpy(ctype,"HM2RD     ");
    break;
  case HIMAFLOAT:
    strcpy(ctype,"HM2RS     ");
    break;
  case HIMACHAR  :
    strcpy(ctype,"HM2C      ");
    break;
  case HIMAUCHAR :
    strcpy(ctype,"HM2UC     ");
    break;
  case HIMAINT   :
    strcpy(ctype,"HM2I      ");
    break;
  case HIMAUINT  :
    strcpy(ctype,"HM2UI     ");
    break;
  default:
    fprintf(stderr,"write_header_i3d: type de fichier impossible\n");
    return -1;
  }
  */
  fwrite(ctype,10,1,f);
  /* on ecrit le type de codage binaire */
  switch(endianness){
  case BIGENDIAN:
    memcpy(ccode,"Big Endian          ",20);
    break;
  case LITTLEENDIAN:
    memcpy(ccode,"Little Endian       ",20);
    break;
  case OTHERENDIAN:
    memcpy(ccode,"Non Endian          ",20);
    break;
  default:
    fprintf(stderr,"write_header_i3d: type de codage binaire impossible.\n");
    return -1;
  }
  /*
  switch(endianness){
  case BIGENDIAN:
    strcpy(ccode,"Big Endian          ");
    break;
  case LITTLEENDIAN:
    strcpy(ccode,"Little Endian          ");
    break;
  case OTHERENDIAN:
    strcpy(ccode,"Non Endian          ");
    break;
  default:
    fprintf(stderr,"write_header_i3d: type de codage binaire impossible.\n");
    return -1;
  }
  */
  fwrite(ccode,20,1,f);

  /* faut il faire une inversion poids forts/poids faibles? */
  inversion_necessaire =
    ( (test_endian()==BIGENDIAN) && (endianness==LITTLEENDIAN) ) ||
    ( (test_endian()==LITTLEENDIAN) && (endianness==BIGENDIAN) ) ;

  /* on ecrit la taille de l'image */
  if ( inversion_necessaire ) {
    invpds2(&n1,&ibuf,sizeof(n1),1);
    fwrite(&ibuf,sizeof(ibuf),1,f);
    invpds2(&n2,&ibuf,sizeof(n2),1);
    fwrite(&ibuf,sizeof(ibuf),1,f);
    invpds2(&n3,&ibuf,sizeof(n3),1);
    fwrite(&ibuf,sizeof(ibuf),1,f);
  }
  else {
    fwrite(&n1,sizeof(n1),1,f);
    fwrite(&n2,sizeof(n2),1,f);
    fwrite(&n3,sizeof(n3),1,f);
  }

  /* on ecrit les coordonnees du 1er pixel de l'image */
  if ( inversion_necessaire ) {
    invpds2(&s1,&dbuf,sizeof(s1),1);
    fwrite(&dbuf,sizeof(dbuf),1,f);
    invpds2(&s2,&dbuf,sizeof(s2),1);
    fwrite(&dbuf,sizeof(dbuf),1,f);
    invpds2(&s3,&dbuf,sizeof(s3),1);
    fwrite(&dbuf,sizeof(dbuf),1,f);
  }
  else {
    fwrite(&s1,sizeof(s1),1,f);
    fwrite(&s2,sizeof(s2),1,f);
    fwrite(&s3,sizeof(s3),1,f);
  }


  /* on ecrit les pas d'echantillonnage de l'image */
  if ( inversion_necessaire ) {
    invpds2(p1,&dbuf,sizeof(p1[0]),3);
    fwrite(&dbuf,sizeof(dbuf),3,f);

    invpds2(p2,&dbuf,sizeof(p2[0]),3);
    fwrite(&dbuf,sizeof(dbuf),3,f);

    invpds2(p3,&dbuf,sizeof(p3[0]),3);
    fwrite(&dbuf,sizeof(dbuf),3,f);
  }
  else {
    fwrite(p1,sizeof(p1[0]),3,f);
    fwrite(p2,sizeof(p2[0]),3,f);
    fwrite(p3,sizeof(p3[0]),3,f);

  }

  return 0;
}

/****************************************************************************
 * lit l'en tete du fichier:
 * taille de l'image:        n1, n2, n3
 * coord. du pt en bas a gauche:
 *                           s1, s2, s3
 * pas d'echantillonnage:    p1, p2, p3
 * type de donnees:          data_type
 * codage des donnees:       endianness
 *
 * identificateur du fichier:
 *                           f
 * ATTENTION: il faut que le fichier ait d�j� �t� ouvert en lecture.
 ****************************************************************************/
int read_header_i3d(
                int *n1, int *n2, int *n3,
                double *s1, double *s2, double *s3,
                double *p1, double *p2, double *p3,
                int *data_type, int *endianness, FILE *f)
{
  char ctype[10];
  char ccode[20];
  int taille_buffer;
  unsigned char inversion_necessaire;
  float rbuf;
  int type_fichier;

  /* on lit le type de fichier */
  fread(ctype,10,1,f);
  /* ancien format (13d v1.0) */
  if ( strncmp(ctype,"HMRS",4)==0) {
    type_fichier=I3D1;
    *data_type = HIMAFLOAT;

  } else if ( strncmp(ctype,"HM2RS",5)==0) {
    type_fichier=I3D2;
    *data_type = HIMAFLOAT;

  } else if ( strncmp(ctype,"HM2RD",5)==0) {
    type_fichier=I3D2;
    *data_type = HIMADOUBLE;

  } else if ( strncmp(ctype,"HM2I",4)==0) {
    type_fichier=I3D2;
    *data_type = HIMAINT;

  } else if ( strncmp(ctype,"HM2UI",5)==0) {
    type_fichier=I3D2;
    *data_type = HIMAUINT;

  } else if ( strncmp(ctype,"HM2C",4)==0) {
    type_fichier=I3D2;
    *data_type = HIMACHAR;

  } else if ( strncmp(ctype,"HM2UC",5)==0) {
    type_fichier=I3D2;
    *data_type = HIMAUCHAR;
  }
  else{
    return -1;
  }

  /* on lit le type de codage du binaire */
  fread(ccode,20,1,f);
  if (strncmp(ccode,"Big Endian",10)==0) {
    *endianness=BIGENDIAN;
  }
  else if (strncmp(ccode,"Little Endian",13)==0) {
    *endianness=LITTLEENDIAN;
  }
  else{
    *endianness = OTHERENDIAN;
  }

  /* faut il faire une inversion poids forts/poids faibles? */
  inversion_necessaire =
    ( (test_endian()==BIGENDIAN) && (*endianness==LITTLEENDIAN) ) ||
    ( (test_endian()==LITTLEENDIAN) && (*endianness==BIGENDIAN) ) ;

  /* si on est au format i3d version 1.0, il faut lire la taille du header */
  if (type_fichier==I3D1) {
    fread(&taille_buffer,sizeof(taille_buffer),1,f);
    if ( inversion_necessaire ) {
      invpds2(&taille_buffer,&taille_buffer,sizeof(taille_buffer),1);
    }
    taille_buffer*=TEFI; /* conversion en nb d'octets */
  }

  /* lecture de la taille de l'image */
  fread(n1,sizeof(*n1),1,f);
  fread(n2,sizeof(*n2),1,f);
  fread(n3,sizeof(*n3),1,f);
  if(inversion_necessaire) {
    invpds2(n1,n1,sizeof(*n1),1);
    invpds2(n2,n2,sizeof(*n2),1);
    invpds2(n3,n3,sizeof(*n3),1);
  }


  /* lecture des coord. du 1er pixel */
  if (type_fichier==I3D1) {
    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    *s1 = (double)rbuf;
    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    *s2 = (double)rbuf;
    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    *s3 = (double)rbuf;
  } else {
    fread(s1,sizeof(*s1),1,f);
    fread(s2,sizeof(*s2),1,f);
    fread(s3,sizeof(*s3),1,f);
    if(inversion_necessaire) {
      invpds2(s1,s1,sizeof(*s1),1);
      invpds2(s2,s2,sizeof(*s2),1);
      invpds2(s3,s3,sizeof(*s3),1);
    }
  }

  /* lecture des pas d'echantillonnage */
  if (type_fichier==I3D1) {
    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    p1[0] = (double)rbuf;

    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    p2[1] = (double)rbuf;

    fread(&rbuf, sizeof(rbuf),1,f);
    if (inversion_necessaire) {
      invpds2(&rbuf,&rbuf,sizeof(rbuf),1);
    }
    p3[2] = (double)rbuf;

    p1[1] = p1[2] = 0.;
    p2[0] = p2[2] = 0.;
    p3[0] = p3[1] = 0.;
  } else {
    fread(p1,sizeof(*p1),3,f);
    fread(p2,sizeof(*p2),3,f);
    fread(p3,sizeof(*p3),3,f);
    if(inversion_necessaire) {
      invpds2(p1,p1,sizeof(*p1),3);
      invpds2(p2,p2,sizeof(*p2),3);
      invpds2(p3,p3,sizeof(*p3),3);
    }
  }

  /* si c'est une image au format i3d 1.0, il faut se positionner en debut
     du champ de pixel */
  if (type_fichier==I3D1) {
    fseek( f, taille_buffer, SEEK_SET);
  }
  return 0;
}

/*****************************************************************************
 donne la version i3d du fichier (si, si! cela sert!)
*****************************************************************************/
int read_file_version_i3d(FILE *f) {
  int type_fichier;
  char ctype[10];

  /* on lit le type de fichier */
  fread(ctype,10,1,f);
  /* ancien format (13d v1.0) */
  if ( strncmp(ctype,"HMRS",4)==0) {
    type_fichier=I3D1;
  } else if ( strncmp(ctype,"HM2",3)==0) {
    type_fichier=I3D2;
  }
  else {
    type_fichier = -1;
  }
  /* ne pas oublier de se repositionner en debut de fichier */
  fseek( f, 0, SEEK_SET);

  return type_fichier;
}



/*****************************************************************************
 * vient �crire ds le fichier file le contenu de l'image 3d de n1xn2xn3
 * pixels, contenue ds le tableau pxl de nd1xnd2xn3.
 * Le type des donn�es pixel est donn� par data_type.
 *****************************************************************************/
int write_pixels(
               int n1, int n2, int n3,
               int data_type, int endianness,
               int nd1, int nd2, int nd3,
               void *pxl,
               FILE *f) {

  unsigned char inversion_necessaire;
  int i2,i3;
  void *buf;
  int length;

  int error;
  /*-------------------------------------------------------------------------*/
  /* faut il faire une inversion poids forts/poids faibles? */
  inversion_necessaire =
    ( (test_endian()==BIGENDIAN) && (endianness==LITTLEENDIAN) ) ||
    ( (test_endian()==LITTLEENDIAN) && (endianness==BIGENDIAN) ) ;
  /*-------------------------------------------------------------------------*/
  /* on verifie que tout est OK en entree: */
  error=0;
  if (n1<1) {
    error++;
    fprintf(stderr,"libi3d:write_pixel: ERROR: n1=%d\n",n1);
  }
  if (n2<1) {
    error++;
    fprintf(stderr,"libi3d:write_pixel: ERROR: n2=%d\n",n2);
  }
  if (n3<1) {
    fprintf(stderr,"libi3d:write_pixel: a: WARNING: n3=%d => forced to 1\n",n3);
    n3 = 1;
  }
  if(error!=0) return -1;


  if (nd1<n1) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd1<n1 : forced to n1 (%d)\n",n1);
    nd1 = n1;
  }
  if (nd2<n2) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd2<n2 : forced to n2 (%d)\n",n2);
    nd2 = n2;
  }
  if (nd3<n3) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd1<n1 : forced to n3 (%d)\n",n3);
    nd3 = n3;
  }


  /*-------------------------------------------------------------------------*/
  length = data_length(data_type);
  if(length==-1) {
    return -1;
  }
  /*
  switch(data_type) {
  case HIMADOUBLE:
    length = sizeof(double);
    break;
  case HIMAFLOAT:
    length = sizeof(float);
    break;
  case HIMACHAR  :
    length = sizeof(char);
    break;
  case HIMAUCHAR :
    length = sizeof(unsigned char);
    break;
  case HIMAINT   :
    length = sizeof(int);
    break;
  case HIMAUINT  :
    length = sizeof(unsigned int);
    break;
  default:
    fprintf(stderr,"write_pixels: type de donn�es impossible\n");
    return -1;
  }
  */

  buf = malloc((size_t)length*(size_t)n1);

  for(i3=0; i3<n3; i3++) {
    for(i2=0; i2<n2; i2++) {
      memcpy(buf,(char *)pxl+length*nd1*(i2 + nd2*i3), n1*length);
      if (inversion_necessaire){
        invpds2(buf,buf,length,n1);
      }
      fwrite(buf, length, n1, f);
    }
  }
  free(buf);

  return 0;

}
/*****************************************************************************
 * vient lire ds le fichier file le contenu de l'image 3d de n1xn2xn3
 * pixels, et le place ds le tableau pxl de nd1xnd2xn3.
 * Le type des donn�es pixel est donn� par data_type.
 * Cette fonctionne suppose que le tableau r�sultat et le fichier lu contiennent
 * des donn�es de meme type.
 *****************************************************************************/
int read_pixels(
                int n1, int n2, int n3,
                int data_type, int endianness,
                int nd1, int nd2, int nd3,
                void *pxl,
                FILE *f) {

  unsigned char inversion_necessaire;
  int i2,i3;
  void *p;
  int length;
  int istat;
  /*-------------------------------------------------------------------------*/
  /* faut il faire une inversion poids forts/poids faibles? */
  inversion_necessaire =
    ( (test_endian()==BIGENDIAN) && (endianness==LITTLEENDIAN) ) ||
    ( (test_endian()==LITTLEENDIAN) && (endianness==BIGENDIAN) ) ;
  /*-------------------------------------------------------------------------*/
  length=data_length(data_type);


  for(i3=0; i3<n3; i3++) {
    for(i2=0; i2<n2; i2++) {
      p = (char *)pxl+(size_t)length*(size_t)nd1*((size_t)i2 + (size_t)nd2*(size_t)i3);
      istat = fread(p, length, n1, f);
      if (istat != n1) {
        return -1;
      }
      if (inversion_necessaire){
        invpds2(p,p,length,n1);
      }
    }
  }
  /*  printf("%d %d\n",data_type,length); */
  /*
  if (data_type==HIMADOUBLE){
    int i1, i;
    double s;
    s=0.;
    i=0;
    for(i3=0;i3<n3;i3++) {
      for(i2=0;i2<n2;i2++) {
        for(i1=0; i1<n1; i1++) {
          s += ((double *)pxl)[i];
          i++;
        }
      }
    }
    printf("s=%lf\n",s/(n1*n2*n3));
  }
  */

return 0;
}


/*****************************************************************************
 * vient lire ds le fichier file le contenu de l'image 3d de n1xn2xn3
 * pixels, et le place ds le tableau pxl de nd1xnd2xn3.
 * Le type des donn�es dans le fichier est donn� par data_type.
 * Le type des donn�es du tableau pxl est donn� par table_type.
 * Cette fonctionne admet que le tableau r�sultat et le fichier lu contiennent
 * des donn�es de types DIFFERENTS.
 *****************************************************************************/
int read_pixels_t(
               int n1, int n2, int n3,
               int data_type, int endianness,
               int nd1, int nd2, int nd3,
               int table_type, void **pxl,
               FILE *f) {


  unsigned char inversion_necessaire;
  int istat;
  void *p,*pp;
  //  int length;
  size_t length;
  int i1,i2,i3;
  int i;
  
  /* Check nd>=n */
  if ( (nd1<n1) || (nd2<n2) || (nd3<n3) ){
    fprintf(stderr, "Please check that nd[i]>=n[i].\n");
    return -1;
  }
  *pxl = realloc(*pxl, (size_t)nd1*(size_t)nd2*(size_t)nd3*(size_t)data_length(table_type));
  if (*pxl==NULL){
    fprintf(stderr, "Unable to reallocate pxl in read_pixels_t (i3d.c)\n");
    exit(EXIT_FAILURE);
  }

  if (data_type == table_type) {
    istat = read_pixels(
                        n1, n2, n3,
                        data_type, endianness,
                        nd1, nd2, nd3,
                        *pxl,
                        f);
    return istat;
  }

  /*-------------------------------------------------------------------------*/
  /* faut il faire une inversion poids forts/poids faibles? */
  inversion_necessaire =
    ( (test_endian()==BIGENDIAN) && (endianness==LITTLEENDIAN) ) ||
    ( (test_endian()==LITTLEENDIAN) && (endianness==BIGENDIAN) ) ;
  /*-------------------------------------------------------------------------*/
  /* on lit les donnees ds le fichier */
  length=data_length(data_type);

  p = malloc((size_t)nd1*(size_t)nd2*(size_t)nd3*(size_t)length);

  for(i3=0; i3<n3; i3++) {
    for(i2=0; i2<n2; i2++) {
      pp = (char *)p+length*nd1*(i2 + nd2*i3);
      istat = fread(pp, length, n1, f);
      if (istat != n1) return -1;
      if (inversion_necessaire){
        invpds2(pp,pp,length,n1);
      }
    }
  }
  /*-------------------------------------------------------------------------*/
  /* on copie les donnees lues ds le tableau */
  if (table_type==HIMADOUBLE) {
    double *pd;
    pd = (double *)*pxl;
    if (data_type==HIMAFLOAT) {
      float *pf;
      pf = (float *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pd[i] = (double)pf[i];
          }
        }
      }
    }
    else if (data_type==HIMAINT) {
      int *pi;
      pi = (int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pd[i] = (double)pi[i];
          }
        }
      }
    }
    else if (data_type==HIMAUINT) {
      unsigned int *pui;
      pui = (unsigned int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pd[i] = (double)pui[i];
          }
        }
      }
    }
    else if (data_type==HIMACHAR) {
      char *pc;
      pc = (char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pd[i] = (double)pc[i];
          }
        }
      }
    }
    else if (data_type==HIMAUCHAR) {
      unsigned char *puc;
      puc = (unsigned char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pd[i] = (double)puc[i];
          }
        }
      }
    }


  }
  /*- - - - - - - - - - - - - - - - - - - - -*/
  else if (table_type==HIMAFLOAT) {
    float *pf;
    pf = (float *)*pxl;
    if (data_type==HIMADOUBLE) {
      double *pd;
      pd = (double *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pf[i] = (float)pd[i];
          }
        }
      }
    }
    else if (data_type==HIMAINT) {
      int *pi;
      pi = (int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pf[i] = (float)pi[i];
          }
        }
      }
    }
    else if (data_type==HIMAUINT) {
      unsigned int *pui;
      pui = (unsigned int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pf[i] = (float)pui[i];
          }
        }
      }
    }
    else if (data_type==HIMACHAR) {
      char *pc;
      pc = (char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pf[i] = (float)pc[i];
          }
        }
      }
    }
    else if (data_type==HIMAUCHAR) {
      unsigned char *puc;
      puc = (unsigned char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pf[i] = (float)puc[i];
          }
        }
      }
    }



  }
  /*- - - - - - - - - - - - - - - - - - - - -*/
  else if (table_type==HIMAINT) {
    int *pi;
    pi = (int *)*pxl;
    if (data_type==HIMAFLOAT) {
      float *pf;
      pf = (float *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pi[i] = (int)pf[i];
          }
        }
      }
    }
    else if (data_type==HIMADOUBLE) {
      double *pd;
      pd = (double *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pi[i] = (int)pd[i];
          }
        }
      }
    }
    else if (data_type==HIMAUINT) {
      unsigned int *pui;
      pui = (unsigned int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pi[i] = (int)pui[i];
          }
        }
      }
    }
    else if (data_type==HIMACHAR) {
      char *pc;
      pc = (char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pi[i] = (int)pc[i];
          }
        }
      }
    }
    else if (data_type==HIMAUCHAR) {
      unsigned char *puc;
      puc = (unsigned char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pi[i] = (int)puc[i];
          }
        }
      }
    }


  }
  /*- - - - - - - - - - - - - - - - - - - - -*/
  else if (table_type==HIMAUINT) {
    unsigned int *pui;
    pui = (unsigned int *)*pxl;
    if (data_type==HIMAFLOAT) {
      float *pf;
      pf = (float *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pui[i] = (unsigned int)pf[i];
          }
        }
      }
    }
    else if (data_type==HIMADOUBLE) {
      double *pd;
      pd = (double *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pui[i] = (unsigned int)pd[i];
          }
        }
      }
    }
    else if (data_type==HIMAINT) {
      int *pi;
      pi = (int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pui[i] = (unsigned int)pi[i];
          }
        }
      }
    }
    else if (data_type==HIMACHAR) {
      char *pc;
      pc = (char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pui[i] = (unsigned int)pc[i];
          }
        }
      }
    }
    else if (data_type==HIMAUCHAR) {
      unsigned char *puc;
      puc = (unsigned char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pui[i] = (unsigned int)puc[i];
          }
        }
      }
    }

  }
  /*- - - - - - - - - - - - - - - - - - - - -*/
  else if (table_type==HIMACHAR) {
    char *pc;
    pc = (char *)*pxl;

    if (data_type==HIMADOUBLE) {
      double *pd;
      pd = (double *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pc[i] = (char)pd[i];
          }
        }
      }
    }
    else if (data_type==HIMAFLOAT) {
      float *pf;
      pf = (float *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pc[i] = (char)pf[i];
          }
        }
      }
    }
    else if (data_type==HIMAINT) {
      int *pi;
      pi = (int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pc[i] = (char)pi[i];
          }
        }
      }
    }
    else if (data_type==HIMAUINT) {
      unsigned int *pui;
      pui = (unsigned int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pc[i] = (char)pui[i];
          }
        }
      }
    }
    else if (data_type==HIMAUCHAR) {
      unsigned char *puc;
      puc = (unsigned char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            pc[i] = (char)puc[i];
          }
        }
      }
    }

  }
  /*- - - - - - - - - - - - - - - - - - - - -*/
  else if (table_type==HIMAUCHAR) {
    unsigned char *puc;
    puc = (unsigned char *)*pxl;

    if (data_type==HIMADOUBLE) {
      double *pd;
      pd = (double *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            puc[i] = (unsigned char)pd[i];
          }
        }
      }
    }
    else if (data_type==HIMAFLOAT) {
      float *pf;
      pf = (float *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            puc[i] = (unsigned char)pf[i];
          }
        }
      }
    }
    else if (data_type==HIMAINT) {
      int *pi;
      pi = (int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            puc[i] = (unsigned char)pi[i];
          }
        }
      }
    }
    else if (data_type==HIMAUINT) {
      unsigned int *pui;
      pui = (unsigned int *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            puc[i] = (unsigned char)pui[i];
          }
        }
      }
    }
    else if (data_type==HIMACHAR) {
      char *pc;
      pc = (char *)p;
      for(i3=0; i3<n3; i3++) {
        for(i2=0; i2<n2; i2++) {
          for(i1=0; i1<n1; i1++) {
            i = i1+nd1*(i2+nd2*i3);
            puc[i] = (unsigned char)pc[i];
          }
        }
      }
    }

  }

  free(p);

  return 0;
}


/**************************************************************************/
/* Ecriture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 */
int write_image_i3d(
                    int *n1, int *n2, int *n3,
                    double *s1, double *s2, double *s3,
                    double *p1, double *p2, double *p3,
                    int *data_type, int *endianness,
                    int *nd1, int *nd2, int *nd3,
                    void *pxl,
                    char *nom){
  FILE *f;
  int istat;

  int error;
  /*-------------------------------------------------------------------------*/

  f=fopen(nom,"w");
  if (f==(FILE *)0) {
    fprintf(stderr,"write_image_i3d: pb a l'ouverture de %s.\n",nom);
    return -1;
  }

  /*-------------------------------------------------------------------------*/
  /* on verifie que tout est OK en entree: */
  error=0;
  if (*n1<1) {
    error++;
    fprintf(stderr,"libi3d:write_pixel: ERROR: n1=%d\n",*n1);
  }
  if (*n2<1) {
    error++;
    fprintf(stderr,"libi3d:write_pixel: ERROR: n2=%d\n",*n2);
  }
  if (*n3<1) {
    fprintf(stderr,"libi3d:write_pixel: a: WARNING: n3=%d => forced to 1\n",*n3);
    *n3 = 1;
  }
  if(error!=0) return -1;


  if (*nd1<*n1) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd1<n1 : forced to n1 (%d)\n",*n1);
    *nd1 = *n1;
  }
  if (*nd2<*n2) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd2<n2 : forced to n2 (%d)\n",*n2);
    *nd2 = *n2;
  }
  if (*nd3<*n3) {
    fprintf(stderr,"libi3d:write_pixel: WARNING: nd1<n1 : forced to n3 (%d)\n",*n3);
    *nd3 = *n3;
  }

  /*-------------------------------------------------------------------------*/







  istat = write_header_i3d(
                       *n1, *n2, *n3,
                       *s1, *s2, *s3,
                       p1, p2, p3,
                       *data_type, *endianness,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "write_image_i3d: pb a l'ecriture de l'en-tete de %s.\n",
            nom);
    return -1;
  }


  istat = write_pixels(
                       *n1, *n2, *n3,
                       *data_type, *endianness,
                       *nd1, *nd2, *nd3,
                       pxl,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "write_image_i3d: pb a l'ecriture des pixels de %s.\n",
            nom);
    return -1;
  }



  fclose(f);
  return 0;
}
/**************************************************************************/
/* Lecture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 * Si le champ pxl contient la valeur 0, on alloue l'espace necessaire
 * au stockage de l'image.
 */
int read_image_i3d(
                    int *n1, int *n2, int *n3,
                    double *s1, double *s2, double *s3,
                    double *p1, double *p2, double *p3,
                    int *data_type, int *endianness,
                    int *nd1, int *nd2, int *nd3,
                    void **pxl,
                    char *nom){
  FILE *f;
  int istat;
  int length;


  f=fopen(nom,"r");
  if (f==(FILE *)0) {
    fprintf(stderr,"read_image_i3d: pb a l'ouverture de %s.\n",nom);
    return -1;
  }

  istat = read_header_i3d(
                       n1, n2, n3,
                       s1, s2, s3,
                       p1, p2, p3,
                       data_type, endianness,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d: pb a la lecture de l'en-tete de %s.\n",
            nom);
    return -1;
  }

  if (*pxl == (void *)0) {
    length = data_length(*data_type);
    /*    if (*nd1<=0) { */
    *nd1 = *n1;
    *nd2 = *n2;
    *nd3 = *n3;
    /*    } */
    *pxl = malloc((size_t)length*(size_t)(*nd1)*(size_t)(*nd2)*(size_t)(*nd3));
  }


  istat = read_pixels(
                       *n1, *n2, *n3,
                       *data_type, *endianness,
                       *nd1, *nd2, *nd3,
                       *pxl,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d: pb a la lecture des pixels de %s.\n",
            nom);
    return -1;
  }



  fclose(f);
  return 0;
}
/**************************************************************************/
/* Lecture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 * Si le champ pxl contient la valeur 0, on alloue l'espace necessaire
 * au stockage de l'image.
 * LES DONNEES DS LE FICHIER ET DS LE TABLEAU NE SONT FORCEMENT DE MEME TYPE.
 */
int read_image_i3d_t(
                    int *n1, int *n2, int *n3,
                    double *s1, double *s2, double *s3,
                    double *p1, double *p2, double *p3,
                    int *data_type, int *endianness,
                    int *nd1, int *nd2, int *nd3,
                    int *pxl_type,
                    void **pxl,
                    char *nom){
  FILE *f;
  int istat;
  int length;


  f=fopen(nom,"r");
  if (f==(FILE *)0) {
    fprintf(stderr,"read_image_i3d: pb a l'ouverture de %s.\n",nom);
    return -1;
  }

  istat = read_header_i3d(
                       n1, n2, n3,
                       s1, s2, s3,
                       p1, p2, p3,
                       data_type, endianness,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d: pb a la lecture de l'en-tete de %s.\n",
            nom);
    return -1;
  }

  if ( *pxl_type==HIMAUNKNOWN ) {
    *pxl_type=*data_type;
  }


  if ( *data_type != *pxl_type ) {
    /*
    fprintf(stderr,"warning: \n");
    fprintf(stderr,"  data_type in file are %d\n",*data_type);
    fprintf(stderr,"  data_type in H2Image struct are %d\n",*pxl_type);
    */
  }

  if (*pxl == (void *)0) {
    length = data_length(*pxl_type);
    /*    if (*nd1<=0) { */
    *nd1 = *n1;
    *nd2 = *n2;
    *nd3 = *n3;
    /*    } */
    *pxl = malloc((size_t)length*(size_t)(*nd1)*(size_t)(*nd2)*(size_t)(*nd3));
  }


  istat = read_pixels_t(
                       *n1, *n2, *n3,
                       *data_type, *endianness,
                       *nd1, *nd2, *nd3,
                       *pxl_type,
                       pxl,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d: pb a la lecture des pixels de %s.\n",
            nom);
    return -1;
  }



  fclose(f);
  return 0;
}
/**************************************************************************/
/* Lecture d'une image au format i3d a partir d'un champ de pixel,
 * de la taille, etc ...
 * On passe le pointeur pxl dans lequel on stockera l'image
 * apres qu'on aura verifie que l'espace derriere ce pixel
 * est suffisant (cet espace est donne par *nd1 x *nd2 x *nd3
 * Si l'espace de stockage derri�re pxl est trop petit
 * c'est a dire si nd1<n1 ou nd2<n2 ou nd3<n3, on lit quand meme
 * l'en tete mais le contenu de l'image n'est pas lu.
 * Dans ce cas, read_image_i3d_2 retourne la valeur -1.
 */
int read_image_i3d_2(
                    int *n1, int *n2, int *n3,
                    double *s1, double *s2, double *s3,
                    double *p1, double *p2, double *p3,
                    int *data_type, int *endianness,
                    int *nd1, int *nd2, int *nd3,
                    void *pxl,
                    char *nom){
  FILE *f;
  int istat;

  f=fopen(nom,"r");
  if (f==(FILE *)0) {
    fprintf(stderr,"read_image_i3d_2: pb a l'ouverture de %s.\n",nom);
    return -1;
  }

  istat = read_header_i3d(
                       n1, n2, n3,
                       s1, s2, s3,
                       p1, p2, p3,
                       data_type, endianness,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d_2: pb a la lecture de l'en-tete de %s.\n",
            nom);
    return -1;
  }

  if ( (*nd1<*n1) || (*nd2<*n2) || (*nd3<*n3) ) {
    /*    fprintf(stderr,
            "read_image_i3d_2: espace de stockage insuffisant pour y mettre l'image.\n");
    */
    return -1;
  }

  istat = read_pixels(
                       *n1, *n2, *n3,
                       *data_type, *endianness,
                       *nd1, *nd2, *nd3,
                       pxl,
                       f);

  if (istat!=0) {
    fprintf(stderr,
            "read_image_i3d_2: pb a la lecture des pixels de %s.\n",
            nom);
    return -1;
  }



  fclose(f);
  return 0;
}
/**************************************************************************/
/* HM 5 mars 97
   lecture d'image i3d et stockage dans une structure H2Image
   */
#ifdef OLD
int ri3d( char *name, H2Image *ima) {
  int istat;
  int endianness;
  istat = read_image_i3d(
                         &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                         &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                         &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                         &(ima->type), &endianness,
                         &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                         &(ima->pxl),
                         name);

  /* copie du nom de l'image */
  ima->name = (char *)malloc( strlen(name) + 1 );
  strcpy( ima->name, name );

  return istat;

}
#endif

/* nll version 4 oct 2010 */
/* si le type des donn�es dans la structure ima est donn�e en entr�e,
   le contenu de l'image est converti (si n�cessaire) dans ce type de
   donn�es lors de la lecture du fichier.
   Si le type de donn�e de la structure en entr�e est HIMAUNKNOWN,
   en sortie la structure HImage contiendra des donn�es de m�me
   type que celles du fichiers (pas de for�age de type).
   avec le m�me type).
*/
int ri3d( char *name, H2Image *ima) {
  int istat;
  int endianness;
  int data_type;

  int i3d_or_vtk_file_format( const char *filename);

  switch ( i3d_or_vtk_file_format( name ) ){
  case 1:
    /* format i3d 1.0 */
    istat = read_image_i3d_t(
                             &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                             &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                             &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                             &data_type, &endianness,
                             &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                             &(ima->type),
                             &(ima->pxl),
                             name);
    break;
  case 2:
    /* format i3d 2.0 */
    istat = read_image_i3d_t(
                             &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                             &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                             &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                             &data_type, &endianness,
                             &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                             &(ima->type),
                             &(ima->pxl),
                             name);
    break;
  case 3:
    ima->name = name;
    int rvtk( H2Image *ima );
    istat = rvtk( (H2Image *)ima );
    break;
  default:
    fprintf(stderr,"ri3d error: unknown file format\n");
    istat = -1;
  }

  /* copie du nom de l'image */
  ima->name = malloc( strlen(name) + 1 );
  strcpy( ima->name, name );

  return istat;

}


/**************************************************************************/
/* HM 5 mars 97
   ecriture d'image i3d et stockage dans une structure H2Image
   */
int wi3d( char *name, H2Image *ima) {
  int istat;
  int endianness;
  endianness=test_endian();
  if (name==0) {
    name = ima->name;
  }
  istat = write_image_i3d(
                         &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                         &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                         &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                         &(ima->type), &endianness,
                         &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                         (ima->pxl),
                         name);

  return istat;

}


/**************************************************************************/
/* HM 23/09
   lecture d'image i3d et stockage dans une structure H2Image
   */
int ri3d_2( H2Image *ima ) {
  int istat;
  int endianness;
  istat = read_image_i3d(
                         &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                         &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                         &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                         &(ima->type), &endianness,
                         &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                         &(ima->pxl),
                         ima->name);

  return istat;

}


/**************************************************************************/
/* HM 5 mars 97
   ecriture d'image i3d et stockage dans une structure H2Image
   */
int wi3d_2( H2Image *ima) {
  int istat;
  int endianness;
  endianness=test_endian();
  istat = write_image_i3d(
                         &(ima->n[0]), &(ima->n[1]), &(ima->n[2]),
                         &(ima->s[0]), &(ima->s[1]), &(ima->s[2]),
                         &(ima->p[0][0]), &(ima->p[1][0]), &(ima->p[2][0]),
                         &(ima->type), &endianness,
                         &(ima->nd[0]), &(ima->nd[1]), &(ima->nd[2]),
                         (ima->pxl),
                         ima->name);

  return istat;

}


/**************************************************************************************/
/* lecture d'un plan (en z) d'une image i3d */
/* HM 15 juillet 1998
 */
int ri3dz( int strin, int iz, H2Image *image) {
  int istat ;
  int endianness;

  FILE *file;

  /*--------------------------------------------------------------------------*/
  /*on ouvre le fichier image: */
  file = fdopen(strin,"r");
  if (file==NULL)
    {
      istat = -1;
      fprintf(stderr,"ri3dz: probl�me � l'ouverture du fichier.\n");
      return istat;
    }
  /*--------------------------------------------------------------------------*/

  if (image == (H2Image *)NULL) {
    image = malloc( sizeof(H2Image) );
    image->pxl = NULL;
    image->type = HIMAFLOAT;
  }

  if ( image->type != HIMAFLOAT ) {
    istat = -2;
    fprintf(stderr,"ri3dz: le type de la structure H2Image doit etre HIMAFLOAT\n");
    return istat;
  }

  istat = read_header_i3d(
                         &(image->n[0]), &(image->n[1]), &(image->n[2]),
                         &(image->s[0]), &(image->s[1]), &(image->s[2]),
                         &(image->p[0][0]), &(image->p[1][0]),
                         &(image->p[2][0]),
                         &(image->type), &endianness,
                         file
                         );
  image->nd[0] = image->n[0];
  image->nd[1] = image->n[1];


  if ( ( iz < 0 ) || ( iz >= (image->n)[2] ) ) {
    fprintf(stderr,"ri3dz: le plan indique est hors de l'image.\n");
    istat = -3;
  }

  /* on ne lit qu'une coupe donc: */
  image->nd[2] = image->n[2] = 1;
  image->pxl = realloc( image->pxl, (size_t)image->nd[0]*(size_t)image->nd[1]*sizeof(float) );

  istat = fseek(
               file,
               image->nd[0]*image->nd[1]*sizeof(float)*(iz),
               SEEK_CUR
               );

  istat = read_pixels(
                          image->n[0], image->n[1], 1 ,
                          image->type, endianness,
                          image->nd[0], image->nd[1], 1,
                          image->pxl,
                          file);

  fclose(file);
  return 0;
}

/**************************************************************************************/
