* HM
* LMA 16/11/2001
*
* fonctions fortran 77 d'interfacage avec les fonctions C de manipulation
* des images au format i3d version 2.0.
*
*------------------------------------------------------------------------
* lecture d'une image i3d et stockage ds le tableau ima
* dimensionne en:
* ima(nd1, nd2, nd3)
* le type de ima (double precision, real, integer, character) est determine
* la variable type (voir i3df.inc ou i3d.h):
*     type=1:      character
*     type=3:      integer
*     type=5:      real
*     type=6:      double precision
*
* Si la dimension de l'image est superieure a celle du tableau
* ima, l'en tete de l'image est correctement lu mais pas son
* contenu, un message d'erreur est alors retourne (valeur -1)
* Pour lire simplement l'en tete, il suffit de passer des arguments
* nuls pour nd1, nd2 et nd3 (en fait il suffit que l'un des 3 soit
* nul).
*
      integer function read_image_i3d_f(
     s     n1, n2, n3,
     s     s1, s2, s3,
     s     p1, p2, p3,
     s     type, codage,
     s     nd1, nd2, nd3,
     s     ima,
     s     nom)
      
      implicit none

      integer n1, n2, n3
      double precision s1, s2, s3
      double precision p1, p2, p3
      
      integer type, codage
      integer nd1, nd2, nd3

* en fait le type de ima (ici real) n'a aucune importance:
      real ima(*)

      character*(*) nom

      integer inom(100)

      integer read_image_i3d_2


      integer lg

      lg = index(nom,' ')-1
      if (lg.eq.-1) then
        lg = len(nom)
        if (lg.le.0) then
          write(*,*) 'read_image_i3d_f: pb de nom d''image'
          read_image_i3d_f = -1
          return
        end if
      end if

      call char2s( nom, lg, inom, 4*100)

      read_image_i3d_f = 
     s     read_image_i3d_2(
     s     n1, n2, n3,
     s     s1, s2, s3,
     s     p1, p2, p3,
     s     type, codage,
     s     nd1, nd2, nd3,
     s     ima,
     s     inom)

      end

*------------------------------------------------------------------------
* Ecriture dans une image i3d du contenu du tableau ima
* dimensionne en:
* ima(nd1, nd2, nd3)
* le type de ima (double precision, real, integer, character) est determine
* la variable type (voir i3df.inc ou i3d.h):
*     type=1:      character
*     type=3:      integer
*     type=5:      real
*     type=6:      double precision
*
*
      integer function write_image_i3d_f(
     s     n1, n2, n3,
     s     s1, s2, s3,
     s     p1, p2, p3,
     s     type, codage,
     s     nd1, nd2, nd3,
     s     ima,
     s     nom)
      
      implicit none

      integer n1, n2, n3
      double precision s1, s2, s3
      double precision p1, p2, p3
      
      integer type, codage
      integer nd1, nd2, nd3

* le type de ima (ici real) n'a aucune importance:
      real ima(*)

      character*(*) nom

      integer inom(100)

      integer write_image_i3d



      integer lg

      lg = index(nom,' ')-1
      if (lg.eq.-1) then
        lg = len(nom)
        if (lg.le.0) then
          write(*,*) 'write_image_i3d_f: pb de nom d''image'
          write_image_i3d_f = -1
          return
        end if
      end if



      call char2s( nom, lg, inom, 4*100)

      write_image_i3d_f = 
     s     write_image_i3d(
     s     n1, n2, n3,
     s     s1, s2, s3,
     s     p1, p2, p3,
     s     type, codage,
     s     nd1, nd2, nd3,
     s     ima,
     s     inom)

      end

*------------------------------------------------------------------------
