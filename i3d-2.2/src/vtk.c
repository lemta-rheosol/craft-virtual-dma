#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <i3d.h>
#include <vtk.h>

#ifndef TINY
#define TINY 1.e-20
#endif

/*****************************************************************/
/* write a i3d image into a legacy vtk file in BINARY */
int wvtk( H2Image *ima ) {
  return wvtk0( ima, 1);
}
/*****************************************************************/
/* write a i3d image into a legacy vtk file */
int wvtk0( H2Image *ima , int binary ) {
  FILE *f;
  int endianness;
  void *buf;
  int length;
  int inversion_necessaire;

  int I[3];
  int i;

  /*-----------------------------------------------------------------------------*/
  /* legacy vtk file format does not enable spacings between pixels
     which are not along x1, x2 and x3 respectively.
     So, it has to be verified that:
     p[0] is colinear to x1,
     p[1] is colinear to x2,
     p[2] is colinear to x3,
   */
  if ( (ima->p[0][0]<TINY) || (ima->p[0][1]>TINY) || (ima->p[0][2]>TINY) ||
       (ima->p[1][0]>TINY) || (ima->p[1][1]<TINY) || (ima->p[1][2]>TINY) ||
       (ima->p[2][0]>TINY) || (ima->p[2][1]>TINY) || (ima->p[2][2]<TINY) ){
    fprintf(stderr,"warning in wvtk: \n");
    fprintf(stderr,"invalid spacings for vtk file format \n");
    fprintf(stderr,"legacy vtk file format does not enable spacings between pixels\n");
    fprintf(stderr,"which are not along x1, x2 and x3 respectively.\n");
  }


  /*-----------------------------------------------------------------------------*/
  f = fopen(ima->name, "w");
  if (f==(FILE *)0) {
    fprintf(stderr,"error in wvtk when opening %s\n",ima->name);
    return -1;
  }

  /*-----------------------------------------------------------------------------*/
  /* header */

  fprintf(f,"# vtk DataFile Version 3.0\n");
  fprintf(f,"craft output\n");
  if( binary) {
    fprintf(f,"BINARY\n");
  }
  else{
    fprintf(f,"ASCII\n");
  }
  fprintf(f,"DATASET STRUCTURED_POINTS\n");
  fprintf(f,"DIMENSIONS %d %d %d\n",ima->n[0],ima->n[1],ima->n[2]);
  fprintf(f,"ORIGIN %25.15g %25.15g %25.15g\n",ima->s[0],ima->s[1],ima->s[2]);
  fprintf(f,"SPACING  %25.15g %25.15g %25.15g\n",ima->p[0][0],ima->p[1][1],ima->p[2][2]);

  fprintf(f,"POINT_DATA %d\n",ima->n[0]*ima->n[1]*ima->n[2]);

  switch(ima->type){
  case HIMAFLOAT:
    fprintf(f,"SCALARS scalars float\n");
    break;
  case HIMADOUBLE:
    fprintf(f,"SCALARS scalars double\n");
    break;
  case HIMAINT:
    fprintf(f,"SCALARS scalars int\n");
    break;
  case HIMAUINT:
    fprintf(f,"SCALARS scalars unsigned_int\n");
    break;
  case HIMACHAR:
    fprintf(f,"SCALARS scalars char\n");
    break;
  case HIMAUCHAR:
    fprintf(f,"SCALARS scalars unsigned_char\n");
    break;
  }

  fprintf(f,"LOOKUP_TABLE default\n");

  /*-----------------------------------------------------------------------------*/
  /* in case of binary file */
  if(binary==1){
    /* VTK legacy format requires big endian data encoding, so :*/
    endianness=test_endian();
    if (endianness==LITTLEENDIAN){
      inversion_necessaire=1;
    }
    else if(endianness==BIGENDIAN){
      inversion_necessaire=0;
    }
    else{
      fprintf(stderr,"error in wvtk with endianness\n");
      return -1;
    }
    length=data_length(ima->type);
    if(length<=0) {
      return -1;
    }

    /* but it is not necessary to inverse byte orders if there is just
       one byte (in the case of char or unsigned char data) */
    if (length==1) inversion_necessaire=0;


    buf = malloc(length*ima->n[0]);


    for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
      for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
        memcpy(buf,(char *)(ima->pxl)+(size_t)length*(size_t)ima->nd[0]*(I[1] + (size_t)ima->nd[1]*I[2]), (size_t)ima->n[0]*(size_t)length);
        if (inversion_necessaire){
          invpds2(buf,buf,length,ima->n[0]);
          }
        fwrite(buf, length, ima->n[0], f);
      }
    }
    free(buf);
  }

  /*-----------------------------------------------------------------------------*/
  /* in case of ascii file */
  else{
    switch( ima->type ){
    case(HIMADOUBLE):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%lg ",((double *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    case(HIMAFLOAT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%g ",((float *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    case(HIMAINT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%d ",((int *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    case(HIMAUINT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%ud ",((unsigned int *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    case(HIMACHAR):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%d ",((int *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    case(HIMAUCHAR):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fprintf(f,"%ud ",((unsigned int *)(ima->pxl))[i]);
            i++;
          }
          fprintf(f,"\n");
        }
      }
      break;

    }

  }
  /*-----------------------------------------------------------------------------*/
  fclose(f);
  /*-----------------------------------------------------------------------------*/

  return 0;
}
/*****************************************************************/
/* read a i3d image from a legacy vtk file */
int rvtk( H2Image *ima ) {

  /*--------------------------------------------------------------------*/
  FILE *f;
  int endianness;
  char line[256];

  int i,j;

  int status;
  /* tells if data are ascii (0) binary (1) or none of them (-1) */
  int binary;

  int setn, setp, sets;

  int n;

  char dataname[200];
  char datatype[200];

  int filedatatype;
  int I[3];
  /*--------------------------------------------------------------------*/
  f = fopen(ima->name, "r");
  if (f==(FILE *)0) {
    fprintf(stderr,"error in rvtk when opening %s\n",ima->name);
    return -1;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* first line indicates if it is a VTK file */
  fgets(line, 256, f);
  status=strncmp(line, "# vtk DataFile Version ", strlen("# vtk DataFile Version "));
  if(status!=0) {
    fprintf(stderr,"error in rvtk:\n");
    fprintf(stderr,"%s does not seem to be a VTK legacy file\n",ima->name);
    return -1;
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* 2d line contains just a small description header */
  fgets(line, 256, f);

  /* tells wheter data are in ASCII or BINARY */
  fgets(line, 256, f);

  binary=-1;
  if( strncmp(line,"ASCII",strlen("ASCII"))==0 ) {
    binary=0;
  }
  else if( strncmp(line,"BINARY",strlen("BINARY"))==0 ) {
    binary=1;
  }

  if(binary==-1) {
    fprintf(stderr,"error in rvtk\n");
    return -1;
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* data type */
  fgets(line, 256, f);
  if ( strncmp(line,"DATASET STRUCTURED_POINTS",strlen("DATASET STRUCTURED_POINTS")) !=0){
    fprintf(stderr,"error in rvtk:\n");
    fprintf(stderr,"rvtk just accepts DATASET STRUCTURED_POINTS\n");
    fprintf(stderr,"(and not %s)\n",line);
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* dataset description */
  setn=0;
  sets=0;
  setp=0;
  do{
    fgets(line, 256, f);

    if (strncmp(line,"DIMENSIONS",strlen("DIMENSIONS"))==0) {
      status=sscanf(line+strlen("DIMENSIONS"),"%d %d %d",
                    &(ima->n[0]), &(ima->n[1]), &(ima->n[2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk when reading line:\n %s\n",line);
        return -1;
      }
      /* for(i=0;i<3;i++) ima->nd[i]=ima->n[i]; */
      setn=1;
    }

    else if(strncmp(line,"ORIGIN",strlen("ORIGIN"))==0) {
      status=sscanf(line+strlen("ORIGIN"),"%lf %lf %lf",
                    &(ima->s[0]), &(ima->s[1]), &(ima->s[2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk when reading line:\n %s\n",line);
        return -1;
      }
      sets=1;
    }

    else if(strncmp(line,"SPACING",strlen("SPACING"))==0) {
      for(i=0;i<3;i++){
        for(j=0;j<3;j++){
          ima->p[i][j]=0.;
        }
      }
      status=sscanf(line+strlen("SPACING"),"%lf %lf %lf",
                    &(ima->p[0][0]), &(ima->p[1][1]), &(ima->p[2][2]) );
      if(status!=3) {
        fprintf(stderr,"error in rvtk when reading line:\n %s\n",line);
        return -1;
      }
      setp=1;
    }

  } while ( ( strncmp(line,"POINT_DATA", strlen("POINT_DATA"))!=0 )
         && ( strncmp(line,"CELL_DATA",  strlen("CELL_DATA"))!=0  ) );

  /* origin has not been specified */
  if(setn==0) {
    fprintf(stderr,"error in rvtk: no dimensions specified in %s\n",ima->name);
    return -1;
  }

  /* origin has not been specified */
  if(sets==0) {
    fprintf(stderr,"warning in rvtk: \n  no origin specified in %s \n",ima->name);
    fprintf(stderr,"set to 0. 0. 0.\n");

    for(i=0;i<3;i++) ima->s[i]=0.;
  }

  /* spacing has not been specified */
  if(setp==0) {
    fprintf(stderr,"warning in rvtk: \n  no spacing specified in %s \n",ima->name);
    fprintf(stderr,"set to: 1. 0. 0.   0. 1. 0.   0. 0. 1. \n");

      for(i=0;i<3;i++){
        for(j=0;j<3;j++){
          ima->p[i][j]=0.;
        }
        ima->p[i][i]=1.;
      }
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* point data */
  if ( strncmp(line,"CELL_DATA", strlen("CELL_DATA"))==0 ){
    fprintf(stderr,"error in rvtk: rvtk can not read CELL_DATA\n");
    return -1;
  }

  status=sscanf( line+strlen("POINTS_DATA"),"%d",&n);
  if (status!=1){
    fprintf(stderr,"error in rvtk: invalid line %s in %s",line,ima->name);
    return -1;
  }

  if (n != ima->n[0]*ima->n[1]*ima->n[2] ){
    fprintf(stderr,"warning in rvtk: \n  non consistent number of points\n");
    fprintf(stderr,"%d vs %d\n",n,ima->n[0]*ima->n[1]*ima->n[2]);
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* type of data (float, int, ...) */
  fgets(line, 256, f);
  if( strncmp(line,"SCALARS",strlen("SCALARS"))!=0) {
#ifdef DEBUG
    fprintf(stderr,"warning in rvtk: awaiting for SCALARS keyword. Try rvtk_vectorial...\n");
#endif
    return -2;
  }

  status=sscanf(line+strlen("SCALARS"),"%s %s %d", dataname, datatype, &n);
  if (status<2){
    fprintf(stderr,"error in rvtk: invalid specification in %s\n",line);
    return -1;
  }
  if(status==2) n =1;

  if(n>1){
    fprintf(stderr,"error in rvtk: invalid specification in %s\n",line);
    fprintf(stderr,"rvtk does not accept numcomp > 1\n");
    return -1;
  }


  if (strncmp(datatype,"float",strlen("float"))==0)                        filedatatype=HIMAFLOAT;
  else if (strncmp(datatype,"double",strlen("double"))==0)                 filedatatype=HIMADOUBLE;
  else if (strncmp(datatype,"int",strlen("int"))==0)                       filedatatype=HIMAINT;
  else if (strncmp(datatype,"unsigned_int",strlen("unsigned_int"))==0)     filedatatype=HIMAUINT;
  else if (strncmp(datatype,"char",strlen("char"))==0)                     filedatatype=HIMACHAR;
  else if (strncmp(datatype,"unsigned_char",strlen("unsigned_char"))==0)   filedatatype=HIMAUCHAR;
  else{
    fprintf(stderr,"error in rvtk: rvtk does not accept  %s datatype\n",datatype);
    return -1;
  }

  /* lookup table */
  fgets(line, 256, f);
  if( strncmp(line,"LOOKUP_TABLE",strlen("LOOKUP_TABLE"))!=0) {
    fprintf(stderr,"error in rvtk: rvtk can not read line %s\n",line);
  }

  if (ima->type == HIMAUNKNOWN ) ima->type=filedatatype;

  /* memory allocation if necessary */
  if(ima->pxl==(void *)0) {
    for(i=0;i<3;i++) ima->nd[i]=ima->n[i];
    
  }
  ima->pxl = realloc(ima->pxl, data_length(ima->type)*ima->nd[0]*ima->nd[1]*ima->nd[2]);
    if(ima->pxl==(void *)0) {
      fprintf(stderr,"error in rvtk: impossible to allocate memory for pixels data\n");
      return -1;
    }
  /*-----------------------------------------------------------------------*/
  /* VTK legacy format requires big endian data encoding, so :*/
  switch (binary){
  case 1:

#ifdef OLD
    endianness=test_endian();
    if (endianness==LITTLEENDIAN){
      inversion_necessaire=1;
    }
    else if(endianness==BIGENDIAN){
      inversion_necessaire=0;
    }
    else{
      fprintf(stderr,"error in wvtk with endianness\n");
      return -1;
    }

    length=data_length(ima->type);
    if(length<=0) return -1;
    buf = malloc(length*ima->nd[0]);

    for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
      for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
        fread(buf, length, ima->n[0], f);
        if (inversion_necessaire){
          invpds2(buf,buf,length,ima->n[0]);
        }
        memcpy((char *)(ima->pxl)+length*ima->nd[0]*(I[1] + ima->nd[1]*I[2]),
               buf, ima->n[0]*length);
      }
    }
#endif

    endianness=BIGENDIAN;
    read_pixels_t( ima->n[0], ima->n[1], ima->n[2],
             filedatatype, endianness,
             ima->nd[0], ima->nd[1], ima->nd[2],
             ima->type,&(ima->pxl), f);
    break;

  case 0:
    switch( ima->type ){
    case(HIMADOUBLE):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%lf",&((double *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    case(HIMAFLOAT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%f",&((float *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    case(HIMAINT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%d",&((int *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    case(HIMAUINT):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%ud",&((unsigned int *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    case(HIMACHAR):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%d",&((int *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    case(HIMAUCHAR):
      i=0;
      for(I[2]=0; I[2]<ima->n[2]; I[2]++) {
        for(I[1]=0; I[1]<ima->n[1]; I[1]++) {
          for( I[0]=0; I[0]<ima->n[0]; I[0]++ ) {
            fscanf(f,"%ud",&((unsigned int *)(ima->pxl))[i]);
            i++;
          }
          i += (ima->nd[0]-ima->n[0]);
        }
      }
      break;

    }
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    break;
  }

  /*----------------------------------------------------------------------------*/
  fclose(f);
  return 0;
}
/*********************************************************************************/
/* this function tests wether the file in input is in i3d format, or legacy VTK format
   (or something else)


   return value:
     -1 : problem encountered while opening or reading the file
     O  : unknown file format
     1  : i3d 1.x file format
     2  : i3d 2.x file format
     3  : legacy VTK format

*/
int i3d_or_vtk_file_format( const char *filename){
  FILE *f;
  int status=0;
  char mot[23];

  //  printf("<%s>\n",filename);

  f = fopen( filename, "r");
  if (f == (FILE *)0){
    printf("fopen a plante!\n");
    status=-1;
    return status;
  }


  status=fread(mot, 1, 23 , f);

  if(status!=23) {
    status=-1;
    fclose(f);
    return status;
  }

  if ( strncmp(mot,"# vtk DataFile Version ",23)==0 ){
    status=3;
  }
  else if ( strncmp(mot,"HMRS",4)==0 ){
    status=1;
  }
  else if (
           ( strncmp(mot,"HM2RS",5)==0 ) ||
           ( strncmp(mot,"HM2RD",5)==0 ) ||
           ( strncmp(mot,"HM2I", 4)==0 ) ||
           ( strncmp(mot,"HM2UI",5)==0 ) ||
           ( strncmp(mot,"HM2C", 4)==0 ) ||
           ( strncmp(mot,"HM2UC",5)==0 )
           ){
    status=2;
  }
  else{
    status=0;
  }
  fclose(f);

  //  printf("%s status=%d\n",mot,status);
  return status;
}


