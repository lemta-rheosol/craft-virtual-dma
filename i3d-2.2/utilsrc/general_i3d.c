#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <i3d.h>
#include <math.h>

#include <unistd.h>
#include <getopt.h>
/****************************************************************************/
void usage(){
  fprintf(stderr,"general_i3d creates a header file readable by OpenDX from a file in i3d format.\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"Usage:\n");
  fprintf(stderr,"general_i3d src [dest]\n");
  fprintf(stderr,"(when file dest is omitted, it takes the name of src with \".general\" extension)\n");
}
/******************************************************************************************************/
int main(int argc, char **argv){

  H2Image image;
  int status;

  char *src, *dest;

  char *p;

  int c;
  /*-------------------------------------------------------------------------------------------------*/
  src = (char *)0;
  dest = (char *)0;
  status = construct_h2image(&image, HIMAUNKNOWN);

  while( (c=getopt(argc,argv, "h")) != -1 ){
    switch(c){
    case 'h':
      usage();
      exit(0);
    }
  }

  if( argc <=1 ) {
    usage();
    exit(1);
  }

  if(argc>=2) {
    src = (char *)malloc(strlen(argv[1])+1);
    sscanf( argv[1],"%s", src);
  }

  if(argc>=3) {
    dest = (char *)malloc(strlen(argv[2])+1);
    sscanf( argv[2],"%s", dest);
  }

  if (dest == (char *)0) {

    p= strstr(src,".i3d") ;

    if ( (p != (char *)0) && ( (int)(p-src+strlen(".i3d")) == strlen(src) ) ){
      dest = (char *)malloc(strlen(src)-strlen(".vtk")+strlen(".general")+1);
      strncpy( dest, src, strlen(src)-strlen(".i3d") );
      strcat( dest, ".general");
    }
    else{
      dest = (char *)malloc(strlen(src)+strlen(".general")+1);
      strncpy( dest, src, strlen(src) );
      strcat( dest, ".general");
    }
    

  }

  /*-------------------------------------------------------------------------------------------------*/
  image.name = src;
  status=ri3d(src,&image);
  if(status!=0) {
    fprintf(stderr,"error in general_3d when reading %s\n",src);
    exit(1);
  }

  /*-------------------------------------------------------------------------------------------------*/
  FILE *file;  
  int type_fichier;

  file = fopen(src,"r");
  type_fichier = read_file_version_i3d(file); 
  fclose(file);
  if(type_fichier==-1) {
    fprintf(stderr,"error in general_3d:\n");
    fprintf(stderr,"file %s is not in i3d format\n",src);

    exit(1);
  }

  file = fopen(dest,"w");
  fprintf(file,"file = %s\n",image.name);
  fprintf(file,"grid = %d x %d x %d\n",image.n[0],image.n[1],image.n[2]);
  fprintf(file,"format = lsb ieee\n");
  fprintf(file,"interleaving = record\n");
  fprintf(file,"majority = column\n");
  if (type_fichier == I3D1) {
    fprintf(file,"header = bytes 512\n");
  }
  else {
    fprintf(file,"header = bytes 138\n");
  }
  fprintf(file,"field = field0\n");
  fprintf(file,"structure = scalar\n");
  fprintf(file,"type = float\n");
  fprintf(file,"dependency = positions\n");
  fprintf(file,"positions = regular, regular, regular, %f, %f, %f, %f, %f, %f\n",
	  image.s[0],image.p[0][0],
	  image.s[1],image.p[1][1],
	  image.s[2],image.p[2][2]
	  );
  fprintf(file,"\n");
  fprintf(file,"end\n");
  fclose(file);
  /*-------------------------------------------------------------------------------------------------*/


}
