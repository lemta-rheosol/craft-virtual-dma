/* readima2 nouvelle saucee
 * HM 26/11/01
 *
 * modifie le 29 septembre 2010
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <i3d.h>
#include <math.h>

#include <unistd.h>
#include <getopt.h>

/****************************************************************************/
void usage(){
  fprintf(stderr,"i3dstat displays some information about an image in i3d or vtk format\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"Usage:\n");
  fprintf(stderr,"i3dstat [-b] [-f] file\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"option -b: \"brief\" i.e.: displays no statistics on file, just prints header infos\n");
}
/****************************************************************************/

int main(int argc, char **argv){
  /*
  int n1, n2, n3;
  int nd1, nd2, nd3;
  double p1[3], p2[3], p3[3];
  double s1, s2, s3;
  
  void *pxl;
  int type;
  int endianness;
  */

  double s;
  double x2;
  float xmin, xmax;
  char *nom=(char *)0;

  H2Image image;

  int i;

  int opt;
  int bref;
  /*------------------------------------------------------------------------*/
  bref=0;
  while ( (opt=getopt(argc,argv,"hbf:")) != -1 ) {
    
    switch(opt){
    case 'h':
      usage();
      exit(0);
      break;
    case 'b':
      bref=1;
      break;
    case 'f':
      nom = (char *)malloc(strlen(optarg)+1);
      if(nom==(char *)0) {
	fprintf(stderr,"error!\n");
	usage();
	exit(1);
      }
      strcpy(nom,optarg);
      break;
    }
  }
    
  if ( (nom==(char *)0) && (optind<argc) ) {
    nom = (char *)malloc(strlen(argv[optind])+1);
    sscanf( argv[optind],"%s",nom);
  }
  
  if (nom==(char *)0){
    fprintf(stderr,"name of file required!\n");
    usage();
    exit(1);
  }
			   
  /*------------------------------------------------------------------------*/
  construct_h2image( &image, HIMAUNKNOWN);
  i=ri3d(nom,&image);

  if(i!=0) {
    fprintf(stderr,"i3dstat: an error occured while reading image %s .\n",nom);
    exit(1);
  }

  printf("Data type:  ");
  switch(image.type){
  case HIMAFLOAT:
    printf("float.\n");
    break;
  case HIMADOUBLE:
    printf("double.\n");
    break;
  case HIMAINT:
    printf("integer.\n");
    break;
  case HIMAUINT:
    printf("unsigned integer.\n");
    break;
  case HIMACHAR:
    printf("signed character.\n");
    break;
  case HIMAUCHAR:
    printf("unsigned char.\n");
    break;
  case HIMAUNKNOWN:
    printf("unknown. \n");
    break;
  default:
    printf("? error in data type.\n");
    break;
  }

  printf("size of image:  ");
  printf("n1=%d n2=%d n3=%d\n",image.n[0], image.n[1], image.n[2]);
  printf("coordinates of 1st pixel: (%f , %f , %f)\n", image.s[0], image.s[1], image.s[2]);
  printf(
	 "sample step in direction 1: (%16.8g , %16.8g , %16.8g)\n", 
	 image.p[0][0],image.p[0][1],image.p[0][2]);
  printf(
	 "sample step in direction 2: (%16.8g , %16.8g , %16.8g)\n", 
	 image.p[1][0],image.p[1][1],image.p[1][2]);
  printf(
	 "sample step in direction 3: (%16.8g , %16.8g , %16.8g)\n", 
	 image.p[2][0],image.p[2][1],image.p[2][2]);

  if(bref==0){
    printf("\n");

  /*------------------------------------------------------------------------*/
    printf("Statistics on image:\n");
    switch(image.type){
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMAFLOAT:
      s=0.;
      x2 = 0.;
      xmin=(double)((float *)image.pxl)[0];
      xmax=(double)((float *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((float *)image.pxl)[i];
	x2 += (double)((float *)image.pxl)[i] * (double)((float *)image.pxl)[i] ;
	xmax = ( (double)((float *)image.pxl)[i]>xmax ? (double)((float *)image.pxl)[i] : xmax);
	xmin = ( (double)((float *)image.pxl)[i]<xmin ? (double)((float *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("  min=%16.8g\n",xmin);
      printf("  max=%16.8g\n",xmax);
      printf("  mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      //      printf("mean = %16.8g\n",s);
      printf("  variance = %16.8g\n",x2);
      printf("  root mean square = %16.8g\n",sqrt(x2));
      break;
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMADOUBLE:
      s=0.;
      x2 = 0.;
      xmin=(double)((double *)image.pxl)[0];
      xmax=(double)((double *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((double *)image.pxl)[i];
	x2 += (double)((double *)image.pxl)[i] * (double)((double *)image.pxl)[i] ;
	xmax = ( (double)((double *)image.pxl)[i]>xmax ? (double)((double *)image.pxl)[i] : xmax);
	xmin = ( (double)((double *)image.pxl)[i]<xmin ? (double)((double *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("min=%16.8g\n",xmin);
      printf("max=%16.8g\n",xmax);
      printf("mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      printf("variance = %16.8g\n",x2);
      printf("root mean square = %16.8g\n",sqrt(x2));
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMAINT:
      s=0.;
      x2 = 0.;
      xmin=(double)((int *)image.pxl)[0];
      xmax=(double)((int *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((int *)image.pxl)[i];
	x2 += (double)((int *)image.pxl)[i] * (double)((int *)image.pxl)[i] ;
	xmax = ( (double)((int *)image.pxl)[i]>xmax ? (double)((int *)image.pxl)[i] : xmax);
	xmin = ( (double)((int *)image.pxl)[i]<xmin ? (double)((int *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("min=%16.8g\n",xmin);
      printf("max=%16.8g\n",xmax);
      printf("mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      printf("variance = %16.8g\n",x2);
      printf("root mean square = %16.8g\n",sqrt(x2));
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMAUINT:
      s=0.;
      x2 = 0.;
      xmin=(double)((unsigned int *)image.pxl)[0];
      xmax=(double)((unsigned int *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((unsigned int *)image.pxl)[i];
	x2 += (double)((unsigned int *)image.pxl)[i] * (double)((unsigned int *)image.pxl)[i] ;
	xmax = ( (double)((unsigned int *)image.pxl)[i]>xmax ? (double)((unsigned int *)image.pxl)[i] : xmax);
	xmin = ( (double)((unsigned int *)image.pxl)[i]<xmin ? (double)((unsigned int *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("min=%16.8g\n",xmin);
      printf("max=%16.8g\n",xmax);
      printf("mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      printf("variance = %16.8g\n",x2);
      printf("root mean square = %16.8g\n",sqrt(x2));
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMACHAR:
      s=0.;
      x2 = 0.;
      xmin=(double)((char *)image.pxl)[0];
      xmax=(double)((char *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((char *)image.pxl)[i];
	x2 += (double)((char *)image.pxl)[i] * (double)((char *)image.pxl)[i] ;
	xmax = ( (double)((char *)image.pxl)[i]>xmax ? (double)((char *)image.pxl)[i] : xmax);
	xmin = ( (double)((char *)image.pxl)[i]<xmin ? (double)((char *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("min=%16.8g\n",xmin);
      printf("max=%16.8g\n",xmax);
      printf("mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      printf("variance = %16.8g\n",x2);
      printf("root mean square = %16.8g\n",sqrt(x2));
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMAUCHAR:
      s=0.;
      x2 = 0.;
      xmin=(double)((unsigned char *)image.pxl)[0];
      xmax=(double)((unsigned char *)image.pxl)[0];
      
      for(i=0; i<image.n[0]*image.n[1]*image.n[2]; i++) {
	s+=(double)((unsigned char *)image.pxl)[i];
	x2 += (double)((unsigned char *)image.pxl)[i] * (double)((unsigned char *)image.pxl)[i] ;
	xmax = ( (double)((unsigned char *)image.pxl)[i]>xmax ? (double)((unsigned char *)image.pxl)[i] : xmax);
	xmin = ( (double)((unsigned char *)image.pxl)[i]<xmin ? (double)((unsigned char *)image.pxl)[i] : xmin);
      }
      s /= (double)image.n[0]*(double)image.n[1]*(double)image.n[2];
      printf("min=%16.8g\n",xmin);
      printf("max=%16.8g\n",xmax);
      printf("mean = %16.8g\n",s);
      x2 = x2  / ( (double)image.n[0]*(double)image.n[1]*(double)image.n[2] - 1.) - s*s ;
      printf("variance = %16.8g\n",x2);
      printf("root mean square = %16.8g\n",sqrt(x2));
      break;

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    case HIMAUNKNOWN:
      fprintf(stderr,"unknown data type: statistics unable to be computed\n");
      break;
    default:
      fprintf(stderr,"error in file %s: impossible data type\n",nom);
    }

  }
}
