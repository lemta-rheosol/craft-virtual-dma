/*
  HM 04/10/2010

  convert an i3d file into a legacy VTK file

 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <i3d.h>
#include <h2image.h>
#include <vtk.h>

#include <unistd.h>
#include <getopt.h>

/******************************************************************************************************/
void usage(){
  fprintf(stderr,"Usage of i3dtovtk :\n");
  fprintf(stderr,"i3dtovtk -h\n");
  fprintf(stderr,"    print this help message and exit\n");
  fprintf(stderr," \n");
  fprintf(stderr,"i3dtovtk [-a] [-b] file_src [file_dest]\n");
  fprintf(stderr,"(when file_dest is omitted, it takes the name of file_src with vtk extension)\n");
  fprintf(stderr,"option -a : ASCII file format used\n");
  fprintf(stderr,"option -b : BINARY file format used\n");
}
/******************************************************************************************************/
int main(int argc, char **argv){

  H2Image image;
  int status;

  char *src, *dest;

  char *p;


  int c;
  int binary;
  /*-------------------------------------------------------------------------------------------------*/
  binary=-1;
  while( (c=getopt(argc,argv, "hab")) != -1 ){
    switch(c){
    case 'h':
      usage();
      exit(0);
    case 'a':
      if(binary!=-1){
	fprintf(stderr,"error i3dtovtk: options a and b are not compatible.\n");
	exit(1);
      }
      binary=0;
      break;
    case 'b':
      if(binary!=-1){
	fprintf(stderr,"error i3dtovtk: options a and b are not compatible.\n");
	exit(1);
      }
      binary=1;
      break;
    }
  }

  if(binary==-1){
    binary=1;
  }

  src = (char *)0;
  dest = (char *)0;
  status = construct_h2image(&image, HIMAUNKNOWN);

  //  printf("optind=%d argc=%d\n",optind,argc);
  //  for(i=0;i<argc;i++){
  //  printf("argv[%d]=%s ",i,argv[i]);
  // }
  // printf("\n");

  if( optind >=  argc ) {
    usage();
    exit(1);
  }

  //  printf("ici1\n");
  src = (char *)malloc(strlen(argv[optind])+1);
  sscanf( argv[optind],"%s", src);
  optind++;

  if( optind <  argc ) {
    dest = (char *)malloc(strlen(argv[2])+1);
    sscanf( argv[optind],"%s", dest);
  }

  if (dest == (char *)0) {
    
    p= strstr(src,".i3d") ;
    if (p==(char *)0) p=strstr(src,".ima");
    
    if ( (p != (char *)0) && ( (int)(p-src+strlen(".ima")) == strlen(src) ) ){
      dest = (char *)malloc(strlen(src)+1);
      strncpy( dest, src, strlen(src)-strlen(".ima") );
      strcat( dest, ".vtk");
    }
    else{
      dest = (char *)malloc(strlen(src)+strlen(".vtk")+1);
      strncpy( dest, src, strlen(src) );
      strcat( dest, ".vtk");
    }
    
    
  }

  /*-------------------------------------------------------------------------------------------------*/
  status=ri3d(src,&image);
  if(status!=0) {
    fprintf(stderr,"error in i3dtovtk when reading %s\n",src);
    exit(1);
  }

  image.name=dest;
  status=wvtk0(&image, binary);
  if(status!=0) {
    fprintf(stderr,"error in i3dtovtk when writing %s\n",dest);
    exit(1);
  }
  

  /*-------------------------------------------------------------------------------------------------*/


}
