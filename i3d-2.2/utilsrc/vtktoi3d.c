/*
  HM 04/10/2010

  convert a legacy VTK file into an i3d file

 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <i3d.h>
#include <h2image.h>
#include <vtk.h>


/******************************************************************************************************/
void usage(){
  fprintf(stderr,"Usage of vtktoi3d :\n");
  fprintf(stderr,"vtktoi3d -h\n");
  fprintf(stderr,"    print this help message and exit\n");
  fprintf(stderr," \n");
  fprintf(stderr,"vtktoi3d file_src [file_dest]\n");
  fprintf(stderr,"(when file_dest is omitted, it takes the name of file_src with i3d extension)\n");
}
/******************************************************************************************************/
int main(int argc, char **argv){

  H2Image image;
  int status;

  char *src, *dest;

  char *p;

  int c;
  /*-------------------------------------------------------------------------------------------------*/
  src = (char *)0;
  dest = (char *)0;
  status = construct_h2image(&image, HIMAUNKNOWN);

  while( (c=getopt(argc,argv, "h")) != -1 ){
    switch(c){
    case 'h':
      usage();
      exit(0);
    }
  }

  if( argc <=1 ) {
    usage();
    exit(1);
  }

  if(argc>=2) {
    src = (char *)malloc(strlen(argv[1])+1);
    sscanf( argv[1],"%s", src);
  }

  if(argc>=3) {
    dest = (char *)malloc(strlen(argv[2])+1);
    sscanf( argv[2],"%s", dest);
  }

  if (dest == (char *)0) {

    p= strstr(src,".vtk") ;
    if (p==(char *)0) p=strstr(src,".vtk");

    if ( (p != (char *)0) && ( (int)(p-src+strlen(".vtk")) == strlen(src) ) ){
      dest = (char *)malloc(strlen(src)+1);
      strncpy( dest, src, strlen(src)-strlen(".vtk") );
      strcat( dest, ".i3d");
    }
    else{
      dest = (char *)malloc(strlen(src)+strlen(".i3d")+1);
      strncpy( dest, src, strlen(src) );
      strcat( dest, ".i3d");
    }
    

  }

  /*-------------------------------------------------------------------------------------------------*/
  image.name = src;
  status=rvtk(&image);
  if(status!=0) {
    fprintf(stderr,"error in vtktoi3d when reading %s\n",src);
    exit(1);
  }

  image.name=dest;
  status=wi3d(dest,&image);
  if(status!=0) {
    fprintf(stderr,"error in vtktoi3d when writing %s\n",dest);
    exit(1);
  }
  

  /*-------------------------------------------------------------------------------------------------*/


}
