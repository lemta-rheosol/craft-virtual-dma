#ifndef CRAFT_ALEA
#define CRAFT_ALEA
/* en tete pour l'utilisation de alea qui genere un nombre
   double precision aleatoire dans [0.,1.[

   salea permet l'initialisation du processus aleatoire
*/
double alea();
int salea(unsigned seed);
int random_poisson(double lambda);
#endif
