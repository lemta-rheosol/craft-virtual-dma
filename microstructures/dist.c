#include <math.h>
#ifndef CRAFT_DIST
#define CRAFT_DIST
/***************************************************************************/
/*
   Calcul de la distance entre 2 points a et b ds un "parallelepipede
   periodique":
   [0,T1[ x [0,T2[ x [0,T3[
   La periode est donnee par t.
   Les points donnes en entree peuvent etre en dehors de [0,t1[ x [0,t2[ x [0,t3[
   on calcule leur position "periodisee" (c'est le sens du calcul avec le floor)
   Inconvenient: c'est lent.
   Voir dist_p0 pour aller plus vite.

   Principe de l'algorithme:
   
   la distance entre 2 points a et b dans un espace periodique de periode t
   (i.e. on a la meme chose en (x1,x2,x3) et en (x1+k1*t1, x2+k2*t2, x3+k3*t3)
   pour k1, k2, k3 entiers) consiste a placer les points a et b dans 
   toutes les periodes et a chercher la distance euclidienne la plus
   petite entre tous ces points.
   Un algorithme simple (mais lent) consiste a ramener a et b dans la periode
   [0,t1[*[0,t2[*[0,t3[ et a chercher la plus petite distance euclidienne
   entre a et les points (b1+k1*t1, b2+k2*t2, b3+k3*t3) pour k1, k2, k3 = -1, 0, 1.
   Mais cela revient a calculer la plus petite distance euclidienne au point (0,0,0)
   des vecteurs v=(c1+k1*t1, c2+k2*t2, c3+k3*t3) pour k1, k2, k3 = -1, 0, 1,
   pour c=a-b. Ou, autrement dit, de calculer la distance minimale de c aux 27 points
   de coordonnees (k1*t1, k2*t2, k3*t3) avec k1, k2, k3 = -1,0,1. On peut
   voir simplement que cela revient a calculer la distance de v'=(|v1|,|v2|,|v3|)
   aux 8 points (k1*t1,k2*t2,k3*t3) avec k1,k2,k3 = 0 ou 1.
   Chercher dans le parallelepipede [0,t1[*[0,t2[*[0,t3[ lequels de ces 8 points
   est le plus proche de v', consiste a regarder ou se trouve v' parmi les 8 
   sous-parallelepipedes obtenus en coupant le volume en t1/2, t2/2 et t3/2
   (se sont les 8 cellules de voronoi des 8 points en question).
   On peut meme simplement se ramener au seul calcul de la distance euclidienne
   au point 0 en se ramenant au parallelepipede [-t1/2,t1/2[*[-t2/2,t2/2[*[-t3/2,t3/2[
   par une translation idoine de (k1*t1,k2*t2,k3*t3) k1,k2,k3 = 0 ou -1 selon
   le sous-parallelepipede ou se trouve v'.

*/
/***************************************************************************/
double dist_p( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  /* on calcule le vecteur difference des positions de a et b ramenees
     ds le volume t */
  for(i=0; i<3; i++) {
    if ( fabs(t[i])<1.e-10) {
      tmp[i]=0.;
    }
    else {
      /* 
      tmp[i] = 
	( a[i] - t[i]*floor(a[i]/t[i]) ) -
	( b[i] - t[i]*floor(b[i]/t[i]) );
      */
      /* plus rapide : */
      tmp[i] = a[i]-b[i];
      tmp[i] -= t[i]*floor(tmp[i]/t[i]);
    }
  }

  for(i=0; i<3; i++) {
    /* on se ramene au calcul de la distance aux 8 sommets du volume 
       [0,t1[*[0,t2[*[0,t3[  :
    */
    tmp[i] = fabs(tmp[i]);
    /* on se ramene au calcul de la distance a 0, en se ramenant dans le
       volume [-t1/2,t1/2[*[-t2/2,t2/2[*[-t3/2,t3/2[  :
    */
    if ( tmp[i]>t[i]/2. ) tmp[i] -= t[i];
  }
  /* calcul de la distance a 0, et retour */
  return sqrt( 
	   tmp[0]*tmp[0] +
	   tmp[1]*tmp[1] +
	   tmp[2]*tmp[2] 
	   );

}
/***************************************************************************/
double dist2_p( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  /* on calcule le vecteur difference des positions de a et b ramenees
     ds le volume t */
  for(i=0; i<3; i++) {
    if ( fabs(t[i])<1.e-10) {
      tmp[i]=0.;
    }
    else {
      /*
	tmp[i] = 
	( a[i] - t[i]*floor(a[i]/t[i]) ) -
	( b[i] - t[i]*floor(b[i]/t[i]) );
      */
      /* plus rapide que la ligne ci dessus : */
      tmp[i] = a[i]-b[i];
      tmp[i] -= t[i]*floor(tmp[i]/t[i]);

    }
#ifdef VARIANTES
    /* on economise du temps en ne testant pas si t est valide */
    /*
    tmp[i] = 
      ( a[i] - t[i]*floor(a[i]/t[i]) ) -
      ( b[i] - t[i]*floor(b[i]/t[i]) );
    */
    /* plus rapide que la ligne ci dessus : */
    tmp[i] = a[i]-b[i];
    tmp[i] -= t[i]*floor(tmp[i]/t[i]);
#endif
  }

  for(i=0; i<3; i++) {
    /* on se ramene au calcul de la distance aux 8 sommets du volume 
       [0,t1[*[0,t2[*[0,t3[  :
    */
    tmp[i] = fabs(tmp[i]);
    /* on se ramene au calcul de la distance a 0, en se ramenant dans le
       volume [-t1/2,t1/2[*[-t2/2,t2/2[*[-t3/2,t3/2[  :
    */
    if ( tmp[i]>t[i]/2. ) tmp[i] -= t[i];
  }
  /* calcul de la distance a 0 AU CARRE , et retour */
  return ( 
	   tmp[0]*tmp[0] +
	   tmp[1]*tmp[1] +
	   tmp[2]*tmp[2] 
	   );

}
/***************************************************************************/
/* same as dist2_p except that it is optimized and that 
   a and b MUST be insid [0,T1[ x [0,T2[ x [0,T3[
   (more precisely: the difference a-b must be inside [0,T1[ x [0,T2[ x [0,T3[ )
*/
double dist2_p3( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  /* on calcule le vecteur difference des positions de a et b ramenees
     ds le volume t */
  for(i=0;i<3;i++) {
    //    tmp[i] = a[i]-b[i];
    //    tmp[i] -= t[i]*floor(tmp[i]/t[i]);
    //    tmp[i] = fabs(tmp[i]);
    tmp[i] = fabs(a[i]-b[i]);
    if ( tmp[i]>t[i]*0.5 ) tmp[i] -= t[i];
  }

  return ( 
	   tmp[0]*tmp[0] +
	   tmp[1]*tmp[1] +
	   tmp[2]*tmp[2] 
	   );

}

double dist_p3( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  /* on calcule le vecteur difference des positions de a et b ramenees
     ds le volume t */
  for(i=0;i<3;i++) {
    //    tmp[i] = a[i]-b[i];
    //    tmp[i] -= t[i]*floor(tmp[i]/t[i]);
    //    tmp[i] = fabs(tmp[i]);
    tmp[i] = fabs(a[i]-b[i]);

    if ( 2.*tmp[i]>t[i] ) tmp[i] -= t[i];
  }

  return ( sqrt(tmp[0]*tmp[0] + tmp[1]*tmp[1] + tmp[2]*tmp[2] )   );

}
/***************************************************************************/
/*
   Calcul de la distance entre 2 points a et b ds un "parallelepipede
   periodique":
   [0,T1[ x [0,T2[ x [0,T3[
   La periode est donnee par t.
   Les points donnes en entree NE DOIVENT PAS etre en dehors de [0,T1[ x [0,T2[ x [0,T3[
   Avantage: c'est beaucoup plus rapide que dist_p
*/
/***************************************************************************/
double dist_p0( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  for(i=0; i<3; i++) {
    //------------------------------------------------------
    // si on voulait rester ds le cas g�n�ral o� les points a et b 
    // peuvent �tre en dehors de [0,T1[ x [0,T2[ x [0,T3[
    // il fadrait mettre les 3 lignes suivantes:
    //    tmp[i] = a[i]-b[i];
    //    tmp[i] -= t[i]*floor(tmp[i]/t[i]);
    //    tmp[i] = fabs(tmp[i]);
    //------------------------------------------------------
    tmp[i] = fabs(a[i] -b[i]) ;

    if ( tmp[i]>t[i]/2. ) tmp[i] -= t[i];
  }
  return sqrt( 
	   tmp[0]*tmp[0] +
	   tmp[1]*tmp[1] +
	   tmp[2]*tmp[2] 
	   );

}
/***************************************************************************/
/*
   Calcul de la distance AU CARRE entre 2 points a et b ds un 
   "parallelepipede periodique":
   [0,T1[ x [0,T2[ x [0,T3[
   La periode est donnee par t.
   Les points donnes en entree NE DOIVENT PAS etre en dehors de [0,T1[ x [0,T2[ x [0,T3[
   Avantage: c'est beaucoup plus rapide que dist_p
             et comme on ne retourne pas la distance mais son carre,
	     cela va encore plus vite que dist_p0 (pas de racine
	     carree a calculer).
*/
/***************************************************************************/
double dist2_p0( double a[3], double b[3], double t[3]) {
  double tmp[3];
  int i;


  for(i=0; i<3; i++) {
    tmp[i] = fabs(a[i] -b[i]) ;
    //------------------------------------------------------
    // si on voulait rester ds le cas g�n�ral o� les points a et b 
    // peuvent �tre en dehors de [0,T1[ x [0,T2[ x [0,T3[
    // il fadrait mettre les 3 lignes suivantes:
    //    tmp[i] = a[i]-b[i];
    //    tmp[i] -= t[i]*floor(tmp[i]/t[i]);
    //    tmp[i] = fabs(tmp[i]);
    //------------------------------------------------------
    /* Les 2 lignes qui suivent sont equivalentes, la 2e est un peu plus rapide */
    /* if ( tmp[i]>t[i]/2. ) tmp[i] -= t[i];   */
	tmp[i] -= ( tmp[i]>0.5*t[i] ) * t[i] ;
  }
  return ( 
	  tmp[0]*tmp[0] +
	  tmp[1]*tmp[1] +
	  tmp[2]*tmp[2] 
	  );
  
}


/************************************************************************************/
/* moins rapide mais pas si mal que ca! */
/*   Les points donnes en entree NE DOIVENT PAS etre en dehors de [0,T1[ x [0,T2[ x [0,T3[ 
 (du moins, ils doivent �tre dans la m�me p�riode) */
double dist2_p1( double *x, double *xx, double *t){
  int i,j,k;
  double d;
  double dbuf;
  double dd[3];
  
  d = 1.e128;

  for(i=-1;i<=1;i++) {
    dd[2] = x[2]+(double)i*t[2] - xx[2];
    dd[2] = dd[2]*dd[2];

    for(j=-1;j<=1;j++) {
      dd[1] = x[1] + (double)j*t[1] - xx[1];
      dd[1] = dd[1]*dd[1];

      for(k=-1;k<=1;k++) {
	dd[0] = x[0] + (double)k*t[0] - xx[0];

	dbuf = dd[0]*dd[0] + dd[1] + dd[2];

	if (dbuf<d) d = dbuf;

      }
    }
  }
  return d;
}

/************************************************************************************/
/* aussi rapide que dist2_p0 (p.e. mais un peu plus) MAIS seulement sur des periodes 
  [0,1[x[O,1[x[0,1[ 
*/
double dist2_p2( double *x, double *xx){
  int i,j,k;
  double d;
  double dbuf;
  double dd[3];
  
  dd[0] = fabs(x[0]-xx[0]);
  dd[0] = dd[0]-(int)(dd[0]+0.5);
  dd[1] = fabs(x[1]-xx[1]);
  dd[1] = dd[1]-(int)(dd[1]+0.5);
  dd[2] = fabs(x[2]-xx[2]);
  dd[2] = dd[2]-(int)(dd[2]+0.5);
  
  return dd[0]*dd[0]+dd[1]*dd[1]+dd[2]*dd[2];

}

/************************************************************************************/
/* distance euclidienne */
double dist( double x[3], double y[3]){
  double d[3];
  d[0] = x[0]-y[0];
  d[1] = x[1]-y[1];
  d[2] = x[2]-y[2];
  return sqrt( d[0]*d[0] + d[1]*d[1] + d[2]*d[2] );
}
/************************************************************************************/
/* carr� de la distance euclidienne */
double dist2( double x[3], double y[3]){
  double d[3];
  d[0] = x[0]-y[0];
  d[1] = x[1]-y[1];
  d[2] = x[2]-y[2];
  return ( d[0]*d[0] + d[1]*d[1] + d[2]*d[2] );
}
  
/************************************************************************************/
double dp( double a[3], double b[3], double t[3]) {
  double v[3];

  
  /* - - - - - - - - - - - -
  int n[3];
  double d;

  v[0]=b[0]-a[0];
  v[1]=b[1]-a[1];
  v[2]=b[2]-a[2];

  n[0] = (int)floor(0.5-v[0]/t[0]);
  n[1] = (int)floor(0.5-v[1]/t[1]);
  n[2] = (int)floor(0.5-v[2]/t[2]);

  v[0]=v[0]+n[0]*t[0];
  v[1]=v[1]+n[1]*t[1];
  v[2]=v[2]+n[2]*t[2];

  d = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
  printf("d=%f\n",d);
  return d;
  - - - - - - - - - - - - */
  
  /* plus concis: */
  v[0] = b[0]-a[0]+floor(0.5-(b[0]-a[0])/t[0])*t[0];
  v[1] = b[1]-a[1]+floor(0.5-(b[1]-a[1])/t[1])*t[1];
  v[2] = b[2]-a[2]+floor(0.5-(b[2]-a[2])/t[2])*t[2];
  
  return sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
  
}
/************************************************************************************/
double dp2( double a[3], double b[3], double t[3]) {
  double v[3];

  v[0] = b[0]-a[0]+floor(0.5-(b[0]-a[0])/t[0])*t[0];
  v[1] = b[1]-a[1]+floor(0.5-(b[1]-a[1])/t[1])*t[1];
  v[2] = b[2]-a[2]+floor(0.5-(b[2]-a[2])/t[2])*t[2];
  
  return (v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
  
}
/*********************************************************************************/
/* modulo function

   modulo(a,n) computes:
   a modulo n = a - n * E(a/n)    
   where E(x) is the integer part of x, i.e. the largest integer less than or equal to x

   It differs from the % operation for negative operand which computes:
   a%n = a - n * T(a/n)
   where T(x) is the decimal truncature of x, i.e. T(x) = sign(x)*E(|x|)
   
*/
int modulo(const int a, const int n){
  return  a-(int)floor((double)a/(double)n)*n;
}


#endif
