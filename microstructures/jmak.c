/******************************************************************************************/
/* HM
   21/06/2011

   This program generates an image following one of the following processes:
   - Johnson-Mehl-Avram-Kolmogorov process
   - Laguerre-Delaunay process
   - "Suquet" process
   - Vorono� process (degenerated case of Laguerre or JMAK process)

   For a given set of "seeds" described by their spatial positions xi and a given scalar ti
   (i = 1, ..., N) , a given point of the space is set to belong to the kth grain if
   k is the index minimizing f(x,xi)

   For JMAK process:
         f(x,xi) = d(x,xi) + g*ti      
   where d(x,xi) is the euclidian distance, g is a given speed of grain growing. 
   In this case ti is the time at which ith grain appears and begins to grow with radial
   velocity g.

   For Laguerre-Delaunay process:
         f(x,xi) = d(x,xi)^2 + v*ti      

   For "Suquet process":
         f(x,xi) = d(x,xi) / (g*ti) 
   It simulates the case of grain seeds appearing at same time,
   but growing with varying velocity v=g*ti

   For Vorono� process:
         f(x,xi) = d(x,xi)        




*/
/******************************************************************************************/
#define _DEFAULT_SOURCE
#define _ISOC99_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <string.h>

#include <dist.h>
#include <craftimage.h>
#include <utils.h>

#include <errno.h>

#ifndef TINY
#define TINY 1.e-12
#endif

/******************************************************************************************/
void usage();

/******************************************************************************************/
int main( int argc, char **argv) {
  
  int status;

  double V[3]={1.,1.,1.};
  int N[3]={0,0,0};
  char *filename=(char *)0;

  double growrate=NAN;

  CraftImage image;

  int type[3]={CRAFT_IMAGE_INT,0,0};


  double (*distance)( double *x, double *y, double *t);


  /* method=0  Laguerre
     method=1 JMAK
     method=2 Suquet
     method=3 Voronoi
   */
  int method=-1;

  craft_image_init( &image, type );

  while( (status=getopt(argc, argv, "hg:v:n:f:o:s:LJSV")) != -1 ){
    
    switch(status) {
      
    case 'h':
      usage();
      exit(0);
    case 'g':
      if (method==3) {
	fprintf(stderr,"jmak warning: -g option is ignored for Voronoi process\n");
      }
      sscanf(optarg,"%lf",&growrate);
      break;
    case 'v':
      sscanf(optarg,"%lfx%lfx%lf",V,V+1,V+2);
      break;
    case 's':
      sscanf(optarg,"%lf,%lf,%lf",image.s,image.s+1,image.s+2);
      break;
    case 'n':
      sscanf(optarg,"%dx%dx%d",N,N+1,N+2);
      break;
    case 'f':
      filename = malloc(strlen(optarg)+1);
      sscanf(optarg,"%s",filename);
      break;
    case 'o':
      image.name = malloc(strlen(optarg)+1);
      sscanf(optarg,"%s",image.name);
      break;
    case 'L':
      if(method!=-1){
	fprintf(stderr,"Error in jmak: \n");
	fprintf(stderr,"  options -L and -J are incompatible\n");
	exit(1);
      }
      method=0;
      
      break;
    case 'J':
      if(method!=-1){
	fprintf(stderr,"Error in jmak: \n");
	fprintf(stderr,"  options -J and -L -S or -V are incompatible\n");
	exit(1);
      }
      method=1;
      break;
    case 'S':
      if(method!=-1){
	fprintf(stderr,"Error in jmak: \n");
	fprintf(stderr,"  options -S and -L -V or -J are incompatible\n");
	exit(1);
      }
      method=2;
      
      break;
    case 'V':
      if(method!=-1){
	fprintf(stderr,"Error in jmak: \n");
	fprintf(stderr,"  options -V and -L -S or -J are incompatible\n");
	exit(1);
      }
      method=3;
      if (!isnan(growrate)) {
	fprintf(stderr,"jmak warning: -g option is ignored for Voronoi process\n");
      }
      
      break;
    default:
      fprintf(stderr,"invalid option %s\n",optarg);
      usage();
      exit(1);
      break;
    }
  }

  
  /*---------------------------------------------------------------------------------------*/
  if (isnan(growrate)) growrate=1.;
  if(method==-1) method=1;

  switch (method) {
  case 1:
    distance = dist_p0;
    break;
  case 0:
    distance = dist2_p0;
    break;
  case 2:
    distance = dist2_p0;
    break;
  case 3:
    distance = dist2_p0;
    break;
  default:
    fprintf(stderr,"error in jmak. Contact developper. \n");
    exit(1);
  }





  if (argc==1) {
    usage();
    exit(1);
  }


  if ( (N[0]<=0)||(N[1]<=0)||(N[2]<=0) ) {
    fprintf(stderr,"-n n1xn2xn3 option is mandatory (number of points to be placed)\n");
    exit(1);
  }

  if( (optind<argc) ){
    if (filename==(char *)0) {
      filename = malloc(strlen(argv[optind]+1));
      sscanf( argv[optind] , "%s", filename);
      optind++;
    }
  }

  if( (optind<argc) ){
    if (image.name==(char *)0) {
      image.name = malloc(strlen(argv[optind]+1));
      sscanf( argv[optind] , "%s", image.name);
    }
    else{
      fprintf(stderr,"invalid argument %s\n",argv[optind]);
      usage();
      exit(1);
    }
  }

  if(image.name==(char *)0){
    fprintf(stderr,"-o output_file option is mandatory\n");
    exit(1);
  }

  /* if image.name extension is vtk, one must suppress it now because write_image will add it */
  if (strcmp(image.name+strlen(image.name)-strlen(".vtk"),".vtk")==0) {
    image.name[strlen(image.name)-strlen(".vtk")]='\0';
  }
  /*---------------------------------------------------------------------------------------*/

  FILE *f;
  int k;

  f = fopen(filename,"r");
  if (f == (FILE *)0 ){
    if (errno == ENOENT) {
      
      fprintf(stderr,"Error in jmak:\n");
      fprintf(stderr,"file %s does not exist.\n", filename);
      exit(1);
    }
    else{
      
      fprintf(stderr,"Error in jmak:\n");
      fprintf(stderr," a problem occurred while opening file %s.\n", filename);
      exit(1);
    }
  }
  
  int nseed=0;
  double (*x)[3]=(double (*)[3])0;
  double *t=(double *)0;
  double *t2=(double *)0;
  double *val=(double *)0;

  x = (double (*)[3])malloc( 1 * sizeof(*x));
  t = (double *)malloc(1*sizeof(*t));
  val = (double *)malloc(1*sizeof(*val));
  
  //  printf("\n"); k=0;
  do{
    status=craft_fscanf(f,"%lf %lf %lf %lf %lf", &x[0][0], &x[0][1], &x[0][2], &t[0], &val[0] );
    //    printf("%lf %lf %lf %lf %lf \n", x[k][0], x[k][1], x[k][1], t[k] , val[k]);
    if (status==5) nseed++;
  } while(status==5);
  
  if(nseed==0){
    fprintf(stderr,"Error in jmak: empty file %s\n",filename);
    exit(1);
  }

  free(x);
  free(t);
  free(val);

  x = (double (*)[3])malloc( nseed * sizeof(*x));
  t = (double *)malloc(nseed*sizeof(*t));
  val = (double *)malloc(nseed*sizeof(*val));
  
  rewind(f);

  //  printf("\n");
  for(k=0;k<nseed; k++) {
    status=craft_fscanf(f,"%lf %lf %lf %lf %lf", &x[k][0], &x[k][1], &x[k][2], &t[k], &val[k] );
    //    printf("%lf %lf %lf %lf %lf \n", x[k][0], x[k][1], x[k][1], t[k] , val[k]);
  }
  


  fclose(f);
    
  if (method==2) {
    t2 = (double *)malloc(nseed*sizeof(*t2));
    for(k=0;k<nseed;k++) t2[k] = 1. / pow(t[k],2.*growrate);
  }
  /*---------------------------------------------------------------------------------------*/
  int i;
  for(i=0;i<3;i++) {
    image.n[i] = N[i];
    if(image.n[i]==0) image.n[i]=1;
    image.nd[i] = image.n[i];
    image.p[i] = V[i]/(double)N[i];
    if (image.p[i] < TINY ){
      if (image.n[i]==1) {
	image.p[i] = 1.;
      }
      else{
	fprintf(stderr,"invalid dimension of image:\n");
	fprintf(stderr," V=%f x %f x %f\n",V[0],V[1],V[2]);
	fprintf(stderr," n=%d x %d x %d\n",N[0],N[1],N[2]);
	exit(1);
      }
    }

  }
  craft_image_alloc(&image);
  /*---------------------------------------------------------------------------------------*/
  int I2,I1,I0;
  int ii;
  double X[3];
  double d2, d2m;
  
  ii=0;

  switch(method){
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Laguerre  + JMAK */
  case 0:
  case 1:
#pragma omp parallel for				\
  default(none)						\
  shared(image,val,growrate,nseed,t,x,V,distance)	\
  private(I1,I2,I0,X,i,ii,d2,k,d2m)

    for(I2=0; I2<image.n[2]; I2++){
      //    printf("%d\n",I2);
      i=2;X[i]=image.s[i] + image.p[i]*I2;
      for(I1=0; I1<image.n[1]; I1++){
	i=1; X[i]=image.s[i] + image.p[i]*I1;
	for(I0=0; I0<image.n[0]; I0++){
	  i=0; X[i]=image.s[i] + image.p[i]*I0;

	  ii = (I2*image.nd[1] + I1)*image.nd[0] + I0;
	
	  k=0;
	  d2 = distance( X, x[k], V ) + t[k]*(growrate);
	  d2m = d2;
	
	  image.pxl.i[ii] = val[0];
	  for(k=1;k<nseed;k++) {
	    d2 = distance( X, x[k], V ) + t[k]*(growrate);

	    if ( d2 < d2m ) {
	      d2m = d2;
	      image.pxl.i[ii] = val[k];
	    }
	  
	  
	  }
	}
      }
    }
    break;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Vorono� */
  case 3:
#pragma omp parallel for				\
  default(none)						\
  shared(image,val,nseed,t,x,V,distance)	\
  private(I1,I2,I0,X,i,ii,d2,k,d2m)

    for(I2=0; I2<image.n[2]; I2++){
      i=2;X[i]=image.s[i] + image.p[i]*I2;
      for(I1=0; I1<image.n[1]; I1++){
	i=1; X[i]=image.s[i] + image.p[i]*I1;
	for(I0=0; I0<image.n[0]; I0++){
	  i=0; X[i]=image.s[i] + image.p[i]*I0;

	  ii = (I2*image.nd[1] + I1)*image.nd[0] + I0;
	
	  k=0;
	  d2 = distance( X, x[k], V );
	  d2m = d2;
	
	  image.pxl.i[ii] = val[0];
	  for(k=1;k<nseed;k++) {
	    d2 = distance( X, x[k], V );


	    if ( d2 < d2m ) {
	      d2m = d2;
	      image.pxl.i[ii] = val[k];
	    }
	  
	  
	  }
	}
      }
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /* Suquet's method */
  case 2:
#pragma omp parallel for				\
  default(none)						\
  shared(image,val,nseed,t2,x,V,distance)	\
  private(I1,I2,I0,X,i,ii,d2,k,d2m)

    for(I2=0; I2<image.n[2]; I2++){
      //    printf("%d\n",I2);
      i=2;X[i]=image.s[i] + image.p[i]*I2;
      for(I1=0; I1<image.n[1]; I1++){
	i=1; X[i]=image.s[i] + image.p[i]*I1;
	for(I0=0; I0<image.n[0]; I0++){
	  i=0; X[i]=image.s[i] + image.p[i]*I0;

	  ii = (I2*image.nd[1] + I1)*image.nd[0] + I0;
	
	  k=0;
	  //d2 = dist2_p0( X, x[k], V ) * t2[k];

	  d2 = distance( X, x[k], V ) * t2[k];
	  d2m = d2;
	
	  image.pxl.i[ii] = val[0];
	  for(k=1;k<nseed;k++) {
	    d2 = dist2_p0( X, x[k], V ) * t2[k];
	    // d2 = distance( X, x[k], V ) + t[k]*(growrate);

	    if ( d2 < d2m ) {
	      d2m = d2;
	      image.pxl.i[ii] = val[k];
	    }
	  
	  
	  }
	}
      }
    }
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  default:
    fprintf(stderr,"error in jmak. Contact developper. \n");
    exit(1);
    break;
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  }
  /*---------------------------------------------------------------------------------------*/
  write_image( image.name, &image, "vtk" );
  /*---------------------------------------------------------------------------------------*/

  
}
/******************************************************************************************/
void usage(){
  fprintf(stderr,"Usage of jmak\n");
  fprintf(stderr,"   jmak -n n1xn2xn3 [-g growing_rate] [-s s1,s2,s3] [-v v1xv2xv3] [-J] [-L] [-V] [-f] point_file [-o] image_file\n");
  fprintf(stderr,"\n");
  fprintf(stderr,"Options:\n");
  fprintf(stderr," -n n1xn2xn3 : number of pixels of the output image (madatory option) \n");

  fprintf(stderr," -v v1xv2xv3 : the volume in which the points are to be placed is\n");
  fprintf(stderr,"               given by v1 v2 and v3 (in 3d) or v1 and v2 (in 2d)\n");
  fprintf(stderr,"              ( by default: 1. 1. 1. (in 3d) or 1. 1. (in 2d) ) \n");

  fprintf(stderr," -s s1,s2,s3 : the coordinates of the first pixel in the image   \n");
  fprintf(stderr,"               ( by default: 0. 0. 0. ). \n");

  fprintf(stderr," -g growing_rate : crystal growing velocity (default: 1.) \n");

  fprintf(stderr," -f point_file : name of the input file containing the seeds of the tesselation\n");
  fprintf(stderr," -o image_file : name of the output file to be created \n");

  fprintf(stderr," -J : Johnson-Mehl-Avrami-Kolmogorov tesselation (default) \n");
  fprintf(stderr," -L : Laguerre tesselation\n");
  fprintf(stderr," -V : Voronoi tesselation\n");
  fprintf(stderr," options -L and -J are incompatible\n"); 
  

  
}
