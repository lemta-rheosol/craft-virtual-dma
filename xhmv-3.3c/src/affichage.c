/*************************************************************************/
/* HM decembre 2005
 *
 * fonctions gerant le "fichier  d'affichage" de xhmv, i.e. le fichier
 * qui contient les donnees decrivant l'affichage d'une image (le nom
 * du fichier-image, le facteur de zoom, la taille de la fenetre d'affichage,
 * les cuts, le start, la lut, ...).
 *    
 *   lit_affichage   : lit le fichier d'affichage et copie les donnees ds
 *                     une structure Affichage
 *   print_affichage : ecrit les donnes d'une structure Affichage ds un
 *                     flux de sortie
 *   sauve_affiche   : ecrit les donnes d'une structure Affichage ds un
 *                     fichier
 *   copie_affiche   : copie tous les champs d'une variable de type Affichage
 *                     dans une autre
 */
/*************************************************************************/
#include <stdio.h>
#include <xhmv.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
/*************************************************************************/
#define MAXCOMPTE 200000
int lit_affichage( char *fichier_affiche, Affichage *affiche ){
  FILE *f;
  int status;
  int return_status=0;
  int erreur;
  int compte;

  compte = 0;
  do {
    f = fopen(fichier_affiche,"r");
    compte++;
  } while( (f==(FILE *)NULL) && (compte<MAXCOMPTE) );
  erreur = errno;
  if (f==(FILE *)NULL) {
    /*
      fprintf(stderr," lit_affichage: erreur a l'ouverture de %s\n",fichier_affiche); 
      fprintf(stderr," errno = %d %d\n",erreur,compte); 
    */
    return -1;
  }

  status = fscanf(f, "%s", affiche->fichier_image);
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de fichier_image\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%d", &(affiche->zoom) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de zoom\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%g", &(affiche->cuts[0]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de cuts[0]\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%g", &(affiche->cuts[1]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de cuts[1]\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%lf", &(affiche->s[0]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de s[0]\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%lf", &(affiche->s[1]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de s[1]\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%d", &(affiche->n[0]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de n[0]\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%d", &(affiche->n[1]) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de n[1]\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%d", &(affiche->lut) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de lut\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%ld", (long *)&(affiche->display) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de display\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%ld", (long *)&(affiche->fenetre) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de fenetre\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%ld", (long *)&(affiche->display_interloc) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de display_interloc\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%ld", (long *)&(affiche->fenetre_interloc) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de fenetre_interloc\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%d", &(affiche->message) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de message\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%d", &(affiche->occupe) );
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de occupe\n");
#endif
    return_status = -2;
  }

  fgets( (char *)(affiche->texte) , LONGTXT , f ); /* il faut passer a la ligne suivante! */
  fgets( (char *)(affiche->texte) , LONGTXT , f ); 

  status = fscanf(f, "%s", affiche->fichier_postscript);
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de fichier_postscript\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%d", &(affiche->orientation_postscript));
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de orientation_postscript\n");
#endif
    return_status = -2;
  }


  status = fscanf(f, "%d", &(affiche->period));
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de orientation_postscript\n");
#endif
    return_status = -2;
  }

  status = fscanf(f, "%d", &(affiche->lutbar));
  if (status!=1) {
#ifdef DEBUG
    fprintf(stderr,"erreur de lecture ds %s\n",fichier_affiche);
    fprintf(stderr,"   a la lecture de orientation_postscript\n");
#endif
    return_status = -2;
  }


  fclose(f);

  return return_status;
 
}
/*************************************************************************/
int print_affichage( FILE *f, int verbosite, Affichage affiche ){
  if (verbosite==0) {
    fprintf(f,"%s\n",affiche.fichier_image);
    fprintf(f,"%d\n",affiche.zoom);
    fprintf(f,"%g %g\n",affiche.cuts[0],affiche.cuts[1]);
    fprintf(f,"%lg %lg\n",affiche.s[0],affiche.s[1]);
    fprintf(f,"%d %d\n",affiche.n[0],affiche.n[1]);
    fprintf(f,"%d\n",affiche.lut);
    fprintf(f,"%ld \n", affiche.display);
    fprintf(f,"%ld \n", affiche.fenetre);
    fprintf(f,"%ld \n", affiche.display_interloc);
    fprintf(f,"%ld \n", affiche.fenetre_interloc);
    fprintf(f,"%d\n",affiche.message);
    fprintf(f,"%d\n",affiche.occupe);
    fprintf(f,"%s\n",affiche.texte);
    fprintf(f,"%s\n",affiche.fichier_postscript);
    fprintf(f,"%d\n",affiche.orientation_postscript);
    fprintf(f,"%d\n",affiche.period);
    fprintf(f,"%d\n",affiche.lutbar);
  }
  else {
    fprintf(f,"  image   = %s\n",affiche.fichier_image);
    fprintf(f,"  zoom    = %d\n",affiche.zoom);
    fprintf(f,"  cuts    = %g %g\n",affiche.cuts[0],affiche.cuts[1]);
    fprintf(f,"  start   = %lg %lg\n",affiche.s[0],affiche.s[1]);
    fprintf(f,"  n       = %d %d\n",affiche.n[0],affiche.n[1]);
    fprintf(f,"  lut     = %d\n",affiche.lut);
    fprintf(f,"  display = %ld \n", affiche.display);
    fprintf(f,"  fenetre = %ld \n", affiche.fenetre);
    fprintf(f,"  display = %ld \n", affiche.display_interloc);
    fprintf(f,"  fenetre = %ld \n", affiche.fenetre_interloc);
    fprintf(f,"  message = %d\n",affiche.message);
    fprintf(f,"  occupe  = %d\n",affiche.occupe);
    fprintf(f,"  texte   = %s\n",affiche.texte);
    fprintf(f,"  imprime = %s\n",affiche.fichier_postscript);
    fprintf(f,"  orientn = %d\n",affiche.orientation_postscript);
    fprintf(f,"  periodn = %d\n",affiche.period);
    fprintf(f,"  periodn = %d\n",affiche.lutbar);
  }
  return 0;
}

/*************************************************************************/
int sauve_affiche(char *fichier_affiche, Affichage affiche){
  FILE *f;
  int status;

  f = fopen(fichier_affiche,"w");
  if (f==(FILE *)0) {
    //    fprintf(stderr,"xhmv: erreur a l'ouverture de %s pour ecriture\n",fichier_affiche);
    return -1;
  }
  status = print_affichage(f,0,affiche);
  fclose(f);
  
  return 0;
  
}

/*************************************************************************/
/* permet la copie de tous les champs d'un objet Affichage ds un autre */
int copie_affiche(Affichage *dst, Affichage *src) {

  strcpy(dst->fichier_image, src->fichier_image);

  dst->zoom = src->zoom;

  dst->cuts[0] = src->cuts[0];
  dst->cuts[1] = src->cuts[1];

  dst->s[0] = src->s[0];
  dst->s[1] = src->s[1];
  
  dst->n[0] = src->n[0];
  dst->n[1] = src->n[1];
  
  dst->lut  = src->lut ;

  dst->display = src->display;

  dst->fenetre = src->fenetre;

  dst->display_interloc = src->display_interloc;
  dst->fenetre_interloc = src->fenetre_interloc;

  dst->message = src->message;

  dst->occupe = src->occupe;

  strcpy(dst->texte , src->texte);

  strcpy(dst->fichier_postscript , src->fichier_postscript);

  dst->orientation_postscript = src->orientation_postscript;

  dst->period = src->period;

  dst->lutbar = src->lutbar;

  return 0;

}


