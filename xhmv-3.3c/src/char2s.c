/*****************************************************************************
 * cette fonction convertit une chaine de n caracteres FORTRAN c en 
 * une chaine de caracteres C s.
 * 
 * 
 * HM. 29 septembre 1995.
 *****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef CRAY
void CHAR2S(unsigned char* c, int *lc, unsigned char *s, int *ls)
     {
	int i;
	unsigned char *ps, *pc;
	if (*ls<*lc+1)
	  {
	    printf("pb ds char2s: la chaine destinataire est plus courte ");
	    printf("que la chaine source.\n");
	    exit(-1);
          }
	ps = s; pc = c;
	for(i=0;i<*lc;i++) *ps++ = *pc++;
	*ps = 0;
     }
#elif VAX
#include <descrip.h>

void char2s(struct dsc$descriptor_s *c, int *lc, unsigned char *s, int *ls)
     {
	if (*ls<*lc+1)
	  {
	    printf("pb ds char2s: la chaine destinataire est plus courte ");
	    printf("que la chaine source.\n");
	    exit(-1);
          }
	memcpy(s,c->dsc$a_pointer,*lc);
     }

#else
void char2s_(unsigned char* c, int *lc, unsigned char *s, int *ls)
     {
	int i;
	unsigned char *ps, *pc;
	if (*ls<*lc+1)
	  {
	    printf("pb ds char2s: la chaine destinataire est plus courte ");
	    printf("que la chaine source.\n");
	    exit(-1);
          }
	ps = s; pc = c;
	for(i=0;i<*lc;i++) *ps++ = *pc++;
	*ps = 0;
     }
#endif
