#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <X11/cursorfont.h>


/* niveau de verbosite */
extern int verbose;

/*------------------------------------------------------------*/
/* choix du mode visuel le plus "riche" parmi ceux que permet 
   le display.

   on regarde si on peut arriver a fonctionner en mode
   - pseudo-couleur 8 bits/pixel
   - vraie couleur 16 bits/pixel
   - vraie couleur 24 bits/pixel

   La fonction retourne le mode visual retenu.
   La valeur retourne est 0 en cas d'echec, 1 en cas de succes.

*/
/*------------------------------------------------------------*/
int choix_visual(   Display *display, XVisualInfo *vinfo_return) {

  int status;

  int choix=0;
  

  int i, j, k;

#define NB_VISUELS 3
  int mode_visuel[NB_VISUELS]={TrueColor, TrueColor, PseudoColor};
  int bpp_visuel[NB_VISUELS]={24,16,8};
  char *nom_visuel[NB_VISUELS]={"TrueColor", "TrueColor", "PseudoColor"};
  int visuel_possible[NB_VISUELS]={0,0,0};
  int nb_visuels_possibles=0;

  
  if (verbose) {
    fprintf(stderr,"Modes visuels possibles sur votre serveur X: \n");
  }
  for(i=0; i<NB_VISUELS; i++) {
    status = XMatchVisualInfo(
			      display, 
			      DefaultScreen(display), 
			      bpp_visuel[i], 
			      mode_visuel[i], 
			      vinfo_return);
    if(status!=0) {
      nb_visuels_possibles++;
      visuel_possible[i]=1;
      if (verbose) {
	fprintf(stderr,
		"   %s %d bits/pixel\n",
		nom_visuel[i], 
		bpp_visuel[i]
		);
      }
    }
  }
  
  if(nb_visuels_possibles==0) {
    fprintf(stderr,"... aucun mode visuel possible!\n");
    return 0;
  }
  
  /* on retient le mode le plus "riche" (16 bits/pxl plutot que 8, 24
     plutot que 16) */
  nb_visuels_possibles=0;
  for(i=0; i<NB_VISUELS; i++) {
    if (visuel_possible[i]==1) {
      if (verbose) {
	fprintf(stderr,
		"\nMode visuel retenu: \n   %s %d bits/pixel\n\n",
		nom_visuel[i], 
		bpp_visuel[i]
		);
      }
      status = XMatchVisualInfo(
				display, 
				DefaultScreen(display), 
				bpp_visuel[i], 
				mode_visuel[i], 
				vinfo_return);
      break;
    }
  }

  return 1;
  
  
}
