#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <X11/cursorfont.h>

#include <xhmv.h>

/****************************************************************************/
void ima2ps( char *name, 
	    int n1, int n2, 
	    unsigned char *r,
	    unsigned char *g,
	    unsigned char *b,
	    int turnflag
	    );
/****************************************************************************/
/* copie de l'image affichee ds un fichier postscript */
void copie_postscript(
		      H2Imageuc ima3, H2Imageuc ima2, couleur *lut,
		      char *nom_image_eps, int turn){


  int *r,*g,*b;
  unsigned char *imar, *imag, *imab;
  H2Imageuc cadre;
  int i,j;
  int i1,i2;
  int ii1, ii2, ii;
  int d1, d2;


  /* on commence par aller chercher la LUT a utiliser: */

  r = malloc(sizeof(*r)*256);
  g = malloc(sizeof(*g)*256);
  b = malloc(sizeof(*b)*256);

  for(i=0;i<256;i++){
    r[i] = lut[i].r;
    g[i] = lut[i].g;
    b[i] = lut[i].b;

  }

#ifdef CADRAGE
  /* ds ce cas on cadre l'image a imprimer "au plus serr�" avec l'image
     affich�e: c'est � dire que si l'image affich�e est plus petite
     que la fen�tre ds laquelle elle est affich�e, alors on n'imprime
     que l'image (on n'imprime pas les bords noirs). Ce cas est
     MAL PROGRAMM�: cela plante quand l'image n'est pas centr� ET
     qu'il y a des bords noirs (c'�tait un cas impossible ds les
     pr�c�dentes version sde xhmv).
     Je ne m'emb�te plus, et ds le cas ou on ne fait pas ce 
     CADRAGE, on imprime ce qui est affich�, tel quel.
  */

  printf("copie post: 2\n");
     
  /* on est condamne a chercher l'image a afficher car ce peut etre
     une portion seule de l'image etudiee: */
  for(i=0;i<3;i++){
    cadre.n[i]=ima3.n[i];
    cadre.nd[i]=ima3.nd[i];
    cadre.s[i]=ima3.s[i];
    for(j=0;j<3;j++) {
      cadre.p[i][j] = ima3.p[i][j];
    }
  }
  cadre.pxl = (unsigned char *)NULL;

  /* est ce que le cadre de l'image affichee (ima3) 
     depasse celui de l'image zoomee (ima2)? */
  for (i=0; i< 2; i++) {
    if (cadre.n[i] > ima2.n[i] ) {
      cadre.n[i] = ima2.n[i];
      cadre.s[i] = ima2.s[i];
    }
  }
  for (i=0; i< 2; i++) {
    cadre.nd[i] = cadre.n[i];
  }

  cadre.pxl = (unsigned char *)malloc(cadre.nd[0]*cadre.nd[1]*sizeof(*cadre.pxl));

  d2 = rint((cadre.s[1]-ima2.s[1])/ima2.p[1][1]);
  d1 = rint((cadre.s[0]-ima2.s[0])/ima2.p[0][0]);

  for(i2=0; i2<cadre.n[1]; i2++) {
    for(i1=0; i1<cadre.n[0]; i1++) {
      i = i1 + i2 * cadre.nd[0];
      ii = (i1+d1) + (i2+d2)*ima2.nd[0];
      ii1 = i1+d1;
      ii2 = i2+d2;
      if ( (ii2>=ima2.n[1]) || (ii1<0) ) {
	printf("ca va pas %d %d %d %d\n",i1,i2,ii1,ii2);
      }
      cadre.pxl[i] = ima2.pxl[ii];
    }
  }
#endif

  cadre = ima3;

  imar = (unsigned char *)malloc(cadre.nd[0]*cadre.nd[1]*sizeof(*imar));
  imag = (unsigned char *)malloc(cadre.nd[0]*cadre.nd[1]*sizeof(*imag));
  imab = (unsigned char *)malloc(cadre.nd[0]*cadre.nd[1]*sizeof(*imab));

  i = 0 ;
  /* HM 21 mars 2006 je rajoute une inversion haut-bas, je ne sais
     pas pourquoi mais maintenant il le faut ! */
  for(i2=0; i2<cadre.n[1]; i2++) {
    for (i1=0; i1<cadre.n[0]; i1++) {
      ii = (cadre.n[1]-i2-1)*cadre.n[0]+i1;
      imar[i] = r[cadre.pxl[ii]];
      imag[i] = g[cadre.pxl[ii]];
      imab[i] = b[cadre.pxl[ii]];
      i++;
    }
  }


  /* et on imprime */
  ima2ps(nom_image_eps, cadre.n[0], cadre.n[1], imar, imag, imab, turn);

  free(r);
  free(g);
  free(b);

  free(imar);
  free(imag);
  free(imab);
  /*
  free(cadre.pxl);
  */

}
		      



