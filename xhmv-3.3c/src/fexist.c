#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

/* retourne 1 si le fichier existe, 0 sinon */
int fexist(char *file){
  int status;
  struct stat buf;
  
  status = stat(file, &buf);

  if ( (status==-1) && (errno==ENOENT) ) {
    return 0;
  }
  return 1;
}    
