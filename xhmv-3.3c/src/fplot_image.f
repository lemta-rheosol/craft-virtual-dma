*******************************************************************************
*
* HM 27/12/2005
*
* wrapper fortran-C pour les fonctions de plot image:
*
* - f_plot_image
* - f_replot_image
* - f_modify_plot_image
* - f_close_plot_image
*
* Remarque: 
* en fait seules les fonctions f_plot_image et f_modify_plot_image
* sont vraiment utiles car elles ont pour argument une chaine de caractere
* (qu'il faut donc traduire avant de le passer a plot_image_ ou modify_plot_image_
*  qui sont ecrites en C).
* J'ai cependant cree ces fonctions par souci de coherence ds le nom des 
* fonctions d'appel depuis fortran.
*
*
*******************************************************************************
      integer function f_plot_image(
     &     fichier_image,
     &     zoom,     
     &     n,
     &     s,
     &     c,
     &     l
     &     )     
      implicit none


      integer plot_image
      external plot_image

      character*(*) fichier_image
      integer zoom
      integer n(2)
      real c(2)
      double precision s(2)
      integer l


      integer ilen
      integer ifile(200)

      ilen=index(fichier_image," ")-1
      if(ilen.eq.-1) then
         f_plot_image = -1
         return
      end if

      call char2s(fichier_image,ilen,ifile,200)

      f_plot_image = plot_image(ifile,zoom,n,s,c,l)

      return
      end

      
*******************************************************************************
      integer function f_modify_plot_image(
     &     id_plot,
     &     fichier_image,
     &     zoom,     
     &     n,
     &     s,
     &     c,
     &     l
     &     )
      implicit none

      integer modify_plot_image
      external modify_plot_image

      integer id_plot
      character*(*) fichier_image
      integer zoom
      integer n(2)
      real c(2)
      double precision s(2)
      integer l

      integer ilen
      integer ifile(200)

      ilen=index(fichier_image," ")-1
      if(ilen.eq.-1) then
         f_modify_plot_image = -1
         return
      end if

      call char2s(fichier_image,ilen,ifile,200)

      f_modify_plot_image = 
     &     modify_plot_image(id_plot,ifile,zoom,n,s,c,l)

      return
      end

      

*******************************************************************************
      integer function f_close_plot_image(id)
      implicit none

      integer close_plot_image
      external close_plot_image


      integer id
      f_close_plot_image = close_plot_image(id)
      end
*******************************************************************************
      integer function f_replot_image(id)
      implicit none

      integer replot_image
      external replot_image

      integer id
      f_replot_image = replot_image(id)
      end

