/* 
   ima2ps

   HM
   programme creant une image postscript en orientation
   portrait ou landscape a partir des 3 canaux RGB de l'image
   (autrement dit de 3 images pour le rouge le vert et le bleu).

   J'ai pique ce programme je ne sais plus ou.

*/
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>

#define COULEUR 1
#define MARGIN 0.95

void putrest(FILE *file);
void putinit( char* name, FILE *file, 
	      int n1, int n2, int padright, int bps, float scale,
	      int dpi, int pagewid, int pagehgt, int format, int turnflag,
	      int turnokflag, int rleflag );
void ima2ps( char *name, 
	    int n1, int n2, 
	    unsigned char *r,
	    unsigned char *g,
	    unsigned char *b,
	    int turnflag
	    ){

  FILE *file;
  
  int padright;
  int bps;
  float scale;
  int dpi;
  int pagewid;
  int pagehgt;
  int format;
  int turnokflag;
  int rleflag;

  unsigned char m[2];

  int i,i1,i2;

  format = COULEUR;

  scale = 1.0;
  /* HM 23 mars 2006 
     ce flag, s'il est a 1, fait tourner l'image si n1>n2: c'est nul!
     je le mets donc a 0 */
  turnokflag = 0;
  rleflag = 0;
  /* LaserWriter defaults. */
  dpi = 300;
  bps = 8;
  
  pagewid = n1;
  pagehgt = n2;
  
  /* Compute padding to round n1 * bps up to the nearest multiple of 8. */
  padright = ( ( ( n1 * bps + 7 ) / 8 ) * 8 - n1 * bps ) / bps;

  file = fopen(name,"w");
  
  putinit(
	  name, file, n1, n2, padright, bps, scale, dpi, pagewid, pagehgt, format,
	  turnflag, turnokflag, rleflag );

  i=0;

  for(i2=0; i2<n2; i2++) {
    
    for (i1=0; i1<n1; i1++) {
      if (i%30==0) fprintf(file,"\n");
      fprintf(file,"%2.2x",r[i1+i2*n1]);
      i++;
    }

    for(i1=0; i1<n1; i1++) {
      if(i%30==0) fprintf(file,"\n");
      fprintf(file,"%2.2x",g[i1+i2*n1]);
      i++;
    }

    for(i1=0; i1<n1; i1++) {
      if(i%30==0) fprintf(file,"\n");
      fprintf(file,"%2.2x",b[i1+i2*n1]);
      i++;
    }
    
  }


  if ( rleflag )
    putrest(file);
  else
    putrest(file);
  
  fclose(file);

}

/*********************************************************************/
void putinit( char* name, FILE *file, int n1, int n2, int padright, int bps, float scale,
	  int dpi, int pagewid, int pagehgt, int format, int turnflag,
	  int turnokflag, int rleflag ) {
    int in1, in2, devpix;
    float pixfac, sn1, sn2, llx, lly;
    
    /* Turn? */
    in1 = n1;
    in2 = n2;
    if ( turnflag || ( turnokflag && n1 > n2 ) )
      {
	turnflag = 1;
	n1 = in2;
	n2 = in1;
      }

    /* Figure out size. */
    devpix = dpi / 72.0 + 0.5;		/* device pixels per unit, approx. */
    pixfac = 72.0 / dpi * devpix;	/* 1, approx. */
    sn1 = scale * n1 * pixfac;
    sn2 = scale * n2 * pixfac;
    if ( sn1 > pagewid * MARGIN || sn2 > pagehgt * MARGIN )
	{
	if ( sn1 > pagewid * MARGIN )
	    {
	    scale *= pagewid / sn1 * MARGIN;
	    sn1 = scale * n1 * pixfac;
	    sn2 = scale * n2 * pixfac;
	    }
	if ( sn2 > pagehgt * MARGIN )
	    {
	    scale *= pagehgt / sn2 * MARGIN;
	    sn1 = scale * n1 * pixfac;
	    sn2 = scale * n2 * pixfac;
	    }
	}
    llx = ( pagewid - sn1 ) / 2;
    lly = ( pagehgt - sn2 ) / 2;

    fprintf(file, "%%!PS-Adobe-2.0 EPSF-2.0\n" );
    fprintf(file, "%%%%Creator: xhmv\n" );
    fprintf(file, "%%%%Title: %s.ps\n", name );
    fprintf(file, "%%%%Pages: 1\n" );
    fprintf(file,
	"%%%%BoundingBox: %d %d %d %d\n",
	(int) llx, (int) lly,
	(int) ( llx + sn1 + 0.5 ), (int) ( lly + sn2 + 0.5 ) );
    fprintf(file, "%%%%EndComments\n" );
    if ( rleflag )
	{
	fprintf(file, "/rlestr1 1 string def\n" );
	fprintf(file, "/readrlestring {\n" );				/* s -- nr */
	fprintf(file, "  /rlestr exch def\n" );			/* - */
	fprintf(file, "  currentfile rlestr1 readhexstring pop\n" );	/* s1 */
	fprintf(file, "  0 get\n" );					/* c */
	fprintf(file, "  dup 127 le {\n" );				/* c */
	fprintf(file, "    currentfile rlestr 0\n" );			/* c f s 0 */
	fprintf(file, "    4 3 roll\n" );				/* f s 0 c */
	fprintf(file, "    1 add  getinterval\n" );			/* f s */
	fprintf(file, "    readhexstring pop\n" );			/* s */
	fprintf(file, "    length\n" );				/* nr */
	fprintf(file, "  } {\n" );					/* c */
	fprintf(file, "    256 exch sub dup\n" );			/* n n */
	fprintf(file, "    currentfile rlestr1 readhexstring pop\n" );/* n n s1 */
	fprintf(file, "    0 get\n" );				/* n n c */
	fprintf(file, "    exch 0 exch 1 exch 1 sub {\n" );		/* n c 0 1 n-1*/
	fprintf(file, "      rlestr exch 2 index put\n" );
	fprintf(file, "    } for\n" );				/* n c */
	fprintf(file, "    pop\n" );					/* nr */
	fprintf(file, "  } ifelse\n" );				/* nr */
	fprintf(file, "} bind def\n" );
	fprintf(file, "/readstring {\n" );				/* s -- s */
        fprintf(file, "  dup length 0 {\n" );				/* s l 0 */
	fprintf(file, "    3 copy exch\n" );				/* s l n s n l*/
	fprintf(file, "    1 index sub\n" );				/* s l n s n r*/
	fprintf(file, "    getinterval\n" );				/* s l n ss */
	fprintf(file, "    readrlestring\n" );			/* s l n nr */
	fprintf(file, "    add\n" );					/* s l n */
        fprintf(file, "    2 copy le { exit } if\n" );		/* s l n */
        fprintf(file, "  } loop\n" );					/* s l l */
        fprintf(file, "  pop pop\n" );				/* s */
	fprintf(file, "} bind def\n" );
	}
    else
	{
	fprintf(file, "/readstring {\n" );				/* s -- s */
	fprintf(file, "  currentfile exch readhexstring pop\n" );
	fprintf(file, "} bind def\n" );
	}
    if ( format == COULEUR )
	{
	fprintf(file, "/rpicstr %d string def\n", ( in1 + padright ) * bps / 8 );
	fprintf(file, "/gpicstr %d string def\n", ( in1 + padright ) * bps / 8 );
	fprintf(file, "/bpicstr %d string def\n", ( in1 + padright ) * bps / 8 );
	}
    else
	fprintf(file, "/picstr %d string def\n", ( in1 + padright ) * bps / 8 );
    fprintf(file, "%%%%EndProlog\n" );
    fprintf(file, "%%%%Page: 1 1\n" );
    fprintf(file, "gsave\n" );
    fprintf(file, "%g %g translate\n", llx, lly );
    fprintf(file, "%g %g scale\n", sn1, sn2 );
    if ( turnflag )
	fprintf(file, "0.5 0.5 translate  90 rotate  -0.5 -0.5 translate\n" );
    fprintf(file, "%d %d %d\n", in1, in2, bps );
    fprintf(file, "[ %d 0 0 -%d 0 %d ]\n", in1, in2, in2 );
    if ( format == COULEUR )
	{
	fprintf(file, "{ rpicstr readstring }\n" );
	fprintf(file, "{ gpicstr readstring }\n" );
	fprintf(file, "{ bpicstr readstring }\n" );
	fprintf(file, "true 3\n" );
	fprintf(file, "colorimage\n" );
	}
    else
	{
	fprintf(file, "{ picstr readstring }\n" );
	fprintf(file, "image\n" );
	}

/*
    bitspersample = bps;
    itemsperline = items = 0; 
    if ( rleflag )
	{
	rleitem = 0;
	rlebitsperitem = 0;
	rlebitshift = 8 - bitspersample;
	repeat = 1;
	count = 0;
	}
    else
	{
	  item = 0;
	  bitsperitem = 0;
	  bitshift = 8 - bitspersample;
	}
*/
	}


/*--------------------------------------------*/
void putrest(FILE *file)
    {
    fprintf(file, "\n" );
    fprintf(file, "grestore\n" );
    fprintf(file, "showpage\n" );
    fprintf(file, "%%%%Trailer\n" );
    }
/*--------------------------------------------*/

