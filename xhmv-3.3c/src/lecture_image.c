/********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>

#include <i3d.h>
#include <vtk.h>

int rfc( char *nom, 
	 int *n1, int *n2, 
	 double *s1, double *s2,
	 double *p1, double *p2,
	 unsigned char **ima);




/********************************************************/
/*
  lecture de l'image en entree

  valeur retournee:
  0 :   lecture OJ
  -1:   le fichier n'existe pas
  -2:   pb sur le fichier (sur stat)
  -3:  	Probleme a la lecture de l'image au format fc
  -4:   Probleme a la lecture de l'image au format i3d
  -5:   Probleme a la lecture de l'image au format i3: format de donnees non implemente
*/
/********************************************************/
int lecture_image( char *fichier, H2Imagef *image){
  
  int i;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - -*/
  //  printf("on entre bien ds lecture image\n");
  sscanf(fichier,"%s",(*image).nom);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* test de l'existence du fichier */
  {
    struct stat filestat;

    i = stat(image->nom, &filestat);
    
    if (i<0) {
      if (errno==ENOENT) {
	return -1;
      }
      return -2;
    } 
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* fichier .fc */
  if ( strstr((*image).nom,".fc") != (char *)0) {
    unsigned char *buffer=(unsigned char *)0;

    i = rfc( (*image).nom,
	     &(*image).n[0],
	     &(*image).n[1],
	     &(*image).s[0],
	     &(*image).s[1],
	     &(*image).p[0][0],
	     &(*image).p[1][1],
	     &buffer);
    /*
    image->n[0]=image->n[1]=100;
    image->s[0]=image->s[1]=0.;
    image->p[0][0]=image->p[1][1]=1./image->n[0];
    buffer=malloc(100*100);
    for(i=0;i<10000;i++) buffer[i]=0;
    */

    if(i<0) {
      /*
	fprintf(stderr,
	      "Probleme a la lecture de l'image %s au format fc.\n",
	      image->nom);
      */

      return -3;
    }
    (*image).p[2][2]=1.;
    (*image).pxl = 
      (float *)realloc(
		       image->pxl 	    ,
		       (*image).n[0] * (*image).n[1] * sizeof(float)
		       );

    for(i=0;i<(*image).n[0]*(*image).n[1];i++) (*image).pxl[i]=buffer[i];

    free(buffer);

    image->n[2]=1;
    image->s[2]=0.;
    image->p[2][2]=1.;
    image->nd[0]=image->n[0];
    image->nd[1]=image->n[1];
    image->nd[2]=image->n[2];
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* fichier .i3d ou .vtk */
  else{
    int fmt;
    
    fmt=i3d_or_vtk_file_format( (*image).nom );

    /* i3d file format */
    if ( (fmt==1) || (fmt==2) ) {
      
      //      printf("avant le free avant ri3d\n");
      free(image->pxl);
      //      printf("avant ri3d\n");
      image->pxl=0;
      //      printf("apres le free avant ri3d\n");
      i=ri3d((*image).nom, (H2Image *)&(*image));
      //      printf("apres ri3d\n");

      
      if(i!=0) {
	/* 
	   fprintf(stderr,
	   "probleme a l'ouverture de %s\n",(*image).nom);
	   fprintf(stderr,
	   "il semble que ce ne soit pas une image au format i3d\n");
	*/
	return -4;
      }
      /*
      if ((*image).type != HIMAFLOAT) {
	fprintf(stderr,"xhmv: actuellement, xhmv ne sait lire que des images\n");
	fprintf(stderr,"      de valeurs flottantes.\n");
	fprintf(stderr,"      D�sol�!.\n");
	return -5;
      }
      */
    }
    /* legacy VTK file format */
    else if (fmt==3) {
      free(image->pxl);
      image->pxl=0;
      i = rvtk( (H2Image *)image );
      if (i!=0) {
	fprintf(stderr,"xhmv: error while reading VTK file %s \n",image->nom);
	return -4;
      }
    }
    /* unknown file format */
    else{
      fprintf(stderr,"xhmv: %s has an unknown file format\n",image->nom);
      return -4;
    }
      
      
#ifdef OLD
    /* pour le moment on ne s'occupe que des images 2d */
    if((*image).n[2]>1) {
      /*
	fprintf(stderr,"xhmv: %s est une image reellement 3D\n",(*image).nom);
	fprintf(stderr,"      c'est un cas non traite par xhmv.\n");
      */
      int f;
      fprintf(stderr,"      on lit le 1er plan de cette image.\n");
      /* ri3dz attend que le fichier ait d�j� �t� ouvert par un open */
      f = open((*image).nom,O_RDONLY);
      i=ri3dz(f, 0, (H2Image *)&(*image));
      close(f);
      if (i!=0) {
	fprintf(stderr,
		"probleme a l'ouverture de %s\n",(*image).nom);
	fprintf(stderr,
		"il semble que ce ne soit pas une image au format i3d\n");
	
	return -4;
      }
    }
    /* on passe outre le cas ou l'image a ete creee avec un pas d'echantillonnage
       de 0 ds la 3e direction */
    if( image->n[2] == 1 ) {
      image->p[2][0]=0.;
      image->p[2][1]=0.;
      image->p[2][2]=1.;
    }    
    
#endif
  }
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return 0;
  
}
/********************************************************/
