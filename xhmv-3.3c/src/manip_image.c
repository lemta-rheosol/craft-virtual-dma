/**********************************************************************/
/* qq fonctions de manipulations d'images 
 *
 * apply_zoom_cuts  :  applique un facteur de zoom et des cuts
 *                     a une image
 *
 * subimage         :  copie une image d'unsigned char ds une autre
 *                     dont les start est diff�rent (on suppose qu'elles
 *                     ont ttes les 2 les mm pas d'echantillonnage,
 *                     et que les grilles d'echantillonnage de l'une et 
 *                     l'autre sont superposables (a un decalage pres).
 *                    
 */
/**********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <xhmv.h>
#include <i3d.h>


/* on pourrait mettre un nb de dimension de 3 (c'est ce qui est fait 
   dans la version 2.2c de xhmv, mais franchement on n'y gagne RIEN
   si ce n'est qu'on grossit inutilement l'espace necessaire au stockage)
*/
#define NBDIM 2
/**********************************************************************/
/*
 * apply_zoom_cuts  :  applique un facteur de zoom et des cuts
 *                     a une image
 *
 * en entree:
 *    une image en flottant de type H2Imagef
 *    le facteur de zoom et les cuts qu'on souhaite appliquer
 * en sortie: 
 *    une image en unsigned char de type H2Imageuc
 *    dont les pixels ont une valeur entre 0 et 255 pour des valeurs
 *    correspondantes entre cuts[0] et cuts[1] ds l'image d'origine.
 */
/*===========================================================================*/
int apply_zoom_cuts( H2Imageuc *imauc, H2Imagef *imaf, 
		      int *zoom, float cuts[2]){


  int j;
  int i, ii;
  double x[3], ix[3];
  int i1, i2, ii1, ii2;
  int status=0;
  
  float coef1, coef2;


  H2Imageuc *imabuf;

  imabuf = malloc(sizeof(H2Imageuc));
  *imabuf = *imauc;

  copy_header_h2image( (H2Image *)imabuf, (H2Image *)imaf);
  imabuf->type = HIMAUCHAR;

  if (*zoom==0) *zoom=1;

  /* zoom */
  if(*zoom>0) {
    for(i=0;i<NBDIM;i++) {
      imabuf->n[i] *= (*zoom);
      imabuf->nd[i] = imabuf->n[i];
    }
    for(i=0;i<NBDIM;i++) {
      for(j=0;j<NBDIM;j++) {
	imabuf->p[i][j] /= (double)(*zoom);
      }
    }
#if (NBDIM == 3)
    ix[0] = ix[1] = ix[2] = - ((double)(*zoom)-1.)/2.;
#elif (NBDIM == 2)
    ix[0] = ix[1] = - ((double)(*zoom)-1.)/2.;
#endif

    i2x( (H2Image *)imabuf, x , ix);
    for(i=0; i<NBDIM; i++) {
      imabuf->s[i]= x[i];
    }  
  }
  /* dezoom */
  else {
    for(i=0;i<NBDIM;i++) {
      imabuf->n[i] /= -(*zoom);
      imabuf->nd[i] = imabuf->n[i];
    }
    for(i=0;i<NBDIM;i++) {
      for(j=0;j<NBDIM;j++) {
	imabuf->p[i][j] *= -(double)(*zoom);
      }
    }
  }
    
  
  status = reallocate_h2image( (H2Image *)imabuf);
  if (status!=0) {
    return status;
  }

  coef1 = 255.f/(cuts[1]-cuts[0]);
  coef2 = -cuts[0]*coef1;
				     
  int i3, ii3;
  
  if (*zoom>0) {
    for(i3=0,i=0; i3<imabuf->n[2]; i3++) {
      ii3 = i3*imaf->n[0]*imaf->n[1];
    for(i2=0; i2<imabuf->n[1]; i2++) {
      ii2 = i2/(*zoom);
      ii2 *= imaf->n[0];
      for(i1=0; i1<imabuf->n[0]; i1++){
	ii1 = i1/(*zoom);
	ii = ii1 + ii2 + ii3;
	
	if (imaf->pxl[ii]>=cuts[1]){
	  imabuf->pxl[i] = 255;
	}
	else if (imaf->pxl[ii]<=cuts[0]){
	  imabuf->pxl[i] = 0;
	}
	else {
	/*
	  imabuf->pxl[i] = (unsigned char)(   
	  (imaf->pxl[ii]-cuts[0]) /
	  (cuts[1]-cuts[0]) * 255.f );
	*/
	  imabuf->pxl[i] = (unsigned char)(imaf->pxl[ii]*coef1 + coef2);
	}
#ifdef DEBUG
	if (i==0) {
	  fprintf(stderr,"hop: %d %d %d %f\n",i1,i2,imabuf->pxl[i],imaf->pxl[ii]);
	}
#endif
	i++;

      }
    }
    }
  }
  else {
    for(i2=0,i=0; i2<imabuf->n[1]; i2++) {
      ii2 = i2*(-(*zoom));
      ii2 *= imaf->n[0];
      for(i1=0; i1<imabuf->n[0]; i1++){
	ii1 = i1*(-(*zoom));
	ii = ii1 + ii2;


	if (imaf->pxl[ii]>cuts[1]){
	  imabuf->pxl[i] = 255;
	}
	else if (imaf->pxl[ii]<cuts[0]){
	  imabuf->pxl[i] = 0;
	}
	/*
	  imabuf->pxl[i] = (unsigned char)(   
	  (imaf->pxl[ii]-cuts[0]) /
	  (cuts[1]-cuts[0]) * 255.f );
	*/
	else {
	  imabuf->pxl[i] = (unsigned char)(imaf->pxl[ii]*coef1 + coef2);
	}

	i++;

      }
    }
  

  }

  /*-------------------------------------*/
  /* inversion haut bas */
  /*
  ix[0]=0;
  ix[1]=imabuf->n[1]-1;
  ix[2]=0;
  i2x( (H2Image *)imabuf, x, ix);
  for(i=0; i<3; i++) {  
    imabuf->s[i]=x[i];
  }

  for(j=0; j<3; j++) {
    imabuf->p[1][j] = -imabuf->p[1][j];
  }

  
  {
    unsigned char *ligne;
    ligne = malloc(imabuf->n[0]);
    for(i2=0; i2<imabuf->n[1]/2;i2++) {
      memcpy(ligne,
	     imabuf->pxl+i2*imabuf->n[0],
	     imabuf->n[0]);
      memcpy(imabuf->pxl+i2*imabuf->n[0], 
	     imabuf->pxl+(imabuf->n[1]-1-i2)*imabuf->n[0],
	     imabuf->n[0]);
      memcpy(imabuf->pxl+(imabuf->n[1]-1-i2)*imabuf->n[0],
	     ligne,
	     imabuf->n[0]);
    }
    free(ligne);

  }
  */
  /*-------------------------------------*/


  /* on actualise imauc */
  *imauc=*imabuf;

  free(imabuf);
  /*
  printf("imauc\n");
  printf("n=%d %d %d\n",imauc->n[0],imauc->n[1],imauc->n[2]);
  printf("nd=%d %d %d\n",imauc->nd[0],imauc->nd[1],imauc->nd[2]);
  printf("s=%f %f %f\n",imauc->s[0],imauc->s[1],imauc->s[2]);
  printf("p[0]=%f %f %f\n",imauc->p[0][0],imauc->p[0][1],imauc->p[0][2]);
  printf("p[1]=%f %f %f\n",imauc->p[1][0],imauc->p[1][1],imauc->p[1][2]);
  printf("p[2]=%f %f %f\n",imauc->p[2][0],imauc->p[2][1],imauc->p[2][2]);
  */
  status = 0;
  return status;
}



/**********************************************************************/
/* on copie l'image imauc d'unsigned char ds une autre image imauc2
   d'unsigned char. 
   On suppose qu'elles ont les memes pas d'echantillonnage 
   et qu'un pixel d'une image tombe pile sur la grille d'echantillonnage
   de l'autre.
*/
void subimage( H2Imageuc *imauc2, H2Imageuc *imauc, int periodicite){

  int status;
  int i1,i2,ii1,ii2,i,ii;
  int j2;
  double x[3];
  double ix[3];

  int dec[3];
  
  int ibas[2];
  int iht[2];

  x[0] = imauc->s[0];
  x[1] = imauc->s[1];
  x[2] = imauc->s[2];
  x2i( (H2Image *)imauc2, x, ix);
  dec[0] = ix[0];
  dec[1] = ix[1];
  dec[2] = ix[2];

  /*
    printf("dev2=%d\n",dec[2]);
    printf("imauc n=%d %d %d\n",imauc->n[0],imauc->n[1],imauc->n[2]);
  */

  /* affichage periodique */
  if(periodicite){
    for ( i2=0;i2<imauc2->n[1]; i2++){
      ii2 = (i2-dec[1])%imauc->n[1];
      if(ii2<0) ii2 += imauc->n[1];
      
      for ( i1=0;i1<imauc2->n[0]; i1++){
	ii1 = (i1-dec[0])%imauc->n[0];
	if(ii1<0) ii1 += imauc->n[0];
	
	imauc2->pxl[i1+i2*imauc2->n[0]] = imauc->pxl[ ii1 + ii2*imauc->n[0] + dec[2]*imauc->n[0]*imauc->n[1] ];
	
      }
    }
  }

  /* affichage non-periodique */
  else {
    /* dec contient donc les coordonnees en pixels du premier pixel
       ds imauc ds le repere de imauc2 */
    
    
    iht[0] = ( imauc2->n[0] < imauc->n[0]+dec[0] ? 
	       imauc2->n[0] :
	       imauc->n[0]+dec[0]);
    iht[1] = ( imauc2->n[1] < imauc->n[1]+dec[1] ? 
	       imauc2->n[1] :
	       imauc->n[1]+dec[1]);
    
    ibas[0] = ( dec[0]>0 ? dec[0] : 0);
    ibas[1] = ( dec[1]>0 ? dec[1] : 0);
    
    
#ifdef DEBUG
    fprintf(stderr,"subimage: point d'arret 1\n");
    fprintf(stderr,"subimage imauc2->pxl = %d\n",imauc2->pxl);
    fprintf(stderr,"subimage imauc2->n = %d %d\n",
	    imauc2->n[0],
	    imauc2->n[1]);
#endif
    
    for(i=0; i< imauc2->n[0]*imauc2->n[1]; i++) {
      imauc2->pxl[i] = 0;
    }
#ifdef DEBUG
    fprintf(stderr,"subimage: point d'arret 2\n");
#endif
    
    for( i2=ibas[1]; i2<iht[1]; i2++) {
      ii2 = (i2 - dec[1])*(imauc->n[0]);
      j2 = i2*imauc2->n[0];
      for( i1=ibas[0]; i1<iht[0]; i1++) {
	ii1 = i1 - dec[0];
	i = i1 + j2;
	ii = ii1 + ii2;
#ifdef DEBUG
	if( 
	   (ii1<0) || (ii1>=imauc->n[0]) ||
	   (ii2<0) || (ii2>=imauc->n[1]) ) {
	  printf("bizarre: i1=%d i2=%d\n",i1,i2);
	  printf("bizarre: ii1=%d ii2=%d\n",ii1,ii2);
	}
#endif
	imauc2->pxl[i] = imauc->pxl[ii];
      }
    }

  }

#ifdef TOTO
  /* maintenant on recopie l'image imauc ds imauc2 */
  status = x2i( (H2Image *)imauc, imauc2->s, ix);
  for(i2=0,j=0; i2<imauc2->n[1]; i2++) {
    ii2 = i2 + ix[1];
    if ( (ii2<0) || (ii2>=imauc->n[1]) ){
      for(i1=0; i1<imauc2->n[0]; i1++, j++) {
	imauc2->pxl[j] = 0;
      }
    }
    else {
      ii2 *= imauc->n[0];
      for(i1=0; i1<imauc2->n[0]; i1++,j++) {
	ii1 = i1 + ix[0];
	if ( (ii1<0) || (ii1>=imauc->n[0]) ){
	  imauc2->pxl[j] = 0;
	}
	else{
	  i = ii1 + ii2;
	  /*	  j = i1 + i2*imauc2->n[0]; */
	  
	  imauc2->pxl[j] = imauc->pxl[i]; 

	}
      }
    }
  }
#endif

}



