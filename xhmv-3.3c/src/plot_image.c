/*********************************************************************/
/* xhmv version 3.2
 * HM 14 novembre 2005
 *
 *
 * Dans ce fichier on trouvera les fonctions suivantes:
 *  close_plot_image_
 *  replot_image_
 *  modify_plot_image_
 *  plot_image_
 *  get_plot_features
 *  put_plot_features
 *  evt_expose
 *  evt_zoom
 *  evt_centrage
 *  attention
 *  plot_file_name
 *  fais_moi_signe  : ecrit ds le fichier d'affichage le numero de
 *                    fenetre XWindow a qui le process que gere l'affichage
 *                    doit "faire un signe" a ch fois qu'il modie l'affichage
 * 
 */
/*********************************************************************/
#define _DEFAULT_SOURCE

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <X11/cursorfont.h>

#define __USE_SVID
#define __USE_XOPEN

#ifndef P_tmpdir
#define P_tmpdir "/tmp/"
#endif

/* et les includes "maison" */
#include <xhmv.h>

#define MAINLUT
#include <lut.h>
#include <i3d.h>

#define MAXCOMPTE 1000000

/* niveau de verbosite */
int verbose=0;
/* periodicite de l'affichage */
int period=0;

void epsfilename( char *efn, char *fn);
int fexist(char *file);

/*****************************************************************************************/
void XRaiseWindow2(Display * display,
		 Window win){
  
  XEvent xev;
  Window root;
  
  xev.type = ClientMessage;
  xev.xclient.display = display;
  xev.xclient.window = win;
  xev.xclient.message_type = XInternAtom(
					 display,
					 "_NET_ACTIVE_WINDOW",0);
  xev.xclient.format = 32;
  xev.xclient.data.l[0] = 2L;
  xev.xclient.data.l[1] = CurrentTime;
  
  root = XDefaultRootWindow(display);
  
  XSendEvent( display,root,0,
	      SubstructureNotifyMask |
	      SubstructureRedirectMask,
	      &xev);
}
/****************************************************************************/
int get_plot_features(Affichage *a,int *id_plot){
  char fichier_affichage[200];
  int compte=0;
  int retourne;
  int status;

  /* le nom du fichier qui contiendra les consignes */
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status;

  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, a ); 
    
  } while( 
	  ((status!=0) || ((status==0) && (a->occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    //    exit(1);
  }

  if( (status !=0) || (compte==MAXCOMPTE) )  {
    retourne = -1;
  }
  else {
    retourne = 0;
  }

  return retourne;

}
/****************************************************************************/
int put_plot_features(Affichage *a,int *id_plot){
  char fichier_affichage[200];
  int compte=0;
  int retourne;
  int status;
  Affichage aa;

  /* le nom du fichier qui contiendra les consignes */
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status;

  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, &aa ); 
    
  } while( 
	  ((status!=0) || ((status==0) && (aa.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    //    exit(1);
  }

  if( (status !=0) || (compte==MAXCOMPTE) )  {
    retourne = -1;
  }
  else {
    status=sauve_affiche(fichier_affichage,*a);
    retourne = 0;
  }

  return retourne;

}
/****************************************************************************/
int close_plot_image_(int *id_plot){
  int status;
  Affichage affiche;

  char fichier_affichage[200];
  int compte=0;
  int retourne;
  
  /* le nom du fichier qui contiendra les consignes */
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status;

  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, &affiche ); 
    
  } while( 
	  ((status!=0) || ((status==0) && (affiche.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }


  affiche.occupe=1;
  status=sauve_affiche(fichier_affichage,affiche);


  if (status!=0) {
    fprintf(stderr,"close_plot_image: erreur a la lecture du fichier affichage\n");
  }

  if( (long int)affiche.fenetre != 0L) {
      
    affiche.message = XHMV_MSG_CLOSE;
    status=sauve_affiche(fichier_affichage,affiche);

    status = attention(affiche.display, affiche.fenetre);
    retourne = 0;

  }
  else {
    retourne = -1;
  }

  affiche.occupe=0;
  status=sauve_affiche(fichier_affichage,affiche);
  return retourne;

}



/****************************************************************************/
int replot_image_(int *id_plot){
  int status;
  Affichage affiche;

  char fichier_affichage[200];
  int compte=0;

  int retourne;

  /* le nom du fichier qui contiendra les consignes */
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status;
  /*  sprintf(fichier_affichage,"tmp_xhmv_%6.6d",*id_plot); */
  
  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, &affiche ); 
  } while( 
	  ((status!=0) || ((status==0) && (affiche.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }

  /*
  } while( (affiche.occupe) && (compte<MAXCOMPTE)) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }
  */
  affiche.occupe=1;
  status=sauve_affiche(fichier_affichage,affiche);


  if (status!=0) {
    fprintf(stderr,"replot_image: erreur a la lecture du fichier affichage\n");
  }

  if( (long int)affiche.fenetre != 0L) {
      
    affiche.message = XHMV_MSG_TOUT;
    status=sauve_affiche(fichier_affichage,affiche);

    status = attention(affiche.display, affiche.fenetre);
    //    status = attention(affiche.fenetre);

    retourne= 0;

  }
  else {
    retourne = -1;
  }

  affiche.occupe=0;
  status=sauve_affiche(fichier_affichage,affiche);
  return retourne;

}



/*********************************************************************/
int modify_plot_image_(
		      int *id_plot,
		      char *fichier_image,
		      int *zoom,
		      int *n,
		      double *s,
		      float *cuts,
		      int *lut
		      ) {

  int status;
  Affichage affiche;

  char fichier_affichage[200];
  int compte;
  int retourne;
  
  /* le nom du fichier qui contient les consignes d'affichage*/
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status;
  
  /* on lit l'affichage mais pour cela on attend que le fichier soit
     "libre"
  */
  compte = 0;
  do {
    status = lit_affichage( fichier_affichage, &affiche ); 
    compte++;
    if (status!=0) {
      fprintf(stderr,"modify_plot_image: erreur a la lecture du fichier affichage\n");
    }
  } while( 
	  ((status!=0) || ((status==0) && (affiche.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }


    /*  } while( (affiche.occupe) && (compte<MAXCOMPTE)) ;
  if(compte==MAXCOMPTE) fprintf(stderr,"HHHHHHHHHHHHHHHHH\n");
    */
  affiche.occupe=1;
  status=sauve_affiche(fichier_affichage,affiche);


  if( (long int)affiche.fenetre != 0L) {
      
    /* lecture des arguments de la fonction  plot_image_          */
    
    strcpy(affiche.fichier_image, fichier_image);
    
    affiche.zoom = *zoom;
    
    affiche.n[0] = n[0];
    affiche.n[1] = n[1];
    
    affiche.s[0] = s[0];
    affiche.s[1] = s[1];
    
    affiche.cuts[0] = cuts[0];
    affiche.cuts[1] = cuts[1];
    
    affiche.lut = *lut;

    affiche.message = XHMV_MSG_TOUT;

    if (verbose) {
      print_affichage(stdout,1,affiche);
    }
    
    status = attention(affiche.display, affiche.fenetre);
    //    status = attention(affiche.fenetre);

    retourne = 0;

  }
  else {
    retourne = -1;
  }


  affiche.occupe=0;
  status=sauve_affiche(fichier_affichage,affiche);

  return retourne;

}

/*********************************************************************/
/* LA routine d'affichage d'image.
   Les donnees de l'affichage (nom de l'image a afficher, cuts, 
   zoom,  etc.) sont ds le fichier ficher_affichage.

   Plot_image retourne l'id du process assurant le plot (il est donc UNIQUE).

   Cet identificateur est un entier, il servira lors de tout
   appel se referant a ce plot (par appel de modify_plot_image, 
   replot_image, ou close_plot_image).

   Un fichier temporaire est cree:
   - il contiendra les informations relatives au plot (nom de l'image
     a afficher, facteur de zoom, cuts, etc.
   - son nom a:
      * pour prefixe:  tmp_xhmv_
      * pour suffixe:  le no du process du plot 
     et est ds le repertoire dont est donne par:
      * P_tmpdir
     


*/
int plot_image_(
		char *fichier_image,
		int *zoom,
		int *n,
		double *s,
		float *cuts,
		int *lut
     ) {


  /*=========================================================================*/
  /*------------------------------------------------------------*/
  /* DECLARATIONS: */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  Display *display;

  XVisualInfo vinfo_return;
  
  Affichage affiche={
    "",
    0,
    0.f, 0.f,
    0., 0.,
    0,0,
    0,
    (Display *)0,
    (Window)0,
    (Display *)0,
    (Window)0,
    0,
    0,
    "",
    "",
    0,
    0,
    1
  };

  Affichage paffiche={
    "",
    0,
    0.f, 0.f,
    0., 0.,
    0,0,
    0,
    (Display *)0,
    (Window)0,
    (Display *)0,
    (Window)0,
    0,
    0,
    "",
    "",
    0,
    0,
    1
  };


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* declaration des grandeurs lies aux images utilisees pendant 
     le programme */

  /*- - - - - - - - - - -*/
  /* l'image en entree */
  /* on suppose que:
     - elle est de dimension 2 (epaisseur 1 ds la 3e direction)
     - elle a des pixels CARRES
  */
  H2Imagef imaf;

  /*- - - - - - - - - - -*/
  /* image (en pixels unsigned char) obtenue par conversion
   de l'image en entree (imaf) en appliquant les cuts*/
  /* elle a un nb de pixels egal a celui de imaf multiplie 
     par le facteur de zoom
     Ces pixels sont carres mais zoom*zoom fois plus grand que
     ceux de imaf
  */
  H2Imageuc imauc;

  /*- - - - - - - - - - -*/
  /* image (en pixels unsigned char) que l'on veut afficher */
  /* c'est la sous-image de imauc qui sera affiche. Elle a donc
     la meme grille d'echantillonnage (meme pas d'echantillonnage
     et un pixel en bas a gauche qui tombe pile sur un pixel de imauc).
  */
  H2Imageuc imauc2;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* declaration des grandeurs lies aux evenements X11 */
  KeySym keysym;
  KeySym touche;
  XEvent evenem;
  XComposeStatus compose;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  int i,j;
  int status;
  FILE *f;

  pid_t pid;

  /* le nom des fichiers de consigne */
  char fichier_affichage[200];

  int compte=0;
  /*=========================================================================*/
  construct_h2image_f(  &imaf  );
  construct_h2image_uc( &imauc );
  construct_h2image_uc( &imauc2);



  if (verbose) {
    fprintf(stderr,"le nom de l'image est %s\n",fichier_image);
  }
  /*------------------------------------------------------------*/
  pid = fork();

  if(pid != 0) {
    return (int)pid;
  }
  
  /* ds le cas ou on est ds le process fils, on a besoin de son 
     pid pour plus tard */
  pid = getpid();
  /*------------------------------------------------------------*/
  /* lecture des arguments de la fonction  plot_image_          */

  strcpy(affiche.fichier_image, fichier_image);

  affiche.zoom = *zoom;

  affiche.n[0] = n[0];
  affiche.n[1] = n[1];

  affiche.s[0] = s[0];
  affiche.s[1] = s[1];

  affiche.cuts[0] = cuts[0];
  affiche.cuts[1] = cuts[1];

  affiche.lut = *lut;

  /* le nom du fichier postscript de copie d'ecran */
  epsfilename( affiche.fichier_postscript, affiche.fichier_image);
  copie_affiche(&paffiche,&affiche);

  /* le nom du fichier qui contiendra les consignes */
  status = plot_file_name(fichier_affichage,pid);
  if (status!=0) {
      fprintf(stderr,"probleme a la creation du nom du fichier d'affichage\n");
      return status;
  }
  /*  sprintf(fichier_affichage,"tmp_xhmv_%6.6d",pid); */
  /*------------------------------------------------------------*/
  if (verbose) {
    status = print_affichage(stdout,1,affiche);
  }
  /*------------------------------------------------------------*/
  /* lecture de l'image */
  /*  printf("%s %d\n",imaf.nom,imaf.nom); */
  status = lecture_image( affiche.fichier_image, &imaf);
  if (status!=0) {
    fprintf(stderr,"erreur de lecture du fichier image\n");
  }
  /*------------------------------------------------------------*/
  /* on regarde si les cuts vont bien, s'ils n'ont pas ete initialises
     on y met le min et le max de l'image */
  if (affiche.cuts[1]<=affiche.cuts[0]) {
    affiche.cuts[0]=affiche.cuts[1]=imaf.pxl[0];
    for(i=0; i<imaf.n[0]*imaf.n[1];i++) {
      affiche.cuts[0] = ( imaf.pxl[i]<affiche.cuts[0]? imaf.pxl[i] : affiche.cuts[0]);
      affiche.cuts[1] = ( imaf.pxl[i]>affiche.cuts[1]? imaf.pxl[i] : affiche.cuts[1]);
    }
  }
  /*------------------------------------------------------------*/
  /* on applique les cuts pour remplir l'image entiere et zoomee
     en unsigned char */

  status=apply_zoom_cuts( &imauc, &imaf, &affiche.zoom, affiche.cuts);
  if (status!=0) {
    fprintf(stderr,"on n'arrive pas a zoomer avec le facteur %d.\n",affiche.zoom);
    fprintf(stderr,"sans doute un pb de memoire.\n");
  }
  /*------------------------------------------------------------*/
  /* on regarde si la taille de la fenetre va bien, s'ils n'ont pas 
     ete initialises on y met la taille de l'image majoree par 1024x1024
  */
  if (affiche.n[0]<=0) {
    affiche.n[0] = (imauc.n[0]>1024? 1024:imauc.n[0]);
  }
  if (affiche.n[1]<=0) {
    affiche.n[1] = (imauc.n[1]>1024? 1024:imauc.n[1]);
  }
  
  /*------------------------------------------------------------*/
  /* on "recopie" l'image zoomee ds l'image collant a la fenetre */
  /* ( autrement dit: imauc -> imauc2 )*/
  imauc2.n[0] = affiche.n[0];
  imauc2.n[1] = affiche.n[1];
  imauc2.n[2] = 1;

  imauc2.nd[0] = imauc2.n[0] ;
  imauc2.nd[1] = imauc2.n[1] ; 
  imauc2.nd[2] = imauc2.n[2] ;



  imauc2.s[0] = affiche.s[0];
  imauc2.s[1] = affiche.s[1];
  imauc2.s[2] = 0.;
  
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      imauc2.p[i][j] = imauc.p[i][j];
    }
  }

  reallocate_h2image( (H2Image *)&imauc2);
  subimage( &imauc2, &imauc , period);

  
  /*------------------------------------------------------------*/
  /* creation du fichier d'affichage */

  affiche.fenetre_interloc = (Window)0; /* par defaut il n'y a pas d'interlocuteur */

  strcpy(affiche.texte,"    "); /* pas de message pour l'instant */

  /*------------------------------------------------------------*/
  /* ouverture du Display */
  display = XOpenDisplay("");

#ifdef DEBUG
  printf("apres le opendisplay display vaut %ld %d (%d)\n",display, display,sizeof(display));
#endif
  affiche.display=display;
  affiche.occupe = 1;

  /*------------------------------------------------------------*/
  /* choix d'un mode visuel adapte */
  status = choix_visual( display , &vinfo_return ) ;

  if(verbose) {
    fprintf(stderr,"mode visuel = %ld\n",(long int)vinfo_return.visual);
    fprintf(stderr,"prof = %d\n",vinfo_return.depth);
  }

  if ( status == 0 ) {
    fprintf(stderr,"pb pour le choix d'un mode visuel adequat.\n");
    exit(1);
  }

  /*------------------------------------------------------------*/
  /* creation d'une fenetre */
  if ((affiche.n[0]<=0)||(affiche.n[1]<=0)) {
    affiche.n[0] = ( imauc.n[0] > 768 ? 768 : imauc.n[0] );
    affiche.n[1] = ( imauc.n[1] > 768 ? 768 : imauc.n[1] );
  }
  affiche.fenetre = cree_fenetre( display, 
			     vinfo_return.visual, 
			     vinfo_return.depth,
			     affiche.n[0],
			     affiche.n[1]
			  );
  

  remplit_fenetre( display, affiche.fenetre, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);




  /*------------------------------------------------------------*/
  /* et on ecrit ds le fichier ad hoc le contenu de l'affichage */
  affiche.occupe = 0;
  status = sauve_affiche( fichier_affichage, affiche ); 

  /*=========================================================================*/
  /* boucle infinie sur les evenements X11 */
  /*=========================================================================*/
#ifdef DEBUG
  fprintf(stderr,"plot_image_\n");
  fprintf(stderr,"plot_image_\n");
  fprintf(stderr,"          display=%d fenetre=%d\n",display,affiche.fenetre);
#endif
  
  {
    char cbuf[20];
    int cbufsize=20;

    int shift_l=0, shift_r=0;
    int alt_l=0, alt_r=0;
    int bouton_appuye=0;
    

    int i1,i2,i3;
    int ic[3];

    double x[3];
    double ix[3];


  while(1) {

    /*----------------------------------------------------------*/
    /* Traitement d'un nouvel evenement */
    XNextEvent(display,&evenem); 
    /* on ne prend en compte qu'un seul evenement Expose
       quand il y en a plusieurs d'affile dans la queue des
       evenements
    */
    if (evenem.type==Expose){
      XEvent evenem2;
      /* s'il y a encore des evts ds la queue ... */
      if ( XEventsQueued(display,QueuedAlready)>0) {
	/* ... on lit le suivant (sans le retirer de la queue */
	XPeekEvent(display,&evenem2);
	/* si c'est un Expose on passe a l'evt suivant */
	if(evenem2.type==Expose) continue;
      }
    }
    XFlush(display);     

    //    printf("evenem.type=%d\n",(evenem.type) );
  
    switch(evenem.type) {
      /*------------------------------------------------------------*/
      /* evenement Expose: raffraichissement de la fenetre */ 
    case Expose:
      evt_expose( display, affiche.fenetre, &imauc, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);

      /* un peu degueu: il peut y avoir eu un resize de la fenetre a la main,
	 ds ce cas evt_expose a mis a jour imauc.n, et il faut maintenant 
	 le repercuter sur la taille ds affiche */
      {
	/* on attend que le fichier d'affichage soit libre */
	compte = 0;
	do {
	  compte ++;
	  status = lit_affichage( fichier_affichage, &affiche ); 
	} while( 
		((status!=0) || ((status==0) && (affiche.occupe))) 
		&& 
		(compte<MAXCOMPTE)
		) ;
	if(compte==MAXCOMPTE) {
	  fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
	  exit(1);
	}


	  /*
	} while( (affiche.occupe) && (compte<MAXCOMPTE)) ;
	if(compte==MAXCOMPTE) fprintf(stderr,"HHHHHHHHHHHHHHHHH\n");
	  */	  

	if (
	    (affiche.n[0] != imauc2.n[0]) ||
	    (affiche.n[1] != imauc2.n[1])
	    ) {
	  affiche.n[0] = imauc2.n[0];
	  affiche.n[1] = imauc2.n[1];
	  
	  status = sauve_affiche( fichier_affichage, affiche ); 
	}

      }
      
      break;
      
      /*------------------------------------------------------------*/
      /* plot_image recoit un message de l'exterieur lui disant
	 de quitter ou de (re-)afficher les donnees apres les
	 avoir lues ds le fichier de message */
      /* NB: c'est assez "sale" comme programmation *
       */
    case ClientMessage:
      {
	int reapply=0;
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* lecture des donnees a afficher */
	/* on attend que le fichier d'affichage soit libre */
	status = get_plot_features( &affiche, &pid);
	if (status<0) {
	  sprintf(affiche.texte,"plot_image: pb de lecture de %s\n",fichier_affichage);
	  affiche.message = XHMV_MSG_MESG;
	  status = sauve_affiche( fichier_affichage, affiche ); 
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //	  attention(affiche.fenetre_interloc);
	  break;
	}
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* le message dit qu'il faut quitter */
	if(affiche.message==XHMV_MSG_CLOSE) {
	  quitte(fichier_affichage,display);
	  break;
	}
	
	if (affiche.message == XHMV_MSG_PRINT) {

	  /*
	  copie_postscript(
			   imauc2, imauc,
			   hmlut[affiche.lut],
			   affiche.fichier_postscript, 
			   affiche.orientation_postscript);
	  */
	  /* one has to raise window to avoid that any part of the displayed image be hidden by
	     another wiondow */
	  XRaiseWindow2(display, affiche.fenetre);  /* XRaiseWindow does not work
						       for complicated reasons ...
						       http://notloeser.blogspot.com/2010/03/xraisewindow.html
						       gives a solution writting a new XRaiseWindow
						    */
	  evt_expose( display, affiche.fenetre, &imauc, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);
	  /* on has to expose the window (otherwise, it raises the way and let empty
	     the previously hidden part of the window) */


	  status = snapshot(display, affiche.fenetre,affiche.fichier_postscript);

	  if(status==0){
	    sprintf(
		    affiche.texte,
		    "probleme lors de l'impression dans le fichier %s",
		    affiche.fichier_postscript);
	  }
	  if(status==1) {
	    sprintf(
		    affiche.texte,
		    "impression dans le fichier %s terminee",
		    affiche.fichier_postscript);
	  }

	  if(status==2) {
	    sprintf(
		    affiche.texte,
		    "Impression dans le fichier %s terminee.\rATTENTION: seule la partie visible a ete imprimee.",
		    affiche.fichier_postscript);
	  }

	  affiche.message = XHMV_MSG_MESG;
	  status = sauve_affiche( fichier_affichage, affiche ); 
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //	  attention(affiche.fenetre_interloc);
	  


	  break;
	}
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	  /* le nom du fichier a change */

	/*
	if ( ( affiche.message==XHMV_MSG_FILE) || (affiche.message==XHMV_MSG_TOUT) ){
	
	  if ( strcmp(paffiche.fichier_image,affiche.fichier_image) != 0) {
	*/  
	if ( 
	    ( ( affiche.message==XHMV_MSG_FILE) || (affiche.message==XHMV_MSG_TOUT) ) 
	    && 
	    ( strcmp(paffiche.fichier_image,affiche.fichier_image) != 0) 
	     ){

	  //	  printf("HHH chgt de fichier!\n");
	  /* lecture de l'image */
	  status = lecture_image( affiche.fichier_image, &imaf);
	  if(status==-1) {
	    sprintf(
		    affiche.texte,
		    "ERREUR: le fichier %s n'existe pas              \n",
		    affiche.fichier_image
		    );
	    strcpy(affiche.fichier_image , paffiche.fichier_image);
	  }
	  else if(status==-2) {
	    sprintf(
		    affiche.texte,
		    "ERREUR: probleme a l'ouverture du fichier %s \n",
		    affiche.fichier_image
		    );
	    strcpy(affiche.fichier_image , paffiche.fichier_image);
	  }
	  else if(status==-3) {
	    sprintf(
		    affiche.texte,
		    "ERREUR: erreur de lecture au format .fc du fichier %s\n",
		    affiche.fichier_image
		    );
	    strcpy(affiche.fichier_image , paffiche.fichier_image);
	  }
	  else if(status==-4) {
	    sprintf(
		    affiche.texte,
		    "ERREUR: erreur de lecture au format .i3d du fichier %s\n",
		    affiche.fichier_image
		    );
	    strcpy(affiche.fichier_image , paffiche.fichier_image);
	  }
	  else if(status==-5) {
	    sprintf(
		    affiche.texte,
		    "ERREUR: desole, le type de donnees dans %s n'est pas (encore) traite par ce programme\n",
		    affiche.fichier_image
		    );
	    strcpy(affiche.fichier_image , paffiche.fichier_image);
	  }
	  else {
	    /* si on change le nom du fichier alors on change aussi 
	       le nom du fichier postscript 
	    */
	    //	    printf("HHH ok\n");
	    epsfilename( affiche.fichier_postscript, affiche.fichier_image);
	    sprintf(affiche.texte,"");

	    copy_header_h2image( (H2Image *)&imauc, (H2Image *)&imaf);
	    reallocate_h2image( (H2Image *)&imauc);

	    reapply++;
	  }
	  
	  
	  
	  affiche.message = XHMV_MSG_TOUT;
	  /* on est oblige de dire cela car dans le cas ou on a une erreur
	     de fichier en entree (ex: le fichier image n'existe pas) il
	     faudrait envoyer 2 messages: l'un pour dire que le fichier
	     specifie n'existe pas et le second pour remettre le nom du fichier
	     image precedemment affiche.
	     Le probleme c'est que le programme de menu n'a pas le temps
	     de lire le 1er message que le 2e est deja arrive (et a ecrase le 1er);
	     il faudrait combiner les 2 messages ou attendre un peu 
	     entre les 2. Le plus simple est encore de dire de tout faire
	  */
	  status = sauve_affiche( fichier_affichage, affiche ); 
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //attention(affiche.fenetre_interloc);
	  
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* s'il faut mettre les cuts aux valeurs par d�faut: */
	if ( affiche.message==XHMV_MSG_SET_DEFAULT_CUTS ) {

	  /* calcul du min et du max de l'image */
	  affiche.cuts[0]=affiche.cuts[1]=imaf.pxl[0];
	  for(i=0; i<imaf.n[0]*imaf.n[1];i++) {
	    affiche.cuts[0] = ( imaf.pxl[i]<affiche.cuts[0]? imaf.pxl[i] : affiche.cuts[0]);
	    affiche.cuts[1] = ( imaf.pxl[i]>affiche.cuts[1]? imaf.pxl[i] : affiche.cuts[1]);
	  }
	 
	  sprintf(affiche.texte,"");
	  affiche.message = XHMV_MSG_CUTS;
	  status = sauve_affiche( fichier_affichage, affiche ); 
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  reapply++;
 
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si les cuts on change */
	/*
	if ( ( affiche.message==XHMV_MSG_CUTS) || (affiche.message==XHMV_MSG_TOUT) ){
	  
	  if ( 
	      ( fabs( (double)(paffiche.cuts[0]-affiche.cuts[0]) ) < 1.e-10 )|| 
	      ( fabs( (double)(paffiche.cuts[1]-affiche.cuts[1]) ) < 1.e-10 )
	      ){
	*/
	if ( 
	    ( affiche.message==XHMV_MSG_CUTS) &&
	    ( ( fabs( (double)(paffiche.cuts[0]-affiche.cuts[0]) ) < 1.e-10 )|| 
	      ( fabs( (double)(paffiche.cuts[1]-affiche.cuts[1]) ) < 1.e-10 ) )
	    || (affiche.message==XHMV_MSG_TOUT) 
	     ){
	  {
	    if (affiche.cuts[1]<=affiche.cuts[0]) {
	      affiche.cuts[0] = paffiche.cuts[0] ;
	      affiche.cuts[1] = paffiche.cuts[1] ;
	      
	      sprintf(affiche.texte,"ATTENTION: valeurs de cuts incorrectes");
	      affiche.message = XHMV_MSG_CUTS;
	      status = sauve_affiche( fichier_affichage, affiche ); 
	      attention(affiche.display_interloc, affiche.fenetre_interloc);
	      //attention(affiche.fenetre_interloc);
	      
	      
	    }
	    else {
	      reapply++;
	    /*
	      if (affiche.cuts[1]<=affiche.cuts[0]) {
	      affiche.cuts[0]=affiche.cuts[1]=imaf.pxl[0];
	      for(i=0; i<imaf.n[0]*imaf.n[1];i++) {
	      affiche.cuts[0] = ( imaf.pxl[i]<affiche.cuts[0]? imaf.pxl[i] : affiche.cuts[0]);
	      affiche.cuts[1] = ( imaf.pxl[i]>affiche.cuts[1]? imaf.pxl[i] : affiche.cuts[1]);
	      }
	      }
	    */
	    }
	  }
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si le zoom a change */
	/*
	if ( ( affiche.message==XHMV_MSG_ZOOM) || (affiche.message==XHMV_MSG_TOUT) ){
	  if ( affiche.zoom != paffiche.zoom) reapply++;
	}
	*/	 
	if ( ( affiche.message==XHMV_MSG_ZOOM) && ( affiche.zoom != paffiche.zoom) 
	     || (affiche.message==XHMV_MSG_TOUT) ){
	  reapply++;
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si le start a change (JE NE VOIS PAS TROP COMMENT MAIS BON! */
	if ( 
	    ( fabs( (double)(paffiche.s[0]-affiche.s[0]) ) < 1.e-10 )|| 
	    ( fabs( (double)(paffiche.s[1]-affiche.s[1]) ) < 1.e-10 )
	    ){
	  reapply++;
	  imauc2.s[0] = affiche.s[0];
	  imauc2.s[1] = affiche.s[1];
	  imauc2.s[2] = 0.;
	}
	
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si la lut a change */
	if ( ( affiche.message==XHMV_MSG_LUT) || (affiche.message==XHMV_MSG_TOUT) ){
	  if(affiche.lut != paffiche.lut) reapply++;
	}
	
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si la periodicite/non periodicite de l'affichage a chang� d'�tat: */
	if ( affiche.message==XHMV_MSG_PERIOD) {
	  //	  printf("le plot periodise \n");
	  period = affiche.period;

	  reapply++;
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* si la barre de lut doit etre affichee ou eteinte */
	if ( affiche.message==XHMV_MSG_LUTBAR) {

	  reapply++;
	}
	/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/* s'il faut changer l'affichage, on le fait */
	if (reapply>0) {

	  //	  printf("HHH reapply!\n");
	  status=evt_zoom(display, affiche.fenetre, 
			  &imaf, &imauc, &imauc2, 
			  hmlut[affiche.lut], &affiche.zoom, affiche.cuts, 
			  ic, 0, affiche.lutbar);
	  //	  printf("HHH sortie de evt zoom \n");
	  /* on n'a pas pu zoomer, on reste l� ou on est */
	  if (status!=0) {
	    affiche.zoom=paffiche.zoom;
	    


	  }
	  
	  affiche.s[0] = imauc2.s[0];
	  affiche.s[1] = imauc2.s[1];
	 
	  // affiche.message = XHMV_MSG_RIEN;
	  // status = sauve_affiche( fichier_affichage, affiche ); 

	  /* on avertit le process interlocuteur s'il existe */
	  // attention(affiche.fenetre_interloc);


	}
	copie_affiche(&paffiche,&affiche);
	

      }
      
      
      break;
      /*------------------------------------------------------------*/
      /* evenement MotionMask: */
    case MotionNotify:
      if(bouton_appuye){
	static int comptage=0;
	
	if (comptage==5) {
	  comptage=0;
	  i1 = evenem.xmotion.x;
	  /* attention a l'inversion haut/bas */
	  i2 = imauc2.n[1]-1-evenem.xmotion.y;

	  imauc2.s[0]-=(i1 - ic[0])*imauc2.p[0][0];;
	  imauc2.s[1]-=(i2 - ic[1])*imauc2.p[1][1];


	  /* on attend que le fichier d'affichage soit libre */
	  compte = 0;
	  do {
	    compte ++;
	    status = lit_affichage( fichier_affichage, &affiche ); 
	  } while( 
		  ((status!=0) || ((status==0) && (affiche.occupe))) 
		  && 
		  (compte<MAXCOMPTE)
		  ) ;
	  if(compte==MAXCOMPTE) {
	    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
	    exit(1);
	  }
	    /*
	  } while( (affiche.occupe) && (compte<MAXCOMPTE)) ;
	  if(compte==MAXCOMPTE) fprintf(stderr,"HHHHHHHHHHHHHHHHH\n");
	    */

	  affiche.s[0] = imauc2.s[0];
	  affiche.s[1] = imauc2.s[1];

	  // printf("%s\n",affiche.texte);
	  status = sauve_affiche(fichier_affichage, affiche); 

	  ic[0] = i1;
	  ic[1] = i2;
	  
	  /* maintenant on recopie l'image imauc ds imauc2 */
	  subimage( &imauc2, &imauc, period);
	  remplit_fenetre( display, affiche.fenetre, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);
	}
	comptage++;
      }
      break;
      /*------------------------------------------------------------*/
    case ButtonRelease:
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      /* Bouton 1 */
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      if ( evenem.xbutton.button==Button1) {
	bouton_appuye=0;
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      /* Bouton 4: 
         touche CTRL enfonc�e: on zoome , bouton 5 on d�zoome 
         touche SHIFT enfonc�e: on se d�place vers la gauche ou vers la droite
         pas de touche enfonc�e: on se d�place vers le haut ou vers le bas
      */
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      if ( ( evenem.xbutton.button==Button4) || ( evenem.xbutton.button==Button5) ){
        /*-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        if ( (alt_l==1) || (alt_r==1)) {
          int pzoom;
          pzoom = affiche.zoom;
          if ( evenem.xbutton.button==Button4) {
            affiche.zoom++;
            if ((affiche.zoom==0)||(affiche.zoom==-1)) affiche.zoom=1;       
          }
          
          if ( evenem.xbutton.button==Button5) {
            affiche.zoom--;
            if ( (affiche.zoom==0) || (affiche.zoom==-1) ) affiche.zoom=-2;
          }
          
          
          //	printf("affiche zoom = %d\n",affiche.zoom);
          status=evt_zoom(display, affiche.fenetre, 
                          &imaf, &imauc, &imauc2, 
                          hmlut[affiche.lut], &affiche.zoom, affiche.cuts, 
                          ic, bouton_appuye, affiche.lutbar);
          /* on n'a pas pu zoomer, on reste l� ou on est */
          if (status!=0) {
            affiche.zoom=pzoom;
          }
          
          affiche.s[0] = imauc2.s[0];
          affiche.s[1] = imauc2.s[1];
          affiche.message = XHMV_MSG_ZOOM;
          sprintf(affiche.texte,"");
          status = sauve_affiche( fichier_affichage, affiche ); 
          
          /* on avertit le process interlocuteur s'il existe */
          attention(affiche.display_interloc, affiche.fenetre_interloc);
          //	attention(affiche.fenetre_interloc);
	}
        /*-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        else{

          if ( (shift_l==1) || (shift_r==1)) {
            int d=10;
            if ( evenem.xbutton.button==Button4) {
              imauc2.s[0] += d*imauc2.p[0][0];
            }
            else if ( evenem.xbutton.button==Button5) {
              imauc2.s[0] -= d*imauc2.p[0][0];
            }
          }
          else{
            int d=10;
            if ( evenem.xbutton.button==Button4) {
              imauc2.s[1] += d*imauc2.p[1][1];
            }
            else if ( evenem.xbutton.button==Button5) {
              imauc2.s[1] -= d*imauc2.p[1][1];
            }
          }
          
	/* maintenant on recopie l'image imauc ds imauc2 */
	subimage( &imauc2, &imauc, period);
	
	remplit_fenetre( display, affiche.fenetre, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);
	
	affiche.s[0] = imauc2.s[0];
	affiche.s[1] = imauc2.s[1];

	status = sauve_affiche( fichier_affichage, affiche );
	          
	}
        /*-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      break;
      /*------------------------------------------------------------*/
    case ButtonPress:

      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      /* Bouton 1 */
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
      if ( evenem.xbutton.button==Button1) {
	bouton_appuye=1;
	ic[0] = evenem.xbutton.x;
	/* attention a l'inversion haut/bas */
	ic[1] = imauc2.n[1]-1-evenem.xmotion.y;

	ic[2] = 0;
      
	ix[0] = (double)ic[0];
	ix[1] = (double)ic[1];
	ix[2] = (double)ic[2];
      
	status = i2x( (H2Image *)&imauc2, x, ix);
	status = x2i( (H2Image *)&imaf, x, ix);

	i1 = floor((double)ix[0]+0.5);
	i2 = floor((double)ix[1]+0.5);
	i3 = floor((double)ix[2]+0.5);
      
      
	ix[0] = i1;
	ix[1] = i2;
	ix[2] = i3;
	i2x( (H2Image *)&imaf, x,ix);

	status = get_plot_features( &affiche, &pid);
	if (status<0) {
	  sprintf(affiche.texte,"plot_image: pb de lecture de %s\n",fichier_affichage);
	  affiche.message = XHMV_MSG_MESG;
	  status = sauve_affiche( fichier_affichage, affiche ); 
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //	attention(affiche.fenetre_interloc);
	}
      
	/* si on est ds le cas de l'affichage non periodis�, on doit v�rifier si l'on est en dehors
	   de l'image */
	if ( 
	    (period==0) && 
	    ( (i1<0) || (i1>=imaf.n[0]) ||
	      (i2<0) || (i2>=imaf.n[1]) ||
	      (i3<0) || (i3>=imaf.n[2]) )
	     ) {
	  
	  affiche.message=XHMV_MSG_MESG;
	  sprintf(affiche.texte,"ATTENTION: vous cliquez en dehors de l'image\n");
	  status = sauve_affiche( fichier_affichage, affiche );
	
	  /* on avertit le process interlocuteur s'il existe */
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //	attention(affiche.fenetre_interloc);
	}
	else {
	  int j1,j2,j3;
	
	  j1 = i1%imaf.n[0];
	  if(j1<0) j1 += imaf.n[0];
	
	  j2 = i2%imaf.n[1];
	  if(j2<0) j2 += imaf.n[1];

	  j3 = i3%imaf.n[2];
	  if(j3<0) j3 += imaf.n[2];

          //	  sprintf(affiche.texte,"%6d %6d %6d %10.5lf %10.5lf %10.5lf %6d %10.5f",
          	  sprintf(affiche.texte,"%d %d %d %g %g %g %d %g",
		  i1, i2 , i3,
		  x[0], x[1], x[2],
		  imauc2.pxl[ic[0] + imauc2.nd[0]*(ic[1] + imauc2.nd[2]*ic[2])],
		  imaf.pxl[j1 + imaf.nd[0]*(j2 + imaf.nd[1]*j3)]
		  );
	  affiche.message=XHMV_MSG_PICK;
	  status = sauve_affiche( fichier_affichage, affiche );
	
	  /* on avertit le process interlocuteur s'il existe */
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //	attention(affiche.fenetre_interloc);
	
	}

      }
      /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

      break;
	  
      /*------------------------------------------------------------*/
    case KeyRelease:
      XLookupString( (XKeyEvent *)&evenem, cbuf, cbufsize, 
		     &keysym, &compose);
      strcpy(cbuf,XKeysymToString(keysym));
	  
      if ((strcmp(cbuf,"Shift_L")==0) ) {
	shift_l = 0;
      }
      if ((strcmp(cbuf,"Shift_R")==0) ) {
	shift_r = 0;
      }
      if ((strcmp(cbuf,"Alt_L")==0) ) {
	alt_l = 0;
      }
      if ((strcmp(cbuf,"Alt_R")==0) ) {
	alt_r = 0;
      }
      
      break;

      /*------------------------------------------------------------*/
    case KeyPress:
      XLookupString( (XKeyEvent *)&evenem, cbuf, cbufsize, 
		     &keysym, &compose);
      strcpy(cbuf,XKeysymToString(keysym));
      
      /*------------------------------------------------------------*/
      /*pour quitter*/
      if ((strcmp(cbuf,"q") == 0) || (strcmp(cbuf,"Q") == 0)) {
	status = lit_affichage( fichier_affichage, &affiche ); 
	affiche.message = XHMV_MSG_CLOSE;
	status = sauve_affiche( fichier_affichage, affiche );
	/* s'il y a un interlocuteur, on le charge de gerer la fermeture
	   sinon plot_image le fait lui meme */
	if(affiche.fenetre_interloc!=0) {
	  attention(affiche.display_interloc, affiche.fenetre_interloc);
	  //attention(affiche.fenetre_interloc);
	}
	else {
	  quitte(fichier_affichage,display);
	}

      }
      
      /*------------------------------------------------------------*/
      /*------------------------------------------------------------*/
      /* shift: */
      if ((strcmp(cbuf,"Shift_L")==0) ) {
	shift_l = 1;
      }
      if ((strcmp(cbuf,"Shift_R")==0) ) {
	shift_r = 1;
      }
      /* alt */
      //      printf("cbuf=%s\n",cbuf);
      if ((strcmp(cbuf,"Alt_L")==0) ) {
	alt_l = 1;
      }
      if ((strcmp(cbuf,"Alt_R")==0) ) {
	alt_r = 1;
      }
      /*------------------------------------------------------------*/
      /* changement de lut */
      else if (strcmp(cbuf,"l") == 0) {
	
	affiche.lut = (affiche.lut+1)%NBLUT;
	status = sauve_affiche( fichier_affichage, affiche );
  
	evt_expose( display, affiche.fenetre, &imauc, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);
	/* un peu degueu: il peut y avoir eu un resize de la fenetre a la main,
	   ds ce cas evt_expose a mis a jour imauc.n, et il faut maintenant 
	   le repercuter sur la taille ds affiche */
	affiche.n[0] = imauc2.n[0];
	affiche.n[1] = imauc2.n[1];

	affiche.message = XHMV_MSG_LUT;
	status = sauve_affiche( fichier_affichage, affiche ); 

	/* on avertit le process interlocuteur s'il existe */
	attention(affiche.display_interloc, affiche.fenetre_interloc);
	//	attention(affiche.fenetre_interloc);
	
	
      }	
      /*------------------------------------------------------------*/
      /* pour centrer l'image*/
      if (
	  (strcmp(cbuf,"c") == 0) || 
	  (strcmp(cbuf,"C") == 0) ) {
	evt_centrage(
		     display, affiche.fenetre, 
		     &imauc, &imauc2, 
		     ic, bouton_appuye,
		     hmlut[affiche.lut], affiche.cuts, affiche.lutbar);

	affiche.s[0] = imauc2.s[0];
	affiche.s[1] = imauc2.s[1];

	status = sauve_affiche( fichier_affichage, affiche ); 

	
      }
      /*------------------------------------------------------------*/
      /* periodisation */
      if (
	  (strcmp(cbuf,"p") == 0) || 
	  (strcmp(cbuf,"P") == 0) ) {

	//	printf("on envoie le message de period %d\n",affiche.period);
	affiche.message=XHMV_MSG_PERIOD;
	status = sauve_affiche( fichier_affichage, affiche ); 

	/* on avertit le process interlocuteur s'il existe */
	attention(affiche.display_interloc, affiche.fenetre_interloc);
	//	attention(affiche.fenetre_interloc);
	
      }
	  
      /*------------------------------------------------------------*/
      /* affichage de la lutbar */
      if (strcmp(cbuf,"L") == 0) {

	//	printf("on envoie le message de period %d\n",affiche.period);
	affiche.message=XHMV_MSG_LUTBAR;
	status = sauve_affiche( fichier_affichage, affiche ); 

	/* on avertit le process interlocuteur s'il existe */
	attention(affiche.display_interloc, affiche.fenetre_interloc);
	//	attention(affiche.fenetre_interloc);
	
      }
	  
      /*------------------------------------------------------------*/
      /* pour zoomer */
      if (
	  (strcmp(cbuf,"z") == 0) || 
	  (strcmp(cbuf,"Z") == 0) ||
	  (strcmp(cbuf,"d") == 0) || 
	  (strcmp(cbuf,"D") == 0)
	  ) {
	int pzoom;
	pzoom = affiche.zoom;
	if ((strcmp(cbuf,"z") == 0) || (strcmp(cbuf,"Z") == 0)) {
	  affiche.zoom++;
	  if ((affiche.zoom==0)||(affiche.zoom==-1)) affiche.zoom=1;       
	  
	}
	else if((strcmp(cbuf,"d") == 0) || (strcmp(cbuf,"D") == 0)) {
	  affiche.zoom--;
	  if ( (affiche.zoom==0) || (affiche.zoom==-1) ) affiche.zoom=-2;
	}
	


	status=evt_zoom(display, affiche.fenetre, 
			&imaf, &imauc, &imauc2, 
			hmlut[affiche.lut], &affiche.zoom, affiche.cuts, 
			ic, bouton_appuye, affiche.lutbar);
	/* on n'a pas pu zoomer, on reste l� ou on est */
	if (status!=0) {
	  affiche.zoom=pzoom;
	}
	
	affiche.s[0] = imauc2.s[0];
	affiche.s[1] = imauc2.s[1];
	affiche.message = XHMV_MSG_ZOOM;
	sprintf(affiche.texte,"");
	status = sauve_affiche( fichier_affichage, affiche ); 

	/* on avertit le process interlocuteur s'il existe */
	attention(affiche.display_interloc, affiche.fenetre_interloc);
	//	attention(affiche.fenetre_interloc);

	break;
      }
      /*------------------------------------------------------------*/
      /* deplacement dans l'image (frappe des touches de fleches) */
      if ( 
	  (strcmp(cbuf,"Up")    == 0 )  ||
	  (strcmp(cbuf,"Down")  == 0 )  ||
	  (strcmp(cbuf,"Left")  == 0 )  ||
	  (strcmp(cbuf,"Right") == 0 )
	  ) {
	
	int d=1;
	if ( ( shift_l==1 ) || (shift_r==1) ) d = 10;
	if ( ( alt_l==1 ) || (alt_r==1) ) d = 50;
	
	if ( strcmp(cbuf,"Up") == 0) {
	  imauc2.s[1] += d*imauc2.p[1][1];
	}
	else if (strcmp(cbuf,"Down") == 0) {
	  imauc2.s[1] -= d*imauc2.p[1][1];
	}
	else if (strcmp(cbuf,"Left") == 0) {
	  imauc2.s[0] -= d*imauc2.p[0][0];
	}
	else if (strcmp(cbuf,"Right") == 0) {
	  imauc2.s[0] += d*imauc2.p[0][0];
	}
	
	
	
	
	
	/* maintenant on recopie l'image imauc ds imauc2 */
	subimage( &imauc2, &imauc, period);
	
	remplit_fenetre( display, affiche.fenetre, &imauc2, hmlut[affiche.lut], affiche.cuts, affiche.lutbar);
	
	affiche.s[0] = imauc2.s[0];
	affiche.s[1] = imauc2.s[1];

	status = sauve_affiche( fichier_affichage, affiche );
	
      }
      break;
      
      /*------------------------------------------------------------*/
      /* fin des cas Key Press */
      break;
    }
    /*------------------------------------------------------------*/
  }
  }
  /*===============================================================================*/
  /* fin de la boucle infinie sur les evenements X                                 */
  /*===============================================================================*/

  XCloseDisplay(display);
  affiche.fenetre = (Window)0L;
  status = sauve_affiche( fichier_affichage, affiche );

  return 0;
}


/************************************************************************/
void evt_expose( Display *display, Window fenetre, 
	     H2Imageuc *imauc, H2Imageuc *imauc2, 
		 couleur *lut, float *cuts, int lutbar) {

  XWindowAttributes a;
  int status;

#ifdef DEBUG
  fprintf(stderr,"expose\n");
#endif

  /* on regarde la taille de la fenetre (elle peut avoir changer
     depuis la derniere fois) */
  status = XGetWindowAttributes(
				display,
				fenetre,
				&a);

  /* si la taille n'est pas celle de l'image imauc2, on recalcule
     imauc2 */
#ifdef DEBUG
  fprintf(stderr,"     a=%d %d\n",a.width,a.height);
  fprintf(stderr,"     imauc2=%d %d\n",imauc2->n[0],imauc2->n[1]);
#endif
  if ( (a.width!=imauc2->n[0]) || (a.height!=imauc2->n[1]) ) {
    imauc2->n[0] = a.width;
    imauc2->n[1] = a.height;
    imauc2->n[2] = 1;
    imauc2->nd[0] = imauc2->n[0];
    imauc2->nd[1] = imauc2->n[1];
    imauc2->nd[2] = imauc2->n[2];
    reallocate_h2image( (H2Image *)imauc2);
    subimage( imauc2, imauc, period);
  }
  
  

  remplit_fenetre( display, fenetre, imauc2, lut, cuts, lutbar );
  /* si on veut savoir ds quelle fenetre a eu lieu l'evenement,
     il faut regarder ds evenem.xany.window 
     (voir /usr/include/X11/Xlib.h)
  */
  
  
}




/*********************************************************************/
int evt_zoom( Display *display, Window fenetre, 
	      H2Imagef *imaf,
	      H2Imageuc *imauc, H2Imageuc *imauc2,
	      couleur *lut,
	      int *zoom,
	      float cuts[2],
	      int ic[3],
	      int flagc,
	      int lutbar){


  XWindowAttributes a;
  int i,j;
  int status;


  double x[3];
  double ix[3], dx[3];
  int i1,i2,i3;

  status=apply_zoom_cuts( imauc, imaf, zoom, cuts);
  if (status!=0) {
    fprintf(stderr,"on n'arrive pas a zoomer avec le facteur %d.\n",*zoom);
    fprintf(stderr,"sans doute un pb de memoire.\n");
    return -2;
  }

  status = XGetWindowAttributes(
				display,
				fenetre,
				&a);


  /* on prend pour centre de l'image zoomee
     le centre de l'image affichee ou bien on immobilise
     le pixel sur lequel on a clique: */
  if (flagc) {
    ix[0] = ic[0];
    ix[1] = ic[1];
    ix[2] = ic[2];

  }
  else {
    ix[0] = a.width/2.;
    ix[1] = a.height/2.;
    ix[2] = 0.;

  }
  
  dx[0] = ix[0];
  dx[1] = ix[1];
  dx[2] = ix[2];
    
  i2x( (H2Image *)imauc2, x, ix);

	 


  imauc2->n[0] = a.width;
  imauc2->n[1] = a.height;
  imauc2->n[2] = 1;
  imauc2->nd[0] = imauc2->n[0];
  imauc2->nd[1] = imauc2->n[1];
  imauc2->nd[2] = imauc2->n[2];
  
  for(i=0;i<3;i++){
    for(j=0;j<3;j++) {
      imauc2->p[i][j]=imauc->p[i][j];
    }
  }


  /* le point fixe de l'image affichee se trouve ds imauc en: */
  x2i( (H2Image *)imauc, x, ix);
  /* les coordonnees du point en haut a gauche est donc en: */
  i1 = ix[0]-dx[0];
  i2 = ix[1]-dx[1];
  i3 = 0;

  ix[0]=i1;
  ix[1]=i2;
  ix[2]=i3;

  i2x( (H2Image *)imauc, imauc2->s, ix);
  

  status = reallocate_h2image( (H2Image *)imauc2);
  if (status==-1) {
    return status;
  }
  
  subimage( imauc2, imauc, period);
  
  evt_expose( display, fenetre, imauc, imauc2, lut, cuts, lutbar);
  return 0;
  
}

/*********************************************************************/
int evt_centrage(
		 Display *display, Window fenetre, 
		 H2Imageuc *imauc, H2Imageuc *imauc2,
		 int ic[3],
		 int flagc,
		 couleur *lut, float *cuts, int lutbar){

  XWindowAttributes a;
  int i,j;
  int status;

  double x[3];
  double ix[3], dx[3];
  int i1,i2,i3;


  status = XGetWindowAttributes(
				display,
				fenetre,
				&a);


  /* on prend pour centre de l'image a afficher
     le centre de l'image ou bien 
     le pixel sur lequel on a clique: */
  if (flagc) {
    ix[0] = ic[0];
    ix[1] = ic[1];
    ix[2] = ic[2];
    i2x( (H2Image *)imauc2, x, ix);
    x2i( (H2Image *)imauc, x, ix);

  }
  else {
    ix[0] = imauc->n[0]/2;
    ix[1] = imauc->n[1]/2;
    ix[2] = 0;

  }

  dx[0] = a.width/2;
  dx[1] = a.height/2;
  dx[2] = 0;


  /* a ce point, ix nous donne les coordonnees ds imauc du
     pixel a mettre au centre de la fenetre */
  i2x( (H2Image *)imauc, x, ix);

  x2i( (H2Image *)imauc2, x, ix);

  /* a ce point, ix nous donne les coordonnees ds imauc2 du
     pixel a mettre au centre de la fenetre */


  /* les coordonnees ds imauc2 du point en haut a gauche sont donc: */
  i1 = ix[0]-dx[0];
  i2 = ix[1]-dx[1];
  i3 = 0;

  ix[0]=i1;
  ix[1]=i2;
  ix[2]=i3;

  i2x( (H2Image *)imauc2, imauc2->s, ix);
  
  subimage( imauc2, imauc,period);

  evt_expose( display, fenetre, imauc, imauc2, lut, cuts, lutbar);

  return 0;
  

}


/**********************************************************************/
/* envoi d'un client message a la fenetre X11 en argument */
int attention(Display *display, Window fenetre) {
  
  Status s;
  XClientMessageEvent event;
  static int deja=0;
  static Display *display2;
  /*------------------------------------------------------------*/
  if (fenetre == (Window)0) return 0;
  /*------------------------------------------------------------*/
  /* ouverture du Display */

  if(deja==0) {
    deja=1;
    display2 = XOpenDisplay("");  
  }
  /* HM 13 mai 2011 
     je ne sais pas trop pourquoi �a ne marche pas avec le display pass�
     en argument.
     Du coup, cet argument est totalement inutile.
  */
  /*------------------------------------------------------------*/
  event.display = display2;
  //  printf("display de attention=%d %d\n",(int)display,(int)display2);
  event.window = fenetre;
  event.format = 8;
  event.type = ClientMessage;  

/* n�cessaire si on veut �viter que Gdk �mette un warning � chaque message re�u */
  event.message_type=0;  

  //  sprintf((char *)event.data.s,"hello!!!\n");

  /*------------------------------------------------------------*/
  s = XSendEvent(event.display, event.window, True , 
		 NoEventMask , (XEvent *)&event );
  /*------------------------------------------------------------*/
  XFlush(display2);  
  /*------------------------------------------------------------*/
  return 0;
}
/**********************************************************************/
int plot_file_name(char *fichier_affichage,int id_plot){
  
  strcpy(fichier_affichage,P_tmpdir);
  sprintf(fichier_affichage+strlen(fichier_affichage),"/tmp_xhmv_%6.6d",id_plot);

  return 0;
}



/*************************************************************************/
/*
 *   fais_moi_signe  : ecrit ds le fichier d'affichage le numero de
 *                     fenetre XWindow a qui le process que gere l'affichage
 *                     doit "faire un signe" a ch fois qu'il modie l'affichage
 *
 * en pratique: 
 * mise a jour du champ fenetre_interloc ds le fichier d affichage pour que
 * le process gerant l affichage emette un evenement ClientMessage a la fenetre 
 * en question.
 */
/*************************************************************************/
int fais_moi_signe(int *id_plot, Display *display_interloc, Window fenetre_interloc){
  
  int status=0;
  char fichier_affichage[200];
  Affichage affiche;
  int compte;
  
  /* le nom du fichier qui contiendra les consignes */
  /*  fprintf(stderr,"fms=%ld\n",fenetre_interloc); */
  status = plot_file_name(fichier_affichage,*id_plot);
  if (status!=0) return status; 
  
  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, &affiche ); 
  } while( 
	  ((status!=0) || ((status==0) && (affiche.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }

  affiche.display_interloc =  display_interloc;
  //  printf("display interloc = %d\n",(int)affiche.display_interloc);
  affiche.fenetre_interloc = fenetre_interloc;

  status=sauve_affiche(fichier_affichage,affiche);

  return status;

}

/*************************************************************************/
/*
  fermeture de la fenetre 
*/
/*************************************************************************/
void quitte(char *fichier_affichage, Display *display){
  int compte=0;
  int status;

  Affichage affiche;

  /* on attend que le fichier d'affichage soit libre */
  compte = 0;
  do {
    compte ++;
    status = lit_affichage( fichier_affichage, &affiche ); 
  } while( 
	  ((status!=0) || ((status==0) && (affiche.occupe))) 
	  && 
	  (compte<MAXCOMPTE)
	  ) ;
  if(compte==MAXCOMPTE) {
    fprintf(stderr,"erreur de communication menu <-> image: timeout!\n");
    exit(1);
  }


    /*
  } while( (affiche.occupe) && (compte<MAXCOMPTE)) ;
  if(compte==MAXCOMPTE) fprintf(stderr,"HHHHHHHHHHHHHHHHH\n");
    */  
  /* on detruit le fichier d'affichage: */
  remove(fichier_affichage);
  

  /* on ferme la fenetre X */  
  XDestroyWindow(display,affiche.fenetre);
  XCloseDisplay(display); 
  
  exit(0);
  
}
/*************************************************************************/
/* creation du nom du fichier postscript:
   on replace l'extension du nom de fichier en entree (s'il y en 
   a une) par "eps"; si le nom en entree est vide, le fichier
   en sortie s'appellera xhmv.eps
*/
/*************************************************************************/
void epsfilename( char *efn, char *fn)
{
  int i, ipos;
  ipos=-1;
  for(i=strlen(fn)-1; i>=0; i--){
    if (fn[i]=='.') {
      ipos=i;
      break;
    }
  }
  
  if (ipos!=-1) {
    for(i=0;i<ipos;i++) efn[i]=fn[i];
    sprintf(efn+ipos,".eps\0");
  }
  else {
    sprintf(efn,"xhmv.eps\0");
  }
  
}
