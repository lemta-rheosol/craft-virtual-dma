/* HM 8 aout 2001
   Je me decide enfin a ecrire une fonction qui lit
   les images au format fc et les stocke dans un tableau

   Je l'ai teste, mais il faudra sans doute le faire encore.
*/
#include <stdio.h>
#include <stdlib.h>
int rfc( char *nom, 
	 int *n1, int *n2, 
	 double *s1, double *s2,
	 double *p1, double *p2,
	 unsigned char **ima){
  
  FILE *f = (FILE *)NULL;
  int istat=0;
  int i,j,n;
  unsigned char buffer[65];

  /* ouverture du fichier fc */
  f = fopen( nom, "r" );
  if ( f == (FILE *)NULL ) {
    fprintf(stderr,"rfc: erreur a l'ouverture du fichier %s\n",nom);
    return(-1);
  }

  

  /* les coordonnees du pixel en bas a gauche de l'image ne sont pas specifiees
     dans le format fc, on les met a 0: */
  *s1 = *s2 = 0.;


  istat = fscanf(f, "%6d     %6d",n1, n2);
  if (istat!=2) {
    fprintf(stderr,"rfc: probleme a la lecture du fichier %s\n",nom);
    fclose(f);
    return(-1);
  }
  istat = fscanf(f, "%lf",p1);
  if (istat!=1) {
    fprintf(stderr,"rfc: probleme a la lecture du fichier %s\n",nom);
    fclose(f);
    return(-1);
  }
  istat = fscanf(f, "%lf",p2);
  if (istat!=1) {
    fprintf(stderr,"rfc: probleme a la lecture du fichier %s\n",nom);
    fclose(f);
    return(-1);
  }

  if (*ima == (unsigned char *)NULL) {
    *ima = malloc( (*n1)*(*n2) );
  }


  for(i=0;i<(*n1)*(*n2)/64;i++) {
    istat = fscanf( f, "%s", buffer );
    for(j=0;j<64;j++) {
      (*ima)[i*64+j]=buffer[j];
    }
  }

  n=(*n1)*(*n2)%64;
  if(n!=0) {
    istat = fscanf( f, "%s", buffer );
    i = (*n1)*(*n2)/64;
    for(j=0;j<n;j++) {
      (*ima)[i*64+j]=buffer[j];
    }
  }


  for(i=0; i<(*n1)*(*n2); i++) {
    (*ima)[i] -= 48;
  }

  fclose(f);
  return(0);

}
