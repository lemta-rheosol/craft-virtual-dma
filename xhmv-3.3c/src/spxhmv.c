/*********************************************************************/
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <X11/cursorfont.h>

#include <xhmv.h>
#include <i3d.h>

#define nint(x) rint(x)
#define SEUIL_ERREUR 1.e-10


GC gc;

/*********************************************************************/
/* calcule dans un mot de type int:
 *d: la position du premier bit non nul 
 *n: 2 a la puissance (le nb de bits entre le premier et le 
      dernier bit non nuls) 
 */

void bidule(int *n, int *d, int i){

  int ii;
  int f;

  *d = 0;
  ii = i;
  while(ii!=0) {
    ii<<=1;
    (*d)++;
  }
  *d = 32 - *d;


  f = 0;
  ii = i;
  while(ii!=0) {
    ii>>=1;
    f++;
  }

  *n = f-*d;
  
  *n = 1<<(*n);
}

/*********************************************************************/
int met_lut_pseudo8( Display *display, 
		     Window fenetre, 
		     couleur *lut ){


  int status;
  XWindowAttributes a;
  int i;

  int rstat=0;

  status = XGetWindowAttributes(
				  display,
				  fenetre,
				  &a);
  if(status==0) {
    fprintf(stderr,"pb pour obtenir les attributs de la fenetre.\n");
    rstat=1;
  }

#ifdef DEBUG
  printf("%d %d\n",
	   a.width, a.height);
  printf("%d\n",a.visual->class);
  printf("%d\n",a.depth);
  printf("attribut map_installed=%d\n",a.map_installed);
#endif
  /*----------------------------------------------------*/
  /* installation de la lut ds le cas 256 pseudo-couleurs */
  if ( (a.visual->class == PseudoColor ) 
       &&
       (a.depth == 8 ) ) {

      XColor *mycol;

      /* remplissage des cellules. On fait l'hypothese que la
       colormap a deja ete installe (en l'occurence par la
      fonction cree_fenetre */
      mycol = malloc(256*sizeof(*mycol));
      for (i=0;i<256;i++) {
	mycol[i].flags = DoRed|DoGreen|DoBlue;
	/* ce serait p.e. mieux de mettre le tableau pixels[256]
	   rempli par le XAllocColorCells et de faire 
	   mycol[i].pixel = pixels[i]
	   mais je ne sais pas comment recuperer ce tableau
	   et comme on utilise a priori les 256 couleurs,
	   je fais comme suit:
	*/
	mycol[i].pixel=i;
	
	mycol[i].red=(unsigned short)(lut[i].r<<8);
	mycol[i].green=(unsigned short)(lut[i].g<<8);
	mycol[i].blue=(unsigned short)(lut[i].b<<8);
      }       
      
      
      /* on place les cellules dans la carte de couleur */
      XStoreColors(display,a.colormap,mycol,256); 

  
  }
  
  

  return rstat;
}
/*********************************************************************/
/* remplit la fenetre "fenetre" presente dans le Display "display"
   avec le contenu de l'image "image".
   Si la fenetre d'affichage n'a pas la taille de image, en redimensionne
   la fenetre.
*/
int remplit_fenetre( Display *display, 
		     Window fenetre,
		     H2Imageuc *image, 
		     couleur *lut,
		     float *cuts,
		     int lutbar
		     ){

  int status;

  int bitmap_pad;   
  int format; 

  XWindowAttributes a;


  /* l'image affichee precedente */
  static H2Imageuc pimage = 
  {     
    "",
    HIMAUCHAR, 
    {0,0,0}, {0,0,0}, 
    { {0.,0.,0.},{0.,0.,0.},{0.,0.,0.} },
    {0.,0.,0.},
    (unsigned char *)0
  };

  XImage *xi;

  int i,j,ii;

  int nbb[3], decal[3];

  /*----------------------------------------------------*/
  /* on regarde les attibuts de la fenetre (on en a besoin
     pour connaitre le mode visuel utilise) */
  status = XGetWindowAttributes(
				display,
				fenetre,
				&a);
  if(status==0) {
    fprintf(stderr,"pb pour obtenir les attributs de la fenetre.\n");
  }

  /*----------------------------------------------------*/
  /* on resize la fenetre si necessaire */
  if ( (pimage.n[0] != image->n[0] ) ||
       (pimage.n[1] != image->n[1] ) ) {
    status=XResizeWindow( display, fenetre, image->n[0], image->n[1]);
#ifdef DEBUG
    printf("XResizeWindow=%d\n",status);
#endif
  }
  XFlush(display);
  status = XGetWindowAttributes(
				display,
				fenetre,
				&a);
  if(status==0) {
    fprintf(stderr,"pb pour obtenir les attributs de la fenetre.\n");
  }

#ifdef DEBUG
  if ( (a.width != image->n[0] ) ||
       (a.height != image->n[1] ) ) {
    printf("%d %d\n",a.width, image->n[0]);
    printf("%d %d\n",a.height,image->n[1]);
    printf("malgr� le XResizeWindow, la fen�tre a tj une taille diff�rente\n");
    printf("de ce qu'on demande!\n");
  }
#endif

  /*----------------------------------------------------*/
  /* installation de la lut ds le cas 256 pseudo-couleurs */
  met_lut_pseudo8( display, fenetre, lut);

  /*----------------------------------------------------*/
  /* affichage de l'image */
  
  if ( 
      ( pimage.pxl ==(unsigned char *)0) || 
      ( pimage.n[0] != image->n[0] ) ||
      ( pimage.n[1] != image->n[1] ) 
      ) {
    
    pimage.n[0] = image->n[0];
    pimage.n[1] = image->n[1];
    pimage.n[2] = 1;
    
    for(i=0; i<3; i++) {
      pimage.nd[i]=pimage.n[i];
    }
  }
  /* on peut gagner un peu de temps en ne faisant le calcul
     des pixels de pimage que lorsque c'est n�cessaire */
  /*----------------------------------------------------*/
  /* ds le cas 256 pseudo-couleurs */
  if ( (a.visual->class == PseudoColor ) 
       &&
       (a.depth == 8 ) ) {
    
    
#ifdef DEBUG
    printf("ici: 8 bits PseudoColor\n");
#endif

    pimage.pxl = realloc(pimage.pxl,1*pimage.n[0]*pimage.n[1]);

    ii=0;
    for(j=0; j<pimage.n[1]; j++) {
      ii = pimage.n[0]*(pimage.n[1]-1-j);

      if(j<image->n[1]) {
	for(i=0; i<pimage.n[0]; i++) {
	  if (i<image->n[0]) {
	    pimage.pxl[ii] = image->pxl[i+j*image->nd[0]];
	  }
	  else {
	    pimage.pxl[ii] = 0;
	  }
	  ii++;
	}
      }
      else {
	for(i=0; i<pimage.n[0]; i++) {
	  pimage.pxl[ii] =0;
	  ii++;
	}
	
      }
    }

    /* attention qu'en faisant ainsi, on ne fait pas l'inversion 
       haut/bas
    */
    /*    pimage.pxl = image->pxl; */
    
  }
  /*----------------------------------------------------*/
  /* ds le cas vraies-couleurs 16 bits*/
  else if ( (a.visual->class == TrueColor )
	    &&
	    (a.depth == 16 ) ) {
    
#ifdef DEBUG
    printf("ici: 16 bits TrueColor\n");
#endif
    pimage.pxl = realloc(pimage.pxl,2*pimage.n[0]*pimage.n[1]);
    
    /* on a besoin de connaitre le nb de bits utilises par couleur rgb
       et de combien de bits il faudra decaler un "niveau"
       code sur le nb de bits convenable pour la placer sur
       le masque de la couleur en question
    */
    
    bidule( nbb, decal, a.visual->red_mask);
    bidule( nbb+1, decal+1, a.visual->green_mask);
    bidule( nbb+2, decal+2, a.visual->blue_mask);
    
#ifdef DEBUG
    for(i=0; i<3; i++) {
      printf("bidule=%d %d\n", nbb[i], decal[i]);
    }
#endif    
    ii=0;
    for(j=0; j<pimage.n[1]; j++) {
      /* on fait l'inversion ht/bas ds cette fonction */
      ii = pimage.n[0]*(pimage.n[1]-1-j); 
	     /*      ii = pimage.n[0]*(j);*/
      for(i=0+j*image->nd[0]; i<pimage.n[0]+j*image->nd[0]; i++) {
	*( (short int *)pimage.pxl + ii) = 
	  (lut[image->pxl[i]].r*nbb[0]/256)<<decal[0] |
	  (lut[image->pxl[i]].g*nbb[1]/256)<<decal[1] |
	  (lut[image->pxl[i]].b*nbb[2]/256)<<decal[2] ;
	ii++;
      }
      
      
    }
    
    
    
  }
  /*----------------------------------------------------*/
  /* ds le cas vraies-couleurs 24 bits*/
  else if ( (a.visual->class == TrueColor )
	    &&
	    (a.depth == 24 ) ) {
    /* on a besoin de connaitre le nb de bits utilises par couleur rgb
       et de combien de bits il faudra decaler un "niveau"
       code sur le nb de bits convenable pour la placer sur
       le masque de la couleur en question
    */
    int nbb[3], decal[3];
    
    bidule( nbb, decal, a.visual->red_mask);
    bidule( nbb+1, decal+1, a.visual->green_mask);
    bidule( nbb+2, decal+2, a.visual->blue_mask);
    

#ifdef DEBUG
    for(i=0; i<3; i++) {
      fprintf(stderr,"bidule=%d %d\n", nbb[i], decal[i]);
    }
    
    fprintf(stderr,"ici: 24 bits TrueColor\n");
#endif

    
    pimage.pxl = realloc(pimage.pxl,4*pimage.n[0]*pimage.n[1]);
    
    ii=0;
    for(j=0; j<pimage.n[1]; j++) {
      /* on fait l'inversion ht/bas ds cette fonction */
      ii = pimage.n[0]*(pimage.n[1]-1-j); 
      /* ii = pimage.n[0]*(j);*/
      for(i=0+j*image->nd[0]; i<pimage.n[0]+j*image->nd[0]; i++) {
	*( (int *)pimage.pxl + ii) = 
	  ((lut[image->pxl[i]].r*nbb[0]/256) << (decal[0])   ) |
	  ((lut[image->pxl[i]].g*nbb[1]/256) << (decal[1])   ) |
	  ((lut[image->pxl[i]].b*nbb[2]/256) << (decal[2])   ) ;
	  ii++;
      }

    }
    
  }
  
  
  
  
  /*----------------------------------------------------*/
  format=ZPixmap;
  bitmap_pad=XBitmapPad(display);

  xi=XCreateImage(display,
		  DefaultVisual(display,DefaultScreen(display)),
		  a.depth,
		  format, 0,
		  (char *)pimage.pxl,
		  pimage.n[0],pimage.n[1],
		  8,0);
  xi->byte_order=LSBFirst;
		  /*		  bitmap_pad,0); */
#ifdef DEBUG
  printf("bitmap_pad=%d\n",bitmap_pad);
#endif
  XPutImage(display,fenetre,gc,xi,
             0,0,0,0,pimage.n[0],pimage.n[1]);  


  if (lutbar) {
    display_lut(display, fenetre, 20, 20 , 170, 300, cuts, lut);
  }

  /*----------------------------------------------------*/
  XFree(xi);
  /*----------------------------------------------------*/
  return 0;
}
/*********************************************************************/
Window cree_fenetre( 
		 Display *display,
		 Visual *classe,
		 unsigned int prof,
		 int fenetre_l, int fenetre_h
		 ) {

  unsigned long masq_attrib;
  XSetWindowAttributes attributs;     
  XWindowAttributes a;

  int fenetre_x, fenetre_y;

  Window fenetre;
  Colormap cmap;
  XColor mycol[256];
  int xstat;
  unsigned long plane_mask[1]; 
  unsigned long pixels[256];


  int i;
  /*-------------------------------------------------------*/
  /* position et taille de la fenetre: */
  fenetre_x = 0;
  fenetre_y = 0;
  if (fenetre_l<=0) fenetre_l = 512;
  if (fenetre_h<=0) fenetre_h = 512;
  /*-------------------------------------------------------*/
  /* Creation de la fenetre */
  /* on DOIT utiliser XCreateWindow et non XCreateSimpleWindow pour 
     pouvoir utiliser une classe visuelle qui n'est pas celle par
     defaut */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* en mode pseudo couleur */
  if ( (classe->class==PseudoColor) ) {

    
    masq_attrib = 
      CWColormap | 
      CWBackingStore | 
      CWBorderPixel | 
      CWBackPixel | 
      CWWinGravity ;

    /* creation de la colormap 
       on n'est pas obliger de la remplir des maintenant
       mais on en a besoin pour creer la fenetre image par XCreateWindow
       creation de la carte de couleur */
    cmap=XCreateColormap(display,
			 DefaultRootWindow(display),
			 classe,
			 AllocNone);
    
    xstat=XAllocColorCells(display,cmap,False,plane_mask,0,pixels,256);   
    if (xstat==0) {
      fprintf(
	      stderr,
	      "ATTENTION: probleme a la creation des cellules de couleurs\n");
      fprintf(stderr,"           par XAllocColorCells.\n");
    }
    
    /*
    for(i=0; i<256; i++) {
      mycol[i].flags = DoRed|DoGreen|DoBlue;
      mycol[i].pixel=pixels[i];

      mycol[i].red=(unsigned short)0;
      mycol[i].green=(unsigned short)0;
      mycol[i].blue=(unsigned short)0;
    }       
    */

    attributs.colormap=cmap;
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* en mode autre que pseudo couleur */
  else {
    masq_attrib = 
      CWBackingStore | 
      CWBorderPixel | 
      CWBackPixel | 
      CWWinGravity ;
    //      CWOverrideRedirect;
  }


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  attributs.win_gravity = NorthWestGravity;
  attributs.backing_store=Always;
  attributs.border_pixel = BlackPixel( display, DefaultScreen(display) ) ;
  attributs.background_pixel = WhitePixel( display, DefaultScreen(display) ) ;
  
  //  attributs.override_redirect=False;

  /* 
     il FAUT mettre les attributs colormap et border_pixel 
     SINON CA NE MARCHE PAS EN 8bpp! 
     et apparemment il faut le faire maintenant et (curieusement) pas 
par un 
     XChangeWindowAttributes cons�cutif
     (th�oriquement il faut aussi mettre l'attribut background_pixel,
     mais cela semble marcher meme si on ne le met pas)
  */

  fenetre=XCreateWindow(display,
                        DefaultRootWindow(display),
                        fenetre_x, fenetre_y,
			fenetre_l, fenetre_h,
                        5, prof, InputOutput, classe, 
                        masq_attrib, &attributs);

  XGetWindowAttributes(
				  display,
				  fenetre,
				  &a);


#ifdef DEBUG
  printf("la taille de la fenetre cree est: %d %d\n", 
	 a.width, a.height);
#endif
  XStoreName (display, fenetre, "xhmv");  
  /* apparemment pas besoin de mettre une lut maintenant */
  /*
  met_lut_pseudo8(display, fenetre, lut);
  */
#ifdef toto
  if ( (classe->class==PseudoColor) ) {
    /* on place les cellules dans la carte de couleur */
    XStoreColors(display,cmap,mycol,256);  
    /* on attache cette colormap a la fenetre d'affichage */
    /* (il faut le faire si on utilise une colormap nvllt creee */
    XSetWindowColormap(display, fenetre, cmap); 
    /* on attache cette colormap a la fenetre d'affichage */
    /* (il faut le faire si on utilise une colormap nvllt creee */
    XSetWindowColormap(display, fenetre, cmap); 
  }
#endif

  /* la question que je me pose est s'il est judicieux d'utiliser
     BlackPixel et WhitePixel qui se refere au display et a l'ecran
     MAIS PAS a la fenetre de l'image. N'y a-t-il pas un risque
     que ces couleurs ne conviennent pas a la colormap qu'on
     a attachee a la fenetre?
  */

  /* Creation et initialisation du contexte graphique 
     c'est indispensble: il FAUT un GC */
  gc = XCreateGC(display, fenetre, 0, 0);       
  
  /* Selection des evenements en entree */
  XSelectInput(display,
	       fenetre,
	       ButtonPressMask | 
	       ButtonReleaseMask | 
	       KeyPressMask | 
	       KeyReleaseMask |
	       PointerMotionMask | 
	       ExposureMask );

  //	       SubstructureRedirectMask|
  //	       VisibilityChangeMask);


  
  /* affichage de la fenetre */
  XMapWindow( display, fenetre);
  XFlush(display);     

  return fenetre;
}
      
/*************************************************************************************/
/* 
   HM march 7th 2011

   snapshot function builds a postscript file of what is plotted in "window"

 */
/*************************************************************************************/
int snapshot( Display *display, Window window, char *filename) {
  
  XImage *xi;
  XWindowAttributes a;

  unsigned char *imar, *imag, *imab;
  int iw, ih, i, ii;

  int status=1;

  /*-----------------------------------------------------------------------------------*/
  XGetWindowAttributes(
		       display,
		       window,
		       &a);

  imar = (unsigned char *)malloc( a.width*a.height * sizeof(*imar) );
  imag = (unsigned char *)malloc( a.width*a.height * sizeof(*imag) );
  imab = (unsigned char *)malloc( a.width*a.height * sizeof(*imab) );

  /*-----------------------------------------------------------------------------------*/
  /* XGetImage fails with a BadMatch error if the part of window actually
     displayed is smaller than the width and the height given as argument
     (see manual of XGetImage).
     For example, if the user moved the window partially out of the display,
     and one enters the width and the height of the window returned by
     XGetWindowAttributes, XGetImage fails.

     Thus, one must determines the actual size of window that is displayed.

  */
  {
    Window rw,cw;
    int rx, ry, wx, wy;
    unsigned int m;
    int dx, dy;

    /*
     On uses XQueryPointer which returns the position of the pointer in 
     window and in the root window. 
    */
    XQueryPointer(display, window, &rw, &cw,
		  &rx, &ry, &wx, &wy, &m);

    /* 
       Position of the upper left corner of the window in
       the root corner
    */
    int xstart, ystart;
    xstart = rx-wx;
    ystart = ry-wy;

    /* 
       Position of the lower right corner of the window in
       the root corner
    */
    int xend, yend;
    xend = xstart+a.width;
    yend = ystart+a.height;


    XWindowAttributes aa; 
    XGetWindowAttributes(display,rw,&aa);
    /*
      aa.width and aa.height gives the size of the root window 
    */


    /* position of the lower right VISIBLE corner of the window in
       the root window  referencee
    */
    int xvend, yvend;
    if (xend > aa.width) {
      xvend=aa.width;
      status=2;
    }
    else {
      xvend = xend;
    }
    if (yend > aa.height) {
      yvend=aa.height;
      status=2;
    }
    else {
      yvend = yend;
    }

    /* 
       Position of the upper left VISIBLE corner of the window in
       the root corner
    */
    int xvstart, yvstart;
    if(xstart<0) {
      xvstart = 0;
      status=2;
    }
    else{
      xvstart=xstart;
    }
    if(ystart<0) {
      yvstart = 0;
      status=2;
    }
    else{
      yvstart=ystart;
    }
    
  
    a.width = xvend-xvstart;
    a.height = yvend-yvstart;

    if (a.width < 0) a.width = 0;
    if (a.height < 0) a.height = 0;

    /* vstart-start gives the position of the upper left visible pixel in the window referencee*/
    xi = XGetImage( display, window, xvstart-xstart, yvstart-ystart, a.width, a.height, 0xFFFFFFFF , ZPixmap);
  }

  /*-----------------------------------------------------------------------------------*/
  if ( (a.visual->class == PseudoColor ) 
       &&
       (a.depth == 8 ) ) {
    
    XColor mycol[256];
    XQueryColors( display, a.colormap, mycol, 256);

    for(i=0; i < a.width*a.height; i++) {
      ii = xi->data[i];
      imar[i] = (unsigned char)(mycol[ii].red>>8);
      imag[i] = (unsigned char)(mycol[ii].green>>8);
      imab[i] = (unsigned char)(mycol[ii].blue>>8);
    }

    
 
  } 
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  else if ( (a.visual->class == TrueColor ) && (a.depth == 24 ) ) {
    int nbb[3], decal[3];
    unsigned int pxl;
    //    unsigned long lpxl;

    bidule( nbb, decal, a.visual->red_mask);
    bidule( nbb+1, decal+1, a.visual->green_mask);
    bidule( nbb+2, decal+2, a.visual->blue_mask);
    
    for( ih=0; ih<a.height; ih++) {
      for( iw=0; iw < a.width; iw++) {

	i = iw + ih*a.width;
	//	printf("%d ",i);
	pxl = ((unsigned int *)(xi->data))[i];
	// lpxl = XGetPixel(xi, iw, ih);
	imar[i] = ( pxl  & a.visual->red_mask)   >> decal[0];
	imag[i] = ( pxl  & a.visual->green_mask) >> decal[1];
	imab[i] = ( pxl  & a.visual->blue_mask)  >> decal[2];
	


	//	printf("%d %d %d %d %d\n",a.width, a.height, imar[i],imag[i],imab[i]);
      }
    }

  }
  else {
    fprintf(stderr,"problem occuring during snapshot (invalid visual)\n");
    status = 0;
  }
  
  /*-----------------------------------------------------------------------------------*/
  if(status>0) {
    /* et on imprime */
    ima2ps(filename, a.width, a.height, imar, imag, imab, 0);
  }
    
  /*-----------------------------------------------------------------------------------*/
  free(imar);
  free(imag);
  free(imab);

  return status;
}

/***************************************************************************************/
/* display lookup table on the window */
int display_lut(
		Display *d,
		Window w,
		int xs, int ys, int dx, int dy,
		float *cuts,
		couleur *lut
		) {

  int xe,ye;
  int x,y;
  int status;
  XImage *xi;
  int format; 
  int bitmap_pad;   
  static float pcuts[2]={0.f,0.f};
  float vcuts[2];


  /*-----------------------------------------------------------------------------------*/
  XWindowAttributes a;
  status = XGetWindowAttributes( d, w, &a);
  if(status==0) {
    fprintf(stderr,"error with XGetWindowAttributes\n");
  }

  /*-----------------------------------------------------------------------------------*/
  /* bottom left */
  ys = a.height - ys - dy;

  xe=xs+dx-1;
  ye=ys+dy-1;

  XSetForeground(d, gc, 0x404040);
  XFillRectangle( d, w, gc, xs, ys, dx, dy );
  XSetForeground(d, gc, 0xB0B0B0);
  XDrawRectangle( d, w, gc, xs, ys, dx, dy );
  /*-----------------------------------------------------------------------------------*/
  /* the position of the lut bar */
  int xls, yls, xle, yle, dlx, dly;

  xls = xs + 15;
  yls = ys + 20;
  
  xle = xs + dx / 4;
  yle = ye - 10;
  
  
  dlx = xle - xls + 1;
  dly = yle - yls + 1;

  /*-----------------------------------------------------------------------------------*/
  XSetForeground(d, gc, 0xFFFFFF);

  XDrawRectangle(d, w, gc, xls-1, yls-1, dlx+1, dly+1);

  /*-----------------------------------------------------------------------------------*/

  {
    char **liste;
    char word[200];
    int n;
    float v;

    /* on essaie de trouver une fonte qui va bien */
    liste = XListFonts(d,"*helvetica-medium-r-normal--18*",10000,&n);

    /* si la precedente n'a pas ete trouvee on essaie celle la: */
    if (n==0) {
      liste = XListFonts(d,"*arial-medium-r-normal*",10000,&n);
    }

    /* si la precedente n'a pas ete trouvee on essaie celle la: */
    if (n==0) {
      liste = XListFonts(d,"*normal--18*",10000,&n);
    }

    /* si la precedente n'a pas ete trouvee on essaie celle la: */
    if (n==0) {
      liste = XListFonts(d,"-misc-fixed-medium-r-normal--18*iso8859*",10000,&n);
    }

    /*
      int i;
    for( i=0; i<n;i++) {
      printf("font no %d: %s\n",i,liste[i]);
    }
    */

    if (n==0) { 
      fprintf(stderr,"pb de font pour l'affichage de la barre de couleurs!\n");
      //      exit(1);
      return -1;
    }
    //    printf("font=%s\n",liste[0]);
    XSetFont(d,gc,XLoadFont(d,liste[0]));


    v = cuts[0];
    sprintf(word,"%11.4e",v);
    y = (yle-yls)/(cuts[0]-cuts[1])*(v-cuts[1])+yls;
    XDrawString(d, w, gc, xle+7, y, word, 11);
    XDrawLine(d, w, gc, xle, y, xle+5, y );

    v = cuts[1];
    sprintf(word,"%11.4e",v);
    y = (yle-yls)/(cuts[0]-cuts[1])*(v-cuts[1])+yls;
    XDrawString(d, w, gc, xle+7, y, word, 11);
    XDrawLine(d, w, gc, xle, y, xle+5, y );

    v = cuts[0]+(cuts[1]-cuts[0])*0.5;
    sprintf(word,"%11.4e",v);
    y = (yle-yls)/(cuts[0]-cuts[1])*(v-cuts[1])+yls;
    XDrawString(d, w, gc, xle+7, y, word, 11);
    XDrawLine(d, w, gc, xle, y, xle+5, y );

    v = cuts[0]+(cuts[1]-cuts[0])*0.25;
    sprintf(word,"%11.4e",v);
    y = (yle-yls)/(cuts[0]-cuts[1])*(v-cuts[1])+yls;
    XDrawString(d, w, gc, xle+7, y, word, 11);
    XDrawLine(d, w, gc, xle, y, xle+5, y );
    
    v = cuts[0]+(cuts[1]-cuts[0])*0.75;
    sprintf(word,"%11.4e",v);
    y = (yle-yls)/(cuts[0]-cuts[1])*(v-cuts[1])+yls;
    XDrawString(d, w, gc, xle+7, y, word, 11);
    XDrawLine(d, w, gc, xle, y, xle+5, y );


  }



  /*-----------------------------------------------------------------------------------*/
  if ( (a.visual->class == TrueColor ) && (a.depth == 24 ) ) {
    int nbb[3], decal[3];
    unsigned int pxl0;
    unsigned int pxl[dlx*dly];
    int v;
    int i;

    bidule( nbb, decal, a.visual->red_mask);
    bidule( nbb+1, decal+1, a.visual->green_mask);
    bidule( nbb+2, decal+2, a.visual->blue_mask);
    
    i = 0;
    for(y=0; y<dly; y++) {
	v = 0 + y*255/(dly-1);

	pxl0 = 
	  (lut[v].r*nbb[0]/256) << decal[0] |
	  (lut[v].g*nbb[0]/256) << decal[1] |
	  (lut[v].b*nbb[0]/256) << decal[2] ;
	
	for(x=0;x<dlx;x++) {
	  i = (dly-1 - y)*dlx+x;
	  pxl[i] = pxl0;
	}
    }
    
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    format=ZPixmap;
    bitmap_pad=XBitmapPad(d);
    
    xi=XCreateImage(d,
		    DefaultVisual(d,DefaultScreen(d)),
		    a.depth,
		    format, 0,
		    (char *)pxl,
		    dlx, dly, 
		    8,0);

    xi->byte_order=LSBFirst;

    XPutImage(d , w , gc , xi , 0 , 0 , xls , yls , dlx , dly );
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  }
  /*-----------------------------------------------------------------------------------*/


  return 0;

}
