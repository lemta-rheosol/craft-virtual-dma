#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <errno.h>

#include <xhmv.h>
#include <lut.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#define FRANCAIS 1
#define ENGLISH 0

/*---------------------------------------------------------------------------*/
char message[200];
char commande[200];

int idplot;
/*---------------------------------------------------------------------------*/
/* les widgets dont ecdm a besoin */
GtkWidget *wzoom;
GtkWidget *wfile;

GtkWidget *wix1;
GtkWidget *wix2;
GtkWidget *wiv;
GtkWidget *wx1;
GtkWidget *wx2;
GtkWidget *wv;

GtkWidget *wcuts[2];
GtkWidget *wlut[NBLUT];

GtkWidget *wpost_orientation[2];
GtkWidget *wpost_nom;
GtkWidget *wmesg;

GtkWidget *wperiod;

GtkWidget *wlutbar;


/*---------------------------------------------------------------------------*/
/* programmes lances a la suite d'un evenement */
void quit(GtkWidget *w, gpointer data);
void texte_zoom(GtkWidget *w, gpointer data);
void texte_file(GtkWidget *w, gpointer data);
void texte_cuts(GtkWidget *w, gpointer data);
void lut(GtkWidget *w, gpointer data);
void default_cuts(GtkWidget *w, gpointer data);
void print(GtkWidget *w, gpointer data);
void periodise(GtkWidget *w, gpointer data);
void lutbar(GtkWidget *w, gpointer data);

/* programme lance en cas de reception d'un message de la fenetre image */
void ecdm(void);

/* les options de la commande */
int option(int argc, char **argv,
	    Affichage *a);
/* le help en ligne */
void usage(int lang);

/*test d'existence d'un fichier */
int fexist(char *file);

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data );
static void destroy( GtkWidget *widget,
                     gpointer   data );

static gboolean message_recu( GtkWidget *w, GdkEvent *event, gpointer data);

/****************************************************************************/
int main(int argc, char **argv){

/*---------------------------------------------------------------------------*/
/* declaration des variables */
  GtkWidget *window;

  GtkWidget *wquit;
  GtkWidget *wlzoom;
  GtkWidget *wlfile;

  GtkWidget *wlix1;
  GtkWidget *wlix2;
  GtkWidget *wliv;
  GtkWidget *wlx1;
  GtkWidget *wlx2;
  GtkWidget *wlv;

  GtkWidget *wlcuts[2];
  GtkWidget *wdefcuts;
  
  GtkWidget *wpost;

  GtkWidget *box0;
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *box3;
  
  char texte[50];
  


  int istat;

  int i;

  Affichage a;
  /*---------------------------------------------------------------------------*/
  /* seul moyen que j'ai trouve d'empecher gtk de nous mettre dans la langue
     de l'environnement, fixe par LANG. Sinon, en francais, on se retrouve
     avec un affichage des printf("%g",...) avec des virgules au lieu de points
     ce qui est tres embetant pour la fabrication du fichier postscript
  */
  setenv("LC_ALL","C",1); /* equivalent a export LC_ALL=C en shell */

  /* ouverture du Display par libsx */
  gtk_init(&argc, &argv);

  /* les options de la commande */
  istat = option(argc, argv, &a);
  if (istat!=0) {
    exit(0);
  }
  
  /*---------------------------------------------------------------------------*/
  /* create a new window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL); 
  /* Here we just set a handler for delete_event that immediately
   * exits GTK. */
  g_signal_connect (window, "delete-event",
		    G_CALLBACK (delete_event), NULL);
  
  g_signal_connect (window, "destroy",
		    G_CALLBACK (destroy), NULL);

  g_signal_connect( window, "client-event", G_CALLBACK( message_recu ), NULL);

  /* Sets the border width of the window. */
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  
  /*-------------------------------------------------------------------------*/
  box0 = gtk_vbox_new (FALSE, 5);
  //  gtk_box_pack_start (GTK_BOX (box0), "hello", FALSE, FALSE, 0);
  /*-------------------------------------------------------------------------*/
  /* le bouton Quit */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Creates a new button with the label "Hello World". */
  wquit = gtk_button_new_with_label ("Quit");
  
  /* When the button receives the "clicked" signal, it will call the
   * function hello() passing it NULL as its argument.  The hello()
   * function is defined above. */
  g_signal_connect (wquit, "clicked",
		    G_CALLBACK (quit), NULL);
  
  /* Instead of gtk_container_add, we pack this button into the invisible
   * box, which has been packed into the window. */
  gtk_box_pack_start (GTK_BOX(box1), wquit , FALSE, FALSE, 0);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (wquit);
  gtk_widget_show (box1); 

  /*-------------------------------------------------------------------------*/
  /* le nom du fichier en entree */

  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wlfile =  gtk_label_new("file: ");
  
  sprintf(texte,"\n");
  wfile  = gtk_entry_new( );

  gtk_entry_set_max_length (GTK_ENTRY (wfile), 1000);
  gtk_entry_set_width_chars( GTK_ENTRY(wfile), 50 );

  g_signal_connect (wfile, "activate", G_CALLBACK (texte_file), NULL);


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_box_pack_start (GTK_BOX(box1), wlfile , FALSE, FALSE, 0 );
  gtk_box_pack_start (GTK_BOX(box1), wfile , FALSE, FALSE, 0 );


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (wlfile);
  gtk_widget_show (wfile);

  gtk_widget_show (box1); 



  /*-------------------------------------------------------------------------*/
  /* le "zoom" */


  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wlzoom = gtk_label_new("zoom: ");
  

  wzoom = gtk_entry_new( );

  gtk_entry_set_max_length (GTK_ENTRY (wzoom), 10);
  gtk_entry_set_width_chars( GTK_ENTRY(wzoom), 4 );

  g_signal_connect (wzoom, "activate", G_CALLBACK (texte_zoom), NULL);

  /* Instead of gtk_container_add, we pack this button into the invisible
   * box, which has been packed into the window. */
  gtk_box_pack_start (GTK_BOX(box1), wlzoom , FALSE, FALSE, 0 );
  gtk_box_pack_start (GTK_BOX(box1), wzoom , FALSE, FALSE, 0 );


  /* The final step is to display this newly created widget. */
  gtk_widget_show (wlzoom);
  gtk_widget_show (wzoom);

  gtk_widget_show (box1); 

  /*-------------------------------------------------------------------------*/
  /* les cuts */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wlcuts[0] = gtk_label_new("cuts min = ");
  wlcuts[1] = gtk_label_new("cuts max = ");


  for(i=0;i<2;i++) {
    wcuts[i] =  gtk_entry_new( );

    gtk_entry_set_max_length (GTK_ENTRY (wcuts[i]), 1000);
    gtk_entry_set_width_chars( GTK_ENTRY(wcuts[i]), 15);
    
    g_signal_connect (wcuts[i], "activate",
		      G_CALLBACK (texte_cuts),
		      wcuts[i]);
    
    gtk_box_pack_start (GTK_BOX(box1), wlcuts[i] , FALSE, FALSE, 0 );
    gtk_box_pack_start (GTK_BOX(box1), wcuts[i] , FALSE, FALSE, 0 );
    
    gtk_widget_show (wlcuts[i]);
    gtk_widget_show (wcuts[i]);
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wdefcuts = gtk_button_new_with_label( "default cuts" );

  g_signal_connect(wdefcuts, "pressed", G_CALLBACK(default_cuts), NULL );
  gtk_box_pack_start (GTK_BOX (box1), wdefcuts, FALSE, TRUE, 20);
  gtk_widget_show (wdefcuts);


  gtk_widget_show (box1); 

  /*-------------------------------------------------------------------------*/
  /* le relev� de points sur l'image */
  
  GtkWidget *wframe;

  wframe=gtk_frame_new("selected pixel   ");
  gtk_box_pack_start (GTK_BOX (box0), wframe , FALSE, FALSE, 10);

  box3 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (wframe), box3);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (box3), box1);
  //  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  box2 = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (box3), box2);
  //  gtk_box_pack_start (GTK_BOX (box0), box2 , FALSE, FALSE, 0);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wlix1 = gtk_label_new("i1 = ");
  gtk_label_set_width_chars( GTK_LABEL(wlix1), 5 );
  wlix2 = gtk_label_new("i2 = ");
  gtk_label_set_width_chars( GTK_LABEL(wlix2), 5 );
  wliv  = gtk_label_new("iv = ");
  gtk_label_set_width_chars( GTK_LABEL(wliv), 4 );

  wlx1 = gtk_label_new("x1 = ");
  gtk_label_set_width_chars( GTK_LABEL(wlx1), 5 );
  wlx2 = gtk_label_new("x2 = ");
  gtk_label_set_width_chars( GTK_LABEL(wlx2), 5 );
  wlv  = gtk_label_new("v  = ");
  gtk_label_set_width_chars( GTK_LABEL(wlv), 4 );


  wix1 = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wix1), 10 );
  wix2 = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wix2), 10 );
  wiv  = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wiv), 10 );

  wx1 = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wx1), 10 );
  wx2 = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wx2), 10 );
  wv  = gtk_label_new("         ");
  gtk_label_set_width_chars( GTK_LABEL(wv), 10 );
  
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_box_pack_start (GTK_BOX(box1), wlix1 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box1), wix1 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box1), wlix2 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box1), wix2 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box1), wliv , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box1), wiv , TRUE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX(box2), wlx1 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), wx1 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), wlx2 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), wx2 , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), wlv , TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), wv , TRUE, FALSE, 0);


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (wlix1);
  gtk_widget_show (wlix2);
  gtk_widget_show (wliv);
  gtk_widget_show (wlx1);
  gtk_widget_show (wlx2);
  gtk_widget_show (wlv);

  gtk_widget_show (wix1);
  gtk_widget_show (wix2);
  gtk_widget_show (wiv);
  gtk_widget_show (wx1);
  gtk_widget_show (wx2);
  gtk_widget_show (wv);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (box1); 
  gtk_widget_show (box2); 
  gtk_widget_show (box3); 
  gtk_widget_show (wframe); 

  /*-------------------------------------------------------------------------*/
  /* periodicite de l'affichage */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* periodisation ou non */
  wperiod = gtk_toggle_button_new_with_label( "Periodize" );
  gtk_toggle_button_set_active( (GtkToggleButton *)wperiod, FALSE );

  g_signal_connect(wperiod, "toggled", G_CALLBACK(periodise), NULL );
  gtk_box_pack_start (GTK_BOX (box1), wperiod, FALSE, TRUE, 0);
  gtk_widget_show (wperiod);

  gtk_widget_show (box1); 

/*-------------------------------------------------------------------------*/
  /* les luts */
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* affichage ou non de la barre de lut */
  wlutbar = gtk_toggle_button_new_with_label( "Lut Bar" );
  gtk_toggle_button_set_active( (GtkToggleButton *)wlutbar, TRUE );

  g_signal_connect(wlutbar, "toggled", G_CALLBACK(lutbar), NULL );



  gtk_box_pack_start (GTK_BOX (box1), wlutbar, TRUE, TRUE, 10);
  gtk_widget_show (wlutbar);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for(i=0; i<NBLUT; i++) { 
    if(i==0) {
      wlut[i] = gtk_radio_button_new_with_label( (GSList *)0 , nomlut[i]);
    }
    else {
      wlut[i] = gtk_radio_button_new_with_label( 
						gtk_radio_button_get_group (GTK_RADIO_BUTTON (wlut[0])),
						nomlut[i]
						 );
    }
    g_signal_connect(wlut[i], "toggled", G_CALLBACK(lut), NULL );

    gtk_box_pack_start (GTK_BOX (box1), wlut[i], TRUE, TRUE, 0);
    gtk_widget_show (wlut[i]);

  }


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (box1); 

  /*-------------------------------------------------------------------------*/
  /* le fichier postscript */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  box1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box0), box1 , FALSE, FALSE, 0);
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wpost = gtk_button_new_with_label("print");

  g_signal_connect(wpost, "pressed", G_CALLBACK(print), NULL );
  
  gtk_box_pack_start (GTK_BOX (box1), wpost, TRUE, TRUE, 10);
  gtk_widget_show (wpost);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  wpost_orientation[0] = gtk_radio_button_new_with_label( (GSList *)0 , "portrait");
  wpost_orientation[1] =  
    gtk_radio_button_new_with_label( 
				    gtk_radio_button_get_group (GTK_RADIO_BUTTON (wpost_orientation[0])),
				    "landscape"
				     );
  for(i=0;i<2;i++) {
    g_signal_connect(wpost_orientation[i], "toggled", G_CALLBACK(lut), NULL );
    
    gtk_box_pack_start (GTK_BOX (box1), wpost_orientation[i], TRUE, TRUE, 0);
    gtk_widget_show (wpost_orientation[i]);
    
  }
 
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  wpost_nom =  gtk_entry_new( );
  gtk_entry_set_max_length (GTK_ENTRY (wpost_nom), 1000);
  gtk_entry_set_width_chars( GTK_ENTRY(wpost_nom), 30 ); 

  gtk_box_pack_start (GTK_BOX (box1), wpost_nom, TRUE, TRUE, 0);
  gtk_widget_show(wpost_nom);
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show(box1);
  /*-------------------------------------------------------------------------*/
  /* une fenetre de message */
  /* (50 caracteres)*/
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wframe=gtk_frame_new("messages   ");
  gtk_box_pack_start (GTK_BOX (box0), wframe , FALSE, FALSE, 10);


  box1 = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (wframe), box1);
  //  gtk_box_pack_start (GTK_BOX (wframe), box1 , TRUE, FALSE, 0);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  wmesg = gtk_label_new("\n\n");
  gtk_label_set_width_chars( GTK_LABEL(wmesg), 50 );  
  gtk_label_set_max_width_chars( GTK_LABEL(wmesg), 50 );  

  gtk_box_pack_start (GTK_BOX(box1), wmesg , FALSE, FALSE, 0);
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  gtk_widget_show (wmesg);
  gtk_widget_show (box1); 
  gtk_widget_show(wframe);

  /*-------------------------------------------------------------------------*/
  gtk_container_add (GTK_CONTAINER (window), box0);
  gtk_widget_show (box0); 
  /* and the window */
  gtk_widget_show (window);
  
  /*-------------------------------------------------------------------------*/

  {
    /*
    
    a.zoom=2;
    a.s[0]=a.s[1]=0.;
    
    a.n[0]=a.n[1]=256;
    
    a.cuts[0]=0.5f;
    a.cuts[1]=0.6f;
    a.lut=1;
    
    strcpy(a.fichier_image,"toto.i3d");
    strcpy(a.fichier_postscript,"toto.eps");
    */

    idplot = plot_image_(a.fichier_image,&a.zoom,a.n,a.s,a.cuts,&a.lut);

    GdkWindow *gw;
    gw = gtk_widget_get_window(window);
    fais_moi_signe(&idplot,GDK_WINDOW_XDISPLAY((GdkDrawable *)gw), GDK_WINDOW_XID((GdkDrawable *)gw) );
   
    /* c'est un peu complique: 
       c'est la fonction plot_image (detachee!) qui cree le fichier
       d'echange entre le menu et la fenetre image, mais pour que ces 
       2 process puissent communiquer, il faut que le fichier 
       d'echange contienne le nom du id X de chacun des 2 process.
       Celui de la fenetre image est remplie sans difficulte lors
       de la creation du fichier puisque c'est plot_image qui le fait.
       En revanche, l'id du menu (ce programme) est plus difficile
       a charger ds le fichier. C'est fais_moi_signe qui s'en charge,
       fais_moi_signe doit donc attendre d'abord que le fichier 
       d'affichage soit cree par plot_image (il se met en boucles
       d'attente pour cela), quand le fichier est cree il ajoute
       dedans l'id du menu.
       Mais il faut ensuite que le menu affiche ce qu'il a lu, 
       une facon simple de ce faire est que le menu s'envoie a lui meme
       l'ordre d'afficher "tout" .
    */
    {
      int status;
      status = get_plot_features( &a, &idplot);
      a.message = XHMV_MSG_TOUT;
      status = put_plot_features( &a, &idplot);
      attention(  GDK_WINDOW_XDISPLAY((GdkDrawable *)gw), GDK_WINDOW_XID((GdkDrawable *)gw)    );
    }

  }
  /*-------------------------------------------------------------------------*/
  /* All GTK applications must have a gtk_main(). Control ends here
   * and waits for an event to occur (like a key press or
   * mouse event). */
  gtk_main ();
  /*-------------------------------------------------------------------------*/



}


/****************************************************************************/
void quit(GtkWidget *w, gpointer data)
{
  int istat;
  close_plot_image_(&idplot);
  gtk_main_quit ();

  exit(0);

}

/***************************************************************/
/* 
   Fonction lisant les options de commande et mettant a jour la
   variable Affichage a.

   valeur retournee:
          0:  tout est OK
         -1:  pb avec le fichier image
         -2:  pb avec le zoom
	 -3:  pb avec les cuts
	 -5:  pb avec la taille de la fenetre

 */
/***************************************************************/
int option(int argc, char **argv,
	    Affichage *a){

  int c;
  int istat;
  double buff;
  int f;

  /* valeur par defaut de a */
  a->fichier_image[0]=0;
  a->n[0] = a->n[1] = -1;
  a->s[0] = a->s[1] = 0.;
  a->cuts[0] = a->cuts[1] = 0.;
  a->lut=0;
  a->zoom = 1;
  a->period=0;

  /* traitement des parametres en entree du programme: */  
  while( ( c=getopt(argc, argv, "phHw:c:z:f:l:" )) !=-1) {
    /* la taille de la fenetre au depart */
    switch(c) {
    case 'w' :
      istat = sscanf( optarg, "%dx%d", 
                      &(a->n[0]), 
                      &(a->n[1])
                      );
      if(istat<=0) {
        perror("xhmv:");
        usage(ENGLISH);
	return -5;
      }
      break;
/* les cuts au debut: */
    case 'c':
      istat = sscanf( optarg, "%g,%g", 
                      &(a->cuts[0]), 
                      &(a->cuts[1])
                      );
      if(istat<=0) {
        perror("xhmv:");
        usage(ENGLISH);
	return -3;
      }

      if (a->cuts[0]>= a->cuts[1] ) {
        buff = a->cuts[1];
        a->cuts[1] = a->cuts[0];
        a->cuts[0] = buff;
      }

      break;
      /* le parametre: facteur de zoom:                                        */
    case 'z':
      istat = sscanf( optarg, "%d" , &(a->zoom) );
      if (istat<=0) {
        perror("xhmv: ");
        return -2;
      }
      break;
      
 /* le parametre nom du fichier en entree: */ 
    case 'f':
      istat = sscanf( optarg, "%s" , a->fichier_image );
      if (istat<=0) {
        perror("xhmv: ");
	return -1;
      }
      
      break;

      /* la lut */
    case 'l':
      istat = sscanf( optarg, "%d" , &(a->lut) );
      if (istat<=0) {
        perror("xhmv: ");
        return -2;
      }
      if ( (a->lut<0) || (a->lut>NBLUT) ) {
	fprintf(stderr,"xhmv: attention valeur de lut incorrect!\n");
	a->lut=0;
      }
      break;

      
      /* mini help en ligne: */
    case 'h':
      usage(ENGLISH);
      exit(0);
      break;


    case 'H':
      usage(FRANCAIS);
      exit(0);
      break;

    }
  }

  /* s'il reste un argument, c'est le nom du fichier */
  if (optind<argc) {
    if ( a->fichier_image[0] != 0 ) {
      fprintf(stderr,"xhmv: le fichier de projection est specifie 2 fois!\n");
      exit (-1);
    }
    else {
      sscanf( argv[optind] , "%s", a->fichier_image );
      optind++;
    }
  }
  
  /* s'il reste un argument, c'est qu'il est en trop */
  if (optind<argc) {
    fprintf(stderr,"xhmv: le parametre %s est de trop.\n",argv[optind]);
    usage(ENGLISH);
    exit (-1);
  }
  
/* on traite le cas ou l'on a fourni un fichier en entree: */
  if (a->fichier_image[0] != 0 ) {
    if (fexist(a->fichier_image)==0) {
      fprintf(stderr,"xhmv: le fichier %s n'existe pas.\n",a->fichier_image);
      exit(-1);
    }
  }
  /* cas ou on n'a pas donner de nom de fichier en entree */
  else{
    fprintf(stderr,"xhmv: aucun nom de fichier n'a ete donne.\n");
    return -1;
    
  }
  return 0;
}

/*******************************************************************************/
void usage(int lang){
  int i;

  if (lang==FRANCAIS) {
    fprintf(stderr,"Mode d'emploi de xhmv version 3.3:\n");
    fprintf(stderr,"   xhmv [-z zoom] [-w lxh] [-c min,max] [-l lut] [-f] fichier\n");
    fprintf(stderr,"      -h  :     affiche le mode d'emploi de CraFT en anglais\n");     
    fprintf(stderr,"      -H  :     affiche le mode d'emploi de CraFT en fran�ais\n");     

    fprintf(stderr,"      zoom:     valeur entiere                   \n");
    fprintf(stderr,"                   si >0 facteur de grossissement   \n");
    fprintf(stderr,"                   si <0 oppos� du facteur de r�duction\n");
    fprintf(stderr,"                (1 par d�faut)\n");
    fprintf(stderr,"      l h :     longeur et hauteur de la fen�tre d'affichage, en pixels\n");
    fprintf(stderr,"                (par d�faut: taille de l'image)\n");
    fprintf(stderr,"      min max:  valeur r�elle de l'image qui sera affich�e \n");
    fprintf(stderr,"                respectivement � 0 et � 255                 \n");
    fprintf(stderr,"                (minimum et maximum de l'image, par d�faut)\n");
    fprintf(stderr,"      lut :     num�ro de la lut (entre 0 et %d)\n",NBLUT-1);
    for(i=0; i<NBLUT;i++) {
      fprintf(stderr,"                %d:   table de lut \"%s\"\n",i,nomlut[i]);
    }
    fprintf(stderr,"      fichier:  nom du fichier contenant l'image � afficher\n");
    
    
    fprintf(stderr,"\n");  
    fprintf(stderr,"le type de fichier (CraFT ou VTK) est reconnu automatiquement\n");
  }
  else {
    fprintf(stderr,"xhmv version 3.3 usage:\n");
    fprintf(stderr,"   xhmv [-h] [-H] [-z zoom] [-w wxh] [-c min,max] [-l lut] [-f] filename\n");
    fprintf(stderr,"      -h  :     print usage (this message)\n");     
    fprintf(stderr,"      -H  :     print usage in french \n");     

    fprintf(stderr,"      zoom:     integer value                    \n");
    fprintf(stderr,"                   if >0 magnification factor       \n");
    fprintf(stderr,"                   if <0 opposite to reduction factor  \n");
    fprintf(stderr,"                (1 by default)\n");
    fprintf(stderr,"      w h :     width and height of the display window (in pixels)     \n");
    fprintf(stderr,"                (image size by default)\n");
    fprintf(stderr,"      min max:  minimum and maximum values in the image     \n");
    fprintf(stderr,"                to be displayed in the range of the LUT. \n");
    fprintf(stderr,"                Image values lower or equal than min are to be\n");
    fprintf(stderr,"                displayed with the bottom color of the LUT.\n");
    fprintf(stderr,"                Image values greater or equal than max are to be\n");
    fprintf(stderr,"                displayed with the top color of the LUT.\n");
    fprintf(stderr,"                (actual image minimum and maximum, by default)\n");
    
    fprintf(stderr,"      lut :     lut id number (in the range 0-%d)\n",NBLUT-1);
    for(i=0; i<NBLUT;i++) {
      fprintf(stderr,"                %d:   lookup table \"%s\"\n",i,nomlut[i]);
    }
    fprintf(stderr,"      filename: name of the file to be displayed (in CraFT ot legacy VTK format).\n");
    
    
    fprintf(stderr,"                The file format is automatically recognized\n");
  }


}

/*********************************************************************************/
static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data 
			      ) {
  /* If you return FALSE in the "delete-event" signal handler,
   * GTK will emit the "destroy" signal. Returning TRUE means
   * you don't want the window to be destroyed.
   * This is useful for popping up 'are you sure you want to quit?'
   * type dialogs. */
  
  //  g_print ("delete event occurred\n");
  
  /* Change TRUE to FALSE and the main window will be destroyed with
   * a "delete-event". */
  quit((GtkWidget *)0,(gpointer)0);
  return FALSE;
}

/*********************************************************************************/
/* Another callback */
static void destroy( GtkWidget *widget, gpointer   data ) {
  quit((GtkWidget *)0,(gpointer)0);
  gtk_main_quit ();
}
/**************************************************************************************/
void lut(GtkWidget *w, gpointer data) {
  int i;

  Affichage a;
  char fichier_affichage[100];
  int status;




  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }
  
  
  /* quel est le bouton de choix de lut active? */
  for(i=0; i<NBLUT; i++) {
    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (wlut[i]))) {
      break;
    }
  }

  
  a.lut = i;
  a.message = XHMV_MSG_LUT;
  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display,a.fenetre); 

}
/*****************************************************************************/
/* affichage ou pas de la barre de lut */
void lutbar(GtkWidget *w, gpointer data){

  int i;

  Affichage a;
  char fichier_affichage[100];
  int status;
  
  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }
  
  
  a.message = XHMV_MSG_LUTBAR;
  a.lutbar = (a.lutbar+1)%2;

  gtk_toggle_button_set_active( (GtkToggleButton *)wlutbar, a.lutbar );
  /*
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (wlutbar))) {
    a.lutbar = 1;
  }
  else {
    a.lutbar = 0;
  }
  */

  //  printf("lutbar=%d\n",a.lutbar);

  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display,a.fenetre); 


}


/*****************************************************************************/
/* on vient de rentrer une nlle valeur de zoom */
void texte_zoom(GtkWidget *w, gpointer data)
{
  char *p;
  int izoom;

  char fichier_affichage[100];

  Affichage a;
  int status;

  char texte[20];

  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_zoom\n");
    exit(1);
  }

  /* on lit la nlle valeur du zoom */
  p =  (char *)gtk_entry_get_text( GTK_ENTRY(w) );
  status = sscanf(p,"%d",&izoom);
  if(status==1) {
    a.zoom = izoom;
    a.message = XHMV_MSG_TOUT;
    
    status = plot_file_name(fichier_affichage,idplot);
    status=sauve_affiche(fichier_affichage,a);
    status = attention(a.display,a.fenetre); 
  }
  else {
    sprintf(texte,"%d",a.zoom);
    gtk_entry_set_text (GTK_ENTRY(w), texte);

  }
  

}

/*****************************************************************************/
/* on vient de rentrer un nouveau fichier */
void texte_file(GtkWidget *w, gpointer data)
{
  int istat;
  char *p;
  int izoom;

  char fichier_affichage[100];

  Affichage a;
  int status;

  /* on lit la nlle valeur du zoom */
  p =  (char *)gtk_entry_get_text( GTK_ENTRY(w) );
  
  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }
    
  
  strcpy(a.fichier_image,p);
  a.message = XHMV_MSG_TOUT;

  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display, a.fenetre); 
  

}
/*****************************************************************************/
/* en cas de chgt de cuts */
void texte_cuts(GtkWidget *w, gpointer data){

  char *p;
  double cuts[2];

  char fichier_affichage[100];

  Affichage a;
  int status;

  int i;
  char texte[20];

  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }

  /* on lit la nlle valeur des cuts */
  for(i=0; i<2;i++) {
    p = (char *)gtk_entry_get_text( GTK_ENTRY(wcuts[i]) );
    status = sscanf(p,"%lf",&cuts[i]);
    //    printf("cuts[%d]=%g\n",i,cuts[i]);

    if(status==1) {
      //      printf("ici\n");
      a.cuts[i]=cuts[i];
      a.message = XHMV_MSG_TOUT;
      
      status = plot_file_name(fichier_affichage,idplot);
      status = sauve_affiche(fichier_affichage,a);
      status = attention(a.display,a.fenetre); 
    }
    else{
      sprintf(texte,"%g",a.cuts[i]);
      //      printf("texte=%s\n",texte);
      gtk_entry_set_text (GTK_ENTRY(wcuts[i]), texte);     
    }

  }

  

}
/*****************************************************************************/
/* periodisation ou pas de l'affichage */
void periodise(GtkWidget *w, gpointer data){

  int i;

  Affichage a;
  char fichier_affichage[100];
  int status;

  //  printf("periodise!\n");
  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }


  a.message = XHMV_MSG_PERIOD;
  a.period = (a.period+1)%2;
    
  gtk_toggle_button_set_active( (GtkToggleButton *)wperiod, a.period );

  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display,a.fenetre); 


}

/****************************************************************************/
void default_cuts(GtkWidget *w, gpointer data) {

  Affichage a;
  char fichier_affichage[100];
  int status;

  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }

  
  a.message = XHMV_MSG_SET_DEFAULT_CUTS;

  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display,a.fenetre); 
  

  
}
/****************************************************************************/
void print(GtkWidget *w, gpointer data) {
  char *nom_image_eps;

  Affichage a;
  char fichier_affichage[100];
  int status;

  status = get_plot_features(&a,&idplot);
  if (status != 0) {
    fprintf(stderr,"probleme ds texte_file\n");
    exit(1);
  }

  
  nom_image_eps = (char *)gtk_entry_get_text( GTK_ENTRY(wpost_nom) );
  if ( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(wpost_orientation[0]) ) == TRUE ) {
    a.orientation_postscript = 0;
  }
  else {
    a.orientation_postscript = 1;
  }
  

  strcpy(a.fichier_postscript,nom_image_eps);
  a.message = XHMV_MSG_PRINT;
  status = plot_file_name(fichier_affichage,idplot);
  status=sauve_affiche(fichier_affichage,a);
  status = attention(a.display,a.fenetre); 
  

  
}
/*********************************************************************************/
static gboolean message_recu( GtkWidget *w, GdkEvent *event, gpointer data) {
  int status;
  Affichage a;
  static Affichage pa = { " ", 0, 0., 0., 0., 0., 0, 0, 0, 0, 0, 0, 0, 0, 0, " "};


  /*------------------------------------------*/
  /* nettoie la fenetre message (a tout hasard) */
  gtk_label_set_text( GTK_LABEL(wmesg),"");
  /* et on affichera le message a chaque fois qu'on recoit qq chose, 
     je sais: il y aurait moyen de faire mieux!!!
  */
   /*------------------------------------------*/
  /* on lit le fichier des donnees d'affichage */
  status = get_plot_features(&a,&idplot);
  
  if (status != 0) {
    fprintf(stderr,"probleme lors de l'echange des donnees entre le menu\n");
    fprintf(stderr,"et la fenetre image\n");
    exit(0);
  }

  /*------------------------------------------*/
  /* on quitte */
  if (a.message==XHMV_MSG_CLOSE) quit((GtkWidget *)0,(gpointer)0);
  /*------------------------------------------*/
  /* mise a jour de la valeur du zoom */
  if ( (a.message == XHMV_MSG_ZOOM)  || (a.message == XHMV_MSG_TOUT) )  {
      char texte[20];
      if( a.zoom != pa.zoom ) {
	sprintf(texte,"%d",a.zoom);
	gtk_entry_set_text (GTK_ENTRY (wzoom), texte);
      }
      gtk_label_set_text( GTK_LABEL(wmesg),a.texte);

  }
  /*------------------------------------------*/
  /* periodisation de l'affichage oui ou non */
  if ( (a.message == XHMV_MSG_PERIOD) ){
    //        SetToggleState(wperiod,a.period);
    periodise(wperiod, (void *)0);
  }
  /*------------------------------------------*/
  /* affichage ou effacement de la barre de lut */
  if ( (a.message == XHMV_MSG_LUTBAR) ){
    lutbar(wlutbar, (void *)0);
  }
      /*------------------------------------------*/
  /* mise a jour de la lut(te) */
  if ( (a.message == XHMV_MSG_LUT)  || (a.message == XHMV_MSG_TOUT) )  {
    char texte[20];
    if( a.lut != pa.lut ) {
      sprintf(texte,"%d",a.lut);

      gtk_toggle_button_set_active( (GtkToggleButton *)wlut[a.lut], TRUE );
    }
  }
  
  /*------------------------------------------*/
  /* affichage des valeurs relevees au curseur */
  if (a.message == XHMV_MSG_PICK) {
    if ( strcmp(a.texte, pa.texte) != 0 ) {
      int i1,i2,i3;
      double x1,x2,x3;
      int iv;
      double v;
      char texte[20];
      
      status = sscanf(
		      a.texte,
		      "%6d %6d %6d %lf %lf %lf %6d %lf",
		      &i1,&i2,&i3, &x1,&x2,&x3, &iv, &v);
      
      if(status == 8) {
	sprintf(texte,"%d",i1);
	gtk_label_set_text( GTK_LABEL(wix1),texte);
	
	sprintf(texte,"%d",i2);
	gtk_label_set_text( GTK_LABEL(wix2),texte);
	
	sprintf(texte,"%d",iv);
	gtk_label_set_text( GTK_LABEL(wiv),texte);
	
	sprintf(texte,"%g",x1);
	gtk_label_set_text( GTK_LABEL(wx1),texte);
	
	sprintf(texte,"%g",x2);
	gtk_label_set_text( GTK_LABEL(wx2),texte);
	
	sprintf(texte,"%g",v);
        //        printf("--------> %g %s \n",v,texte);
	gtk_label_set_text( GTK_LABEL(wv),texte);
      }

    }   
  }
    
  /*------------------------------------------*/
  /* mise a jour du nom du fichier */
  if ( (a.message == XHMV_MSG_FILE)  || (a.message == XHMV_MSG_TOUT) ) {
    strcpy(pa.fichier_image, gtk_entry_get_text(GTK_ENTRY(wfile)) );
    if ( strcmp(a.fichier_image,pa.fichier_image) != 0) 
      {
	//	printf("a.fichier_image=%s\n",a.fichier_image);
	gtk_entry_set_text (GTK_ENTRY (wfile), a.fichier_image);	
      }

    
    strcpy(pa.fichier_postscript, gtk_entry_get_text(GTK_ENTRY(wpost_nom)));
    if ( strcmp(a.fichier_postscript,pa.fichier_postscript) != 0) 
      {
	gtk_entry_set_text (GTK_ENTRY (wpost_nom),a.fichier_postscript);
      }
    
      gtk_label_set_text( GTK_LABEL(wmesg),a.texte);


  }
  /*------------------------------------------*/
  /* affichage des cuts */
  if ( (a.message == XHMV_MSG_CUTS) || (a.message == XHMV_MSG_TOUT) ){
    char texte[20];
    int i;
    for(i=0;i<2;i++) {
      sprintf(texte,"%g",a.cuts[i]);
	gtk_entry_set_text (GTK_ENTRY (wcuts[i]), texte);
    }
    gtk_label_set_text( GTK_LABEL(wmesg),a.texte);
  }      
  /*------------------------------------------*/
  /* la fenetre de message */
  if ( (a.message == XHMV_MSG_MESG) || (a.message == XHMV_MSG_TOUT) ) {
    gtk_label_set_text( GTK_LABEL(wmesg),a.texte);
  }
  /*------------------------------------------*/
  /* on memorise les donnees affichees */
  copie_affiche( &pa, &a);
  


}

