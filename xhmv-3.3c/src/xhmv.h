#ifndef _XHMV_
#define _XHMV_
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <i3d.h>
/*------------------------------------------------------*/
#define LONGTXT 200 

typedef struct{

  char fichier_image[400];        /* nom du fichier-image a afficher */

  int zoom;                       /* facteur de zoom */

  float cuts[2];                  /* troncatures min et max des valeurs des pixels */

  double s[2];                    /* position ds l'image du pixel a mettre en bas 
				     a gauche ds la fenetre d'affichage */

  int n[2];                       /* taille de la fenetre */

  int lut;                        /* numero de la lut utilisee (voir lut.h) */

  Display *display;               /* le "Display *" de la fenetre d'affichage */

  Window fenetre;                 /* le "Window" de la fenetre d'affichage */

  Display *display_interloc;
  Window fenetre_interloc;        /* le "Window" de la fenetre du process qui discute  
				     le process gerant la fenetre (si il y en a une)
				     contient 0 s'il n'y en a pas.
				     C'est la fenetre a qui il faut envoyer des 
				     evenements ClientMessage a chaque fois 
				     que le process gerant la fenetre d'affichage a
				     change qq chose ds le fichier d'affichage.
				  */

  int message;                    /* message echange entre le process gerant
                                     la fenetre et le process envoyant des
				     plot_image ou replot_image ou modify_plot_image */

  int occupe;                     /* ce flag indique si le fichier est deja en cours 
				     de lecture
				     ou d'ecriture par un autre process (ds ce cas 
				     il est non nul)
				     ou pas (ds ce cas il est null).
				  */

  char texte[LONGTXT];                /* texte divers */

  char fichier_postscript[400];       /* nom du fichier-postscript pour une sauvegarde
				         de l'image affichee */
  int orientation_postscript;     /* orientation du fichier postscript
				     0 = portrait
				     1 = landscape */

  int period;                     /* indique si l'affiche doit etre periodisé (1) ou non (0) */

  int lutbar;                     /* indique si on affiche ou pas la barre de lut */

} Affichage;

/* la liste des messages possibles*/
/*   

      XHMV_MSG_RIEN:  rien a faire:
      XHMV_MSG_TOUT:  tout a faire, c'est a dire qu'il faut lister tous les champs du fichier
                      d'affichage et les appliquer (si necessaire) a l'affichage: 
      XHMV_MSG_CLOSE: il faut fermer l'affichage 
      XHMV_MSG_FILE : le nom du fichier en entree a change
      XHMV_MSG_ZOOM : le zoom a change
      XHMV_MSG_CUTS : les cuts ont change
      XHMV_MSG_PICK : une valeur a ete releve au curseur
      XHMV_MSG_LUT  : la lut a change
      XHMV_MSG_PRINT : impression postscript demandee
      XHMV_MSG_MESG : un message a ete envoye (il est ds le champ "texte")
      XHMV_MSG_PERIOD : il faut périodiser ou dépériodiser l'affichage 
      XHMV_MSG_LUTBAR : il faut afficher ou pas la barre de lut 

*/
#define XHMV_MSG_CLOSE  -1
#define XHMV_MSG_RIEN  0
#define XHMV_MSG_TOUT  1

#define XHMV_MSG_FILE  2
#define XHMV_MSG_ZOOM  3
#define XHMV_MSG_CUTS  4
#define XHMV_MSG_PICK  5
#define XHMV_MSG_LUT   6
#define XHMV_MSG_PRINT 7
#define XHMV_MSG_MESG  8
#define XHMV_MSG_PERIOD 9
#define XHMV_MSG_LUTBAR 10
#define XHMV_MSG_SET_DEFAULT_CUTS 11

/*------------------------------------------------------*/
typedef struct{
  unsigned char r;
  unsigned char g;
  unsigned char b;
} couleur;

/*------------------------------------------------------*/
/* declaration des fonctions */

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/* LA fonction */

int plot_image_(
		char *fichier_image,
		int *zoom,
		int *n,
		double *s,
		float *cuts,
		int *lut
		) ;

int replot_image_(int *id_plot);
int modify_plot_image_(
		      int *id_plot,
		      char *fichier_image,
		      int *zoom,
		      int *n,
		      double *s,
		      float *cuts,
		      int *lut
		      ) ;

int close_plot_image_(int *id_plot);

int plot_file_name(char *fichier_affichage,int id_plot);
int get_plot_features(Affichage *a, int *id_plot);
int put_plot_features(Affichage *a, int *id_plot);

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/* les fonctions utilisees */

int choix_visual(   Display *display, XVisualInfo *vinfo_return);

int lit_affichage( char *fichier_affiche, Affichage *affiche );
int print_affichage( FILE *f, int verbosite, Affichage affiche);
int sauve_affiche(char *fichier_affiche, Affichage affiche);
int lecture_image( char *fichier, H2Imagef *image);
int copie_affiche(Affichage *dst, Affichage *src);

int apply_zoom_cuts( H2Imageuc *imauc, H2Imagef *imaf, int *zoom, float cuts[2]);
int met_lut_pseudo8( Display *display, 
		     Window fenetre, 
		     couleur *lut ) ;
void subimage( H2Imageuc *imauc2, H2Imageuc *imauc, int periodicite);

Window cree_fenetre( 
		    Display *display,
		    Visual *classe,
		    unsigned int prof,
		    int fenetre_l, int fenetre_h
		    ) ;
int remplit_fenetre( Display *display, 
		     Window fenetre,
		     H2Imageuc *image, 
		     couleur *lut,
		     float *cuts,
		     int lutbar
		     ) ;

void evt_expose( Display *display, Window fenetre, 
		 H2Imageuc *imauc, H2Imageuc *imauc2,
		 couleur *lut, float *cuts, int lutbar);
int evt_zoom( Display *display, Window fenetre, 
	      H2Imagef *imaf,
	      H2Imageuc *imauc, H2Imageuc *imauc2,
	      couleur *lut,
	      int *zoom,
	      float cuts[2],
	      int ic[3],
	      int flagc,
	      int lutbar);

int evt_centrage(
		 Display *display, Window fenetre, 
		 H2Imageuc *imauc, H2Imageuc *imauc2,
		 int ic[3],
		 int flagc,
		 couleur *lut, float *cuts, int lutbar);

int attention(Display *display, Window fenetre) ;

int fais_moi_signe(int *id_plot, Display *display, Window fenetre);

void quitte(char *fichier_affichage, Display *display);


void copie_postscript(
		      H2Imageuc ima3, H2Imageuc ima2, couleur *lut,
		      char *nom_image_eps, int turn);

int snapshot( Display *display, Window window, char *filename);

void ima2ps( char *name, 
	    int n1, int n2, 
	    unsigned char *r,
	    unsigned char *g,
	    unsigned char *b,
	    int turnflag
             );

void copie_postscript(
		      H2Imageuc ima3, H2Imageuc ima2, couleur *lut,
		      char *nom_image_eps, int turn);

int display_lut(
		Display *d,
		Window w,
		int xs, int ys, int dx, int dy,
		float *cuts,
		couleur *lut
		);
#endif
